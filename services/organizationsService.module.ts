import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';

import {SearchOrganizationsService} from './searchOrganizations.service';


@NgModule({
  imports: [
    CommonModule, FormsModule
  ],
  declarations: [
  ],
  providers:[
  SearchOrganizationsService
],
  exports: [
    ]
})
export class OrganizationsServiceModule { }
