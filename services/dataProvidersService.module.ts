import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';

import {SearchDataprovidersService} from './searchDataproviders.service';


@NgModule({
  imports: [
    CommonModule, FormsModule
  ],
  declarations: [
  ],
  providers:[
  SearchDataprovidersService
],
  exports: [
    ]
})
export class DataProvidersServiceModule { }
