import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CustomizationService} from "./customization.service";


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [
    CustomizationService
  ],
  exports: []
})
export class CustomizationServiceModule {
}
