import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {CustomOptions} from './servicesUtils/customOptions.class';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: "root"
})
export class UtilitiesService {
    constructor(private http: HttpClient) {
    }

    uploadPhoto(url: string, photo: File): Observable<any> {
        const formData = new FormData();
        formData.append('photo', photo);
        return this.http.post(url, formData, {withCredentials: true});
    }

    deletePhoto(url: string): Observable<any> {
        return this.http.delete(url, {withCredentials: true});
    }
    
}
