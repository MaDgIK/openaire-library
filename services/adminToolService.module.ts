import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HelpContentService} from "./help-content.service";
import {PluginsService} from "./plugins.service";


@NgModule({
  imports: [
    CommonModule,
  ],
  declarations:[ ],
  providers: [HelpContentService, PluginsService],


})
export class AdminToolServiceModule { }
