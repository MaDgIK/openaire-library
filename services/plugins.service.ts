import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {CustomOptions} from "./servicesUtils/customOptions.class";
import {PluginTemplate} from "../utils/entities/adminTool/pluginTemplate";
import {Plugin} from "../utils/entities/adminTool/plugin";
import {properties} from "../../../environments/environment";

@Injectable()
export class PluginsService {

  constructor(private http: HttpClient) {
  }
  getPluginTemplates(pageId) {
    return this.http.get<Array<PluginTemplate>>(properties.adminToolsAPIURL + 'pluginTemplates' + (pageId ? '/page/' + pageId : ''))

  }

  savePluginTemplate(pluginTemplate: PluginTemplate) {
    return this.http.post<PluginTemplate>(properties.adminToolsAPIURL + 'pluginTemplate/save', pluginTemplate, CustomOptions.getAuthOptionsWithBody());
  }

  updatePluginTemplateOrder(pluginTemplate: PluginTemplate, position) {
    return this.http.post<PluginTemplate>(properties.adminToolsAPIURL + 'pluginTemplate/save/order/' + position, pluginTemplate, CustomOptions.getAuthOptionsWithBody());
  }

  updatePluginOrder(plugin: Plugin, position, community) {
    return this.http.post<Plugin>(properties.adminToolsAPIURL + 'community/'+community+'/plugin/save/order/' + position, plugin, CustomOptions.getAuthOptionsWithBody());
  }

  savePlugin(plugin, community) {
    return this.http.post<Plugin>(properties.adminToolsAPIURL + 'community/' + community + '/plugin/save', JSON.stringify(plugin), CustomOptions.getAuthOptionsWithBody());
  }

  deletePluginTemplate(id) {
    return this.http.delete<PluginTemplate>(properties.adminToolsAPIURL + 'pluginTemplate/' + id, CustomOptions.getAuthOptionsWithBody());
  }
  deletePlugin(id) {
    return this.http.delete<PluginTemplate>(properties.adminToolsAPIURL + 'plugin/' + id, CustomOptions.getAuthOptionsWithBody());
  }

  countPluginTemplatePerPage(pid: string) {
    return this.http.get(properties.adminToolsAPIURL + properties.adminToolsPortalType + '/' + pid + '/pluginTemplate/page/count');
  }

  countPluginTemplatePerPageForAllPortals() {
    return this.http.get(properties.adminToolsAPIURL + 'pluginTemplate/page/count');
  }

  getPluginsByPage(pid: string, pageId: string) {
    return this.http.get<Array<Plugin>>(properties.adminToolsAPIURL + properties.adminToolsPortalType + '/' + pid + '/plugins/page/' + pageId);
  }

  getPluginTemplatesByPage(pid: string, pageId: string) {
    return this.http.get<Array<PluginTemplate>>(properties.adminToolsAPIURL + properties.adminToolsPortalType + '/' + pid + '/pluginTemplates/page/' + pageId);
  }
  getPluginsByPageRoute(pid:string, route:string){
    let url = properties.adminToolsAPIURL + 'community/' +pid+'/plugins/page/route?route=' + route;
    return this.http.get<Array<Plugin>>(/*(properties.useLongCache) ? (properties.cacheUrl + encodeURIComponent(url) + (properties.forceCacheReload?'&forceReload=true':'')) : */url);
  }
  getPluginTemplatesByPageRoute(pid:string, route:string){
    let url =   properties.adminToolsAPIURL + 'community/' + pid + '/pluginTemplates/page/route?route=' + route;
    return this.http.get<Array<PluginTemplate>>(/*(properties.useLongCache) ? (properties.cacheUrl + encodeURIComponent(url) + (properties.forceCacheReload?'&forceReload=true':'')) :*/ url);
  }

  togglePlugin(id: string, status: boolean, community) {
    return this.http.post(properties.adminToolsAPIURL + 'community/' + community + '/plugin/status/' + id, status, CustomOptions.getAuthOptionsWithBody());
  }
  getPluginById(id: string) {
    return this.http.get<Plugin>(properties.adminToolsAPIURL + 'plugin/'  + id);
  }

  getPluginTemplateById(id: string) {
    return this.http.get<PluginTemplate>(properties.adminToolsAPIURL + 'pluginTemplates/'  + id);
  }

}
