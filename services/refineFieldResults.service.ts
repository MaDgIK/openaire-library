import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {throwError} from 'rxjs';
import {AutoCompleteValue} from '../searchPages/searchUtils/searchHelperClasses.class';



import {RefineResultsUtils} from './servicesUtils/refineResults.class';
import{EnvProperties} from '../utils/properties/env-properties';
import {map} from "rxjs/operators";

@Injectable()
export class RefineFieldResultsService {
     constructor(private http: HttpClient ) {}
     getRefineFieldsResultsByEntityName(fields:string[], entityName:string, properties:EnvProperties, communityQuery=null):any{
        let url = properties.searchAPIURLLAst + this.getSearchAPIURLForEntity(entityName)+"?format=json&refine=true&page=1&size=0";
        for(var i=0; i < fields.length; i++){
            url += "&fields="+fields[i];
        }
        if(communityQuery!= null && communityQuery != ''  ) {
            url += communityQuery;
        }
        return this.http.get((properties.useCache)? (properties.cacheUrl+encodeURIComponent(url)): url)
                    //.map(res => <any> res.json())

                    .pipe(map(res =>   [res['meta'].total, RefineResultsUtils.parse(res['refineResults'],fields, entityName)]));

     }

  getAllRefineFieldResultsByFieldName(fieldName:string, entityName:string, properties:EnvProperties, refineQuery:string=null):any{
    // let keys:string[]=["funder", "relfunder", "fundinglevel"];
    let url = properties.searchAPIURLLAst +this.getSearchAPIURLForEntity(entityName)+"?fields="+fieldName +('&sf='+fieldName)+ "&format=json&size=0";
    if(refineQuery!= null && refineQuery != ''  ) {
      url += refineQuery;
    }
    return this.http.get((properties.useCache)? (properties.cacheUrl+encodeURIComponent(url)): url)
      //.map(res => <any> res.json())

      .pipe(map(res =>   [res['meta'].total, RefineResultsUtils.parse(res['refineResults'],[fieldName], entityName)]));
     }

    getRefineFieldResultsByFieldName(fieldName:string, entityName:string, properties:EnvProperties):any{
       let keys:string[]=["funder", "fundinglevel"]; // this covers funder, relfunder, funding stream fields and funding level fields
       let link = properties.searchAPIURLLAst +this.getSearchAPIURLForEntity(entityName)+"?fields="+fieldName +(entityName == "service" ? "&type=services" : "")+(this.fieldIncludesAnyOfTheKeywords(fieldName, keys)?('&sf='+fieldName):'')+ "&format=json";
       return  this.getField(link,fieldName, properties);
    }

    fieldIncludesAnyOfTheKeywords(field: string, keywords: string[]) {
       for(let keyword of keywords) {
         if(field.toString().indexOf(keyword)!=-1) {
           return true;
         }
       }
       return false;
    }

    getField (link:string,fieldName:string, properties:EnvProperties):any{
      let url = link+"&refine=true&page=1&size=0";

      // return this.http.get((properties.useCache)? (properties.cacheUrl+encodeURIComponent(url)): url)
      return this.http.get((properties.useLongCache && link.includes("sf=") && !link.includes("fq="))? (properties.cacheUrl+encodeURIComponent(url)): url)
                  //.map(res => <any> res.json())
                  .pipe(map(res => res['refineResults']))
                  .pipe(map(res =>  this.parse(res,fieldName)));

    }
    parse(data: any,fieldName:string):any {
      var values:AutoCompleteValue[] = [];
      if(data){
          let field = data[fieldName];
          for(let i=0; i<field.length; i++) {
            var value:AutoCompleteValue = new AutoCompleteValue();
            value.label = field[i].name;
            value.label = RefineResultsUtils.inParenthesisThePartAfterCharacters(field[i],"||");
            value.id = field[i].id;
            values.push(value);

        }
    }

    return values;
    }
    getSearchAPIURLForEntity(entityType:string):string{
      var suffix = "";
      if(entityType == "project"){
        suffix="projects/";
      }else if(entityType == "publication"){
        suffix="publications/";
      }else if(entityType == "dataset"){
        suffix="datasets/";
      } else if(entityType == "software"){
        suffix="software/";
      } else if(entityType == "other"){
        suffix="other/";
      }else if(entityType == "organization"){
        suffix="organizations/";
      }else if(entityType == "dataprovider") {
        suffix = "datasources/";
      }else if(entityType == "service") {
        suffix = "resources2/";
      }else if(entityType == "person"){
        suffix="people/";
      }else if(entityType == "result"){
        suffix="results/";
      }
      return  suffix;
    }
    private handleError (error: HttpErrorResponse) {
      // in a real world app, we may send the error to some remote logging infrastructure
      // instead of just logging it to the console
      console.log(error);
      return throwError(error  || 'Server error');
    }
}
