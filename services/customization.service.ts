import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {CustomOptions} from './servicesUtils/customOptions.class';
import {CustomizationOptions, Layout} from '../connect/community/CustomizationOptions';
import {EnvProperties} from "../utils/properties/env-properties";

@Injectable()
export class CustomizationService {
    constructor(private http: HttpClient) {
    }

    static removeNulls(obj) {
        const isArray = obj instanceof Array;
        for (let k in obj) {
            if (obj[k] === null || obj[k] === '') {
                isArray ? obj.splice(Number.parseInt(k), 1) : delete obj[k];
            } else if (typeof obj[k] === 'object') {
                CustomizationService.removeNulls(obj[k]);
            }
        }
    }
    createCSS( pid: string, connectPortalUrl:string, suffix = null, layout = null): Observable<any> {
        return this.http.post(connectPortalUrl + "/build-css/" + (layout?"preview/":"") + pid + (suffix?("/"+ suffix):""), layout );
    }

    saveLayout(properties: EnvProperties, pid: string, layout: Layout, portalType = null): Observable<Layout> {
        CustomizationService.removeNulls(layout);
        return this.http.post<Layout>(properties.adminToolsAPIURL + (portalType?portalType: properties.adminToolsPortalType) + '/'
          + pid + '/layout', layout, CustomOptions.getAuthOptionsWithBody());
    }
    deleteLayout(properties: EnvProperties, pid: string,  portalType = null): Observable<Layout> {
        return this.http.delete<Layout>(properties.adminToolsAPIURL + (portalType?portalType: properties.adminToolsPortalType) + '/'
          + pid + '/layout',  CustomOptions.getAuthOptionsWithBody());
    }
    getLayout(properties: EnvProperties, pid: string): Observable<Layout> {
        return this.http.get<Layout>(properties.adminToolsAPIURL + properties.adminToolsPortalType + '/'
          + pid + '/layout');
    }
    getLayouts(properties: EnvProperties): Observable<Layout[]> {
        return this.http.get<Layout[]>(properties.adminToolsAPIURL + 'community/layouts');
    }
    mockLayout(): any {
        return this.http.get('./assets/customizationOptions.json') ;

    }

    private handleError(error: HttpErrorResponse) {
        // in a real world app, we may send the error to some remote logging infrastructure
        // instead of just logging it to the console
        console.error(error);
        return throwError(error.error || 'Server error');
    }
}
