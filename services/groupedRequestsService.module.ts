import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';
import {GroupedRequestsService} from "./groupedRequests.service";

@NgModule({
  imports: [
    CommonModule, FormsModule
  ],
  declarations: [
  ],
  providers: [
    GroupedRequestsService
  ],
  exports: [
  ]
})

export class GroupedRequestsServiceModule { }
