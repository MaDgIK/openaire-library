import {Inject, Injectable, PLATFORM_ID} from "@angular/core";
import {BehaviorSubject, Observable} from "rxjs";
import {isPlatformBrowser} from "@angular/common";

@Injectable({
  providedIn: "root"
})
export class LocalStorageService {

  private communityDirectLink: BehaviorSubject<boolean>;

  constructor(@Inject(PLATFORM_ID) private platformId: string) {
    if(isPlatformBrowser(this.platformId)) {
      let item = localStorage.getItem('directLink');
      if(item !== null) {
        this.communityDirectLink = new BehaviorSubject<boolean>(item == 'true');
      } else {
        this.communityDirectLink = new BehaviorSubject<boolean>(false);
      }
    } else this.communityDirectLink = new BehaviorSubject<boolean>(true);
  }

  public setCommunityDirectLink(value: string) {
    this.communityDirectLink.next(Boolean(value).valueOf());
    localStorage.setItem('directLink', value);
  }

  public get(): Observable<boolean> {
    return this.communityDirectLink.asObservable();
  }
}
