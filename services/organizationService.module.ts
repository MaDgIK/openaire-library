import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';

import {OrganizationService} from './organization.service';


@NgModule({
  imports: [
    CommonModule, FormsModule
  ],
  declarations: [
  ],
  providers:[
  OrganizationService
],
  exports: [
    ]
})
export class OrganizationServiceModule { }
