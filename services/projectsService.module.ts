import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';

import {SearchProjectsService} from './searchProjects.service';


@NgModule({
  imports: [
    CommonModule, FormsModule
  ],
  declarations: [
  ],
  providers:[
  SearchProjectsService
],
  exports: [
    ]
})
export class ProjectsServiceModule { }
