import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";



import {SearchResult}     from '../utils/entities/searchResult';
import {RefineResultsUtils} from './servicesUtils/refineResults.class';
import{EnvProperties} from '../utils/properties/env-properties';
import {StringUtils} from '../utils/string-utils.class';
import {map} from "rxjs/operators";
import {ParsingFunctions} from "../landingPages/landing-utils/parsingFunctions.class";
@Injectable()
export class SearchProjectsService {
    private sizeOfDescription: number = 270;
    public parsingFunctions: ParsingFunctions = new ParsingFunctions();

  constructor(private http: HttpClient ) {}

    searchProjects (params: string, refineParams:string, page: number, size: number, refineFields:string[] , properties:EnvProperties ):any {

        let link = properties.searchAPIURLLAst+"projects";

        let url = link+"?";
        if(params!= null && params != ''  ) {
            url += params;
        }
        if(refineParams!= null && refineParams != ''  ) {
            url += refineParams;
        }
        url += "&page="+(page-1)+"&size="+size + "&format=json";
        return this.http.get((properties.useCache)? (properties.cacheUrl+encodeURIComponent(url)): url)
                    //.map(res => <any> res.json())
                    .pipe(map(res => [res['meta'].total, this.parseResults(res['results']),RefineResultsUtils.parse(res['refineResults'],refineFields, "project")]));
    }
    getProjectsforDataProvider (datasourceId: string, page: number, size: number, properties:EnvProperties  ):any {
      let url = properties.searchResourcesAPIURL;
      var basicQuery = "(oaftype exact project) ";
      var filterQuery = "&fq=(projectcode<>\"unidentified\")";
      url += "?query=";
      if(datasourceId!= null && datasourceId != ''  ) {
        url +=" ( "+basicQuery+ " ) " +" and (collectedfromdatasourceid exact \"" + datasourceId + "\" or resulthostingdatasourceid exact \""+ datasourceId + "\")";
      }else{
        url +=" ( "+basicQuery+ " ) ";
      }
      url += filterQuery;
      url += "&page="+(page-1)+"&size="+size;
      url += "&format=json";

      return this.http.get((properties.useCache)? (properties.cacheUrl+encodeURIComponent(url)): url)
      //.map(res => <any> res.json())
      .pipe(map(res => [res['meta'].total, this.parseResults(res['results'])]));
    }
    advancedSearchProjects (params: string, page: number, size: number, properties:EnvProperties, refineParams:string=null,  refineFields:string[] =null, refineQuery:string = null, minRef: boolean = false, excludeUnidentfied: boolean = true):any {
    // &type=projects
      let url = properties.searchAPIURLLAst+"resources2/?format=json";
      // var basicQuery = "(oaftype exact project) "
      var basicQuery = excludeUnidentfied ? "&fq=(projectcode<>\"unidentified\")" : "";
      // url += "?query=";
      if(params!= null && params != ''  ) {
        url +="&query=(" + params + ")";
      }
      // else{
      //   url +=" ( "+basicQuery+ " ) ";
      // }
      if(refineParams!= null && refineParams != ''  ) {
        url += refineParams;
      }
      if(refineQuery) {
        url += "&" + refineQuery;
      }
      // url += "&fq="+basicQuery;
      url += basicQuery;
      url += "&page="+(page-1)+"&size="+size;
      url += minRef ? "&minRef=true" : "";

      // url += "&format=json";
      // return this.http.get((properties.useCache)? (properties.cacheUrl+encodeURIComponent(url)): url)
      return this.http.get((properties.useLongCache && size == 0 && !params && (!refineQuery || !refineQuery.includes("fq="))) ? (properties.cacheUrl + encodeURIComponent(url)) : url)
        //.map(res => <any> res.json())
      .pipe(map(res => [res['meta'].total, this.parseResults(res['results']), RefineResultsUtils.parse(res['refineResults'],refineFields, "project")]));
    }
    
    getProjectsForOrganizations (organizationId: string, filterquery: string, page: number, size: number, refineFields:string[] , properties:EnvProperties  ):any {
      let url = properties.searchResourcesAPIURL;
      var basicQuery = "(oaftype exact project) "
      url += "?query=";
      // if(filterquery!= null && filterquery != ''  ) {
      //   url +="( ( "+basicQuery+ " )  and (relorganizationid exact \"" + organizationId + "\")"+"  " + filterquery + ")";
      // }else{
        url +=" (( "+basicQuery+ " ) " +" and (relorganizationid exact \"" + organizationId + "\"))";
      // }
      if(filterquery) {
        url += "&fq=" + filterquery;
      }
      url += "&fq=(projectcode<>\"unidentified\")";
      if(refineFields!= null && refineFields.length > 0 ) {
        url +="&refine=true";
        for(let i=0; i< refineFields.length ; i++ ){
          url +="&fields="+refineFields[i];
        }
      }
      url += "&page="+(page-1)+"&size="+size;
      url += "&format=json";

      return this.http.get((properties.useCache)? (properties.cacheUrl+encodeURIComponent(url)): url)
                  //.map(res => <any> res.json())
                  .pipe(map(res => [res['meta'].total, this.parseResults(res['results']),RefineResultsUtils.parse(res['refineResults'],refineFields, "project")]));
    }
    getFunders(params: string, properties:EnvProperties ):any {
       let url = properties.searchAPIURLLAst+"projects?refine=true&fields=funder"+(params ? "&"+params : "")+"&size=0"+ "&format=json";

       return this.http.get((properties.useCache)? (properties.cacheUrl+encodeURIComponent(url)): url)
                   //.map(res => <any> res.json())
                   .pipe(map(res => [res['meta'].total, RefineResultsUtils.parse(res['refineResults'], ['funder'], 'project')]));


      }

      searchForProjectsObs(keyword:string, funderId:string, properties:EnvProperties ):any {
         let url = 'search?action=search&sTransformer=projects_openaire&query='+
         '%28oaftype+exact+project%29+and+%28%28projecttitle+%3D+%22'+keyword+'%22%29+or+%28projectacronym+%3D+%22'+keyword+'%22%29+or+%28projectcode+%3D+%22'+keyword+'%22%29%29+and+%28funder+exact+'+funderId+'%29&size=10&locale=en_GB&format=json';

         return this.http.get((properties.useCache)? (properties.cacheUrl+encodeURIComponent(url)): url).toPromise()
                    .then(request =>{
                      //return (request.json().response.results)?request.json().response.results.result:request.json().response.result;
                      return (request['response'].results)?request['response'].results.result:request['response'].result;

                    }) ;
        }
    parseResults(data: any): SearchResult[] {
        let results: SearchResult[] = [];

        let length = Array.isArray(data) ? data.length : 1;

        for(let i=0; i<length; i++) {
            let resData = Array.isArray(data) ? data[i]['result']['metadata']['oaf:entity']['oaf:project'] : data['result']['metadata']['oaf:entity']['oaf:project'];

            var result: SearchResult = new SearchResult();
            result.openAccessMandatePublications = (resData['oamandatepublications'])?true:false;
            if(resData.hasOwnProperty("oamandatedata")) {
              result.openAccessMandateDatasets = (resData['oamandatedata'])?true:false;
            } else {
              result.openAccessMandateDatasets = (resData['ecarticle29_3'])?true:false;
            }
            result['title'] = {"name": '', "accessMode": ''};
            // if(resData['acronym'] != undefined && resData['acronym'] != "") {
            //     result['title'].name = resData['acronym'] + " - ";
            // }
          if(resData['title']) {
            if (Array.isArray(resData['title'])) {
              result['title'].name += StringUtils.HTMLToString(String(resData['title'][0]));
            } else {
              result['title'].name += StringUtils.HTMLToString(String(resData['title']));
            }
          } else {
            result['title'].name = "";
          }

            // if(result['title'].name != '') {
            //     result['title'].name += " ("+resData['code']+")"
            // }

            // if(resData['ecsc39'] == true) {
            //     result['title'].sc39 = "sc39";
            // }

            result.acronym = resData['acronym'];
            result.code = resData['code'];

            result.budget = resData.totalcost;//"10000";
            result.contribution = resData.fundedamount;//"200100";
            result.currency = resData.currency;//"EUR";

            result.id = Array.isArray(data) ? data[i]['result']['header']['dri:objIdentifier'] : data['result']['header']['dri:objIdentifier'];

            let abstracts = this.parsingFunctions.parseDescription(resData.summary, true);
            result.description = abstracts;
            // if (result.description && result.description.length > this.sizeOfDescription) {
            //   result.description = result.description.substring(0, this.sizeOfDescription) + "...";
            // }

            if(resData['rels'].hasOwnProperty("rel")) {
                let relLength = Array.isArray(resData['rels']['rel']) ? resData['rels']['rel'].length : 1;

                for(let j=0; j<relLength; j++) {
                    let relation = Array.isArray(resData['rels']['rel']) ? resData['rels']['rel'][j] : resData['rels']['rel'];

                    if(relation.hasOwnProperty("to")) {
                         if(relation['to'].class && relation['to'].class.toLowerCase() == "hasparticipant") {
                            if(result['organizations'] == undefined) {
                                result['organizations'] = new Array<
                                    { "name": string, "id": string}>();
                            }

                            let countOrganizations = result['organizations'].length;

                            result['organizations'][countOrganizations] = { "name": "", "id": "" }

                            result['organizations'][countOrganizations]['id'] =
                                /*OpenaireProperties.getsearchLinkToOrganization() + */relation['to'].content;
                            if(relation.legalshortname) {
                              result['organizations'][countOrganizations]['name'] = relation.legalshortname;
                            } else {
                              result['organizations'][countOrganizations]['name'] = relation.legalname;
                            }
                            if(!result['organizations'][countOrganizations]['name']) {
                              result['organizations'][countOrganizations]['name'] = "[no title available]";
                            }
                        }
                    }
                }
            }
            if(resData.hasOwnProperty("fundingtree")) {
              /*let funderSet: Set<string>;
              if(result['funders'] == undefined) {
                  result['funders'] = new Array<
                      {"funderShortname": string, "funderName": string}>();
                  funderSet = new Set<string>();
              }

                let fundingLength = Array.isArray(resData['fundingtree']) ? resData['fundingtree'].length : 1;

                for(let z=0; z<fundingLength; z++) {
                    let fundingData = Array.isArray(resData['fundingtree']) ? resData['fundingtree'][z] : resData['fundingtree'];
                    if(fundingData.hasOwnProperty("funder")) {
                        if(!funderSet.has(fundingData['funder'].shortname)) {
                          let countFunders = result['funders'].length;

                          result['funders'][countFunders] =   {"funderShortname": "", "funderName": ""};
                          result['funders'][countFunders]['funderShortname'] = fundingData['funder'].shortname;
                          result['funders'][countFunders]['funderName'] = fundingData['funder'].name;
                          funderSet.add(fundingData['funder'].shortname);
                        }
                    }
                }*/
                let fundingLength = Array.isArray(resData['fundingtree']) ? resData['fundingtree'].length : 1;

                for(let z=0; z<fundingLength; z++) {
                  let fundingData = Array.isArray(resData['fundingtree']) ? resData['fundingtree'][z] : resData['fundingtree'];
                  if(fundingData.hasOwnProperty("funder")) {
                    result['funderShortname'] = fundingData['funder'].shortname;
                    result['funderName'] = fundingData['funder'].name;
                    result['funderId'] = fundingData['funder'].id;
                    result['jurisdiction'] = (fundingData['funder']['id']['jurisdiction'] )?fundingData['funder']['id']['jurisdiction']:"";

                    if(fundingData['funding_level_2']){
                      result['fundingLevel0'] = (fundingData['funding_level_2'] && fundingData['funding_level_2']['parent'] &&
                       fundingData['funding_level_2']['parent']['funding_level_1'] && fundingData['funding_level_2']['parent']['funding_level_1']['parent']
                     && fundingData['funding_level_2']['parent']['funding_level_1']['parent']['funding_level_0'])?
                      fundingData['funding_level_2']['parent']['funding_level_1']['parent']['funding_level_0']['name']:"";
                    }else if(fundingData['funding_level_1']){
                      result['fundingLevel0'] = (fundingData['funding_level_1'] && fundingData['funding_level_1']['parent'] && fundingData['funding_level_1']['parent']['funding_level_0'])?
                      fundingData['funding_level_1']['parent']['funding_level_0']['name']:"";
                    }else if(fundingData['funding_level_0']){
                      result['fundingLevel0'] = (fundingData['funding_level_0']  )?fundingData['funding_level_0']['name']:"";
                    }else {
                      result['fundingLevel0']="";
                    }
                    break;
                  }
                }
            }
            if(resData.hasOwnProperty("startdate")  && resData['startdate']) {
              result.startYear = resData.startdate.split('-')[0];
            }
            if(resData.hasOwnProperty("enddate") && resData['enddate']) {
              result.endYear = resData.enddate.split('-')[0];
            }
            // Measure
            result.measure = this.parsingFunctions.parseMeasures(resData['measure']);

            results.push(result);
        }

        return results;
    }

    numOfProjects(url: string, properties:EnvProperties ):any {

      return this.http.get((properties.useCache)? (properties.cacheUrl+encodeURIComponent(url)): url)
                  //.map(res => <any> res.json())
                  .pipe(map(res => res['total']));
    }

    numOfEntityProjects(id: string, entity: string, properties:EnvProperties ):any {
        //let url = properties.searchAPIURLLAst+params+(params.indexOf("?") == -1 ?"?":"&")+"format=json";
        var parameters: string = "";
        if(entity == "organization") {
          parameters = "organizations/"+id+"/projects/count";
        }

        let url = properties.searchAPIURLLAst+parameters+ "?format=json";

        return this.numOfProjects(url, properties);
    }

    numOfSearchProjects(params: string, properties:EnvProperties, refineParams:string=null ):any {
        let url = properties.searchAPIURLLAst+"projects/count?format=json";
        if(params != "") {
          url += "&q=" + StringUtils.URIEncode(params);
        }
      if(refineParams!= null && refineParams != ''  ) {
        url += refineParams;
      }
        return this.numOfProjects(url, properties);
    }
    numOfSearchProjects2(params: string, properties:EnvProperties, refineParams:string=null ):any {
      let url = properties.searchAPIURLLAst+"resources2/?format=json&size=0&type=projects";
      if(params != "") {
        url += "&query=" + params;
      }
      if(refineParams!= null && refineParams != ''  ) {
        url += refineParams;
      }
      url += "&fq=(projectcode<>\"unidentified\")";

      return this.http.get((properties.useCache)? (properties.cacheUrl+encodeURIComponent(url)): url)
        .pipe(map(res => res['meta']['total']));
    }
}
