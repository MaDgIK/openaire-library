import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';

import {RefineFieldResultsService} from './refineFieldResults.service';


@NgModule({
  imports: [
    CommonModule, FormsModule
  ],
  declarations: [
  ],
  providers:[
  RefineFieldResultsService
],
  exports: [
    ]
})
export class RefineFieldResultsServiceModule { }
