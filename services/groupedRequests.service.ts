import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";
import {properties} from "../../../environments/environment";

@Injectable()
export class GroupedRequestsService {
  constructor(private http: HttpClient = null) {}

  home(): any {
    let url = properties.utilsService+"/explore/home";

    return this.http.get((properties.useLongCache) ? (properties.cacheUrl + encodeURIComponent(url)) : url)
      .pipe(map(res => res));
  }

  search(): any {
    let url = properties.utilsService+"/explore/search";

    return this.http.get((properties.useLongCache) ? (properties.cacheUrl + encodeURIComponent(url)) : url)
      .pipe(map(res => res));
  }

  funders(): any {
    let url = properties.utilsService+"/explore/funders";

    return this.http.get((properties.useLongCache) ? (properties.cacheUrl + encodeURIComponent(url)) : url)
      .pipe(map(res => res));
  }
}