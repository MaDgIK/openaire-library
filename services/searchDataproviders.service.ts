import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {SearchResult}     from '../utils/entities/searchResult';
import {RefineResultsUtils} from './servicesUtils/refineResults.class';
import {StringUtils} from '../utils/string-utils.class';
import{EnvProperties} from '../utils/properties/env-properties';
import {map} from "rxjs/operators";
import {ParsingFunctions} from "../landingPages/landing-utils/parsingFunctions.class";
import {properties} from '../../../environments/environment';

@Injectable()
export class SearchDataprovidersService {
    private sizeOfDescription: number = 270;
    public parsingFunctions: ParsingFunctions = new ParsingFunctions();

    constructor(private http: HttpClient ) {}

    searchDataproviders (params: string, refineParams:string, page: number, size: number, refineFields:string[], properties:EnvProperties, usedBy: string="search" ):any {
        let link = properties.searchAPIURLLAst+"datasources";

        let url = link+"?";
        if(params!= null && params != ''  ) {
            url += params;
        }
        if(refineParams!= null && refineParams != ''  ) {
            url += refineParams;
        }
        url += "&page="+(page-1)+"&size="+size+"&format=json";


        return this.http.get((properties.useCache)? (properties.cacheUrl+encodeURIComponent(url)): url)
                    .pipe(map(res => [res['meta'].total, this.parseResults(res['results']),RefineResultsUtils.parse(res['refineResults'],refineFields, "datasource", usedBy)]));
    }

    advancedSearchDataproviders (params: string, page: number, size: number, properties: EnvProperties, refineParams:string=null,  refineFields:string[] =null, refineQuery:string = null, depositQuery:boolean = false, minRef: boolean = false):any {
      let url =  properties.searchAPIURLLAst+"resources"+(depositQuery?'':2)+"/?format=json";

      if(params!= null && params != ''  ) {
        url +="&query=(" + params + ")";

      }
      if(refineParams!= null && refineParams != ''  ) {
        url += refineParams;
      }
      if(refineQuery) {
        url += "&" + refineQuery;
      }
      url += "&page="+(page-1)+"&size="+size;
      url += minRef ? "&minRef=true" : "";

      // return this.http.get((properties.useCache)? (properties.cacheUrl+encodeURIComponent(url)): url)
      return this.http.get((properties.useLongCache && size == 0 && !params && (!refineQuery || !refineQuery.includes("fq="))) ? (properties.cacheUrl + encodeURIComponent(url)) : url)
        .pipe(map(res => [res['meta'].total, this.parseResults(res['results']), RefineResultsUtils.parse(res['refineResults'],refineFields, "datasource")]));
    }

    searchDataprovidersForDeposit (id: string,type:string, page: number, size: number, properties:EnvProperties):any {
        let link = properties.searchResourcesAPIURL;
        var compatibilities = "";
        if(type == "Research Data"){
          //compatibilities = " and (datasourcecompatibilityid <> UNKNOWN) and (datasourcecompatibilityid = openaire2.0_data)"
          compatibilities = " and (datasourcecompatibilityid = openaire2.0_data)";
        }else if(type == "Publications"){
          //compatibilities = " and (datasourcecompatibilityid <> UNKNOWN) and (datasourcecompatibilityid <> openaire2.0_data)"
          compatibilities = " and (datasourcecompatibilityid <> openaire2.0_data)";
        }
        let url = link+"?query=(((deletedbyinference = false) AND (oaftype exact datasource)) "+((compatibilities && compatibilities.length > 0)?" "+compatibilities+" ":"")+") and (relorganizationid exact \""+id+"\")";
        url += "&page="+(page-1)+"&size="+size+"&format=json";


        return this.http.get((properties.useCache)? (properties.cacheUrl+encodeURIComponent(url)): url)
                    //.map(res => <any> res.json())
                    .pipe(map(res => [res['meta'].total, this.parseResults(res['results'])]));
    }

    getDataProvidersforEntityRegistry(datasourceId: string, page: number, size: number , properties:EnvProperties):any {
      let url = properties.searchResourcesAPIURL;
      var basicQuery = "(oaftype exact datasource) "
      url += "?query=";
      if(datasourceId!= null && datasourceId != ''  ) {
        url +=" ( "+basicQuery+ " ) " +" and (collectedfromdatasourceid exact \"" + datasourceId + "\" or resulthostingdatasourceid exact \""+ datasourceId + "\")";
      }else{
        url +=" ( "+basicQuery+ " ) ";
      }

      url += "&page="+(page-1)+"&size="+size+"&format=json";

      return this.http.get((properties.useCache)? (properties.cacheUrl+encodeURIComponent(url)): url)
      //.map(res => <any> res.json())
      .pipe(map(res => [res['meta'].total, this.parseResults(res['results'])]));
    }
    searchDataprovidersForEntity (params: string, page: number, size: number, properties:EnvProperties):any {
        let link = properties.searchAPIURLLAst;
        let url = link+params+"/datasources?format=json";

        return this.http.get((properties.useCache)? (properties.cacheUrl+encodeURIComponent(url)): url)
                    //.map(res => <any> res.json())
                    .pipe(map(res => [res['meta'].total, this.parseResults(res['results'])]));
    }

    parseResults(data: any): SearchResult[] {
        let results: SearchResult[] = [];

        let length = Array.isArray(data) ? data.length : 1;

        for(let i=0; i<length; i++) {
            let resData = Array.isArray(data) ? data[i]['result']['metadata']['oaf:entity']['oaf:datasource'] : data['result']['metadata']['oaf:entity']['oaf:datasource'];

            var result: SearchResult = new SearchResult();

            result['title'] = {"name": '', "accessMode": ''};

            if(resData.officialname) {
              result['title'].name = StringUtils.HTMLToString(String(resData.officialname));
            }
            if(resData.englishname) {
              result['englishname'] = StringUtils.HTMLToString(String(resData.englishname));
            } else {
              result['englishname'] = "";
            }
            result['originalId'] = resData.originalId;
            //result['title'].url = OpenaireProperties.getsearchLinkToDataProvider();
            //result['title'].url += Array.isArray(data) ? data[i]['result']['header']['dri:objIdentifier'] : data['result']['header']['dri:objIdentifier'];
            result['id'] = Array.isArray(data) ? data[i]['result']['header']['dri:objIdentifier'] : data['result']['header']['dri:objIdentifier'];
            let canId = ParsingFunctions.parseRelCanonicalId(Array.isArray(data) ? data[i] : data, "datasource");
            if (canId) {
              result['id'] = canId;
            }
            result['relcanId'] = result['id'];

            result['type'] = this.getDataproviderType(resData);
            if (resData['eosctype']) {
              result.entityType = resData['eosctype'].classname == "Service" ? "service" : "dataprovider";
            }

            let abstracts = this.parsingFunctions.parseDescription(resData.description, true);
            result.description = abstracts;
            // if (result.description && result.description.length > this.sizeOfDescription) {
            //   result.description = result.description.substring(0, this.sizeOfDescription) + "...";
            // }

            let typeid: string = resData['datasourcetype'] && resData['datasourcetype'].classid;
            if(typeid != "entityregistry" && typeid != "entityregistry::projects" && typeid != "entityregistry::repositories") {

                if(resData.hasOwnProperty('accessinfopackage')) {
                    let OAIPMHURL: string;
                    if(Array.isArray(resData['accessinfopackage'])) {
                        OAIPMHURL = resData['accessinfopackage'][0];
                    } else {
                        OAIPMHURL = resData['accessinfopackage'];
                    }

                    if(OAIPMHURL != '' && OAIPMHURL != 'unknown') {
                        result['OAIPMHURL'] = OAIPMHURL;
                    }
                }
                let compatibility = this.getDataproviderCompatibility(resData);
                result['compatibility'] = compatibility.classname;
                if(compatibility.classid == "UNKNOWN") {
                  result['compatibilityUNKNOWN'] = true;
                }
            } else {
              result['compatibility'] = "";
            }

            result['websiteURL'] = resData.websiteurl;

            let res:[string[], {"name":string, "id":string}[]] = this.getDataproviderCountriesOrganizations(resData, true, true);
            result['organizations'] = res[1];
            result['countries'] = res[0];
            result['subjects'] = this.getDataproviderSubjects(resData);

            if(resData['pid']) {
              result.identifiers = this.parsingFunctions.parseIdentifiers(resData['pid']);
            }
            // Measure
            result.measure = this.parsingFunctions.parseMeasures(resData['measure']);

          results.push(result);
        }

        return results;
    }
    getDataproviderSubjects(resData: any): string [] {
      var subjects:string [] = [];

      let length = Array.isArray(resData['subjects']) ? resData['subjects'].length : 1;
      for(let i=0; i<length; i++) {
          let subject = Array.isArray(resData['subjects']) ? resData['subjects'][i] :resData['subjects'];
          if(subject && subject.content) {
            subjects.push(subject.content);
          }
      }
      return subjects;
    }
    getDataproviderType(resData: any): string {
        if(resData.hasOwnProperty('datasourcetype') && resData['datasourcetype'].hasOwnProperty("classname")) {
            return resData['datasourcetype'].classname;
        } else {
            return '';
        }
    }

    getDataproviderCompatibility(resData: any): {"classid": string, "classname": string} {
        if(resData.hasOwnProperty('openairecompatibility')) {
            return {"classid": resData['openairecompatibility'].classid, "classname": resData['openairecompatibility'].classname};
        } else {
            return {"classid": "", "classname": ""};
        }
    }

    getDataproviderCountriesOrganizations(resData: any, getCountries: boolean, getOrganizations: boolean): [string[], {"name": string, "id": string}[]] {
        let countries: string[] = [];
        let organizations: {"name": string, "id": string}[] = [];

        if(resData['rels'].hasOwnProperty("rel")) {
            let countriesSet: Set<string> = new Set<string>();

            let relLength = Array.isArray(resData['rels']['rel']) ? resData['rels']['rel'].length : 1;

            for(let i=0; i<relLength; i++) {
                let relation = Array.isArray(resData['rels']['rel']) ? resData['rels']['rel'][i] : resData['rels']['rel'];

                if(relation.hasOwnProperty("to")) {
                    if(relation['to'].class && relation['to'].class.toLowerCase() == "isprovidedby" && relation['to'].type == "organization") {
                        if(getOrganizations) {
                            let item: {"name":string, "id":string} = {"name": "", "id": ""};
                            //item['name'] = relation.legalname;
                            if(relation.legalshortname) {
                              item['name'] = relation.legalshortname;
                            } else {
                              item['name'] = relation.legalname;
                            }
                            if(!item['name']) {
                              item['name'] = "[no title available]";
                            }
                            item['id'] = /*OpenaireProperties.getsearchLinkToOrganization()+*/relation['to'].content;
                            organizations.push(item);
                        }

                        if(getCountries) {
                            if(relation.hasOwnProperty('country') &&
                               relation.country.hasOwnProperty('classname')) {
                                if(!countriesSet.has(relation.country.classname)) {
                                    countriesSet.add(relation.country.classname);
                                    countries.push(relation.country.classname);
                                }
                            }
                        }
                    }
                }
            }
        }
        return [countries, organizations];
    }

    numOfDataproviders(url: string, properties:EnvProperties):any {
        return this.http.get((properties.useCache)? (properties.cacheUrl+encodeURIComponent(url)): url)
                    .pipe(map(res => res['total']));
    }

    numOfEntityDataproviders(id: string, entity: string, properties:EnvProperties):any {
        var parameters = "";
        if(entity == "organization") {
          parameters = "organizations/"+id+"/datasources/count";
        }

        let url = properties.searchAPIURLLAst+parameters+"?format=json";
        return this.numOfDataproviders(url, properties);
    }

    numOfSearchDataproviders(params: string, properties:EnvProperties, refineQuery:string=null):any {
        let url: string = properties.searchAPIURLLAst+"datasources/count?format=json";
        if(params != "") {
          url += "&q=" + StringUtils.URIEncode(params);
        }
      if(refineQuery!= null && refineQuery != ''  ) {
        url += refineQuery;
      }

        return this.numOfDataproviders(url, properties);
    }
  numOfSearchDataproviders2(params: string, properties:EnvProperties, refineQuery:string=null, typePathParam: string="datasources"):any {
    let url: string = properties.searchAPIURLLAst+"resources2/?format=json&size=0&type="+typePathParam;
    if(params != "") {
      url += "&query=" +params;
    }
    if(refineQuery!= null && refineQuery != ''  ) {
      url += refineQuery;
    }
    return this.http.get((properties.useCache)? (properties.cacheUrl+encodeURIComponent(url)): url)
      .pipe(map(res => res['meta']['total']));
  }

  searchDataproviderById(id: string):any {
    let url = properties.searchAPIURLLAst + "datasources/" + id + '?format=json';

    return this.http.get((properties.useCache)? (properties.cacheUrl+encodeURIComponent(url)): url)
    .pipe(map(res => this.parseResults(res)));
  }
}
