import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {properties} from '../../../environments/environment';
import {CustomOptions} from './servicesUtils/customOptions.class';
import {map} from 'rxjs/operators';
import {Role} from "../login/utils/helper.class";

@Injectable({
  providedIn: 'root'
})
export class UserRegistryService {

  constructor(private http: HttpClient) {
  }
  
  public createRole(type: string, id: string): Observable<any[]> {
    return this.http.post<any>(properties.registryUrl + 'create/' + Role.GROUP + type + '/' + id, null,
      CustomOptions.registryOptions()).pipe(map((response: any) => response.response));
  }
  
  public getMembersCount(type: string, id: string): Observable<any> {
    let url = properties.registryUrl + type + '/' + id + '/members/count';
    return this.http.get((properties.useCache) ? (properties.cacheUrl + encodeURIComponent(url)) : url);
  }

  public subscribeTo(type: string, id: string): Observable<any> {
    return this.http.post(properties.registryUrl + 'subscribe/' + type + '/' + id,
      null, CustomOptions.registryOptions());
  }

  public unsubscribeFrom(type: string, id: string): Observable<any> {
    return this.http.post(properties.registryUrl + 'unsubscribe/' + type + '/' + id,
      null, CustomOptions.registryOptions());
  }

  public remove(type: string, id: string, email: string, role: "member" | "manager" = "manager"): Observable<any> {
    return this.http.delete<any>(properties.registryUrl +
        Role.GROUP + type + '/' + id + '/' + role + '/' + encodeURIComponent(email), CustomOptions.registryOptions());
  }
  
  public invite(type: string, id: string, details: any, role: "member" | "manager" = "manager"): Observable<any[]> {
    return this.http.post<any>(properties.registryUrl + 'invite/' +
        Role.GROUP + type + '/' + id + '/' + role, details,
      CustomOptions.registryOptions()).pipe(map((response: any) => response.response));
  }
  
  public verify(id: string, code: string, role: "member" | "manager" = "manager"): Observable<any> {
    return this.http.post<any>(properties.registryUrl + 'verification/' + role + '/' + id, code, CustomOptions.registryOptions());
  }

  public getInvitation(id: string): Observable<any> {
    return this.http.get<any>(properties.registryUrl + 'verification/' + id, CustomOptions.registryOptions())
      .pipe(map((response: any) => response.response));
  }

  public deleteVerification(id: string): Observable<any> {
    return this.http.delete<any>(properties.registryUrl + 'verification/' + id, CustomOptions.registryOptions());
  }

  public getActive(type: string, id: string, role: "member" | "manager" = "manager", admin = false): Observable<any[]> {
    let url = properties.registryUrl + Role.GROUP + type + '/' + id + "/" + role + 's';
    return this.http.get<any>((properties.useCache && !admin) ? (properties.cacheUrl + encodeURIComponent(url)) : url,
      CustomOptions.registryOptions()).pipe(map((response:any) => response.response), map(users => {
        if(users.length > 0 && !users[0].email) {
          return [];
        } else {
          return users;
        }
    }));
  }

  public getPending(type: string, id: string, role: "member" | "manager" = "manager", admin = false): Observable<any[]> {
    let url = properties.registryUrl + 'invite/' + Role.GROUP + type + '/' +id + "/" + role + 's';
    return this.http.get<any>((properties.useCache && !admin) ? (properties.cacheUrl + encodeURIComponent(url)) : url,
      CustomOptions.registryOptions()).pipe(map((response: any) => response.response));
  }
  
  public cancelInvitation(type: string, id: string, email: string, role: "member" | "manager" = "manager"): Observable<any> {
    return this.http.delete<any>(properties.registryUrl + 'invite/' +
        Role.GROUP + type + '/' + id + '/' + role + '/' + encodeURIComponent(email),
      CustomOptions.registryOptions());
  }
}
