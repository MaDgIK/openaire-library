import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router, RouterStateSnapshot, UrlSegment, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';

import {properties} from "../../../environments/environment";

@Injectable({
	providedIn: 'root'
})
export class isDevelopmentGuard {
	constructor(private router: Router) {
	}
	
	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree{
		if(properties.environment == 'development') {
			return true;
		} else {
			this.router.navigate([properties.errorLink]);
		}
	}
}
