import {Notification} from "./notifications";
import {HelperFunctions} from "../utils/HelperFunctions.class";
import {Composer} from "../utils/email/composer";

export class NotificationUtils {
  public static IMPORT_INDICATORS: Notification = new Notification('IMPORT', ['monitor'], 'User ((__user__)) has imported new indicators in ((__stakeholder__)) profile.', 'section')
  public static CREATE_STAKEHOLDER: Notification = new Notification('CREATE', ['monitor'], 'User ((__user__)) has created a new profile.', 'stakeholder')
  public static EDIT_STAKEHOLDER: Notification = new Notification('EDIT', ['monitor'], 'User ((__user__)) has updated ((__stakeholder__)) profile.', 'stakeholder')
  public static CREATE_INDICATOR: Notification = new Notification('CREATE', ['monitor'], 'User ((__user__)) has created a new indicator in ((__stakeholder__)) profile.', 'indicator');
  public static EDIT_INDICATOR: Notification = new Notification('EDIT', ['monitor'], 'User ((__user__)) has updated an indicator in ((__stakeholder__)) profile.', 'indicator');
  public static DELETE_INDICATOR: Notification = new Notification('DELETE', ['monitor'], 'User ((__user__)) has deleted an indicator in ((__stakeholder__)) profile.', 'indicator');
  public static INVITE_MONITOR_MANAGER: Notification = new Notification('INVITE_MANAGER', ['monitor'], null, 'user');
  public static INVITE_MONITOR_MEMBER: Notification = new Notification('INVITE_MEMBER', ['monitor'], null, 'user');
  
  public static importIndicators(user: string, stakeholder: string): Notification {
    let notification: Notification = HelperFunctions.copy(this.IMPORT_INDICATORS);
    notification.message = notification.message.replace('((__user__))', user);
    notification.message = notification.message.replace('((__stakeholder__))', stakeholder);
    return notification;
  }
  
  public static createStakeholder(user: string): Notification {
    let notification: Notification = HelperFunctions.copy(this.CREATE_STAKEHOLDER);
    notification.message = notification.message.replace('((__user__))', user);
    return notification;
  }
  
  public static editStakeholder(user: string, stakeholder: string): Notification {
    let notification: Notification = HelperFunctions.copy(this.EDIT_STAKEHOLDER);
    notification.message = notification.message.replace('((__user__))', user);
    notification.message = notification.message.replace('((__stakeholder__))', stakeholder);
    return notification;
  }
  
  public static createIndicator(user: string, stakeholder: string): Notification {
    let notification: Notification = HelperFunctions.copy(this.CREATE_INDICATOR);
    notification.message = notification.message.replace('((__user__))', user);
    notification.message = notification.message.replace('((__stakeholder__))', stakeholder);
    return notification;
  }
  
  public static editIndicator(user: string, stakeholder: string): Notification {
    let notification: Notification = HelperFunctions.copy(this.EDIT_INDICATOR);
    notification.message = notification.message.replace('((__user__))', user);
    notification.message = notification.message.replace('((__stakeholder__))', stakeholder);
    return notification;
  }
  
  public static deleteIndicator(user: string, stakeholder: string): Notification {
    let notification: Notification = HelperFunctions.copy(this.DELETE_INDICATOR);
    notification.message = notification.message.replace('((__user__))', user);
    notification.message = notification.message.replace('((__stakeholder__))', stakeholder);
    return notification;
  }
  
  public static invite(name: string,role: "manager" | "member", user, invitation: string): Notification {
    let notification: Notification = HelperFunctions.copy(this.INVITE_MONITOR_MANAGER);
    if(role === "member") {
      notification = HelperFunctions.copy(this.INVITE_MONITOR_MEMBER);
    }
    notification.message = Composer.composeMessageForMonitorDashboard(name, role, user, invitation);
    return notification;
  }
}
