import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {SearchResearchResultsServiceModule} from '../services/searchResearchResultsService.module';
import {SearchResearchResultsComponent} from "./searchResearchResults.component";
import {NewSearchPageModule} from "./searchUtils/newSearchPage.module";

@NgModule({
  imports: [
    CommonModule, FormsModule,
    SearchResearchResultsServiceModule,
    NewSearchPageModule

  ],
  declarations: [
    SearchResearchResultsComponent
  ],
  providers:[],
  exports: [
    SearchResearchResultsComponent
  ]
})
export class SearchResearchResultsModule { }
