import {ChangeDetectorRef, Component, Input, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {Meta, Title} from '@angular/platform-browser';

import {FetchResearchResults} from '../../utils/fetchEntitiesClasses/fetchResearchResults.class';
import {FetchDataproviders} from '../../utils/fetchEntitiesClasses/fetchDataproviders.class';
import {FetchProjects} from '../../utils/fetchEntitiesClasses/fetchProjects.class';
import {FetchOrganizations} from '../../utils/fetchEntitiesClasses/fetchOrganizations.class';

import {SearchResearchResultsService} from '../../services/searchResearchResults.service';
import {SearchDataprovidersService} from '../../services/searchDataproviders.service';
import {SearchProjectsService} from '../../services/searchProjects.service';
import {SearchOrganizationsService} from '../../services/searchOrganizations.service';

import {OpenaireEntities, SearchFields} from '../../utils/properties/searchFields';
import {ErrorCodes} from '../../utils/properties/errorCodes';
import {RouterHelper} from '../../utils/routerHelper.class';
import {RefineFieldResultsService} from '../../services/refineFieldResults.service';
import {PiwikService} from '../../utils/piwik/piwik.service';
import {ConfigurationService} from '../../utils/configuration/configuration.service';
import {EnvProperties} from '../../utils/properties/env-properties';
import {SEOService} from '../../sharedComponents/SEO/SEO.service';
import {StringUtils} from '../../utils/string-utils.class';
import {SearchCustomFilter} from "../searchUtils/searchUtils.class";
import {combineLatest, Subscription} from "rxjs";
import {debounceTime, map} from "rxjs/operators";
import {AdvancedField, Filter} from "../searchUtils/searchHelperClasses.class";
import {SearchResearchResultsComponent} from "../searchResearchResults.component";
import {SearchProjectsComponent} from "../searchProjects.component";
import {SearchOrganizationsComponent} from "../searchOrganizations.component";
import {SearchDataProvidersComponent} from "../searchDataProviders.component";
import {NewSearchPageComponent, SearchForm} from "../searchUtils/newSearchPage.component";
import {properties} from "../../../../environments/environment";
import {Breadcrumb} from "../../utils/breadcrumbs/breadcrumbs.component";
import {LayoutService} from "../../dashboard/sharedComponents/sidebar/layout.service";
import {GroupedRequestsService} from "../../services/groupedRequests.service";

@Component({
  selector: 'search-all',
  templateUrl: 'searchAll.component.html'
})
export class SearchAllComponent {
  isMobile: boolean = false;

  reload:{result:boolean, projects:boolean, datasources: boolean, services: boolean, organizations:boolean, all: boolean} =
    {result:true, projects:true, datasources: true, services: true, organizations: true, all: true};
  allCounts = null;
  public pageTitle = "Search in OpenAIRE"
  public keyword: string = "";
  public publications: string[];
  public datasets: string[];
  public software: string[];
  public orps: string[];
  public projectsTab: string[];
  public dataproviders: string[];
  public organizations: string[];
  public activeEntity = null;
  public linkToSearchPublications = "";
  public linkToSearchProjects = "";
  public linkToSearchDataproviders = "";
  public linkToSearchOrganizations = "";
  public fetchPublications: FetchResearchResults;
  public fetchDataproviders: FetchDataproviders;
  public fetchServices: FetchDataproviders;
  public fetchProjects: FetchProjects;
  public fetchDatasets: FetchResearchResults;
  public fetchSoftware: FetchResearchResults;
  public fetchOrps: FetchResearchResults;
  public fetchOrganizations: FetchOrganizations;
  public searchFields: SearchFields = new SearchFields();
  public errorCodes: ErrorCodes = new ErrorCodes();
  public routerHelper: RouterHelper = new RouterHelper();
  public resultsName = OpenaireEntities.RESULTS;
  public projectName = OpenaireEntities.PROJECTS;
  public organizationsName = OpenaireEntities.ORGANIZATIONS;
  public dataSourcesName = OpenaireEntities.DATASOURCES;
  public servicesName = OpenaireEntities.SERVICES;
  showPublications: boolean = false;
  showDatasets: boolean = false;
  showSoftware: boolean = false;
  showOrps: boolean = false;
  showProjects: boolean = false;
  showDataProviders: boolean = false;
  showServices: boolean = false;
  showOrganizations: boolean = false;
  advancedSearchLink: string = properties.searchLinkToAdvancedResults;
  properties: EnvProperties = properties;
  offset: number;
  public openaireEntities = OpenaireEntities;
  @Input() logoURL;
  @Input() name;
  @Input() customFilter: SearchCustomFilter   = null;

  @Input() formPlaceholderText = "Search"
  // @Input() formPlaceholderText = "Search for "+OpenaireEntities.RESULTS+", "+OpenaireEntities.PROJECTS+", "+OpenaireEntities.DATASOURCES+" & "+OpenaireEntities.ORGANIZATIONS+" in OpenAIRE";
  @Input() searchForm: SearchForm = {class: 'search-form', dark: true}
  @Input() breadcrumbs: Breadcrumb[] = [];
  @Input() openAccess: boolean = true;

  subs: Subscription[] = [];

  quickFilter: { filter: Filter, selected: boolean, filterId: string, value: string } = null;//{
    // filter: null,
    // selected: true,
    // filterId: "resultbestaccessright",
    // value: "Open Access"
  // };

  resultTypes = {publication: true, dataset: true, software: true, other: true};

  //adv Search Form
  public fieldIds: string[] = this.searchFields.getResultAdvancedFields();
  public fieldIdsMap = this.searchFields.RESULT_FIELDS;
  public selectedFields: AdvancedField[] = [];

  //new
  parameters = {};
  disableForms: boolean = true;
  @ViewChild(SearchResearchResultsComponent) searchResearchResultsComponent : SearchResearchResultsComponent ;
  @ViewChild(SearchProjectsComponent) searchProjectsComponent : SearchProjectsComponent ;
  @ViewChild(SearchDataProvidersComponent) searchDataprovidersComponent : SearchDataProvidersComponent ;
  @ViewChild(SearchDataProvidersComponent) searchServicesComponent : SearchDataProvidersComponent ;
  @ViewChild(SearchOrganizationsComponent) searchOrganizationsComponent : SearchOrganizationsComponent ;
  constructor(private route: ActivatedRoute,
              private _router: Router,
              private _searchResearchResultsService: SearchResearchResultsService,
              private _searchDataprovidersService: SearchDataprovidersService,
              private _searchProjectsService: SearchProjectsService,
              private _searchOrganizationsService: SearchOrganizationsService,
              private _refineFieldResultsService: RefineFieldResultsService,
              private groupedRequestsService: GroupedRequestsService,
              private location: Location,
              private _meta: Meta,
              private _title: Title,
              private _piwikService: PiwikService,
              private config: ConfigurationService,
              private layoutService: LayoutService,
              private seoService: SEOService, private router: Router, private cdr:ChangeDetectorRef) {
    this.fetchPublications = new FetchResearchResults(this._searchResearchResultsService);
    this.fetchDataproviders = new FetchDataproviders(this._searchDataprovidersService);
    this.fetchServices = new FetchDataproviders(this._searchDataprovidersService);
    this.fetchProjects = new FetchProjects(this._searchProjectsService);
    this.fetchDatasets = new FetchResearchResults(this._searchResearchResultsService);
    this.fetchSoftware = new FetchResearchResults(this._searchResearchResultsService);
    this.fetchOrps = new FetchResearchResults(this._searchResearchResultsService);
    this.fetchOrganizations = new FetchOrganizations(this._searchOrganizationsService);
    this.searchFields.sortFieldsByName(this.fieldIds, this.fieldIdsMap);
    this.selectedFields.push(new AdvancedField(this.fieldIds[0], this.fieldIdsMap[this.fieldIds[0]].param, this.fieldIdsMap[this.fieldIds[0]].name, this.fieldIdsMap[this.fieldIds[0]].type, '', "and"));

  }

  public ngOnInit() {

        var description = "Search for "+OpenaireEntities.RESULTS+" ("+OpenaireEntities.PUBLICATIONS+", "+OpenaireEntities.DATASETS+", "+OpenaireEntities.SOFTWARE+", "+OpenaireEntities.OTHER+"), "+OpenaireEntities.PROJECTS+", "+OpenaireEntities.ORGANIZATIONS+", "+OpenaireEntities.DATASOURCES+" in the OpenAIRE Graph. ";
        var title = "OpenAIRE |Search for "+OpenaireEntities.RESULTS+", "+OpenaireEntities.PROJECTS+", "+OpenaireEntities.DATASOURCES+" & "+OpenaireEntities.ORGANIZATIONS;
        var url = this.properties.domain + this.properties.baseLink + this._router.url;
        this._title.setTitle(title);
        this._meta.updateTag({content: description}, "name='description'");
        this._meta.updateTag({content: description}, "property='og:description'");
        this._meta.updateTag({content: title}, "property='og:title'");
        this._meta.updateTag({content: url}, "property='og:url'");
        this.seoService.createLinkForCanonicalURL(this.properties.domain +this.properties.baseLink +  this._router.url, false);
        this.subs.push(this._piwikService.trackView(this.properties, title).subscribe());
        if ((this.customFilter && this.customFilter.queryFieldName == "communityId") || this.properties.adminToolsCommunity) {
          //this.config.getCommunityInformation(this.properties, (this.customFilter && this.customFilter.queryFieldName == "communityId") ? this.customFilter.valueId : this.properties.adminToolsCommunity).subscribe(data => {
          this.subs.push(this.config.portalAsObservable.subscribe(data => {
            if(data && data['entities']) {
              var showEntity = {};
              for (var i = 0; i < data['entities'].length; i++) {

                showEntity["" + data['entities'][i]["pid"] + ""] = data['entities'][i]["isEnabled"];
              }
              this.showPublications = showEntity["publication"];
              this.showDatasets = showEntity["dataset"];
              this.showProjects = showEntity["project"];
              this.showOrganizations = showEntity["organization"];
              this.showDataProviders = showEntity["datasource"];
              this.showServices = showEntity["service"];
              this.showSoftware = showEntity["software"];
              this.showOrps = showEntity["orp"];
              if (this.customFilter && this.customFilter.queryFieldName == "communityId") {
                this.showProjects = false;
                this.showOrganizations = false;
                this.showDataProviders = false;
                this.showServices = false;
              }
              this.loadAll();
            }
          }));
        } else {
          if ((this.customFilter && this.customFilter.queryFieldName == "country")) {
            this.showPublications = true;
            this.showDatasets = true;
            this.showSoftware = true;
            this.showOrps = true;
            this.showProjects = true;
            this.showOrganizations = true;
            this.showDataProviders = true;
            this.showServices = false;
          }
          this.loadAll();
        }

    this.subs.push(this.layoutService.isMobile.subscribe(isMobile => {
      this.isMobile = isMobile;

      if (typeof document !== 'undefined') {
        if(!this.isMobile && document.getElementById("main-menu")) {
          this.offset = Number.parseInt(getComputedStyle(document.documentElement).getPropertyValue('--header-height'));
          this.cdr.detectChanges();
        } else {
          this.offset = 0;
          this.cdr.detectChanges();
        }
      }
    }))
  }

  loadAll() {
    this.reloadTabs();
    // https://github.com/angular/angular/issues/26764
    this.subs.push(combineLatest([this.route.params, this.route.queryParams])
      .pipe(debounceTime(0))
      .pipe(map(results => ({params: results[0], query: results[1]})))
      .subscribe(results => {
        let params = results['params'];
        let queryParams = results['query'];

          this.parameters = Object.assign({}, queryParams);
          this.keyword = (queryParams['keyword']) ? queryParams['keyword'] : (queryParams["q"] ? queryParams["q"] : (queryParams["f0"] && queryParams["f0"] == "q" && queryParams["fv0"]?queryParams["fv0"]:""));
          this.selectedFields[0].value = StringUtils.URIDecode(this.keyword);
          // this.quickFilter.selected = ((queryParams['qf']== undefined || queryParams["qf"] == "true") == true);
          if (queryParams["type"] && queryParams["type"].length > 0) {
            this.resultTypes['publication'] = (queryParams["type"].split(",").indexOf("publications") != -1);
            this.resultTypes['dataset'] = (queryParams["type"].split(",").indexOf("datasets") != -1);
            this.resultTypes['software'] = (queryParams["type"].split(",").indexOf("software") != -1);
            this.resultTypes['other'] = (queryParams["type"].split(",").indexOf("other") != -1);
          }
          let active = null;
          if (queryParams["active"] && queryParams["active"].length > 0) {
            active = ((["result","projects","organizations","datasources","services"]).indexOf(queryParams["active"])!= -1)?queryParams["active"]:null;
            delete this.parameters['active'];
          }
        // if(this.activeEntity == null) {
          if (this.activeEntity == null && (!params["entity"] || params["entity"].length == 0) && (!active || this.activeEntity != active)) {
            if (active) {
              this.activeEntity = active;
              if ((typeof document !== 'undefined')) {
                if (active == "result") {
                  active = "research-outcomes";
                }
                let query = location.search.replace(/(active=)([^&]*)&?/, '');
                this.location.go(location.pathname + '/' + active, query == '?' ? '' : query);
              }
            } else {
              this.activeEntity = this.getDefaultEntityToShow();
            }
          } else if (params["entity"] && params["entity"].length > 0) {
            let entity = params["entity"];
            if (entity == "research-outcomes") {
              entity = "result";
            } else if (entity == "dataproviders") {
              entity = "datasources";
            }
            this.activeEntity = ((["result", "projects", "organizations", "datasources", "services"]).indexOf(entity) != -1) ? entity : this.getDefaultEntityToShow();
          }
        // }
        if (this.activeEntity == "result") {
          this.searchResults();
        } else if (this.activeEntity == "projects") {
          this.searchProjects();
        } else if (this.activeEntity == "datasources") {
          this.searchDataProviders();
        } else if (this.activeEntity == "services") {
          this.searchServices();
        } else if (this.activeEntity == "organizations") {
          this.searchOrganizations();
        }
        this.count();
      // }));
    }));
  }

  getDefaultEntityToShow(){
    if (this.showPublications || this.showDatasets || this.showSoftware || this.showOrps) {
      return "result";
    } else if (this.showProjects) {
      return "projects";
    } else if (this.showDataProviders) {
      return "datasources";
    } else if (this.showOrganizations) {
      return "organizations";
    }
  }

  public ngOnDestroy() {
    for (let sub of this.subs) {
      sub.unsubscribe();
    }
    this.fetchDatasets.clearSubscriptions();
    this.fetchPublications.clearSubscriptions();
    this.fetchSoftware.clearSubscriptions();
    this.fetchOrps.clearSubscriptions();
    this.fetchOrganizations.clearSubscriptions();
    this.fetchDataproviders.clearSubscriptions();
    this.fetchServices.clearSubscriptions();
    this.fetchProjects.clearSubscriptions();
  }

  public searchResults() {
    this.advancedSearchLink = this.properties.searchLinkToAdvancedResults;//"/search/advanced/research-outcomes";
    this.formPlaceholderText = "Search by title, author, abstract, DOI, orcid...";

    if (this.reload[this.activeEntity] &&
      this.fetchPublications.searchUtils.status != this.errorCodes.NONE) {
      this.reload[this.activeEntity] = false;
      this.linkToSearchPublications = this.properties.searchLinkToPublications;// + "?keyword="  + this.keyword;
    }
  }


  public searchProjects() {
    this.advancedSearchLink = this.properties.searchLinkToAdvancedProjects;//"/search/advanced/projects";
    this.formPlaceholderText = "Search by title, acronym, project code...";

    if (this.reload[this.activeEntity] &&
      this.fetchProjects.searchUtils.status != this.errorCodes.NONE ) {
      this.reload[this.activeEntity]  = false;
      this.linkToSearchProjects = this.properties.searchLinkToProjects;// + "?keyword="  + this.keyword;
    }
  }

  public searchDataProviders() {
    this.advancedSearchLink = this.properties.searchLinkToAdvancedDataProviders;//"/search/advanced/dataproviders";
    this.formPlaceholderText = "Search by name, description, subject...";

    if ( this.reload[this.activeEntity] &&
      this.fetchDataproviders.searchUtils.status != this.errorCodes.NONE) {
      this.reload[this.activeEntity] = false;
      this.linkToSearchDataproviders = this.properties.searchLinkToDataProviders;// + "?keyword="  + this.keyword;
    }
  }

  public searchServices() {
    this.advancedSearchLink = this.properties.searchLinkToAdvancedServices;//"/search/advanced/services";
    this.formPlaceholderText = "Search by name, description, subject...";

    if ( this.reload[this.activeEntity] &&
      this.fetchServices.searchUtils.status != this.errorCodes.NONE) {
      this.reload[this.activeEntity] = false;
      this.linkToSearchDataproviders = this.properties.searchLinkToServices;// + "?keyword="  + this.keyword;
    }
  }

  public searchOrganizations() {
    this.advancedSearchLink = this.properties.searchLinkToAdvancedOrganizations;//"/search/advanced/organizations";
    this.formPlaceholderText = "Search by organization name...";

    if (this.reload[this.activeEntity] &&
      this.fetchOrganizations.searchUtils.status != this.errorCodes.NONE) {
      this.reload[this.activeEntity] = false;
      this.linkToSearchOrganizations = this.properties.searchLinkToOrganizations;// + "?keyword="  + this.keyword;
    }
  }
  private prepareKeywordParam(keyword){
    if (this.parameters["q"]) {
      delete this.parameters['q'];
      delete this.parameters['op'];
    }
     if(keyword.length > 0){
      this.parameters["fv0"] = keyword;
      this.parameters["f0"] = "q";
    }else if(keyword.length ==0 && this.parameters["f0"]=="q"){
       delete this.parameters['f0'];
       delete this.parameters['fv0'];
     }
  }
  /*
  //quickSelection moved inside the searchpage
  private prepareResultParameters() {
    //quickSelections
    if (this.resultTypes && this.activeEntity == "result") {
      let values = [];
      if (this.resultTypes.publication) {
        values.push("publications");
      }
      if (this.resultTypes.dataset) {
        values.push("datasets");
      }
      if (this.resultTypes.software) {
        values.push("software");
      }
      if (this.resultTypes.other) {
        values.push("other");
      }
      if (values.length > 0) {
        this.parameters["type"] = values.join(",");
      }
    }
    if (this.quickFilter && this.activeEntity == "result") {
      this.parameters["qf"] = this.quickFilter.selected;
    }
  }

  public quickSelectionsChanged() {
    this.prepareResultParameters();
    this.parameters["page"] = 1;
    this.reload[this.activeEntity] = true;
    this.router.navigate([location.pathname], {queryParams: this.parameters});
  }*/


  public keywordChanged($event) {
    this.prepareKeywordParam(this.selectedFields[0].value);
    this.parameters["page"] = 1;
    this.reloadTabs();
    this.router.navigate([location.pathname], {queryParams: this.parameters});
  }

  private count() {
    var refineParams = null;
    let groupedQuery: boolean = false;
    let reload = JSON.parse(JSON.stringify(this.reload));
    if (this.customFilter) {
      refineParams = (refineParams ? (refineParams + '&') : '') + "&fq=" + StringUtils.URIEncode(this.customFilter.queryFieldName + " exact " + StringUtils.quote((this.customFilter.valueId)));
    } else if(!this.keyword || this.keyword.length == 0) {
      groupedQuery = true;
    }
    if (this.activeEntity != "result" && this.reload["result"] &&  (this.showPublications || this.showSoftware || this.datasets || this.showOrps)) {
      this.fetchPublications.searchUtils.status = this.errorCodes.LOADING;
      this.reload["result"] = false;
      this.fetchPublications.results = [];
      if(!groupedQuery) {
        //Add Open Access Filter
        this.subs.push(this.numOfSearchResults(this.fetchPublications, (refineParams ? (refineParams ) : '') ));
      }
    }

    if (this.activeEntity != "projects" && this.reload["projects"] && this.showProjects) {
      this.fetchProjects.searchUtils.status = this.errorCodes.LOADING;
      this.fetchProjects.results = [];
      this.reload["projects"] = false;
      if(!groupedQuery) {
        this.subs.push(this._searchProjectsService.numOfSearchProjects2(this.keyword.length > 0 ? NewSearchPageComponent.createKeywordQuery("project", this.keyword, "q", "=") : "", this.properties, refineParams).subscribe(
          data => {
            this.fetchProjects.searchUtils.totalResults = data;
            this.fetchProjects.searchUtils.status = this.errorCodes.DONE;
            if (this.fetchProjects.searchUtils.totalResults == 0) {
              this.fetchProjects.searchUtils.status = this.errorCodes.NONE;
            }
          },
          err => {
            //console.log(err);
            this.handleError("Error getting number of " + OpenaireEntities.PROJECTS, err);
            this.fetchProjects.searchUtils.status = this.errorCodes.ERROR;
            this.fetchProjects.searchUtils.totalResults = null;
          }
        ));
      }
    }
    if (this.activeEntity != "datasources"  && this.reload["datasources"] && this.showDataProviders) {
      this.fetchDataproviders.results = [];
      this.reload["datasources"] = false;
      if(!groupedQuery) {
        // this.fetchDataproviders.getNumForSearch(this.keyword, this.properties, refineParams);
        this.subs.push(this._searchDataprovidersService.numOfSearchDataproviders2(this.keyword.length > 0 ? NewSearchPageComponent.createKeywordQuery("dataprovider", this.keyword, "q", "=") : "", this.properties, refineParams).subscribe(
          data => {
            this.fetchDataproviders.searchUtils.totalResults = data;
            this.fetchDataproviders.searchUtils.status = this.errorCodes.DONE;
            if (this.fetchDataproviders.searchUtils.totalResults == 0) {
              this.fetchDataproviders.searchUtils.status = this.errorCodes.NONE;
            }
          },
          err => {
            //console.log(err);
            this.handleError("Error getting number of " + OpenaireEntities.DATASOURCES, err);
            this.fetchDataproviders.searchUtils.status = this.errorCodes.ERROR;
            this.fetchDataproviders.searchUtils.totalResults = null;
          }
        ));
      }
    }
    if (this.activeEntity != "services"  && this.reload["services"] && this.showServices) {
      this.fetchServices.results = [];
      this.reload["services"] = false;
      if(!groupedQuery) {
        this.subs.push(this._searchDataprovidersService.numOfSearchDataproviders2(this.keyword.length > 0 ? NewSearchPageComponent.createKeywordQuery("service", this.keyword, "q", "=") : "", this.properties, refineParams, "services").subscribe(
          data => {
            this.fetchServices.searchUtils.totalResults = data;
            this.fetchServices.searchUtils.status = this.errorCodes.DONE;
            if (this.fetchServices.searchUtils.totalResults == 0) {
              this.fetchServices.searchUtils.status = this.errorCodes.NONE;
            }
          },
          err => {
            //console.log(err);
            this.handleError("Error getting number of " + OpenaireEntities.SERVICES, err);
            this.fetchServices.searchUtils.status = this.errorCodes.ERROR;
            this.fetchServices.searchUtils.totalResults = null;
          }
        ));
      }
    }
    if (this.activeEntity != "organizations" && this.reload["organizations"] && this.showOrganizations) {
      this.fetchOrganizations.searchUtils.status = this.errorCodes.LOADING;
      this.fetchOrganizations.results = [];
      this.reload["organizations"] = false;
      if(!groupedQuery) {
        this.subs.push(this._searchOrganizationsService.numOfSearchOrganizations2(this.keyword.length > 0 ? NewSearchPageComponent.createKeywordQuery("organizations", this.keyword, "q", "=") : "", this.properties, refineParams).subscribe(
          data => {
            this.fetchOrganizations.searchUtils.totalResults = data;
            this.fetchOrganizations.searchUtils.status = this.errorCodes.DONE;
            if (this.fetchOrganizations.searchUtils.totalResults == 0) {
              this.fetchOrganizations.searchUtils.status = this.errorCodes.NONE;
            }
          },
          err => {
            //console.log(err);
            this.handleError("Error getting number of " + OpenaireEntities.ORGANIZATIONS, err);
            this.fetchOrganizations.searchUtils.status = this.errorCodes.ERROR;
            this.fetchOrganizations.searchUtils.totalResults = null;

          }
        ));
      }
    }

    if(groupedQuery && (reload["result"] || reload["datasources"] || reload["projects"] || reload["organizations"])) {
      if(this.reload["all"]) {
        this.reload["all"] = false;
        this.subs.push(this.groupedRequestsService.search().subscribe(
          data => {
            this.allCounts = data;
            this.setDefaultCounts();
          },
          err => {
            this.handleError("Error getting number of search entities", err);
            this.numErrorReturned(this.fetchPublications);
            this.numErrorReturned(this.fetchDataproviders);
            this.numErrorReturned(this.fetchOrganizations);
            this.numErrorReturned(this.fetchProjects);
          }
        ));
      } else {
        this.setDefaultCounts();
      }
    }
  }

  private numOfSearchResults(fetchClass: FetchResearchResults, refineParams): Subscription {
    return this._searchResearchResultsService.numOfResearchOutcomes(this.keyword.length>0?NewSearchPageComponent.createKeywordQuery("result",this.keyword,"q","="):"", this.properties, refineParams).subscribe(
      data => {
        this.numReturned(fetchClass, data);
      },
      err => {
        this.handleError("Error getting number of research results", err);
        this.numErrorReturned(fetchClass);
      }
    );
  }

  private setDefaultCounts() {
    if (this.activeEntity != "result" && this.allCounts.hasOwnProperty("results") && this.allCounts['results'] > 0) {
      this.numReturned(this.fetchPublications, this.allCounts['results']);
    }
    if (this.allCounts.hasOwnProperty(this.activeEntity != "datasources" && "datasources") && this.allCounts['datasources'] > 0) {
      this.numReturned(this.fetchDataproviders, this.allCounts['datasources']);
    }
    if (this.allCounts.hasOwnProperty(this.activeEntity != "organizations" && "organizations") && this.allCounts['organizations'] > 0) {
      this.numReturned(this.fetchOrganizations, this.allCounts['organizations']);
    }
    if (this.allCounts.hasOwnProperty(this.activeEntity != "projects" && "projects") && this.allCounts['projects'] > 0) {
      this.numReturned(this.fetchProjects, this.allCounts['projects']);
    }
  }

  private numReturned(fetchClass, num) {
    fetchClass.searchUtils.totalResults = num;
    fetchClass.searchUtils.status = this.errorCodes.DONE;
    if (fetchClass.searchUtils.totalResults == 0) {
      fetchClass.searchUtils.status = this.errorCodes.NONE;
    }
  }

  private numErrorReturned(fetchClass) {
    fetchClass.searchUtils.status = this.errorCodes.ERROR;
    fetchClass.searchUtils.totalResults = null;
  }

  private reloadTabs() {
    this.reload = {result:true, projects:true, datasources: true, services: true, organizations:true, all: !this.allCounts};
    this.fetchOrganizations.searchUtils.status = this.errorCodes.LOADING;
    this.fetchDataproviders.searchUtils.status =  this.errorCodes.LOADING;
    this.fetchServices.searchUtils.status =  this.errorCodes.LOADING;
    this.fetchProjects.searchUtils.status =  this.errorCodes.LOADING;
    this.fetchPublications.searchUtils.status =  this.errorCodes.LOADING;
    this.fetchDatasets.searchUtils.status =  this.errorCodes.LOADING;
    this.fetchSoftware.searchUtils.status =  this.errorCodes.LOADING;
    this.fetchOrps.searchUtils.status =  this.errorCodes.LOADING;
  }

  private handleError(message: string, error) {
    console.error("General Search Page: " + message, error);
  }

  activeEntityUpdate($event) {
    if($event.searchUtils.status!=this.errorCodes.LOADING) {
      this.disableForms = $event.disableForms;
    }
    let updated = true;
    if (this.activeEntity == "result") {
      if($event.searchUtils.status!=this.errorCodes.LOADING) {
        this.fetchPublications.searchUtils.totalResults = $event.searchUtils.totalResults;
      }
      this.fetchPublications.searchUtils.status = $event.searchUtils.status;
    }else if (this.activeEntity == "projects"){
      if($event.searchUtils.status!=this.errorCodes.LOADING) {
        this.fetchProjects.searchUtils.totalResults = $event.searchUtils.totalResults;
      }
      this.fetchProjects.searchUtils.status = $event.searchUtils.status;
    }else if (this.activeEntity == "datasources"){
      if($event.searchUtils.status!=this.errorCodes.LOADING) {
        this.fetchDataproviders.searchUtils.totalResults = $event.searchUtils.totalResults;
      }
      this.fetchDataproviders.searchUtils.status = $event.searchUtils.status;
    }else if (this.activeEntity == "services"){
      if($event.searchUtils.status!=this.errorCodes.LOADING) {
        this.fetchServices.searchUtils.totalResults = $event.searchUtils.totalResults;
      }
      this.fetchServices.searchUtils.status = $event.searchUtils.status;
    }else if (this.activeEntity == "organizations") {
      if($event.searchUtils.status!=this.errorCodes.LOADING) {
        this.fetchOrganizations.searchUtils.totalResults = $event.searchUtils.totalResults;
      }
      this.fetchOrganizations.searchUtils.status = $event.searchUtils.status;
    }else{
      updated = false;
    }
    if(updated) {
      this.cdr.detectChanges();
    }
  }

  entityChanged(entity) {
    if (this.activeEntity !== entity) {
      if (this.activeEntity == "result") {
        this.resultTypes = {publication: true, dataset: true, software: true, other: true};
      }
      if (this.activeEntity == "result" && this.searchResearchResultsComponent) {
        this.searchResearchResultsComponent.ngOnDestroy();
      } else if (this.activeEntity == "projects" && this.searchProjectsComponent) {
        this.searchProjectsComponent.ngOnDestroy();
      } else if (this.activeEntity == "datasources" && this.searchDataprovidersComponent) {
        this.searchDataprovidersComponent.ngOnDestroy();
      } else if (this.activeEntity == "services" && this.searchDataprovidersComponent) {
        this.searchDataprovidersComponent.ngOnDestroy();
      } else if (this.activeEntity == "organizations" && this.searchOrganizationsComponent) {
        this.searchOrganizationsComponent.ngOnDestroy();
      }
      this.activeEntity = entity;
      this.parameters = {};
      this.reload[entity] = true;
      if (this.keyword.length > 0) {
        this.parameters["fv0"] = this.keyword;
        this.parameters["f0"] = "q";
      }
      if (this.customFilter) {
        this.parameters = this.customFilter.getParameters(this.parameters);
      }
      if (entity == "result") {
        entity = "research-outcomes";
        // if(!!this.openAccess) {
        //   this.parameters["resultbestaccessright"] = '"' + encodeURIComponent("Open Access") + '"';
        // }
      }
      this.router.navigate(["/search/find", entity], {queryParams: this.parameters});
    }
  }

}
