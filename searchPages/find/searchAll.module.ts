import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';
import { RouterModule } from '@angular/router';


import {SearchResultsModule } from '../searchUtils/searchResults.module';

import {DataProvidersServiceModule} from '../../services/dataProvidersService.module';
import {ProjectsServiceModule} from '../../services/projectsService.module';
import {SearchResearchResultsServiceModule} from '../../services/searchResearchResultsService.module';
import {OrganizationsServiceModule} from '../../services/organizationsService.module';
import {Schema2jsonldModule} from '../../sharedComponents/schema2jsonld/schema2jsonld.module';

import { SEOServiceModule } from '../../sharedComponents/SEO/SEOService.module';
import {SearchAllComponent} from "./searchAll.component";
import {AdvancedSearchFormModule} from "../searchUtils/advancedSearchForm.module";
import {SearchResearchResultsModule} from "../searchResearchResults.module";
import {SearchProjectsModule} from "../searchProjects.module";
import {SearchOrganizationsModule} from "../searchOrganizations.module";
import {SearchDataProvidersModule} from "../searchDataProviders.module";
import {BreadcrumbsModule} from "../../utils/breadcrumbs/breadcrumbs.module";
import {SliderTabsModule} from "../../sharedComponents/tabs/slider-tabs.module";
import {NumberRoundModule} from "../../utils/pipes/number-round.module";
import {GroupedRequestsServiceModule} from "../../services/groupedRequestsService.module";

@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule,
    DataProvidersServiceModule, ProjectsServiceModule,
    SearchResearchResultsServiceModule, OrganizationsServiceModule,
    SearchResultsModule, Schema2jsonldModule, SEOServiceModule, AdvancedSearchFormModule, SearchResearchResultsModule, SearchProjectsModule, SearchOrganizationsModule, SearchDataProvidersModule, BreadcrumbsModule, SliderTabsModule, NumberRoundModule,
    GroupedRequestsServiceModule
  ],
  declarations: [
    SearchAllComponent
   ],
  providers:[],
  exports: [
    SearchAllComponent
     ]
})
export class SearchAllModule { }
