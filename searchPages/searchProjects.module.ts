import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {NewSearchPageModule} from "./searchUtils/newSearchPage.module";
import {SearchProjectsComponent} from "./searchProjects.component";
import {ProjectsServiceModule} from "../services/projectsService.module";

@NgModule({
  imports: [
    CommonModule, FormsModule,
    ProjectsServiceModule,
    NewSearchPageModule

  ],
  declarations: [
    SearchProjectsComponent
  ],
  providers: [],
  exports: [
    SearchProjectsComponent
  ]
})
export class SearchProjectsModule {
}
