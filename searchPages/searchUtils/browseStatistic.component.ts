import {Component, Input, } from '@angular/core';
import {RouterHelper} from '../../utils/routerHelper.class';

@Component({
    selector: 'browse-statistic',
    templateUrl: 'browseStatistic.component.html'

})
export class BrowseStatisticComponent {

  @Input() public baseUrl:string = "";
  @Input() public filter:any = "";
  private _maxCharacters = 30;
  public viewAll = false;
  public routerHelper:RouterHelper = new RouterHelper();
  keyword = "";
  sortBy = "num";
  constructor () {

  }

  public ngOnInit() {


  }

      quote(str:string){
        return '"'+str+'"';
      }

  public _formatName(value){
    let maxLineLength = 29;
    //3 space before number + parenthesis
    if(value.name.length+value.number.toLocaleString().length +3> maxLineLength){
      return value.name.substr(0, (maxLineLength - 3 -3 - value.number.toLocaleString().length))+'...';
    }

    return value.name;
  }
  filterKeywords(value){
    if(this.keyword.length > 0){
      if(value.toLowerCase().indexOf(this.keyword.toLowerCase()) ==-1){
        return false;
      }
    }
    return true;
  }

  getValues(filter, sortBy:string = "num"):any{

    if(sortBy == "name"){
      var array = filter.values.slice();
      array.sort((n1,n2)=> {
        if (n1.name > n2.name) {
            return 1;
        }

        if (n1.name < n2.name) {
            return -1;
        }

        return 0;
      });
      return array;
    }else{
      return filter.values;
    }


  }
}
