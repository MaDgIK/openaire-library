import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {ErrorCodes} from '../../utils/properties/errorCodes';
import {RouterHelper} from '../../utils/routerHelper.class';
import {EnvProperties} from '../../utils/properties/env-properties';
import {CommunityInfo} from "../../connect/community/communityInfo";
import {Router} from "@angular/router";
import {LocalStorageService} from "../../services/localStorage.service";
import {Stakeholder, StakeholderInfo, Visibility} from "../../monitor/entities/stakeholder";
import {StringUtils} from '../../utils/string-utils.class';

@Component({
  selector: 'portal-search-result',
  templateUrl: 'portal-search-result.component.html',
	styleUrls: ['portal-search-result.component.less']
})

export class PortalSearchResultComponent implements OnInit{
  @Input() results: (CommunityInfo & StakeholderInfo)[];
  @Input() status: number;
  @Input() type: string;
  @Input() showType = false;
  @Input() showLoading: boolean = false;
  @Input() custom_class: string = "search-results";
  @Input() properties: EnvProperties;
  @Input() isMobile: boolean = false;
  @ViewChild('AlertModal') modal;
  visibilityIcon: Map<Visibility, string> = new Map<Visibility, string> ([
    ["PRIVATE", 'incognito'],
    ["RESTRICTED", 'restricted']
  ]);
  public urlParam: string;
  public errorCodes: ErrorCodes = new ErrorCodes();
  public routerHelper: RouterHelper = new RouterHelper();
  public errorMessage: string = "No results found";
  public selected: CommunityInfo & Stakeholder;
  public directLink: boolean = true;
  sub;
  
  constructor(private router: Router,
              private localStorageService: LocalStorageService) {
  }
  ngOnDestroy() {
    if(this.sub){
      this.sub.unsubscribe();
    }
  }
  ngOnInit() {
    this.sub = this.localStorageService.get().subscribe(value => {
      this.directLink = value;
    });
  }
  
  mapType(type: string) {
		if(type === 'community') {
			return 'Research Community';
		} else {
			return StringUtils.getStakeholderType(type, false);
		}
  }
  
  hasPermission(result: CommunityInfo & StakeholderInfo) {
    if(this.type === "community") {
      return result.isPublic() || result.isRestricted();
    } else if(this.type === "stakeholder") {
      return result.visibility === "PUBLIC" || result.visibility === "RESTRICTED" ||
        (result.visibility === "PRIVATE" && result.isManager);
    }
  }
  
  getEnvironmentPrefix(): string {
    return (this.properties.environment == "production") ? "" : "beta.";
  }
  
  isProduction(): boolean {
    return this.properties.environment != "development";
  }
  
  public confirmModalOpen(result: CommunityInfo & Stakeholder) {
    this.selected = result;
    if(this.type === 'stakeholder') {
      this.modal.alertTitle = 'You are going to visit ' + result.name + ' Monitor Dashboard';
    } else if (this.type === 'community') {
      this.modal.alertTitle = 'You are going to visit ' + ((result.title) ? result.title : result.shortTitle) +' Gateway';
    }
    this.modal.message = 'You will be navigated to a new tab. Are you sure that you want to proceed?';
    this.modal.okButtonLeft = false;
    this.modal.okButtonText = 'Yes';
    this.modal.cancelButtonText = 'No';
    this.modal.choice = true;
    this.modal.open();
  }
  
  public getCommunityPageUrl(communityInfo: CommunityInfo): string {
    if (this.isProduction()) {
      return 'https://' + this.getEnvironmentPrefix() + communityInfo.communityId + '.openaire.eu';
    } else {
      return this.router.createUrlTree(['/'], {
        queryParams: {'communityId': communityInfo.communityId}
      }).toString();
    }
  }

  public getStakeholderPageUrl(stakeholder: Stakeholder) {
    return this.properties.domain + this.properties.baseLink +"/" + stakeholder.alias;
  }
  
  public goToPage(data: any) {
    if (data.value == true) {
      let url = '';
      if (this.type === 'stakeholder') {
        url = this.getStakeholderPageUrl(this.selected);
      } else if (this.type === 'community') {
        url = this.getCommunityPageUrl(this.selected);
      }
      this.localStorageService.setCommunityDirectLink(data.choice);
      window.open(url, '_blank');
    }
  }
}
