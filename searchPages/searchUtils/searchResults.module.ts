import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {ErrorMessagesModule} from '../../utils/errorMessages.module';
import {SearchResultComponent} from './searchResult.component';
import {ApprovedByCommunityModule} from '../../connect/approvedByCommunity/approved.module';
import {ShowAuthorsModule} from "../../utils/authors/showAuthors.module";
import {HighlightModule} from "./highlight/highlight.module";
import {ResultPreviewModule} from "../../utils/result-preview/result-preview.module";
import {OrcidService} from "../../orcid/orcid.service";

@NgModule({
  imports: [
    CommonModule, FormsModule,
    RouterModule, ErrorMessagesModule,
    ApprovedByCommunityModule,
    ShowAuthorsModule, HighlightModule, ResultPreviewModule
  ],
  declarations: [
    SearchResultComponent
  ],
  providers:[
    OrcidService
  ],
  exports: [
    SearchResultComponent
  ]
})
export class SearchResultsModule { }
