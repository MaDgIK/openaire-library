import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule, ReactiveFormsModule }         from '@angular/forms';
import { MyDatePickerModule } from '../../utils/my-date-picker/my-date-picker.module';
import {DateFilterComponent} from './dateFilter.component';
import { MatNativeDateModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
@NgModule({
  imports: [
    CommonModule, FormsModule, ReactiveFormsModule, MyDatePickerModule,
    MatNativeDateModule, MatDatepickerModule, MatFormFieldModule, MatInputModule, MatSelectModule
  ],
  declarations: [
    DateFilterComponent
],

  providers:[
  ],
  exports: [
    DateFilterComponent
    ]
})
export class DateFilterModule { }
