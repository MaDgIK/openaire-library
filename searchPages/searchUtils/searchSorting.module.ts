import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {SearchSortingComponent} from './searchSorting.component';
import {InputModule} from '../../sharedComponents/input/input.module';

@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule, InputModule
  ],
  declarations: [
    SearchSortingComponent
],

  providers:[
  ],
  exports: [
    SearchSortingComponent

    ]
})
export class SearchSortingModule { }
