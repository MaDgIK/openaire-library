import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {EntitiesSelectionComponent} from "./entitiesSelection.component";
import {InputModule} from "../../sharedComponents/input/input.module";

@NgModule({
  imports: [
    CommonModule, FormsModule,
    RouterModule, ReactiveFormsModule, InputModule
  ],
  declarations: [
    EntitiesSelectionComponent,
  ],
  providers: [],
  exports: [
    EntitiesSelectionComponent
  ]
})
export class EntitiesSelectionModule {
}
