import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MatSelectModule } from "@angular/material/select";

import {RefineFieldResultsServiceModule} from '../../services/refineFieldResultsService.module';

import {BrowseEntitiesComponent} from './browseEntities.component';
import {BrowseStatisticComponent} from './browseStatistic.component';

import {ErrorMessagesModule} from '../../utils/errorMessages.module';

@NgModule({
  imports: [
    CommonModule, FormsModule, ErrorMessagesModule,
    RefineFieldResultsServiceModule, RouterModule,
    MatSelectModule
  ],
  declarations: [
      BrowseEntitiesComponent,
      BrowseStatisticComponent

],

  providers:[
  ],
  exports: [
    BrowseEntitiesComponent,
    BrowseStatisticComponent
    ]
})
export class BrowseEntitiesModule { }
