import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {QuickSelectionsComponent} from "./quick-selections.component";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import {SearchFilterModule} from "./searchFilter.module";

@NgModule({
  imports: [
    CommonModule, FormsModule,
    RouterModule, MatCheckboxModule, ReactiveFormsModule, MatSlideToggleModule, SearchFilterModule
  ],
  declarations: [
    QuickSelectionsComponent,
  ],
  providers:[
  ],
  exports: [
    QuickSelectionsComponent
    ]
})
export class QuickSelectionsModule { }
