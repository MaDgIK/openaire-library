import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
	selector: 'no-load-paging',
	template: `
		<div class="uk-margin-small-bottom">
			<div class="uk-panel uk-grid uk-flex uk-flex-middle uk-child-width-1-1 uk-child-width-1-2@m" uk-grid>
				<results-and-pages [type]="type" [page]="page" [pageSize]="pageSize" [totalResults]="totalResults"></results-and-pages>
				<div>
					<paging-no-load  #paging [currentPage]="page"
													customClasses="uk-margin-remove-bottom uk-flex-right"
													[totalResults]="totalResults" [size]="pageSize"
													(pageChange)="updatePage($event)">
					</paging-no-load>
				</div>
			</div>
		</div>
	`
})
export class NoLoadPagingComponent {
	@Input() type: string;
	@Input() page: number = 1;
	@Input() pageSize: number = 10;
	@Input() totalResults: number;
	@Output() pageChange: EventEmitter<any> = new EventEmitter<any>();
	
	public updatePage(event) {
		this.pageChange.emit({
			value: event.value
		});
	}
}
