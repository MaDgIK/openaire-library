import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {SearchResult} from '../../utils/entities/searchResult';
import {EnvProperties} from '../../utils/properties/env-properties';
import {ResultPreview} from "../../utils/result-preview/result-preview";
import {OrcidService} from "../../orcid/orcid.service";
import {Session} from "../../login/utils/helper.class";
import {properties} from "../../../../environments/environment";
import {LayoutService} from "../../dashboard/sharedComponents/sidebar/layout.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'search-result',
  templateUrl: 'searchResult.component.html'
})
export class SearchResultComponent implements OnInit, OnChanges {
  @Input() prevPath: string = "";
  @Input() results: SearchResult[];
  previewResults: ResultPreview[];
  @Input() status: number;
  @Input() type: string;
  @Input() showLoading: boolean = false;
  @Input() showSubjects: boolean = true;
  @Input() showOrganizations: boolean = true;
  @Input() custom_class: string = "";
  @Input() properties: EnvProperties;
  @Input() showEnermaps: boolean;
  @Input() compactView: boolean = false; // if true, show less info (e.g. hide description) on each result

  @Input() isLoggedIn: boolean = false;

  public isMobile: boolean = false;
  private subscriptions: any[] = [];

  constructor(private orcidService: OrcidService,
              private layoutService: LayoutService) {
  }

  ngOnInit() {
    this.subscriptions.push(this.layoutService.isMobile.subscribe(isMobile => {
      this.isMobile = isMobile;
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if(subscription instanceof Subscription) {
        subscription.unsubscribe();
      }
    })
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.results) {
      this.initialize();
    }
  }

  initialize() {
    this.previewResults = [];
    for (let result of this.results) {
      this.previewResults.push(this.getResultPreview(result));
    }

    if ((properties.adminToolsPortalType == "explore" || properties.adminToolsPortalType == "community" || properties.adminToolsPortalType == "aggregator" || properties.dashboard == "irish")
        && this.isLoggedIn && this.results && this.results.length > 0
        && (this.type == "result" || this.type == "publication" || this.type == "dataset" || this.type == "software" || this.type == "other")
    ) {
      this.subscriptions.push(this.orcidService.getPutCodes(
        this.previewResults.map(previewResult => {return previewResult.relcanId}),
        this.previewResults.map(
          previewResult => {
            if (previewResult.identifiers) {
              let pidsArray: string[] = null;
              if(previewResult.identifiers?.size > 0) {
                pidsArray = [];
                for (let key of Array.from(previewResult.identifiers.keys())) {
                  pidsArray = pidsArray.concat(previewResult.identifiers.get(key));
                }
              }
              return pidsArray;//.join();
            }
          })).subscribe(
          putCodes => {
            for (let i = 0; i < this.previewResults.length; i++) {
              //if (this.previewResults[i].identifiers) {
                this.previewResults[i].orcidPutCodes = putCodes[i];
                // console.debug(i, this.previewResults[i].orcidPutCodes);
              //}
            }
            this.previewResults = JSON.parse(JSON.stringify(this.previewResults, this.replacer), this.reviver);
          }, error => {

          }
      ));
    }
  }

  private replacer(key, value) {
    if(value instanceof Map) {
      return {
        dataType: 'Map',
        value: Array.from(value.entries()), // or with spread: value: [...value]
      };
    } else {
      return value;
    }
  }

  private reviver(key, value) {
    if(typeof value === 'object' && value !== null) {
      if (value.dataType === 'Map') {
        return new Map(value.value);
      }
    }
    return value;
  }

  public getResultPreview(result: SearchResult): ResultPreview {
    return ResultPreview.searchResultConvert(result, (result.entityType) ? result.entityType : this.type);
  }


  public quote(params: string): string {
    return '"' + params + '"';
  }
}
