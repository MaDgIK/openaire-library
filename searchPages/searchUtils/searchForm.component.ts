import {Component, Input, Output, EventEmitter} from '@angular/core';
import {RouterHelper} from '../../utils/routerHelper.class';
import {Router} from '@angular/router';

@Component({
  selector: 'search-form',
  template: `

        <form [class]="((isDisabled)?'uk-disabled':'')+(setFormCentered?' uk-text-center':'')">
            <div class="uk-inline">
                <a *ngIf="keyword.length > 0" class="uk-form-icon uk-form-icon-flip"
                   (click)="keyword = ''; keywordChanged()"
                   uk-icon="icon: close"></a>
                <input type="text" class="uk-input text-input-box uk-width-xlarge@l uk-width-large@m uk-width-medium"
                       [placeholder]="placeholderText" aria-describedby="sizing-addon2"
                       [(ngModel)]="keyword" name="keyword">
            </div>
            <button *ngIf="!link" (click)="keywordChanged()"  type="submit" class=" uk-button  portal-button uk-margin-small-left">
                Search
          </button>
          <button *ngIf="link"   (click)="goTo()"  type="submit" class="uk-button portal-button uk-padding uk-padding-remove-vertical uk-margin-small-left">
              Search
          </button>
        </form>
    `
})

export class SearchFormComponent {
    @Input() isDisabled: boolean = false;
    @Input() keyword: string = '';
    @Input() generalSearch: boolean = false;
    @Input() placeholderText: string = "Type keywords";
    @Input() link: string = "";
    @Input() setFormCentered: boolean = true;
    public routerHelper:RouterHelper = new RouterHelper();

    @Output() keywordChange  = new EventEmitter();

    constructor (private _router:Router) {
     }

    ngOnInit() {

    }

    keywordChanged() {
        //console.info("inside form: "+this.keyword);
        this.keywordChange.emit({
            value: this.keyword
        });
    }
    goTo() {
      //this._router.navigate(['/search/find'], { queryParams: this.routerHelper.createQueryParam('keyword',this.keyword) });
      this._router.navigate([this.link], { queryParams: this.routerHelper.createQueryParam('keyword',this.keyword) });
    }
}
