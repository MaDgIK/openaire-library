import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {AdvancedSearchFormComponent} from './advancedSearchForm.component';
import {EntitiesAutocompleteModule} from '../../utils/entitiesAutoComplete/entitiesAutoComplete.module';
import {StaticAutocompleteModule} from '../../utils/staticAutoComplete/staticAutoComplete.module';
import {DateFilterModule} from './dateFilter.module';
import {SearchFormModule} from './searchForm.module';
import {QuickSelectionsModule} from "./quick-selections.module";
import {EntitiesSelectionModule} from "./entitiesSelection.module";
import {IconsModule} from "../../utils/icons/icons.module";
import {SearchInputModule} from "../../sharedComponents/search-input/search-input.module";
import {AdvancedSearchInputModule} from "../../sharedComponents/advanced-search-input/advanced-search-input.module";
import {InputModule} from "../../sharedComponents/input/input.module";
import {IconsService} from "../../utils/icons/icons.service";
import {filters} from "../../utils/icons/icons";


@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule, EntitiesAutocompleteModule, StaticAutocompleteModule, DateFilterModule,
    SearchFormModule, QuickSelectionsModule, EntitiesSelectionModule, IconsModule, SearchInputModule, AdvancedSearchInputModule, InputModule
  ],
  declarations: [
    AdvancedSearchFormComponent,
  ],
  exports: [
    AdvancedSearchFormComponent
  
  ]
})
export class AdvancedSearchFormModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([filters]);
  }
}
