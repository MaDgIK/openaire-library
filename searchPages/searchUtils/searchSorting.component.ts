import {Component, Input, Output, EventEmitter} from '@angular/core';
import {Option} from "../../sharedComponents/input/input.component";
import {properties} from "../../../../environments/environment";

@Component({
  selector: 'search-sorting',
  template: `
    <div *ngIf="options" class="uk-width-small">
      <div input
           type="select" placeholder="Sort by" inputClass="border-bottom"
           [options]="options" [(value)]="sortBy" [disabled]="isDisabled"
           (valueChange)="sortByChanged()"></div>
    </div>
  `
})

export class SearchSortingComponent {
  @Input() isDisabled: boolean = false;
  @Input() sortBy: string = '';
  @Input() entityType: string = '';
  @Output() sortByChange = new EventEmitter();
  public options: Option[];
  private generalOptions = [
      {value: '', label: 'Relevance'},
      {value: 'resultdateofacceptance,descending', label: 'Date (most recent)'},
      {value: 'resultdateofacceptance,ascending', label: 'Date (least recent)'},
      {value: 'citation_count,descending', label: 'Citation Count'},
      {value: 'popularity,descending', label: 'Popularity'},
      {value: 'influence,descending', label: 'Influence'},
      {value: 'impulse,descending', label: 'Impulse'}
    ];
  private communityOptions = [
    {value: '', label: 'Title'},
    {value: 'creationdate,descending', label: 'Creation Date (most recent)'},
    {value: 'creationdate,ascending', label: 'Creation Date (least recent)'},
  ];
  private stakeholderOptions = [
    {value: '', label: 'Name'},
    {value: 'creationdate,descending', label: 'Creation Date (most recent)'},
    {value: 'creationdate,ascending', label: 'Creation Date (least recent)'},
  ];
  
  constructor() {
  }
  
  ngOnInit() {
    if (this.entityType === 'community') {
      this.options = this.communityOptions;
    } else if (this.entityType === 'stakeholder') {
      this.options = this.stakeholderOptions;
    } else {
      this.options = this.generalOptions;
    }
  }
  
  
  sortByChanged() {
    this.sortByChange.emit(this.sortBy);
  }
}
