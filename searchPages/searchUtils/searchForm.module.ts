import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';
import { RouterModule } from '@angular/router';

import{SearchFormComponent} from './searchForm.component';

@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule
  ],
  declarations: [
    SearchFormComponent
],

  providers:[
  ],
  exports: [
    SearchFormComponent

    ]
})
export class SearchFormModule { }
