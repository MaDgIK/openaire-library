import {Component, ViewEncapsulation, ComponentRef, ElementRef, Input, EventEmitter, Output} from '@angular/core';
import { Filter} from './searchHelperClasses.class';

@Component({
  selector: 'modal-search-filter',
  template: `
  <div *ngIf="filter!=undefined" [class]="(!isOpen)?'uk-modal ':'uk-modal uk-open uk-animation-fade'" uk-modal  [open]="!isOpen" id="modal-{{filter.filterId}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="uk-modal-dialog uk-modal-body">
      <button type="button" class="uk-modal-close-default" uk-close (click)="close()"></button>


      <h3  class="uk-margin-bottom-remove uk-margin-top">
        {{filter.title}}
      </h3>

      <div class="uk-accordion-content searchFilterBoxValues ">
        <div *ngFor = "let value of getSelectedValues(filter)"  class = "uk-animation-fade filterItem">

           <span class="filterName"><div title = "{{value.name}}">
           <input [(ngModel)]="value.selected" type="checkbox" (ngModelChange)="filterModalChange(value.selected)">
           {{value.name}} </div></span>
           <span class="filterNumber" *ngIf = "showResultCount === true" >  ({{value.number | number}})</span>
        </div>
        <hr *ngIf="getSelectedValues(filter).length > 0 && getNotSelectedValues(filter).length > 0" class="uk-grid-divider uk-margin-remove">
        <div *ngFor = "let value of getNotSelectedValues(filter)" class = "uk-animation-fade filterItem">
           <span class="filterName"><div title = "{{value.name}}">
           <input [(ngModel)]="value.selected" type="checkbox" (ngModelChange)="filterModalChange(value.selected)">
           {{value.name}} </div></span>
           <span class="filterNumber" *ngIf = "showResultCount === true" >  ({{value.number}})</span>
        </div>
      </div>
    </div>
  </div>

  `,
  encapsulation: ViewEncapsulation.None,
})
/**
  * API to an open search filter window.
  */
export class SearchFilterModalComponent{
  @Input() filter: Filter;
  @Input() showResultCount:boolean = true;
  @Output() modalChange = new EventEmitter();

  public isOpen:boolean=false;

  constructor( public _elementRef: ElementRef){}

  filterModalChange(selected:boolean) {
    //console.info("Modal Changed");
    this.filterChange(selected);
    this.modalChange.emit({
      value: selected
    });
    this.close();
  }
  filterChange(selected:boolean){
      if(selected){
          this.filter.countSelectedValues++;
          // this.reorderFilterValues();
      }else{
          this.filter.countSelectedValues--;
          // this.reorderFilterValues();
      }
  }
  getSelectedValues(filter):any{
    var selected = [];
    if(filter.countSelectedValues >0){
      for (var i=0; i < filter.values.length; i++){
        if(filter.values[i].selected){
          selected.push(filter.values[i]);
        }
      }
    }
    return selected;

  }
  getNotSelectedValues(filter):any{
    var notSselected = [];
    if(filter.countSelectedValues >0){
      for (var i=0; i < filter.values.length; i++){
        if(!filter.values[i].selected){
          notSselected.push(filter.values[i]);
        }
      }
    }else {
      notSselected = filter.values;
    }
    return notSselected;
  }

  open() {
    this.isOpen = true;
  }

  close() {
    this.isOpen = false;
  }

}
