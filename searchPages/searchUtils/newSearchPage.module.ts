import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {NewSearchPageComponent} from './newSearchPage.component';
import {SearchFormModule} from './searchForm.module';
import {SearchFilterModule} from './searchFilter.module';
import {RangeFilterModule} from "../../utils/rangeFilter/rangeFilter.module";
import {LoadingModalModule} from '../../utils/modal/loadingModal.module';
import {ReportsServiceModule} from '../../services/reportsService.module';
import {SearchPagingModule} from './searchPaging.module';
import {SearchSortingModule} from './searchSorting.module';
import {SearchDownloadModule} from './searchDownload.module';
import {ModalModule} from '../../utils/modal/modal.module';
import {HelperModule} from '../../utils/helper/helper.module';
import {Schema2jsonldModule} from '../../sharedComponents/schema2jsonld/schema2jsonld.module';
import {SEOServiceModule} from '../../sharedComponents/SEO/SEOService.module';
import {PortalSearchResultModule} from "./portal-search-result.module";
import {SearchResultsModule} from "./searchResults.module";
import {SearchResultsInDepositModule} from "../../deposit/searchResultsInDeposit.module";
import {AdvancedSearchFormModule} from "./advancedSearchForm.module";
import {QuickSelectionsModule} from "./quick-selections.module";
import {BreadcrumbsModule} from "../../utils/breadcrumbs/breadcrumbs.module";
import {AlertModalModule} from "../../utils/modal/alertModal.module";
import {ClickModule} from "../../utils/click/click.module";
import {SearchResultsForOrcidModule} from "../../orcid/recommend-orcid-links/searchResultsForOrcid.module";
import {IconsModule} from "../../utils/icons/icons.module";
import {LoadingModule} from "../../utils/loading/loading.module";
import {InputModule} from '../../sharedComponents/input/input.module';
import {IconsService} from "../../utils/icons/icons.service";
import {graph} from "../../utils/icons/icons";

@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule, SearchFormModule, PortalSearchResultModule,
    LoadingModalModule, ReportsServiceModule,
    SearchPagingModule, SearchSortingModule, SearchDownloadModule, ModalModule,
    SearchFilterModule, RangeFilterModule,
    HelperModule, Schema2jsonldModule, SEOServiceModule, SearchResultsModule,
    SearchResultsInDepositModule, SearchResultsForOrcidModule,
    AdvancedSearchFormModule, QuickSelectionsModule, BreadcrumbsModule, AlertModalModule, ClickModule, IconsModule, LoadingModule,
    InputModule
  ],
  declarations: [
    NewSearchPageComponent
  ],
  providers: [],
  exports: [
    NewSearchPageComponent
  ]
})
export class NewSearchPageModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([graph])
  }
}
