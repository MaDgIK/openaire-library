import {NgModule} from "@angular/core";
import {HighlightComponent} from "./highlight.component";
import {CommonModule} from "@angular/common";

@NgModule({
  declarations: [HighlightComponent],
  imports: [CommonModule],
  exports: [HighlightComponent]
})
export class HighlightModule {}