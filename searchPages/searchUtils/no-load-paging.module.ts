import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {PagingModule} from '../../utils/paging.module';
import {NoLoadPagingComponent} from "./no-load-paging.component";

@NgModule({
  imports: [
    CommonModule, FormsModule, PagingModule
  ],
  declarations: [
    NoLoadPagingComponent
  ],
  exports: [
    NoLoadPagingComponent
  ]
})
export class NoLoadPaging {
}
