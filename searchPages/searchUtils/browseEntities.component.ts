import {Component, Input, } from '@angular/core';
import {RefineFieldResultsService} from '../../services/refineFieldResults.service';
import {ErrorCodes} from '../../utils/properties/errorCodes';
import {ErrorMessagesComponent}    from '../../utils/errorMessages.component';
import {SearchFields} from '../../utils/properties/searchFields';
import {SearchCustomFilter} from '../searchUtils/searchUtils.class';
import{EnvProperties} from '../../utils/properties/env-properties';
import {StringUtils}             from '../../utils/string-utils.class';

@Component({
    selector: 'browse-entities',
    template: `
<div>

  <errorMessages [status]="[status]" [type]="'results'"></errorMessages>

  <div class ="uk-grid" *ngIf="status ===errorCodes.DONE">
    <div *ngFor= "let filter of filters" class = "uk-margin-bottom uk-width-1-3@l uk-width-1-3@m  uk-width-1-2@s">
      <browse-statistic   [baseUrl]=baseUrl [filter]=filter ></browse-statistic>
    </div>
  </div>
</div>
`

})
export class BrowseEntitiesComponent {
  public searchFields:SearchFields = new SearchFields();
  public filters =[];
  @Input() public baseUrl:string = "";
  @Input() public entityName:string = "";
  @Input() public refineFields: string[] ;//=  this.searchFields.RESULT_REFINE_FIELDS;
  @Input() properties:EnvProperties;
  @Input() customFilter:SearchCustomFilter= null;

  public sub: any;
  public errorCodes:ErrorCodes = new ErrorCodes();
  private errorMessages: ErrorMessagesComponent;
  public status = this.errorCodes.LOADING;
   public fieldIdsMap=this.searchFields.RESULT_REFINE_FIELDS;

  constructor ( private _refineFieldsService: RefineFieldResultsService ) {
    this.errorMessages = new ErrorMessagesComponent();
  }

  public ngOnInit() {
        for(var i=0; i < this.searchFields.HIDDEN_FIELDS.length; i++){
          var index = this.refineFields.indexOf(this.searchFields.HIDDEN_FIELDS[i]) ;
          if(index > -1){
            this.refineFields.splice(index,1);

          }
        }
        this.getStats();
  }

  public ngOnDestroy() {
    if(this.sub){
      this.sub.unsubscribe();
    }
  }


private getStats(){

  this.status = this.errorCodes.LOADING;
  var refineParams = null;
  if(this.customFilter){
    refineParams = (refineParams?(refineParams+'&'):'')+"&fq="+StringUtils.URIEncode(this.customFilter.queryFieldName + " exact " + StringUtils.quote((this.customFilter.valueId )));
  }
  this.sub = this._refineFieldsService.getRefineFieldsResultsByEntityName(this.refineFields,this.entityName, this.properties,refineParams).subscribe(
      data => {
          //console.info("Get Stats for "+this.entityName+ ": [Total:"+data[0]+" ]  [fields: "+data[1].length+"]");
          this.filters = data[1];
          this.status = this.errorCodes.DONE;
          if(data[0] == 0 ){
            this.status = this.errorCodes.NONE;
          }
      },
      err => {
          //console.log(err);
          this.handleError("Error getting refine fields results (stats) by entity: "+this.entityName, err);
          this.status = this.errorMessages.getErrorCode(err.status);

           //TODO check erros (service not available, bad request)
          // if( ){
          //   this.searchUtils.status = ErrorCodes.ERROR;
          // }
          //this.status = this.errorCodes.ERROR;
          /*if(err.status == '404') {
            this.status = this.errorCodes.NOT_FOUND;
          } else if(err.status == '500') {
            this.status = this.errorCodes.ERROR;
          } else {
            this.status = this.errorCodes.NOT_AVAILABLE;
          }*/
      }
  );
 }

  private handleError(message: string, error) {
    console.error("Browse Entities (Search): "+message, error);
  }
}
