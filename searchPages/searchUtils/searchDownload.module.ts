import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';

import {SearchDownloadComponent} from './searchDownload.component';
import {LoadingModalModule} from '../../utils/modal/loadingModal.module';
import {AlertModalModule} from '../../utils/modal/alertModal.module';
import {IconsModule} from "../../utils/icons/icons.module";

@NgModule({
  imports: [
    CommonModule, FormsModule, LoadingModalModule, AlertModalModule, IconsModule
  ],
  declarations: [
    SearchDownloadComponent
],

  providers:[
  ],
  exports: [
    SearchDownloadComponent

    ]
})
export class SearchDownloadModule { }
