import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {SearchPagingComponent} from './searchPaging.component';
import {PagingModule} from '../../utils/paging.module';

@NgModule({
  imports: [
    CommonModule, FormsModule, PagingModule
  ],
  declarations: [
    SearchPagingComponent
  ],
  exports: [
    SearchPagingComponent
  ]
})
export class SearchPagingModule {
}
