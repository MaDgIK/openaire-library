import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {ModalResultComponent} from './modal-result.component';
import {RouterModule} from '@angular/router';
import {ErrorMessagesModule} from "../../../utils/errorMessages.module";
import {ResultPreviewModule} from "../../../utils/result-preview/result-preview.module";
import {NoLoadPaging} from "../no-load-paging.module";

@NgModule({
  imports: [
    CommonModule, FormsModule,
    RouterModule, ErrorMessagesModule, ResultPreviewModule, NoLoadPaging
  ],
  declarations: [
    ModalResultComponent,
  ],
  exports: [
    ModalResultComponent
  ]
})
export class ModalResultModule {
}
