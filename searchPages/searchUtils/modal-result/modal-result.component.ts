import {Component, EventEmitter, Input, Output} from '@angular/core';
import {SearchResult} from "../../../utils/entities/searchResult";
import {ErrorCodes} from "../../../utils/properties/errorCodes";
import {RouterHelper} from "../../../utils/routerHelper.class";
import {ResultPreview} from "../../../utils/result-preview/result-preview";
import {AlertModal} from "../../../utils/modal/alert";
import {EnvProperties} from "../../../utils/properties/env-properties";

@Component({
  selector: 'modal-result',
  template: `
    <errorMessages [status]="[status]" [type]="type" tab_error_class=true></errorMessages>
    <div *ngIf="status == errorCodes.DONE">
      <no-load-paging *ngIf="totalResults > pageSize" [type]="type"
                      (pageChange)="update($event)"
                      [page]="page" [pageSize]="pageSize"
                      [totalResults]="totalResults">
      </no-load-paging>
      <ul class="uk-list uk-list-divider  uk-margin">
        <li *ngFor="let result of results">
          <result-preview [modal]="modal" [properties]="properties"
                          [result]="getResultPreview(result)"
                          [showSubjects]="showSubjects"
                          [showOrganizations]="showOrganizations"
													[isCard]="false"></result-preview>
        </li>
      </ul>
      <no-load-paging *ngIf="totalResults > pageSize" [type]="type"
                      (pageChange)="update($event)"
                      [page]="page" [pageSize]="pageSize"
                      [totalResults]="totalResults">
      </no-load-paging>
    </div>
  `
})
export class ModalResultComponent {
  @Input() results: SearchResult[];
  @Input() totalResults: number = 0;
  @Input() type: string;
  @Input() resultType: string;
  @Input() showOrganizations: boolean = true;
  @Input() showSubjects: boolean = false;
  @Input() modal: AlertModal;
  @Input() properties: EnvProperties;
  @Input() status: number;
  @Input() pageSize: number = 1;
  @Output() updatePage: EventEmitter<any> = new EventEmitter<any>();
  
  public page: number = 1;
  public errorCodes: ErrorCodes = new ErrorCodes();
  public routerHelper: RouterHelper = new RouterHelper();
  public errorMessage: string = "No results found";
  
  constructor() {
  }
  
  ngOnInit() {
  }
  
  public getResultPreview(result: SearchResult): ResultPreview {
    return ResultPreview.searchResultConvert(result, this.resultType);
  }
  
  public update(event) {
    this.page = event.value;
    this.updatePage.emit({
      value: event.value
    });
  }
}
