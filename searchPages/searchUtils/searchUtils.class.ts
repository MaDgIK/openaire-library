import {properties} from "../../../../environments/environment";

export class SearchUtilsClass{
  page:number = 1;
  size:number = 10;
  defaultSize:number = 10;
  status:number = 1;
  refineStatus:number = 1;
  keyword:string = "";
  baseUrl:string = "";
  totalResults = null;
  totalResultsNoFilters:number; // for organization landing - tab with projects
  sortBy: string = "";

  validateSize(sizeFromParams){
    this.size = (sizeFromParams=== undefined)?this.defaultSize:+sizeFromParams;
    if(this.size != 5 && this.size != 10 && this.size != 20 && this.size != 50) {
      this.size = this.defaultSize;
    }
  }
}

export class SearchCustomFilter{
 fieldName:string; //Country
 queryFieldName:string; //country
 valueId:string; //gr
 valueName:string; // Greece
 isHiddenFilter:boolean;
 selected:boolean;
 showFilterMessage:boolean;
 customQuery:string;
 constructor( fieldName:string, queryFieldName:string, valueId:string, valueName:string, showFilterMessage:boolean = true, customQuery =  null ){
   if(valueId == "test" && properties.environment == "development"){
     valueId = "covid-19";
   }
   this.showFilterMessage = showFilterMessage;
   this.isHiddenFilter = true;
   this.fieldName = fieldName;
   this.queryFieldName = queryFieldName;
   this.valueId = valueId;
   this.valueName = valueName;
   this.selected = null;
   this.customQuery = customQuery;
 }

 public getParameters(params={}){
   if(!this.isHiddenFilter){
     params[this.queryFieldName] = this.valueId;
     params['cf'] = true;
   }
   return params;
 }
  // public setFilter(filter:SearchCustomFilter){
  //  if(!filter){
  //    return;
  //  }
  //   this.fieldName = filter.fieldName;
  //   this.queryFieldName = filter.queryFieldName;
  //   this.valueId = filter.valueId;
  //   this.valueName = filter.valueName;
  // }
}
