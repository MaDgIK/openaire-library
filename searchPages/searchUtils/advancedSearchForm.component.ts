import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges, ViewChild
} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {AdvancedField, Filter} from './searchHelperClasses.class';
import {OpenaireEntities, SearchFields} from '../../utils/properties/searchFields';
import {Dates} from '../../utils/string-utils.class';
import {EnvProperties} from '../../utils/properties/env-properties';
import {SearchCustomFilter} from "./searchUtils.class";
import {Subscriber} from "rxjs";
import {properties} from "../../../../environments/environment";
import {Option} from "../../sharedComponents/input/input.component";

@Component({
  selector: 'advanced-search-form',
  templateUrl: 'advancedSearchForm.component.html'
})
export class AdvancedSearchFormComponent implements OnInit, OnDestroy, OnChanges {
  @ViewChild("container") container;
  @Input() entityType;
  @Input() fieldValues;
  @Input() fieldIds: string[];
  @Input() fieldIdsMap;
  public fieldIdsOptions: Option[] = [];
  @Input() selectedFields: AdvancedField[];
  @Input() isDisabled: boolean = false;
  @Input() simpleSearchLink;
  @Input() advancedSearchLink;
  @Input() advancedSearchLinkParameters;
  @Input() simpleView: boolean = false;
  @Input() formPlaceholderText = "Type Keywords...";
  @Input() dark: boolean;
  @Output() queryChange = new EventEmitter();
  @Input() resultTypes;
  @Input() quickFilter: { filter: Filter, selected: boolean, filterId: string, value: string };
  @Input() isMobile: boolean = false;
  public disableSelect: boolean = false;
  validDateFrom: boolean = true;
  validDateTo: boolean = true;
  @Input() customFilter: SearchCustomFilter;
  newFieldId: string;
  newFieldName: string;
  fieldList: { [id: string]: any[] } = {};
  public searchFields: SearchFields = new SearchFields();
  properties: EnvProperties = properties;
  public openaireEntities = OpenaireEntities;
  public operators: string[] = this.searchFields.ADVANCED_SEARCH_OPERATORS;
  public isOperators: Option[] = [{label: 'is', value: true}, {label: 'is not', value: false}];
  public isWithinOperators: Option[] = [{label: 'is within', value: true}, {label: 'is not within', value: false}];
  public includesOperators: Option[] = [{label: 'includes', value: true}, {label: 'not includes', value: false}];
  selectedEntity;
  selectedEntitySimpleUrl;
  selectedEntityAdvancedUrl;
  @Input() entitiesSelection: boolean;
  @Input() showSwitchSearchLink: boolean = true;
  @Output() filtersClicked: EventEmitter<boolean> = new EventEmitter<boolean>();
  sub;
  
  constructor(private route: ActivatedRoute, private router: Router, private cdr: ChangeDetectorRef) {}
  
  ngOnInit() {
    this.selectedEntity = this.entityType;

    for (var i = 0; i < this.fieldIds.length; i++) {
      this.fieldList[this.fieldIds[i]] = [];
    }
    this.newFieldId = this.fieldIds[0];
    this.newFieldName = this.fieldIdsMap[this.newFieldId].name;
    this.buildFieldIdsOptions();
  }
  
  ngOnDestroy() {
    if (this.sub instanceof Subscriber) {
      this.sub.unsubscribe();
    }
  }
  
  ngOnChanges(changes: SimpleChanges) {
    if(changes.fieldIds || changes.fieldIdsMap) {
      this.buildFieldIdsOptions();
    }
  }
  
  getNotOperators(field: AdvancedField) {
    if(this.fieldIdsMap[field.id].equalityOperator.includes('exact')) {
      return this.isOperators;
    } else if(this.fieldIdsMap[field.id].equalityOperator.includes('within')) {
      return this.isWithinOperators;
    } else {
      return this.includesOperators;
    }
  }
  
  buildFieldIdsOptions() {
    this.fieldIdsOptions = [];
    this.fieldIds.forEach(id => {
      this.fieldIdsOptions.push({label: this.fieldIdsMap[id].name, value: id})
    });
  }
  
  simpleEntityChanged($event) {
    this.selectedEntity = $event.entity;
    this.selectedEntitySimpleUrl = $event.simpleUrl;
    this.selectedEntityAdvancedUrl = $event.advancedUrl;
  }
  
  simpleKeywordChanged() {
    // this.selectedFields[0].value = $event.value;
    if ((this.selectedEntity == this.entityType) || !this.entitiesSelection) {
      this.queryChanged();
    } else {
      this.router.navigate([this.selectedEntitySimpleUrl], {queryParams: {q: this.selectedFields[0].value, op: "and"}});
    }
  }
  
  queryChanged() {
    this.validDateFrom = true;
    this.validDateTo = true;
    
    this.selectedFields.forEach(selectedField => {
      if (selectedField.type == 'date') {
        if (selectedField.dateValue.type.indexOf("range") != -1) {
          if (!Dates.isValidDate(Dates.getDateToString(selectedField.dateValue.from))) {
            //console.info("INVALID: isValidDate FROM");
            this.validDateFrom = false;
          }
          if (!Dates.isValidDate(Dates.getDateToString(selectedField.dateValue.to))) {
            //console.info("INVALID: isValidDate TO");
            this.validDateTo = false;
          }
        }
      }
    });
    
    if (this.validDateFrom && this.validDateTo) {
      this.queryChange.emit({});
    }
  }
  
  disableSelectChange(value) {
    this.disableSelect = value;
    this.cdr.detectChanges();
  }
  
  addField() {
    this.newFieldId = this.fieldIds[0];
    var type = this.fieldIdsMap[this.newFieldId].type;
    if (type == "boolean") {
      this.selectedFields.push(new AdvancedField(this.newFieldId, this.fieldIdsMap[this.newFieldId].param, this.fieldIdsMap[this.newFieldId].name, type, "true", "and"));
    } else {
      this.selectedFields.push(new AdvancedField(this.newFieldId, this.fieldIdsMap[this.newFieldId].param, this.fieldIdsMap[this.newFieldId].name, type, "", "and"));
    }

    this.cdr.detectChanges();
    this.container.nativeElement.scrollTo({top: this.container.nativeElement.scrollHeight, behavior: 'smooth'});
  }
  
  removeField(index: number) {
    if (this.selectedFields.length == 1) {
      this.selectedFields[index] = new AdvancedField(this.newFieldId, this.fieldIdsMap[this.newFieldId].param, this.fieldIdsMap[this.newFieldId].name, this.fieldIdsMap[this.newFieldId].type, "", "and");
    } else {
      this.selectedFields.splice(index, 1);
    }
    //if only one filter left, set the operator to and
    if (this.selectedFields.length == 1) {
      this.selectedFields[0].operatorId = "and";
    }
  }
  
  fieldOperatorChanged(index: number, operatorId: string, operatorName: string) {
    this.selectedFields[index].operatorId = operatorId;
    this.selectedFields[index].operatorName = operatorName;
  }
  
  validateDate(index: number, value: string) {
    this.selectedFields[index].valid = Dates.isValidYear(value);
  }
  
  fieldIdsChanged(index: number, fieldId: string) {
    this.selectedFields[index].name = this.fieldIdsMap[fieldId].name;
    if(this.selectedFields[index].type !== 'keyword' || this.fieldIdsMap[fieldId].type !== 'keyword') {
      this.selectedFields[index].value = "";
    }
    this.selectedFields[index].type = this.fieldIdsMap[fieldId].type;
    this.selectedFields[index].param = this.fieldIdsMap[fieldId].param;
    if (this.fieldIdsMap[fieldId].type == "boolean") {
      this.selectedFields[index].value = "true";
    }
  }
  
  valueChanged($event, index: number) {
    this.selectedFields[index].value = $event.value;
  }
  
  updatedValueLabel($event, index: number) {
    this.selectedFields[index].valueLabel = $event.value;
  }
  
  listUpdated($event, fieldId: number) {
    this.fieldList[fieldId] = $event.value;
  }
  
  onlyAndSupported(index: number) {
    return (this.selectedFields[index] && !this.selectedFields[index].includes) || (this.selectedFields[index+1] && !this.selectedFields[index+1].includes)
  }
  
  onlyIncludesSupported(index: number) {
    return (this.selectedFields[index] && this.selectedFields[index].operatorId === 'or') || (this.selectedFields[index+1] && this.selectedFields[index+1].operatorId === 'or')
  }

  public getFieldValuesForId(id: string) {
    if(this.fieldValues[id]) {
      let obj = {};
      obj[id] = this.fieldValues[id];
      return obj;
    }
    return null;
  }
}
