import {Component, Input} from '@angular/core';

import {DateValue} from './searchHelperClasses.class';
import {UntypedFormControl} from "@angular/forms";
import { MatDatepickerInputEvent } from "@angular/material/datepicker";

@Component({
    selector: 'date-filter',
    template: `
			<div>
				<mat-select *ngIf="dateValue && dateValue.type!='range' "  name="{{'select_date_type'+filterId}}" class=" matSelection uk-input"    [(ngModel)]=dateValue.type  [disableOptionCentering]="true" panelClass="matSelectionPanel">
					<mat-option *ngFor="let type of dateValue.types let i = index"  [value]="type" (click)="typeChanged(type)">{{dateValue.typesTitle[i]}}</mat-option>
				</mat-select>
				<div *ngIf="dateValue && dateValue.type=='range' ">
					<div class=" uk-grid uk-margin-remove-left" >
						<div class="uk-padding-remove uk-margin-small-bottom uk-width-1-1">
							<mat-select name="{{'select_date_type'+filterId}}"    [(ngModel)]=dateValue.type [disableOptionCentering]="true" class="matSelection uk-input" panelClass="matSelectionPanel">
								<mat-option *ngFor="let type of dateValue.types let i = index"  [value]="type" (click)="typeChanged(type)">{{dateValue.typesTitle[i]}}</mat-option>
							</mat-select>
						</div>
						<div class="uk-padding-remove uk-margin-small-left uk-width-1-1">
							<mat-form-field class="uk-width-1-1">
								<input matInput [matDatepicker]="pickerFrom" placeholder="Choose from date" 
									[formControl]="fromDate" (click)="pickerFrom.open()" (dateChange)="fromDateChanged($event)">
								<mat-datepicker-toggle matSuffix [for]="pickerFrom"></mat-datepicker-toggle>
								<mat-datepicker #pickerFrom></mat-datepicker>
							</mat-form-field>
						</div>
						<div class="uk-padding-remove uk-margin-small-left uk-width-1-1">
							<mat-form-field class="uk-width-1-1">
								<input matInput [matDatepicker]="pickerTo" placeholder="Choose to date" 
									[formControl]="toDate" (click)="pickerTo.open()" (dateChange)="toDateChanged($event)">
								<mat-datepicker-toggle matSuffix [for]="pickerTo"></mat-datepicker-toggle>
								<mat-datepicker #pickerTo></mat-datepicker>
							</mat-form-field>
						</div>
					</div>
				</div>
			</div>
    `

})

export class DateFilterComponent {

    @Input() dateValue = new DateValue("any");
    @Input() filterId;
    @Input() validDateFrom: boolean = true;
    @Input() validDateTo: boolean = true;

      // Initialized to specific date (09.10.2018).
      public from;//: Object = { date: { year: 2018, month: 10, day: 9 } };
      public to;//: Object = { date: { year: 2018, month: 10, day: 9 } };
      public fromDate;
      public toDate;

constructor() {
}

ngOnInit() {
  this.updateDefaultRangeDates(this.dateValue.from,this.dateValue.to);
}
updateDefaultRangeDates(df:Date,dt:Date){
    this.fromDate = new UntypedFormControl(df);
    this.toDate = new UntypedFormControl(dt);

  }
typeChanged(type:string){
  this.dateValue.setDatesByType(type);
  this.updateDefaultRangeDates(this.dateValue.from, this.dateValue.to);
}


fromDateChanged(event: MatDatepickerInputEvent<Date>) {
  this.dateValue.from = event.value;
  //console.info("FROM: "+Dates.getDateToString(this.dateValue.from));
}

toDateChanged(event: MatDatepickerInputEvent<Date>) {
  this.dateValue.to = event.value;
  //console.info("TO: "+Dates.getDateToString(this.dateValue.to));
}
}
