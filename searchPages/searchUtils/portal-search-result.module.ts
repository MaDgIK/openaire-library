import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {ErrorMessagesModule} from '../../utils/errorMessages.module';
import {PortalSearchResultComponent} from "./portal-search-result.component";
import {AlertModalModule} from "../../utils/modal/alertModal.module";
import {ManageModule} from "../../utils/manage/manage.module";
import {IconsModule} from "../../utils/icons/icons.module";
import {UrlPrefixModule} from "../../utils/pipes/url-prefix.module";
import {IconsService} from "../../utils/icons/icons.service";
import {incognito, restricted} from "../../utils/icons/icons";
import {LogoUrlPipeModule} from "../../utils/pipes/logoUrlPipe.module";
import {HTMLToStringPipeModule} from "../../utils/pipes/HTMLToStringPipe.module";

@NgModule({
  imports: [
    CommonModule, FormsModule,
    RouterModule, ErrorMessagesModule,
    AlertModalModule, ManageModule, IconsModule, UrlPrefixModule, LogoUrlPipeModule, HTMLToStringPipeModule
  ],
  declarations: [
    PortalSearchResultComponent
  ],
  providers:[
  ],
  exports: [
    PortalSearchResultComponent
  ]
})
export class PortalSearchResultModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([incognito, restricted])
  }
}
