import {Filter, Value} from "./searchHelperClasses.class";
import {OpenaireEntities, SearchFields} from "../../utils/properties/searchFields";
import {properties} from "../../../../environments/environment";

export class DatasourcesHelperClass {

  public static getrefineFields(type: "all" | "registries" | "journals" | "compatible" | "deposit" | "services") {
    let searchFields:SearchFields = new SearchFields();
    if (type == "registries") {
      return searchFields.ENTITY_REGISTRIES_FIELDS;
    } else if (type == "journals") {
      return searchFields.JOURNAL_FIELDS;

    } else if (type == "compatible") {
      return searchFields.COMPATIBLE_DATAPROVIDER_FIELDS;
    } else if (type == "deposit") {
      return searchFields.DEPOSIT_DATASOURCE_REFINE_FIELDS;
    } else {
      return searchFields.DATASOURCE_REFINE_FIELDS;
    }
  }

  public static getTitle(type: "all" | "registries" | "journals" | "compatible" | "deposit" | "services") {
    if (type == "registries") {
      return "Research Entity Registries"
    } else if (type == "journals") {
      return "Journals"
    } else if (type == "compatible") {
      return "Compatible "+OpenaireEntities.DATASOURCES;
    } else if (type == "services") {
      return OpenaireEntities.SERVICES;
    } else {
      return OpenaireEntities.DATASOURCES;
    }
  }
  public static getDescription(type: "all" | "registries" | "journals" | "compatible" | "deposit" | "services") {
    if (type == "registries") {
      return ["Entity Registries","Discover research Entity Registries.","For each entity are available description and the research projects managed. Categorized by type and OpenAIRE compatibility level."];
    } else if (type == "journals") {
      return ["Research journals","Discover research Journals. ","For each entity are available description, subjects, related "+OpenaireEntities.DATASOURCES.toLowerCase()+", publications and "+OpenaireEntities.RESULTS.toLowerCase()+" per year and type."];
    } else if (type == "compatible") {
      return ["research repositories", "Discover publication, data, software, istitutional and thematic repositories.", "Available repository " + OpenaireEntities.RESULTS.toLowerCase() + " per year and type."];
    } else if (type == "services") {
      return [OpenaireEntities.SERVICES,"Discover worldwide research "+OpenaireEntities.SERVICES.toLowerCase()+" and correlated research.","Statistics data about produced "+OpenaireEntities.RESULTS.toLowerCase()+" per year available."];
    } else {
      return [OpenaireEntities.DATASOURCES,"Discover worldwide research "+OpenaireEntities.DATASOURCES.toLowerCase()+" and correlated research.","Statistics data about produced "+OpenaireEntities.RESULTS.toLowerCase()+" per year available."];
    }
  }
  public static getQueryPrefix(type: "all" | "registries" | "journals" | "compatible" | "deposit" | "services"): string {
    if (type == "registries") {
      return 'eoscdatasourcetype exact "Research Entity Registry"';
    } else if (type == "journals") {
      // return ' not(datasourcecompatibilityid = UNKNOWN) not(datasourcecompatibilityid = notCompatible) and (datasourcetypeuiid exact "pubsrepository::journal" or datasourcetypeuiid exact "aggregator::pubsrepository::journals" ) ';
      return 'oaftype exact datasource  not datasourcecompatibilityid = notCompatible and eoscdatasourcetype exact "Journal Archive"  ';

    } else if (type == "compatible") {
      return ' oaftype exact datasource  and datasourcecompatibilityid <> notCompatible  and eoscdatasourcetype exact "Repository" ';
    }else if (type == "deposit") {
      return ' oaftype exact datasource  and  (eoscdatasourcetype exact "Journal Archive"  or eoscdatasourcetype exact "Repository")';
    } else {
      return "";
    }
  }

  public static createFilters(type: "all" | "registries" | "journals" | "compatible" | "deposit" | "services"): Filter[] {
    if (type == "registries") {
      return this.createRegistriesFilters();
    } else if (type == "journals") {
      return this.createJournalFilters();
/*    } else if (type == "compatible") {
      return this.createCompatibleFilters();*/
    } else {
      return [];
    }
  }

  private static createCompatibleFilters(): Filter[] {
    var filter_names = ["Type", "Compatibility Level"];
    var filter_ids = ["datasourcetypeuiid", "datasourcecompatibilityname"];
    var searchFields = new SearchFields();
    var filter_original_ids = searchFields.COMPATIBLE_DATAPROVIDER_FIELDS;
    var value_names = [

      [
        "Institutional Repository", "Thematic Repository", "Publication Repository",
        "Institutional Repository Aggregator",
        "Thematic Repository Aggregator", "Publication Repository Aggregator",
        "Data Repository", "Data Repository Aggregator", "CRIS System", "Publication Catalogue",
        "Software Repository", "Software Repository Aggregator"],
      ["OpenAIRE Basic (DRIVER OA)", "OpenAIRE 2.0 (EC funding)", "OpenAIRE 2.0+ (DRIVER OA, EC funding)", "OpenAIRE 3.0 (OA, funding)","OpenAIRE 4.0 (inst.&thematic. repo.)", "OpenAIRE Data (funded, referenced datasets)", "OpenAIRE CRIS v1.1",
        "collected from a compatible aggregator", "proprietary", "under validation"]];
//{"englishName":"OpenAIRE 4.0 (inst.&thematic. repo.)","nativeName":"OpenAIRE 4.0 (inst.&thematic. repo.)","encoding":"OPENAIRE","code":"openaire4.0","synonyms":null}
    var value_original_ids = [
      ["pubsrepository::institutional", "pubsrepository::thematic", "pubsrepository::unknown", "aggregator::pubsrepository::institutional", "aggregator::pubsrepository::thematic", "aggregator::pubsrepository::unknown",
        "datarepository::unknown", "aggregator::datarepository", "crissystem", "pubscatalogue::unknown", "softwarerepository", "aggregator::softwarerepository"],
      //["driver","openaire2.0", "driver-openaire2.0", "openaire3.0","openaire2.0_data"]
      ["OpenAIRE Basic (DRIVER OA)", "OpenAIRE 2.0 (EC funding)", "OpenAIRE 2.0+ (DRIVER OA, EC funding)", "OpenAIRE 3.0 (OA, funding)","OpenAIRE 4.0 (inst.&thematic. repo.)", "OpenAIRE Data (funded, referenced datasets)", "OpenAIRE CRIS v1.1",
        "collected from a compatible aggregator", "proprietary", "under validation"]];
    var filters: Filter[] = [];
    for (var i = 0; i < filter_names.length; i++) {
      var values: Value[] = [];
      for (var j = 0; j < value_names[i].length; j++) {
        var value: Value = {name: value_names[i][j], id: value_original_ids[i][j], number: 0, selected: false}
        values.push(value);
      }
      var filter: Filter = {
        title: filter_names[i],
        filterId: filter_ids[i],
        originalFilterId: filter_original_ids[i],
        values: values,
        countSelectedValues: 0,
        "filterOperator": 'or',
        valueIsExact: true,
        filterType: "checkbox"
      };
      filters.push(filter);
    }
    return filters;
  }

  private static createRegistriesFilters(): Filter[] {
    var filter_names = ["Type", "Compatibility Level"];
    var filter_ids = ["datasourcetypename", "datasourcecompatibilityname"];
    var searchFields = new SearchFields();
    var filter_original_ids = searchFields.ENTITY_REGISTRIES_FIELDS;
    var value_names = [
      ["Funder", "Registry of repositories", "Scholarly Comm. Infrastructure", "Registry", "Information Space", "Web Source"],

      ["OpenAIRE Basic (DRIVER OA)", "OpenAIRE 2.0 (EC funding)", "OpenAIRE 2.0+ (DRIVER OA, EC funding)", "OpenAIRE 3.0 (OA, funding)", "OpenAIRE 4.0 (inst.&thematic. repo.)","OpenAIRE Data (funded, referenced datasets)",
        "collected from a compatible aggregator", "proprietary", "under validation"]];

    var value_original_ids = [
      ["Funder database", "Registry of repositories", "Scholarly Comm. Infrastructure", "Registry", "Information Space", "Web Source"],
      //["entityregistry::projects","entityregistry::repositories","scholarcomminfra","entityregistry","infospace","websource"],
      //["driver","openaire2.0", "driver-openaire2.0", "openaire3.0","openaire2.0_data"]
      ["OpenAIRE Basic (DRIVER OA)", "OpenAIRE 2.0 (EC funding)", "OpenAIRE 2.0+ (DRIVER OA, EC funding)", "OpenAIRE 3.0 (OA, funding)", "OpenAIRE 4.0 (inst.&thematic. repo.)", "OpenAIRE Data (funded, referenced datasets)",
        "collected from a compatible aggregator", "proprietary", "under validation"]];

    var filters: Filter[] = [];
    for (var i = 0; i < filter_names.length; i++) {
      var values: Value[] = [];
      for (var j = 0; j < value_names[i].length; j++) {
        var value: Value = {name: value_names[i][j], id: value_original_ids[i][j], number: 0, selected: false}
        values.push(value);
      }
      var filter: Filter = {
        title: filter_names[i],
        filterId: filter_ids[i],
        originalFilterId: filter_original_ids[i],
        values: values,
        countSelectedValues: 0,
        "filterOperator": 'or',
        valueIsExact: true,
        filterType: "checkbox"
      };
      filters.push(filter);
    }
    return filters;
  }


  private static createJournalFilters():Filter[] {
    var filter_names=["Type","Compatibility Level"];
    var filter_ids=["datasourcetypeuiid","datasourcecompatibilityname"];
    var searchFields = new SearchFields();
    var filter_original_ids = searchFields.JOURNAL_FIELDS;
    var value_names=[
      /*[
      "Institutional Publication Repository","Thematic Publication Repository", "Other Publication Repository",
     "Institutional Repositories Aggregators",
     "Thematic Repositories Aggregators", "Other Repositories Aggregators",
      "Data Repositories", "Data Repositories Aggregators", "Journals", "Journals Aggregators", "CRIS Systems", "Publication Catalogues"],
      */
      ["Journal", "Journal Aggregator\/Publisher"],
      ["OpenAIRE Basic (DRIVER OA)","OpenAIRE 2.0 (EC funding)", "OpenAIRE 2.0+ (DRIVER OA, EC funding)", "OpenAIRE 3.0 (OA, funding)", "OpenAIRE 4.0 (inst.&thematic. repo.)","OpenAIRE Data (funded, referenced datasets)",
        "collected from a compatible aggregator", "proprietary", "under validation"]];

    var value_original_ids=[
      ["pubsrepository::journal", "aggregator::pubsrepository::journals"],
      //["driver","openaire2.0", "driver-openaire2.0", "openaire3.0","openaire2.0_data"]
      ["OpenAIRE Basic (DRIVER OA)","OpenAIRE 2.0 (EC funding)", "OpenAIRE 2.0+ (DRIVER OA, EC funding)", "OpenAIRE 3.0 (OA, funding)", "OpenAIRE 4.0 (inst.&thematic. repo.)","OpenAIRE Data (funded, referenced datasets)",
        "collected from a compatible aggregator", "proprietary", "under validation"]];
    var filters: Filter[] =[];
    for(var i =0 ; i < filter_names.length;i++){
      var values:Value[] = [];
      for(var j =0 ; j < value_names[i].length;j++){
        var value:Value = {name: value_names[i][j], id: value_original_ids[i][j], number:0, selected:false}
        values.push(value);
      }
      var filter:Filter = {title: filter_names[i], filterId: filter_ids[i], originalFilterId:  filter_original_ids[i], values : values, countSelectedValues:0, "filterOperator": 'or', valueIsExact: true, filterType: "checkbox" };
      filters.push(filter);
    }
    return filters;
  }


}
