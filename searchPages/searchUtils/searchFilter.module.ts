import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {SearchFilterComponent} from './searchFilter.component';
import {SearchFilterModalComponent} from './searchFilterModal.component';
import {ModalModule} from '../../utils/modal/modal.module';
import {RouterModule} from "@angular/router";
import {InputModule} from '../../sharedComponents/input/input.module';
import {IconsModule} from "../../utils/icons/icons.module";
import {LoadingModule} from "../../utils/loading/loading.module";
import {MatButtonToggleModule} from "@angular/material/button-toggle";

@NgModule({
  imports: [
    CommonModule, FormsModule, ModalModule, RouterModule,
    InputModule, IconsModule, LoadingModule, MatButtonToggleModule
  ],
  declarations: [
    SearchFilterComponent, SearchFilterModalComponent
  ],
  exports: [
    SearchFilterComponent, SearchFilterModalComponent
  ]
})
export class SearchFilterModule {
}
