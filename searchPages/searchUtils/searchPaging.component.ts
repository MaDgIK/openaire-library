import {Component, Input} from '@angular/core';
import {ErrorCodes} from '../../utils/properties/errorCodes';

@Component({
  selector: 'search-paging',
  template: `
    <paging *ngIf="results && searchUtils.totalResults > searchUtils.size" [position]="isMobile?'center':'right'"
            [currentPage]="searchUtils.page" [totalResults]="searchUtils.totalResults" [baseUrl]="baseUrl"
            [size]="searchUtils.size" [parameterNames]="parameterNames" [parameterValues]="parameterValues"
            [isDisabled]="isDisabled">
    </paging>
    <paging *ngIf="!loadPaging && oldTotalResults > searchUtils.size && searchUtils.status == errorCodes.LOADING"
            [currentPage]="searchUtils.page" [totalResults]="oldTotalResults" [baseUrl]="baseUrl" [position]="isMobile?'center':'right'"
            [size]="searchUtils.size" [parameterNames]="parameterNames" [parameterValues]="parameterValues"
            [isDisabled]="isDisabled">
    </paging>
  `
})
export class SearchPagingComponent {
  @Input() isDisabled: boolean = false;
  @Input() searchUtils;
  @Input() results;
  @Input() baseUrl;
  @Input() type;
  @Input() parameterNames: string[];
  @Input() parameterValues: string[];
  @Input() loadPaging: boolean = true;
  @Input() oldTotalResults: number = 0;
  @Input() isMobile: boolean = false;
  
  public totalResults: number = 0;
  public errorCodes: ErrorCodes = new ErrorCodes();
  
  constructor() {
  }
}
