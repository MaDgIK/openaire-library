import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {NewSearchPageModule} from "./searchUtils/newSearchPage.module";
import {OrganizationsServiceModule} from "../services/organizationsService.module";
import {SearchOrganizationsComponent} from "./searchOrganizations.component";

@NgModule({
  imports: [
    CommonModule, FormsModule,
    OrganizationsServiceModule,
    NewSearchPageModule

  ],
  declarations: [
    SearchOrganizationsComponent
  ],
  providers: [],
  exports: [
    SearchOrganizationsComponent
  ]
})

export class SearchOrganizationsModule { }
