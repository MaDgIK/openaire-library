import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {NewSearchPageModule} from "./searchUtils/newSearchPage.module";
import {DataProvidersServiceModule} from "../services/dataProvidersService.module";
import {SearchDataProvidersComponent} from "./searchDataProviders.component";

@NgModule({
  imports: [
    CommonModule, FormsModule,
    DataProvidersServiceModule,
    NewSearchPageModule

  ],
  declarations: [
    SearchDataProvidersComponent
  ],
  providers: [],
  exports: [
    SearchDataProvidersComponent
  ]
})

export class SearchDataProvidersModule { }
