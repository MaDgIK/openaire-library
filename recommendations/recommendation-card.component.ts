import {Component, Input} from "@angular/core";
import {EnvProperties} from "../utils/properties/env-properties";
import {properties} from "../../../environments/environment";
import {RecommendationsService} from "./recommendations.service";
import {User} from "../login/utils/helper.class";

@Component({
  selector: 'recommendation-card',
  template: `
    <a [routerLink]="properties.searchLinkToResult.split('?')[0]" [queryParams]="{'id': result.result_id}"
      (click)="category && result && community ? selectResult(category, result, community) : ''" class="uk-link-reset" target="_blank">
      <div class="uk-card uk-card-default uk-card-hover uk-text-xsmall uk-padding-small">
        <ng-container *ngIf="result.result_title">
          <div class="multi-line-ellipsis lines-2">
            <span class="uk-text-small uk-text-bold">{{result.result_title}}</span>
          </div>
          <hr *ngIf="result.result_publisher || result.result_publication_date || result.result_country || result.result_authors">
        </ng-container>
        <div *ngIf="result.result_publisher" class="uk-text-truncate"><span class="uk-text-meta">Publisher:</span> {{result.result_publisher}}</div>
        <div *ngIf="result.result_publication_date" class="uk-text-truncate"><span class="uk-text-meta">Date:</span> {{result.result_publication_date}}</div>
        <div *ngIf="result.result_country" class="uk-text-truncate"><span class="uk-text-meta">Country:</span> {{result.result_country}}</div>
        <div *ngIf="result.result_authors" class="uk-text-truncate"><span class="uk-text-meta">Authors:</span> {{result.result_authors}}</div>
      </div>
    </a>
  `
})

export class RecommendationCardComponent {
  private subscriptions = [];
  properties: EnvProperties = properties;

  @Input() result;
  @Input() category;
  @Input() community;
  @Input() user: User;

  constructor(private recommendationsService: RecommendationsService) {

  }

  selectResult(item, result, community) {
    this.subscriptions.push(this.recommendationsService.giveFeedbackForRecommendation(this.properties.feedbackForRecommendationAPI, community, item.field, result.result_id, this.user ? this.user.id : null).subscribe(data => {
      // console.log(data);
    }));
  }
}