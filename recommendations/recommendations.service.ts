import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Recommendation} from "./recommendation";
import {map} from "rxjs/operators";

@Injectable()

export class RecommendationsService {
  constructor(private http: HttpClient) {
  }

  getAvailableCommunities(url: string) {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.get(url, options);
  }

  getRecommendationsForCommunity(url: string, communityId: string, user: string) {
    let body = {};
    if(user) {
      body = {
        "community": communityId,
        "top_k_publications": 6,
        "user": user
      };
    } else {
      body = {
        "community": communityId,
        "top_k_publications": 6,
      };
    }
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post(url, body, options);
  }

  giveFeedbackForRecommendation(url: string, communityId: string, category: string, id: string, user: string) {
    let body = {};
    if(user) {
      body = {
        "category": category,
        "community": communityId,
        "publication": id,
        "user": user
        // "user": "1823219319238u1923441237"
      }
    } else {
      body = {
        "category": category,
        "community": communityId,
        "publication": id,
      }
    }
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post(url, body, options);
  }

  getRecommendationsForPublication(url: string, id: string) {
    const body = {
      "num": 6,
      "result_id": id
      // "result_id": "doi_dedup___::b0c1c7d86521ca2cce50610e8f61fa04"
    };
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post(url, body, options);
  }

  getRecommendationsForOrcid(url: string, id: string) {
    const body = {
      "author_id": id,

      // the one below returns 500 internal server error
      // "author_id": "0009-0003-3633-2226"
    }
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post(url, body, options);
  }

  // parseRecommendations(data: any): Recommendation[] {
  //   const recommendations: Recommendation[] = [];
  //   const length = Array.isArray(data) ? data.length : 1;

  //   for (let i = 0; i < length; i++) {
  //     const resData = Array.isArray(data) ? data[i] : data;
  //     const recommendation: Recommendation = new Recommendation();
  //     recommendation['id'] = resData.result_id;
  //     recommendation['title'] = resData.result_title;
  //     recommendation['country'] = resData.result_country;
  //     recommendation['authors'] = resData.result_authors;
  //     recommendation['publisher'] = resData.result_publisher;
  //     recommendation['date'] = resData.result_publication_date;
  //     recommendations.push(recommendation);
  //   }
    
  //   return recommendations;
  // }
}