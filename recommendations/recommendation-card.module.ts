import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {RecommendationCardComponent} from "./recommendation-card.component";
import {RouterModule} from "@angular/router";
import {RecommendationsService} from "./recommendations.service";

@NgModule({
  imports: [
    CommonModule, RouterModule
  ],
  declarations: [
    RecommendationCardComponent
  ],
  providers: [
    RecommendationsService
  ],
  exports: [
    RecommendationCardComponent
  ]
})

export class RecommendationCardModule {
  
}