export class Recommendation {
  id: string;
  title: string;
  country: string;
  authors: string;
  publisher: string;
  date: string;
}