import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {PreviousRouteRecorder} from '../../utils/piwik/previousRouteRecorder.guard';
import {LoginGuard} from "../../login/loginGuard.guard";
import {MyStakeholdersComponent} from "./my-stakeholders.component";

@NgModule({
  imports: [
    RouterModule.forChild([
      {path: '', component: MyStakeholdersComponent, canActivate: [LoginGuard], canDeactivate: [PreviousRouteRecorder]}

    ])
  ]
})
export class MyStakeholdersRoutingModule {
}
