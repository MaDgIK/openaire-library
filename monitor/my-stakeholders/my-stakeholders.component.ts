import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {Meta, Title} from '@angular/platform-browser';
import {EnvProperties} from '../../utils/properties/env-properties';

import {PiwikService} from '../../utils/piwik/piwik.service';
import {User} from '../../login/utils/helper.class';
import {StringUtils} from '../../utils/string-utils.class';

import {ErrorCodes} from '../../utils/properties/errorCodes';
import {ErrorMessagesComponent} from '../../utils/errorMessages.component';
import {SEOService} from "../../sharedComponents/SEO/SEO.service";
import {UserManagementService} from "../../services/user-management.service";
import {properties} from "../../../../environments/environment";
import {StakeholderService} from "../services/stakeholder.service";
import {StakeholderInfo} from "../entities/stakeholder";
import {StakeholderBaseComponent} from "../../monitor-admin/utils/stakeholder-base.component";

/**
 * @deprecated
 * */
@Component({
  selector: 'my-stakeholders',
  templateUrl: 'my-stakeholders.component.html',
})
export class MyStakeholdersComponent extends StakeholderBaseComponent {
  public pageTitle = "OpenAIRE | My " + this.stakeholderUtils.entities.stakeholders;
  public description = "OpenAIRE - Monitor, A new era of monitoring research. Open data. Open methodologies | My managing and member of " + this.stakeholderUtils.entities.stakeholders;
  public stakeholders: StakeholderInfo[] = [];
  public pageContents = null;
  public divContents = null;
  // Message variables
  public status: number;
  public loading: boolean = true;
  public subscriberErrorMessage: string = "";
  public errorCodes: ErrorCodes;
  private errorMessages: ErrorMessagesComponent;
  public properties: EnvProperties = properties;
  private user: User;

  constructor(
      protected _router: Router,
      protected _meta: Meta,
      protected _title: Title,
      protected seoService: SEOService,
      protected _piwikService: PiwikService,
      private stakeholderService: StakeholderService,
      private userManagementService: UserManagementService) {
    super();
    this.errorCodes = new ErrorCodes();
    this.errorMessages = new ErrorMessagesComponent();
    this.status = this.errorCodes.LOADING;
  }

  public ngOnInit() {
    this.setMetadata();
    this.subscriptions.push(this.userManagementService.getUserInfo().subscribe(user => {
      this.user = user;
      if (this.user) {
        this.getStakeholders();
      }
    }));

  }

  get manager(): StakeholderInfo[] {
    return (this.stakeholders) ? this.stakeholders.filter(stakeholder => stakeholder.isManager) : [];
  }

  get member(): StakeholderInfo[] {
    return (this.stakeholders) ? this.stakeholders.filter(stakeholder => stakeholder.isMember) : [];
  }

  public getStakeholders() {
    this.loading = true;
    this.status = this.errorCodes.LOADING;
    this.subscriberErrorMessage = "";
    this.stakeholders = [];
    this.subscriptions.push(this.stakeholderService.getMyStakeholders().subscribe(
        stakeholders => {
          this.stakeholders = StakeholderInfo.toStakeholderInfo(stakeholders.standalone.concat(stakeholders.umbrella), this.user);
          this.sort(this.stakeholders);
          this.loading = false;
        },
        error => {
          this.status = this.handleError("Error getting stakeholders", error);
          this.loading = false;
        }
    ));
  }

  private sort(results: StakeholderInfo[]) {
    results.sort((left, right): number => {
      if (left.name > right.name) {
        return 1;
      } else if (left.name < right.name) {
        return -1;
      } else {
        return 0;
      }
    })
  }

  public quote(param: string): string {
    return StringUtils.quote(param);
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }

  private handleError(message: string, error): number {
    let code = "";
    if (!error.status) {
      code = error.code;
    } else {
      code = error.status;
    }
    console.error("Communities (component): " + message, error);
    return this.errorMessages.getErrorCode(code);
  }
}
