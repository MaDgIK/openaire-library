import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {ManageModule} from '../../utils/manage/manage.module';
import {PreviousRouteRecorder} from '../../utils/piwik/previousRouteRecorder.guard';

import {PiwikService} from '../../utils/piwik/piwik.service';
import {ErrorMessagesModule} from '../../utils/errorMessages.module';
import {Schema2jsonldModule} from "../../sharedComponents/schema2jsonld/schema2jsonld.module";
import {SEOServiceModule} from "../../sharedComponents/SEO/SEOService.module";
import {LoginGuard} from "../../login/loginGuard.guard";
import {HelperModule} from "../../utils/helper/helper.module";
import {BreadcrumbsModule} from "../../utils/breadcrumbs/breadcrumbs.module";
import {MyStakeholdersRoutingModule} from "./my-stakeholders-routing.module";
import {MyStakeholdersComponent} from "./my-stakeholders.component";
import {LoadingModule} from "../../utils/loading/loading.module";
import {BrowseStakeholderModule} from "../browse-stakeholder/browse-stakeholder.module";

@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule, HelperModule,
    ManageModule, ErrorMessagesModule, Schema2jsonldModule, SEOServiceModule,
    MyStakeholdersRoutingModule, BreadcrumbsModule, LoadingModule, BrowseStakeholderModule
  ],
  declarations: [
    MyStakeholdersComponent
  ],
  providers: [
    LoginGuard, PreviousRouteRecorder, PiwikService
  ],
  exports: [
    MyStakeholdersComponent
  ]
})
export class MyStakeholdersModule {
}
