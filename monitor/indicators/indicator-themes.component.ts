import {Component, OnDestroy, OnInit} from "@angular/core";
import {properties} from "../../../../environments/environment";
import {ActivatedRoute, Router} from "@angular/router";
import {Meta, Title} from "@angular/platform-browser";
import {SEOService} from "../../sharedComponents/SEO/SEO.service";
import {Breadcrumb} from "../../utils/breadcrumbs/breadcrumbs.component";
import {Subscriber} from "rxjs";
import {StakeholderBaseComponent} from "../../monitor-admin/utils/stakeholder-base.component";

@Component({
  selector: 'indicator-themes-page',
  template: `
		<div class="uk-visible@m">
			<div class="uk-container uk-container-large uk-section uk-section-small uk-padding-remove-bottom">
				<div class="uk-padding-small uk-padding-remove-horizontal">
					<breadcrumbs [breadcrumbs]="breadcrumbs"></breadcrumbs>
				</div>
			</div>
			<div class="uk-container uk-container-large uk-section">
				<h1>Indicator Themes<span class="uk-text-primary">.</span></h1>
				<div class="uk-section">
					<div class="uk-grid uk-grid-large uk-flex-middle" uk-grid>
						<div class="uk-width-3-5">
							<img src="assets/common-assets/monitor-assets/indicator-themes-circle.png">
						</div>
						<div class="uk-width-expand">
							<div>
								<h5>Indicator themes that we are covering in the Monitor dashboards.</h5>
								<p>
									This is the current set of indicator themes we cover. We’ll keep enriching it as new requests and data are coming into the <a href="https://graph.openaire.eu" class="text-graph" target="_blank">OpenAIRE Graph</a>. We are at your disposal, should you have any recommendations!
								</p>
								<p>
									Check out the indicator pages (for <a [routerLink]="['../funder']" [relativeTo]="_route">funders</a>,
									<a [routerLink]="['../organization']" [relativeTo]="_route">research institutions</a> and
									<a [routerLink]="['../ri']" [relativeTo]="_route">research initiatives</a>)
									for the specific indicators for each type of dashboard, and the  <a [routerLink]="['../../methodology']" [relativeTo]="_route">methodology and terminology</a> page on how we produce the metrics.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="uk-hidden@m">
			<div class="uk-section uk-container uk-container-large">
				<h1 class="uk-heading-small uk-text-center uk-padding-small uk-padding-remove-vertical">Indicator Themes<span class="uk-text-primary">.</span></h1>
				<div class="uk-text-center uk-margin-large-top">
					<div class="uk-padding-small uk-padding-remove-vertical">
						<img src="assets/common-assets/monitor-assets/indicator-themes-circle.png">
					</div>
					<div>
						<h5 class="uk-margin-large-top uk-margin-large-bottom">Indicator themes that we are covering in the Monitor dashboards.</h5>
						<p>
							This is the current set of indicator themes we cover. We’ll keep enriching it as new requests and data are coming into the <a href="https://graph.openaire.eu" class="text-graph" target="_blank">OpenAIRE Graph</a>. We are at your disposal, should you have any recommendations!
						</p>
						<p>
							Check out the indicator pages (for <a [routerLink]="['../funder']" [relativeTo]="_route" class="uk-text-lowercase">{{entities.funders}}</a>,
							<a [routerLink]="['../organization']" [relativeTo]="_route" class="uk-text-lowercase">{{entities.organizations}}</a> and
							<a [routerLink]="['../ri']" [relativeTo]="_route" class="uk-text-lowercase">{{entities.ris}}</a>)
							for the specific indicators for each type of dashboard, and the  <a [routerLink]="['../../methodology']" [relativeTo]="_route">methodology and terminology</a> page on how we produce the metrics.
						</p>
					</div>
				</div>
			</div>
		</div>
  `
})
export class IndicatorThemesComponent extends StakeholderBaseComponent implements OnInit {
  public breadcrumbs: Breadcrumb[] = [{name: 'home', route: '/'}, {name: 'Resources - Themes'}];
  
  constructor(protected _router: Router,
              protected _meta: Meta,
              protected _title: Title,
              protected seoService: SEOService,
              protected _route: ActivatedRoute) {
    super();
  }
  
  ngOnInit() {
    this.subscriptions.push(this._route.params.subscribe(params => {
      this.title = "Monitor | Indicator Themes";
      this.description = "Monitor | Indicator Themes";
      this.setMetadata();
      this.breadcrumbs[0].route = '/' + (params['stakeholder']?params['stakeholder']:'');
      this.breadcrumbs[0].name = (params['stakeholder']?'dashboard':'home');
    }));
  }
}
