import {Component, OnDestroy, OnInit} from "@angular/core";
import {Subscriber} from "rxjs";
import {properties} from "../../../../environments/environment";
import {ActivatedRoute, Router} from "@angular/router";
import {HelperService} from "../../utils/helper/helper.service";
import {Meta, Title} from "@angular/platform-browser";
import {SEOService} from "../../sharedComponents/SEO/SEO.service";
import {ResourcesService} from "../services/resources.service";
import {Breadcrumb} from "../../utils/breadcrumbs/breadcrumbs.component";

@Component({
  selector: 'indicators-page',
  template: `
    <div class="uk-visible@m uk-container uk-container-large uk-section uk-section-small uk-padding-remove-bottom">
      <div class="uk-padding-small uk-padding-remove-horizontal">
        <breadcrumbs [breadcrumbs]="breadcrumbs"></breadcrumbs>
      </div>
    </div>
    <helper *ngIf="pageContents && pageContents['top'] && pageContents['top'].length > 0" [texts]="pageContents['top']"></helper>
  `
})
export class IndicatorsComponent implements OnInit, OnDestroy {
  private subscriptions: any[] = [];
  public types = ResourcesService.types;
  public properties = properties;
  public pageContents;
  public breadcrumbs: Breadcrumb[] = [{name: 'home', route: '/'}, {name: 'Resources'}];
  
  constructor(private helper: HelperService,
              private resourcesService: ResourcesService,
              private router: Router,
              private meta: Meta,
              private title: Title,
              private seoService: SEOService,
              private route: ActivatedRoute) {
  }
  
  ngOnInit() {
    this.subscriptions.push(this.route.params.subscribe(params => {
      if(params['type']) {
        let type = this.types.find(type => type.value === params['type']);
        if(type) {
          this.getPageContents(type.value);
          const description = "Monitor | Indicators for " + type.label;
          const title = "Monitor | Indicators for " + type.label;
          this.metaTags(title, description);
					this.breadcrumbs[1] = {name: 'Resources - ' + type.label}
        }
      }
      this.breadcrumbs[0].route = '/' + (params['stakeholder']?params['stakeholder']:'');
      this.breadcrumbs[0].name = (params['stakeholder']?'dashboard':'home');
    }));
  }
  
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscriber) {
        subscription.unsubscribe();
      }
    });
  }
  
  metaTags(title, description) {
    const url = properties.domain + properties.baseLink + this.router.url;
    this.seoService.createLinkForCanonicalURL(url, false);
    this.meta.updateTag({content: url}, "property='og:url'");
    this.meta.updateTag({content: description}, "name='description'");
    this.meta.updateTag({content: description}, "property='og:description'");
    this.meta.updateTag({content: title}, "property='og:title'");
    this.title.setTitle(title);
  }
  
  public getPageContents(type: string) {
    this.subscriptions.push(this.resourcesService.isPagesEnabled().subscribe(status => {
      let index = this.types.findIndex(t => t.value === type);
      if(index !== -1 && status[index]) {
        this.subscriptions.push(this.helper.getPageHelpContents(this.properties, 'monitor', '/indicators/' + type, 'monitor').subscribe(contents => {
          this.pageContents = contents;
        }));
      } else {
        this.navigateToError();
      }
    }));
  }
  
  private navigateToError() {
    this.router.navigate([this.properties.errorLink], {queryParams: {'page': this.properties.baseLink + this.router.url}});
  }
}
