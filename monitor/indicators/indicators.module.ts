import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {IndicatorsComponent} from "./indicators.component";
import {PreviousRouteRecorder} from "../../utils/piwik/previousRouteRecorder.guard";
import {HelperModule} from "../../utils/helper/helper.module";
import {IndicatorThemesComponent} from "./indicator-themes.component";
import {BreadcrumbsModule} from "../../utils/breadcrumbs/breadcrumbs.module";

@NgModule({
  imports: [CommonModule, RouterModule.forChild([
    {
      path: 'themes',
      component: IndicatorThemesComponent,
      canDeactivate: [PreviousRouteRecorder]
    },
    {
      path: ':type',
      component: IndicatorsComponent,
      canDeactivate: [PreviousRouteRecorder]
    },
  ]), HelperModule, BreadcrumbsModule],
  declarations: [IndicatorsComponent, IndicatorThemesComponent],
  exports: [IndicatorsComponent, IndicatorThemesComponent]
})
export class IndicatorsModule {}
