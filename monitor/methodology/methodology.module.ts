import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {TerminologyComponent} from "./terminology.component";
import {MethodolocigalApproachComponent} from "./methodological-approach.component";
import {RouterModule} from "@angular/router";
import {PreviousRouteRecorder} from "../../utils/piwik/previousRouteRecorder.guard";
import {PageContentModule} from "../../dashboard/sharedComponents/page-content/page-content.module";
import {IconsModule} from "../../utils/icons/icons.module";
import {BreadcrumbsModule} from "../../utils/breadcrumbs/breadcrumbs.module";
import {SliderTabsModule} from "../../sharedComponents/tabs/slider-tabs.module";
import {HelperModule} from "../../utils/helper/helper.module";

@NgModule({
  declarations: [TerminologyComponent, MethodolocigalApproachComponent],
  imports: [CommonModule, RouterModule.forChild([
    {
      path: '',
      redirectTo: 'terminology',
      pathMatch: 'full',
      canDeactivate: [PreviousRouteRecorder]
    },
    {
      path: 'terminology',
      component: TerminologyComponent,
      canDeactivate: [PreviousRouteRecorder]
    },
		{
      path: 'methodological-approach',
      component: MethodolocigalApproachComponent,
			canDeactivate: [PreviousRouteRecorder]
    }
  ]), PageContentModule, IconsModule, BreadcrumbsModule, SliderTabsModule, HelperModule],
  exports: [TerminologyComponent, MethodolocigalApproachComponent]
})
export class MethodologyModule {
}
