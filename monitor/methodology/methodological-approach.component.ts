import {Component, OnDestroy, OnInit} from "@angular/core";
import {Subscription} from "rxjs";
import {Meta, Title} from "@angular/platform-browser";
import {ActivatedRoute, Router} from "@angular/router";
import {Stakeholder} from "../entities/stakeholder";
import {OpenaireEntities} from "../../utils/properties/searchFields";
import {SEOService} from "../../sharedComponents/SEO/SEO.service";
import {properties} from "../../../../environments/environment";
import {Breadcrumb} from "../../utils/breadcrumbs/breadcrumbs.component";

@Component({
  selector: 'methodological-approach',
  template: `
		<div class="uk-visible@m">
			<div class="uk-container uk-container-large uk-section uk-section-small uk-padding-remove-bottom">
				<div class="uk-padding-small uk-padding-remove-horizontal">
					<breadcrumbs [breadcrumbs]="breadcrumbs"></breadcrumbs>
				</div>
			</div>
			<div class="uk-section" uk-scrollspy="target: [uk-scrollspy-class]; cls: uk-animation-fade; delay: 250">
				<div id="how" class="uk-container uk-container-large">
					<h1 class="uk-h1" uk-scrollspy-class>
						Inclusion, transparency, <br> quality, state of the art <br> technology<span class="uk-text-primary">.</span>
					</h1>
					<div class="uk-padding uk-padding-remove-vertical">
						<div class="uk-margin-large-top uk-card uk-card-default uk-card-body" uk-scrollspy-class>
							<p class="uk-margin-top">Our methodological approach is based on the following operational quality
								criteria:</p>
							<ul>
								<li><span class="uk-text-bold">Openness and transparency:</span> Methodological assumptions are openly and
									clearly presented.
								</li>
								<li><span class="uk-text-bold">Coverage and accuracy:</span> As detailed in <a
										href="https://graph.openaire.eu/" target="_blank">graph.openaire.eu</a>
									multiple data sources are ingested in the OpenAIRE Graph for coverage to the fullest extent
									possible, in order to provide meaningful indicators.
								</li>
								<li><span class="uk-text-bold">Clarity and replicability:</span> We describe our construction methodology in
									detail, so that
									it can be verified and used by the scholarly communication community to create ongoing updates to our
									proposed statistics and indicators.
								</li>
								<li><span class="uk-text-bold">Readiness and timeliness:</span> The methodology is built around
									well-established open databases
									and already tested knowledge extraction technologies - natural language processing (NLP)/machine-learning
									(ML) - using operational
									workflows in OpenAIRE to warrant timely results.
								</li>
								<li><span class="uk-text-bold">Trust and robustness:</span> Our methodology also strives to be reliable,
									robust, and aligned
									to other assessment methods so that it can be operationalized, used and reused, in conjunction with other
									assessment methods.
								</li>
							</ul>
							<div class="uk-text-small uk-text-italic uk-text-right">The text above is modified from <a
									href="https://op.europa.eu/en/publication-detail/-/publication/56cc104f-0ebb-11ec-b771-01aa75ed71a1"
									target="_blank">this report</a> (DOI: 10.2777/268348).
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="uk-background-muted uk-margin-large-top">
				<div class="uk-section uk-section-large uk-container uk-container-large">
					<div class="uk-grid uk-child-width-1-2@m" uk-grid>
						<div>
							<div style="max-width: 575px;">
								<span class="uk-text-large text-graph">Completeness, inclusion, transparency and replicability</span>
								<h2 class="uk-margin-small-top">How? It’s about open data and collaboration<span class="text-graph">.</span></h2>
								<ul class="uk-list uk-list-bullet uk-margin-large-top">
									<li class="uk-flex uk-flex-column uk-margin-medium-bottom">
										<span class="uk-text-large uk-margin-small-bottom">Built on the <span class="uk-text-bold">OpenAire Graph</span></span>
										<span>Linked scholarly information from open initiatives around the world. Beyond publications.</span>
									</li>
									<li class="uk-flex uk-flex-column uk-margin-medium-bottom">
										<span class="uk-text-large uk-margin-small-bottom">Based on <span class="uk-text-bold">Open Science principles</span></span>
										<span>Open data sources, Open APIs, well documented metrics and indicators.</span>
									</li>
									<li class="uk-flex uk-flex-column">
										<span class="uk-text-large uk-margin-small-bottom">Relevance for the <span class="uk-text-bold">Community</span></span>
										<span>Indicators development and visualizations to meet community requirements.</span>
									</li>
								</ul>
							</div>
						</div>
						<div class="uk-position-relative">
							<img class="uk-visible@m uk-height-1-1 uk-position-center-right" src="assets/common-assets/common/graph-nodes.svg" alt="OpenAIRE Graph" loading="lazy">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="uk-hidden@m">
			<div class="uk-section" uk-scrollspy="target: [uk-scrollspy-class]; cls: uk-animation-fade; delay: 250">
				<div class="uk-container uk-container-large">
					<h1 class="uk-heading-small uk-text-center uk-padding-small uk-padding-remove-vertical" uk-scrollspy-class>
						Inclusion, transparency, <br> quality, state of the art <br> technology<span class="uk-text-primary">.</span>
					</h1>
					<div class="uk-margin-xlarge-top" uk-scrollspy-class>
						<p>Our <span class="uk-text-primary">methodological approach</span> is based on the following operational quality
						<span class="uk-text-primary">criteria</span>:</p>
						<ul>
							<li class="uk-margin-small-bottom"><span class="uk-text-bold">Openness and transparency:</span> Methodological assumptions are openly and
								clearly presented.
							</li>
							<li class="uk-margin-small-bottom"><span class="uk-text-bold">Coverage and accuracy:</span> As detailed in <a
									href="https://graph.openaire.eu/" target="_blank">graph.openaire.eu</a>
								multiple data sources are ingested in the OpenAIRE Graph for coverage to the fullest extent
								possible, in order to provide meaningful indicators.
							</li>
							<li class="uk-margin-small-bottom"><span class="uk-text-bold">Clarity and replicability:</span> We describe our construction methodology in
								detail, so that
								it can be verified and used by the scholarly communication community to create ongoing updates to our
								proposed statistics and indicators.
							</li>
							<li class="uk-margin-small-bottom"><span class="uk-text-bold">Readiness and timeliness:</span> The methodology is built around
								well-established open databases
								and already tested knowledge extraction technologies - natural language processing (NLP)/machine-learning
								(ML) - using operational
								workflows in OpenAIRE to warrant timely results.
							</li>
							<li><span class="uk-text-bold">Trust and robustness:</span> Our methodology also strives to be reliable,
								robust, and aligned
								to other assessment methods so that it can be operationalized, used and reused, in conjunction with other
								assessment methods.
							</li>
						</ul>
						<div class="uk-text-small uk-text-italic uk-text-muted">The text above is modified from <a
								href="https://op.europa.eu/en/publication-detail/-/publication/56cc104f-0ebb-11ec-b771-01aa75ed71a1"
								target="_blank">this report</a> (DOI: 10.2777/268348).
						</div>
					</div>
					<div class="uk-margin-xlarge-top uk-padding-large">
						<img src="assets/common-assets/common/graph-nodes.svg" alt="OpenAIRE Graph" loading="lazy">
					</div>
					<div class="uk-margin-top uk-margin-large-bottom">
						<span class="uk-text-large text-graph">Completeness, inclusion, transparency and replicability</span>
						<h2 class="uk-margin-small-top uk-margin-remove-bottom">How?</h2>
						<h2 class="uk-margin-remove-top uk-padding uk-padding-remove-vertical uk-padding-remove-left">It’s about open data and collaboration<span class="text-graph">.</span></h2>
						<ul class="uk-list uk-list-bullet uk-margin-large-top">
							<li class="uk-flex uk-flex-column uk-margin-medium-bottom">
								<span class="uk-text-large uk-margin-small-bottom">Built on the <span class="uk-text-bold">OpenAire Graph</span></span>
								<span>Linked scholarly information from open initiatives around the world. Beyond publications.</span>
							</li>
							<li class="uk-flex uk-flex-column uk-margin-medium-bottom">
								<span class="uk-text-large uk-margin-small-bottom">Based on <span class="uk-text-bold">Open Science principles</span></span>
								<span>Open data sources, Open APIs, well documented metrics and indicators.</span>
							</li>
							<li class="uk-flex uk-flex-column">
								<span class="uk-text-large uk-margin-small-bottom">Relevance for the <span class="uk-text-bold">Community</span></span>
								<span>Indicators development and visualizations to meet community requirements.</span>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- <div class="uk-section uk-section-large uk-container uk-container-large">
			TODO: Graph scheme
		</div> -->
  `
})
export class MethodolocigalApproachComponent implements OnInit, OnDestroy {
  public stakeholder: Stakeholder;
  public tab: 'entities' | 'attributes' = 'entities';
  private subscriptions: any[] = [];
  public openaireEntities = OpenaireEntities;
  public breadcrumbs: Breadcrumb[] = [{name: 'home', route: '/'}, {name: 'Resources - Methodological approach', keepFormat: true}];
  
  constructor(private seoService: SEOService,
              private meta: Meta,
              private router: Router,
              private route: ActivatedRoute,
              private title: Title) {
  }
  
  ngOnInit() {
    this.subscriptions.push(this.route.params.subscribe(params => {
      const description = "Monitor | Methodological approach";
      const title = "Monitor | Methodological approach";
      this.metaTags(title, description);
      this.breadcrumbs[0].route = '/' + (params['stakeholder']?params['stakeholder']:'');
      this.breadcrumbs[0].name = (params['stakeholder']?'dashboard':'home');
    }));
  }
  
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscription) {
        subscription.unsubscribe();
      }
    });
  }
  
  metaTags(title, description) {
    const url = properties.domain + properties.baseLink + this.router.url;
    this.seoService.createLinkForCanonicalURL(url, false);
    this.meta.updateTag({content: url}, "property='og:url'");
    this.meta.updateTag({content: description}, "name='description'");
    this.meta.updateTag({content: description}, "property='og:description'");
    this.meta.updateTag({content: title}, "property='og:title'");
    this.title.setTitle(title);
  }
}
