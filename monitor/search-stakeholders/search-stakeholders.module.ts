import {NgModule} from "@angular/core";
import {CommonModule, TitleCasePipe} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {SearchStakeholdersComponent} from "./search-stakeholders.component";
import {SearchFormModule} from "../../searchPages/searchUtils/searchForm.module";
import {SearchStakeholdersRoutingModule} from "./search-stakeholders-routing.module";
import {PreviousRouteRecorder} from "../../utils/piwik/previousRouteRecorder.guard";
import {NewSearchPageModule} from "../../searchPages/searchUtils/newSearchPage.module";

@NgModule({
  imports: [
    CommonModule, FormsModule,
    SearchFormModule,
    SearchStakeholdersRoutingModule, NewSearchPageModule
  ],
  declarations: [
    SearchStakeholdersComponent
  ],
  providers:[ PreviousRouteRecorder, TitleCasePipe],
  exports: [
    SearchStakeholdersComponent
  ]
})
export class SearchStakeholdersModule { }
