import {Component, Input, ViewChild} from '@angular/core';
import {StakeholderInfo, Visibility} from "../entities/stakeholder";
import {LocalStorageService} from "../../services/localStorage.service";
import {StringUtils} from "../../utils/string-utils.class";
import {LayoutService} from "../../dashboard/sharedComponents/sidebar/layout.service";
import {AlertModal} from "../../utils/modal/alert";
import {BaseComponent} from "../../sharedComponents/base/base.component";

@Component({
  selector: 'browse-stakeholder',
  templateUrl: 'browse-stakeholder.component.html',
  styleUrls: ['browse-stakeholder.component.less']
})
export class BrowseStakeholderComponent extends BaseComponent {
  @Input() public stakeholder: StakeholderInfo = null;
  @ViewChild('AlertModal', { static: true }) modal: AlertModal;
  public directLink: boolean = true;
  public visibilityIcon: Map<Visibility, string> = new Map<Visibility, string> ([
    ["PRIVATE", 'incognito'],
    ["RESTRICTED", 'restricted']
  ]);

  constructor(private localStorageService: LocalStorageService,
              private layoutService: LayoutService) {
    super();
  }


  public ngOnInit() {
    this.subscriptions.push(this.layoutService.isMobile.subscribe(value => {
      if(value) {
        this.directLink = true;
      } else {
        this.subscriptions.push(this.localStorageService.get().subscribe(value => {
          this.directLink = value;
        }));
      }
    }));
    

  }
  ngOnDestroy() {
    super.ngOnDestroy()
  }
  
  mapType(type: string) {
		return StringUtils.getStakeholderType(type, false);
  }

  public confirmModalOpen() {
    this.modal.cancelButton = true;
    this.modal.okButton = true;
    this.modal.alertTitle = 'You are going to visit ' + this.stakeholder.name + ' Monitor Dashboard';
    this.modal.message = 'You will be navigated to a new tab. Are you sure that you want to proceed?';
    this.modal.okButtonLeft = false;
    this.modal.okButtonText = 'Yes';
    this.modal.cancelButtonText = 'No';
    this.modal.choice = true;
    this.modal.open();
  }
  
  public getStakeholderPageUrl() {
    return this.properties.domain + this.properties.baseLink + "/" + this.stakeholder.alias;
  }
  
  hasPermission() {
    return this.stakeholder.visibility === "PUBLIC" ||
      (this.stakeholder.visibility === "RESTRICTED" && (this.stakeholder.isManager || this.stakeholder.isMember)) ||
      (this.stakeholder.visibility === "PRIVATE" && this.stakeholder.isManager);
  }
  
  public goToPage(data: any) {
    if (data.value == true) {
      let url = this.getStakeholderPageUrl();
      this.localStorageService.setCommunityDirectLink(data.choice);
      window.open(url, '_blank');
    }
  }
}
