import {ChangeDetectorRef, Directive, OnInit} from "@angular/core";
import {StakeholderBaseComponent} from "../../monitor-admin/utils/stakeholder-base.component";
import {OpenaireEntities} from "../../utils/properties/searchFields";
import {StakeholderType} from "../entities/stakeholder";
import {Option} from "../../sharedComponents/input/input.component";
import {FormBuilder, FormControl} from "@angular/forms";
import {LayoutService} from "../../dashboard/sharedComponents/sidebar/layout.service";
import {debounceTime, distinctUntilChanged} from "rxjs/operators";


@Directive()
export class BrowseStakeholderBaseComponent<T> extends StakeholderBaseComponent implements OnInit {
  openaireEntities = OpenaireEntities;
  stakeholderType: StakeholderType;
  stakeholders: T[] = [];
  filteredStakeholders: T[] = [];
  showLoading: boolean = true;
  isMobile: boolean = false;
  gridView: boolean = true;
  sortOptions: Option[] = [
    {value: 'alphAsc', label: 'Alphabetically Asc. (A-Z)'},
    {value: 'alphDsc', label: 'Alphabetically Desc. (Z-A)'},
  ];
  pageOptions: number[] = [10, 20, 30, 40];
  sortBy: string = null;
  currentPage: number = 1;
  pageSize: number = 10;
  parameters = {};
  keywordControl: FormControl;
  hasPublications: boolean = true;
  hasOpenAccess: boolean = true;
  id = 'results';

  /* Services */
  protected layoutService: LayoutService;
  protected cdr: ChangeDetectorRef;
  protected fb: FormBuilder;

  constructor() {
    super();
  }

  ngOnInit() {
    this.keywordControl = this.fb.control(this._route.snapshot.queryParams.keyword);
    this.subscriptions.push(this.layoutService.isMobile.subscribe(isMobile => {
      this.isMobile = isMobile;
      this.cdr.detectChanges();
    }));
    if (!this.stakeholderType) {
      this.stakeholderType = this._route.snapshot.data.type;
      if (!this.stakeholderType) {
        this.navigateToError();
      }
    }
    this.init();
    this.subscriptions.push(this.keywordControl.valueChanges.pipe(debounceTime(200), distinctUntilChanged()).subscribe(value => {
      this.filtering(value);
      if (value?.length > 0) {
        this.parameters['keyword'] = value;
      } else {
        delete this.parameters['keyword']
      }
      this._router.navigate([], {queryParams: this.parameters});
    }));
  }

  init() {
    this.stakeholders = [];
    this.filteredStakeholders = [];
  }

  protected filtering(value) {
    if (!value) {
      this.filteredStakeholders = this.stakeholders;
    } else {
      this.filteredStakeholders = this.stakeholders.filter(item => (item['name'] && item['name'].toLowerCase().includes(value.toLowerCase())) || (item['alias'] && item['alias'].toLowerCase().includes(value.toLowerCase())));
    }
    this.afterStakeholdersInitialized();
    this.currentPage = 1;
  }

  sortByChanged() {
    switch (this.sortBy) {
      case 'alphAsc':
        this.stakeholders = this.stakeholders.sort((a, b) => a['name'].localeCompare(b['name']));
        break;
      case 'alphDsc':
        this.stakeholders = this.stakeholders.sort((a, b) => b['name'].localeCompare(a['name']));
        break;
      default:
        break;
    }
  }

  sizeChanged($event) {
    this.pageSize = $event;
    this.currentPage = 1;
    this.afterStakeholdersInitialized();
  }

  updateCurrentPage($event) {
    this.currentPage = $event.value;
    this._router.navigate([], {fragment: 'results', onSameUrlNavigation: 'reload'});
    this.afterStakeholdersInitialized();
  }

  get typeAsLabel() {
    return this.stakeholderUtils.types.find(type => type.value === this.stakeholderType)?.label;
  }

  afterStakeholdersInitialized() {
    // this is a method that will be overriden from the components extending this base component, if needed
    this.showLoading = false;
  }
}
