import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Router, RouterStateSnapshot, UrlTree} from "@angular/router";
import {Observable} from "rxjs";
import {map, take, tap} from "rxjs/operators";
import {StakeholderService} from "./stakeholder.service";
import {properties} from "../../../../environments/environment";
import {LinksResolver} from "../../../search/links-resolver";

@Injectable({
  providedIn: 'root'
})
export class HasDashboardGuard {

  constructor(protected router: Router,
              protected stakeholderService: StakeholderService) {
  }

  check(path: string, alias: string, type: string, child: string): Observable<boolean> | boolean {
    return (child?this.stakeholderService.getChildStakeholder(alias, type, child):this.stakeholderService.getStakeholder(alias)).pipe(take(1), map(stakeholder => {
      return stakeholder?.standalone || (!!stakeholder && !!child);
    }),tap(authorized => {
      if(!authorized){
        this.router.navigate([LinksResolver.default.errorLink], {queryParams: {'page': path}});
      }
    }));
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.check(state.url, route.params.stakeholder, route.params.type, route.params.child);
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.check(state.url, childRoute.data?.stakeholder?childRoute.data.stakeholder:childRoute.params.stakeholder,
        childRoute.params.type, childRoute.params.child);
  }

}
