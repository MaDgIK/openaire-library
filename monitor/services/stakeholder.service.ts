import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, from, Observable, Subscriber} from "rxjs";
import {
  Indicator, ManageStakeholders,
  Section,
  Stakeholder,
  StakeholderInfo, StakeholderType,
  SubCategory, Umbrella,
  Visibility
} from "../entities/stakeholder";
import {HelperFunctions} from "../../utils/HelperFunctions.class";
import {map} from "rxjs/operators";
import {properties} from "../../../../environments/environment";
import {CustomOptions} from "../../services/servicesUtils/customOptions.class";

export interface SectionInfo {
  id: string;
  indicators: string[];
}

export interface MoveIndicator {
  target: string;
  from: SectionInfo;
  to: SectionInfo;
}

export interface UpdateUmbrella {
  type: StakeholderType;
  action: "ADD" | "REMOVE",
  child: string
}

@Injectable({
  providedIn: "root"
})
export class StakeholderService {

  private stakeholderSubject: BehaviorSubject<Stakeholder> = null;
  private promise: Promise<void>;
  private sub;

  constructor(private http: HttpClient) {
    this.stakeholderSubject = new BehaviorSubject<Stakeholder>(null);
  }

  ngOnDestroy() {
    this.clearSubscriptions();
  }

  clearSubscriptions() {
    if (this.sub instanceof Subscriber) {
      this.sub.unsubscribe();
    }
  }

  getStakeholder(alias: string, shouldUpdate: boolean = false): Observable<Stakeholder> {
    if (!this.stakeholderSubject.value || this.stakeholderSubject.value.alias !== alias || shouldUpdate) {
      this.promise = new Promise<void>((resolve, reject) => {
        this.sub = this.http.get<Stakeholder>(properties.monitorServiceAPIURL + 'stakeholder/' + encodeURIComponent(alias), CustomOptions.registryOptions()).pipe(map(stakeholder => {
          return HelperFunctions.copy(Stakeholder.checkIsUpload(stakeholder));
        })).subscribe(stakeholder => {
          this.stakeholderSubject.next(stakeholder);
          resolve();
        }, error => {
          this.stakeholderSubject.next(null);
          resolve();
        });
      });
    }
    return from(this.getStakeholderAsync());
  }

  getChildStakeholder(parent: string, type: string, child: string, shouldUpdate: boolean = false): Observable<Stakeholder> {
    if (!this.stakeholderSubject.value || this.stakeholderSubject.value.alias !== child || shouldUpdate) {
      this.promise = new Promise<void>((resolve, reject) => {
        this.sub = this.http.get<Stakeholder>(properties.monitorServiceAPIURL + 'stakeholder/' + encodeURIComponent(parent) + '/' + type + '/' + encodeURIComponent(child), CustomOptions.registryOptions()).pipe(map(stakeholder => {
          return HelperFunctions.copy(Stakeholder.checkIsUpload(stakeholder));
        })).subscribe(stakeholder => {
          this.stakeholderSubject.next(stakeholder);
          resolve();
        }, error => {
          this.stakeholderSubject.next(null);
          resolve();
        });
      });
    }
    return from(this.getStakeholderAsync());
  }

  getResearcherStakeholder(orcid, name, results, shouldUpdate: boolean = false): Observable<Stakeholder> {
    if (!this.stakeholderSubject.value || this.stakeholderSubject.value.alias !== orcid || shouldUpdate) {
      this.promise = new Promise<void>((resolve, reject) => {
        this.sub = this.http.get<Stakeholder>(properties.monitorServiceAPIURL + 'stakeholder/' + encodeURIComponent("researcher"), CustomOptions.registryOptions()).pipe(map(stakeholder => {
          return HelperFunctions.copy(Stakeholder.checkIsUpload(stakeholder));
        })).subscribe(stakeholder => {
          stakeholder.index_id = orcid;
          stakeholder.index_name = name;
          stakeholder.name = name;
          stakeholder.alias = orcid;
          if (results < 7 && stakeholder.topics[0]?.categories[0]?.subCategories[0]) {
            stakeholder.topics[0].categories[0].subCategories[0].charts = []; // keep only numbers - charts wont show much anyway
          }
          this.stakeholderSubject.next(stakeholder);
          resolve();
        }, error => {
          let stakeholder = new Stakeholder(null, "researcher", orcid, name, name, orcid, "PUBLIC", null, null, "");
          this.stakeholderSubject.next(stakeholder);
          resolve();
        });
      });
    }
    return from(this.getStakeholderAsync());
  }

  async getStakeholderAsync() {
    if (this.promise) {
      await this.promise;
      this.promise = null;
    }
    this.clearSubscriptions();
    return this.stakeholderSubject.getValue();
  }

  getAlias(): Observable<string[]> {
    return this.http.get<Stakeholder[]>(properties.monitorServiceAPIURL + 'stakeholder/alias', CustomOptions.registryOptions()).pipe(map(stakeholders => {
      return HelperFunctions.copy(stakeholders);
    }));
  }

  getStakeholders(type: string = null, defaultId: string = null): Observable<(Stakeholder & StakeholderInfo)[]> {
    return this.http.get<Stakeholder[]>(properties.monitorServiceAPIURL + 'stakeholder' + ((type) ? ('?type=' + type) : (defaultId?'?defaultId=' + defaultId:'')) + ((type && defaultId) ? ('&defaultId=' + defaultId) : ''), CustomOptions.registryOptions()).pipe(map(stakeholders => {
      return HelperFunctions.copy(Stakeholder.checkIsUpload(stakeholders));
    }));
  }

  getMyStakeholders(type: string = null): Observable<ManageStakeholders> {
    return this.http.get<ManageStakeholders>(properties.monitorServiceAPIURL + 'my-stakeholder' + ((type) ? ('?type=' + type) : ''), CustomOptions.registryOptions()).pipe(map(manageStakeholder => {
      return HelperFunctions.copy({
        templates: Stakeholder.checkIsUpload(manageStakeholder.templates),
        standalone: Stakeholder.checkIsUpload(manageStakeholder.standalone),
        dependent: Stakeholder.checkIsUpload(manageStakeholder.dependent),
        umbrella: Stakeholder.checkIsUpload(manageStakeholder.umbrella),
      });
    }));
  }

  buildStakeholder(stakeholder: Stakeholder, copyId: string, standalone: boolean = true, umbrella: boolean = false): Observable<Stakeholder> {
    if (stakeholder.alias && stakeholder.alias.startsWith('/')) {
      stakeholder.alias = stakeholder.alias.slice(1);
    }
    let buildStakeholder = {
      stakeholder: stakeholder,
      copyId: copyId,
      umbrella: umbrella,
      standalone: standalone
    }
    return this.http.post<Stakeholder>(properties.monitorServiceAPIURL + 'build-stakeholder', buildStakeholder, CustomOptions.registryOptions()).pipe(map(stakeholder => {
      return HelperFunctions.copy(Stakeholder.checkIsUpload(stakeholder));
    }));
  }

  changeVisibility(path: string[], visibility: Visibility, propagate: boolean = false): Observable<any> {
    path.push('change-visibility');
    return this.http.post<Visibility>(properties.monitorServiceAPIURL + path.join('/') + '?visibility=' + visibility + (propagate ? '&propagate=true' : ''), null, CustomOptions.registryOptions());
  }

  saveElement(element: any, path: string[] = [], isFull: boolean = false): Observable<any> {
    if (element.alias && element.alias.startsWith('/')) {
      element.alias = element.alias.slice(1);
    }
    path.push('save');
    if(isFull) {
      path.push('full');
    }
    return this.http.post<any>(properties.monitorServiceAPIURL + path.join('/'), 
      element, CustomOptions.registryOptions()).pipe(map(element => {
      if (path.length === 0) {
        return HelperFunctions.copy(Stakeholder.checkIsUpload(element));
      } else {
        return HelperFunctions.copy(element);
      }
    }));
  }

  saveBulkElements(indicators, path: string[] = []): Observable<any> {
    path.push('save-bulk');
    return this.http.post<any>(properties.monitorServiceAPIURL + path.join('/'), 
      indicators, CustomOptions.registryOptions()).pipe(map(element => {
      if (path.length === 0) {
        return HelperFunctions.copy(Stakeholder.checkIsUpload(element));
      } else {
        return HelperFunctions.copy(element);
      }
    }));
  }

  saveSection(element: any, path: string[] = [], index: number = -1): Observable<Section> {
    path.push('save/');
    return this.http.post<Section>(properties.monitorServiceAPIURL + path.join('/') + index, 
      element, CustomOptions.registryOptions()).pipe(map(element => {
      return HelperFunctions.copy(element);
    }));
  }

  deleteElement(path: string[], childrenAction: string = null): Observable<any> {
    let params: string = "";
    if (childrenAction) {
      params = "?children=" + childrenAction;
    }
    path.push('delete');
    return this.http.delete<any>(properties.monitorServiceAPIURL + path.join('/') + params, CustomOptions.registryOptions());
  }

  reorderElements(path: string[], ids: string[]): Observable<any> {
    path.push('reorder');
    return this.http.post<any>(properties.monitorServiceAPIURL + path.join('/'), ids, CustomOptions.registryOptions());
  }

  reorderIndicators(path: string[], indicators: string[]): Observable<Indicator[]> {
    path.push('reorder');
    return this.http.post<Indicator[]>(properties.monitorServiceAPIURL + path.join('/'), indicators, CustomOptions.registryOptions()).pipe(map(indicators => {
      return HelperFunctions.copy(indicators);
    }));
  }

  moveIndicator(path: string[], moveIndicator: MoveIndicator): Observable<SubCategory> {
    path.push('moveIndicator');
    return this.http.post<SubCategory>(properties.monitorServiceAPIURL + path.join('/'), moveIndicator, CustomOptions.registryOptions()).pipe(map(subCategory => {
      return HelperFunctions.copy(subCategory);
    }));
  }

  updateUmbrella(id: string, update: UpdateUmbrella): Observable<Umbrella> {
    return this.http.post<Umbrella>(properties.monitorServiceAPIURL + id + '/umbrella', update, CustomOptions.registryOptions()).pipe(map(umbrella => {
      return HelperFunctions.copy(umbrella);
    }));
  }

  getStakeholderAsObservable(): Observable<Stakeholder> {
    return this.stakeholderSubject.asObservable();
  }

  setStakeholder(stakeholder: Stakeholder) {
    this.stakeholderSubject.next(stakeholder);
  }
}
