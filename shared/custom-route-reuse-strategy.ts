import {ActivatedRouteSnapshot, BaseRouteReuseStrategy} from "@angular/router";

export class CustomRouteReuseStrategy extends BaseRouteReuseStrategy {
  shouldReuseRoute(future: ActivatedRouteSnapshot, curr: ActivatedRouteSnapshot): boolean {
    return future.routeConfig === curr.routeConfig || (!!future.routeConfig?.component && future.routeConfig?.component === curr.routeConfig?.component)
  }
}