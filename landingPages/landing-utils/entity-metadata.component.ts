import {Component, Input, ViewChild} from "@angular/core";
import {EnvProperties} from "../../utils/properties/env-properties";
import {properties} from "../../../../environments/environment";
import {OpenaireEntities} from "../../utils/properties/searchFields";
import {Organization, Project} from "../../utils/result-preview/result-preview";
import {RouterHelper} from "../../utils/routerHelper.class";

@Component({
  selector: 'entity-metadata',
  template: `
    <div class="uk-text-xsmall entity-metadata uk-flex-inline uk-flex-wrap uk-text-emphasis">
      <!-- oa -->
      <span class="uk-flex-inline uk-flex-middle uk-flex-wrap"
            *ngIf="(openAccessMandatePublications != undefined && openAccessMandatePublications) || (openAccessMandateDatasets != undefined && openAccessMandateDatasets)">
        <span class="uk-margin-xsmall-right open-access">
          <icon name="open_access" [flex]="true" [ratio]="0.8"></icon>
        </span>
        <span class="uk-text-bolder"
              *ngIf="openAccessMandatePublications != undefined && openAccessMandatePublications && openAccessMandateDatasets != undefined && openAccessMandateDatasets">
          Open Access Mandate for {{openaireEntities.PUBLICATIONS}} and {{openaireEntities.DATASETS}}
        </span>
        <span class="uk-text-bolder"
              *ngIf="openAccessMandatePublications != undefined && openAccessMandatePublications && (openAccessMandateDatasets == undefined || !openAccessMandateDatasets)">
          Open Access Mandate for {{openaireEntities.PUBLICATIONS}}
        </span>
        <span class="uk-text-bolder"
              *ngIf="openAccessMandateDatasets != undefined && openAccessMandateDatasets && (openAccessMandatePublications == undefined || !openAccessMandatePublications)">
          Open Access Mandate for {{openaireEntities.DATASETS}}
        </span>
      </span>
      <!-- types -->
      <span *ngIf="entityType" class="uk-flex-inline uk-flex-middle uk-flex-wrap"
            [class.other-separator]="types && removeUnknown(types, true).length > 0">
        <icon *ngIf="entityType.toLowerCase() == 'publication'" name="description" type="outlined"
              [flex]="true" [ratio]="0.8" class="uk-margin-xsmall-right"></icon>
        <icon *ngIf="entityType.toLowerCase() == 'research data'" name="database" type="outlined"
              [flex]="true" [ratio]="0.8" class="uk-margin-xsmall-right"></icon>
        <icon *ngIf="entityType.toLowerCase() == 'research software'" name="integration_instructions"
              type="outlined" [flex]="true" [ratio]="0.8" class="uk-margin-xsmall-right"></icon>
        <icon *ngIf="entityType.toLowerCase() == 'other research product'" name="apps" type="outlined"
              [flex]="true" [ratio]="0.8" class="uk-margin-xsmall-right"></icon>
        <icon *ngIf="entityType.toLowerCase() == 'project'" name="assignment_turned_in" type="outlined"
              [flex]="true" [ratio]="0.8" class="uk-margin-xsmall-right"></icon>
        <icon *ngIf="entityType.toLowerCase() == 'data source'" name="note_add" type="outlined"
              [flex]="true" [ratio]="0.8" class="uk-margin-xsmall-right"></icon>
        <icon *ngIf="entityType.toLowerCase() == 'organization'" name="corporate_fare" type="outlined"
              [flex]="true" [ratio]="0.8" class="uk-margin-xsmall-right"></icon>
        <u class="uk-text-capitalize uk-text-bolder">{{entityType}}</u>
        <span *ngIf="types && removeUnknown(types, true).length > 0">
          <icon name="keyboard_double_arrow_right" [flex]="true" [ratio]="0.8"></icon>
        </span>
      </span>
      <span *ngIf="types && removeUnknown(types, true).length > 0" class="uk-text-italic">
        {{removeUnknown(types, true).join(' , ')}}
      </span>
      <!-- years -->
      <ng-container *ngIf="year">
        <span>{{year}}</span>
      </ng-container>
      <ng-container *ngIf="startYear && !endYear">
        <span>From {{startYear}}</span>
      </ng-container>
      <ng-container *ngIf="!startYear && endYear">
        <span>Until {{endYear}}</span>
      </ng-container>
      <ng-container *ngIf="startYear && endYear">
        <ng-container>
          <span>{{startYear}} - {{endYear}}</span>
        </ng-container>
      </ng-container>
      <!-- dates -->
      <span *ngIf="startDate && !endDate">
        <span class="uk-margin-xsmall-right">From</span>
        <span>{{startDate | date: 'dd MMM yyyy'}}</span>
      </span>
      <span *ngIf="!startDate && endDate">
        <span class="uk-margin-xsmall-right">Until</span>
        <span>{{endDate | date: 'dd MMM yyyy'}}</span>
      </span>
      <span *ngIf="startDate && endDate">
        <ng-container *ngIf="startDate">
          <span>{{startDate | date: 'dd MMM yyyy'}}</span>
          <span class="uk-margin-xsmall-left">(Started)</span>
        </ng-container>
        <span *ngIf="startDate && endDate" class="uk-margin-xsmall-left uk-margin-xsmall-right">-</span>
        <ng-container *ngIf="endDate">
          <span>{{endDate | date: 'dd MMM yyyy'}}</span>
          <span class="uk-margin-xsmall-left">{{currentDate >= endDate ? '(Ended)' : '(Ending)'}}</span>
        </ng-container>
      </span>
      <ng-container *ngIf="status">
        <span>{{status}} <ng-container *ngIf="currentDate <= endDate && currentDate >= startDate">(M{{calcCurrentMonth}})</ng-container></span>
      </ng-container>
      <ng-container *ngIf="date">
        <span>{{date | date: 'dd MMM yyyy': 'UTC'}}</span>
      </ng-container>
      <ng-container *ngIf="embargoEndDate">
        <span>Embargo end date: {{embargoEndDate | date: 'dd MMM yyyy'}}</span>
      </ng-container>
      <span *ngIf="underCuration">
        <span title="{{buildCurationTooltip()}}" uk-tooltip="pos:bottom-right; delay:10;"
              class="uk-text-primary">Under curation</span>
      </span>
      <!-- countries -->
      <span *ngIf="countries && removeUnknown(countries).length > 0">
        {{removeUnknown(countries).join(', ')}}
      </span>
      <!-- languages -->
      <span *ngIf="languages && removeUnknown(languages).length > 0">
        {{removeUnknown(languages).join(', ')}}
      </span>
      <!-- programming languages -->
      <span *ngIf="programmingLanguages && removeUnknown(programmingLanguages).length > 0"
            class="uk-flex uk-flex-middle uk-flex-wrap">
        {{removeUnknown(programmingLanguages).join(', ')}}
      </span>
      <!-- published info -->
      <showPublisher *ngIf="publisher || journal" [publisher]="publisher" [journal]="journal"
                     [properties]="properties"></showPublisher>
      <!-- data provider labels -->
      <span *ngIf="compatibility && !(compatibility.info == 'not available' && type == 'service')">
        <span class="uk-text-meta uk-margin-xsmall-right">Compatibility:</span>
        <span>
          <a *ngIf="compatibility.id"
             [queryParams]="addEoscPrevInParams({datasourceId: compatibility.id})" routerLinkActive="router-link-active"
             [routerLink]="properties.searchLinkToDataProvider.split('?')[0]">
            {{compatibility.info}}
            <ng-container *ngIf="compatibility.name">{{compatibility.name}}</ng-container>
          </a>
          <span *ngIf="!compatibility.id && compatibility.info">
            <ng-container
                *ngIf="compatibility.info.toLowerCase() != 'not yet registered'">{{compatibility.info}}</ng-container>
            <ng-container *ngIf="compatibility.info.toLowerCase() == 'not yet registered'">
              {{compatibility.info}} <span *ngIf="properties.adminToolsPortalType == 'eosc'">in OpenAIRE</span>
            </ng-container>
          </span>
          <span *ngIf="compatibility.name && !compatibility.id">
            {{compatibility.name}}
          </span>
        </span>
      </span>
      <span *ngIf="compatibilityString">
        <span class="uk-text-meta uk-margin-xsmall-right">Compatibility:</span>
        <span>{{compatibilityString}}</span>
      </span>
      <ng-container
          *ngIf="aggregationStatus && aggregationStatus.fulltexts && aggregationStatus.fulltexts > 0">
        <span>OpenAIRE Text Mining</span>
      </ng-container>
      <ng-container *ngIf="thematic">
        <span>Thematic</span>
      </ng-container>
      <ng-container *ngIf="publiclyFunded">
        <span>Publicly funded</span>
      </ng-container>
      <!-- Projects -->
      <span *ngIf="projects && projects.length > 0">
        <span class="uk-text-meta uk-margin-xsmall-right">Funded by:</span>
        <span>{{showInline ? projectNames.join(', ') : projectNames.slice(0, projectsLimit).join(', ')}}</span>
        <span *ngIf="projects.length > projectsLimit">
          <a *ngIf="!showInline" (click)="viewAllProjectsClick();" class="uk-background-muted custom-extra-entities">
            +{{projects.length - projectsLimit | number}}{{projects.length == 1000 ? ' more' : ''}} projects
          </a>
          <a *ngIf="showInline && lessBtn" (click)="showInline = !showInline; lessBtn = false;"
            class="uk-background-muted custom-extra-entities">
            View less
          </a>
        </span>
      </span>
      <!-- Organizations -->
      <span *ngIf="organizations && organizations.length > 0">
        <span class="uk-text-meta uk-margin-xsmall-right">Partners:</span>
        <span>{{showInline ? organizationNames.join(', ') : organizationNames.slice(0, organizationsLimit).join(', ')}}</span>
        <span *ngIf="organizations.length > organizationsLimit">
          <a *ngIf="!showInline" (click)="viewAllPartnersClick();" class="uk-background-muted custom-extra-entities">
            +{{organizations.length - organizationsLimit | number}}{{organizations.length == 1000 ? ' more' : ''}} partners
          </a>
          <a *ngIf="showInline && lessBtn" (click)="showInline = !showInline; lessBtn = false;"
            class="uk-background-muted custom-extra-entities">
            View less
          </a>
        </span>
      </span>
      <!-- Subjects -->
      <span *ngIf="subjects && subjects.length > 0" [class.truncated]="subjects.length > 3">
        {{subjects.slice(0, 3).join(', ')}}
      </span>
      <!-- For tabs in landing -->
      <ng-container *ngIf="provenanceAction">
        <span>{{provenanceAction}}</span>
      </ng-container>
      <ng-container *ngIf="relationName">
        <span>{{relationName}}</span>
      </ng-container>
    </div>



    <modal-alert *ngIf="!isMobile" #partnersModal>
      <div *ngIf="organizations?.length == 1000" class="uk-text-meta uk-margin-medium-bottom">Only 1000 Partners<span *ngIf="resultTitle"> of {{resultTitle}}</span> are shown here.</div>
      <div class="uk-text-small uk-text-emphasis uk-grid uk-grid-column-collapse uk-grid-row-small" uk-grid>
        <ng-container *ngFor="let item of organizations; let i = index">
          <div class="uk-margin-xsmall-right">
            {{item.name}}{{i == organizations.length - 1 ? '' : ','}}
          </div>
        </ng-container>
      </div>
    </modal-alert>

    <modal-alert *ngIf="!isMobile" #projectsModal>
      <div *ngIf="projects?.length == 1000" class="uk-text-meta uk-margin-medium-bottom">Only 1000 {{openaireEntities.PROJECTS}}<span *ngIf="resultTitle"> of {{resultTitle}}</span> are shown here.</div>
      <div class="uk-text-small uk-text-emphasis uk-grid uk-grid-column-collapse uk-grid-row-small" uk-grid>
        <ng-container *ngFor="let item of projects; let i = index">
          <div class="uk-margin-xsmall-right">
          <span
            *ngIf="item['funderShortname'] || item['funderName']">{{item['funderShortname'] ? item['funderShortname'] : item['funderName']}}</span>
          <span *ngIf="!item['funderShortname'] && !item['funderName']">[no funder available]</span>
          <span *ngIf="item['acronym'] || item['title']">| {{ item['acronym'] ? item['acronym'] : item['title']}}</span>
          {{i == projects.length - 1 ? '' : ','}}
          </div>
        </ng-container>
      </div>
    </modal-alert>
  `,
  styleUrls: ['entity-metadata.component.less']
})
export class EntityMetadataComponent {
  @Input() resultTitle: string = null;
  @Input() isMobile: boolean = false;
  @Input() entityType: string;
  @Input() types: string[];
  @Input() year: string; // search result
  @Input() startDate: number; // project landing
  @Input() startYear: string; // search result
  @Input() endDate: number; // project landing
  @Input() endYear: string; // search result
  @Input() currentDate: number; // project landing
  @Input() status: string;  // project landing
  @Input() openAccessMandatePublications: boolean // project landing
  @Input() openAccessMandateDatasets: boolean // project landing
  @Input() date: Date;
  @Input() embargoEndDate: Date | string;
  @Input() underCuration: boolean = false;
  @Input() publisher; // showPublisher component
  @Input() journal; // showPublisher component
  @Input() countries;
  @Input() languages;
  @Input() programmingLanguages;
  @Input() compatibilityString: string;
  @Input() compatibility; // data provider landing
  @Input() aggregationStatus; // data provider landing
  @Input() thematic: boolean; // data provider landing
  @Input() type; // data provider landing
  @Input() provenanceAction: string; // search result
  @Input() relationName: string; // search result
  @Input() publiclyFunded: boolean; // search result
  @Input() projects: Project[];
  @Input() organizations: Organization[];
  @Input() subjects: string[];
  @Input() prevPath: string = "";

  @ViewChild('partnersModal') partnersModal;
  @ViewChild('projectsModal') projectsModal;
  
  organizationsLimit: number = 5;
  projectsLimit: number = 3;
  showInline: boolean = false;
  lessBtn: boolean = false;

  properties: EnvProperties = properties;
  public openaireEntities = OpenaireEntities;
  public routerHelper: RouterHelper = new RouterHelper();

  public removeUnknown(array: string[], type: boolean = false): string[] {
    if (type) {
      return this.removeDuplicates(array).filter(value => value.toLowerCase() !== 'unknown');
    } else {
      return array.filter(value => value.toLowerCase() !== 'unknown');
    }
  }
  
  public removeDuplicates(array: string[]): string[] {
    return array.filter(value => value.toLowerCase() !== this.entityType);
  }
  
  public buildCurationTooltip(): string {
    let tooltipContent: string = "<div class='uk-padding-small'>";
    
    tooltipContent += "<h5>Record in preview</h5>";
    tooltipContent += "<p>Bibliographic record accepted by the system, but not yet processed by <br> OpenAIRE tools for information quality improvement and de-duplication</p>";
    
    tooltipContent += "</div>";
    return tooltipContent;
  }
  
  get projectNames(): string[] {
    if (this.projects && this.projects.length > 0) {
      return this.projects.map(project => {
        let value = project.funderShortname ? project.funderShortname : project.funderName;
        if (project.acronym || project.title) {
          value = (value ? value + ' | ' : '') + (project.acronym ? project.acronym :
            (project.title.length > 25 ? (project.title.slice(0, 25) + '...'): project.title));
        }
        // if(project.code) {
        //   value = value + ' (' + project.code + ')';
        // }
        return value;
      });
    }
    return [];
  }
  
  get organizationNames(): string[] {
    if (this.organizations && this.organizations.length > 0) {
      return this.organizations.map(organization => organization.name);
    }
    return [];
  }

  public addEoscPrevInParams(obj) {
    if(properties.adminToolsPortalType == "eosc" && this.prevPath) {
      return this.routerHelper.addQueryParam("pv", this.prevPath, obj);
    }
    return obj;
  }

  public viewAllPartnersClick() {
    if(this.organizations.length <= this.organizationsLimit * 2) {
      this.showInline = true;
      this.lessBtn = true;
    } else {
      this.openPartnersModal();
    }
  }

  public openPartnersModal() {
    if (this.isMobile) {
      this.partnersModal.okButton = false;
      this.partnersModal.title = "Partners";
      this.partnersModal.open();
    } else {
      this.partnersModal.cancelButton = false;
      this.partnersModal.okButton = false;
      this.partnersModal.alertTitle = "Partners";
      this.partnersModal.open();
    }
  }

  public viewAllProjectsClick() {
    if(this.projects.length <= this.projectsLimit * 2) {
      this.showInline = true;
      this.lessBtn = true;
    } else {
      this.openProjectsModal();
    }
  }

  public openProjectsModal() {
    if (this.isMobile) {
      this.projectsModal.okButton = false;
      this.projectsModal.title = "Projects";
      this.projectsModal.open();
    } else {
      this.projectsModal.cancelButton = false;
      this.projectsModal.okButton = false;
      this.projectsModal.alertTitle = "Projects";
      this.projectsModal.open();
    }
  }

  public get calcCurrentMonth() {
    let currentDate = new Date(this.currentDate);
    let startDate = new Date(this.startDate);

    var months;
    months = (currentDate.getFullYear() - startDate.getFullYear()) * 12;
    months -= startDate.getMonth();
    months += currentDate.getMonth();
    if(startDate.getDate() > currentDate.getDate()) {
      months--;
    }
    return months <= 0 ? 0 : months+1;
  }
}