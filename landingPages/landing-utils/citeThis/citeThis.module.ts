import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {CiteThisComponent} from './citeThis.component';
import {InputModule} from "../../../sharedComponents/input/input.module";

@NgModule({
  imports: [

    CommonModule, FormsModule, InputModule
  ],
  declarations: [
     CiteThisComponent
  ],
  providers:[

   ],
  exports: [

    CiteThisComponent
    ]
})
export class CiteThisModule { }
