import {Component, Input} from '@angular/core';
import {EnvProperties} from "../../utils/properties/env-properties";

@Component({
  selector: 'showPublisher, [showPublisher]',
  template: `
    <ng-container *ngIf="publisher">
      <span class="uk-text-meta uk-margin-xsmall-right">Publisher:</span>
      <span>{{publisher}}</span>
      <span *ngIf="journal && (journal['journal'] || journal['issn'] || journal['lissn']
        || journal['volume'] || journal['eissn'] || journal['issue'])" 
        class="uk-margin-xsmall-left uk-margin-xsmall-right bullet"></span>
    </ng-container>
    <ng-container *ngIf="journal && (journal['journal'] || journal['issn'] || journal['lissn']
			|| journal['volume'] || journal['eissn'] || journal['issue'])">
			<ng-container *ngIf="journal && (journal['journal'] || journal['issn'] || journal['lissn']
				|| journal['volume'] || journal['eissn'] || journal['issue'])">
				<span>
          <span class="uk-text-meta uk-margin-xsmall-right">Journal:</span>
					<span *ngIf="journal['journal']">{{journal['journal']}}</span>
					<span *ngIf="journal['journal'] && (journal['volume'] || journal['issue'])">, </span>
					<ng-container *ngIf="journal['volume']">
						<span  class="uk-display-inline-block">
							volume
							<span *ngIf="journal['volume'] > 0">{{journal['volume'] | number}}</span>
							<span *ngIf="!(journal['volume'] > 0)">{{journal['volume']}}</span>
						</span>
						<span *ngIf="journal['issue'] || journal['start_page'] || journal['end_page']">, </span>
					</ng-container>
					<ng-container *ngIf="journal['issue']">
						<span class="uk-display-inline-block">
							issue
							<span *ngIf="journal['issue'] > 0">{{journal['issue'] | number}}</span>
							<span *ngIf="!(journal['issue'] > 0)">{{journal['issue']}}</span>
						</span>
						<span *ngIf="journal['start_page'] || journal['end_page']">, </span>
					</ng-container>
					<span *ngIf="(journal['volume'] || journal['issue']) && (journal['start_page'] || journal['end_page'])"
								class="uk-display-inline-block">
						{{(journal['start_page'] && journal['end_page']) ? 'pages' : 'page'}}
						<span *ngIf="journal['start_page']">
							<span *ngIf="journal['start_page'] > 0">{{journal['start_page'] | number}}</span>
							<span *ngIf="!(journal['start_page'] > 0)">{{journal['start_page']}}</span>
						</span>
						<span *ngIf="journal['start_page'] && journal['end_page']">-</span>
						<span *ngIf="journal['end_page']">
							<span *ngIf="journal['end_page'] > 0">{{journal['end_page'] | number}}</span>
							<span *ngIf="!(journal['end_page'] > 0)">{{journal['end_page']}}</span>
						</span>
					</span>
					<span *ngIf=" journal['journal'] && (journal['issn'] || journal['eissn'] || journal['lissn'])"> (</span>
					<ng-container *ngIf="journal['issn']">
						<span class="uk-display-inline-block">issn: {{journal['issn']}}</span>
						<span>, </span>
					</ng-container>
					<ng-container *ngIf="journal['eissn']">
						<span class="uk-display-inline-block">eissn: {{journal['eissn']}}</span>
						<span>, </span>
					</ng-container>
					<span *ngIf="journal['lissn']" class="uk-display-inline-block">
						<span class="uk-display-inline-block">lissn: {{journal['lissn']}}</span>
						<span>, </span>
					</span>
					<span *ngIf="journal && (journal['issn'] ||journal['lissn'] || journal['eissn'] )">
						<a target="_blank" class="uk-display-inline-block custom-external"
							[href]="properties.sherpaURL+(journal['issn']?journal['issn']:(journal['eissn']?journal['eissn']:journal['lissn'] ))+properties.sherpaURLSuffix"
							uk-tooltip="title: View information on Open policy finder  ">
						<img src="assets/common-assets/common/jisc.jpeg" width=16 height=16 alt="" loading="lazy">
							Copyright policy
						</a>
					</span>
					<span *ngIf=" journal['journal'] && (journal['issn'] || journal['eissn'] || journal['lissn'])">)</span>
				</span>
			</ng-container>
    </ng-container>
  `
})
export class ShowPublisherComponent {
  @Input() publisher;
  @Input() journal;
  //@Input() sherpaUrl = 'http://www.sherpa.ac.uk/romeo/search.php?issn=';
  //http://sherpa.ac.uk/romeo/issn/2304-6775
  @Input() properties: EnvProperties;
  
  
  constructor() {
  
  }
  
  ngOnInit() {
  }
  
}
