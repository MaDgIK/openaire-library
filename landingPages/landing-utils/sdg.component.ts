import {Component, EventEmitter, Input, Output} from "@angular/core";
import {RouterHelper} from "../../utils/routerHelper.class";
import {properties} from "../../../../environments/environment";
import {StringUtils} from "../../utils/string-utils.class";

@Component({
  selector: 'sdg',
  template: `
		<div class="uk-visible@m">
			<div class="uk-text-xsmall" style="color: #EEB204">Beta</div>
			<div [class]="'uk-flex uk-flex-between uk-flex-middle uk-margin-'+(viewAll?'':'small-')+'bottom'">
				<span *ngIf="viewAll" class="clickable uk-h6 uk-flex uk-flex-middle uk-margin-small-right uk-margin-remove-bottom" 
							(click)="viewLessClick()">
					<icon class="uk-margin-small-right" name="arrow_back" flex="true" ratio="1.2"></icon>
					<span uk-tooltip="Sustainable Development Goals">{{title}}</span>
				</span>
				<span *ngIf="!viewAll" class="uk-text-emphasis uk-text-bolder uk-text-nowrap uk-margin-small-right" uk-tooltip="Sustainable Development Goals">
					{{title}} 
					<span *ngIf="subjects && subjects.length > threshold && !viewAll">({{subjects.length}})</span>
					</span>
        <a *ngIf="properties.adminToolsPortalType == 'eosc' && subjects && subjects.length > threshold && !viewAll"
           (click)="viewAllClick();" class="view-more-less-link uk-link uk-link-text uk-text-truncate">
          View all</a>
				<!-- <a *ngIf="viewAll && lessBtn" (click)="viewAll = !viewAll; lessBtn=false;">View less</a> -->
				<a *ngIf="properties.adminToolsPortalType != 'eosc' && subjects && subjects.length > threshold && !viewAll"
					(click)="viewAllClick();" class="view-more-less-link uk-link uk-link-text uk-text-truncate">
					View all & suggest</a>
				<a *ngIf="properties.adminToolsPortalType != 'eosc' && (subjects && subjects.length <= threshold || viewAll)" class="uk-link uk-link-text uk-text-truncate"
					(click)="suggestClick();">Suggest</a>
			</div>
			<div class="uk-margin-small-bottom uk-flex">
				<img src="assets/common-assets/common/The_Global_Goals_Icon_Color.svg"
					loading="lazy" alt="sdg_colors" style="width:27px; height:27px">
				<div class="uk-margin-small-left">
					<div *ngFor="let subject of subjects.slice(0, viewAll?subjects.length:threshold); let i=index" class="uk-text-truncate">
						<a *ngIf="properties.adminToolsPortalType != 'eosc'" 
               [routerLink]=" properties.searchLinkToResults" [queryParams]="{'sdg': urlEncodeAndQuote(subject)}">
							{{subject}}
						</a>
            <a *ngIf="properties.adminToolsPortalType == 'eosc'" class="custom-external" target="_blank"
               [href]="'https://explore.openaire.eu'+properties.searchLinkToResults+'?sdg='+urlEncodeAndQuote(subject)">
              {{subject}}
            </a>
					</div>
				</div>
			</div>
		</div>
		<div class="uk-hidden@m">
      <div *ngIf="!viewAll">
        <div class="uk-text-xsmall" style="color: #EEB204">Beta</div>
        <div class="uk-flex uk-flex-middle uk-flex-wrap">
          <img src="assets/common-assets/common/The_Global_Goals_Icon_Color.svg"
            loading="lazy" alt="sdg_colors" style="width:18px; height:18px">
          <span class="uk-margin-xsmall-left uk-margin-xsmall-right uk-text-meta">SDGs:</span>
          <div *ngFor="let subject of subjects.slice(0, viewAll?subjects.length:threshold); let i=index" class="uk-text-truncate">
            <a [routerLink]=" properties.searchLinkToResults" [queryParams]="{'sdg': urlEncodeAndQuote(subject)}">{{subject}}</a>
            <span *ngIf="(viewAll && i<subjects.length-1) || i<threshold-1">, &nbsp;</span>
          </div>
          <!-- <div>
            {{subjects.slice(0, viewAll?subjects.length:threshold).join(", ")}}
          </div> -->
        </div>
      </div>
      <div *ngIf="viewAll">
        <div *ngFor="let subject of subjects; let i=index" class="uk-text-truncate">
					<a [routerLink]=" properties.searchLinkToResults" [queryParams]="{'sdg': urlEncodeAndQuote(subject)}">{{subject}}</a>
				</div>
      </div>
      <div class="uk-text-right uk-margin-small-top">
        <a *ngIf="subjects && subjects.length > threshold && !viewAll"
           (click)="viewAllClick();" class="view-more-less-link uk-link uk-text-truncate">
          View all</a>
<!--          View all & suggest</a>-->
<!--        <a *ngIf="(subjects && subjects.length <= threshold || viewAll)" class="uk-link uk-link-text uk-text-truncate"-->
<!--           (click)="suggestClick();">Suggest</a>-->
      </div>
		</div>
  `
})

export class SdgComponent {
  @Input() subjects: string[];
  @Input() viewAll: boolean = false;
  @Output() viewAllClicked  = new EventEmitter();
  @Output() suggestClicked  = new EventEmitter();
  public lessBtn: boolean = false;
  public threshold: number = 2;
  public routerHelper: RouterHelper = new RouterHelper();
  public properties = properties;
  public title: string = "SDGs";

  public viewAllClick() {
    // if(this.subjects.length <= this.threshold*2) {
    //   this.viewAll = true;
    //   this.lessBtn = true;
    // } else {
      this.viewAll = true;
      this.viewAllClicked.emit('sdg');
    // }
  }

  public viewLessClick() {
    this.viewAll = false;
    this.viewAllClicked.emit("");
  }

  public suggestClick() {
    this.suggestClicked.emit('sdg');
  }

  public urlEncodeAndQuote(str: string): string {
    return StringUtils.quote(StringUtils.URIEncode(str));
  }
}
