import {NgModule} from "@angular/core";
import {LandingHeaderComponent} from "./landing-header.component";
import {CommonModule} from "@angular/common";
import {LandingModule} from "../landing.module";
import {ShowAuthorsModule} from "../../../utils/authors/showAuthors.module";
import {IconsModule} from "src/app/openaireLibrary/utils/icons/icons.module";
import {RouterModule} from "@angular/router";
import {ResultLandingUtilsModule} from "../resultLandingUtils.module";
import {EntityMetadataModule} from "../entity-metadata.module";

@NgModule({
  imports: [CommonModule, LandingModule, ShowAuthorsModule, IconsModule, RouterModule, ResultLandingUtilsModule, EntityMetadataModule],
  declarations: [LandingHeaderComponent],
  exports: [LandingHeaderComponent]
})
export class LandingHeaderModule {}
