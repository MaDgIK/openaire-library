import {Component, Input} from "@angular/core";
import {EnvProperties} from "../../../utils/properties/env-properties";
import {Author} from "../../../utils/result-preview/result-preview";
import {AlertModal} from "../../../utils/modal/alert";

@Component({
	selector: 'landing-header',
	template: `
		<div class="title-section" [class.uk-margin-small-bottom]="!isSticky" [ngClass]="titleClass">
			<div class="uk-margin-small-bottom">
				<showTitle [isSticky]=isSticky [titleName]="title" classNames="uk-margin-remove-bottom"></showTitle>
				<div *ngIf="subTitle && !isSticky">
					<span class="uk-text-meta uk-text-small" [innerHTML]="subTitle"></span>
				</div>
			</div>
			<div *ngIf="!isSticky" class="uk-margin-bottom">
				<entity-metadata [entityType]="entityType" [types]="types" [startDate]="startDate" [endDate]="endDate"
												 [currentDate]="currentDate" [status]="status" [openAccessMandatePublications]="openAccessMandatePublications"
												 [openAccessMandateDatasets]="openAccessMandateDatasets" [date]="date" [embargoEndDate]="embargoEndDate"
												 [underCuration]="underCuration" [publisher]="publisher" [journal]="journal"
												 [languages]="languages" [programmingLanguages]="programmingLanguages"
												 [compatibility]="compatibility" [aggregationStatus]="aggregationStatus"
												 [thematic]="thematic" [type]="type" [prevPath]="prevPath"
                         [countries]="countries" [publiclyFunded]="publiclyFunded" [projects]="projects"
				></entity-metadata>
			</div>
			<div *ngIf="authors">
				<showAuthors [authorsLimit]="authorLimit" [modal]="modal" [showAll]="showAllAuthors" [authors]="authors"
							 [isSticky]="isSticky" [isMobile]="isMobile"></showAuthors>
			</div>
		</div>
	`
})
export class LandingHeaderComponent {
  @Input() isMobile: boolean = false;
	@Input() entityType: string;
	@Input() properties: EnvProperties;
	@Input() types: string[];
	@Input() startDate: number; // project landing
	@Input() endDate: number; // project landing
	@Input() currentDate: number; // project landing
	@Input() status: string;  // project landing
	@Input() openAccessMandatePublications: boolean // project landing
	@Input() openAccessMandateDatasets: boolean // project landing
	@Input() date: Date;
	@Input() embargoEndDate: Date;
	@Input() title: string;
	@Input() subTitle: string;
	@Input() authors: Author[];
	@Input() authorLimit: number = 7;
	@Input() showAllAuthors: boolean = true;
	@Input() underCuration: boolean = false;
	@Input() modal: AlertModal;
	@Input() titleClass: string = null;
	@Input() isSticky: boolean = false;
	@Input() publisher; // showPublisher component
	@Input() journal; // showPublisher component
	@Input() languages;
	@Input() programmingLanguages;
	@Input() compatibility; // data provider landing
	@Input() aggregationStatus; // data provider landing
	@Input() thematic: boolean; // data provider landing
	@Input() type; // data provider landing
  @Input() prevPath: string = "";
  @Input() countries;
  @Input() publiclyFunded;
  @Input() projects;
}
