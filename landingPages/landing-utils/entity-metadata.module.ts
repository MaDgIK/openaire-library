import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EntityMetadataComponent} from './entity-metadata.component';
import {IconsModule} from '../../utils/icons/icons.module';
import {AlertModalModule} from '../../utils/modal/alertModal.module';
import {ShowPublisherModule} from "./showPublisher.module";

@NgModule({
  imports: [
    CommonModule, IconsModule, AlertModalModule, ShowPublisherModule
  ],
  declarations: [EntityMetadataComponent],
  providers:[],
  exports: [EntityMetadataComponent]
})
export class EntityMetadataModule { }
