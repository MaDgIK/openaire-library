import {Component, ElementRef, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {HostedByCollectedFrom} from "../../utils/result-preview/result-preview";
import {properties} from "../../../../environments/environment";
import {StringUtils} from '../../utils/string-utils.class';
import {FullScreenModalComponent} from "../../utils/modal/full-screen-modal/full-screen-modal.component";

declare var UIkit;
import {RouterHelper} from "../../utils/routerHelper.class";

@Component({
  selector: 'availableOn',
  template: `
		<ng-container *ngIf="availableOn && availableOn.length > 0">
			<div class="uk-flex uk-flex-middle"
           [ngClass]="isMobile ? ('uk-flex-column ' + (!(usedBy == 'landing') ? 'uk-margin-left uk-margin-xsmall-bottom' : '')) : 'uk-grid uk-grid-small uk-child-width-auto'" 
           [attr.uk-grid]="!isMobile ? '' : null">

        <ng-container *ngIf="inModal">
          <div class="uk-text-small uk-height-max-small uk-overflow-auto">
            <ng-container *ngTemplateOutlet="availableOnList"></ng-container>
          </div>
        </ng-container>

        <ng-container *ngIf="!inModal">
          <div *ngIf="availableOn[0].fulltext" class="uk-flex uk-flex-middle" 
               [ngClass]="isMobile ? 'uk-width-1-1' : 'uk-text-bolder'">
            <a [href]="availableOn[0].fulltext" target="_blank"
               class="uk-flex uk-flex-middle uk-button-link"
               [ngClass]="isMobile ? ((usedBy == 'landing') ? 'uk-width-1-1 uk-padding-small uk-padding-remove-horizontal' : '') : 'uk-flex-center'">
  <!--            <icon [flex]="true" [ratio]="0.7" name="download" visuallyHidden="download"></icon>-->
              <icon name="download" visuallyHidden="donwload" [flex]="true" [ratio]="(isMobile && usedBy == 'landing') ? 1 : 0.8"></icon>
              <span class="uk-margin-xsmall-left">Full-Text</span>
            </a>
          </div>
        
          <div *ngIf="isMobile && (usedBy == 'landing') && availableOn[0].fulltext" class="uk-width-1-1"><hr></div>
          <div *ngIf="availableOn[0].downloadUrl" [ngClass]="isMobile ? 'uk-width-1-1' : ''">
            <div class="uk-flex uk-flex-middle" [ngClass]="isMobile ? ((usedBy == 'landing') ? 'uk-padding-small uk-padding-remove-horizontal' : '') : ''">
              <span [class]="'uk-margin-xsmall-right ' + (availableOn[0].accessRightIcon == 'open_access' ? 'open-access' : 'closed-access')"
                  uk-tooltip [title]="getAccessLabel(availableOn[0].accessRight)">
                <icon [name]="availableOn[0].accessRightIcon" [flex]="true" [ratio]="(isMobile && usedBy == 'landing') ? 1 : 0.8"></icon>
              </span>
              <ng-container *ngIf="!isMobile">
                <a target="_blank"
                  class="uk-flex uk-flex-middle uk-flex-center uk-button-link uk-text-bolder">
                  <span>{{sliceString(availableOn[0].downloadNames.join("; "), 20)}}</span>
                  <span>
                    <!-- <icon [flex]="true" [name]="'expand_' + (isOpen?'less':'more')"></icon>-->
                    <icon [flex]="true" [name]="(isOpen?'arrow_drop_up':'arrow_drop_down')"></icon>
                  </span>
                </a>
                <div #dropElement uk-drop="mode: click; pos: bottom-left; flip: false; shift: false" class="uk-drop download-drop">
                  <div class="uk-card uk-card-default uk-padding-small uk-padding-remove-horizontal uk-text-small uk-height-max-large uk-overflow-auto">
                      <ng-container *ngTemplateOutlet="availableOnList"></ng-container>
                  </div>
                </div>
              </ng-container>
              
              <ng-container *ngIf="isMobile">
                <a #toggle class="uk-flex uk-flex-between uk-flex-middle uk-flex-center uk-width-expand uk-button-link">
                  <span>{{sliceString(availableOn[0].downloadNames.join("; "), 20)}}</span>
                  <span>
                    <icon [flex]="true" ratio="1.5" name="arrow_right"></icon>
                </span>
                </a>
                <mobile-dropdown [toggle]="toggle">
                  <div class="uk-text-emphasis uk-text-bolder uk-text-center uk-padding-small uk-padding-remove-vertical uk-text-uppercase">
                      Sources
                  </div>
                  <div class="uk-text-small download-drop uk-padding uk-padding-remove-horizontal">
                    <ng-container *ngTemplateOutlet="availableOnList"></ng-container>
                  </div>
                </mobile-dropdown>
              </ng-container>
            </div>
          </div>
        </ng-container>
			</div>
		</ng-container>
    
    <ng-template #availableOnList>
      <div *ngFor="let instance of availableOn let i=index" class="download-drop-item uk-flex uk-flex-top" 
           [ngClass]="inModal ? 'uk-margin-small-bottom' : ''">
        <span
          [class]="'uk-margin-small-right ' + (instance.accessRightIcon == 'open_access' ? 'open-access' : 'closed-access')"
          uk-tooltip [title]="getAccessLabel(instance.accessRight)">
          <icon [name]="instance.accessRightIcon" [flex]="true" [ratio]="inModal ? 0.8 : 1"></icon>
        </span>
        <div class="uk-padding-small uk-padding-remove-left uk-padding-remove-vertical" 
             [ngClass]="inModal ? 'uk-grid' : 'uk-width-expand'">
          <span class="uk-text-emphasis">
            <a *ngIf="instance.downloadUrl" [href]="instance.downloadUrl" target="_blank"
               class="title uk-link-text uk-text-bold custom-external uk-display-inline-block">
              {{instance.downloadNames.join("; ")}}
            </a>
          </span>
          <div *ngIf="instance.types?.length > 0 || instance.years?.length > 0 || instance.peerReviewed" class="uk-text-meta">
            <span *ngIf="instance.types?.length > 0" class="uk-text-capitalize">{{instance.types.join(" . ")}}</span>
            <span *ngIf="instance.types?.length > 0 && instance.years?.length > 0"> . </span>
            <span *ngIf="instance.years?.length > 0">{{instance.years.join(" . ")}}</span>
            <span *ngIf="(instance.types?.length > 0 || instance.years?.length > 0) && instance.peerReviewed"> . </span>
            <span *ngIf="instance.peerReviewed">Peer-reviewed</span>
          </div>
          <div *ngIf="instance.license" class="uk-text-meta uk-text-truncate" uk-tooltip [title]="instance.license">
            License:
            <a *ngIf="isUrl(instance.license); else elseBlock"
               [href]="instance.license" target="_blank" class="custom-external uk-link-text">
              {{instance.license}}
            </a>
            <ng-template #elseBlock> {{instance.license}}</ng-template>
          </div>
          <div *ngIf="instance.fulltext" class="uk-text-meta uk-text-truncate" uk-tooltip [title]="instance.fulltext">
            Full-Text:
            <a *ngIf="isUrl(instance.fulltext); else elseBlock"
               [href]="instance.fulltext" target="_blank" class="custom-external uk-link-text">
              {{instance.fulltext}}
            </a>
            <ng-template #elseBlock> {{instance.fulltext}}</ng-template>
          </div>
          <div *ngIf="instance.collectedNamesAndIds?.size > 0" class="uk-text-meta">
            <span>Data sources: </span>
            <a *ngFor="let collectedName of getKeys(instance.collectedNamesAndIds); let i=index" [routerLink]="dataProviderUrl"
               [queryParams]="addEoscPrevInParams({datasourceId: instance.collectedNamesAndIds.get(collectedName)})" class="uk-link-text">
              {{collectedName}}<ng-container *ngIf="(i !== (instance.collectedNamesAndIds.size - 1))">; </ng-container>
            </a>
          </div>
        </div>
      </div>
    </ng-template>
  `
})

export class AvailableOnComponent {
  @Input() isMobile: boolean = false;
  @Input() inModal: boolean = false;
  @Input() usedBy: "search" | "landing" = "search";
  @Input() prevPath: string = "";
  @Input() availableOn: HostedByCollectedFrom[];
  /** @deprecated */
  @Output() viewAllClicked = new EventEmitter();
  @ViewChild("dropElement") dropElement: ElementRef;
  public threshold: number = 1;
  public dataProviderUrl = properties.searchLinkToDataProvider.split('?')[0];
  public title: string = "Download from";
  public routerHelper:RouterHelper = new RouterHelper();

  constructor() {
  }

  ngOnInit() {
  }

  public getKeys(map) {
    return Array.from(map.keys());
  }

  public isUrl(str: string): boolean {
    return str.startsWith('http://') || str.startsWith('https://') || str.startsWith('//') || str.startsWith('www.');
  }

  get isOpen() {
    return (typeof document !== 'undefined') && this.dropElement && UIkit.drop(this.dropElement.nativeElement).isActive();
  }

  public sliceString(str: string, size: number) {
    return StringUtils.sliceString(str, size)
  }

  public getAccessLabel(accessRight) : string {
    if(accessRight) {
      return (accessRight + (accessRight.toLowerCase().endsWith(" access") ? "" : " access"));
    }
    return "Not available access";
  }

  public addEoscPrevInParams(obj) {
    if(properties.adminToolsPortalType == "eosc" && this.prevPath) {
      let splitted: string[] = this.prevPath.split("?");
      obj = this.routerHelper.addQueryParam("return_path", splitted[0], obj);
      if(splitted.length > 0) {
        obj = this.routerHelper.addQueryParam("search_params", splitted[1], obj);
      }
    }
    return obj;
  }

  protected readonly properties = properties;
}
