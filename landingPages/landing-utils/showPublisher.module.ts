import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ShowPublisherComponent} from "./showPublisher.component";
import {IconsModule} from '../../utils/icons/icons.module';

@NgModule({
  imports: [
    CommonModule, IconsModule
  ],
  declarations: [
    ShowPublisherComponent
  ],
  providers:[
  ],
  exports: [
    ShowPublisherComponent
  ]
})
export class ShowPublisherModule { }
