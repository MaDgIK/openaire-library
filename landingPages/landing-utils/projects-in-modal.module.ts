/* This module contains all common components for all landing pages */

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {ProjectsServiceModule} from '../../services/projectsService.module';
import {ProjectsInModalComponent} from './projects-in-modal.component';

import {PagingModule} from '../../utils/paging.module';
import {ErrorMessagesModule} from '../../utils/errorMessages.module';
import {NoLoadPaging} from "../../searchPages/searchUtils/no-load-paging.module";
import {SearchResultsModule} from "../../searchPages/searchUtils/searchResults.module";
import {SearchFilterModule} from '../../searchPages/searchUtils/searchFilter.module';
import {DropdownFilterModule} from "../../utils/dropdown-filter/dropdown-filter.module";

@NgModule({
  imports: [
    RouterModule, CommonModule, FormsModule,
    ProjectsServiceModule,
    PagingModule, ErrorMessagesModule, NoLoadPaging,
    SearchResultsModule, SearchFilterModule, DropdownFilterModule
  ],
  declarations: [
    ProjectsInModalComponent
  ],
  providers: [],
  exports: [
    ProjectsInModalComponent
  ]
})
export class ProjectsInModalModule {
}
