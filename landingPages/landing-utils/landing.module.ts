/* This module contains all common components for all landing pages */

import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';
import { RouterModule } from '@angular/router';

import {ShowTitleComponent} from './showTitle.component';
import {AddThisComponent} from './addThis.component';

@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule
  ],
  declarations: [
    ShowTitleComponent, AddThisComponent
  ],
  providers:[],
  exports: [
     ShowTitleComponent, AddThisComponent
    ]
})
export class LandingModule { }
