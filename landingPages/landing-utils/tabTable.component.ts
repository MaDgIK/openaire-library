import {Component, Input} from '@angular/core';
import {RouterHelper} from '../../utils/routerHelper.class';
import{EnvProperties} from '../../utils/properties/env-properties';

@Component({
    selector: 'tabTable',
    template: `
        <div *ngIf="info && info.length > pageSize" class="uk-margin">
          <span class="uk-text-bold">{{info.length | number}} research results, page {{page | number}} of {{totalPages(info.length) | number}}</span>
          <paging-no-load class="uk-float-right" [currentPage]="page" [totalResults]="info.length" [size]="pageSize" (pageChange)="updatePage($event)"></paging-no-load>
        </div>

        <table class="uk-table">
            <!--thead>
                <tr>
                    <th>Title</th>
                    <th width="20%">{{percentageName}}</th>
                </tr>
            </thead-->
            <tbody>
                <tr *ngFor="let item of info.slice((page-1)*pageSize, page*pageSize)" class="{{item['class']}}">
                    <td *ngIf="item != undefined">
                        <!--span *ngIf="item['class'] == 'dataset'" class="glyphicon glyphicon-star" aria-hidden="true"-->
<!--                         <span *ngIf="item['class'] == 'publication'" class="uk-icon" title = "Publication"><svg height="20" ratio="1" viewBox="0 0 20 20" width="20" xmlns="http://www.w3.org/2000/svg"><rect fill="none" height="16" stroke="#000" width="12" x="3.5" y="2.5"></rect><polyline fill="none" points="5 0.5 17.5 0.5 17.5 17" stroke="#000"></polyline></svg></span>-->
<!--                         <span class="uk-icon"  *ngIf="item['class'] == 'dataset'" title = "Research data" ><svg height="20" ratio="1" viewBox="0 0 20 20" width="20" xmlns="http://www.w3.org/2000/svg"><ellipse cx="10" cy="4.64" fill="none" rx="7.5" ry="3.14" stroke="#000"></ellipse><path d="M17.5,8.11 C17.5,9.85 14.14,11.25 10,11.25 C5.86,11.25 2.5,9.84 2.5,8.11" fill="none" stroke="#000"></path><path d="M17.5,11.25 C17.5,12.99 14.14,14.39 10,14.39 C5.86,14.39 2.5,12.98 2.5,11.25" fill="none" stroke="#000"></path><path d="M17.49,4.64 L17.5,14.36 C17.5,16.1 14.14,17.5 10,17.5 C5.86,17.5 2.5,16.09 2.5,14.36 L2.5,4.64" fill="none" stroke="#000"></path></svg></span>-->
<!--                         <span class="uk-icon" *ngIf="item['class'] == 'software'" title = "Software"  >-->
<!--                          <svg height="20" ratio="1" viewBox="0 0 20 20" width="20" xmlns="http://www.w3.org/2000/svg"> <circle cx="9.997" cy="10" fill="none" r="3.31" stroke="#000"></circle> <path d="M18.488,12.285 L16.205,16.237 C15.322,15.496 14.185,15.281 13.303,15.791 C12.428,16.289 12.047,17.373 12.246,18.5 L7.735,18.5 C7.938,17.374 7.553,16.299 6.684,15.791 C5.801,15.27 4.655,15.492 3.773,16.237 L1.5,12.285 C2.573,11.871 3.317,10.999 3.317,9.991 C3.305,8.98 2.573,8.121 1.5,7.716 L3.765,3.784 C4.645,4.516 5.794,4.738 6.687,4.232 C7.555,3.722 7.939,2.637 7.735,1.5 L12.263,1.5 C12.072,2.637 12.441,3.71 13.314,4.22 C14.206,4.73 15.343,4.516 16.225,3.794 L18.487,7.714 C17.404,8.117 16.661,8.988 16.67,10.009 C16.672,11.018 17.415,11.88 18.488,12.285 L18.488,12.285 Z" fill="none" stroke="#000"></path></svg>-->
<!--                        </span>-->
<!--                        <span class="uk-icon"  *ngIf="item['class'] == 'other'" title = "Other" ><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" ratio="1"> <rect fill="none" stroke="#000" x="3.5" y="1.5" width="13" height="17"></rect></svg></span>-->
                        <!--/span-->
                        <!--a *ngIf="item['url'] != '' && item['name'] != ''" href="{{item['url']}}"-->
                      <div class="uk-text-muted">
                        {{item['class']}}
                      </div>
                        <a *ngIf="item['id'] != '' && item['name'] != '' && item['class'] == 'dataset'"
                            [queryParams]="{datasetId: item.id}" routerLinkActive="router-link-active" routerLink="/search/dataset">
                                {{item['name']}}
                        </a>

                        <a *ngIf="item['id'] != '' && item['name'] != '' && item['class'] == 'software'"
                            [queryParams]="{softwareId: item.id}" routerLinkActive="router-link-active" routerLink="/search/software">
                                {{item['name']}}
                        </a>

                        <a *ngIf="item['id'] != '' && item['name'] != '' && item['class'] == 'other'"
                            [queryParams]="{orpId: item.id}" routerLinkActive="router-link-active" routerLink="/search/other">
                                {{item['name']}}
                        </a>

                        <a *ngIf="item['id'] != '' && item['name'] != '' && item['class'] == 'publication'"
                            [queryParams]="{articleId: item.id}" routerLinkActive="router-link-active" routerLink="/search/publication">
                        <!--a *ngIf="item['url'] != '' && item['name'] != '' && item['class'] == 'publication'"
                            href="http://astero.di.uoa.gr:3000/search/publication?articleId={{item['id']}}"-->

                                {{item['name']}}
                        </a>

                        <p *ngIf="item['id'] == '' && item['name'] != ''">{{item['name']}}</p>
                        <span *ngIf="item['date']">
                            ({{item['date']}})
                        </span>
                    </td>
                    <td>
                        <div *ngIf="item['percentage'] != ''"  title="{{item['percentage']}}%" >
                            <div class="uk-text-center uk-text-small">{{item['percentage']}}%</div>
                            <progress class="uk-progress uk-margin-remove" value="{{item['percentage']}}" max="100"></progress>
                         </div>
                        <div *ngIf="item['percentage'] == ''">
                            <p>No {{percentageName}} available</p>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    `

    })

export class TabTableComponent {
    @Input() percentageName: string = "Trust";
    @Input() info: { "name": string, "id": string, "date": string, "percentage": number, "class": string}[];//Map<string, string[]>;
    @Input() properties:EnvProperties;
    public routerHelper:RouterHelper = new RouterHelper();
    public searchLinkToPublication: string;
    public searchLinkToDataset: string;

    public page: number = 1;
    public pageSize: number = 10;

    constructor () {
    }

    ngOnInit() {
        this.searchLinkToPublication = this.properties.searchLinkToPublication;
        this.searchLinkToDataset = this.properties.searchLinkToDataset;
    }

    totalPages(totalResults: number): number {
      let totalPages:any = totalResults/this.pageSize;
      if(!(Number.isInteger(totalPages))) {
          totalPages = (parseInt(totalPages, this.pageSize) + 1);
      }
      return totalPages;
    }

    updatePage($event) {
      this.page = $event.value;
    }
}
