/* This module contains all common components for Publication & Daasets Landing Pages */
import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';
import { RouterModule } from '@angular/router';
import {PagingModule} from '../../utils/paging.module';
import {ShowIdentifiersComponent} from './showIdentifiers.component';
import {ShowSubjectsComponent} from './showSubjects.component';
import {FundedByComponent} from './fundedBy.component';
import {AvailableOnComponent} from './availableOn.component';
import {TabTableComponent} from './tabTable.component';
import {ShowPublisherModule} from "./showPublisher.module";
import {RelatedToComponent} from "./relatedTo.component";
import {FosComponent} from "./fos.component";
import {SdgComponent} from "./sdg.component";
import {IconsModule} from "../../utils/icons/icons.module";
import {AlertModalModule} from "../../utils/modal/alertModal.module";
import { SearchInputModule } from '../../sharedComponents/search-input/search-input.module';
import {IconsService} from "../../utils/icons/icons.service";
import {book, closed_access, cog, database, earth, open_access, unknown_access} from "../../utils/icons/icons";
import {FullScreenModalModule} from "../../utils/modal/full-screen-modal/full-screen-modal.module";
import {MobileDropdownModule} from "../../utils/mobile-dropdown/mobile-dropdown.module";

@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule, PagingModule, ShowPublisherModule, IconsModule, AlertModalModule,
    SearchInputModule, FullScreenModalModule, MobileDropdownModule
  ],
  declarations: [
    ShowIdentifiersComponent,ShowSubjectsComponent,
    FundedByComponent,AvailableOnComponent,TabTableComponent,
    RelatedToComponent, FosComponent, SdgComponent
  ],
  providers:[
  ],
  exports: [
    ShowIdentifiersComponent, ShowSubjectsComponent,
    FundedByComponent,AvailableOnComponent, TabTableComponent,
    RelatedToComponent, FosComponent, SdgComponent
  ]
})
export class ResultLandingUtilsModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([open_access, closed_access, unknown_access, book, cog, database, earth]);
  }
}
