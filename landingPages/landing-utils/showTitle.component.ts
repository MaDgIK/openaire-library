import {Component, ElementRef, Input, ViewChild} from '@angular/core';

@Component({
  selector: 'showTitle',
  template: `
    <h1 [ngClass]="classNames" class="uk-h6">
      <ng-container *ngTemplateOutlet="_title;"></ng-container>
    </h1>

    <ng-template #_title>
      <div *ngIf="title != undefined" class="landingTitle uk-text-break">
        <span *ngIf="title['url'] != undefined && title['url'] != null && title['url'] != ''"
          class="custom-external">
          <a *ngIf="title['name'] != undefined && title['name'] != ''"
            href="{{title['url']}}" target="_blank"
            [innerHTML]="title['name']">
          </a>
          <a *ngIf="title['name'] == undefined || title['name'] == ''"
            href="{{title['url']}}" target="_blank">
            [no title available]
          </a>
        </span>
        <span *ngIf="(title['name'] != undefined && title['name'] != '') &&
          (title['url'] == undefined || title['url'] == null || title['url'] == '')"
          [innerHTML]="title['name']">
        </span>
        <span *ngIf="(title['name'] == undefined || title['name'] == '') &&
          (title['url'] == undefined || title['url'] == null || title['url'] == '')">
          [no title available]
        </span>
      </div>
      <div *ngIf="titleName" #titleDiv class="uk-text-break multi-line-ellipsis" [ngClass]="isSticky ? 'lines-1' : 'lines-5'">
        <span [innerHTML]="titleName" [attr.uk-tooltip]="showTitleTooltip ? 'cls: uk-active uk-width-large' : 'cls: uk-invisible'"
          [title]="titleName"></span>
      </div>
      <div *ngIf="!titleName && !title">
        <span>No title available</span>
      </div>
    </ng-template>
  `
})

export class ShowTitleComponent {
  @Input() titleName: string;
  @Input() title: { [key: string]: string };
  @Input() iconClass:string;
  @Input() classNames: string = "";
  @Input() isSticky: boolean = false;
  @ViewChild("titleDiv") titleDiv: ElementRef;

  constructor () {}

  get showTitleTooltip():boolean {
		return !!this.titleDiv && (this.titleDiv.nativeElement.clientHeight >= 5 * 21);
	}
}
