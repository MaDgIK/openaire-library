import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Subscriber} from 'rxjs';
import {properties} from 'src/environments/environment';

import {CommunityService} from '../../connect/community/community.service';
import {ConnectHelper} from '../../connect/connectHelper';
import {UserManagementService} from '../../services/user-management.service';
import {HelperFunctions} from "../../utils/HelperFunctions.class";
import {Context} from "../../utils/entities/resultLandingInfo";
import {OpenaireEntities} from "../../utils/properties/searchFields";

@Component({
  selector: 'relatedTo, [relatedTo]',
  template: `
    <ng-container *ngIf="communities && communities.length > 0">
      <div *ngIf="!mobileView" class="uk-margin-small-bottom uk-flex uk-flex-between">
        <span *ngIf="viewAll && !lessBtn" class="clickable uk-h6 uk-flex uk-flex-middle" (click)="viewLessClick()">
          <icon class="uk-margin-small-right" name="arrow_back" flex="true" ratio="1.2"></icon>
          {{title}}
        </span>
        <span *ngIf="!viewAll || lessBtn" class="uk-text-emphasis uk-text-bolder">{{title}}</span>
        <a *ngIf="viewAll && lessBtn" (click)="viewAll = !viewAll; lessBtn=false;" class="view-more-less-link uk-link-text">View less</a>
        <a *ngIf="communities && communities.length > threshold && !viewAll"
           (click)="viewAllClick();" class="view-more-less-link uk-link-text">View all</a>
      </div>
      <div *ngFor="let community of communities.slice(0, viewAll?communities.length:threshold)" class="uk-text-truncate" 
           [class.uk-margin-small-bottom]="mobileView">
        <!-- If there are any communities with dashboard -->
        <a *ngIf="community.link" href="{{community.link}}" target="_blank" [attr.uk-tooltip]="community.labelContext" class="custom-external">
          {{community.labelContext}}
        </a>
        <!-- Other communities (without dashboards) -->
        <ng-container *ngIf="!community.link">
          {{community.labelContext}}
          <span *ngIf="community.labelCategory && (currentCommunity == community.idContext)">
          <span uk-icon="icon: arrow-right"></span> {{community.labelCategory}}
        </span>
          <span *ngIf="community.labelConcept && (currentCommunity == community.idContext)">
          : {{community.labelConcept}}
        </span>
        </ng-container>
      </div>
    </ng-container>
  `
})

export class RelatedToComponent implements OnInit {
  @Input() mobileView: boolean = false;
  @Input() contexts: Context[];
  @Input() viewAll: boolean = false;
  @Output() viewAllClicked  = new EventEmitter();
  @Output() noCommunities  = new EventEmitter();
  public lessBtn: boolean = false;
  public threshold: number = 4;
	public communities: Context[] = [];
	public currentCommunity = ConnectHelper.getCommunityFromDomain(properties.domain);
	private subscriptions = [];
  public title: string = "Related to "+OpenaireEntities.COMMUNITIES;

  constructor(private communityService: CommunityService,
							private userManagementService: UserManagementService) {
  }
  
  ngOnInit() {
		this.contexts.sort(this.compare);
		let index = 0;
		this.contexts.forEach( context => {
			if(context.idContext) {
				this.subscriptions.push(
					this.userManagementService.getUserInfo().subscribe( user => {
						//- handling subscribe errors?
						this.subscriptions.push(
							this.communityService.getCommunityInfo(context.idContext).subscribe( community => {
								if(community && !ConnectHelper.isPrivate(community,user) && (this.currentCommunity != context.idContext)) {
									if(properties.environment == "beta") {
										context.link = 'https://beta.' + context.idContext + '.openaire.eu';
									} else {
										context.link = 'https://' + context.idContext + '.openaire.eu';
									}
								}
                for(let community of this.communities) {
                	if(community.link == context.link) {
                    index++;
                    if(index == this.contexts.length) {
                      this.communitiesInitialized();
                    }
                    return; // skips so that we don't get duplicate gateways
                	}
                }
                this.communities.push(context);
								index++;
								if(index == this.contexts.length) {
									this.communitiesInitialized();
								}
							}, error => {
                index++;
                if(index == this.contexts.length) {
                  this.communitiesInitialized();
                }
              })
						);
					})
				);
			}
		});
  }

	ngOnDestroy() {
		this.subscriptions.forEach(subscription => {
			if(subscription instanceof Subscriber) {
				subscription.unsubscribe();
			}
		});
	}
  
  public scroll() {
    HelperFunctions.scroll();
  }

  public communitiesInitialized() {
    if(this.communities.length == 0) {
      this.noCommunities.emit(true);
    }
    this.communities.sort(this.compare);
  }

	public compare(a, b) {
    if (a.link && !b.link) {
      return -1;
    } else if(!a.link && b.link) {
      return 1;
    } else {
      if (a.labelContext < b.labelContext) {
        return -1;
      } else if (a.labelContext > b.labelContext) {
        return 1;
      }
    }
    return 0;
	}

  public viewAllClick() {
    if(this.communities.length <= this.threshold*2) {
      this.viewAll = true;
      this.lessBtn = true;
    } else {
      this.viewAll = true;
      this.viewAllClicked.emit('relatedTo');
    }
  }

  public viewLessClick() {
    this.viewAll = false;
    this.viewAllClicked.emit("");
  }
}
