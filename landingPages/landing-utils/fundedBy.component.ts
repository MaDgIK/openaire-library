import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Project} from "../../utils/result-preview/result-preview";
import {properties} from "../../../../environments/environment";
import {HelperFunctions} from '../../utils/HelperFunctions.class';
import {RouterHelper} from "../../utils/routerHelper.class";

@Component({
  selector: 'fundedBy',
  template: `
    <div *ngIf="!isMobile" class="uk-margin-small-bottom uk-flex uk-flex-between">
      <span *ngIf="viewAll && !lessBtn" class="clickable uk-h6 uk-flex uk-flex-middle" (click)="viewLessClick()">
        <icon class="uk-margin-small-right" name="arrow_back" flex="true" ratio="1.2"></icon>
        {{title}}
      </span>
      <span *ngIf="!viewAll || lessBtn" class="uk-text-emphasis uk-text-bolder">{{title}}</span>
      <a *ngIf="viewAll && lessBtn" (click)="viewAll = !viewAll; lessBtn=false;"
         class="view-more-less-link uk-link-text">View less</a>
      <a *ngIf="fundedByProjects && fundedByProjects.length > threshold && !viewAll"
         (click)="viewAllClick();" class="view-more-less-link uk-link-text">View all</a>
    </div>
    <div *ngIf="!isMobile">
			<span *ngFor="let item of fundedByProjects.slice(0, viewAll?fundedByProjects.length:threshold) let i=index">
        <span class="uk-text-emphasis" (click)="dropClicked=true">
          <a class="uk-link uk-link-text">
            <ng-container *ngTemplateOutlet="funder; context: {item: item}"></ng-container>
          </a>
        </span>
        <div class="default-dropdown uk-margin-remove-top uk-dropdown"
             uk-dropdown="pos: bottom-left; mode:click">
          <ng-container *ngIf="dropClicked">
            <ng-container *ngTemplateOutlet="dropInfo; context: { item: item, index: i}"></ng-container>
          </ng-container>
        </div>
        <span *ngIf="i < (fundedByProjects.slice(0, viewAll?fundedByProjects.length:threshold).length - 1)">, </span>
      </span>
    </div>
    <div *ngIf="isMobile">
      <div *ngFor="let item of fundedByProjects.slice(0, viewAll?fundedByProjects.length:threshold) let i=index"
           class="uk-flex uk-flex-middle uk-margin-small-bottom">
        <span class="uk-text-emphasis uk-width-expand" (click)="dropClicked=true">
         <ng-container *ngTemplateOutlet="funder; context: {item: item}"></ng-container>
        </span>
        <a #toggle><icon name="info" visuallyHidden="info" [type]="'outlined'"></icon></a>
        <mobile-dropdown [toggle]="toggle">
          <div *ngIf="dropClicked" class="uk-margin-remove-top">
              <ng-container *ngTemplateOutlet="dropInfo; context: { item: item, index: i}"></ng-container>
          </div>
        </mobile-dropdown>
      </div>
    </div>

    <ng-template #funder let-item=item>
      <span
        *ngIf="item['funderShortname'] || item['funderName']">{{item['funderShortname'] ? item['funderShortname'] : item['funderName']}}</span>
      <span *ngIf="!item['funderShortname'] && !item['funderName']">[no funder available]</span>
      <span *ngIf="item['acronym'] || item['title']">| {{ item['acronym'] ? item['acronym'] : item['title']}}</span>
    </ng-template>

    <ng-template #dropInfo let-item=item let-index=index>
      <div class="uk-padding-small">
        <span>Project</span>
        <div class="uk-margin-bottom">
          <a *ngIf="item.id" class="uk-h6 uk-margin-remove portal-link"
             [queryParams]="addEoscPrevInParams({projectId: item.id})" [routerLink]="url">
            {{item['acronym'] ? item['acronym'] : item['title']}}
          </a>
          <span *ngIf="!item.id" class="uk-h6 uk-margin-remove">
                    {{item['acronym'] ? item['acronym'] : item['title']}}
                  </span>
          <div *ngIf="item.acronym && item.title" class="uk-text-meta">
            {{item.title}}
          </div>
        </div>
        <ul class="uk-list uk-padding-remove-left uk-margin-bottom">
          <li *ngIf="item.funderShortname || item.funderName">
            <span class="uk-text-meta">Funder: </span>
            {{item.funderName ? item.funderName : item.funderShortname}}
            <span *ngIf="item.funderShortname && item.funderName">
                      ({{item.funderShortname}})
                    </span>
          </li>
          <li *ngIf="item.code">
            <span class="uk-text-meta">Project Code: </span>{{item.code}}
          </li>
          <li *ngIf="item.funding">
            <span class="uk-text-meta">Funding stream: </span>{{item.funding}}
          </li>
        </ul>
        <div *ngIf="item.provenanceAction || item.validated" class="uk-text-meta">
          <span *ngIf="item.validated">Validated by funder</span>
          <span *ngIf="item.provenanceAction && item.validated"> | </span>
          <span *ngIf="item.provenanceAction">{{item.provenanceAction}}</span>
        </div>
      </div>
    </ng-template>
  `
})

export class FundedByComponent {
  @Input() isMobile: boolean = false;
  @Input() prevPath: string = "";
  @Input() fundedByProjects: Project[];
  @Input() viewAll: boolean = false;
  @Output() viewAllClicked  = new EventEmitter();
  public lessBtn: boolean = false;
  public threshold: number = 4;
  public url = properties.searchLinkToProject.split('?')[0];
  public title: string = "Funded by";
	@Input() provenanceActionVocabulary = null;
  // public provenancesCalculated: boolean[] = [];
  public routerHelper:RouterHelper = new RouterHelper();
  public dropClicked: boolean = false;

  public ngOnInit() {
    this.fundedByProjects.forEach((project, index) => {
      this.getVocabularyLabel(project, this.provenanceActionVocabulary, index);
    })
  }

  public viewAllClick() {
    if(this.fundedByProjects.length <= this.threshold*2) {
      this.viewAll = true;
      this.lessBtn = true;
    } else {
      this.viewAll = true;
      this.viewAllClicked.emit('fundedBy');
    }
  }

  public viewLessClick() {
    this.viewAll = false;
    this.viewAllClicked.emit("");
  }

	public getVocabularyLabel(item: any, vocabulary: any, index: number) {
    // if(!this.provenancesCalculated[index]) {
    //   this.provenancesCalculated[index] = true;
      item.provenanceAction = HelperFunctions.getVocabularyLabel(item.provenanceAction, vocabulary, false);
    // }
		// return item.provenanceAction;
	}

  public addEoscPrevInParams(obj) {
    if(properties.adminToolsPortalType == "eosc" && this.prevPath) {
      let splitted: string[] = this.prevPath.split("?");
      obj = this.routerHelper.addQueryParam("return_path", splitted[0], obj);
      if(splitted.length > 0) {
        obj = this.routerHelper.addQueryParam("search_params", splitted[1], obj);
      }
    }
    return obj;
  }
}
