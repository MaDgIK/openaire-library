import {
  Component,
  Inject, Input,
  OnInit,
  RendererFactory2,
  ViewEncapsulation
} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {DOCUMENT} from "@angular/common";
import {Subscriber} from "rxjs";

declare var a2a;

@Component({
  selector: 'addThis',
  template: `
    <div [id]="'addToAny'+(url ? ('_'+url) : '')" #addToAny class="a2a_kit a2a_kit_size_42 a2a_default_style fully_rounded" [attr.data-a2a-url]="url">
      <a class="a2a_button_twitter"></a>
      <a class="a2a_button_facebook"></a>
      <a class="a2a_button_linkedin"></a>
      <a class="a2a_button_mendeley"></a>
      <a class="a2a_button_reddit"></a>
      <a class="a2a_button_email"></a>
      <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
    </div>
    <div *ngIf="showWarning" class="uk-alert uk-alert-warning uk-animation-fade">
      Do the share buttons not appear? Please make sure, any blocking addon is disabled, and then reload the page.
    </div>
  `
})
export class AddThisComponent implements OnInit {
  subs = [];
  showWarning = false;
  @Input() url: string = null;

  constructor(private route: ActivatedRoute, @Inject(DOCUMENT) private document, private rendererFactory: RendererFactory2) {
  }
  
  public ngOnDestroy() {
    for (let value of this.subs) {
      if (value instanceof Subscriber) {
        value.unsubscribe();
      } else if (value instanceof Function) {
        value();
      }
    }
    //
    // if(typeof document !== 'undefined') {
    //   const renderer = this.rendererFactory.createRenderer(this.document, {
    //     id: '-1',
    //     encapsulation: ViewEncapsulation.None,
    //     styles: [],
    //     data: {}
    //   });
    //   const head = this.document.body;
    //   if (head === null) {
    //     throw new Error('<head> not found within DOCUMENT.');
    //   }
    //
    //
    //   let script = null;
    //   head.childNodes.forEach(node => {
    //     if(node.id === "addToAnyScript") {
    //       // script = node;
    //       node.remove();
    //     }
    //   })
    //   // let script = head.nativeElement.getElementById("script");
    //   // if(script) {
    //     // renderer.removeChild(head, script);
    //     // script.remove();
    //   // }
    // }
  }

  ngOnInit() {

    this.subs.push(this.route.queryParams.subscribe(data => {
      this.showWarning = false;
      try {
        if (!this.document.getElementById('addToAnyScript') && typeof document !== 'undefined') {
          const renderer = this.rendererFactory.createRenderer(this.document, {
            id: '-1',
            encapsulation: ViewEncapsulation.None,
            styles: [],
            data: {}
          });
          const head = this.document.body;
          if (head === null) {
            throw new Error('<head> not found within DOCUMENT.');
          }
          const script = renderer.createElement('script');
          renderer.setAttribute(script, "id", "addToAnyScript");
          renderer.setAttribute(script, "src", "https://static.addtoany.com/menu/page.js");
          renderer.setAttribute(script, "type", "text/javascript");
          renderer.appendChild(head, script);
        }
        if (typeof document !== 'undefined') {
          if(typeof a2a !== 'undefined' && this.document.getElementById('addToAny'+(this.url ? ('_'+this.url) : '')) && !this.document.getElementById('addToAny'+(this.url ? ('_'+this.url) : '')).innerText) {
            a2a.init_all();
          }
        }
        this.subs.push(setTimeout(() => {
          if (this.document.getElementById('addToAny'+(this.url ? ('_'+this.url) : '')) && !this.document.getElementById('addToAny'+(this.url ? ('_'+this.url) : '')).innerText) {
            this.showWarning = true;
          }
        }, 4000));
      } catch (e) {
        // console.error(e)
      }

    }));
  }
}
