import {Component, Input} from '@angular/core';

import {FetchProjects} from '../../utils/fetchEntitiesClasses/fetchProjects.class';

import {ErrorCodes} from '../../utils/properties/errorCodes';
import {StringUtils} from '../../utils/string-utils.class';
import {RouterHelper} from '../../utils/routerHelper.class';
import {EnvProperties} from '../../utils/properties/env-properties';
import {AlertModal} from "../../utils/modal/alert";
import {OpenaireEntities} from '../../utils/properties/searchFields';

@Component({
  selector: 'projectsInModal',
  template: `
    <div *ngIf="fetchProjects.funders.length > 1" class="uk-margin-medium-bottom">
      <ng-container *ngFor="let filter of fetchProjects.filters">
        <div class="uk-inline">
          <dropdown-filter #dropdownFilter dropdownClass="uk-width-large uk-padding-small uk-margin-medium-bottom"
                           [name]="filter.title" [count]="filter.countSelectedValues" [isMobile]="isMobile">
            <div class="uk-padding-small uk-overflow-auto uk-height-max-large uk-height-min-medium">
              <search-filter [filter]="filter"
                             [showResultCount]=true filterValuesNum="0"
                             (onFilterChange)="filterChange($event)" [actionRoute]="false">
              </search-filter>
            </div>
          </dropdown-filter>
        </div>
      </ng-container>
    </div>
    <errorMessages [status]="[fetchProjects.searchUtils.status]" [type]="openaireEntities.PROJECTS"
                   tab_error_class=true></errorMessages>
    <div *ngIf="fetchProjects.searchUtils.status == errorCodes.DONE">
			<results-and-pages [type]="openaireEntities.PROJECTS"
				[page]="page" [pageSize]="size"
				[totalResults]="fetchProjects.searchUtils.totalResults">
      </results-and-pages>
      <search-result  [results]="fetchProjects.results"
                      [status]="fetchProjects.searchUtils.status"
                      [type]="'project'"
                      [showLoading]="true" [properties]="properties" [prevPath]="prevPath">
      </search-result>
			<paging-no-load *ngIf="fetchProjects.searchUtils.totalResults > size"
				(pageChange)="pageChange($event)"
				[currentPage]="page" 
				[size]="size"
				[totalResults]="fetchProjects.searchUtils.totalResults">
      </paging-no-load>
    </div>
  `
})

export class ProjectsInModalComponent {
  @Input() prevPath: string = "";
  @Input() fetchProjects: FetchProjects;
  @Input() organizationId: string = "";
  @Input() properties: EnvProperties;
  @Input() modal: AlertModal;
  @Input() isMobile: boolean = false;
  public page: number = 1;
  public size: number = 5;
  
  public routerHelper: RouterHelper = new RouterHelper();
  public errorCodes: ErrorCodes = new ErrorCodes();
  
  private filterQuery: string = "";

	public openaireEntities = OpenaireEntities;

  ngOnInit() {
    if (this.organizationId) {
      if (this.fetchProjects.searchUtils.totalResults > 0) {
        this.search(false, "");
      }
    }
  }
  
  private search(refine: boolean, filterQuery: string) {
    var refineFields: string [] = ["funder"];
    this.fetchProjects.getResultsForOrganizations(this.organizationId, filterQuery, this.page, this.size, (refine) ? refineFields : [], this.properties);
    this.scrollToTabTop("projects");
  }
  
  public pageChange($event) {
    this.page = $event.value;
    this.search(false, this.filterQuery);
  }

  scrollToTabTop(tabId:string){
    setTimeout(() => {
      window.scrollTo({
        top: document.getElementById(tabId) ? document.getElementById(tabId).offsetTop - 250 : 250,
        behavior: 'smooth'
      });
    }, 200);
  }
  
  public filterChange($event) {
    this.page = 1;
    this.updateFilters();
    //this.search(true, this.filterQuery);
    this.search(false, this.filterQuery);
  }
  
  private updateFilters() {
    this.filterQuery = "";
    for (let filter of this.fetchProjects.filters) {
      filter.countSelectedValues = 0;
      var filterLimits = "";
      for (let value of filter.values) {
        if (value.selected == true) {
          //filterLimits+=((filterLimits.length == 0)?'':',') +'"'+ StringUtils.URIEncode(value.id)+'"';
          filterLimits += ((filterLimits.length == 0) ? '' : ' or ') + filter.filterId + ' exact ';
          filterLimits += '"' + StringUtils.URIEncode(value.id) + '"';
          filter.countSelectedValues++;
        }
      }
      if (filterLimits.length > 0) {
        //this.filterQuery+=' and '+filter.filterId + ' exact '+ filterLimits + ' ';
        // this.filterQuery += ' and ( ' + filterLimits + ' ) ';
        this.filterQuery += '( ' + filterLimits + ' ) ';
      }
      
    }
    //console.log("Filter Changed"+this.filterQuery);
    
  }
}
