import {Component, EventEmitter, Input, Output} from "@angular/core";
import {RouterHelper} from "../../utils/routerHelper.class";
import {properties} from "../../../../environments/environment";
import {StringUtils} from "../../utils/string-utils.class";
import {SearchFields} from "../../utils/properties/searchFields";

@Component({
  selector: 'fos',
  template: `
		<div class="uk-visible@m">
<!--			<div class="uk-text-xsmall" style="color: #EEB204">Beta</div>-->
			<div [class]="'uk-flex uk-flex-between uk-flex-middle uk-margin-'+(viewAll?'':'small-')+'bottom'">
				<span *ngIf="viewAll" class="clickable uk-h6 uk-flex uk-flex-middle uk-margin-small-right uk-margin-remove-bottom" (click)="viewLessClick()">
					<icon class="uk-margin-small-right" name="arrow_back" [flex]="true" [ratio]="1.2"></icon>
					<span class="uk-text-nowrap">{{title}}</span>
				</span>
				<span *ngIf="!viewAll" class="uk-text-emphasis uk-text-bolder uk-text-nowrap uk-margin-small-right">
					{{title}}
					<span *ngIf="subjects && subjects.length > threshold && !viewAll">({{subjects.length}})</span>
				</span>
        <a *ngIf="properties.adminToolsPortalType == 'eosc' && subjects && subjects.length > threshold && !viewAll"
           (click)="viewAllClick();" class="view-more-less-link uk-link uk-link-text uk-text-truncate">
          View all
        </a>
        <!--                    hide L3 & L4 FoS-->
        <!-- <a *ngIf="viewAll && lessBtn" (click)="viewAll = !viewAll; lessBtn=false;">View less</a> -->
				<a *ngIf="properties.adminToolsPortalType != 'eosc' && subjects && subjects.length > threshold && !viewAll"
					(click)="viewAllClick();" class="view-more-less-link uk-link uk-link-text uk-text-truncate">
          View all<ng-container *ngIf="properties.environment == 'development'"> & suggest</ng-container>
				</a>
				<a *ngIf="properties.environment == 'development' && properties.adminToolsPortalType != 'eosc' && (subjects && subjects.length <= threshold || viewAll)" class="uk-link uk-link-text uk-text-truncate"
					(click)="suggestClick();">Suggest</a>
			</div>
			<div class="uk-margin-small-top">
				<div *ngFor="let subject of subjects.slice(0, viewAll?subjects.length:threshold); let i=index" class="uk-text-truncate">
					<a  *ngIf="properties.adminToolsPortalType != 'eosc'"
              [routerLink]="properties.searchLinkToResults" [queryParams]="buildFosQueryParam(subject)">
						{{subject.label}}
					</a>
          <a *ngIf="properties.adminToolsPortalType == 'eosc'" class="custom-external" target="_blank"
             [href]="'https://explore.openaire.eu'+properties.searchLinkToResults+'?fos='+urlEncodeAndQuote(subject.label)">
            {{subject.label}}
          </a>
				</div>
			</div>
		</div>
		<div class="uk-hidden@m">
			<div *ngIf="!viewAll" class="uk-grid uk-grid-small uk-grid-divider" uk-grid>
				<div class="uk-width-1-4 uk-text-meta">
<!--          <div class="uk-text-xsmall" style="color: #EEB204">Beta</div>-->
					{{title}}
				</div>
				<div class="uk-width-expand">
					<div *ngFor="let subject of subjects.slice(0, viewAll?subjects.length:threshold); let i=index" class="uk-text-truncate">
						<a *ngIf="properties.adminToolsPortalType != 'eosc'"
               [routerLink]="properties.searchLinkToResults" [queryParams]="buildFosQueryParam(subject)">
							{{subject.label}}
						</a>
            <a *ngIf="properties.adminToolsPortalType == 'eosc'" class="custom-external" target="_blank"
               [href]="'https://explore.openaire.eu'+properties.searchLinkToResults+'?'+buildFosHrefParam(subject)">
              {{subject.label}}
            </a>
					</div>
				</div>
			</div>
      <div *ngIf="viewAll">
        <div *ngFor="let subject of subjects; let i=index" class="uk-text-truncate">
          <a *ngIf="properties.adminToolsPortalType != 'eosc'"
             [routerLink]="properties.searchLinkToResults" [queryParams]="buildFosQueryParam(subject)">
            {{subject.label}}
          </a>
          <a *ngIf="properties.adminToolsPortalType == 'eosc'" class="custom-external" target="_blank"
             [href]="'https://explore.openaire.eu'+properties.searchLinkToResults+'?'+buildFosHrefParam(subject)">
            {{subject.label}}
          </a>
        </div>
      </div>
      <div class="uk-text-right uk-margin-small-top">
        <a *ngIf="subjects && subjects.length > threshold && !viewAll"
           (click)="viewAllClick();" class="view-more-less-link uk-text-truncate">
          <span class="">View all</span>
<!--          <span class="">View all & suggest</span>-->
        </a>
<!--        <a *ngIf="(subjects && subjects.length <= threshold || viewAll)" class="uk-link uk-text-truncate"-->
<!--           (click)="feedbackClick();">Feedback</a>-->
      </div>
		</div>
  `
})

export class FosComponent {
  @Input() subjects: {"id": string, "label": string}[];
  @Input() viewAll: boolean = false;
  @Output() viewAllClicked  = new EventEmitter();
  @Output() suggestClicked  = new EventEmitter();
  public lessBtn: boolean = false;
  public threshold: number = 2;
  public routerHelper: RouterHelper = new RouterHelper();
  public properties = properties;
  public title: string = "Fields of Science";
  private searchFieldsHelper: SearchFields = new SearchFields();

  public viewAllClick() {
    // if(this.subjects.length <= this.threshold*2) {
    //   this.viewAll = true;
    //   this.lessBtn = true;
    // } else {
      this.viewAll = true;
      this.viewAllClicked.emit('fos');
    // }
  }

  public viewLessClick() {
    this.viewAll = false;
    this.viewAllClicked.emit("");
  }

  public suggestClick() {
    this.suggestClicked.emit('fos');
  }

  public urlEncodeAndQuote(str: string): string {
    return StringUtils.quote(StringUtils.URIEncode(str));
  }

  public buildFosQueryParam(fos) {
    // return {'foslabel': this.urlEncodeAndQuote(fos.id+"||"+fos.label)};
    return (this.searchFieldsHelper.getFosParameter() == 'foslabel' ? ({'foslabel': this.urlEncodeAndQuote(fos.id+"||"+fos.label)}) : ({'fos': this.urlEncodeAndQuote(fos.id)}));
  }

  public buildFosHrefParam(fos): string {
    // return ('foslabel='+this.urlEncodeAndQuote(fos.id+"||"+fos.label));
    return (this.searchFieldsHelper.getFosParameter() == 'foslabel' ? ('foslabel='+this.urlEncodeAndQuote(fos.id+"||"+fos.label)) : ('fos='+this.urlEncodeAndQuote(fos.id)));
  }
}
