import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {timeout} from "rxjs/operators";

@Injectable()
export class HtmlProjectReportService {

    constructor(private http: HttpClient ) {}

    getHTML(id: string, requestType:string, csvAPIURL: string ):any {
        let url = csvAPIURL;
        url += '?format=html&type='+requestType+'&fq=(' +
          //'((oaftype exact result) and (resulttypeid exact "'+resultTypeId+'")) and
          '(relprojectid exact "'+id+'"))';

        return this.http.get(url,{responseType: 'text'}).pipe(timeout(10000));
    }
}
