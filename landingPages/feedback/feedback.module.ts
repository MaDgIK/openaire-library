import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FeedbackComponent} from "./feedback.component";
import {LandingHeaderModule} from "../landing-utils/landing-header/landing-header.module";
import {ReactiveFormsModule} from "@angular/forms";
import {AlertModalModule} from "../../utils/modal/alertModal.module";
import {EmailService} from "../../utils/email/email.service";
import {RecaptchaModule} from "ng-recaptcha";
import {IconsModule} from "../../utils/icons/icons.module";
import {InputModule} from "../../sharedComponents/input/input.module";

@NgModule({
  imports: [CommonModule, LandingHeaderModule, ReactiveFormsModule, AlertModalModule, RecaptchaModule, IconsModule, InputModule],
  declarations: [FeedbackComponent],
  providers: [EmailService],
  exports: [FeedbackComponent]
})
export class FeedbackModule {}
