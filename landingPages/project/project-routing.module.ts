import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PreviousRouteRecorder } from "../../utils/piwik/previousRouteRecorder.guard";
import { ProjectComponent } from "./project.component";


@NgModule({
  imports: [
    RouterModule.forChild([{ path: '', component: ProjectComponent, canDeactivate: [PreviousRouteRecorder] }])
  ]
})
export class ProjectRoutingModule { }
