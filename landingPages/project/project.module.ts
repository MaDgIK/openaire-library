import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {ProjectComponent} from './project.component';
import {ProjectServiceModule} from './projectService.module';
import {HtmlProjectReportService} from "../htmlProjectReport/htmlProjectReport.service";
import {ReportsServiceModule} from '../../services/reportsService.module';
import {SearchResearchResultsServiceModule} from '../../services/searchResearchResultsService.module';
import {LandingModule} from '../landing-utils/landing.module';
import {LandingHeaderModule} from "../landing-utils/landing-header/landing-header.module";

import {LoadingModalModule} from '../../utils/modal/loadingModal.module';
import {AlertModalModule} from '../../utils/modal/alertModal.module';
import {ErrorMessagesModule} from '../../utils/errorMessages.module';
import {HelperModule} from "../../utils/helper/helper.module";
import {IFrameModule} from '../../utils/iframe.module';

import {Schema2jsonldModule} from '../../sharedComponents/schema2jsonld/schema2jsonld.module';
import {SEOServiceModule} from '../../sharedComponents/SEO/SEOService.module';
import {FeedbackModule} from "../feedback/feedback.module";
import {AltMetricsModule} from "../../utils/altmetrics.module";
import {TabsModule} from "../../utils/tabs/tabs.module";
import {SearchTabModule} from "../../utils/tabs/contents/search-tab.module";
import {LoadingModule} from "../../utils/loading/loading.module";
import {IconsModule} from "../../utils/icons/icons.module";
import {InputModule} from "../../sharedComponents/input/input.module";
import {IconsService} from "../../utils/icons/icons.service";
import {graph, link, link_to, open_access} from "../../utils/icons/icons";
import {FullScreenModalModule} from '../../utils/modal/full-screen-modal/full-screen-modal.module';
import {SafeHtmlPipeModule} from '../../utils/pipes/safeHTMLPipe.module';
import {EGIDataTransferModule} from "../../utils/dataTransfer/transferData.module";
import {EntityActionsModule} from "../../utils/entity-actions/entity-actions.module";
import {ProjectRoutingModule} from "./project-routing.module";

@NgModule({
  imports: [
    ProjectRoutingModule,
    CommonModule, FormsModule, RouterModule, LandingModule,
    LoadingModalModule, AlertModalModule, ErrorMessagesModule,
    IFrameModule, ReportsServiceModule,
    SearchResearchResultsServiceModule, ProjectServiceModule,
    Schema2jsonldModule, SEOServiceModule, HelperModule,
    LandingHeaderModule, FeedbackModule, AltMetricsModule,
    TabsModule, SearchTabModule, LoadingModule, IconsModule, InputModule,
    FullScreenModalModule, SafeHtmlPipeModule, EGIDataTransferModule, EntityActionsModule
  ],
  declarations: [
    ProjectComponent
  ],
  providers:[
     HtmlProjectReportService
  ],
  exports: [
    ProjectComponent
  ]
})
export class ProjectModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([link, graph, link_to, open_access])
  }
}
