import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {ProjectService} from './project.service';

@NgModule({
  imports: [
    CommonModule, FormsModule
  ],
  declarations: [],
  providers: [
    ProjectService
  ],
  exports: []
})
export class ProjectServiceModule {}
