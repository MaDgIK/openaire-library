import { NgModule}            from '@angular/core';
import { CommonModule }       from '@angular/common';
import { FormsModule }        from '@angular/forms';

 import { DeletedByInferenceComponent } from './deletedByInference.component';
 import { DeletedByInferenceService } from './deletedByInference.service';

import {ResultLandingUtilsModule} from '../../landing-utils/resultLandingUtils.module';

 import {PagingModule}        from '../../../utils/paging.module';

 import {ErrorMessagesModule} from '../../../utils/errorMessages.module';
import {ShowAuthorsModule} from "../../../utils/authors/showAuthors.module";
import {SearchResultsModule} from "../../../searchPages/searchUtils/searchResults.module";
import {NoLoadPaging} from "../../../searchPages/searchUtils/no-load-paging.module";
import {ResultPreviewModule} from "../../../utils/result-preview/result-preview.module";

@NgModule({
  imports: [
    CommonModule, FormsModule, ResultLandingUtilsModule,
    PagingModule, ErrorMessagesModule, ShowAuthorsModule, SearchResultsModule, NoLoadPaging, ResultPreviewModule
  ],
  declarations: [
    DeletedByInferenceComponent
  ],
  providers:[
    DeletedByInferenceService
   ],
  exports: [
    DeletedByInferenceComponent
  ]
})
export class DeletedByInferenceModule { }
