import {Component} from '@angular/core';
import {ElementRef, Input} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {EnvProperties} from '../../../utils/properties/env-properties';
import {ResultLandingInfo} from '../../../utils/entities/resultLandingInfo';
import {RouterHelper} from '../../../utils/routerHelper.class';
import {ErrorCodes} from '../../../utils/properties/errorCodes';

import {DeletedByInferenceService} from './deletedByInference.service';
import {HostedByCollectedFrom, ResultPreview} from "../../../utils/result-preview/result-preview";
import {AlertModal} from "../../../utils/modal/alert";
import {Subscriber} from "rxjs";
import {properties} from "../../../../../environments/environment";
import {HelperFunctions} from "../../../utils/HelperFunctions.class";
import {Dates, StringUtils} from "../../../utils/string-utils.class";
import {ParsingFunctions} from "../../landing-utils/parsingFunctions.class";

@Component({
  selector: 'deletedByInference',
  template: `
    <div id="versions_container">
      <errorMessages [status]="[status]" [type]="type" tab_error_class=true></errorMessages>

      <no-load-paging *ngIf="resultsPreview.length > pageSize" [type]="type"
                      (pageChange)="updatePage($event)"
                      [page]="page" [pageSize]="pageSize"
                      [totalResults]="resultsPreview.length">
      </no-load-paging>
      <ul class="uk-list uk-margin">
        <li *ngFor="let result of resultsPreview.slice((page-1)*pageSize, page*pageSize)">
          <result-preview [modal]="modal" [properties]="properties" [hasLink]="false" [result]="result"
                          [showOrcid]="false" [prevPath]="prevPath" [showInline]="true"
                          [isDeletedByInferenceModal]="true" [isMobile]="isMobile"></result-preview>
        </li>
      </ul>
      <no-load-paging *ngIf="resultsPreview.length > pageSize" [type]="type"
                      (pageChange)="updatePage($event)"
                      [page]="page" [pageSize]="pageSize"
                      [totalResults]="resultsPreview.length">
      </no-load-paging>
    </div>
  `
})
export class DeletedByInferenceComponent {
  @Input() isMobile: boolean = false;
  @Input() prevPath: string = "";
  public resultsPreview: ResultPreview[] = [];
  @Input() children = [];
  @Input() id: string;
  @Input() ids: string[] = [];
  @Input() type: string;
  @Input() resultType: string;
  @Input() modal: AlertModal;
  
  // Custom tab paging variables
  public page: number = 1;
  public pageSize: number = 5;
  
  public status: number;
  public routerHelper: RouterHelper = new RouterHelper();
  public errorCodes: ErrorCodes = new ErrorCodes();
  
  subscriptions = [];
  properties: EnvProperties = properties;
  
  constructor(private element: ElementRef,
              private _deletedByInferenceService: DeletedByInferenceService,
              private route: ActivatedRoute) {
  }
  
  ngOnInit() {
    this.subscriptions.push(this.route.queryParams.subscribe(data => {
      this.errorCodes = new ErrorCodes();
      this.status = this.errorCodes.LOADING;
      
      this.parseDeletedByInference();
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscriber) {
        subscription.unsubscribe();
      }
    });
  }

  public parseDeletedByInference() {
    let parsingFunctions: ParsingFunctions = new ParsingFunctions();
    let length = Array.isArray(this.children) ? this.children.length : 1;
    for (let i = 0; i < length; i++) {
      let result = Array.isArray(this.children) ? this.children[i] : this.children;
      let preview = new ResultPreview();
      if(result.hasOwnProperty("creator")) {
        preview.authors = [];
        let authorsLength = Array.isArray(result.creator) ? result.creator.length : 1;
        for (let j = 0; j < authorsLength; j++) {
          let author = {"fullName": Array.isArray(result.creator) ? result.creator[j] : result.creator, "orcid": null, "orcid_pending": null};
          preview.authors.push(author);
        }
      }
      if(result.hasOwnProperty("dateofacceptance")) {
        preview.year = new Date(result.dateofacceptance).getFullYear().toString();
      }
      if(result.hasOwnProperty("title")) {
        let titleLength = Array.isArray(result.title) ? result.title.length : 1;
        for (let j = 0; j < titleLength; j++) {
          let title = Array.isArray(result.title) ? result.title[j] : result.title;
          if (!preview.title || title.classid == "main title") {
            preview.title = StringUtils.HTMLToString(String(title.content));
          }
        }
      }
      if(result.hasOwnProperty("description")) {
        preview.description = result.description;
      }
      preview.resultType = result?.resulttype?.classid ? result.resulttype.classid : this.resultType;


      if (result.hasOwnProperty("instance")) {
        preview.hostedBy_collectedFrom = new Array<HostedByCollectedFrom>();

        preview.types = new Array<string>();
        let types = new Set<string>();

        let counter = 0;
        let instance;

        let instanesLength = Array.isArray(result['instance']) ? result['instance'].length : 1;

        for (let j = 0; j < instanesLength; j++) {
          instance = Array.isArray(result['instance']) ? result['instance'][j] : result['instance'];
          if(result.hasOwnProperty('collectedfrom')) {
            if(Array.isArray(result['collectedfrom'])) {
              // not sure if this is correct mapping
              instance['collectedfrom'] = result['collectedfrom'].length >= j ? result['collectedfrom'][j] : result['collectedfrom'][result['collectedfrom'].length - 1];
            } else {
              instance['collectedfrom'] = result['collectedfrom'];
            }
          }

          parsingFunctions.parseTypes(preview.types, types, instance);

          if (instance.hasOwnProperty("webresource")) {
            let url;
            if (!Array.isArray(instance['webresource'])) {
              url = instance['webresource'].url;
            } else {
              url = instance['webresource'][0].url;
            }
            if (url.includes('&amp;')) {
              url = url.replace(/&amp;/gmu, '&');
            }

            /**********************************************************/
            if (instance.hasOwnProperty("hostedby")) {
              parsingFunctions.parseHostedBy_collectedFrom(preview.hostedBy_collectedFrom, instance, url, null);
            }
            /**********************************************************/
          }
        }

      //   /* Order Download from via openness*/
        preview.hostedBy_collectedFrom.sort(parsingFunctions.compareHostedByCollectedFrom);
      }

      this.resultsPreview.push(preview);
    }
    this.status = this.errorCodes.DONE;
  }
  
  public getResultPreview(result: ResultLandingInfo): ResultPreview {
    return ResultPreview.resultLandingInfoConvert(result, this.resultType);
  }
  
  public totalPages(totalResults: number): number {
    let totalPages: any = totalResults / this.pageSize;
    if (!(Number.isInteger(totalPages))) {
      totalPages = (parseInt(totalPages, this.pageSize) + 1);
    }
    return totalPages;
  }
  
  public updatePage($event) {
    this.page = $event.value;
    HelperFunctions.scrollToId("versions_container");
  }
}
