import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {SharedModule} from '../../shared/shared.module';
import {LandingModule} from '../landing-utils/landing.module';
import {CiteThisModule} from '../landing-utils/citeThis/citeThis.module';
import {IFrameModule} from '../../utils/iframe.module';
import {AltMetricsModule} from '../../utils/altmetrics.module';
import {PagingModule} from '../../utils/paging.module';

import {ResultLandingService} from './resultLanding.service';
import {ResultLandingComponent} from './resultLanding.component';
import {Schema2jsonldModule} from '../../sharedComponents/schema2jsonld/schema2jsonld.module';
import {SEOServiceModule} from '../../sharedComponents/SEO/SEOService.module';

import {DeletedByInferenceModule} from './deletedByInference/deletedByInference.module';
import {ShowAuthorsModule} from "../../utils/authors/showAuthors.module";
import {HelperModule} from "../../utils/helper/helper.module";
import {ResultLandingUtilsModule} from "../landing-utils/resultLandingUtils.module";
import {AlertModalModule} from "../../utils/modal/alertModal.module";
import {LandingHeaderModule} from "../landing-utils/landing-header/landing-header.module";
import {NoLoadPaging} from "../../searchPages/searchUtils/no-load-paging.module";
import {ResultPreviewModule} from "../../utils/result-preview/result-preview.module";
import {FeedbackModule} from "../feedback/feedback.module";
import {TabsModule} from "../../utils/tabs/tabs.module";
import {LoadingModule} from "../../utils/loading/loading.module";
import {IconsModule} from "../../utils/icons/icons.module";
import {IconsService} from "../../utils/icons/icons.service";
import {cite, fire, graph, landmark, link, link_to, quotes, rocket, versions} from "../../utils/icons/icons";
import {InputModule} from "../../sharedComponents/input/input.module";
import {EGIDataTransferModule} from "../../utils/dataTransfer/transferData.module";
import {RecaptchaModule} from 'ng-recaptcha';
import {SdgFosSuggestModule} from '../landing-utils/sdg-fos-suggest/sdg-fos-suggest.module';
import {FullScreenModalModule} from "../../utils/modal/full-screen-modal/full-screen-modal.module";
import {SafeHtmlPipeModule} from '../../utils/pipes/safeHTMLPipe.module';
import {EntityActionsModule} from "../../utils/entity-actions/entity-actions.module";
import {ResultLandingRoutingModule} from "./resultLanding-routing.module";
import {OrcidCoreModule} from "../../orcid/orcid-core.module";
import {SearchTabModule} from "../../utils/tabs/contents/search-tab.module";
import {RecommendationsService} from '../../recommendations/recommendations.service';
import {RecommendationCardModule} from '../../recommendations/recommendation-card.module';

@NgModule({
  imports: [
    CommonModule, FormsModule, LandingModule, SharedModule, RouterModule,
    ResultLandingRoutingModule,
    CiteThisModule, PagingModule, IFrameModule,
    AltMetricsModule, Schema2jsonldModule, SEOServiceModule,
    DeletedByInferenceModule, ShowAuthorsModule, HelperModule, ResultLandingUtilsModule, AlertModalModule,
    LandingHeaderModule, NoLoadPaging, ResultPreviewModule, FeedbackModule, TabsModule, LoadingModule,
    OrcidCoreModule, IconsModule, InputModule, EGIDataTransferModule, RecaptchaModule,
    SdgFosSuggestModule, FullScreenModalModule, SafeHtmlPipeModule, EntityActionsModule, RecommendationCardModule
  ],
  declarations: [
    ResultLandingComponent
  ],
  providers: [
    ResultLandingService, RecommendationsService
  ],
  exports: [
    ResultLandingComponent
  ]
})
export class ResultLandingModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([link, graph, quotes, cite, link_to, versions, rocket, fire, landmark])
  }
}
