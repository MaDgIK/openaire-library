import {ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Meta, Title} from '@angular/platform-browser';

import {EnvProperties} from '../../utils/properties/env-properties';
import {Id, ResultLandingInfo} from '../../utils/entities/resultLandingInfo';
import {RouterHelper} from '../../utils/routerHelper.class';

import {PiwikService} from '../../utils/piwik/piwik.service';
import {ResultLandingService} from './resultLanding.service';
import {SEOService} from '../../sharedComponents/SEO/SEO.service';
import {HelperFunctions} from '../../utils/HelperFunctions.class';
import {HelperService} from '../../utils/helper/helper.service';
import {Location} from "@angular/common";
import {MetricsService} from "../../services/metrics.service";
import {RelationDatasource, RelationResult, ResultPreview} from "../../utils/result-preview/result-preview";
import {IndexInfoService} from "../../utils/indexInfo.service";
import {Identifier, StringUtils} from "../../utils/string-utils.class";
import {properties} from "../../../../environments/environment";
import {ISVocabulariesService} from "../../utils/staticAutoComplete/ISVocabularies.service";
import {Subscription} from "rxjs";
import {ParsingFunctions} from "../landing-utils/parsingFunctions.class";
import {ConnectHelper} from "../../connect/connectHelper";
import {UserManagementService} from "../../services/user-management.service";
import {OpenaireEntities} from "../../utils/properties/searchFields";
import {Option} from "../../sharedComponents/input/input.component";
import {NumberUtils} from '../../utils/number-utils.class';
import {FullScreenModalComponent} from "../../utils/modal/full-screen-modal/full-screen-modal.component";
import {SdgFosSuggestComponent} from '../landing-utils/sdg-fos-suggest/sdg-fos-suggest.component';
import {LayoutService} from "../../dashboard/sharedComponents/sidebar/layout.service";
import {ContextsService} from "../../claims/claim-utils/service/contexts.service";
import {RecommendationsService} from '../../recommendations/recommendations.service';

declare var ResizeObserver;

@Component({
  selector: 'result-landing',
  templateUrl: 'resultLanding.component.html',
})
export class ResultLandingComponent {
  public referrer: string;
  public prevPath: string;

  @Input() type: string = "publication";

  @Input() communityId = null;
  @Output() result: EventEmitter<ResultLandingInfo> = new EventEmitter<ResultLandingInfo>();
  enermapsId;
  // @ViewChild('linkModal') linkModal;
  @ViewChild('citeModal') citeModal;
  @ViewChild('citeFsModal') citeFsModal;
  @ViewChild('addThisModal') addThisModal;
  @ViewChild('addThisFsModal') addThisFsModal;
  @ViewChild('AlertModalDeletedByInference') alertModalDeletedByInference;
  @ViewChild('AlertModalDeletedByInferenceFS') alertModalDeletedByInferenceFS;
  // @ViewChild('relationModal') relationModal;
  public deleteByInferenceOpened: boolean = false;
  @Input() public resultFromInput: boolean = false;
  @Input() public resultLandingInfo: ResultLandingInfo;
  public belongsInEOSC: boolean = false;
  public supplementaryResults: RelationResult[];
  public relation: string = 'trust';
  public id: string;
  public title: string;

  /*Show all organizations*/
  public viewAllOrganizations: boolean = false;
  public lessBtnOrganizations: boolean = false;
  public thresholdOrganizations: number = 5;
  @ViewChild('organizationsModal') organizationsModal;


  // Links for SEO
  public linkToLandingPage: string = null;
  public canonicalUrl: string = null;
  public linkToSearchPage: string = null;

  public citeThisClicked: boolean;
  public addThisClicked: boolean;
  public descriptionClicked: boolean;

  // Metrics tab variables
  public impactClicked: boolean;
  public altMetricsClicked: boolean;
  public metricsClicked: boolean;
  public hasAltMetrics: boolean = false;
  public viewsFrameUrl: string;
  public downloadsFrameUrl: string;
  /** @deprecated*/
  public totalViews: number = null;
  /** @deprecated*/
  public totalDownloads: number = null;
  public hasViews: boolean = false;
  public hasDownloads: boolean = false;
  public pageViews: number = null;
	public bipFrameUrl: string;

  // Custom tab paging variables
  public referencesPage: number = 1;
  public bioentitiesPage: number = 1;

  public pageSize: number = 10;

  // Map counting variables
  public bioentitiesNum: number = 0;
  public relatedResultsNum: number = 0;

  // Message variables
  public errorMessage = "";
  public recommendationError = "";
  public showLoading: boolean = true;

  public routerHelper: RouterHelper = new RouterHelper();
  public activeTab: string = null;
  private reloadEnermapsDetails: boolean = true;
  public enermapsDetails: any;

  private doi: string;
  subscriptions = [];
  properties: EnvProperties = properties;
  public openaireEntities = OpenaireEntities;

  public indexUpdateDate: Date;
  public pageContents = null;
  public divContents = null;
  public showFeedback: boolean = false;
  public feedbackPreSelectedField: string = "";
  public feedbackFields: string [] = [
    'Title', 'Authors', 'Access rights',
    'Publisher information', 'Funding Information',
    'Persistent identifiers', 'Sustainable Development Goals (SDGs)',
		'Fields of Science (FoS)', 'Other'];

  public pidsArrayString: string = "";
  public identifier: Identifier;

  public isLoggedIn: boolean = false;
  public pid: string;
	public contextsWithLink: any;

  public relatedResults: {
    classFilters: Option[],
    selectedClass: string,
    page: number
  } = {
    classFilters: [{"label": "All relations", "value": ""}],
    selectedClass: "",
    page: 1,
  };
  filteredRelatedResults: RelationResult[];

  public relatedServices: {
    classFilters: Option[],
    selectedClass: string,
    page: number
  } = {
    classFilters: [{"label": "All relations", "value": ""}],
    selectedClass: "",
    page: 1
  }
  filteredRelatedServices: RelationDatasource[];


	public provenanceActionVocabulary = null;
	public relationsVocabulary = null;

  public offset: number;
  public stickyHeader: boolean = false;
  public graph_offset: number = 0;
  public graph_height: number = 0;
  @ViewChild("graph_and_feedback") graph_and_feedback;

	@ViewChild("descriptionDiv") descriptionDiv: ElementRef;
	@ViewChild('descriptionModal') descriptionModal;

  public mobileContent: "info" | "metrics" | "actions" = "info";
  public tabMobile: string = "";
  public viewAllMobile: string = "";
  public viewAll: string = "";
	@ViewChild("sdgFosSuggest") sdgFosSuggest: SdgFosSuggestComponent;

  public isMobile: boolean = false;
  public isBottomIntersecting: boolean = false;
  // Full screen modals for small screens (mobile)
  @ViewChild('summaryFsModal') summaryFsModal: FullScreenModalComponent;
  @ViewChild('subjectsFsModal') subjectsFsModal: FullScreenModalComponent;
  @ViewChild('referencesFsModal') referencesFsModal: FullScreenModalComponent;
  @ViewChild('relatedResearchFsModal') relatedResearchFsModal: FullScreenModalComponent;
  @ViewChild('servicesFsModal') servicesFsModal: FullScreenModalComponent;
  @ViewChild('bioentitiesFsModal') bioentitiesFsModal: FullScreenModalComponent;
  @ViewChild('compatibleEOSCFsModal') compatibleEOSCFsModal: FullScreenModalComponent;
  @ViewChild('fundedByFsModal') fundedByFsModal: FullScreenModalComponent;
  @ViewChild('relatedCommunitiesFsModal') relatedCommunitiesFsModal: FullScreenModalComponent;
  @ViewChild('enermapsToolFsModal') enermapsToolFsModal: FullScreenModalComponent;
  @ViewChild('recommendedFsModal') recommendedFsModal: FullScreenModalComponent;
  @ViewChild('sdgsFsModal') sdgsFsModal: FullScreenModalComponent;
  @ViewChild('fosFsModal') fosFsModal: FullScreenModalComponent;

  public noCommunities: boolean = false;
  private promise: Promise<void> = null;
  private publicCommunities: string[] = [];

  public rightSidebarOffcanvasClicked: boolean = false;
  public egiTransferModalOpen = false;

  recommendedResults;

  constructor(private _resultLandingService: ResultLandingService,
              private _vocabulariesService: ISVocabulariesService,
              private _piwikService: PiwikService,
              private route: ActivatedRoute,
              private router: Router,
              private _meta: Meta,
              private _title: Title,
              private _router: Router,
              private helper: HelperService,
              private seoService: SEOService,
              private metricsService: MetricsService,
              private cdr: ChangeDetectorRef,
              private _location: Location,
              private indexInfoService: IndexInfoService,
              private userManagementService: UserManagementService,
              private layoutService: LayoutService,
              private _contextService: ContextsService,
              private recommendationsService: RecommendationsService) {
    if(route.snapshot.data && route.snapshot.data['type']) {
      this.type = route.snapshot.data['type'];
    }
  }

  ngOnInit() {
    this.subscriptions.push(this.layoutService.isMobile.subscribe(isMobile => {
      this.isMobile = isMobile;
      this.cdr.detectChanges();
    }));
    this.subscriptions.push(this.layoutService.isBottomIntersecting.subscribe(isBottomIntersecting => {
      this.isBottomIntersecting = isBottomIntersecting;
      this.cdr.detectChanges();
    }));
    this.subscriptions.push(this.userManagementService.getUserInfo().subscribe(user => {
      this.isLoggedIn = !!user;
    }, error => { this.isLoggedIn = false}  ));
    if (typeof document !== 'undefined') {
      this.subscriptions.push(this.indexInfoService.getLastIndexDate(this.properties).subscribe(lastIndexUpdate => {
        if (lastIndexUpdate) {
          this.indexUpdateDate = new Date(lastIndexUpdate);
        }
      }));
    }
    if(properties.adminToolsPortalType !== "explore") {
      //this.getDivContents();
      this.getPageContents();
    }
    this.updateUrl(this.properties.domain +this.properties.baseLink +  this._router.url);
    this.seoService.createLinkForCanonicalURL(this.properties.domain +this.properties.baseLink +  this._router.url);
    this.subscriptions.push(this.route.queryParams.subscribe(data => {
      this.stickyHeader = false;
      if (data['articleId']) {
        this.id = data['articleId'];
        this.initMetaAndLinks("publication");
      } else if (data['datasetId']) {
        this.id = data['datasetId'];
        this.initMetaAndLinks("dataset");
      } else if (data['softwareId']) {
        this.id = data['softwareId'];
        this.initMetaAndLinks("software");
      } else if (data['orpId']) {
        this.id = data['orpId'];
        this.initMetaAndLinks("orp");
      } else if (data["id"]) {
        this.id = data["id"];
        this.initMetaAndLinks("result");
      } else if (data["pid"]) {
        this.identifier = Identifier.getIdentifierFromString(decodeURIComponent(data["pid"]),false);

        if(!this.type) {
          this.type = "result";
        }
        this.initMetaAndLinks(this.type);
      }
      if(data["egiTransfer"] && data["egiTransfer"] == 't'){
        this.mobileContent = 'actions';
        this.egiTransferModalOpen = true;
      }
      this.updateDescription("");
      if(data["return_path"]) {
        this.prevPath = data["return_path"] + (data["search_params"] ? ("?"+data["search_params"]) : "");
      }
      if((typeof document !== 'undefined') && document.referrer) {
        this.referrer = document.referrer;
      }

      this.metricsClicked = false;
      if(this.resultFromInput){
        this.id = this.resultLandingInfo.objIdentifier;
        this.type="publication";
      }
      if ((this.id && StringUtils.isOpenAIREID(this.id)) || (this.identifier)) {
        this.getPublicCommunities();
        this.getVocabulariesAndResultLandingInfo();
      } else {
        this.showLoading = false;

        this._router.navigate([this.properties.errorLink], {
          queryParams: {
            "page": this._location.path(true),
            "page_type": this.type
          }
        });
      }

      this.scroll();
    }));
  }

  ngAfterViewInit() {
    if (typeof document !== 'undefined') {
      // if(properties.adminToolsPortalType !== 'eosc') {
      if(document.getElementById("main-menu")) {
        this.offset = Number.parseInt(getComputedStyle(document.documentElement).getPropertyValue('--header-height'));
      } else {
        this.offset = 0;
      }
      if(this.graph_and_feedback) {
        this.observeGraphAndFeedback();
      }
    }
  }

  ngAfterContentChecked() {
    if(this.graph_and_feedback && typeof document !== 'undefined') {
      this.graph_offset = this.calcGraphOffset(this.graph_and_feedback.nativeElement);
    }
  }

	get showViewMoreButton():boolean {
		return !!this.descriptionDiv && (this.descriptionDiv.nativeElement.clientHeight >= 10 * 21);
	}

  public observeGraphAndFeedback() {
    let resizeObs = new ResizeObserver(entries => {
      entries.forEach(entry => {
        setTimeout(() => {
          // console.log(entry);
          this.graph_offset = this.calcGraphOffset(entry.target);
          this.cdr.detectChanges();
        });
      })
    });
    this.subscriptions.push(resizeObs);
    resizeObs.observe(this.graph_and_feedback.nativeElement);
  }

  calcGraphOffset(element) {
    this.graph_height = element.offsetHeight;
    return window.innerHeight-this.graph_height;
  }

  private initMetaAndLinks(type: string) {
    if (type == "publication") {
      this.type = "publication";
      this.updateTitle(this.openaireEntities.PUBLICATION);
      this.linkToLandingPage = this.properties.searchLinkToPublication;
      this.linkToSearchPage = this.properties.searchLinkToPublications;
      this.title = this.openaireEntities.PUBLICATION;
    } else if (type == "dataset") {
      this.updateTitle(this.openaireEntities.DATASET);
      this.type = "dataset";
      this.linkToLandingPage = this.properties.searchLinkToDataset;
      this.linkToSearchPage = this.properties.searchLinkToDatasets;
      this.title = this.openaireEntities.DATASET;
    } else if (type == "software") {
      this.updateTitle(this.openaireEntities.SOFTWARE_SINGULAR);
      this.type = "software";
      this.linkToLandingPage = this.properties.searchLinkToSoftwareLanding;
      this.linkToSearchPage = this.properties.searchLinkToSoftware;
      this.title = this.openaireEntities.SOFTWARE_SINGULAR;
    } else if (type == "orp") {
      this.type = "orp";
      this.updateTitle(this.openaireEntities.OTHER_SINGULAR);
      this.linkToLandingPage = this.properties.searchLinkToOrp;
      this.linkToSearchPage = this.properties.searchLinkToOrps;
      this.title = this.openaireEntities.OTHER_SINGULAR;
    } else if (type == "result") {
      this.type = "result";
      this.updateTitle(this.openaireEntities.RESULT);
      this.linkToLandingPage = this.properties.searchLinkToResult;
      this.linkToSearchPage = this.properties.searchLinkToResults;
      this.title = this.openaireEntities.RESULT;
    }
  }

  private getPageContents() {
    if (this.communityId) {
      this.subscriptions.push(this.helper.getPageHelpContents(this.properties, this.communityId, this._router.url).subscribe(contents => {
        this.pageContents = contents;
      }));
    }
  }

  private getDivContents() {
    if (this.communityId) {
      this.subscriptions.push(this.helper.getDivHelpContents(this.properties, this.communityId, this._router.url).subscribe(contents => {
        this.divContents = contents;
      }));
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscription) {
        subscription.unsubscribe();
      } else if (subscription instanceof Function) {
        subscription();
      } else if ((typeof ResizeObserver != 'undefined' && subscription instanceof ResizeObserver) ||
          (typeof IntersectionObserver != 'undefined' && subscription instanceof IntersectionObserver)) {
        subscription.disconnect();
      }
    });
    this._vocabulariesService.clearSubscriptions();
  }

  public pidInit(event) {
    this.pid = event;
    this.cdr.detectChanges();
  }


  public getTypeName(): string {
    return StringUtils.getEntityName(this.type, false);
  }

  public removeUnknown(array: string[], type: boolean = false): string[] {
    if (type) {
      return this.removeDuplicates(array).filter(value => value.toLowerCase() !== 'unknown');
    } else {
      return array.filter(value => value.toLowerCase() !== 'unknown');
    }
  }

  public removeDuplicates(array: string[]): string[] {
    let type = this.getTypeName();
    return array.filter(value => value.toLowerCase() !== type);
  }

  private getVocabulariesAndResultLandingInfo() {
    this.errorMessage = '';
    this.showLoading = true;

    if (typeof document !== 'undefined') {
			this.subscriptions.push(this._vocabulariesService.getProvenanceActionVocabulary(this.properties).subscribe(
				data => {
					this.provenanceActionVocabulary = data;
				}, error => {
					this.handleError("Error getting provenance action vocabulary for " + this.type, error);
				}
			));
			this.subscriptions.push(this._vocabulariesService.getSubjectsVocabulary(this.properties).subscribe(
				data => {
					this.getResultLandingInfo(data);
				}, error => {
					this.getResultLandingInfo(null);
					this.handleError("Error getting subjects vocabulary for " + this.type, error);
				}
			));
			this.subscriptions.push(this._vocabulariesService.getRelationsVocabulary(this.properties).subscribe(
				data => {
					this.relationsVocabulary = data;
				}, error => {
					this.handleError("Error getting relations vocabulary for " + this.type, error);
				}
			));
    } 
		else {
			this.getResultLandingInfo(null);
		}
  }

  private setActiveTab() {
    if (this.hasPrimaryInfo) {
      this.activeTab = 'summary';
    } else if (this.resultLandingInfo.references && this.resultLandingInfo.references.length > 0) {
      this.activeTab = 'references';
      // } else if ((this.resultLandingInfo.supplementaryResearchResults && this.resultLandingInfo.supplementaryResearchResults.length > 0) ||
      //   (this.resultLandingInfo.supplementedByResearchResults && this.resultLandingInfo.supplementedByResearchResults.length > 0)) {
      //   this.activeTab = 'supplementary';
      // } else if ((this.resultLandingInfo.relatedResearchResults && this.resultLandingInfo.relatedResearchResults.length > 0) ||
      //   (this.resultLandingInfo.similarResearchResults && this.resultLandingInfo.similarResearchResults.length > 0)) {
      //   this.activeTab = 'related';
    } else if (this.resultLandingInfo.relatedResults && this.resultLandingInfo.relatedResults.length > 0) {
      this.activeTab = "all_related";
    } else if (this.resultLandingInfo.bioentities && this.bioentitiesNum > 0) {
      this.activeTab = 'bioentities';
    } else if(this.enermapsId && this.properties.enermapsURL) {
      this.activeTab = "enermaps";
    }
  }

  // private get numberOfTabs(): number {
  //   let numberOfTabs = 0;
  //   if(this.hasPrimaryInfo || this.hasSecondaryInfo) {
  //     numberOfTabs++;
  //   }
  //   if(this.resultLandingInfo.references && this.resultLandingInfo.references.length > 0) {
  //     numberOfTabs++;
  //   }
  //   if((this.resultLandingInfo.supplementaryResearchResults && this.resultLandingInfo.supplementaryResearchResults.length > 0) ||
  //     (this.resultLandingInfo.supplementedByResearchResults && this.resultLandingInfo.supplementedByResearchResults.length > 0)) {
  //     numberOfTabs++;
  //   }
  //   if((this.resultLandingInfo.relatedResearchResults && this.resultLandingInfo.relatedResearchResults.length > 0) ||
  //     (this.resultLandingInfo.similarResearchResults && this.resultLandingInfo.similarResearchResults.length > 0)) {
  //     numberOfTabs++;
  //   }
  //   if(this.resultLandingInfo.bioentities && this.bioentitiesNum > 0) {
  //     numberOfTabs++;
  //   }
  //   return numberOfTabs;
  // }

  private getResultLandingInfo(subjectsVocabulary?: any) {
    if(this.resultFromInput && this.resultLandingInfo){
      this.showLoading = false;
      return;
    }
    this.resultLandingInfo = null;
    this.hasAltMetrics = false;
    this.subscriptions.push(this._resultLandingService.getResultLandingInfo(this.id, this.identifier, this.type, subjectsVocabulary, this.properties).subscribe(
      async data => {
        let contexts = data.contexts;
        data.contexts = [];

        this.resultLandingInfo = data;
        this.id = this.resultLandingInfo.objIdentifier;
        let typeId = this.identifier?.id?'pid':'id';
        let id = this.identifier?.id?this.identifier.id:this.id;
        this.resultLandingInfo.setBelongsTo(typeId, id);
        //old
        // this.viewsFrameUrl = this.properties.framesAPIURL + 'merge.php?com=query&data=[{"query":"resRepoViews", "resTitle":"' + this.id + '", "table":"","fields":[{"fld":"sum","agg":"sum","type":"column","yaxis":1,"c":false}],"xaxis":{"name":"month","agg":"sum"},"group":" ","color":"","type":"chart","size":200,"sort":"xaxis","xStyle":{"r":-30,"s":"6","l":"-","ft":"-","wt":"-"},"title":"","subtitle":"","xaxistitle":"Repository","yaxisheaders":["Monthly views"],"generalxaxis":"","theme":0,"in":[],"filters":[{"name":"","values":[""],"to":"-1"}]}]&info_types=["column"]&stacking=normal&steps=false&fontFamily=Courier&spacing=[5,0,0,0]&style=[{"color":"rgba(0, 0, 0, 1)","size":"18"},{"color":"rgba(0, 0, 0, 1)","size":"18"},{"color":"000000","size":""},{"color":"000000","size":""}]&backgroundColor=rgba(255,255,255,1)&colors[]=rgba(67, 67, 72, 1)&colors[]=rgba(144, 237, 125, 1)&colors[]=rgba(247, 163, 92, 1)&colors[]=rgba(128, 133, 233, 1)&colors[]=rgba(241, 92, 128, 1)&colors[]=rgba(228, 211, 84, 1)&colors[]=rgba(43, 144, 143, 1)&colors[]=rgba(244, 91, 91, 1)&colors[]=rgba(145, 232, 225, 1)&xlinew=0&ylinew=1&legends=true&tooltips=true&persistent=false';
        // this.downloadsFrameUrl = this.properties.framesAPIURL + 'merge.php?com=query&data=[{"query":"resRepoDownloads", "resTitle":"' + this.id + '", "table":"","fields":[{"fld":"sum","agg":"sum","type":"column","yaxis":1,"c":false}],"xaxis":{"name":"month","agg":"sum"},"group":" ","color":"","type":"chart","size":200,"sort":"xaxis","xStyle":{"r":-30,"s":"6","l":"-","ft":"-","wt":"-"},"title":"","subtitle":"","xaxistitle":"Repository","yaxisheaders":["Monthly downloads"],"generalxaxis":"","theme":0,"in":[],"filters":[{"name":"","values":[""],"to":"-1"}]}]&info_types=["column"]&stacking=normal&steps=false&fontFamily=Courier&spacing=[5,0,0,0]&style=[{"color":"rgba(0, 0, 0, 1)","size":"18"},{"color":"rgba(0, 0, 0, 1)","size":"18"},{"color":"000000","size":""},{"color":"000000","size":""}]&backgroundColor=rgba(255,255,255,1)&colors[]=rgba(67, 67, 72, 1)&colors[]=rgba(144, 237, 125, 1)&colors[]=rgba(247, 163, 92, 1)&colors[]=rgba(128, 133, 233, 1)&colors[]=rgba(241, 92, 128, 1)&colors[]=rgba(228, 211, 84, 1)&colors[]=rgba(43, 144, 143, 1)&colors[]=rgba(244, 91, 91, 1)&colors[]=rgba(145, 232, 225, 1)&xlinew=0&ylinew=1&legends=true&tooltips=true&persistent=false';
        //new
        this.viewsFrameUrl = this.properties.statisticsFrameNewAPIURL + 'chart?json=' + encodeURIComponent('{"library":"HighCharts","chartDescription":{"queries":[{"name":"Monthly views","type":"column","query":{"name":"usagestats.results.views.monthly", "parameters":["' + this.id + '"], "profile":"OpenAIRE All-inclusive" }}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Monthly views"},"subtitle":{},"yAxis":{"title":{"text":""}},"xAxis":{"title":{}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":false},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":true,"text":""}}}');
        this.downloadsFrameUrl = this.properties.statisticsFrameNewAPIURL + 'chart?json=' +
          encodeURIComponent('{"library":"HighCharts","chartDescription":{"queries":[{"name":"Monthly downloads","type":"column","query":{"name":"usagestats.results.downloads.monthly", "parameters":["' + this.id + '"], "profile":"OpenAIRE All-inclusive" }}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Monthly downloads"},"subtitle":{},"yAxis":{"title":{"text":""}},"xAxis":{"title":{}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":false},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":true,"text":""}}}');
        this.bipFrameUrl = this.properties.bipFrameAPIURL + this.id + (properties.environment == "beta" ? "&src=beta" : "");
        let pid: Identifier = Identifier.getPIDFromIdentifiers(this.resultLandingInfo.identifiers);
        if (this.type == "result") { // no type was specified - update URL based this.resultLandingInfo.resultType
          this.updateUrlWithType(pid);
        }
        this.canonicalUrl = this.properties.domain + properties.baseLink + (pid ? (this.linkToLandingPage.split("?")[0] + "?pid=" + encodeURIComponent(pid.id)) :
          (this.linkToLandingPage + this.resultLandingInfo.relcanId));
        this.seoService.createLinkForCanonicalURL(this.canonicalUrl);
        this.updateUrl(this.canonicalUrl);
        this.addNoIndexFilter();
        if (this.resultLandingInfo.title) {
          this.updateTitle(this.resultLandingInfo.title);
          this.updateDescription((this.resultLandingInfo.description ? (this.resultLandingInfo.description.substr(0, 157) + (this.resultLandingInfo.description.substr(0, 157).length == 157 ? "..." : "")) : (this.resultLandingInfo.title)));
        }
        this.subscriptions.push(this._piwikService.trackViewForCustomUrl(this.properties, this.resultLandingInfo.title, this.linkToLandingPage.split("?")[1] + this.id).subscribe());
        let bioentitiesNum = 0;
        if (this.resultLandingInfo.bioentities != undefined) {
          this.resultLandingInfo.bioentities.forEach(function (value, key, map) {
            bioentitiesNum += value.size;
          });
        }
        this.bioentitiesNum = bioentitiesNum;
        if (typeof document !== 'undefined') {
          if (this.resultLandingInfo.identifiers) {
            let pidsArray: string[] = [];
            for (let key of Array.from(this.resultLandingInfo.identifiers.keys())) {
              pidsArray = pidsArray.concat(this.resultLandingInfo.identifiers.get(key));
              this.pidsArrayString = pidsArray.join();
            }
            if (this.resultLandingInfo.identifiers.has('doi')) {
              this.doi = this.resultLandingInfo.identifiers.get('doi')[0];
              this.subscriptions.push(this.metricsService.hasAltMetrics(this.properties.altMetricsAPIURL, this.doi).subscribe(hasAltMetrics => {
                this.hasAltMetrics = hasAltMetrics;
              }, error => {
                this.hasAltMetrics = false;
              }));
            }
          }
        }
        this.relatedResults.classFilters = [{"label": "All relations", "value": ""}];
        if (this.resultLandingInfo.relatedClassFilters.size > 1) {
          for (let relClass of this.resultLandingInfo.relatedClassFilters) {
            this.relatedResults.classFilters.push({
              "label": HelperFunctions.getVocabularyLabel(relClass, this.relationsVocabulary),
              "value": relClass
            });
          }
        } else {
          this.relatedResults.classFilters.pop();
        }
        this.relatedResults.selectedClass = "";
        this.filteredRelatedResults = this.resultLandingInfo.relatedResults;

        this.relatedServices.classFilters = [{"label": "All relations", "value": ""}];
        if (this.resultLandingInfo.relatedServicesClassFilters.size > 1) {
          for (let relClass of this.resultLandingInfo.relatedServicesClassFilters) {
            this.relatedServices.classFilters.push({
              "label": HelperFunctions.getVocabularyLabel(relClass, this.relationsVocabulary),
              "value": relClass
            });
          }
        } else {
          this.relatedServices.classFilters.pop();
        }
        this.relatedServices.selectedClass = "";
        this.filteredRelatedServices = this.resultLandingInfo.relatedServices

        this.hasViews = false;
        this.hasDownloads = false;
        if (this.resultLandingInfo.measure && this.resultLandingInfo.measure.counts) {
          this.resultLandingInfo.measure.counts.forEach(measure => {
            if (measure.name == "views" && measure.value > 0) {
              this.hasViews = true;
            }
            if (measure.name == "downloads" && measure.value > 0) {
              this.hasDownloads = true;
            }
          })
        }

        this.showLoading = false;
        this.setActiveTab();

        this.cdr.detectChanges();
        if (contexts) {
          if (this.communityId && this.communityId == "enermaps" && properties.enermapsURL) {
            this.enermapsId = ParsingFunctions.getEnermapsConceptId(contexts);
          }
          await this.promise;
          if(this.publicCommunities && this.publicCommunities.length > 0) {
            this.resultLandingInfo.contexts = contexts.filter(context => {
              if(context.idContext == "eosc") {
                this.belongsInEOSC = true;
              }
              return this.publicCommunities.includes(context.idContext)
            });
            this.cdr.detectChanges();
          }
        }
      },
      err => {
        this.handleError("Error getting " + this.type + " for " + (this.id ? ("id: " + this.id) : ("pid: " + this.identifier.id)), err);
        if (err.status == 404) {
          this._router.navigate([this.properties.errorLink], {queryParams: {"page": this._location.path(true), "page_type": this.type}});
        }else if(err.name == "TimeoutError"){
          this.errorMessage = 'An error occurred please try again later';
        }else{
          this.errorMessage = "No " + this.getTypeName() + " found";
          this.seoService.createLinkForCanonicalURL(this.properties.domain+ properties.baseLink  + this.linkToSearchPage);
        }
        this.showLoading = false;
      }
    ));
  }

  public getPublicCommunities() {
    this.promise = new Promise<void>(resolve => {
      this._contextService.getPublicCommunitiesByState().subscribe(
        data => {
          this.publicCommunities = data.map(value => value.id);
          // this.publicCommunities = data;
          resolve();
        },
        error => {
          this.handleError("Error getting communities status", error);
          resolve();
        });
    });
  }

  public metricsResults($event) {
    this.totalViews = $event.totalViews;
    this.totalDownloads = $event.totalDownloads;
    this.pageViews = $event.pageViews;
  }

  public get hasPrimaryInfo(): boolean {
        return !!this.resultLandingInfo && (
      (!!this.resultLandingInfo.description && this.resultLandingInfo.description.length > 0)
      || (!!this.resultLandingInfo.organizations && this.resultLandingInfo.organizations.length > 0));
  }

  public get hasRightSidebarInfo(): boolean {
    return (this.resultLandingInfo.eoscSubjects && this.resultLandingInfo.eoscSubjects.length
        && properties.adminToolsPortalType == 'eosc')
      ||
      (this.resultLandingInfo.sdg && this.resultLandingInfo.sdg.length > 0)
      ||
      (this.resultLandingInfo.fos && this.resultLandingInfo.fos.length > 0)
      ||
      (this.resultLandingInfo.fundedByProjects && this.resultLandingInfo.fundedByProjects.length > 0)
      ||
      (this.resultLandingInfo.contexts && this.resultLandingInfo.contexts.length > 0 && !this.noCommunities)
			||
			(this.resultLandingInfo.measure && Object.keys(this.resultLandingInfo.measure).length > 0);
      // ||
      // (this.resultLandingInfo.hostedBy_collectedFrom && this.resultLandingInfo.hostedBy_collectedFrom.length > 0);
  }

  public get metricsCalculated(): boolean {
    return this.totalViews != null || this.totalDownloads != null || this.pageViews != null;
  }

  public get hasMetrics(): boolean {
    // return !(this.totalViews && this.totalDownloads && this.pageViews) || this.totalViews > 0 || this.totalDownloads > 0 || this.pageViews > 0;
    return !(this.totalViews != null && this.totalDownloads != null && this.pageViews != null) || this.totalViews > 0 || this.totalDownloads > 0 || this.pageViews > 0;
    // return (this.totalViews == null || this.totalViews > 0) || (this.totalDownloads && this.totalDownloads > 0) || (this.pageViews && this.pageViews > 0);
  }

  private updateDescription(description: string) {
    this._meta.updateTag({content: description}, "name='description'");
    this._meta.updateTag({content: description}, "property='og:description'");
  }

  private updateTitle(title: string) {
    var _prefix = "";
    // if(!this.communityId) {
    //   _prefix = "OpenAIRE | ";
    // }
    // var _title = _prefix + ((title.length > 50) ? title.substring(0, 50) : title);
    this._title.setTitle(title);
    this._meta.updateTag({content: title}, "property='og:title'");
  }

  private updateUrl(url: string) {
    this._meta.updateTag({content: url}, "property='og:url'");
  }

  public totalPages(totalResults: number): number {
    let totalPages: any = totalResults / this.pageSize;
    if (!(Number.isInteger(totalPages))) {
      totalPages = (parseInt(totalPages, this.pageSize) + 1);
    }
    return totalPages;
  }

  public updateReferencesPage($event) {
    this.referencesPage = $event.value;
    this.scrollToTabTop('references');
  }

  public updateBioentitiesPage($event) {
    this.bioentitiesPage = $event.value;
    this.scrollToTabTop('bioentities');
  }

  public updateRelatedPage($event, type) {
    if(type == "datasource") {
      this.relatedServices.page = $event.value;
      this.scrollToTabTop("dataProviders");
    } else {
      this.relatedResults.page = $event.value;
      this.scrollToTabTop('all_related');
    }
  }

  scrollToTabTop(tabId:string){
    setTimeout(() => {
      window.scrollTo({
        top: document.getElementById(tabId) ? document.getElementById(tabId).offsetTop - 250 : 250,
        behavior: 'smooth'
      });
    }, 200);
  }
  public accessClass(accessMode: string): string {
    if (accessMode.toLowerCase().indexOf('open') !== -1) {
      return 'open';
    } else if (accessMode.toLowerCase() === 'not available') {
      return 'unknown';
    } else {
      return 'closed';
    }
  }

  public keysToArray(bioentities: Map<string, string>): string[] {
    let keys: string[] = [];
    bioentities.forEach(function (value, key, map) {
      keys.push(key);
    });
    return keys;
  }

  public getKeys(map) {
    return Array.from(map.keys());
  }

  public scroll() {
    HelperFunctions.scroll();
  }

  private handleError(message: string, error) {
    console.error(this.getTypeName() + " Landing Page: " + message, error);
  }

  isRouteAvailable(routeToCheck: string) {
    for (let i = 0; i < this.router.config.length; i++) {
      let routePath: string = this.router.config[i].path;
      if (routePath == routeToCheck) {
        return true;
      }
    }
    return false;
  }

  openDeletedByInference() {
    this.deleteByInferenceOpened = true;
    this.alertModalDeletedByInference.cancelButton = false;
    this.alertModalDeletedByInference.okButton = false;
    this.alertModalDeletedByInference.alertTitle = "Other versions";
    this.alertModalDeletedByInference.open();
  }

  public getResultPreview(result: RelationResult|RelationDatasource, type: string): ResultPreview {
    if(type == "datasource") {
      return ResultPreview.relationDatasourceConvert(<RelationDatasource>result);
    }
    return ResultPreview.relationResultConvert(<RelationResult>result);
  }

  updateUrlWithType(pid) {
    this.type = this.resultLandingInfo.resultType;

    if (this.type == "publication") {
      this.linkToLandingPage = this.properties.searchLinkToPublication;
      this.linkToSearchPage = this.properties.searchLinkToPublications;
    } else if (this.type == "dataset") {
      this.linkToLandingPage = this.properties.searchLinkToDataset;
      this.linkToSearchPage = this.properties.searchLinkToDatasets;
    } else if (this.type == "software") {
      this.linkToLandingPage = this.properties.searchLinkToSoftwareLanding;
      this.linkToSearchPage = this.properties.searchLinkToSoftware;
    } else if (this.type == "other") {
      this.type = "orp";
      this.linkToLandingPage = this.properties.searchLinkToOrp;
      this.linkToSearchPage = this.properties.searchLinkToOrps;
    }
    if(!this.identifier) {
      this._location.go(( pid ? (this.linkToLandingPage.split("?")[0] + "?pid=" + pid.id):
        (this.linkToLandingPage + this.id)) + this.getEoscParams());
    }
    // else {
    //   this._location.go(this.linkToLandingPage.split("?")[0] + "?pid=" + this.identifier.id);
    // }
  }

  public getReferenceUrl(id: Id): string {
    if (id.type === "doi") {
      return this.properties.doiURL + id.value;
    } else if (id.type === "pmc") {
      return this.properties.pmcURL + id.value;
    } else if (id.type === "pmid") {
      return this.properties.pmidURL + id.value;
    } else if (id.type === "handle") {
      return this.properties.handleURL + id.value;
    } else if (id.type === "swhid") {
      return this.properties.swhURL + id.value;
    } else {
      return null;
    }
  }

  public getReferenceIdName(id: Id): string {
    if (id.type === "doi") {
      return 'DOI'
    } else if (id.type === "pmc") {
      return 'Europe PMC'
    } else if (id.type === "pmid") {
      return 'PubMed';
    } else if (id.type === "handle") {
      return 'Handle.NET';
    } else if(id.type == "swhid") {
      return 'Software Heritage';
    } else {
      return null;
    }
  }

  // public openLinkModal() {
  //   this.linkModal.cancelButton = false;
  //   this.linkModal.okButton = false;
  //   this.linkModal.alertTitle = "Link this " + this.getTypeName() + " to";
  //   this.linkModal.open();
  // }

  public openCiteModal() {
    this.citeThisClicked = true;
    this.citeModal.cancelButton = false;
    this.citeModal.okButton = false;
    this.citeModal.alertTitle = "Cite this " + this.getTypeName();
    this.citeModal.open();
  }

  public openAddThisModal() {
    this.addThisClicked = true;
    this.addThisModal.cancelButton = false;
    this.addThisModal.okButton = false;
    this.addThisModal.alertTitle = "Share this " + this.getTypeName() + " in your social networks";
    this.addThisModal.open();
  }

  private addNoIndexFilter() {
    let allow = this.checkIfAllowed(this.resultLandingInfo);
    if(!allow) {
      // console.log("no index");
      this._meta.updateTag({content: 'noindex'}, "name='robots'");
    }
  }

  checkIfAllowed(resultLandingInfo: ResultLandingInfo) {
    let publicCommunities: string[] = [ "covid-19" ];
    try {
      if(!(this.properties.environment == "production" || this.properties.environment == "development") ) {
        return true;
      }else {
         let allow =  !!(
             // !resultLandingInfo.underCurationMessage &&
           ((resultLandingInfo.fundedByProjects && resultLandingInfo.fundedByProjects.length > 0)
             || resultLandingInfo.journal
             || (resultLandingInfo.classifiedSubjects && resultLandingInfo.classifiedSubjects.size > 0)
               //allow free text keywords
             || (resultLandingInfo.otherSubjects && resultLandingInfo.otherSubjects.size > 0)
             || (resultLandingInfo.subjects && resultLandingInfo.subjects.length > 0)

             || (resultLandingInfo.organizations && resultLandingInfo.organizations.length > 0)
             || resultLandingInfo.bioentities || (resultLandingInfo.references && resultLandingInfo.references.length > 0)
             // || (resultLandingInfo.relatedResearchResults && resultLandingInfo.relatedResearchResults.length > 0)
             // || (resultLandingInfo.similarResearchResults && resultLandingInfo.similarResearchResults.length > 0)
             // || (resultLandingInfo.supplementaryResearchResults && resultLandingInfo.supplementaryResearchResults.length > 0)
             // || (resultLandingInfo.supplementedByResearchResults &&
             || (resultLandingInfo.relatedResults && resultLandingInfo.relatedResults.length > 0)
           ) 
           //&& (resultLandingInfo.description && resultLandingInfo.description[0] && resultLandingInfo.description[0].length >=50 &&  resultLandingInfo.description[0].length <=5000)
        );
        // console.log("rich content " + allow)
        //spam words to exclude - need to be in lower case

        let title_authors_words = ["movie","hd","film","kimetsu", "1080p","4k","call of duty", "mobile hack", "tubydi", "电影","電影","download ebook","download [ebook]","düşük hapı", "düşük hapi",
          "protocolo raikov", "top gun maverick streaming vf", "expensive candy full movie", "sigma game download", "阿凡達2線上看", "lack adam torrent magnet",
          "원피스 필름 레드 다시보기", "원피스 필름 레드 자막 다시보기", "gtatogel", "gta to gel"];
        let abstract_words = ["operacao-feliz-natal.blogspot.com", "moviedouban.site", "hack-expert-solution.link"];
        allow = allow &&  !(
          (this.hasKeyword(resultLandingInfo.title,title_authors_words) || (resultLandingInfo.authors && this.hasKeyword(resultLandingInfo.authors.map(o => o.fullName).join(" "),title_authors_words))
            || (resultLandingInfo.description && this.hasKeyword(resultLandingInfo.description,abstract_words))
          ) &&
          ((resultLandingInfo.publisher && resultLandingInfo.publisher.toLowerCase() == "zenodo") ||
            (resultLandingInfo.hostedBy_collectedFrom && resultLandingInfo.hostedBy_collectedFrom.filter(value => {
              return value.downloadNames && value.downloadNames.filter(name => {return name && name.toLowerCase().indexOf("zenodo") != 1}).length > 0
            }).length > 0)));
        // console.log("spam content " + allow)
        //common titles/ description / authors
        let common_titles = ["introduction", "editorial", "book reviews", "preface", "reviews", "none", "book review", "foreword", "conclusion", "review", "reply","einleitung","short notices","erratum","discussion", "letters to the editor","letter to the editor","reviews of books",":{unav)","editorial board"];
        let common_abstract = ["international audience","n/a","peer reviewed","national audience","info:eu-repo/semantics/published","-",".","graphical abstract","met lit. opg","international audience; no abstract",'<jats:p>.</jats:p>',"politics","info:eu-repo/semantics/publishedversion","copia digital. madrid : ministerio de educación, cultura y deporte, 2016",'<jats:p />',"peer-reviewed","copia digital. madrid : ministerio de educación, cultura y deporte. subdirección general de coordinación bibliotecaria, 2015","<jats:p>-</jats:p>","imperial users only","yüksek lisans"];
        let common_authors = ["[s.n.]","null &na;","nn","(:unap)","(:null)","null anonymous","anonymous"];
        allow = allow &&  !(
          this.isKeyword(resultLandingInfo.title,common_titles) || (resultLandingInfo.description && this.isKeyword(resultLandingInfo.description,common_abstract)) ||
          (resultLandingInfo.authors && this.hasKeyword("_"+resultLandingInfo.authors.map(o => o.fullName).join("_")+"_",common_authors, "_"))
        );
        // console.log("common content " + allow)
        //community filter
        allow = allow &&  ((properties.adminToolsPortalType == 'explore' &&
            (!resultLandingInfo.contexts || resultLandingInfo.contexts.length == 0 ||
              resultLandingInfo.contexts.filter( context => { return publicCommunities.includes(context.idContext) }).length == 0)) ||
          (properties.adminToolsPortalType != 'explore' && properties.adminToolsPortalType != 'community') ||
          (properties.adminToolsPortalType == 'community' && (!!resultLandingInfo.contexts &&
              resultLandingInfo.contexts.filter( context => { return ConnectHelper.getCommunityFromDomain(properties.domain) == context.idContext }).length > 0)
          )
        );
        // console.log("community " + allow )
        return allow;
      }
    } catch (e) {
      console.error("Error in passNoIndexFilter()", resultLandingInfo.relcanId, e);
      return false;
    }
  }

  private hasKeyword(value:string, words:string[], wordSuffix = ""){
    return value?words.filter( word => { return value.toLowerCase().indexOf(wordSuffix + word + wordSuffix)!=-1}).length > 0:0;
  }
  private isKeyword(value:string, words:string[]){
    return value?words.filter( word => { return value.toLowerCase() == word}).length > 0:0;
  }

  public onSelectActiveTab(activeTabId) {
    if (this.activeTab != activeTabId) {   // tab really changed
      this.activeTab = activeTabId;
      if (activeTabId == 'enermaps' && this.properties.enermapsURL) {
        this.getEnermapsDetails(this.enermapsId);
      }
      if (activeTabId == 'recommended' && this.properties.adminToolsPortalType == 'faircore4eosc') {
        this.getRecommendations();
      }
    }
  }

  private getEnermapsDetails(id: string) {
    if (this.reloadEnermapsDetails) {
      this.subscriptions.push(this._resultLandingService.getEnermapsDetails(id).subscribe(
        data => {
          this.enermapsDetails = data;
          this.reloadEnermapsDetails = false;
        },
        error => {
          console.error("Error in fetching enermaps details ",error);
        }
      ))
    }
  }

  private getRecommendations() {
    this.recommendationError = null;
    this.subscriptions.push(this.recommendationsService.getRecommendationsForPublication(this.properties.recommendationsForPublicationAPI, this.resultLandingInfo.objIdentifier).subscribe(data => {
      this.recommendedResults = data;
    }, error => {
      console.log(error);
      if(error.status == 404) {
        this.recommendationError = "No recommendations found.";
      } else {
        this.recommendationError = "Sorry, something went wrong.";
      }
    }));
  }

	public enrichContexts(contextsWithLink: any) {
		this.contextsWithLink = contextsWithLink;
	}

  public relatedClassChanged(type) {
    if(type == "datasource") {
      this.relatedServices.page = 1;
      this.filteredRelatedServices = this.resultLandingInfo.relatedServices.filter(result => !this.relatedServices.selectedClass || result.relationName.toLowerCase() == this.relatedServices.selectedClass.toLowerCase());
    } else {
      this.relatedResults.page = 1;
      this.filteredRelatedResults = this.resultLandingInfo.relatedResults.filter(result => !this.relatedResults.selectedClass || result.relationName.toLowerCase() == this.relatedResults.selectedClass.toLowerCase());
    }
  }

  public viewAllOrganizationsClick() {
    this.viewAllOrganizations = true;
    if(this.resultLandingInfo.organizations.length <= this.thresholdOrganizations*2) {
      this.lessBtnOrganizations = true;
    } else {
      this.openOrganizationsModal();
    }
  }

  public viewAllSubjectsMobileClicked(event) {
    this.subjectsFsModal.title += " - "+event['subtitle'];
    this.subjectsFsModal.back = true;
    this.viewAllMobile = event['id'];
  }

  public viewAllOrganizationsMobileClicked() {
    this.summaryFsModal.title += " - Related "+this.openaireEntities.ORGANIZATIONS;
    this.summaryFsModal.back = true;
    this.lessBtnOrganizations = true;
    this.viewAllMobile = "organizations";
  }

  public cancelSummaryClicked() {
    this.summaryFsModal.title = "Summary";
    this.summaryFsModal.back = false;
    this.lessBtnOrganizations = false;
    this.viewAllMobile = "";
  }

  public cancelSubjectsClicked() {
    this.subjectsFsModal.title = "Subjects";
    this.subjectsFsModal.back = false;
    this.viewAllMobile = "";
  }

  public openOrganizationsModal() {
    this.organizationsModal.cancelButton = false;
    this.organizationsModal.okButton = false;
    this.organizationsModal.alertTitle = "Related "+this.openaireEntities.ORGANIZATIONS;
    this.organizationsModal.open();
  }

  public feedbackClicked(preSelectedField: string = "") {
    this.feedbackPreSelectedField=preSelectedField;
    this.showFeedback = true;
    this.scroll();
    this.viewAll = "";
  }

  public sdgsFsModalCancelled() {
    if(this.viewAllMobile == "sdg") {
      this.viewAllMobile = "";
    } else {
      this.viewAllMobile = "sdg";
    }
  }

  public fosFsModalCancelled() {
    if(this.viewAllMobile == "fos") {
      this.viewAllMobile = "";
    } else {
      this.viewAllMobile = "fos";
    }
  }

  public suggestMobileClicked(value: string) {
    if(this.viewAllMobile == 'sdg' || this.viewAllMobile == 'fos') {
      this.sdgsFsModal.title += " - Suggest";
      this.sdgsFsModal.back = true;
      this.sdgFosSuggest.subjects=this.resultLandingInfo.sdg;
      this.sdgFosSuggest.subjectType="sdg";
      this.viewAllMobile = "sdgSuggest";
    }
    if(value == 'sdg') {
      this.sdgFosSuggest.subjects=this.resultLandingInfo.sdg;
      this.sdgFosSuggest.subjectType="sdg";
    } else if(value == 'fos') {
      this.sdgFosSuggest.subjects=this.resultLandingInfo.fos;
      this.sdgFosSuggest.subjectType="fos";
    }
  }

	public suggestClicked(value: string) {
		if(value == 'sdg') {
      this.sdgFosSuggest.subjects=this.resultLandingInfo.sdg;
      this.sdgFosSuggest.subjectType="sdg";
		} else if(value == 'fos') {
      this.sdgFosSuggest.subjects=this.resultLandingInfo.fos;
      this.sdgFosSuggest.subjectType="fos";
		}
    this.cdr.detectChanges();
    this.sdgFosSuggest.isOpen = true;
    this.sdgFosSuggest.openSelectionModal();
  }

  public viewAllDescriptionClicked() {
    if(this.isMobile) {
      this.summaryFsModal.title += " - Abstract";
      this.summaryFsModal.back = true;
      this.viewAllMobile = "description";
    } else {
      this.openDescriptionModal();
    }
  }

	public openDescriptionModal() {
    this.descriptionClicked = true;
		this.descriptionModal.alertFooter = false;
		this.descriptionModal.alertTitle = "Abstract";
		this.descriptionModal.open();
	}

  public clickedImpactIndicators() {
    setTimeout( () => {
      this.impactClicked = true;
    });
  }

  public clickedAltMetrics() {
    setTimeout( () => {
      this.altMetricsClicked = true;
    });
  }

	public clickedUsageMetrics() {
		setTimeout( () => {
      this.metricsClicked = true;
    });
	}

	public formatNumber(num: number | string) {
    let formatted = NumberUtils.roundNumber(+num);
    return formatted.number + formatted.size;
  }

	// private openSdgSelectionModal() {
	// 	this.sdgSelectionModal.cancelButton = false;
	// 	this.sdgSelectionModal.alertTitle = "Please select SDGs that are the most relevant for this publication.";
	// 	this.sdgSelectionModal.okButtonText = "Next";
	// 	this.sdgSelectionModal.stayOpen = true;
	// 	this.sdgSelectionModal.open();
	// }

  public openFsModal(fsModal: FullScreenModalComponent, title: string) {
    fsModal.title = title;
    fsModal.okButton = false;
    fsModal.stayOpenInBack = true;
    fsModal.open();
    this.tabMobile = title;
  }
	/*private openFosSelectionModal() {
		this.fosSelectionModal.cancelButton = false;
		this.fosSelectionModal.alertTitle = "Please select FOS that are the most relevant for this publication.";
		this.fosSelectionModal.open();
	}

	public sdgModalOutput() {
		this.sdgFosSuggest.sdgModalOutput();
	}*/
  public closeFsModal(fsModal: FullScreenModalComponent) {
    fsModal.cancel();
  }

	public getMetricsTooltip(value: string, compact: boolean) {
		if (compact) {
			if (value == 'citations') {
				return "<div>This is an alternative to the \"Influence\" indicator, which also reflects the overall/total impact of an article in the research community at large, based on the underlying citation network (diachronically).</div>";
			} else if (value == 'popularity') {
				return "<div>This indicator reflects the \"current\" impact/attention (the \"hype\") of an article in the research community at large, based on the underlying citation network.</div>";
			} else if (value == 'influence') {
				return "<div>This indicator reflects the overall/total impact of an article in the research community at large, based on the underlying citation network (diachronically).</div>";
			} else if (value == 'impulse') {
				return "<div>This indicator reflects the initial momentum of an article directly after its publication, based on the underlying citation network.</div>";
			}
		} else {
			if (value == 'citations') {
				return "<div>This is an alternative to the \"Influence\" indicator, which also reflects the overall/total impact of an article in the research community at large, based on the underlying citation network (diachronically).</div><div class='uk-flex uk-margin-top'><a href='https://bip.imsi.athenarc.gr/' target='_blank'><img class='uk-margin-small-right' src='assets/common-assets/bip-minimal.svg' loading='lazy' alt='BIP!' style='width: 40px;'></a><span class='uk-text-uppercase'>Citations provided by <a href='https://bip.imsi.athenarc.gr/' target='_blank' class='uk-link-reset'><strong>BIP!</strong></a></div>";
			} else if (value == 'popularity') {
				return "<div>This indicator reflects the \"current\" impact/attention (the \"hype\") of an article in the research community at large, based on the underlying citation network.</div><div class='uk-flex uk-margin-top'><a href='https://bip.imsi.athenarc.gr/' target='_blank'><img class='uk-margin-small-right' src='assets/common-assets/bip-minimal.svg' loading='lazy' alt='BIP!' style='width: 40px;'></a><span class='uk-text-uppercase'>Popularity provided by <a href='https://bip.imsi.athenarc.gr/' target='_blank' class='uk-link-reset'><strong>BIP!</strong></a></div>";
			} else if (value == 'influence') {
				return "<div>This indicator reflects the overall/total impact of an article in the research community at large, based on the underlying citation network (diachronically).</div><div class='uk-flex uk-margin-top'><a href='https://bip.imsi.athenarc.gr/' target='_blank'><img class='uk-margin-small-right' src='assets/common-assets/bip-minimal.svg' loading='lazy' alt='BIP!' style='width: 40px;'></a><span class='uk-text-uppercase'>Influence provided by <a href='https://bip.imsi.athenarc.gr/' target='_blank' class='uk-link-reset'><strong>BIP!</strong></a></div>";
			} else if (value == 'impulse') {
        return "<div>This indicator reflects the initial momentum of an article directly after its publication, based on the underlying citation network.</div><div class='uk-flex uk-margin-top'><a href='https://bip.imsi.athenarc.gr/' target='_blank'><img class='uk-margin-small-right' src='assets/common-assets/bip-minimal.svg' loading='lazy' alt='BIP!' style='width: 40px;'></a><span class='uk-text-uppercase'>Impulse provided by <a href='https://bip.imsi.athenarc.gr/' target='_blank' class='uk-link-reset'><strong>BIP!</strong></a></div>";
      } else if (value == 'downloads') {
				return "<div class='uk-flex uk-flex-middle'><a href='https://usagecounts.openaire.eu/' target='_blank'><img src='assets/common-assets/logo-small-usage-counts.png' loading='lazy' alt='OpenAIRE UsageCounts'></a><span class='uk-text-uppercase uk-margin-small-left'>Downloads provided by <a href='https://usagecounts.openaire.eu/' target='_blank' class='uk-link-reset'><strong>UsageCounts</strong></a></div>";
			} else if (value == 'views') {
				return "<div class='uk-flex uk-flex-middle'><a href='https://usagecounts.openaire.eu/' target='_blank'><img src='assets/common-assets/logo-small-usage-counts.png' loading='lazy' alt='OpenAIRE UsageCounts'></a><span class='uk-text-uppercase uk-margin-small-left'>Views provided by <a href='https://usagecounts.openaire.eu/' target='_blank' class='uk-link-reset'><strong>UsageCounts</strong></a></div>";
			}
		}
	}

  public isNumber(value): boolean {
    return typeof value === 'number';
  }

  public getAccessLabel(accessRight) : string {
    if (accessRight) {
      return (accessRight + (accessRight.toLowerCase().endsWith(" access") ? "" : " access"));
    }
    return "Not available access";
  }

  public get hasSubjects(): boolean {
    return !!(this.resultLandingInfo.subjects || this.resultLandingInfo.otherSubjects || (this.resultLandingInfo.classifiedSubjects && this.properties.environment == 'production') || this.resultLandingInfo.eoscSubjects);
  }

  public getEoscParams() {
    let params = "";
    if(this.prevPath) {
      let splitted: string[] = this.prevPath.split("?");
      params = "&return_path="+StringUtils.URIEncode(splitted[0]);
      if(splitted.length > 0) {
        params += "&search_params="+StringUtils.URIEncode(splitted[1]);
      }
    }
    return params;
  }

  public addEoscPrevInParams(obj) {
    if(properties.adminToolsPortalType == "eosc" && this.prevPath) {
      let splitted: string[] = this.prevPath.split("?");
      obj = this.routerHelper.addQueryParam("return_path", splitted[0], obj);
      if(splitted.length > 0) {
        obj = this.routerHelper.addQueryParam("search_params", splitted[1], obj);
      }
    }
    return obj;
  }

  public get eoscBackLink() {
    if(this.prevPath && this.referrer && ((this.referrer == "https://eosc-search-service.grid.cyfronet.pl/") || (this.referrer == this.properties.eoscMarketplaceURL))) {
      return this.referrer+this.prevPath;
    } else {
      return this.properties.eoscMarketplaceURL;
      // return "https://"+(this.properties.environment == "beta" ? "beta." : "")+"search.marketplace.eosc-portal.eu/";
    }
  }
}
