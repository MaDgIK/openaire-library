import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PreviousRouteRecorder } from "../../utils/piwik/previousRouteRecorder.guard";
import { ResultLandingComponent } from "./resultLanding.component";


@NgModule({
 imports: [
   RouterModule.forChild([{ path: '', component: ResultLandingComponent, canDeactivate: [PreviousRouteRecorder] }])
 ]
})
export class ResultLandingRoutingModule { }
