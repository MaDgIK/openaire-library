import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';
import { RouterModule } from '@angular/router';

import {IFrameModule} from '../../utils/iframe.module';
import {ErrorMessagesModule} from '../../utils/errorMessages.module';
import {LandingModule} from '../landing-utils/landing.module';
import {PagingModule} from '../../utils/paging.module';

import {StatisticsTabComponent} from './statisticsTab.component';
import {RelatedDatasourcesTabComponent} from './relatedDatasourcesTab.component';

import {DataProviderComponent} from './dataProvider.component';
import {DataProviderService} from './dataProvider.service';
import {DataProvidersServiceModule} from '../../services/dataProvidersService.module';
import {ProjectsServiceModule} from '../../services/projectsService.module';
import {SearchResearchResultsServiceModule} from '../../services/searchResearchResultsService.module';

import {Schema2jsonldModule} from '../../sharedComponents/schema2jsonld/schema2jsonld.module';
import { SEOServiceModule } from '../../sharedComponents/SEO/SEOService.module';
import {ShowPublisherModule} from "../landing-utils/showPublisher.module";
import {HelperModule} from "../../utils/helper/helper.module";
import {LandingHeaderModule} from "../landing-utils/landing-header/landing-header.module";
import {AlertModalModule} from "../../utils/modal/alertModal.module";
import {NoLoadPaging} from "../../searchPages/searchUtils/no-load-paging.module";
import {FeedbackModule} from "../feedback/feedback.module";
import {TabsModule} from "../../utils/tabs/tabs.module";
import {SearchTabModule} from "../../utils/tabs/contents/search-tab.module";
import {LoadingModule} from "../../utils/loading/loading.module";
import {IconsModule} from "../../utils/icons/icons.module";
import {IconsService} from "../../utils/icons/icons.service";
import {graph} from "../../utils/icons/icons";
import {LoadingModalModule} from "../../utils/modal/loadingModal.module";
import {ResultLandingUtilsModule} from "../landing-utils/resultLandingUtils.module";
import {FullScreenModalModule} from '../../utils/modal/full-screen-modal/full-screen-modal.module';
import {SafeHtmlPipeModule} from '../../utils/pipes/safeHTMLPipe.module';
import {EntityActionsModule} from "../../utils/entity-actions/entity-actions.module";
import {DataProviderRoutingModule} from "./dataProvider-routing.module";

@NgModule({
  imports:
    [CommonModule, FormsModule, RouterModule,
      DataProviderRoutingModule,
      IFrameModule, ErrorMessagesModule, LandingModule,
      DataProvidersServiceModule, ProjectsServiceModule, SearchResearchResultsServiceModule,
      PagingModule, Schema2jsonldModule, SEOServiceModule, ShowPublisherModule, HelperModule,
      LandingHeaderModule, AlertModalModule, NoLoadPaging, FeedbackModule,
      TabsModule, SearchTabModule, LoadingModule, IconsModule, LoadingModalModule, ResultLandingUtilsModule,
      FullScreenModalModule, SafeHtmlPipeModule, EntityActionsModule
    ],
  declarations:
  [StatisticsTabComponent,
    RelatedDatasourcesTabComponent, DataProviderComponent
  ],
  providers:[
    DataProviderService],
  exports: [
    DataProviderComponent
  ]

})
export class DataProviderModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([graph])
  }
}
