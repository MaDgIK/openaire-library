import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PreviousRouteRecorder } from "../../utils/piwik/previousRouteRecorder.guard";
import { DataProviderComponent } from "./dataProvider.component";


@NgModule({
  imports: [
    RouterModule.forChild([{ path: '', component: DataProviderComponent, canDeactivate: [PreviousRouteRecorder] }])
  ]
})
export class DataProviderRoutingModule { }
