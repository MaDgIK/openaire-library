import {Component, Input}  from '@angular/core';
import {ActivatedRoute}    from '@angular/router';
import {FetchResearchResults} from '../../utils/fetchEntitiesClasses/fetchResearchResults.class';
import {ErrorCodes}        from '../../utils/properties/errorCodes';
import {EnvProperties}     from '../../utils/properties/env-properties';
import {StringUtils} from "../../utils/string-utils.class";
import {Subscriber} from "rxjs";
import {properties} from "../../../../environments/environment";
import {OpenaireEntities} from "../../utils/properties/searchFields";

@Component({
    selector: 'statisticsTab',
    template: `
      <errorMessages [status]="[fetchPublications.searchUtils.status, fetchDatasets.searchUtils.status,
                                fetchSoftware.searchUtils.status, fetchOther.searchUtils.status]" 
                     [type]="'statistics'" tab_error_class=true></errorMessages>
      <div *ngIf="fetchPublications.searchUtils.totalResults > 0 || (fetchDatasets.searchUtils.totalResults > 0 || 
                            fetchSoftware.searchUtils.totalResults > 0 || fetchOther.searchUtils.totalResults > 0)"
            class="uk-grid uk-child-width-1-1 uk-child-width-1-2@m">
        <div class="uk-padding uk-padding-remove-top">
          <div class="uk-card uk-card-default uk-card-body">
            <div *ngIf=" properties && !properties.useNewStatistisTool" class="uk-text-center uk-text-large">Produced {{openaireEntities.RESULTS}} per year</div>
            <i-frame  [url]=docsTimelineUrl
                      customContainerClass="uk-background-default" customIframeClass="uk-blend-multiply"></i-frame>
          </div>
        </div>
        <div class="uk-padding uk-padding-remove-top">
          <div class="uk-card uk-card-default uk-card-body">
            <div *ngIf=" properties && !properties.useNewStatistisTool" class="uk-text-center uk-text-large">{{openaireEntities.RESULTS}} Types</div>
            <i-frame [url]=docsTypesUrl
                     customContainerClass="uk-background-default" customIframeClass="uk-blend-multiply"></i-frame>
          </div>
        </div>
<!--      </ng-container>-->

<!--      <div *ngIf="fetchPublications.searchUtils.totalResults > 0 || (fetchDatasets.searchUtils.totalResults > 0-->
<!--                  || fetchSoftware.searchUtils.totalResults > 0 || fetchOther.searchUtils.totalResults > 0)"-->
        <div class="uk-padding uk-padding-remove-top">
          <div class="uk-card uk-card-default uk-card-body">
            <div *ngIf=" properties && !properties.useNewStatistisTool" class="uk-text-center uk-text-large">{{openaireEntities.RESULTS}} per funder</div>
            <i-frame  [url]=docsFunderUrl
                      customContainerClass="uk-background-default" customIframeClass="uk-blend-multiply"></i-frame>
          </div>
        </div>

      <div *ngIf="fetchPublications.searchUtils.totalResults > 0 "
            class="uk-padding uk-padding-remove-top">
        <div class="uk-card uk-card-default uk-card-body">
          <div *ngIf=" properties && !properties.useNewStatistisTool" class="uk-text-center uk-text-large">{{openaireEntities.PROJECTS}} with most {{openaireEntities.PUBLICATIONS}}</div>
          <i-frame [url]=pubsProjectsUrl
                   customContainerClass="uk-background-default" customIframeClass="uk-blend-multiply"></i-frame>
        </div>
      </div>

      <div *ngIf="(fetchDatasets.searchUtils.totalResults > 0)"
            class="uk-padding uk-padding-remove-top">
        <div class="uk-card uk-card-default uk-card-body">
          <div *ngIf=" properties && !properties.useNewStatistisTool" class="uk-text-center uk-text-large">{{openaireEntities.PROJECTS}} with most {{openaireEntities.DATASETS}}</div>
          <i-frame  [url]=dataProjectsUrl
                    customContainerClass="uk-background-default" customIframeClass="uk-blend-multiply"></i-frame>
        </div>
      </div>
      </div>

    `
})

export class StatisticsTabComponent {
    @Input() datasourceId;
    @Input() fetchPublications : FetchResearchResults;
    @Input() fetchDatasets : FetchResearchResults;
    @Input() fetchSoftware: FetchResearchResults;
    @Input() fetchOther: FetchResearchResults;

    private docsTimelineUrl: string;
    private docsTypesUrl:string;
    private docsFunderUrl:string;
    private dataProjectsUrl:string ;
    private pubsProjectsUrl:string;
    public  errorCodes:ErrorCodes = new ErrorCodes();
    properties:EnvProperties = properties;
    public openaireEntities = OpenaireEntities;
    sub;
    constructor (private route: ActivatedRoute) {}

    ngOnInit() {
          if (this.properties.useNewStatistisTool) {
            this.docsTimelineUrl = this.properties.statisticsFrameNewAPIURL +
              'chart?json='+StringUtils.URIEncode('{"library":"HighCharts","chartDescription":{"queries":[{"name":"'+this.openaireEntities.RESULTS+'","type":"column","query":{"name":"dtsrcYear","parameters":["'+this.datasourceId+'"]}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"Produced '+this.openaireEntities.RESULTS+' per year"},"subtitle":{},"yAxis":{"title":{"text":"'+this.openaireEntities.RESULTS+'"}},"xAxis":{"title":{"text":"Year"}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":false},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":true,"align":"center","verticalAlign":"bottom","layout":"horizontal"},"credits":{"href":null,"enabled":true,"text":"Created by OpenAIRE via HighCharts"}}}');
            this.docsTypesUrl = this.properties.statisticsFrameNewAPIURL +
              'chart?json='+StringUtils.URIEncode('{"library":"HighCharts","chartDescription":{"queries":[{"name":"'+this.openaireEntities.RESULTS+'","type":"pie","query":{"name":"dtsrcPubs","parameters":["'+this.datasourceId+'"]}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"'+this.openaireEntities.RESULTS+' types"},"subtitle":{},"yAxis":{"title":{"text":"Publications"}},"xAxis":{"title":{}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":false},"plotOptions":{"series":{"dataLabels":{"enabled":true}}},"legend":{"enabled":true,"align":"center","verticalAlign":"bottom","layout":"horizontal"},"credits":{"href":null,"enabled":true,"text":"Created by OpenAIRE via HighCharts"}}}');
            this.docsFunderUrl = this.properties.statisticsFrameNewAPIURL +
              'chart?json='+StringUtils.URIEncode('{"library":"HighCharts","chartDescription":{"queries":[{"name":"'+this.openaireEntities.RESULTS+'","type":"pie","query":{"name":"dtsrcPubsFund","parameters":["'+this.datasourceId+'"]}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"'+this.openaireEntities.RESULTS+' per funder"},"subtitle":{},"yAxis":{"title":{}},"xAxis":{"title":{}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":false},"plotOptions":{"series":{"dataLabels":{"enabled":true}}},"legend":{"enabled":true,"align":"center","verticalAlign":"bottom","layout":"horizontal"},"credits":{"href":null,"enabled":true,"text":"Created by OpenAIRE via HighCharts"}}}');
            this.pubsProjectsUrl = this.properties.statisticsFrameNewAPIURL +
              'chart?json='+StringUtils.URIEncode('{"library":"HighCharts","chartDescription":{"queries":[{"name":"'+this.openaireEntities.PUBLICATIONS+'","type":"bar","query":{"name":"dtsrcProjPubs","parameters":["'+this.datasourceId+'"]}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":" '+this.openaireEntities.PROJECTS+' with most '+this.openaireEntities.PUBLICATIONS+'"},"subtitle":{},"yAxis":{"title":{"text":"'+this.openaireEntities.PUBLICATIONS+'"}},"xAxis":{"title":{"text":"'+this.openaireEntities.PROJECT+'"}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":false},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":true,"align":"center","verticalAlign":"bottom","layout":"horizontal"},"credits":{"href":null,"enabled":true,"text":"Created by OpenAIRE via HighCharts"}}}');
            this.dataProjectsUrl = this.properties.statisticsFrameNewAPIURL +
              'chart?json='+StringUtils.URIEncode('{"library":"HighCharts","chartDescription":{"queries":[{"name":"'+this.openaireEntities.DATASETS+'","type":"bar","query":{"name":"dtsrcProjData","parameters":["'+this.datasourceId+'"]}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":"'+this.openaireEntities.PROJECTS+' with most '+this.openaireEntities.DATASETS+'"},"subtitle":{},"yAxis":{"title":{"text":"'+this.openaireEntities.DATASETS+'"}},"xAxis":{"title":{"text":"'+this.openaireEntities.PROJECT+'"}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":false},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":true,"align":"center","verticalAlign":"bottom","layout":"horizontal"},"credits":{"href":null,"enabled":true,"text":"Created by OpenAIRE via HighCharts"}}}');
          } else {
            this.docsTimelineUrl = this.properties.statisticsFrameAPIURL + 'chart.php?com=query&persistent=false&data={"query":"dtsrcYear","dtsrcName":"' + this.datasourceId + '","table": "result", "fields": [{"fld": "number", "agg": "count", "type": "line", "yaxis":1, "c":true}], "xaxis":{"name": "year", "agg": "avg"}, "group": "", "color": "", "type": "chart", "size":80, "sort": "xaxis", "xStyle":{"r": -30, "s": "6", "l": "-", "ft": "-", "wt": "-"}, "yaxisheaders": [""], "fieldsheaders": ["'+this.openaireEntities.RESULTS+'"], "in": [{"f":0, "text": "Yearly"}], "filters": [{"name":"year","max":"2016","min":"1997"},{"name": "result_datasources-datasource-name", "values":[""], "to": "-1"}],"having": [], "incfilters": [], "inchaving": [], "title": "", "subtitle": "", "xaxistitle": "Year"}&w=90%&h=90%';
            this.docsTypesUrl = this.properties.statisticsFrameAPIURL + 'chart.php?com=query&persistent=false&data={"query":"dtsrcPubs","dtsrcName":"' + this.datasourceId + '", "table": "result", "fields": [{"fld": "number", "agg": "count", "type": "pie", "yaxis":1, "c":false}], "xaxis":{"name": "result_classifications-type", "agg": "avg"}, "group": "", "color": "", "type": "chart", "size":30, "sort": "xaxis", "xStyle":{"r": "-", "s": "-", "l": "-", "ft": "-", "wt": "-"}, "yaxisheaders": [""], "fieldsheaders": ["'+this.openaireEntities.RESULTS+'"], "in": [], "filters": [{"name": "result_datasources-datasource-name", "values": [""], "to": "-1"}], "having": [], "incfilters": [], "inchaving": [], "title": "", "subtitle": "", "xaxistitle": ""}&w=90%&h=90%';
            this.docsFunderUrl = this.properties.statisticsFrameAPIURL + 'chart.php?com=query&persistent=false&data={"query":"dtsrcPubsFund","dtsrcName":"' + this.datasourceId + '", "table": "result", "fields": [{"fld": "number", "agg": "count", "type": "pie", "yaxis":1, "c":false}], "xaxis":{"name": "result_classifications-type", "agg": "avg"}, "group": "", "color": "", "type": "chart", "size":30, "sort": "xaxis", "xStyle":{"r": "-", "s": "-", "l": "-", "ft": "-", "wt": "-"}, "yaxisheaders": [""], "fieldsheaders": ["'+this.openaireEntities.RESULTS+'"], "in": [], "filters": [{"name": "result_datasources-datasource-name", "values": [""], "to": "-1"}], "having": [], "incfilters": [], "inchaving": [], "title": "", "subtitle": "", "xaxistitle": ""}&w=90%&h=90%';
            this.dataProjectsUrl = this.properties.statisticsFrameAPIURL + 'chart.php?com=query&persistent=false&data={"query":"dtsrcProjData","dtsrcName":"' + this.datasourceId + '", "table": "result", "fields": [{"fld": "number", "agg": "count", "type": "bar", "yaxis":1, "c":false}], "xaxis":{"name": "result_classifications-type", "agg": "avg"}, "group": "", "color": "", "type": "chart", "size":30, "sort": "xaxis", "xStyle":{"r": "-", "s": "-", "l": "-", "ft": "-", "wt": "-"}, "yaxisheaders": [""], "fieldsheaders": ["'+this.openaireEntities.DATASETS+'"], "in": [], "filters": [{"name": "result_datasources-datasource-name", "values": [""], "to": "-1"}], "having": [], "incfilters": [], "inchaving": [], "title": "", "subtitle": "", "xaxistitle": ""}&w=90%&h=90%';
            this.pubsProjectsUrl = this.properties.statisticsFrameAPIURL + 'chart.php?com=query&persistent=false&data={"query":"dtsrcProjPubs","dtsrcName":"' + this.datasourceId + '", "table": "result", "fields": [{"fld": "number", "agg": "count", "type": "bar", "yaxis":1, "c":false}], "xaxis":{"name": "result_classifications-type", "agg": "avg"}, "group": "", "color": "", "type": "chart", "size":30, "sort": "xaxis", "xStyle":{"r": "-", "s": "-", "l": "-", "ft": "-", "wt": "-"}, "yaxisheaders": [""], "fieldsheaders": ["'+this.openaireEntities.PUBLICATIONS+'"], "in": [], "filters": [{"name": "result_datasources-datasource-name", "values": [""], "to": "-1"}], "having": [], "incfilters": [], "inchaving": [], "title": "", "subtitle": "", "xaxistitle": ""}&w=90%&h=90%';
          }

    }

  ngOnDestroy() {
    if (this.sub instanceof Subscriber) {
      this.sub.unsubscribe();
    }
  }}
