import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {throwError} from 'rxjs';
import {DataProviderInfo, DataproviderProvenance} from '../../utils/entities/dataProviderInfo';
import{EnvProperties} from '../../utils/properties/env-properties';
import {map} from "rxjs/operators";
import {ParsingFunctions} from "../landing-utils/parsingFunctions.class";
import {OpenaireEntities} from "../../utils/properties/searchFields";
import {Identifier, StringUtils} from "../../utils/string-utils.class";
import {properties} from "../../../../environments/environment";


@Injectable()
export class DataProviderService {

    constructor(private http: HttpClient ) {
      this.parsingFunctions = new ParsingFunctions();
    }

    dataProviderInfo: DataProviderInfo;
    public parsingFunctions: ParsingFunctions;

  private buildDatasourceLandingInfoUrl(id: string, identifier: Identifier, typePathParam: string): string {
    if (id) {
      return  properties.searchAPIURLLAst + typePathParam + "/" + id + '?format=json';
    } else if (identifier) {
      return properties.searchAPIURLLAst + "resources2?query=(pid exact \""+encodeURIComponent(identifier.id) + "\")&type="+typePathParam+"&format=json";
    }
  }

  getDataproviderInfo (id: string, identifier: Identifier, properties: EnvProperties, typePathParam: string): any {
    let url: string = this.buildDatasourceLandingInfoUrl(id, identifier, typePathParam);
    let finalUrl: string = (properties.useCache) ? (properties.cacheUrl + encodeURIComponent(url)) : url;
    return this.http.get(finalUrl)
      .pipe(map(res => {
        if(!id && identifier) {
          if(!res['results'] || res['results'].length == 0) {
            throw new HttpErrorResponse({
              status: 404,
              statusText: "Not found",
              url: finalUrl,
              error: "Http failure response for "+finalUrl+": 404 Not Found"
            });
          }
          return res['results'][0];
        } else {
          return res;
        }
      }))
      .pipe(map(res => [res['result']['metadata']['oaf:entity'], res]))
      .pipe(map(res => [
        res[0]['oaf:datasource'],                           // 0
        res[0]['oaf:datasource']['datasourcetype'],         // 1
        res[0]['oaf:datasource']['openairecompatibility'],  // 2
        res[0]['oaf:datasource']['collectedfrom'],          // 3
        res[0]['oaf:datasource']['accessinfopackage'],      // 4
        res[0]['oaf:datasource']['rels']['rel'],            // 5
        res[0]['oaf:datasource']['journal'],                // 6
        res[1]                                              // 7
      ]))
      .pipe(map(res => this.parseDataProviderInfo(res)));
  }

    getDataproviderAggregationStatus(original_id: string, properties:EnvProperties):any {
        //let headers = new Headers({'Content-Type': 'application/json', 'accept': 'application/json'});
        //let options = new RequestOptions({headers: headers});

      // const options = {
      //   headers: new HttpHeaders({
      //     'Content-Type': 'application/json',
      //     'accept': 'application/json'})
      // };
      //
      // let page: number = 0;
      //   let size: number = 1;
      //   return this.http.post(properties.datasourcesAPI+page+"/"+size+"?requestSortBy=id&order=ASCENDING", JSON.stringify({ "id": original_id }), options)
      //       //.map(res => <any> res.json())
      //       .pipe(map(res => res['datasourceInfo']))
      //       .pipe(map(res => this.parseDataproviderAggregationStatus(res)));

      return this.http.get(properties.datasourcesAPI+original_id)
        .pipe(map(res => res['api']))
        .pipe(map(res => this.parseDataproviderAggregationStatus(res)));
    }

    getCollectedFulltexts(datasourceId: string) {
      return this.http.get(properties.pdfStatisticsAPIURL+"/stats/getNumberOfPayloadsForDatasource?datasourceId="+datasourceId)
        .pipe(map(res => res))
    }

    private handleError (error: HttpErrorResponse) {
    // in a real world app, we may send the error to some remote logging infrastructure
    // instead of just logging it to the console
        console.log(error);
        return throwError(error  || 'Server error');
    }

    parseDataproviderAggregationStatus(apis: any): any {
        // var aggregationStatus: {"fundedContent": string, "indexRecords": string, "fulltexts": string, "lastUpdateDate": string} = null;
        // if(data != null && data[0] != null) {
        //   aggregationStatus = {"fundedContent": "-1", "indexRecords": "-1", "fulltexts": "-1", "lastUpdateDate": null};
        //   aggregationStatus.fundedContent = data[0].fundedContent;
        //   aggregationStatus.indexRecords = data[0].indexRecords;
        //   aggregationStatus.fulltexts = data[0].fulltexts;
        //
        //   if(data[0].hasOwnProperty("aggregationHistory")) {
        //     let length = Array.isArray(data[0]["aggregationHistory"]) ? data[0]["aggregationHistory"].length : 1;
        //
        //     for(let i=0; i<length; i++) {
        //       var aggregationHistory = Array.isArray(data[0]["aggregationHistory"]) ? data[0]["aggregationHistory"][i] : data[0]["aggregationHistory"];
        //       if(aggregationHistory && aggregationHistory.indexedVersion == true) {
        //         aggregationStatus.lastUpdateDate = aggregationHistory.date;
        //         break;
        //       }
        //     }
        //   }
        // }

      var aggregationStatus: {"fulltexts": string} = null;
      if(apis != null) {
        aggregationStatus = {"fulltexts": "-1"};

        let mostRecentDate = null;
        let length = Array.isArray(apis) ? apis.length : 1;

        for (let i = 0; i < length; i++) {
          let api = Array.isArray(apis) ? apis[i] : apis;
          if(api.compatibility == "files" && (mostRecentDate == null || mostRecentDate < api.lastDownloadDate)) {
            aggregationStatus.fulltexts = api.lastDownloadTotal;
            mostRecentDate = api.lastDownloadDate;
          }
        }
      }

      return aggregationStatus;
    }

    parseDataProviderInfo (data: any):any {
        this.dataProviderInfo = new DataProviderInfo();

      this.dataProviderInfo.record = data[7];
      this.dataProviderInfo.objIdentifier = data[7]["result"]["header"]["dri:objIdentifier"];
      this.dataProviderInfo.relcanId = ParsingFunctions.parseRelCanonicalId(this.dataProviderInfo.record, "datasource");

        if(data[0] != null) {
          this.dataProviderInfo.title = {"name": "", "url": data[0].websiteurl};
          if(data[0].officialname) {
            this.dataProviderInfo.title.name = StringUtils.HTMLToString(String(data[0].officialname));
            this.dataProviderInfo.officialName =  StringUtils.HTMLToString(String(data[0].officialname));
          }
          if(data[0].englishname) {
            this.dataProviderInfo.title.name = StringUtils.HTMLToString(String(data[0].englishname));
          }

            var pattern = /.{12}::.+/;
            var originalIds =(data[0].originalId)?data[0].originalId:"";
            if(originalIds) {
              let provenances = new DataproviderProvenance().provenance;
              this.dataProviderInfo.provenance = new Map<string, { "url": string[], "name" }>();

              const idRegex = RegExp('[^'+'::'+']*$');

              let length = Array.isArray(originalIds) ? originalIds.length : 1;

              for (let i = 0; i < length; i++) {
                var originalId = Array.isArray(originalIds) ? originalIds[i] : originalIds;
                var matched = originalId.match(pattern);

                if (matched && originalId && originalId != "") {
                  // if (originalId.indexOf("opendoar____::") != -1) {
                  //   this.dataProviderInfo.openDoarId = originalId.split("opendoar____::")[1];
                  // } else if (originalId.indexOf("re3data_____::") != -1) {
                  //   this.dataProviderInfo.r3DataId = originalId.split("re3data_____::")[1];
                  // }

                  let prefix = originalId.substr(0, 14);
                  if(provenances.has(prefix)) {
                    let provenance = provenances.get(prefix);
                    if(this.dataProviderInfo.provenance.has(provenance.name)) {
                      this.dataProviderInfo.provenance.get(provenance.name).url.push(provenance.urlPrefix +  idRegex.exec(originalId)[0]);
                    } else {
                      this.dataProviderInfo.provenance.set(provenance.name, {"url": [provenance.urlPrefix +  idRegex.exec(originalId)[0]]})
                    }

                    // var replace = "/[^"+"::"+"]*$/";
                    // var re = new RegExp(replace,"g");
                    //
                    // console.log(re.exec(originalId)[0]);

                  }
                  this.dataProviderInfo.originalId = originalId;
                }
              }
            }

            this.dataProviderInfo.subjects = [];
            if(data[0].subjects) {
              let length = Array.isArray(data[0]['subjects']) ? data[0]['subjects'].length : 1;
              for (let i = 0; i < length; i++) {
                let subject = Array.isArray(data[0]['subjects']) ? data[0]['subjects'][i] : data[0]['subjects'];
                if (subject && subject.content) {
                  this.dataProviderInfo.subjects.push(subject.content);
                }
              }
            }

            // if(!Array.isArray(data[0]['description'])) {
            //   this.dataProviderInfo.description = (data[0]['description']) ? String(data[0]['description']) : "";
            // } else {
            //   this.dataProviderInfo.description = (data[0]['description'][0]) ? String(data[0]['description'][0]) : "";
            // }
            this.dataProviderInfo.description = this.parsingFunctions.parseDescription(data[0] && data[0].description?data[0].description:[]);

            this.dataProviderInfo.thematic = data[0].thematic;

            if(data[0].jurisdiction != null) {
              this.dataProviderInfo.jurisdiction = data[0].jurisdiction.classname;
            }

            if(data[0].contentpolicy != null) {
              this.dataProviderInfo.contentpolicy = data[0].contentpolicy.classname;
            }

            if(data[0].pid != null) {
              this.dataProviderInfo.identifiers = this.parsingFunctions.parseIdentifiers(data[0].pid);
            }
        }

        if(data[1] != null) {
            this.dataProviderInfo.type = data[1].classname;

            if(data[1].classid == "entityregistry" || data[1].classid == "entityregistry::projects" || data[1].classid == "entityregistry::repositories") {
                this.dataProviderInfo.registry = true;
            } else {
                this.dataProviderInfo.registry = false;
            }

            if(this.dataProviderInfo.tabs == undefined) {
                this.dataProviderInfo.tabs = new Array<{"name": string, "content": string}>();
            }
            this.dataProviderInfo.tabs = [];
            if(this.dataProviderInfo.tabsInTypes.publicationsTab.has(data[1].classid)) {
                this.dataProviderInfo.tabs.push({"name": OpenaireEntities.PUBLICATIONS, "content": "publicationsTab"});
                this.dataProviderInfo.tabs2.push(OpenaireEntities.PUBLICATIONS);
            }
            if(this.dataProviderInfo.tabsInTypes.datasetsTab.has(data[1].classid)) {
                this.dataProviderInfo.tabs.push({"name": OpenaireEntities.DATASETS, "content": "datasetsTab"});
                this.dataProviderInfo.tabs2.push(OpenaireEntities.DATASETS);
            }

            if(this.dataProviderInfo.tabsInTypes.projectsTab.has(data[1].classid)) {
                this.dataProviderInfo.tabs.push({"name": OpenaireEntities.PROJECTS, "content": "projectsTab"});
                this.dataProviderInfo.tabs2.push(OpenaireEntities.PROJECTS);
            }
            if(this.dataProviderInfo.tabsInTypes.datasourcesTab.has(data[1].classid)) {
                this.dataProviderInfo.tabs.push({"name": OpenaireEntities.DATASOURCES, "content": "datasourcesTab"});
                this.dataProviderInfo.tabs2.push(OpenaireEntities.DATASOURCES);
            }

            if(this.dataProviderInfo.tabsInTypes.relatedDatasourcesTab.has(data[1].classid)) {
                this.dataProviderInfo.tabs.push({"name": "Related "+OpenaireEntities.DATASOURCES, "content": "relatedDatasourcesTab"});
                this.dataProviderInfo.tabs2.push("Related "+OpenaireEntities.DATASOURCES);
            }

            if(this.dataProviderInfo.tabsInTypes.statisticsTab.has(data[1].classid)) {
                this.dataProviderInfo.tabs.push({"name": "Statistics", "content": "statisticsTab"});
                this.dataProviderInfo.tabs2.push("Statistics");
            }

            if(this.dataProviderInfo.tabsInTypes.softwareTab.has(data[1].classid)) {
                this.dataProviderInfo.tabs.push({"name": OpenaireEntities.SOFTWARE, "content": "softwareTab"});
                this.dataProviderInfo.tabs2.push(OpenaireEntities.SOFTWARE);
            }

            if(this.dataProviderInfo.tabsInTypes.orpsTab.has(data[1].classid)) {
                this.dataProviderInfo.tabs.push({"name": OpenaireEntities.OTHER, "content": "orpsTab"});
                this.dataProviderInfo.tabs2.push(OpenaireEntities.OTHER);
            }

            if(this.dataProviderInfo.tabsInTypes.metricsTab.has(data[1].classid)) {
                this.dataProviderInfo.tabs.push({"name": "Metrics", "content": "metricsTab"});
                this.dataProviderInfo.tabs2.push("Metrics");
            }

            if(this.dataProviderInfo.resultTypes.collectedFrom.has(data[1].classid)) {
                this.dataProviderInfo.resultsBy = "collectedFrom";
            } else if(this.dataProviderInfo.resultTypes.hostedBy.has(data[1].classid)) {
                this.dataProviderInfo.resultsBy = "hostedBy";
            }
        }

        if(!this.dataProviderInfo.registry) {
            if(data[2] != null) {
                this.dataProviderInfo.compatibility = {"info": "", "name": "", "id": ""};
                this.dataProviderInfo.compatibility.info = data[2].classname;
                //this.dataProviderInfo.compatibility = data[2].classname;
            }

            if(data[2] != null && data[2].classid == "hostedBy" && data[3] != null) {
              this.dataProviderInfo.compatibility.name = data[3].name;
              this.dataProviderInfo.compatibility.id = data[3].id;

              if(this.dataProviderInfo.compatibility.name) {
                this.dataProviderInfo.compatibility.info = "Collected from ";
              }
            }

            if(data[4] != null) {
                let oaiPmhURL:string;
                oaiPmhURL = Array.isArray(data[4]) ? data[4][0]:data[4];
                if(oaiPmhURL != '' && oaiPmhURL != 'unknown') {
                    this.dataProviderInfo.oaiPmhURL = oaiPmhURL;
                }
            }
        }

        if(data[5] != null) {
            let mydata;
            let counter = 0;
            let countriesSet: Set<string>;
            let length = data[5].length!=undefined ? data[5].length : 1;

            for(let i=0; i<length; i++) {
                mydata = data[5].length!=undefined ? data[5][i] : data[5];
                if(mydata.hasOwnProperty("to")) {
                    if(mydata['to'].class && mydata['to'].class.toLowerCase() == "isprovidedby" && mydata['to'].type == "organization") {
                        //if(this.dataProviderInfo.organizations == undefined) {
                        if(this.dataProviderInfo.organizations.length == 0) {
                            //this.dataProviderInfo.organizations = new Array<{"name": string, "url": string}>();
                            this.dataProviderInfo.countries = new Array<string>();
                            countriesSet = new Set<string>();
                        }

                        this.dataProviderInfo.organizations[counter] = {"acronym": "", "name": "", "id": ""};
                        //this.dataProviderInfo.organizations[counter]['name'] = (mydata.legalname ? mydata.legalname : "[no title available");
                        this.dataProviderInfo.organizations[counter]['id'] = mydata['to'].content;

                        if(mydata.hasOwnProperty("legalshortname")) {
                          this.dataProviderInfo.organizations[counter]['acronym'] = mydata.legalshortname;
                        }
                        if(mydata.hasOwnProperty("legalname")) {
                          this.dataProviderInfo.organizations[counter]['name'] = mydata.legalname;
                        }
                        if(!this.dataProviderInfo.organizations[counter]['acronym'] && !this.dataProviderInfo.organizations[counter]['name']){
                          // acronym is displayed with link and name only in tooltip
                          this.dataProviderInfo.organizations[counter]['acronym'] = "[no title available]";
                        }

                        if(mydata.country != '' && mydata['country'].classname != '') {
                            if(!countriesSet.has(mydata['country'].classname)) {
                                this.dataProviderInfo.countries.push(mydata['country'].classname);
                                countriesSet.add(mydata['country'].classname);
                            }
                        }

                        counter++;
                    }
                }
            }
        }
        if(data[6] != null) {
            this.dataProviderInfo.journal = {"journal": "", "issn": "", "lissn": "", "eissn": ""};
            this.dataProviderInfo.journal['journal'] = data[6].content;
            this.dataProviderInfo.journal['issn'] = data[6]['issn'];
            this.dataProviderInfo.journal['lissn'] = data[6]['lissn'];
            this.dataProviderInfo.journal['eissn'] = data[6]['eissn'];

        }else {
            this.dataProviderInfo.journal = null;
            // this.dataProviderInfo.journal = {"journal": "", "issn": "", "lissn": "", "eissn": ""};
        }
        if (data[0]?.measure) {
          this.dataProviderInfo.measure = this.parsingFunctions.parseMeasures(data[0].measure);
        }

        return this.dataProviderInfo;
    }
}
