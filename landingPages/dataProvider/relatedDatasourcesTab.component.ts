import {Component, Input} from '@angular/core';

import { FetchResearchResults } from '../../utils/fetchEntitiesClasses/fetchResearchResults.class';

import { ErrorCodes} from '../../utils/properties/errorCodes';
import { RouterHelper } from '../../utils/routerHelper.class';
import { EnvProperties } from '../../utils/properties/env-properties';
import {AlertModal} from "../../utils/modal/alert";
import {properties} from "../../../../environments/environment";
import {OpenaireEntities} from "../../utils/properties/searchFields";

@Component({
  selector: 'relatedDatasourcesTab',
  template: `
    <errorMessages  [status]="[fetchResults.searchUtils.status]"
                    [type]="'related '+openaireEntities.DATASOURCES" tab_error_class=true>
    </errorMessages>
    
    <div *ngIf="fetchResults.searchUtils.status == errorCodes.DONE && !loading" class="uk-text-small">
      <div *ngIf="results.length > 90" class="uk-text-meta uk-margin-medium-bottom">
        *Only top 100 {{openaireEntities.DATASOURCES}} that host {{openaireEntities.RESULTS}} which are also available via {{collectedFromName}} are shown.     
      </div>
      <results-and-pages *ngIf="results.length >pageSize" [type]="openaireEntities.DATASOURCES"
                         [page]="page" [pageSize]="pageSize"
                         [totalResults]="results.length">
      </results-and-pages>
    
      <table class="uk-table uk-table-striped">
        <thead>
        <tr>
          <th>{{openaireEntities.DATASOURCE}}</th>
          <th *ngIf="fetchResults.results.length > 0 || fetchResults.searchUtils.status == errorCodes.ERROR"
              class="uk-text-center">
            {{openaireEntities.RESULTS}} in Explore
          </th>
        </tr>
        </thead>
        <tbody>
          <ng-container *ngIf="results">
            <ng-container *ngFor="let result of results; let i=index">
              <tr *ngIf="i>=(page-1)*pageSize && i<page*pageSize">
                <td>
                  <a [queryParams]="addEoscPrevInParams({datasourceId: result.id})" routerLinkActive="router-link-active" [routerLink]="dataProviderUrl"
                     (click)="onLinkClick()">
                    {{result.name}}
                  </a>
                </td>
                <td *ngIf="fetchResults.results.length > 0" class="uk-text-center">
                  <a *ngIf="properties.adminToolsPortalType !== 'eosc'" 
                     [queryParams]="routerHelper.createQueryParams(['f0', 'fv0', 'f1', 'fv1', 'f2', 'fv2', 'qf'], ['collectedfromdatasourceid', dataproviderId, 'resulthostingdatasourceid,or', dataproviderId, 'resulthostingdatasourceid', result.id, 'false'])" 
                     routerLinkActive="router-link-active" [routerLink]="properties.searchLinkToAdvancedResults" 
                     (click)="onLinkClick()">
                    {{result.count | number}}
                  </a>
                  <a *ngIf="properties.adminToolsPortalType == 'eosc'" class="custom-external" target="_blank"
                     [href]="'https://explore.openaire.eu'+properties.searchLinkToAdvancedResults+'?f0=collectedfromdatasourceid&fv0='+dataproviderId+'&f1=resulthostingdatasourceid,or&fv1='+dataproviderId+'&f2=resulthostingdatasourceid&fv2='+result.id+'&qf=false'">
                    {{result.count | number}}
                  </a>
                </td>
              </tr>
            </ng-container>
          </ng-container>
        </tbody>
      </table>
      <paging-no-load *ngIf="results.length >pageSize"
                      (pageChange)="updatePage($event)"
                      [currentPage]="page" 
                      [size]="pageSize"
                      [totalResults]="results.length">
      </paging-no-load>
    </div>
  `
})

export class RelatedDatasourcesTabComponent {
    @Input() prevPath: string = "";
    @Input() dataproviderId: string;
    // @Input() fetchPublications : FetchResearchResults;
    // @Input() fetchDatasets : FetchResearchResults;
    // @Input() fetchSoftware : FetchResearchResults;
    // @Input() fetchOrps: FetchResearchResults;
    @Input() fetchResults: FetchResearchResults;

    // true: preprocessing is not over
    @Input() loading: boolean = true;
    // Εvery datasource's id is a single key of a map
    //@Input() results: Map<string, {"name": string, "countPublications": string, "countDatasets": string, "countSoftware": string, "countOrps": string}>;
    @Input() results: {"id": string, "name": string, "count": number}[];
    @Input() properties:EnvProperties ;
    @Input() collectedFromName: string ="[no title available]";
    @Input() modal: AlertModal;

    public routerHelper:RouterHelper = new RouterHelper();
    public errorCodes:ErrorCodes = new ErrorCodes();

    public page: number = 1;
    public pageSize: number = 10;
    public dataProviderUrl;

  public openaireEntities = OpenaireEntities;

  constructor () {}

    ngOnInit() {
      this.dataProviderUrl = properties.searchLinkToDataProvider.split('?')[0];
    }

    ngOnDestroy() {}

    totalPages(totalResults: number): number {
      let totalPages:any = totalResults/this.pageSize;
      if(!(Number.isInteger(totalPages))) {
          totalPages = (parseInt(totalPages, this.pageSize) + 1);
      }
      return totalPages;
    }

    updatePage($event) {
      this.page = $event.value;
      this.scrollToTabTop("relatedDatasources");
    }

    scrollToTabTop(tabId:string){
      setTimeout(() => {
        window.scrollTo({
          top: document.getElementById(tabId) ? document.getElementById(tabId).offsetTop - 250 : 250,
          behavior: 'smooth'
        });
      }, 200);
    }

    public getKeys( map) {
      return Array.from(map.keys());
    }

    public onLinkClick() {
      if(this.modal) {
        this.modal.cancel();
      }
    }

  public addEoscPrevInParams(obj) {
    if(properties.adminToolsPortalType == "eosc" && this.prevPath) {
      let splitted: string[] = this.prevPath.split("?");
      obj = this.routerHelper.addQueryParam("return_path", splitted[0], obj);
      if(splitted.length > 0) {
        obj = this.routerHelper.addQueryParam("search_params", splitted[1], obj);
      }
    }
    return obj;
  }
}
