import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PreviousRouteRecorder } from "../../utils/piwik/previousRouteRecorder.guard";
import { OrganizationComponent } from "./organization.component";


@NgModule({
  imports: [
    RouterModule.forChild([{ path: '', component: OrganizationComponent, canDeactivate: [PreviousRouteRecorder] }])
  ]
})
export class OrganizationRoutingModule { }
