import { NgModule}            from '@angular/core';
import { CommonModule }       from '@angular/common';
import { FormsModule }        from '@angular/forms';

 import { OrganizationsDeletedByInferenceComponent } from './deletedByInference.component';
 import { OrganizationsDeletedByInferenceService } from './deletedByInference.service';

import {ResultLandingUtilsModule} from '../../landing-utils/resultLandingUtils.module';

 import {PagingModule}        from '../../../utils/paging.module';

 import {ErrorMessagesModule} from '../../../utils/errorMessages.module';
import {ShowAuthorsModule} from "../../../utils/authors/showAuthors.module";
import {LandingModule} from "../../landing-utils/landing.module";
import {NoLoadPaging} from "../../../searchPages/searchUtils/no-load-paging.module";
import {ResultPreviewModule} from "../../../utils/result-preview/result-preview.module";

@NgModule({
  imports: [
    CommonModule, FormsModule, ResultLandingUtilsModule,
    PagingModule, ErrorMessagesModule, ShowAuthorsModule, NoLoadPaging, ResultPreviewModule
  ],
  declarations: [
    OrganizationsDeletedByInferenceComponent
  ],
  providers:[
    OrganizationsDeletedByInferenceService
   ],
  exports: [
    OrganizationsDeletedByInferenceComponent
  ]
})
export class OrganizationsDeletedByInferenceModule { }
