import {Component}         from '@angular/core';
import {ElementRef, Input}            from '@angular/core';
import {ActivatedRoute, Router}       from '@angular/router';

import {EnvProperties}                from '../../../utils/properties/env-properties';
import {OrganizationInfo}     from '../../../utils/entities/organizationInfo';
import {RouterHelper}                 from '../../../utils/routerHelper.class';
import {ErrorCodes}                   from '../../../utils/properties/errorCodes';

import {OrganizationsDeletedByInferenceService}    from './deletedByInference.service';
import {ResultPreview} from "../../../utils/result-preview/result-preview";
import {AlertModal} from "../../../utils/modal/alert";
import {Subscriber} from "rxjs";
import {properties} from "../../../../../environments/environment";
import {HelperFunctions} from "../../../utils/HelperFunctions.class";

@Component({
    selector: 'organizationsDeletedByInference',
    template: `
      <div id="versions_container">
        <errorMessages [status]="[status]" [type]="type" tab_error_class=true></errorMessages>
        <no-load-paging *ngIf="resultsPreview.length > pageSize" [type]="type"
                        (pageChange)="updatePage($event)"
                        [page]="page" [pageSize]="pageSize"
                        [totalResults]="resultsPreview.length">
        </no-load-paging>
        <ul class="uk-list uk-list-divider  uk-margin">
          <li *ngFor="let result of resultsPreview.slice((page-1)*pageSize, page*pageSize)">
            <result-preview [modal]="modal" [properties]="properties" [hasLink]="false" [result]="result"
                            [isCard]="false" [prevPath]="prevPath" [isDeletedByInferenceModal]="true"></result-preview>
          </li>
        </ul>
        <no-load-paging *ngIf="resultsPreview.length > pageSize" [type]="type"
                        (pageChange)="updatePage($event)"
                        [page]="page" [pageSize]="pageSize"
                        [totalResults]="resultsPreview.length">
        </no-load-paging>
      </div>
    `
})
export class OrganizationsDeletedByInferenceComponent {
  @Input() prevPath: string = "";
  public resultsPreview: ResultPreview[] = [];
  @Input() children = [];

  @Input() id: string;
  @Input() ids: string[] = [];
  @Input() type: string;
  @Input() modal: AlertModal;

  // Custom tab paging variables
  public page: number = 1;
  public pageSize: number = 5;

  public status: number;
  public routerHelper:RouterHelper = new RouterHelper();
  public errorCodes:ErrorCodes = new ErrorCodes();

  subscriptions = [];
  properties:EnvProperties = properties;

  constructor ( private element: ElementRef,
                private _deletedByInferenceService: OrganizationsDeletedByInferenceService,
                private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.subscriptions.push(this.route.queryParams.subscribe(data => {
       this.errorCodes = new ErrorCodes();
       this.status = this.errorCodes.LOADING;

      this.parseDeletedByInference();
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscriber) {
        subscription.unsubscribe();
      }
    });
  }

  public parseDeletedByInference() {
    let length = Array.isArray(this.children) ? this.children.length : 1;
    for (let i = 0; i < length; i++) {
      let result = Array.isArray(this.children) ? this.children[i] : this.children;
      let preview = new ResultPreview();

      if(result.hasOwnProperty("websiteurl")) {
        preview.websiteURL = result.websiteurl;
      }
      if(result.hasOwnProperty("legalshortname")) {
        preview.title = result.legalshortname;
      }
      if(result.hasOwnProperty("legalname")) {
        if(preview.title && preview.title != result.legalname) {
          preview.title += "("+result.legalname+")";
        } else {
          preview.title = result.legalname;
        }
      }

      if(result.hasOwnProperty("country")) {
        preview.countries = [result['country'].classname];
      }

      preview.resultType = 'organization';

      this.resultsPreview.push(preview);
    }
    this.status = this.errorCodes.DONE;
  }

  public getResultPreview(result: OrganizationInfo): ResultPreview {
    return ResultPreview.organizationInfoConvert(result);
  }

  public totalPages(totalResults: number): number {
    let totalPages:any = totalResults/this.pageSize;
    if(!(Number.isInteger(totalPages))) {
        totalPages = (parseInt(totalPages, this.pageSize) + 1);
    }
    return totalPages;
  }

  public updatePage($event) {
    this.page = $event.value;
    HelperFunctions.scrollToId("versions_container");
  }
}
