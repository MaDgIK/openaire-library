import {ChangeDetectorRef, Component, ElementRef, Input, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Meta, Title} from '@angular/platform-browser';
import {OrganizationService} from '../../services/organization.service';
import {OrganizationInfo} from '../../utils/entities/organizationInfo';
import {ReportsService} from '../../services/reports.service';
import {FetchProjects} from '../../utils/fetchEntitiesClasses/fetchProjects.class';
import {SearchResearchResultsService} from '../../services/searchResearchResults.service';
import {SearchDataprovidersService} from '../../services/searchDataproviders.service';
import {SearchProjectsService} from '../../services/searchProjects.service';
import {ErrorCodes} from '../../utils/properties/errorCodes';
import {RouterHelper} from '../../utils/routerHelper.class';

import {ModalLoading} from '../../utils/modal/loading.component';
import {PiwikService} from '../../utils/piwik/piwik.service';
import {Identifier, StringUtils} from '../../utils/string-utils.class';
import {EnvProperties} from '../../utils/properties/env-properties';
import {SEOService} from '../../sharedComponents/SEO/SEO.service';
import {HelperFunctions} from "../../utils/HelperFunctions.class";
import {HelperService} from "../../utils/helper/helper.service";
import {Location} from "@angular/common";
import {FetchResearchResults} from "../../utils/fetchEntitiesClasses/fetchResearchResults.class";
import {FetchDataproviders} from "../../utils/fetchEntitiesClasses/fetchDataproviders.class";
import {IndexInfoService} from "../../utils/indexInfo.service";
import {Subscriber} from "rxjs";
import {properties} from "../../../../environments/environment";
import {OpenaireEntities} from '../../utils/properties/searchFields';
import {Option} from '../../sharedComponents/input/input.component';
import {LayoutService} from '../../dashboard/sharedComponents/sidebar/layout.service';
import {FullScreenModalComponent} from "../../utils/modal/full-screen-modal/full-screen-modal.component";

declare var ResizeObserver;

@Component({
  selector: 'organization',
  templateUrl: 'organization.component.html',
})
export class OrganizationComponent {
  public referrer: string;
  public prevPath: string;
  @Input() communityId = null;

  public organizationInfo: OrganizationInfo;
  public organizationId: string;
  public identifier: Identifier = null;

  // Message variables
  public warningMessage = "";
  public errorMessage = "";
  public showLoading: boolean = true;

  // CSV variables
  public downloadURLAPI: string;
  public csvProjectParamsHead: string;
  public csvParamsTail: string;

  // Variables for publications, research data, projects, dataproviders tabs
  public fetchProjects: FetchProjects;
  public fetchPublications: FetchResearchResults;
  public fetchDatasets: FetchResearchResults;
  public fetchSoftware: FetchResearchResults;
  public fetchOrps: FetchResearchResults;
  public fetchDataproviders: FetchDataproviders;
  public searchNumber: number = 5;

  // Request results of each tab only the one time (first time tab is clicked)
  private reloadPublications: boolean = true;
  private reloadDatasets: boolean = true;
  private reloadSoftware: boolean = true;
  private reloadOrps: boolean = true;
  private reloadDataproviders: boolean = true;

  // Active tab variable for responsiveness
  public activeTab: string = "";
  @ViewChild('fsModal') fsModal: FullScreenModalComponent;

  @ViewChild('downloadReportsModal') downloadReportsModal;
	@ViewChild('downloadReportsFsModal') downloadReportsFsModal: FullScreenModalComponent;
  // @ViewChild('downloadReportModal') downloadReportModal;
  // @ViewChild('downloadFunderReportModal') downloadFunderReportModal;
	@ViewChild('addThisFsModal') addThisFsModal: FullScreenModalComponent;

  @ViewChild(ModalLoading) loading: ModalLoading;
  // Alert box when CSV: Project Publications for a funder is requested
  @ViewChild('AlertModalApplyAll') alertApplyAll;
  @ViewChild('AlertFsModalApplyAll') alertFsApplyAll;

  // Alert box when something is wrong with CSV requests
  @ViewChild('AlertModalCsvError') alertCsvError;
  @ViewChild('AlertFsModalCsvError') alertFsCsvError;

  public routerHelper: RouterHelper = new RouterHelper();
  public errorCodes: ErrorCodes = new ErrorCodes();
  public pageContents = null;
  public divContents = null;

  // Helper variables to specify funder in downloadPublicationsFile function
  public contentTypes: [string, string, string][] = [
    ['results', 'all ' + OpenaireEntities.RESULTS, OpenaireEntities.RESULTS_FILE],
    ['publications', OpenaireEntities.PUBLICATIONS, OpenaireEntities.PUBLICATIONS_FILE],
    ['datasets', OpenaireEntities.DATASETS, OpenaireEntities.DATASETS_FILE],
    ['software', OpenaireEntities.SOFTWARE, OpenaireEntities.SOFTWARE_FILE],
    ['other', OpenaireEntities.OTHER, OpenaireEntities.OTHER_FILE],
  ];
  public contentFileName: string = '';
  public funderContentType: string = '';
  public funder: any = "";
  private funderId: string;
  private funderCount: number;

	public funderOptions: Option[] = [];

	public offset: number;
	public stickyHeader: boolean = false;
	public graph_offset: number = 0;
  public graph_height: number = 0;
  @ViewChild("graph_and_feedback") graph_and_feedback;

  subscriptions = [];
  innerReportSubscriptions = [];
  properties: EnvProperties = properties;
	public openaireEntities = OpenaireEntities;
  public indexUpdateDate: Date;
  public showFeedback: boolean = false;
  public feedbackFields: string [] = ['Name', 'Country', 'Other'];

  @ViewChild('AlertModalDeletedByInference') alertModalDeletedByInference;
  @ViewChild('AlertModalDeletedByInferenceFS') alertModalDeletedByInferenceFS;
  // @ViewChild('projectsModal') projectsModal;
  public deleteByInferenceOpened: boolean = false;

	public isMobile: boolean = false;
  public isBottomIntersecting: boolean = false;
  public mobileContent: "info" | "actions" = "info";
  public viewAllMobile: string = "";

  constructor(private element: ElementRef,
              private _organizationService: OrganizationService,
              private _piwikService: PiwikService,
              private  route: ActivatedRoute,
              private _searchDataprovidersService: SearchDataprovidersService,
              private _reportsService: ReportsService,
              private _searchResearchResultsService: SearchResearchResultsService,
              private _searchProjectsService: SearchProjectsService,
              private _meta: Meta,
              private _title: Title,
              private _router: Router,
              private helper: HelperService,
              private seoService: SEOService,
              private _location: Location,
              private indexInfoService: IndexInfoService,
              private cdr: ChangeDetectorRef,
							private layoutService: LayoutService) {
    this.fetchProjects = new FetchProjects(this._searchProjectsService);
    this.fetchDataproviders = new FetchDataproviders(this._searchDataprovidersService);
    this.fetchPublications = new FetchResearchResults(this._searchResearchResultsService);
    this.fetchDatasets = new FetchResearchResults(this._searchResearchResultsService);
    this.fetchSoftware = new FetchResearchResults(this._searchResearchResultsService);
    this.fetchOrps = new FetchResearchResults(this._searchResearchResultsService);
  }

  ngOnInit() {
		this.subscriptions.push(this.layoutService.isMobile.subscribe(isMobile => {
      this.isMobile = isMobile;
      this.cdr.detectChanges();
    }));
    this.subscriptions.push(this.layoutService.isBottomIntersecting.subscribe(isBottomIntersecting => {
      this.isBottomIntersecting = isBottomIntersecting;
      this.cdr.detectChanges();
    }));
		if (typeof document !== 'undefined') {
			this.subscriptions.push(this.indexInfoService.getLastIndexDate(this.properties).subscribe(lastIndexUpdate => {
				if (lastIndexUpdate) {
					this.indexUpdateDate = new Date(lastIndexUpdate);
				}
			}));
		}
    if(properties.adminToolsPortalType !== "explore") {
      //this.getDivContents();
      this.getPageContents();
    }
		this.updateUrl(this.properties.domain + this.properties.baseLink + this._router.url);
		this.seoService.createLinkForCanonicalURL(this.properties.domain +this.properties.baseLink +  this._router.url);

    this.subscriptions.push(this.route.queryParams.subscribe(params => {
      this.organizationInfo = null;
      this.updateTitle("Organization");
      this.updateDescription("");

      if(params["return_path"]) {
        this.prevPath = params["return_path"] + (params["search_params"] ? ("?"+params["search_params"]) : "");
      }
      if((typeof document !== 'undefined') && document.referrer) {
        this.referrer = document.referrer;
      }

      if (params['organizationId']) {
        this.organizationId = params['organizationId'];
      } else if (params["pid"]) {
        this.identifier = Identifier.getIdentifierFromString(decodeURIComponent(params["pid"]), false);
      }

      if ((this.organizationId  && StringUtils.isOpenAIREID(this.organizationId)) || (this.identifier)) {
        this.getOrganizationInfo();
      } else {
        this.showLoading = false;
        this._router.navigate([this.properties.errorLink], {
          queryParams: {
            "page": this._location.path(true),
            "page_type": "organization"
          }
        });
      }
      this.csvParamsTail = '" and relorganizationid exact "' + this.organizationId + '" ))';
    }));
    this.downloadURLAPI = this.properties.csvAPIURL;
    //this.csvAffiliatedPublications = this.downloadURLAPI + "?format=csv&type=publications&fq=(((oaftype exact result) and (resulttypeid exact publication)) and (relorganizationid exact \"" + this.organizationId + "\"))";
    this.csvProjectParamsHead = 'format=csv&type=projects&fq=((funder exact "';
    //this.csvPublicationParamsHead = 'format=csv-special&type=publications&page=0&query=((((oaftype exact result) and (resulttypeid exact publication)) and (funderid exact ';
  }

  ngAfterViewInit() {
    if (typeof document !== 'undefined') {
      if(document.getElementById("main-menu")) {
        this.offset = Number.parseInt(getComputedStyle(document.documentElement).getPropertyValue('--header-height'));
      } else {
        this.offset = 0;
      }
      if(this.graph_and_feedback) {
        this.observeGraphAndFeedback();
      }
    }
  }

  ngAfterContentChecked() {
    if(this.graph_and_feedback && typeof document !== 'undefined') {
      this.graph_offset = this.calcGraphOffset(this.graph_and_feedback.nativeElement);
    }
  }

  public observeGraphAndFeedback() {
    let resizeObs = new ResizeObserver(entries => {
      entries.forEach(entry => {
        setTimeout(() => {
          // console.log(entry);
          this.graph_offset = this.calcGraphOffset(entry.target);
          this.cdr.detectChanges();
        });
      })
    });
    this.subscriptions.push(resizeObs);
    resizeObs.observe(this.graph_and_feedback.nativeElement);
  }

  calcGraphOffset(element) {
    this.graph_height = element.offsetHeight;
    return window.innerHeight-this.graph_height;
  }

  private getPageContents() {
    if(this.communityId) {
      this.subscriptions.push(this.helper.getPageHelpContents(this.properties, this.communityId, this._router.url).subscribe(contents => {
        this.pageContents = contents;
      }));
    }
  }

  private getDivContents() {
    if(this.communityId) {
      this.subscriptions.push(this.helper.getDivHelpContents(this.properties, this.communityId, this._router.url).subscribe(contents => {
        this.divContents = contents;
      }));
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscriber) {
        subscription.unsubscribe();
      } else if ((typeof ResizeObserver != 'undefined' && subscription instanceof ResizeObserver) ||
          (typeof IntersectionObserver != 'undefined' && subscription instanceof IntersectionObserver)) {
        subscription.disconnect();
      }
    });
    this.innerReportSubscriptions.forEach(subscription => {
      if(subscription instanceof Subscriber) {
        subscription.unsubscribe();
      }
    });

    this.fetchDatasets.clearSubscriptions();
    this.fetchPublications.clearSubscriptions();
    this.fetchSoftware.clearSubscriptions();
    this.fetchOrps.clearSubscriptions();
    this.fetchDataproviders.clearSubscriptions();
    this.fetchProjects.clearSubscriptions();
  }

  private getTotalResearchResults() {
    this.fetchPublications.getNumForEntity('publication', 'organization', this.organizationId, this.properties);
    this.fetchDatasets.getNumForEntity('dataset', 'organization', this.organizationId, this.properties);
    this.fetchSoftware.getNumForEntity('software', 'organization', this.organizationId, this.properties);
    this.fetchOrps.getNumForEntity('other', 'organization', this.organizationId, this.properties);
  }

  private getTotalDataproviders() {
    this.fetchDataproviders.getNumForEntity('organization', this.organizationId, this.properties);
  }

  // private searchContentProviders() {
  //   this.fetchDataproviders.getResultsForEntity("organization", this.organizationId, 1, this.searchNumber, this.properties);
  // }
  private searchContentProviders(page: number, size: number) {
    if (this.reloadDataproviders &&
      (this.fetchDataproviders.searchUtils.status == this.errorCodes.LOADING ||
        (this.fetchDataproviders.searchUtils.status == this.errorCodes.DONE && this.fetchDataproviders.searchUtils.totalResults > 0)
      )
    ) {
      this.fetchDataproviders.getResultsForEntity("organization", this.organizationId, page, size, this.properties);
    }
    this.reloadDataproviders = false;
  }

  // private searchPublications() {
  //   this.fetchPublications.getResultsForEntity("publication", "organization", this.organizationId, 1, size, this.properties);
  // }
  private searchPublications(page: number, size: number) {
    if (this.reloadPublications &&
      (this.fetchPublications.searchUtils.status == this.errorCodes.LOADING ||
        (this.fetchPublications.searchUtils.status == this.errorCodes.DONE && this.fetchPublications.searchUtils.totalResults > 0)
      )
    ) {
      this.fetchPublications.getResultsForEntity("publication", "organization", this.organizationId, page, size, this.properties);
    }
    this.reloadPublications = false;
  }

  // private searchDatasets() {
  //   this.fetchDatasets.getResultsForEntity("dataset", "organization", this.organizationId, 1, this.searchNumber, this.properties);
  // }
  private searchDatasets(page: number, size: number) {
    if (this.reloadDatasets &&
      (this.fetchDatasets.searchUtils.status == this.errorCodes.LOADING ||
        (this.fetchDatasets.searchUtils.status == this.errorCodes.DONE && this.fetchDatasets.searchUtils.totalResults > 0)
      )
    ) {
      this.fetchDatasets.getResultsForEntity("dataset", "organization", this.organizationId, page, size, this.properties);
    }
    this.reloadDatasets = false;
  }

  // private searchSoftware() {
  //   this.fetchSoftware.getResultsForEntity("software", "organization", this.organizationId, 1, this.searchNumber, this.properties);
  // }
  private searchSoftware(page: number, size: number) {
    if (this.reloadSoftware &&
      (this.fetchSoftware.searchUtils.status == this.errorCodes.LOADING ||
        (this.fetchSoftware.searchUtils.status == this.errorCodes.DONE && this.fetchSoftware.searchUtils.totalResults > 0)
      )
    ) {
      this.fetchSoftware.getResultsForEntity("software", "organization", this.organizationId, page, size, this.properties);
    }
    this.reloadSoftware = false;
  }

  // private searchOrps() {
  //   this.fetchOrps.getResultsForEntity("other", "organization", this.organizationId, 1, this.searchNumber, this.properties);
  // }
  private searchOrps(page: number, size: number) {
    if (this.reloadOrps &&
      (this.fetchOrps.searchUtils.status == this.errorCodes.LOADING ||
        (this.fetchOrps.searchUtils.status == this.errorCodes.DONE && this.fetchOrps.searchUtils.totalResults > 0)
      )
    ) {
      this.fetchOrps.getResultsForEntity("other", "organization", this.organizationId, page, size, this.properties);
    }
    this.reloadOrps = false;
  }

  private getOrganizationInfo() {

    this.warningMessage = '';
    this.errorMessage = ""
    this.showLoading = true;

    this.organizationInfo = null;

    if ((this.organizationId == null || this.organizationId == '') && this.identifier == null) {
      this.showLoading = false;
      this.warningMessage = "No valid organization id";
    } else {
      this.subscriptions.push(this._organizationService.getOrganizationInfo(this.organizationId, this.identifier, this.properties).subscribe(
        data => {
          if (data == null) {
            this.showLoading = false;
            this._router.navigate([this.properties.errorLink], {
              queryParams: {
                "page": this._location.path(true),
                "page_type": "organization"
              }
            });
            this.errorMessage = 'No ' + OpenaireEntities.ORGANIZATION + ' found';
          } else {
            this.organizationInfo = data;
            this.organizationId = this.organizationInfo.objIdentifier; // reset in case the parameter was a canonical url. The related queries  and reports should have the dedup id
            this.organizationInfo.setBelongsTo(this.organizationId);
            this.csvParamsTail = '" and relorganizationid exact "' + this.organizationId + '" ))';
            this.seoService.createLinkForCanonicalURL(this.properties.domain + this.properties.baseLink + this.properties.searchLinkToOrganization + this.organizationInfo.relcanId);
            this.updateTitle((this.organizationInfo.title.name ? this.organizationInfo.title.name : (this.organizationInfo.name ? this.organizationInfo.name : 'No title available')));
            this.updateDescription("Organization, country, " +
              (this.organizationInfo.title.name ? this.organizationInfo.title.name : 'No title available') +
              ((this.organizationInfo.title.name && this.organizationInfo.title.name != this.organizationInfo.name) ? (', ') : '') +
              (this.organizationInfo.name ? this.organizationInfo.name : ''));
            this.subscriptions.push(this._piwikService.trackView(this.properties, this.organizationInfo.title.name).subscribe());
            var refineFields: string [] = ["funder"];

            if (typeof document !== 'undefined') {
              this.getTotalResearchResults();
              this.getTotalDataproviders();
            }

            this.fetchProjects.getResultsForOrganizations(this.organizationId, "", 1, this.searchNumber, refineFields, this.properties);
            // console.log(this.fetchProjects);
            this.showLoading = false;
          }
        },
        err => {
          //console.log(err)
          this.handleError("Error getting organization for id: " + this.organizationId, err);
          if (err.status == 404) {
            this._router.navigate([this.properties.errorLink], {
              queryParams: {
                "page": this._location.path(true),
                "page_type": "organization"
              }
            });
          } else if (err.name == "TimeoutError") {
            this.errorMessage = 'An error occurred please try again later';
          } else {
            this.seoService.createLinkForCanonicalURL(this.properties.domain + this.properties.baseLink + this.properties.searchLinkToOrganizations);
            this.errorMessage = 'No organization found';
          }
          this.showLoading = false;
        }
      ));
    }
  }

  public downloadFile(url: string, filename: string) {
    this.openLoading();
    this.setMessageLoading("Downloading CSV file");

    this.subscriptions.push(this._reportsService.downloadCSVFile(url).subscribe(
      data => {
        this.closeLoading();

        var url = window.URL.createObjectURL(data);
        var a = window.document.createElement('a');
        window.document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = url;
        a.download = filename + ".csv";
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove(); // remove the element

        //window.open(window.URL.createObjectURL(data));
        this.subscriptions.push(this._piwikService.trackDownload(this.properties, "DownloadCSV" + filename).subscribe());
      },
      err => {
        //console.log("Error downloading the file.");
        this.handleError("Error downloading file: " + filename + ".csv", err);

        this.closeLoading();
        this.confirmOpenCsvError();
      }/*,
           () => console.log('Completed file download.')*/
    ));
  }

  private downloadFileByFunder() {

    this.openLoading();
    this.setMessageLoading("Downloading CSV file");

    let response: string[] = [];
    let totalResponse: string = "";
    let projects = [];
    let counter: number = this.funderCount;
    let title: boolean = false;
    let title_index: number = 0;

    let filename: string = 'funder-project-' + this.contentFileName + '-report';

    this.subscriptions.push(this._searchProjectsService.getProjectsForOrganizations(this.organizationId, '(funder exact "' + this.encodeURI(this.funderId) + '" ) ', 1, this.funderCount, [], this.properties).subscribe(
      data => {
        projects = data[1];
        for (let index = 0; index < projects.length; index++) {
          this.innerReportSubscriptions.push(this._searchResearchResultsService.numOfEntityResults(this.funderContentType, projects[index].id, "project", this.properties).subscribe(
            data => {
              //if(data == 0 && title) {   // if no publications for this project
              if (data == 0 && (counter > 1 || title)) {   // if no publications for this project
                counter--;
                response[index] = "";
                //console.info("index: "+index, "counter: "+counter, "id:"+projects[index].id, response[index]);

                if (counter == 0) {
                  //for(let i=count-1; i>=0; i--) {
                  for (let i = 0; i < projects.length; i++) {
                    if (response[i] != "") {
                      if (i == title_index) {
                        totalResponse = response[i] + totalResponse;
                      } else {
                        totalResponse += response[i];
                      }
                    }
                  }
                  this.closeLoading();

                  var csvurl = window.URL.createObjectURL(new Blob([totalResponse], {type: 'text/csv'}));
                  var a = window.document.createElement('a');
                  window.document.body.appendChild(a);
                  a.setAttribute('style', 'display: none');
                  a.href = csvurl;
                  a.download = filename + ".csv";
                  a.click();
                  window.URL.revokeObjectURL(csvurl);
                  a.remove(); // remove the element
                  this.subscriptions.push(this._piwikService.trackDownload(this.properties, "DownloadCSV" + filename).subscribe());
                }
              } else {
                let url: string;
                if (!title) {
                  title_index = index;
                  //console.info(title_index);
                  url = this.downloadURLAPI + '?format=csv-special&' + this.getTypeParam(this.funderContentType) + '&fq=((relprojectid exact "' + projects[index].id + '"))';
                } else {
                  url = this.downloadURLAPI + '?format=csv-special-notitle&' + this.getTypeParam(this.funderContentType) + '&fq=((relprojectid exact "' + projects[index].id + '"))';
                }
                title = true;

                this.innerReportSubscriptions.push(this._reportsService.getCSVResponse(url).subscribe(
                  data => {
                    counter--;
                    response[index] = data;
                    //console.info("index: "+index, "counter: "+counter, "id:"+projects[index].id, response[index]);

                    if (counter == 0) {
                      //for(let i=count-1; i>=0; i--) {
                      for (let i = 0; i < projects.length; i++) {
                        if (response[i] != "") {
                          if (i == title_index) {
                            totalResponse = response[i] + totalResponse;
                          } else {
                            totalResponse += response[i];
                          }
                        }
                      }
                      this.closeLoading();

                      var csvurl = window.URL.createObjectURL(new Blob([totalResponse], {type: 'text/csv'}));
                      var a = window.document.createElement('a');
                      window.document.body.appendChild(a);
                      a.setAttribute('style', 'display: none');
                      a.href = csvurl;
                      a.download = filename + ".csv";
                      a.click();
                      window.URL.revokeObjectURL(csvurl);
                      a.remove(); // remove the element
                      this.subscriptions.push(this._piwikService.trackDownload(this.properties, "DownloadCSV" + filename).subscribe());
                    }
                  },
                  err => {
                    this.handleError("Error downloading file: " + filename, err);

                    this.closeLoading();
                    this.confirmOpenCsvError();
                    this.innerReportSubscriptions.forEach(subscription => subscription.unsubscribe());
                  }/*,
                            () => console.log('Completed file download.')*/
                ) );
              }
            },
            err => {
              this.handleError("Error getting number of publications for project with id: " + projects[index].id, err);
              this.closeLoading();
              this.confirmOpenCsvError();
              this.innerReportSubscriptions.forEach(subscription => subscription.unsubscribe());
            }));
        }
      },
      err => {
        this.handleError("Error getting projects for organization with id: " + this.organizationId, err);

        this.closeLoading();
        this.confirmOpenCsvError();
      }
    ));
  }

  private updateDescription(description: string) {
    this._meta.updateTag({content: description.substring(0, 160)}, "name='description'");
    this._meta.updateTag({content: description.substring(0, 160)}, "property='og:description'");
  }

  private updateTitle(title: string) {
    var _prefix = "";
    // if(!this.communityId) {
    //   _prefix = "OpenAIRE | ";
    // }
    // var _title = _prefix + ((title.length > 50) ? title.substring(0, 50) : title);
    this._title.setTitle(title);
    this._meta.updateTag({content: title}, "property='og:title'");
  }

  private updateUrl(url: string) {
    this._meta.updateTag({content: url}, "property='og:url'");
  }

  private openLoading() {
		this.closeDownloadReportsModal();

    if (this.loading) {
      this.loading.open();
    }
  }

  private closeLoading() {
    if (this.loading) {
      this.loading.close();
    }
  }

  private setMessageLoading(message: string) {
    if (this.loading) {
      this.loading.message = message;
    }
  }

  public confirmOpenApplyAll(contentType: string, contentFileName: string) {
    if(this.isMobile) {
      this.alertFsApplyAll.title = "CSV FILE";
      this.alertFsApplyAll.okButton = false;
      this.alertFsApplyAll.open();
    } else {
      this.alertApplyAll.cancelButton = true;
      this.alertApplyAll.okButton = true;
      this.alertApplyAll.alertTitle = "CSV FILE";
      this.alertApplyAll.message = "Do you wish to download a CSV file? Note that this process may take a while.";
      this.alertApplyAll.okButtonText = "Yes";
      this.alertApplyAll.cancelButtonText = "No";
      this.alertApplyAll.open();
    }
    this.funderId = this.funder.id;
    this.funderCount = this.funder.number;
    this.funderContentType = contentType;
    this.contentFileName = contentFileName;
  }

  public confirmCloseApplyAll() {
    this.downloadFileByFunder();
  }

  public confirmOpenCsvError() {
    if(this.isMobile) {
      this.alertFsCsvError.okButton = true;
      this.alertFsCsvError.okButtonText = "OK";
      this.alertFsCsvError.title = "ERROR DOWNLOADING CSV FILE";
      this.alertFsCsvError.open();
    } else {
      this.alertCsvError.cancelButton = false;
      this.alertCsvError.okButton = true;
      this.alertCsvError.alertTitle = "ERROR DOWNLOADING CSV FILE";
      this.alertCsvError.message = "There was an error in csv downloading. Please try again later.";
      this.alertCsvError.okButtonText = "OK";
      this.alertCsvError.open();
    }
  }

  encodeURI(input: string): string {
    return StringUtils.URIEncode(input);
  }

  private handleError(message: string, error) {
    console.error("Organizaton Landing Page: " + message, error);
  }

  openDeletedByInference() {
    this.deleteByInferenceOpened = true;
    this.alertModalDeletedByInference.cancelButton = false;
    this.alertModalDeletedByInference.okButton = false;
    this.alertModalDeletedByInference.alertTitle = "Other versions";
    this.alertModalDeletedByInference.open();
  }

  public getTypeParam(type: string): string {
    if (type == 'results') {
      type = 'publications&type=datasets&type=software&type=other';
    }
    return 'type=' + type;
  }

  getCSVAffiliated(contentType: string): string {
    return this.downloadURLAPI + '?format=csv&' + this.getTypeParam(contentType) + '&fq=(relorganizationid exact "' + this.organizationId + '")';
  }

  getFunderProjects(): string {
    return this.downloadURLAPI + '?' + this.csvProjectParamsHead + encodeURI(this.funder.id) + this.csvParamsTail;
  }

	openDownloadReportsModal() {
		// this.funder = this.fetchProjects.funders[0];
    this.downloadReportsModal.cancelButton = false;
    this.downloadReportsModal.okButton = false;
    this.downloadReportsModal.alertTitle = "Download";
    this.downloadReportsModal.open();
	}

	closeDownloadReportsModal() {
    this.downloadReportsModal.cancel();
  }

  public getParamsForSearchLink(type: string = "") {
    if(type) {
      return this.routerHelper.createQueryParams(['f0', 'fv0', 'type', 'qf', 'sortBy'], ['relorganizationid', this.organizationId, type, 'false', 'resultdateofacceptance,descending']);
    } else {
      return this.routerHelper.createQueryParams(['f0', 'fv0'], ['relorganizationid', this.organizationId]);
    }
  }

  public onSelectActiveTab(activeTabId) {
    if (this.activeTab != activeTabId) {   // tab really changed
      if (activeTabId == 'projects') {
        this.activeTab = 'projects';
      } else if (activeTabId == 'dataProviders') {
        this.activeTab = 'dataProviders';
        this.searchContentProviders(1, this.searchNumber);
      } else if (activeTabId == 'publications') {
        this.activeTab = 'publications';
        this.searchPublications(1, this.searchNumber);
      } else if (activeTabId == 'datasets') {
        this.activeTab = 'datasets';
        this.searchDatasets(1, this.searchNumber);
      } else if (activeTabId == 'software') {
        this.activeTab = 'software';
        this.searchSoftware(1, this.searchNumber);
      } else if (activeTabId == 'other') {
        this.activeTab = "other";
        this.searchOrps(1, this.searchNumber);
      }
    }
  }

  public get total(): number {
    let total = this.fetchPublications.searchUtils.totalResults;
    total += this.fetchDatasets.searchUtils.totalResults;
    total += this.fetchSoftware.searchUtils.totalResults;
    total += this.fetchOrps.searchUtils.totalResults;
    return total;
  }

  // public get numberOfTabs(): number {
  //   if (this.tabsAreInitialized) {
  //     return this._numberOfTabs;
  //   }
  //   if (!this.organizationInfo
  //     || this.fetchProjects.searchUtils.status === this.errorCodes.LOADING
  //     || this.fetchDataproviders.searchUtils.status === this.errorCodes.LOADING
  //     || this.fetchPublications.searchUtils.status == this.errorCodes.LOADING
  //     || this.fetchDatasets.searchUtils.status == this.errorCodes.LOADING
  //     || this.fetchSoftware.searchUtils.status == this.errorCodes.LOADING
  //     || this.fetchOrps.searchUtils.status == this.errorCodes.LOADING) {
  //     return 0;
  //   }
  //   if (this.fetchProjects.searchUtils.totalResults > 0) {
  //     if (this._numberOfTabs == 0) {
  //       this.firstTab = 'projects';
  //     }
  //     this._numberOfTabs++;
  //   }
  //   if (this.fetchDataproviders.searchUtils.totalResults > 0) {
  //     if (this._numberOfTabs == 0) {
  //       this.firstTab = 'dataProviders';
  //       this.searchContentProviders(1, this.searchNumber);
  //     }
  //     this._numberOfTabs++;
  //   }
  //   if (this.fetchPublications.searchUtils.totalResults > 0) {
  //     if (this._numberOfTabs == 0) {
  //       this.firstTab = 'publications';
  //       this.searchPublications(1, this.searchNumber);
  //     }
  //     this._numberOfTabs++;
  //   }
  //   if (this.fetchDatasets.searchUtils.totalResults > 0) {
  //     if (this._numberOfTabs == 0) {
  //       this.firstTab = 'datasets';
  //       this.searchDatasets(1, this.searchNumber);
  //     }
  //     this._numberOfTabs++;
  //   }
  //   if (this.fetchSoftware.searchUtils.totalResults > 0) {
  //     if (this._numberOfTabs == 0) {
  //       this.firstTab = 'software';
  //       this.searchSoftware(1, this.searchNumber);
  //     }
  //     this._numberOfTabs++;
  //   }
  //   if (this.fetchOrps.searchUtils.totalResults > 0) {
  //     if (this._numberOfTabs == 0) {
  //       this.firstTab = 'other';
  //       this.searchOrps(1, this.searchNumber);
  //     }
  //     this._numberOfTabs++;
  //   }
  //   this.activeTab = this.firstTab;
  //   this.tabsAreInitialized = true;
  //   return this._numberOfTabs;
  // }

	public buildFunderOptions() {
		this.funderOptions = [];
		if(this.fetchProjects.funders && this.fetchProjects.funders.length > 0) {
			for(let funder of this.fetchProjects.funders) {
				this.funderOptions.push({"label": funder.name, "value": funder});
			}
			this.funder = this.funderOptions[0].value;
		}
	}

  public scroll() {
    HelperFunctions.scroll();
  }

  public openFsModal(fsModal: FullScreenModalComponent, title: string) {
    fsModal.title = title;
    fsModal.okButton = false;
    fsModal.stayOpenInBack = true;
    fsModal.open();
  }

  public get eoscBackLink() {
    if(this.prevPath && this.referrer && ((this.referrer == "https://eosc-search-service.grid.cyfronet.pl/") || (this.referrer == this.properties.eoscMarketplaceURL))) {
      return this.referrer+this.prevPath;
    } else {
      return this.properties.eoscMarketplaceURL;
      // return "https://"+(this.properties.environment == "beta" ? "beta." : "")+"search.marketplace.eosc-portal.eu/";
    }
  }
}
