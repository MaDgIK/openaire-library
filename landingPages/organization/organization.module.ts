import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {LoadingModalModule} from '../../utils/modal/loadingModal.module';
import {AlertModalModule} from '../../utils/modal/alertModal.module';
import {ErrorMessagesModule} from '../../utils/errorMessages.module';

import {OrganizationServiceModule} from '../../services/organizationService.module';
import {OrganizationComponent} from './organization.component';
import {LandingModule} from '../landing-utils/landing.module';
import {DataProvidersServiceModule} from '../../services/dataProvidersService.module';
import {ReportsServiceModule} from '../../services/reportsService.module';
import {SearchResearchResultsServiceModule} from '../../services/searchResearchResultsService.module';
import {ProjectsServiceModule} from '../../services/projectsService.module';

import {ProjectsInModalModule} from '../landing-utils/projects-in-modal.module';

import {Schema2jsonldModule} from '../../sharedComponents/schema2jsonld/schema2jsonld.module';
import {SEOServiceModule} from '../../sharedComponents/SEO/SEOService.module';
import {HelperModule} from "../../utils/helper/helper.module";
import {OrganizationsDeletedByInferenceModule} from "./deletedByInference/deletedByInference.module";
import {LandingHeaderModule} from "../landing-utils/landing-header/landing-header.module";
import {FeedbackModule} from "../feedback/feedback.module";
import {TabsModule} from "../../utils/tabs/tabs.module";
import {SearchTabModule} from "../../utils/tabs/contents/search-tab.module";
import {LoadingModule} from '../../utils/loading/loading.module';
import {IconsModule} from '../../utils/icons/icons.module';
import {InputModule} from '../../sharedComponents/input/input.module';
import {IconsService} from '../../utils/icons/icons.service';
import {graph, versions} from "../../utils/icons/icons";
import {FullScreenModalModule} from "../../utils/modal/full-screen-modal/full-screen-modal.module";
import {EGIDataTransferModule} from "../../utils/dataTransfer/transferData.module";
import {EntityActionsModule} from "../../utils/entity-actions/entity-actions.module";
import {OrganizationRoutingModule} from "./organization-routing.module";
import {ResultLandingUtilsModule} from "../landing-utils/resultLandingUtils.module";


@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule,
    OrganizationRoutingModule,
    LoadingModalModule, AlertModalModule, ErrorMessagesModule,
    LandingModule,
    DataProvidersServiceModule,
    ReportsServiceModule,
    OrganizationServiceModule,
    ProjectsInModalModule,
    OrganizationServiceModule,
    SearchResearchResultsServiceModule,
    ProjectsServiceModule,
    Schema2jsonldModule, SEOServiceModule, HelperModule,
    OrganizationsDeletedByInferenceModule, LandingHeaderModule, FeedbackModule,
    TabsModule, SearchTabModule, LoadingModule, IconsModule, InputModule, FullScreenModalModule, EntityActionsModule, ResultLandingUtilsModule
  ],
  declarations: [
    OrganizationComponent,
  ],
  providers: [],
  exports: [
    OrganizationComponent
  ]
})
export class OrganizationModule {
	constructor(private iconsService: IconsService) {
		this.iconsService.registerIcons([graph, versions]);
	}
}
