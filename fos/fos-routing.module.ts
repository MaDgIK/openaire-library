import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {FosComponent} from './fos.component';
import {PreviousRouteRecorder} from '../utils/piwik/previousRouteRecorder.guard';
@NgModule({
  imports: [
    RouterModule.forChild([
			{ path: '', component: FosComponent, canDeactivate: [PreviousRouteRecorder] }
    ])
  ]
})
export class FosRoutingModule { }
