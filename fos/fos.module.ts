import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {IconsModule} from "../utils/icons/icons.module";
import {BreadcrumbsModule} from "../utils/breadcrumbs/breadcrumbs.module";
import {Schema2jsonldModule} from "../sharedComponents/schema2jsonld/schema2jsonld.module";
import {SearchInputModule} from "../sharedComponents/search-input/search-input.module";
import {SEOServiceModule} from "../sharedComponents/SEO/SEOService.module";

import {FosRoutingModule} from './fos-routing.module';
import {FosComponent} from './fos.component';

@NgModule({
	imports: [
		CommonModule, FormsModule, RouterModule,
		FosRoutingModule, BreadcrumbsModule, IconsModule,
		SearchInputModule, Schema2jsonldModule, SEOServiceModule
	],
	declarations: [
		FosComponent
	],
	providers: [],
	exports: [
		FosComponent
	]
})
export class FosModule {

}