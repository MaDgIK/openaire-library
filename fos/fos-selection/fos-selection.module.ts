import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {InputModule} from "../../sharedComponents/input/input.module";
import {SearchInputModule} from "../../sharedComponents/search-input/search-input.module";
import {LoadingModule} from "../../utils/loading/loading.module";
import {FosSelectionComponent} from './fos-selection.component';

@NgModule({
	imports: [
		CommonModule, ReactiveFormsModule, LoadingModule, InputModule, SearchInputModule
	],
	declarations: [
		FosSelectionComponent
	],
	providers: [
		
	],
	exports: [
		FosSelectionComponent
	]
})
export class FosSelectionModule {

}