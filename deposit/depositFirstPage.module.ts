/* Common Component of deposit for both research data & ppublications*/

import { NgModule }            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';
import { RouterModule } from '@angular/router';

import {HelperModule} from '../utils/helper/helper.module';
import {Schema2jsonldModule} from '../sharedComponents/schema2jsonld/schema2jsonld.module';
import { SEOServiceModule } from '../sharedComponents/SEO/SEOService.module';
import {DepositFirstPageComponent} from "./depositFirstPage.component";
import {BreadcrumbsModule} from "../utils/breadcrumbs/breadcrumbs.module";
import {SearchInputModule} from "../sharedComponents/search-input/search-input.module";
import {FullScreenModalModule} from '../utils/modal/full-screen-modal/full-screen-modal.module';
import {IsPageEnabledModule} from "../utils/isPageEnabled/isPageEnabled.module";

@NgModule({
  imports: [
    CommonModule, FormsModule,
    RouterModule,
    HelperModule,
    Schema2jsonldModule, SEOServiceModule, BreadcrumbsModule, SearchInputModule,
    FullScreenModalModule, IsPageEnabledModule
  ],
  declarations: [
    DepositFirstPageComponent
  ],
  exports: [
    DepositFirstPageComponent
  ],
  providers:    []
})
export class DepositFirstPageModule { }
