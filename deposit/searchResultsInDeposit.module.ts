import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {ErrorMessagesModule} from '../utils/errorMessages.module';
import {SearchResultsInDepositComponent} from './searchResultsInDeposit.component';
import {ApprovedByCommunityModule} from '../connect/approvedByCommunity/approved.module';
import {ResultPreviewModule} from "../utils/result-preview/result-preview.module";
import {IsPageEnabledModule} from "../utils/isPageEnabled/isPageEnabled.module";

@NgModule({
  imports: [
    CommonModule, FormsModule,
    RouterModule, ErrorMessagesModule,
    ApprovedByCommunityModule, ResultPreviewModule, IsPageEnabledModule
  ],
  declarations: [
    SearchResultsInDepositComponent
  ],
  providers:[
  ],
  exports: [
    SearchResultsInDepositComponent
  ]
})
export class SearchResultsInDepositModule { }
