import {Component, Input, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {SearchDataprovidersService} from '../services/searchDataproviders.service';
import {ErrorCodes} from '../utils/properties/errorCodes';
import {ErrorMessagesComponent}    from '../utils/errorMessages.component';
import {OpenaireEntities, SearchFields} from '../utils/properties/searchFields';

import {SearchCustomFilter, SearchUtilsClass} from '../searchPages/searchUtils/searchUtils.class';
import {EnvProperties} from '../utils/properties/env-properties';
import {ZenodoInformationClass} from "./utils/zenodoInformation.class";
import {RouterHelper} from "../utils/routerHelper.class";
import {Breadcrumb} from "../utils/breadcrumbs/breadcrumbs.component";
import {properties} from "../../../environments/environment";
import {SearchForm} from "../searchPages/searchUtils/newSearchPage.component";

@Component({
  selector: 'deposit-search-dataproviders',
  template: `
  <div [ngClass]="searchForm.class" [class.uk-light]="searchForm.dark" class="uk-margin-medium-bottom">
    <div class="uk-section uk-padding-remove-bottom uk-padding-remove-top">
      <div class="uk-padding-small">
        <div class="uk-container uk-container-large uk-margin-small-top uk-margin-small-bottom" [class.uk-light]="searchForm.dark">
          <breadcrumbs [breadcrumbs]="breadcrumbs"></breadcrumbs>
        </div>
      </div>
    </div>
    <div class="">
      <div class="uk-container uk-container-large">
        <div class="uk-margin-top uk-flex uk-flex-center">
          <div class="uk-width-large" [class.dark]="searchForm.dark">
            <div search-input [(value)]="keyword" placeholder="Search by name, description, subject..." (searchEmitter)="keywordChanged()" 
                 [searchInputClass]="'inner background'"></div>
						<div class="uk-margin-top uk-text-center uk-margin-medium-bottom">
              <ng-container *ngIf="zenodoInformation.hasSuggestedRepositories">
                <span  page-enabled [pageRoute]="'/participate/deposit/suggested'">
                  <a   class="uk-display-inline-block uk-text-uppercase uk-button uk-button-text" routerLink="/participate/deposit/suggested">
                    <span class="uk-flex uk-flex-middle">
                      <span>Suggested repositories</span>
                    </span>
                  </a>
                  </span>
              </ng-container>
							<a  *ngIf="zenodoInformation.url" [href]="zenodoInformation.url" target="_blank" class="uk-display-inline-block uk-text-uppercase uk-button uk-button-text">
								<span class="uk-flex uk-flex-middle">
									<span>Zenodo Repository</span><span class="custom-external custom-icon space"></span>
								</span>
							</a>
              <ng-container *ngIf="!zenodoInformation.url">
                <span page-enabled pageRoute="/participate/deposit/zenodo"> 
                <a   routerLink="/participate/deposit/zenodo" 
                   class=" uk-text-uppercase uk-button uk-button-text">
                 <span class="uk-flex uk-flex-middle">
									<span>Related zenodo communities</span>
								</span>

                </a>
                </span>                  
              </ng-container>
						</div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <search-dataproviders  [simpleView]="true" [searchForm]="{class: null, dark: false}" type="deposit" simpleSearchLink="/participate/deposit/search" [includeOnlyResultsAndFilter]="true" >
  </search-dataproviders>
    `
})
export class SearchDataprovidersToDepositComponent {
  private errorCodes: ErrorCodes;
  private errorMessages: ErrorMessagesComponent;
  @Input() searchForm: SearchForm = {class: null, dark: false};
  public results =[];
  public filters =[];
  public totalResults:number  = 0 ;
  public baseUrl:string;
  public searchUtils:SearchUtilsClass = new SearchUtilsClass();
  public sub: any; public subResults: any;
  public _location:Location;
  public searchFields:SearchFields = new SearchFields();
  public refineFields: string[] =  this.searchFields.DEPOSIT_DATASOURCE_REFINE_FIELDS;
  public fieldIdsMap= this.searchFields.DEPOSIT_DATASOURCE_FIELDS;
  public keywordFields = this.searchFields.DEPOSIT_DATASOURCE_KEYWORD_FIELDS;
  public csvParams: string;

  public disableForms: boolean = false;
  public loadPaging: boolean = true;
  public oldTotalResults: number = 0;
  pagingLimit = 0;

  properties:EnvProperties = properties;

  // @ViewChild (SearchPageComponent) searchPage : SearchPageComponent ;

  @Input() public communityId: string = null;
  @Input() public zenodoInformation: ZenodoInformationClass = new ZenodoInformationClass();

  depositLearnHowPage: string = null;
  public routerHelper:RouterHelper = new RouterHelper();
  breadcrumbs:Breadcrumb[] = [];
  parameters = {};
  keyword  = "";
  constructor (private route: ActivatedRoute, private _searchDataprovidersService: SearchDataprovidersService, private router: Router) {
    this.errorCodes = new ErrorCodes();
    this.errorMessages = new ErrorMessagesComponent();
    this.searchUtils.status = this.errorCodes.LOADING;
    this.searchUtils.page =1;
  }

  public ngOnInit() {
        this.depositLearnHowPage = this.properties.depositLearnHowPage;
        this.baseUrl = this.properties.depositSearchPage;
        this.pagingLimit = this.properties.pagingLimit;
        this.breadcrumbs.push({name: 'home', route: '/'}, {
          name: "Deposit",
          route: this.depositLearnHowPage
        }, {name: "Browse repositories", route: null});

     this.sub =  this.route.queryParams.subscribe(params => {
       this.parameters = Object.assign({}, params);
       this.keyword = params["fv0"]?params["fv0"]:'';
      });
			
			if (!this.zenodoInformation) {
				this.zenodoInformation = new ZenodoInformationClass();
			}
			if (!this.zenodoInformation.shareInZenodoUrl) {
				this.zenodoInformation.url = this.properties.zenodo;
			}
    // this.searchPage.refineFields = this.refineFields;
    // this.searchPage.fieldIdsMap = this.fieldIdsMap;
    // this.searchPage.keywordFields = this.keywordFields;
    // var firstLoad =true;
    //
    // this.subscriptions =  this.route.queryParams.subscribe(params => {
    //   this.loadPaging = true;
    //   if(params['page'] && this.searchUtils.page != params['page']) {
    //     this.loadPaging = false;
    //     this.oldTotalResults = this.searchUtils.totalResults;
    //   }
    //
    //   this.searchUtils.keyword = (params['keyword']?params['keyword']:'');
    //   var refine = true;
    //   if(this.searchUtils.page != ((params['page']=== undefined)?1:+params['page']) && this.filters && !firstLoad){
    //     refine = false;
    //
    //   }
    //   firstLoad = false;
    //   this.searchUtils.page = (params['page']=== undefined)?1:+params['page'];
    //   this.searchUtils.size = (params['size']=== undefined)?5:+params['size'];
    //   if(this.searchUtils.size != 5 && this.searchUtils.size != 10 && this.searchUtils.size != 20 && this.searchUtils.size != 50) {
    //     this.searchUtils.size = 5;
    //   }
    //   this.searchPage.usedBy = "deposit";
    //   var queryParameters = this.searchPage.getQueryParametersFromUrl(params);
    //   this._getResults(queryParameters, refine, this.searchUtils.page, this.searchUtils.size);
    // });
  }

  public ngOnDestroy() {
    if(this.sub){
      this.sub.unsubscribe();
    }
    if(this.subResults){
      this.subResults.unsubscribe();
    }
  }
  keywordChanged(){

    if ( this.keyword.length > 0) {
      this.parameters["fv0"] = this.keyword;
      this.parameters["f0"] = "q";
    }else{
      delete this.parameters['fv0'];
      delete this.parameters['f0'];
    }

    if(this.parameters["page"]){
      //GO to first page
      delete this.parameters['page'];
    }


    this.router.navigate([this.properties.depositSearchPage], {queryParams: this.parameters} );
  }
  /*public getResults(keyword:string,refine:boolean, page: number, size: number){
    var parameters = "";
    if(keyword.length > 0){
      //parameters = "q="+ keyword;

      if(this.keywordFields.length > 0) {
        parameters = "&fq=";
      }

      for(let i=0; i< this.keywordFields.length ; i++) {
        if(i > 0) {
          parameters += " or ";
        }
        let field = this.keywordFields[i];
        parameters += field.name+field.equalityOperator+StringUtils.URIEncode(this.searchUtils.keyword);
      }
    }
    this._getResults(parameters,refine,page, size);
  }
  private _getResults(parameters:string,refine:boolean, page: number, size: number){
    if(page > this.pagingLimit) {
      size=0;
    }
    if(page <= this.pagingLimit || this.searchUtils.status == this.errorCodes.LOADING) {
      this.csvParams = parameters;

      this.searchUtils.status = this.errorCodes.LOADING;

      this.disableForms = true;
      this.results = [];
      this.searchUtils.totalResults = 0;

      this.subResults = this._searchDataprovidersService.searchDataprovidersForDepositSearch(parameters,(refine)?this.searchPage.getRefineFieldsQuery():null, page, size, this.searchPage.getFields(),this.properties, "deposit").subscribe(
        data => {
          this.searchUtils.totalResults = data[0];
          this.results = data[1];
          if(refine){
            this.filters = data[2];
          }
          this.searchPage.checkSelectedFilters(this.filters);
          this.searchPage.updateBaseUrlWithParameters(this.filters);
          this.searchUtils.status = this.errorCodes.DONE;
          if(this.searchUtils.totalResults == 0 ){
            this.searchUtils.status = this.errorCodes.NONE;
          }
          this.disableForms = false;

          if(this.searchUtils.status == this.errorCodes.DONE) {
            // Page out of limit!!!
            let totalPages:any = this.searchUtils.totalResults/(this.searchUtils.size);
            if(!(Number.isInteger(totalPages))) {
              totalPages = (parseInt(totalPages, 10) + 1);
            }
            if(totalPages < page) {
              this.searchUtils.totalResults = 0;
              this.searchUtils.status = this.errorCodes.OUT_OF_BOUND;
            }
          }
        },
        err => {
          this.searchUtils.status = this.errorMessages.getErrorCode(err.status);

          this.disableForms = false;
        }
      );
    }
  }
*/

  private handleError(message: string, error) {
    console.error(OpenaireEntities.DATASOURCES+" simple Search Page: "+message, error);
  }
}
