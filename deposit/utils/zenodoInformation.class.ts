export class ZenodoInformationClass{
  name: string = "";
  url:  string = "";
  shareInZenodoUrl: string = "";
  hasSuggestedRepositories:boolean = false;
}
