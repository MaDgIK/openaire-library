import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';
import { RouterModule } from '@angular/router';

import{SearchDataprovidersToDepositComponent} from './searchDataprovidersToDeposit.component';

import {SearchResultsModule } from '../searchPages/searchUtils/searchResults.module';

import {DataProvidersServiceModule} from '../services/dataProvidersService.module';
import {SearchFormModule} from  '../searchPages/searchUtils/searchForm.module';
import {SearchDataProvidersModule} from "../searchPages/searchDataProviders.module";
import {BreadcrumbsModule} from "../utils/breadcrumbs/breadcrumbs.module";
import {SearchInputModule} from "../sharedComponents/search-input/search-input.module";
import {IsPageEnabledModule} from "../utils/isPageEnabled/isPageEnabled.module";

@NgModule({
  imports: [
    CommonModule, FormsModule,
    RouterModule,
    DataProvidersServiceModule,
    SearchFormModule, SearchResultsModule, SearchDataProvidersModule, BreadcrumbsModule, SearchInputModule, IsPageEnabledModule

  ],
  declarations: [
    SearchDataprovidersToDepositComponent
  ],
  providers:[],
  exports: [
    SearchDataprovidersToDepositComponent
  ]
})
export class SearchDataprovidersToDepositModule { }
