import {Component, Input, ViewChild}           from '@angular/core';
import {ZenodoInformationClass} from './utils/zenodoInformation.class';
import {EnvProperties} from "../utils/properties/env-properties";
import {ActivatedRoute, Router} from "@angular/router";
import {PiwikService} from "../utils/piwik/piwik.service";
import {HelperService} from "../utils/helper/helper.service";
import {RouterHelper} from "../utils/routerHelper.class";
import {SEOService} from "../sharedComponents/SEO/SEO.service";
import {Meta, Title} from "@angular/platform-browser";
import {Breadcrumb} from "../utils/breadcrumbs/breadcrumbs.component";
import {properties} from "../../../environments/environment";
import {Subscriber} from "rxjs";
import {FullScreenModalComponent} from '../utils/modal/full-screen-modal/full-screen-modal.component';

@Component({
  selector: 'deposit-first-page',
  template: `
		<div>
			<div class="uk-container uk-container-large uk-section uk-section-small uk-padding-remove-bottom">
				<div class="uk-padding-small uk-padding-remove-horizontal">
					<breadcrumbs [breadcrumbs]="breadcrumbs"></breadcrumbs>
				</div>
			</div>
			<div class="uk-container uk-container-large uk-section" uk-scrollspy="target: [uk-scrollspy-class]; cls: uk-animation-slide-bottom-medium; delay: 200">
				<div class="uk-grid uk-grid-large uk-grid-stack uk-padding-small" uk-grid>
					<div class="uk-width-3-5@m uk-width-1-1@s uk-flex uk-flex-column uk-flex-center">
						<div style="max-width: 600px;">
							<h1 class="uk-h2" uk-scrollspy-class>Find a repository to deposit or publish your research in Open Access.</h1>
							<div class="uk-margin-medium-top" uk-scrollspy-class>
								<a class="uk-button uk-button-primary uk-text-uppercase" (click)="openFsModal()">Find Repository</a>
							</div>
						</div>
					</div>
					<div class="uk-width-2-5@m uk-width-1-1@s uk-text-center" uk-scrollspy-class>
						<img [src]="assetsPath + '/deposit/deposit-hero-envelop.svg'" loading="lazy" alt="deposit hero image">
					</div>
				</div>
			</div>
			<div class="uk-container uk-container-large uk-section">
				<div class="uk-width-2-3@m uk-margin-auto">
					<div class="uk-grid uk-grid-large uk-grid-stack" uk-grid>
						<div class="uk-width-1-3@m">
							<img [src]="assetsPath + '/deposit/deposit-step-numbers.svg'" loading="lazy" alt="deposit steps/numbers image">
						</div>
						<div class="uk-width-2-3@m">
							<div class="uk-margin-medium-bottom">
								<div class="uk-h6">
									1. Find the appropriate repository or journal
								</div>
								<div>
									Find the appropriate repository to deposit your research products of any type (publication, data, software, other) or to include in your data management plan. Search and browse for OpenAIRE compliant repositories registered in OpenDOAR, re3data and FAIRsharing.
									Looking for Open Access journals? Find those that suit your needs among the journals registered in the Directory of Open Access Journals (DOAJ).
									To know more, read the OpenAIRE Open Access primer (<a target="_blank" href="https://www.openaire.eu/oa-basics">https://www.openaire.eu/oa-basics</a>)
								</div>
							</div>
							<div class="uk-margin-medium-bottom">
								<div class="uk-h6">
									2. Deposit
								</div>
								<div>
								Find the repository to deposit your research or use the Zenodo<span *ngIf="communityId && communityId != 'openaire'"> communities suggested by the community curators</span><span *ngIf="!communityId || communityId == 'openaire'"> repository</span>.
								</div>
							</div>
							<div class="uk-margin-medium-bottom">
								<div class="uk-h6">
									3. Publish
								</div>
								<div>
									Find the Open Access journals that suit your needs among those registered in the Directory of Open Access Journals (DOAJ).
								</div>
							</div>
						</div>
					</div>
					<div class="uk-section uk-section-large uk-margin-large-top">
						<!-- RESTORE HELP TEXT - EXPLORE -->
						<!-- <helper *ngIf="pageContents && pageContents['bottom'] && pageContents['bottom'].length > 0" [texts]="pageContents['bottom']"></helper> -->
						<div>
							<div class="uk-h6 uk-margin-medium-bottom">
								How to comply with funder Open Access policies
							</div>
							<div>
								<ul class="uk-list uk-list-disc">
									<li class="uk-margin-bottom">
										Read the <a href="https://www.openaire.eu/how-to-comply-to-h2020-mandates-for-publications" target="_blank"><span>OpenAIRE guide to learn how to comply with EC H2020 Open Access policy on publications</span><span class="custom-external custom-icon space"></span></a>
									</li>
									<li class="uk-margin-bottom">
									Read the <a href="https://www.openaire.eu/how-to-comply-to-h2020-mandates-for-data" target="_blank"><span>OpenAIRE guide to learn how to comply with EC H2020 Open Access policy on research data</span><span class="custom-external custom-icon space"></span></a>
									</li>
									<li class="uk-margin-bottom">
										If you want to know about National Open Access policies, please check them out <a href="https://www.openaire.eu/frontpage/country-pages" target="_blank"><span>here</span><span class="custom-external custom-icon space"></span></a>
									</li>
									<li class="uk-margin-bottom">
										All OpenAIRE guides can be found <a href="https://www.openaire.eu/guides" target="_blank"><span>here</span><span class="custom-external custom-icon space"></span></a> 
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<fs-modal #fsModal classTitle="">
			<div class="uk-height-1-1 uk-flex uk-flex-column uk-flex-middle">
				<h2 class="uk-margin-large-bottom">
					Find the appropriate repository.
				</h2>
				<div class="uk-grid  uk-flex uk-flex-middle" uk-height-match="target: .uk-card-body, .uk-tile-primary;" uk-grid>
					<div  page-enabled [pageRoute]="'/participate/deposit/search'" [communityId]="communityId">
						<div class="uk-card uk-card-default uk-text-center" style="width: 360px;">
							<div class="uk-card-body uk-text-small">
								<img src="assets/common-assets/common/Logo_Horizontal.png" alt="OpenAIRE logo" class="uk-width-small uk-margin-bottom">
								<div>
									Search and browse for <span class="uk-text-bold">OpenAIRE compliant repositories</span> registered in OpenDOAR, re3data and FAIRsharing.
								</div>
							</div>
							<div class="uk-card-footer uk-padding-small uk-tile-primary uk-flex uk-flex-center uk-light">
								<a class="uk-button uk-button-text" routerLink="/participate/deposit/search" (click)="closeFsModal()">
                  OpenAIRE compliant repositories
								</a>
							</div>
						</div>
					</div>
          <ng-container *ngIf="zenodoInformation.shareInZenodoUrl">
            <div  page-enabled [pageRoute]="'/participate/deposit/zenodo'" [communityId]="communityId">
              <div class="uk-card uk-card-default uk-text-center" style="width: 360px;">
                <div class="uk-card-body uk-text-small">
                  <img src="assets/common-assets/common/logo-zenodo.png" alt="Zenodo logo" class="uk-width-small uk-margin-bottom">
                  <div class="uk-width-2-3@m uk-margin-auto">
                    Deposit your research in <span class="uk-text-bold">Zenodo repository</span>
                  </div>
                </div>
                <div   class="uk-card-footer uk-padding-small uk-tile-primary uk-flex uk-flex-center uk-light">
                    <a class="uk-button uk-button-text" [routerLink]="zenodoInformation.shareInZenodoUrl" (click)="closeFsModal()">
                        Use related Zenodo communities
                    </a>
                </div>
                 
              </div>
            </div>
          </ng-container>
          <div *ngIf="!zenodoInformation.shareInZenodoUrl">
            <div class="uk-card uk-card-default uk-text-center" style="width: 360px;">
              <div class="uk-card-body uk-text-small">
                <img src="assets/common-assets/common/logo-zenodo.png" alt="Zenodo logo" class="uk-width-small uk-margin-bottom">
                <div class="uk-width-2-3@m uk-margin-auto">
                  Deposit your research in <span class="uk-text-bold">Zenodo repository</span>
                </div>
              </div>
              <div 
                   class="uk-card-footer uk-padding-small uk-tile-primary uk-flex uk-flex-center uk-light">
                <a class="uk-button uk-button-text custom-external" [href]="zenodoInformation.url" target="_blank">
                  Zenodo Repository
                </a>
              </div>
            </div>
          </div>
          <div  page-enabled [pageRoute]="'/participate/deposit/suggested'" *ngIf="zenodoInformation.hasSuggestedRepositories">
            <div class="uk-card uk-card-default uk-text-center" style="width: 360px;">
              <div class="uk-card-body uk-text-small">
                <img src="assets/connect-assets/deposit/suggested-repositories.svg" alt="Suggested Repositories" class="uk-width-small uk-margin-bottom" style="height:55px">
                <div class="uk-margin-auto">
                  Select a repository <span class="uk-text-bold">suggested</span> by the community curators.
                </div>
              </div>

              <div class="uk-card-footer uk-padding-small uk-tile-primary uk-flex uk-flex-center uk-light">
                <a class="uk-button uk-button-text" routerLink="/participate/deposit/suggested" (click)="closeFsModal()">
                  Suggested repositories
                </a>
              </div>
            </div>
          </div>
				</div>
			</div>
		</fs-modal>
  `,
})

export class DepositFirstPageComponent {
  public url: string = null;
  public title: string = "Deposit or publish your research in Open Access";

  @Input() public zenodoInformation: ZenodoInformationClass = new ZenodoInformationClass();
	@Input() assetsPath: string = 'assets/common-assets/common';

  @Input() communityId = null;

  public pageContents = null;
  public divContents = null;

  public keyword: string;
  public depositRoute: string;
  public searchPlaceHolder = "Search by title, country, organization, subject, type...";
  properties:EnvProperties = properties;
  public routerHelper:RouterHelper = new RouterHelper();
  @Input() showBreadcrumb:boolean = false;
  breadcrumbs:Breadcrumb[] = [];
  subscriptions = [];
	
	@ViewChild('fsModal', { static: true }) fsModal: FullScreenModalComponent;

  constructor (private  route: ActivatedRoute, private _piwikService:PiwikService,
							private helper: HelperService,
							private _router: Router,
							private _meta: Meta, private _title: Title,
							private seoService: SEOService) {
  }


  ngOnInit() {
        this.url = this.properties.domain+this.properties.baseLink + this._router.url;
        this.seoService.createLinkForCanonicalURL(this.url, false);

        var description = "Find the appropriate repository or journal or use Zenodo repository to deposit your research and publish in  the Open Access journals that suit your needs.";
        this.updateTitle(this.title);
        this.updateDescription(description);
        this.updateUrl(this.url);

        this.depositRoute = this.properties.depositSearchPage;
        //this.getDivContents();
        this.getPageContents();
        if (!this.zenodoInformation) {
          this.zenodoInformation = new ZenodoInformationClass();
        }
        if (!this.zenodoInformation.shareInZenodoUrl) {
          this.zenodoInformation.url = this.properties.zenodo;
        }
        if (!this.zenodoInformation.name) {
          this.zenodoInformation.name = "Zenodo";
        }
        this.subscriptions.push(this._piwikService.trackView(this.properties, this.title).subscribe());
        this.breadcrumbs.push({name: 'home', route: '/'}, {name: "Deposit", route: null});
  }

  public getPageContents() {
    this.subscriptions.push(this.helper.getPageHelpContents(this.properties, this.communityId, this._router.url).subscribe(contents => {
      this.pageContents = contents;
    }));
  }

  public getDivContents() {
    this.subscriptions.push(this.helper.getDivHelpContents(this.properties, this.communityId, this._router.url).subscribe(contents => {
      this.divContents = contents;
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscriber) {
        subscription.unsubscribe();
      }
    });
  }

  private updateDescription(description:string) {
    this._meta.updateTag({content:description},"name='description'");
    this._meta.updateTag({content:description},"property='og:description'");
  }
  private updateTitle(title:string) {
    var _prefix ="";
    if(!this.communityId) {
      _prefix ="OpenAIRE | ";
    }
    var _title = _prefix + ((title.length> 50 ) ?title.substring(0,50):title);
    this._title.setTitle(_title);
    this._meta.updateTag({content:_title},"property='og:title'");
  }
  private updateUrl(url:string) {
    this._meta.updateTag({content:url},"property='og:url'");
  }
	
	public openFsModal() {
		this.fsModal.open();
	}

	public closeFsModal() {
		this.fsModal.cancel();
	}
}
