import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { RouterModule } from '@angular/router';

import { ApprovedByCommunityComponent } from './approved.component';
import { SearchDataprovidersServiceModule } from '../contentProviders/searchDataprovidersService.module';

@NgModule({
  imports: [
    CommonModule,   RouterModule, SearchDataprovidersServiceModule
  ],
  declarations: [
    ApprovedByCommunityComponent
  ],
  providers:[],
  exports: [
    ApprovedByCommunityComponent
  ]
})
export class ApprovedByCommunityModule { }
