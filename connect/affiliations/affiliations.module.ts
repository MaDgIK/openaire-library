import {NgModule} from '@angular/core';

import {PreviousRouteRecorder} from '../../utils/piwik/previousRouteRecorder.guard';
import {IsRouteEnabled} from '../../error/isRouteEnabled.guard'
import {RouterModule} from '@angular/router';

import {AffiliationsComponent} from './affiliations.component';
import {CommonModule} from "@angular/common";
import {UrlPrefixModule} from "../../utils/pipes/url-prefix.module";
import {AffiliationService} from "./affiliation.service";

@NgModule({
  imports: [
    CommonModule, RouterModule, UrlPrefixModule
  ],
  declarations: [
    AffiliationsComponent
  ],
  providers:[PreviousRouteRecorder, IsRouteEnabled, AffiliationService],
  exports: [
    AffiliationsComponent
  ]
})


export class AffiliationsModule{}
