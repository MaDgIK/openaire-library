import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {throwError} from 'rxjs';
import { CustomOptions } from '../../services/servicesUtils/customOptions.class';

@Injectable()
export class MailPrefsService {
    constructor(private http: HttpClient ) {}

    getUserEmailPreferencesForCommunity (communityId:string, apiUrl:string):any {
      return this.http.get(apiUrl +"users/notification"+"?communityId="+communityId, CustomOptions.getAuthOptions())
    }

    getUserEmailPreferencesForOpenaire (apiUrl:string):any {
      return this.http.get(apiUrl +"users/notification", CustomOptions.getAuthOptions());
    }

    saveUserEmailPreferences (notification: any, apiUrl: string) {
      let body = JSON.stringify(notification);
      return this.http.post(apiUrl +"users/notification/save", body,  CustomOptions.getAuthOptionsWithBody());
    }
}
