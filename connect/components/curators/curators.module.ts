import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }  from '@angular/forms';
import { RouterModule } from '@angular/router';

import {CuratorsComponent} from './curators.component';
import {CuratorService} from "../../curators/curator.service";
import {CuratorsRoutingModule} from "./curators-routing.module";
import {HelperModule} from "../../../utils/helper/helper.module";
import {Schema2jsonldModule} from "../../../sharedComponents/schema2jsonld/schema2jsonld.module";
import {SEOServiceModule} from "../../../sharedComponents/SEO/SEOService.module";
import {PiwikServiceModule} from "../../../utils/piwik/piwikService.module";
import {BreadcrumbsModule} from "../../../utils/breadcrumbs/breadcrumbs.module";
import {UrlPrefixModule} from "../../../utils/pipes/url-prefix.module";
import {LoadingModule} from '../../../utils/loading/loading.module';
import {FullScreenModalModule} from '../../../utils/modal/full-screen-modal/full-screen-modal.module';
import {AffiliationsModule} from "../../affiliations/affiliations.module";

@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule,
    CuratorsRoutingModule, AffiliationsModule, HelperModule,
    Schema2jsonldModule, SEOServiceModule, PiwikServiceModule,
    BreadcrumbsModule, UrlPrefixModule, LoadingModule,
		FullScreenModalModule
  ],
  declarations: [
    CuratorsComponent
  ],
  providers: [CuratorService],
  exports: [
    CuratorsComponent
  ]
})
export class CuratorsModule {}
