import { NgModule}      from '@angular/core';
import { RouterModule } from '@angular/router';
import {CuratorsComponent} from "./curators.component";
import {IsRouteEnabled} from "../../../error/isRouteEnabled.guard";
import {PreviousRouteRecorder} from "../../../utils/piwik/previousRouteRecorder.guard";

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '', component: CuratorsComponent, canActivate: [IsRouteEnabled], canDeactivate: [PreviousRouteRecorder]}
    ])
  ]
})
export class CuratorsRoutingModule {
}
