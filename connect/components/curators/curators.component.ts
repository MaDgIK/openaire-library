import {Component, Input, ViewChild} from '@angular/core';
import {EnvProperties} from '../../../utils/properties/env-properties';
import {CuratorService} from "../../curators/curator.service";
import {Curator} from "../../../utils/entities/CuratorInfo";
import {ActivatedRoute, Router} from "@angular/router";
import {CommunityService} from "../../community/community.service";
import {HelperService} from "../../../utils/helper/helper.service";
import {Meta, Title} from "@angular/platform-browser";
import {SEOService} from "../../../sharedComponents/SEO/SEO.service";
import {PiwikService} from "../../../utils/piwik/piwik.service";
import {Breadcrumb} from "../../../utils/breadcrumbs/breadcrumbs.component";
import {Subscription} from "rxjs";
import {properties} from "../../../../../environments/environment";
import {UserRegistryService} from "../../../services/user-registry.service";
import {FullScreenModalComponent} from '../../../utils/modal/full-screen-modal/full-screen-modal.component';
import {CommunityInfo} from "../../community/communityInfo";

@Component({
  selector: 'curators',
  templateUrl: './curators.component.html'
  
})
export class CuratorsComponent {
  @Input() longView = true;
  community: CommunityInfo;
  public downloadUrl = null;
  public showLoading = true;
  
  public curators: Curator[] = [];
  
  public curatorsLimit: number = 5;
  public numberOfCurators: number = 5;
  
  public showMore = [];
  public maxCharacters = 450;
	public viewingMore: boolean = false;
  public curatorInModal;

  public properties: EnvProperties = properties;
  public pageContents = null;
  public divContents = null;
  
  public url: string = null;
  public pageTitle: string = "Curators";
  
  public breadcrumbs: Breadcrumb[] = [{name: 'Home', route: '/'}, {name: 'About - Curators'}];
  
  subs: Subscription[] = [];

	@ViewChild('fsModal', { static: true }) fsModal: FullScreenModalComponent;
  
  constructor(private route: ActivatedRoute,
              private curatorsService: CuratorService,
              private communityService: CommunityService,
              private userRegistryService: UserRegistryService,
              private _router: Router,
              private helper: HelperService,
              private _meta: Meta,
              private _title: Title,
              private seoService: SEOService,
              private _piwikService: PiwikService) {
  }
  
  ngOnInit() {
    this.showLoading = true;
    this.downloadUrl = this.properties.utilsService + '/download/';
    //if (properties.environment !== 'development') {
    if (!this.longView) {
      this.subs.push(this.communityService.getCommunityAsObservable().subscribe(community => {
        if (community) {
          this.community = community;
          this.getCurators();
        }
      }));
    } else {
      this.subs.push(this.communityService.getCommunityAsObservable().subscribe(community => {
        if (community) {
          this.community = community;
          this.subs.push(this._piwikService.trackView(this.properties, this.pageTitle).subscribe());
          this.url = this.properties.domain + this._router.url;
          this.seoService.createLinkForCanonicalURL(this.url);
          this.updateUrl(this.url);
          this.updateTitle(this.pageTitle);
          this.updateDescription("OpenAIRE - Connect, Community Gateway, research community");
          //this.getDivContents();
          this.getPageContents();
          this.getCurators();
        }
      }));
    }
  }
  
  ngOnDestroy() {
    for (let sub of this.subs) {
      sub.unsubscribe();
    }
  }
  
  private getCurators() {
    this.subs.push(this.curatorsService.getCurators(this.community.communityId).subscribe(curators => {
      this.curators = curators;
			this.showLoading = false;
    }, error => {
			console.error(error);
			this.curators = [];
			this.showLoading = false;
		}));
  }
  
  private getPageContents() {
    this.subs.push(this.helper.getPageHelpContents(this.properties, this.community.communityId, this._router.url).subscribe(contents => {
      this.pageContents = contents;
    }));
  }
  
  private getDivContents() {
    this.subs.push(this.helper.getDivHelpContents(this.properties, this.community.communityId, this._router.url).subscribe(contents => {
      this.divContents = contents;
    }));
  }
  
  format(name: string) {
    if (name) {
      return (((name).length > this.maxCharacters) ? (name.substring(0, (this.maxCharacters - ('...').length)) + '...') : name);
    } else {
      return null;
    }
  }

	public viewMore() {
		this.viewingMore = !this.viewingMore;
	}

	public openFsModal(curator) {
		this.curatorInModal = curator;
    this.fsModal.title = this.community.shortTitle + ' Curator';
		this.fsModal.open();
	}
  
  private updateDescription(description: string) {
    this._meta.updateTag({content: description}, "name='description'");
    this._meta.updateTag({content: description}, "property='og:description'");
  }
  
  private updateTitle(title: string) {
    var _title = ((title.length > 50) ? title.substring(0, 50) : title);
    this._title.setTitle(_title);
    this._meta.updateTag({content: _title}, "property='og:title'");
  }
  
  private updateUrl(url: string) {
    this._meta.updateTag({content: url}, "property='og:url'");
  }
}
