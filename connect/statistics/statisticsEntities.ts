export const availableCharts = [
    'timeline',
    'graph',
    'projectColumn',
    'projectPie',
    'projectTable'
];

export const availableNumbers = [
    'total',
    'project',
    'open',
    'closed',
    'restricted',
    'embargo',
];

export const availableEntities = [
    'publication',
    'dataset',
    'software',
    'orp',
];

export const availableEntitiesMap = new Map([
    ['publication', 'publications'],
    ['dataset', 'research data'],
    ['software', 'software'],
    ['orp', 'other research products'],
]);

export class DisplayStatsOptions {
    showInMonitor: boolean;
    showInDashboard: boolean;
}

export class StatisticsMap {
    map: Map<string, DisplayStatsOptions>;
}

export class EntityStats {
    charts: StatisticsMap;
    numbers: StatisticsMap;
}

export class StatisticsDisplay {
    pid: string;
    isActive: boolean = true;
    entities: Map<string, EntityStats>;
    _id: string;
}

export class StatisticsNumbers {
    total: number;
    open_access: number;
    embargo: number;
    restricted: number;
    closed_access: number;
    projects: number;
}

export class StatisticsSummary {
    publications : StatisticsNumbers;
    datasets: StatisticsNumbers;
    software: StatisticsNumbers;
    total_projects: number;
    virtual_organizations: number;
}
