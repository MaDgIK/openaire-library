import {DomSanitizer} from "@angular/platform-browser";
import {StringUtils} from "../../utils/string-utils.class";
import {EnvProperties} from "../../utils/properties/env-properties";

export class CommunityCharts {

  results = {
    publication: {name: "Publications", type: "publication", chartId: "Pubs"},
    dataset: {name: "Research data", type: "dataset", chartId: "Data"},
    software: {name: "Software", type: "software", chartId: "Soft"},
    orp: {name: "Other research products", type: "other", chartId: "Other"}
  };
  types = ["publication", "dataset", "software", "orp"];


  constructor(private sanitizer: DomSanitizer) {

  }

  getChartsForCommunity(communityId: string, communityName: string, properties:EnvProperties) {
    if (properties.useNewStatistisTool) {
      return this.getNewToolCharts(communityId, communityName, properties.statisticsFrameNewAPIURL);
    }
    return this.getOldToolCharts(communityId,  properties.statisticsFrameAPIURL);
  }

  private getOldToolCharts(communityId: string, chartsFrameAPIURL: string) {
    let chartsInfoMap = {};
    for (let type of this.types) {
      chartsInfoMap[type + 'Timeline'] = {
        url: this.sanitizer.bypassSecurityTrustResourceUrl(chartsFrameAPIURL + 'chart.php?com=query&persistent=false&data=com=query&data={"query":"comTimeline' +
          this.results[type].chartId + '","comId":"' + communityId + '","table":"result","fields":[{"fld":"number","agg":"count","type":"column","yaxis":1,"c":false}],"xaxis":{"name":"year","agg":"avg"},"group":"","color":"","type":"chart","size":30,"sort":"xaxis",' +
          '"xStyle":{"r":"-","s":"-","l":"-","ft":"-","wt":"-"},"yaxisheaders":["' + this.results[type].name + '"],"fieldsheaders":["' + this.results[type].name + '"],"in":[{"f":0,"text":"Yearly"}],"filters":[{"name":"year","max":"2013","min":"2007","to":-1},{"name":"result_projects-project-funding_lvl0","values":["FP7"],"to":"-1"},'
          + '{"name":"type","values":["' + this.results[type].type + '"],"to":"-1"},{"name":"result_projects-project-funding_lvl2","values":["ERC"],"to":"-1"}],"having":[],"incfilters":[],"inchaving":[],"title":"","subtitle":"","xaxistitle":"Year"}&w=90%&h=80%'),
        title: "" + StringUtils.capitalize(this.results[type].name.toLowerCase()) + " per year"
      };

      chartsInfoMap[type + 'Graph'] = {
        url: this.sanitizer.bypassSecurityTrustResourceUrl(chartsFrameAPIURL + 'chart.php?com=query&data={"query":"comOA' + this.results[type].chartId + '","comId":"' + communityId +
          '","table":"result","fields":[{"fld":"number","agg":"count","type":"pie","yaxis":1,"c":false}],"xaxis":{"name":"bestlicense","agg":"avg"},"group":"","color":"","type":"chart","size":30,"sort":"xaxis","xStyle":{"r":"-","s":"-","l":"-","ft":"-","wt":"-"},"order":"d","yaxisheaders":[""],"fieldsheaders":[""],"in":[],"filters":[],"having":[],"incfilters":[],"inchaving":[],"title":"","subtitle":"","xaxistitle":"","nulls":true}&w=80%&h=80%'),
        title: "" +  StringUtils.capitalize(this.results[type].name.toLowerCase()) + " access rights"
      };
      chartsInfoMap[type + 'Projectcolumn'] = {
        url: this.sanitizer.bypassSecurityTrustResourceUrl(chartsFrameAPIURL + 'chart.php?com=query&data={"query":"comProject' + this.results[type].chartId +
          '","comId":"' + communityId + '","table":"result","fields":[{"fld":"number","agg":"count","type":"bar","yaxis":1,"c":false}],"xaxis":{"name":"year","agg":"avg"},"group":"","color":"","type":"chart","size":30,"sort":"xaxis","xStyle":{"r":"-","s":"-","l":"-","ft":"-","wt":"-"},"yaxisheaders":["' + this.results[type].name + '"],"fieldsheaders":["' + this.results[type].name + '"],"in":[],"filters":[{"name":"year","max":"2013","min":"2007","to":-1},{"name":"result_projects-project-funding_lvl0","values":["FP7"],"to":"-1"},{"name":"type","values":["' + this.results[type].type + '"],"to":"-1"},{"name":"result_projects-project-funding_lvl2","values":["ERC"],"to":"-1"}],"having":[],"incfilters":[],"inchaving":[],"title":"","subtitle":"","xaxistitle":"Project"}&w=70%&h=80%'),
        title: "" +  StringUtils.capitalize(this.results[type].name.toLowerCase()) + " per project",
      };
      chartsInfoMap[type + 'Projectpie'] = {
        url: this.sanitizer.bypassSecurityTrustResourceUrl(chartsFrameAPIURL + 'chart.php?com=query&data={"query":"comProject' + this.results[type].chartId +
          '","comId":"' + communityId + '","table":"result","fields":[{"fld":"number","agg":"count","type":"pie","yaxis":1,"c":false}],"xaxis":{"name":"year","agg":"avg"},"group":"","color":"","type":"chart","size":30,"sort":"xaxis","xStyle":{"r":"-","s":"-","l":"-","ft":"-","wt":"-"},"yaxisheaders":["' + this.results[type].name + '"],"fieldsheaders":["' + this.results[type].name + '"],"in":[],"filters":[{"name":"year","max":"2013","min":"2007","to":-1},{"name":"result_projects-project-funding_lvl0","values":["FP7"],"to":"-1"},{"name":"type","values":["' + this.results[type].type + '"],"to":"-1"},{"name":"result_projects-project-funding_lvl2","values":["ERC"],"to":"-1"}],"having":[],"incfilters":[],"inchaving":[],"title":"","subtitle":"","xaxistitle":"Project"}&w=85%&h=90%'),
        title: "" +  StringUtils.capitalize(this.results[type].name.toLowerCase()) + " per project",
      },
        chartsInfoMap[type + 'Projecttable'] = {
          url: this.sanitizer.bypassSecurityTrustResourceUrl(chartsFrameAPIURL + 'gtable.php?com=query&data={"query":"comProject' + this.results[type].chartId +
            '","comId":"' + communityId + '","table":"result","fields":[{"fld":"number","agg":"count","type":"bar","yaxis":1,"c":false}],"xaxis":{"name":"year","agg":"avg"},"group":"","color":"","type":"chart","size":30,"sort":"xaxis","xStyle":{"r":"-","s":"-","l":"-","ft":"-","wt":"-"},"yaxisheaders":["' + this.results[type].name + '"],"fieldsheaders":["' + this.results[type].name + '"],"in":[],"filters":[{"name":"year","max":"2013","min":"2007","to":-1},{"name":"result_projects-project-funding_lvl0","values":["FP7"],"to":"-1"},{"name":"type","values":["' + this.results[type].type + '"],"to":"-1"},{"name":"result_projects-project-funding_lvl2","values":["ERC"],"to":"-1"}],"having":[],"incfilters":[],"inchaving":[],"title":"","subtitle":"","xaxistitle":"Project"}&w=70%&h=90%'),
          title: " " + StringUtils.capitalize(this.results[type].name.toLowerCase()) + " per project",
        };

      if (communityId == "egi") {
        //TODO add VO --> available only for
      }

    }
    return chartsInfoMap;
  }

  private getNewToolCharts(communityId: string, communityName: string, chartsFrameAPIURL: string) {

    let chartsInfoMap = {};
    for (let type of this.types) {
      chartsInfoMap[type + 'Timeline'] = {
        url: this.sanitizer.bypassSecurityTrustResourceUrl(chartsFrameAPIURL + 'chart?json=' + StringUtils.URIEncode('{"library":"HighCharts","chartDescription":{"queries":[{"name":"' + this.results[type].name + '","type":"column","query":{"name":"rcd.timeline","parameters":["'+communityId+'","' + this.results[type].type + '"]}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":""},"subtitle":{},"yAxis":{"title":{"text":"' + this.results[type].name + '"}},"xAxis":{"title":{"text":"Year"}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":false},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":false},"credits":{"href":null,"enabled":true,"text":"Created by OpenAIRE via HighCharts"}}}')),
        title: StringUtils.capitalize(this.results[type].name.toLowerCase()) + " per year"
      };
      chartsInfoMap[type + 'Graph'] = {
        url: this.sanitizer.bypassSecurityTrustResourceUrl(chartsFrameAPIURL + 'chart?json=' + StringUtils.URIEncode('{"library":"HighCharts","chartDescription":{"queries":[{"name":"' + this.results[type].name + '","type":"pie","query":{"select":[{"field":"' + this.results[type].type + '","aggregate":"count"},{"field":"' + this.results[type].type + '.access mode","aggregate":null}],"filters":[{"groupFilters":[{"field":"' + this.results[type].type + '.concept.category.context.name","type":"=","values":["' + communityName + '"]}],"op":"AND"}],"entity":"' + this.results[type].type + '","profile":"OpenAIRE All-inclusive","limit":"30"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":""},"subtitle":{},"yAxis":{"title":{}},"xAxis":{"title":{}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":true,"align":"center","verticalAlign":"bottom","layout":"horizontal"},"credits":{"href":null,"enabled":true,"text":"Created by OpenAIRE via HighCharts"}}}')),
        title: "" +  StringUtils.capitalize(this.results[type].name.toLowerCase()) + " access right"
      };
      chartsInfoMap[type + 'Projectcolumn'] = {
        url: this.sanitizer.bypassSecurityTrustResourceUrl(chartsFrameAPIURL + 'chart?json=' + StringUtils.URIEncode('{"library":"HighCharts","chartDescription":{"queries":[{"name":"' + this.results[type].name + '","type":"bar","query":{"select":[{"field":"' + this.results[type].type + '","aggregate":"count"},{"field":"' + this.results[type].type + '.project.acronym","aggregate":null}],"filters":[{"groupFilters":[{"field":"' + this.results[type].type + '.concept.category.context.name","type":"=","values":["' + communityName + '"]}],"op":"AND"}],"entity":"' + this.results[type].type + '","profile":"OpenAIRE All-inclusive","limit":"30"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":""},"subtitle":{},"yAxis":{"title":{"text":"' + this.results[type].name + '"}},"xAxis":{"title":{}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":true,"align":"center","verticalAlign":"bottom","layout":"horizontal"},"credits":{"href":null,"enabled":true,"text":"Created by OpenAIRE via HighCharts"}}}')),
        title: "" + StringUtils.capitalize(this.results[type].name.toLowerCase())+ " per project",
      };
      chartsInfoMap[type + 'Projectpie'] = {
        url: this.sanitizer.bypassSecurityTrustResourceUrl(chartsFrameAPIURL + 'chart?json=' + StringUtils.URIEncode('{"library":"HighCharts","chartDescription":{"queries":[{"name":"' + this.results[type].name + '","type":"pie","query":{"select":[{"field":"' + this.results[type].type + '","aggregate":"count"},{"field":"' + this.results[type].type + '.project.acronym","aggregate":null}],"filters":[{"groupFilters":[{"field":"' + this.results[type].type + '.concept.category.context.name","type":"=","values":["' + communityName + '"]}],"op":"AND"}],"entity":"' + this.results[type].type + '","profile":"OpenAIRE All-inclusive","limit":"30"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{"text":""},"subtitle":{},"yAxis":{"title":{"text":"' + this.results[type].name + '"}},"xAxis":{"title":{}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":true,"align":"center","verticalAlign":"bottom","layout":"horizontal"},"credits":{"href":null,"enabled":true,"text":"Created by OpenAIRE via HighCharts"}}}')),
        title: "" +  StringUtils.capitalize(this.results[type].name.toLowerCase()) + " per project",
      },
        chartsInfoMap[type + 'Projecttable'] = {
          url: this.sanitizer.bypassSecurityTrustResourceUrl(chartsFrameAPIURL + 'table?json=' + StringUtils.URIEncode('{"library":"GoogleCharts","tableDescription":{"queriesInfo":[{"name":"' + this.results[type].name + '","type":"bar","query":{"select":[{"field":"' + this.results[type].type + '","aggregate":"count"},{"field":"' + this.results[type].type + '.project.acronym","aggregate":null}],"filters":[{"groupFilters":[{"field":"' + this.results[type].type + '.concept.category.context.name","type":"=","values":["' + communityName + '"]}],"op":"AND"}],"entity":"publication","profile":"OpenAIRE All-inclusive","limit":"30"}}],"columns":[],"options":{"hAxis":{},"vAxis":{},"chartArea":{},"series":[],"exporting":true,"pageSize":30,"isStacked":"false"}}}')),
          title: "" + StringUtils.capitalize(this.results[type].name.toLowerCase()) + " per project",
        };
        //TODO check again
      if (communityId == "egi") {
        chartsInfoMap[type + 'VOcolumn'] ={
            url: this.sanitizer.bypassSecurityTrustResourceUrl(chartsFrameAPIURL + 'chart?json=' + StringUtils.URIEncode('{"library":"HighCharts","chartDescription":{"queries":[{"name":"' + this.results[type].name + '","type":"bar","query":{"select":[{"field":"' + this.results[type].type + '","aggregate":"count"},{"field":"' + this.results[type].type + '.concept.name","aggregate":null}],"filters":[{"groupFilters":[{"field":"' + this.results[type].type + '.concept.category.context.name","type":"=","values":["' + communityName + '"]}],"op":"AND"},{"groupFilters":[{"field":"' + this.results[type].type + '.concept.category.name","type":"=","values":["EGI virtual organizations"]}],"op":"AND"}],"entity":"' + this.results[type].type + '","profile":"OpenAIRE All-inclusive","limit":"30"}}],"chart":{"backgroundColor":"#FFFFFFFF","borderColor":"#335cadff","borderRadius":0,"borderWidth":0,"plotBorderColor":"#ccccccff","plotBorderWidth":0},"title":{},"subtitle":{},"yAxis":{"title":{"text":"' + this.results[type].name + '"}},"xAxis":{"title":{"text":"Projects"}},"lang":{"noData":"No Data available for the Query"},"exporting":{"enabled":true},"plotOptions":{"series":{"dataLabels":{"enabled":false}}},"legend":{"enabled":true,"align":"center","verticalAlign":"bottom","layout":"vertical"},"credits":{"href":null,"enabled":true,"text":"Created by OpenAIRE via HighCharts"}}}')),
            title: "" +  StringUtils.capitalize(this.results[type].name.toLowerCase()) + " per EGI virtual organization",
          };
        chartsInfoMap[type + 'VOtable'] ={
          url: this.sanitizer.bypassSecurityTrustResourceUrl(chartsFrameAPIURL + 'table?json=' + StringUtils.URIEncode('{"library":"GoogleCharts","tableDescription":{"queriesInfo":[{"name":"Data","type":"bar","query":{"select":[{"field":"' + this.results[type].type + '","aggregate":"count"},{"field":"' + this.results[type].type + '.concept.name","aggregate":null}],"filters":[{"groupFilters":[{"field":"' + this.results[type].type + '.concept.category.context.name","type":"=","values":["EGI Federation"]}],"op":"AND"},{"groupFilters":[{"field":"' + this.results[type].type + '.concept.category.name","type":"=","values":["EGI virtual organizations"]}],"op":"AND"}],"entity":"publication","profile":"OpenAIRE All-inclusive","limit":"30"}}],"columns":[],"options":{"hAxis":{},"vAxis":{},"chartArea":{},"series":[],"exporting":true,"pageSize":30,"isStacked":"false"}}}')),
          title: "" +  StringUtils.capitalize(this.results[type].name.toLowerCase()) + " per EGI virtual organization",
        };

      }
    }
    return chartsInfoMap;
  }

}
