import {properties} from "../../../../environments/environment";

export class Layout {
  _id: string;
  portalPid: string;
  layoutOptions: CustomizationOptions;
  date;
  
  constructor(community, options: CustomizationOptions) {
    this.portalPid = community;
    this.layoutOptions = options;
  }
  
  public static getVariables(options: CustomizationOptions): {} | null {
    if (options) {
      let variables = {};
      if (options.identity) {
        variables['@global-primary-background'] = Layout.convertRGBAtoRGB(options.identity.mainColor);
        variables['@global-secondary-background'] = Layout.convertRGBAtoRGB(options.identity.secondaryColor);
      }
      if (options.backgrounds) {
        variables['@general-search-form-background'] = Layout.convertRGBAtoRGB(options.backgrounds.form.color);
        variables['@global-background'] = Layout.convertRGBAtoRGB(options.backgrounds.light.color);
        variables['@hero-background-image'] = (options.backgrounds.form.imageFile ? (this.getUrl(properties.utilsService + '/download/' + options.backgrounds.form.imageFile)) : 'none');
        variables['@hero-background-position'] = options.backgrounds.form.position;
        variables['@hero-fonts-mode'] = options.backgrounds.form.fontsDarkMode == true ?'dark':'light';
      }
      if (options.buttons) {
        //general
        variables['@button-border-width'] = options.buttons.lightBackground.borderWidth + "px";
        variables['@button-border-radius'] = options.buttons.lightBackground.borderRadius + "px";
        // default -> on dark background todo check again when we have sucj=h buttons
        variables['@button-default-background'] = Layout.convertRGBAtoRGB(options.buttons.darkBackground.backgroundColor);
        variables['@button-default-color'] = Layout.convertRGBAtoRGB(options.buttons.darkBackground.color);
        variables['@button-default-border'] = Layout.convertRGBAtoRGB(options.buttons.darkBackground.borderColor);
        variables['@button-default-hover-background'] = Layout.convertRGBAtoRGB(options.buttons.darkBackground.onHover.backgroundColor);
        variables['@button-default-hover-color'] = Layout.convertRGBAtoRGB(options.buttons.darkBackground.onHover.color);
        variables['@button-default-hover-border'] = Layout.convertRGBAtoRGB(options.buttons.darkBackground.onHover.borderColor);
        variables['@button-default-active-background'] = Layout.convertRGBAtoRGB(options.buttons.darkBackground.onHover.backgroundColor);
        variables['@button-default-active-color'] = Layout.convertRGBAtoRGB(options.buttons.darkBackground.onHover.color);
        variables['@button-default-active-border'] = Layout.convertRGBAtoRGB(options.buttons.darkBackground.onHover.borderColor);
        
        // primary   
        variables['@button-primary-background'] = Layout.convertRGBAtoRGB(options.buttons.lightBackground.backgroundColor);
        variables['@button-primary-color'] = Layout.convertRGBAtoRGB(options.buttons.lightBackground.color);
        variables['@button-primary-border'] = Layout.convertRGBAtoRGB(options.buttons.lightBackground.borderColor);
        variables['@button-primary-hover-background'] = Layout.convertRGBAtoRGB(options.buttons.lightBackground.onHover.backgroundColor);
        variables['@button-primary-hover-color'] = Layout.convertRGBAtoRGB(options.buttons.lightBackground.onHover.color);
        variables['@button-primary-hover-border'] = Layout.convertRGBAtoRGB(options.buttons.lightBackground.onHover.borderColor);
        variables['@button-primary-active-background'] = Layout.convertRGBAtoRGB(options.buttons.lightBackground.onHover.backgroundColor);
        variables['@button-primary-active-color'] = Layout.convertRGBAtoRGB(options.buttons.lightBackground.onHover.color);
        variables['@button-primary-active-border'] = Layout.convertRGBAtoRGB(options.buttons.lightBackground.onHover.borderColor);
        
        // secondary
        variables['@button-secondary-background'] = Layout.convertRGBAtoRGB(options.buttons.lightBackground.color);
        variables['@button-secondary-color'] = Layout.convertRGBAtoRGB(options.buttons.lightBackground.backgroundColor);
        variables['@button-secondary-border'] = Layout.convertRGBAtoRGB(options.buttons.lightBackground.backgroundColor);
        variables['@button-secondary-hover-background'] = Layout.convertRGBAtoRGB(options.buttons.lightBackground.backgroundColor);
        variables['@button-secondary-hover-color'] = Layout.convertRGBAtoRGB(options.buttons.lightBackground.color);
        variables['@button-secondary-hover-border'] = Layout.convertRGBAtoRGB(options.buttons.lightBackground.borderColor);
        variables['@button-secondary-active-background'] = Layout.convertRGBAtoRGB(options.buttons.lightBackground.backgroundColor);
        variables['@button-secondary-active-color'] = Layout.convertRGBAtoRGB(options.buttons.lightBackground.color);
        variables['@button-secondary-active-border'] = Layout.convertRGBAtoRGB(options.buttons.lightBackground.borderColor);
        
      }
      return variables;
    }
    return null;
  }
  
  public static getUrl(url) {
    return 'url("' + url + '")';
  }
  
  public static convertRGBAtoRGB(color: string): string {
    if(color.includes('rgba')) {
      const regexPattern = /rgba\((\d+),\s*(\d+),\s*(\d+),\s*([\d.]+)\)/;
      const matches = color.match(regexPattern);
      const [, r, g, b, a] = matches;
      let R = parseInt(r)*parseFloat(a) + (1 - parseFloat(a))*255;
      let G = parseInt(g)*parseFloat(a) + (1 - parseFloat(a))*255;
      let B = parseInt(b)*parseFloat(a) + (1 - parseFloat(a))*255;
      return 'rgb(' + R + ',' + G + ',' + B + ',' + ')';
    } else {
      return color;
    }
  }
}

export class CustomizationOptions {
  identity: {
    mainColor: string;
    secondaryColor: string;
    customCss: string;
  };
  identityIsCustom: boolean;
  backgroundsIsCustom: boolean;
  buttonsIsCustom: boolean;
  backgrounds: {
    dark: {
      color: string; //background
    }
    light: {
      color: string; //background
    },
    form: {
      color: string; //background
      imageUrl: string;
      imageFile: string;
      position: string;
      fontsDarkMode:boolean;
    }
  };
  buttons: {
    darkBackground: ButtonsCustomization;
    lightBackground: ButtonsCustomization;
  };

  constructor(mainColor: string = null, secondaryColor: string = null) {
    this.identity = {
      mainColor: mainColor ? mainColor : CustomizationOptions.getIdentity().mainColor,
      secondaryColor: secondaryColor ? secondaryColor : CustomizationOptions.getIdentity().secondaryColor,
      customCss: ""
    };
    this.identityIsCustom = false;
    this.backgroundsIsCustom = false;
    this.buttonsIsCustom = false;
    this.backgrounds = {
      dark: {
        color: this.identity.mainColor,
      },
      light: {
        color: "#f9f9f9" //CustomizationOptions.getRGBA(this.identity.mainColor,0.05),
      },
      form: {
        color: CustomizationOptions.getRGBA(this.identity.mainColor, 0.15),
        imageUrl: null,
        imageFile: null,
        position: null,
        fontsDarkMode: true
      }
    };
    
    
    this.buttons = {
      darkBackground: {
        isDefault: true,
        backgroundColor: "#ffffff",
        color: "#000000",
        borderStyle: "solid",
        borderColor: "#ffffff",
        borderWidth: 1,
        borderRadius: 500,
        onHover: {
          backgroundColor: "#eeeeee",
          color: "#000000",
          borderColor: "#eeeeee",
        }
      },
      lightBackground: {
        isDefault: true,
        backgroundColor: this.identity.mainColor,
        color: '#ffffff',
        borderStyle: "solid",
        borderColor: this.identity.mainColor,
        borderWidth: 1,
        borderRadius: 500,
        onHover: {
          backgroundColor: this.identity.secondaryColor,
          color: '#ffffff',
          borderColor: this.identity.secondaryColor,
        }
      }
      
    };
  }
  
  public static checkForObsoleteVersion(current: CustomizationOptions, communityId: string) {
    if(communityId === 'connect') {
      return null;
    }
    let defaultCO = new CustomizationOptions(CustomizationOptions.getIdentity(communityId).mainColor, CustomizationOptions.getIdentity(communityId).secondaryColor);
    let updated = Object.assign({}, defaultCO);
    if (!current) {
      current = Object.assign({}, defaultCO);
    } else {
      if (current.identity && current.identity.mainColor && current.identity.secondaryColor) {
        updated = new CustomizationOptions(current.identity.mainColor, current.identity.secondaryColor);
      }
      if(current.identity && !current.identity.customCss){
        current.identity.customCss = defaultCO.identity.customCss;
      }
      if (!current.backgrounds) {
        current.backgrounds = Object.assign({}, updated.backgrounds);
      }
      if (!current.backgrounds.dark) {
        current.backgrounds.dark = Object.assign({}, updated.backgrounds.dark);
      }
      if (!current.backgrounds.light) {
        current.backgrounds.light = Object.assign({}, updated.backgrounds.light);
      }
      if (!current.backgrounds.form) {
        current.backgrounds.form = Object.assign({}, updated.backgrounds.form);
      }
      if (!current.backgrounds.form.position) {
        current.backgrounds.form.position = "center bottom"
      }
      if (current.backgrounds.form.fontsDarkMode == undefined) {
        current.backgrounds.form.fontsDarkMode = true;
      }
      if (!current.buttons) {
        current.buttons = Object.assign({}, updated.buttons);
      }
      if (!current.buttons.darkBackground) {
        current.buttons.darkBackground = Object.assign({}, updated.buttons.darkBackground);
      }
      if (!current.buttons.lightBackground) {
        current.buttons.lightBackground = Object.assign({}, updated.buttons.lightBackground);
      }
      if (!current.hasOwnProperty('identityIsCustom')) {
        current.identityIsCustom = (JSON.stringify(current.identity) != JSON.stringify(defaultCO.identity));
      }
      if (!current.hasOwnProperty('backgroundsAndButtonsIsCustom') || (!current.hasOwnProperty('backgroundsIsCustom') || (!current.hasOwnProperty('buttonsIsCustom')))) {
        current.identityIsCustom = (JSON.stringify(current.backgrounds) !=
          JSON.stringify(updated.backgrounds) || JSON.stringify(current.buttons) != JSON.stringify(updated.buttons));
      }
    }
    return current;
    
    
  }
  
  public static getIdentity(community: string = null) {
    let COLORS = {
      default: {
        mainColor: '#4687E6',
        secondaryColor: '#2D72D6'
      },
      "covid-19": {
        mainColor: "#03ADEE",
        secondaryColor: "#F15157"
      }
    };
    if (community && COLORS[community]) {
      return COLORS[community];
    }
    return COLORS.default;
  }
  
  public static getRGBA(color, A) {
    if (color.indexOf("#") != -1) {
      return 'rgba(' + parseInt(color.substring(1, 3), 16) + ',' + parseInt(color.substring(3, 5), 16) + ',' + parseInt(color.substring(5, 7), 16) + ',' + A + ')';
    }
    return color;
  }
  
  public static isForLightBackground(color: string) {
    let L, r, g, b, a = 1;
    if (color.length == 7 || color.length == 9) {
      r = parseInt(color.substring(1, 3), 16);
      g = parseInt(color.substring(3, 5), 16);
      b = parseInt(color.substring(5, 7), 16);
      if (color.length == 9) {
        a = parseInt(color.substring(7, 9), 16);
      }
    } else if (color.length > 9) {
      let array = color.split("rgba(")[1].split(")")[0].split(",");
      r = parseInt(array[0]);
      g = parseInt(array[1]);
      b = parseInt(array[2]);
      a = +array[3];
      
    }
    const brightness = r * 0.299 + g * 0.587 + b * 0.114 + (1 - a) * 255;
    
    return (brightness < 186)
    
    // return !(r*0.299 + g*0.587 + b*0.114 > 186);
    /*for(let c of [r,g,b]){
      c = c / 255.0;
      if(c <= 0.03928){
        c = c / 12.92;
      }else {
        c = ((c + 0.055) / 1.055) ^ 2.4;
      }
    }
    L = 0.2126 * r + 0.7152 * g + 0.0722 * b;
   return L >  0.179// (L + 0.05) / (0.05) > (1.0 + 0.05) / (L + 0.05); //use #000000 else use #ffffff
*/
  }
  
  
}

export class ButtonsCustomization {
  isDefault: boolean;
  backgroundColor: string;
  color: string;
  borderStyle: string;
  borderColor: string;
  borderWidth: number;
  borderRadius: number;
  onHover: {
    backgroundColor: string;
    color: string;
    borderColor: string;
  };
}
