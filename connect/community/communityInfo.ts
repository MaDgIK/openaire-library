import {StringUtils} from "../../utils/string-utils.class";
import {SelectionCriteria} from "../../utils/entities/contentProvider";

export class CommunityInfo {
  title: string;
  shortTitle:string;
  displayTitle: string;
  displayShortTitle:string;
  communityId: string;
  queryId: string;
  type: string;
  /** @warning Use pipe in HTML or StringUtils.getLogoUrl in components */
  logoUrl: string;
  description: string;
  managers: string[];
  date:Date;
  subjects: string[];
  status:"all" | "manager" | "hidden" | "PUBLIC" | "RESTRICTED" | "PRIVATE";
  claim: "all" | "managersOnly" | "membersOnly";
  membership: "open" | "byInvitation";
  zenodoCommunity:string;
  otherZenodoCommunities: string[];
  isUpload: boolean;
  isSubscribed: boolean;
  isManager: boolean;
  fos: string[] = [];
  sdg: string[] = []
  selectionCriteria: SelectionCriteria;
  plan: "Standard" | "Advanced" | "Premium" | "National" | "Default" ;
  featured:boolean;
  public static checkIsUpload(response: CommunityInfo | CommunityInfo[]): any | any[] {
    if (Array.isArray(response)) {
      response.forEach(value => {
        value.isUpload = value.logoUrl && !StringUtils.isValidUrl(value.logoUrl);
      });
    } else {
      response.isUpload = response.logoUrl && !StringUtils.isValidUrl(response.logoUrl);
    }
    return response;
  }

  public isOpen() {
    return !(this.membership && this.membership === 'byInvitation');
  }

  public isPublic(){
    return this.status == "all" || this.status == "PUBLIC";
  }
  public isRestricted(){
    return this.status == "manager" || this.status == "RESTRICTED";
  }
  public isPrivate(){
    return this.status == "hidden" || this.status == "PRIVATE";
  }
  public validateStatus(){
    if(!(this.isPrivate() || this.isRestricted() || this.isPublic())){
      this.status = "PRIVATE";
    }
  }
}
// export const prodReadyCommunities = ["dh-ch", "ee", "fam", "mes", "ni", "covid-19", "dariah", "epos", "egi"];
