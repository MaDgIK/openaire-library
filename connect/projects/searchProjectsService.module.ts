import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';

import {SearchCommunityProjectsService} from './searchProjects.service';


@NgModule({
  imports: [
    CommonModule, FormsModule
  ],
  declarations: [
  ],
  providers:[
  SearchCommunityProjectsService
],
  exports: [
    ]
})
export class SearchProjectsServiceModule { }
