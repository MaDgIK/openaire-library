import {take, tap} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot, UrlTree, CanActivateChild,
  
} from '@angular/router';
import {Observable, Subscription} from 'rxjs';
import {CommunityService} from '../community/community.service';
import {properties} from "../../../../environments/environment";

@Injectable()
export class ConnectCommunityGuard implements CanActivate, CanActivateChild {
  sub: Subscription = null;

  constructor(private router: Router,
              private communityService: CommunityService) {
  }

  check(community: string, url: string): Observable<boolean> | boolean {
    return this.communityService.isCommunityType(community).pipe(take(1), tap(isCommunity => {
      if (!isCommunity) {
        this.router.navigate([properties.errorLink], {queryParams: {'page': url}});
      }
    }));
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    let community = route.params['community'];
    return community && this.check(community, state.url);
  }
  
  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    let community = childRoute.params['community'];
    return community && this.check(community, state.url);
  }
}
