import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';

import {ConnectHelper} from '../connectHelper';
import {properties} from "../../../../environments/environment";
import {CommunityService} from "../community/community.service";
import {map} from "rxjs/operators";

@Injectable()
export class IsCommunity  {

  constructor(private router: Router,
              private communityService: CommunityService) {
  }

  check(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    let community;
    if(properties.isDashboard) {
      community = route.params['community'];
    } else {
      community = ConnectHelper.getCommunityFromDomain(properties.domain);
    }
    if (community) {
      return this.communityService.getCommunity(community).pipe(map(community => {
        if(community) {
          return true;
        } else {
          this.router.navigate([properties.errorLink], {queryParams: {page: state.url}});
          return false;
        }
      }));
    } else {
      this.router.navigate([properties.errorLink], {queryParams: {page: state.url}});
      return false;
    }
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.check(route, state);
  }
  
  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.check(childRoute, state);
  }
}
