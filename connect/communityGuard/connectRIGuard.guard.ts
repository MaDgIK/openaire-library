import {tap} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanActivateChild,
  Router,
  RouterStateSnapshot,
  UrlTree
} from '@angular/router';
import {Observable, Subscription} from 'rxjs';
import {CommunityService} from '../community/community.service';
import {properties} from "../../../../environments/environment";

@Injectable()
export class ConnectRIGuard implements CanActivate, CanActivateChild {
  sub: Subscription = null;
  
  constructor(private router: Router,
              private communityService: CommunityService) {
  }
  
  check(community: string, url: string): Observable<boolean> | boolean {
    return this.communityService.isRIType(community).pipe(tap(authorized => {
      if (!authorized) {
        this.router.navigate([properties.errorLink], {queryParams: {'page': url}});
      }
    }));
  }
  
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    let community = route.params['community'];
    return community && this.check(community, state.url);
  }
  
  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    let community = childRoute.params['community'];
    return community && this.check(community, state.url);
  }
}
