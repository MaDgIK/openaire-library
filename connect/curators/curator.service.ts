import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Curator} from '../../utils/entities/CuratorInfo';
import {EnvProperties} from '../../utils/properties/env-properties';
import {CustomOptions} from "../../services/servicesUtils/customOptions.class";
import {properties} from '../../../../environments/environment';

@Injectable()
export class CuratorService {

  constructor(private http: HttpClient) {
  }

  public getCurators(communityId: string): Observable<Curator[]> {
    let url: string = properties.adminToolsAPIURL + communityId + '/curator';
    return this.http.get<Curator[]>((properties.useCache) ? (properties.cacheUrl + encodeURIComponent(url)) : url);
  }

  public updateCurator(curator: Curator) {
    let url: string = properties.adminToolsAPIURL + "curator";
    return this.http.post<Curator>(url, curator, CustomOptions.registryOptions());
  }

  public getCurator(): Observable<Curator> {
    let url: string = properties.adminToolsAPIURL + 'curator';
    return this.http.get<Curator>((properties.useCache) ? (properties.cacheUrl + encodeURIComponent(url)) : url, CustomOptions.registryOptions());
  }

}
