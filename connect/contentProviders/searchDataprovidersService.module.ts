import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';

import {SearchCommunityDataprovidersService} from './searchDataproviders.service';


@NgModule({
  imports: [
    CommonModule, FormsModule
  ],
  declarations: [
  ],
  providers:[
  SearchCommunityDataprovidersService
],
  exports: [
    ]
})
export class SearchDataprovidersServiceModule { }
