import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {EnvProperties} from '../../utils/properties/env-properties';
import {map} from "rxjs/operators";

@Injectable()
export class SearchCommunityDataprovidersService {
    constructor(private http: HttpClient ) {}
    searchDataproviders (properties:EnvProperties, pid: string, deposit = false):any {
        let url = properties.communityAPI+pid+"/datasources" + (deposit?'?deposit=true':'');

        return this.http.get((properties.useCache)? (properties.cacheUrl+encodeURIComponent(url)): url);
    }
    countTotalDataproviders(properties:EnvProperties,pid:string) {
        let url = properties.communityAPI+pid+"/datasources";
        return  this.http.get((properties.useCache) ? (properties.cacheUrl + encodeURIComponent(url)) : url)
          .pipe(map(res => res['length']));
    }
}
