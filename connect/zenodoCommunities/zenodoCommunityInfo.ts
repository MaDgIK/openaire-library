export class ZenodoCommunityInfo {
  title: string;
  id: string;
  description: string;
  link: string;
  logoUrl: string;
  date: Date;
  page: string;
  master:boolean = false;
}
