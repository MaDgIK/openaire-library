import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';

import {ZenodoCommunitiesService} from './zenodo-communities.service';



@NgModule({
  imports: [
    CommonModule, FormsModule
  ],
  declarations: [
  ],
  providers:[
  ZenodoCommunitiesService
],
  exports: [
    ]
})
export class ZenodoCommunitiesServiceModule { }
