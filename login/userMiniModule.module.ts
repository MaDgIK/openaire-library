import {NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from "@angular/router";

import {UserMiniComponent} from "./userMini.component";
import {IconsModule} from "../utils/icons/icons.module";
import {IconsService} from "../utils/icons/icons.service";
import {login} from "../utils/icons/icons";
import {NotificationsSidebarModule} from "../notifications/notifications-sidebar/notifications-sidebar.module";

@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule, IconsModule, NotificationsSidebarModule
  ],
  declarations: [
    UserMiniComponent
  ],
  providers: [],
  exports: [
    UserMiniComponent
  ]
})
export class UserMiniModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([login])
  }
}
