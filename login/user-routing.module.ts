import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { UserComponent } from './user.component';
import {PreviousRouteRecorder} from '../utils/piwik/previousRouteRecorder.guard';

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '', component: UserComponent, canDeactivate: [PreviousRouteRecorder]},

    ])
  ]
})
export class UserRoutingModule { }
