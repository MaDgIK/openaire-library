import {Injectable} from '@angular/core';
import { ActivatedRouteSnapshot, Data, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import {Observable} from 'rxjs';
import {Session} from './utils/helper.class';
import {LoginErrorCodes} from './utils/guardHelper.class';
import {UserManagementService} from "../services/user-management.service";
import {map, tap} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AdminLoginGuard  {
  
  constructor(private router: Router,
              private userManagementService: UserManagementService) {
  }
  
  check(data: Data, path: string): Observable<boolean> {
    let errorCode = LoginErrorCodes.NOT_LOGIN;
    return this.userManagementService.getUserInfo().pipe(map(user => {
      if (user) {
        errorCode = LoginErrorCodes.NOT_ADMIN;
      }
      return Session.isPortalAdministrator(user) ||
        (data.monitorCurator && Session.isMonitorCurator(user)) ||
        (data.communityCurator && Session.isCommunityCurator(user)) ||
        (data.monitorManager && Session.isKindOfMonitorManager(user)) ||
        (data.communityManager && Session.isKindOfCommunityManager(user))
    }),tap(isAdmin => {
      if(!isAdmin) {
        this.router.navigate(['/user-info'], {
          queryParams: {
            'errorCode': errorCode,
            'redirectUrl': path
          }
        });
      }
    }));

  }
  
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.check(route.data, state.url);
  }
  
  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.check(childRoute.data, state.url);
  }
  
}
