import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import {Observable} from 'rxjs';
import {LoginErrorCodes} from './utils/guardHelper.class';

@Injectable({providedIn: 'root'})
export class FreeGuard  {

  constructor(private router: Router) {
  }

  check(path: string): boolean {
    const valid = true;
    if (!valid) {
      this.router.navigate(['/user-info'], {queryParams: {'errorCode': LoginErrorCodes.NOT_VALID, 'redirectUrl': path}});
    }
    return valid;
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.check(state.url);
  }

}
