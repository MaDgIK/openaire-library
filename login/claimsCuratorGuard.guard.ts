import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {Session} from './utils/helper.class';
import {LoginErrorCodes} from './utils/guardHelper.class';
import {map, tap} from "rxjs/operators";
import {UserManagementService} from "../services/user-management.service";

@Injectable()
export class ClaimsCuratorGuard implements CanActivate {
  
  constructor(private router: Router,
              private userManagementService: UserManagementService) {
  }
  
  check(path: string): Observable<boolean> | boolean {
    let errorCode = LoginErrorCodes.NOT_LOGIN;
    return this.userManagementService.getUserInfo().pipe(map(user => {
      if (user) {
        errorCode = LoginErrorCodes.NOT_ADMIN;
      }
      return Session.isClaimsCurator(user) || Session.isPortalAdministrator(user);
    }),tap(isAdmin  => {
      if(!isAdmin) {
        this.router.navigate(['/user-info'], {
          queryParams: {
            'errorCode': errorCode,
            'redirectUrl': path
          }
        });
      }
    }));
  }
  
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.check(state.url);
  }
  
}
