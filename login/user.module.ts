import {NgModule}              from '@angular/core';
import {CommonModule}          from '@angular/common';
import {FormsModule}           from '@angular/forms';
import { RouterModule } from '@angular/router';

import {UserRoutingModule}     from './user-routing.module';

import {UserComponent}         from './user.component';

import {LoadingModule} from "../utils/loading/loading.module";

@NgModule({
  imports: [
    CommonModule, FormsModule, UserRoutingModule, RouterModule, LoadingModule
  ],
  providers: [],
  declarations: [
    UserComponent
  ],
  exports: [
    UserComponent
  ]
})

export class UserModule { }
