import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';
import { RouterModule } from '@angular/router';

import{ReloadRoutingModule } from './reload-routing.module';
import{ReloadComponent} from './reload.component';

import {LoadingModule} from "../utils/loading/loading.module";

@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule,
    ReloadRoutingModule, LoadingModule
  
  ],
  declarations: [
    ReloadComponent
   ],
  providers:[],
  exports: [
    ReloadComponent
     ]
})
export class ReloadModule { }
