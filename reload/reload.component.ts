import {Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Session} from '../login/utils/helper.class';
import {HelperFunctions} from "../utils/HelperFunctions.class";

@Component({
  selector: 'reload',
  template: `
    <loading [full]="true"></loading>
  `
})
export class ReloadComponent {
  
  constructor(private route: ActivatedRoute, private _router: Router) {
  }
  
  public ngOnInit() {
    let URL = Session.popReloadUrl();
    if (URL && URL["path"] && URL["path"] != "") {
      let url: string = URL["path"];
      let host = URL["host"];
      let paramsObject = ((URL["params"] && URL["params"] != null) ? Session.getParamsObj(URL["params"]) : null);
      if (host == (location.protocol + "//" + location.host)) {
        let baseUrl = (document && document.getElementsByTagName('base')) ? document.getElementsByTagName('base')[0].href.split(document.location.host)[1] : "/";
        url = (baseUrl.length > 1 && url.indexOf(baseUrl) != -1) ? ("/" + url.split(baseUrl)[1]) : url;
        if (paramsObject) {
          if(URL['fragment'] && URL['fragment'] !== '') {
            this._router.navigate([url], {queryParams: paramsObject, fragment: URL['fragment']});
          } else {
            this._router.navigate([url], {queryParams: paramsObject});
          }
        } else {
          if(URL['fragment'] && URL['fragment'] !== '') {
            this._router.navigate([url], {fragment: URL['fragment']});
          } else {
            this._router.navigate([url]);
          }
        }
      } else {
        window.location.href = host + url + ((URL["params"] && URL["params"] != null) ? ((URL["params"].indexOf("?") == -1 ? "?" : "") + URL["params"]) : "");
      }
      
    } else {
      this._router.navigate(['/']);
    }
  }
  
}
