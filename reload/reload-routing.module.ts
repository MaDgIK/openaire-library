import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import{ReloadComponent} from './reload.component';
import {PreviousRouteRecorder} from '../utils/piwik/previousRouteRecorder.guard';

@NgModule({
  imports: [
    RouterModule.forChild([
     	{ path: '', component: ReloadComponent, canDeactivate: [PreviousRouteRecorder] }

    ])
  ]
})
export class ReloadRoutingModule { }
