import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {ReactiveFormsModule} from "@angular/forms";
import {InputModule} from "../../sharedComponents/input/input.module";
import {LoadingModule} from "../../utils/loading/loading.module";
import {SdgSelectionComponent} from "./sdg-selection.component";

@NgModule({
	imports: [
		CommonModule, ReactiveFormsModule, LoadingModule, InputModule
	],
	declarations: [
		SdgSelectionComponent
	],
	providers: [
		
	],
	exports: [
		SdgSelectionComponent
	]
})
export class SdgSelectionModule {

}