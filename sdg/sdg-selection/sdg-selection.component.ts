import {HttpClient} from "@angular/common/http";
import {Component, Input} from "@angular/core";
import {UntypedFormArray, UntypedFormBuilder} from "@angular/forms";
import {properties} from "../../../../environments/environment";
import {EnvProperties} from "../../utils/properties/env-properties";
import {StringUtils} from "../../utils/string-utils.class";
import {ISVocabulariesService} from "../../utils/staticAutoComplete/ISVocabularies.service";

@Component({
	selector: 'sdg-selection',
	templateUrl: 'sdg-selection.component.html',
	styleUrls: ['sdg-selection.component.less']
})
export class SdgSelectionComponent {
	public properties: EnvProperties = properties;
	@Input() subjects: string[];
	@Input() entityType: string;
	@Input() isFeedback: boolean = true;

	public loading: boolean;
	public sdgs: UntypedFormArray;

	constructor(
		private vocabulariesService: ISVocabulariesService,
		private fb: UntypedFormBuilder
	) {}

	ngOnInit() {
		this.loading = true;
		this.sdgs = this.fb.array([]);
		this.vocabulariesService.getSDGs(properties).subscribe(data => {
			data['sdg'].forEach(element => {
				// this.sdgs.push({code: element.code, id: element.id, label: element.label, html: element.html, checked: this.subjects?.includes(element.id)});
				this.sdgs.push(this.fb.group({
					code: this.fb.control(element.code),
					id: this.fb.control(element.id),
					label: this.fb.control(element.label),
					html: this.fb.control(element.html),
					checked: this.fb.control(this.subjects?.includes(element.id))
				}));
			});
			if(this.isFeedback) {
			// 	// this.sdgs.push({code: '18', id: 'No SDGs are relevant for this ' + this.getEntityName(this.entityType), label: 'Not relevant', html: 'Not relevant', checked: false});
				this.sdgs.push(this.fb.group({
					code: this.fb.control('18'),
					id: this.fb.control('No SDGs are relevant for this ' + this.getEntityName(this.entityType)),
					label: this.fb.control('Not relevant'),
					html: this.fb.control('Not relevant'),
					checked: this.fb.control(false)
				}));
			}
			this.loading = false;
    });
	}

	public reset() {
		this.sdgs.controls.forEach(control => {
			control.get('checked').setValue(this.subjects?.includes(control.value.id));
		});
	}

	public get firstColumn() {
		return this.sdgs.controls.slice(0, Math.ceil(this.sdgs.length/2));
	}

	public get secondColumn() {
		return this.sdgs.controls.slice(Math.ceil(this.sdgs.length/2), this.sdgs.length);
	}

  public getSelectedSubjects() {
		if(this.sdgs) {
			return this.sdgs.value.filter(sub => sub.checked == true);
		}
		return [];
  }

	public get hasChanges() {
		return !!this.sdgs && this.sdgs.dirty;
	}

	private getEntityName (entityType:string) {
    return StringUtils.getEntityName(entityType, false);
  }
}
