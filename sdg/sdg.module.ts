import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {BreadcrumbsModule} from "../utils/breadcrumbs/breadcrumbs.module";
import {RefineFieldResultsServiceModule} from "../services/refineFieldResultsService.module";
import {LoadingModule} from "../utils/loading/loading.module";
import {Schema2jsonldModule} from "../sharedComponents/schema2jsonld/schema2jsonld.module";
import {SEOServiceModule} from "../sharedComponents/SEO/SEOService.module";
import {SdgRoutingModule} from './sdg-routing.module';
import {SdgComponent} from './sdg.component';
import {HelperModule} from "../utils/helper/helper.module";

@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule,
    SdgRoutingModule, BreadcrumbsModule, RefineFieldResultsServiceModule,
    LoadingModule, Schema2jsonldModule, SEOServiceModule, HelperModule
  ],
	declarations: [
		SdgComponent
	],
	providers: [],
	exports: [
		SdgComponent
	]
})
export class SdgModule {

}
