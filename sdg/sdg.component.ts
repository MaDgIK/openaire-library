import {HttpClient} from "@angular/common/http";
import {Component, Input, OnDestroy, OnInit} from "@angular/core";
import {Subscription} from "rxjs";
import {Breadcrumb} from "../utils/breadcrumbs/breadcrumbs.component";
import {EnvProperties} from "../utils/properties/env-properties";
import {properties} from "src/environments/environment";
import {RefineFieldResultsService} from "../services/refineFieldResults.service";
import {OpenaireEntities} from "../utils/properties/searchFields";
import {StringUtils} from "../utils/string-utils.class";
import {Router} from '@angular/router';
import {Meta, Title} from "@angular/platform-browser";
import {SEOService} from "../sharedComponents/SEO/SEO.service";
import {PiwikService} from "../utils/piwik/piwik.service";
import {ISVocabulariesService} from "../utils/staticAutoComplete/ISVocabularies.service";

@Component({
	selector: 'sdg',
	templateUrl: 'sdg.component.html',
	styleUrls: ['sdg.component.less']
})
export class SdgComponent implements OnInit, OnDestroy {
	public url: string = null;
	public pageTitle: string = "OpenAIRE | Sustainable Development Goals";
	public pageDescription: string = "Laying the foundation for new approaches and solutions. We have developed a classification scheme for UN Sustainable Development Goals, to view contributions of research towards complex challenges for humanity such as climate change, biodiversity loss, pollution and poverty reduction.";
	
	private sdgs: any = [];
	private sdgsResearchOutcomes: any = [];
	public displayedSdgs: any = [];

	public loading: boolean;
	properties: EnvProperties = properties;
  openaireEntities = OpenaireEntities;
	@Input() customFilter = null;
	@Input() pageContents = null;
	public breadcrumbs: Breadcrumb[] = [{name: 'home', route: '/'}, {name: 'Sustainable Development Goals'}];

	subscriptions: Subscription[] = [];

	constructor(
		private vocabulariesService: ISVocabulariesService, private refineFieldResultsService: RefineFieldResultsService,
		private _router: Router,
		private _meta: Meta,
		private _title: Title,
		private seoService: SEOService,
		private _piwikService: PiwikService
	) {}

	ngOnInit() {
		this.loading = true;
		this.subscriptions.push(this._piwikService.trackView(this.properties, this.pageTitle).subscribe());
		this.url = this.properties.domain + this.properties.baseLink + this._router.url;
		this.seoService.createLinkForCanonicalURL(this.url);
		this.updateUrl(this.url);
    this.updateTitle(this.pageTitle);
    this.updateDescription(this.pageDescription);
		this.subscriptions.push(this.vocabulariesService.getSDGs(properties).subscribe(data => {
			this.sdgs = data['sdg'];
			this.subscriptions.push(this.refineFieldResultsService.getRefineFieldsResultsByEntityName(['sdg'], 'result', this.properties, refineParams).subscribe(data => {
				this.sdgsResearchOutcomes = data[1][0].values;
				let merged =[];
				for(let i=0; i<this.sdgs.length; i++){
					merged.push({
						...this.sdgs[i],
						...(this.sdgsResearchOutcomes.find((innerItem) => innerItem.id === this.sdgs[i].id))
					});
				}
				this.displayedSdgs = merged;
				this.loading = false;
			}));
		}));
		let refineParams = null;
		if(this.customFilter) {
			let refineValue = StringUtils.URIEncode(this.customFilter.queryFieldName + " exact " + StringUtils.quote((this.customFilter.valueId)));
			refineParams = '&fq=' + refineValue;
		}
	}

	public ngOnDestroy() {
    for (let sub of this.subscriptions) {
      sub.unsubscribe();
    }
  }

  public urlEncodeAndQuote(str: string): string {
    return StringUtils.quote(StringUtils.URIEncode(str));
  }

	private updateUrl(url: string) {
    this._meta.updateTag({content: url}, "property='og:url'");
  }

	private updateTitle(title: string) {
    var _title = ((title.length > 50) ? title.substring(0, 50) : title);
    this._title.setTitle(_title);
    this._meta.updateTag({content: _title}, "property='og:title'");
  }

	private updateDescription(description: string) {
    this._meta.updateTag({content: description}, "name='description'");
    this._meta.updateTag({content: description}, "property='og:description'");
  }
}
