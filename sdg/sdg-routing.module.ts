import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {SdgComponent} from './sdg.component';
import {PreviousRouteRecorder} from '../utils/piwik/previousRouteRecorder.guard';
@NgModule({
  imports: [
    RouterModule.forChild([
			{ path: '', component: SdgComponent, canDeactivate: [PreviousRouteRecorder] }
    ])
  ]
})
export class SdgRoutingModule { }
