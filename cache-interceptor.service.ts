import {Injectable, Inject, PLATFORM_ID, TransferState} from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';
import { properties } from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class CacheInterceptorService implements HttpInterceptor {
  cachingRequests = [
    // Remove 2 following lines when bug with hidden refine filters is solved.
    "/resources2/?format=json&refine=true&fields=instancetypename&fields=foslabel&fields=relfunder&fields=relfundinglevel0_id&fields=relproject&fields=sdg&fields=country&fields=resultlanguagename&fields=resulthostingdatasource&fields=community&&fq=resultbestaccessright exact \"Open Access\"&type=results&page=0&size=0",
    "/resources2/?format=json&refine=true&fields=funder&fields=fundinglevel0_id&fields=projectstartyear&fields=projectendyear&fields=projectoamandatepublications&&type=projects&page=0&size=0",

    "/resources2/?format=json&refine=true&fields=instancetypename&fields=foslabel&fields=relfunder&fields=sdg&fields=country&fields=resultlanguagename&fields=resulthostingdatasource&fields=community&&type=results&page=0&size=0",
    "/resources2/?format=json&refine=true&fields=instancetypename&fields=foslabel&fields=relfunder&fields=relfundinglevel0_id&fields=relproject&fields=sdg&fields=country&fields=resultlanguagename&fields=resulthostingdatasource&fields=community&&fq=resultbestaccessright%20exact%20%22Open%20Access%22&fq=relfunder%20exact%20%22ec__________%3A%3AEC%7C%7CEuropean%20Commission%7C%7CEC%22&type=results&page=0&size=0",

    "/resources2/?format=json&refine=true&fields=instancetypename&fields=foslabel&fields=relfunder&fields=sdg&fields=country&fields=resultlanguagename&fields=resulthostingdatasource&fields=community&&type=publications&page=0&size=0",
    "/resources2/?format=json&refine=true&fields=instancetypename&fields=foslabel&fields=relfunder&fields=sdg&fields=country&fields=resultlanguagename&fields=resulthostingdatasource&fields=community&&type=datasets&page=0&size=0",
    "/resources2/?format=json&refine=true&fields=instancetypename&fields=foslabel&fields=relfunder&fields=sdg&fields=country&fields=resultlanguagename&fields=resulthostingdatasource&fields=community&&type=software&page=0&size=0",
    "/resources2/?format=json&refine=true&fields=instancetypename&fields=foslabel&fields=relfunder&fields=sdg&fields=country&fields=resultlanguagename&fields=resulthostingdatasource&fields=community&&type=other&page=0&size=0",

    "/resources2/?format=json&refine=true&fields=instancetypename&fields=foslabel&fields=relfunder&fields=sdg&fields=country&fields=resultlanguagename&fields=resulthostingdatasource&fields=community&&fq=resultbestaccessright%20exact%20%22Open%20Access%22&type=results&page=0&size=0",
    "/resources2/?format=json&refine=true&fields=instancetypename&fields=foslabel&fields=relfunder&fields=sdg&fields=country&fields=resultlanguagename&fields=resulthostingdatasource&fields=community&&fq=resultbestaccessright exact \"Open Access\"&type=results&page=0&size=0",
    "/resources2/?format=json&refine=true&fields=funder&fields=projectoamandatepublications&fields=projectstartyear&fields=projectendyear&&type=projects&page=0&size=0",
    "/resources2/?format=json&refine=true&fields=eoscdatasourcetype&fields=datasourceodlanguages&fields=datasourceodcontenttypes&fields=datasourcecompatibilityname&fields=country&fields=collectedfromname&fields=datasourcethematic&fields=datasourcejurisdiction&&type=datasources&page=0&size=0",
    // "/resources2/?format=json&query= ( (reldatasourcecompatibilityid exact driver or reldatasourcecompatibilityid exact driver-openaire2.0 or reldatasourcecompatibilityid exact openaire2.0 or reldatasourcecompatibilityid exact openaire3.0 or reldatasourcecompatibilityid exact openaire4.0 or reldatasourcecompatibilityid exact openaire-cris_1.1 or reldatasourcecompatibilityid exact openaire2.0_data or reldatasourcecompatibilityid exact hostedBy or relproject=*) ) &refine=true&fields=country&&type=organizations&page=0&size=0",
    "/resources2/?format=json&refine=true&fields=country&&type=organizations&fq=(reldatasourcecompatibilityid exact driver or reldatasourcecompatibilityid exact driver-openaire2.0 or reldatasourcecompatibilityid exact openaire2.0 or reldatasourcecompatibilityid exact openaire3.0 or reldatasourcecompatibilityid exact openaire4.0 or reldatasourcecompatibilityid exact openaire-cris_1.1 or reldatasourcecompatibilityid exact openaire2.0_data or reldatasourcecompatibilityid exact hostedBy or relproject=*)&page=0&size=0&minRef=true",

    "/resources2/?format=json&size=0&type=organizations&fq=(reldatasourcecompatibilityid exact driver or reldatasourcecompatibilityid exact driver-openaire2.0 or reldatasourcecompatibilityid exact openaire2.0 or reldatasourcecompatibilityid exact openaire3.0 or reldatasourcecompatibilityid exact openaire4.0 or reldatasourcecompatibilityid exact openaire-cris_1.1 or reldatasourcecompatibilityid exact openaire2.0_data or reldatasourcecompatibilityid exact hostedBy or relproject=*)",

    // "/projects/?format=json&refine=true&page=1&size=0&fields=funder",
    "/projects/?fields=funder&sf=funder&format=json&size=0",
    "/results/?fields=relfunder&sf=relfunder&format=json&size=0",

    "/resources/?format=json&query=( oaftype exact datasource  and  (eoscdatasourcetype exact \"Journal Archive\"  or eoscdatasourcetype exact \"Repository\"))&refine=true&fields=datasourcetypename&fields=country&fields=datasourceodsubjects&fields=datasourceodcontenttypes&fields=datasourcecompatibilityname&fields=datasourcethematic&fields=datasourcejurisdiction&&type=datasources&page=0&size=0",

    "/search/v2/api/datasources/count?format=json", "/search/v2/api/publications/count?format=json", "/search/v2/api/datasets/count?format=json", "/search/v2/api/software/count?format=json", "/search/v2/api/other/count?format=json",
    "relresulttype%3Dpublication",

    "/resources2/?format=json&size=0&type=projects",
    "/resources2/?format=json&size=0&type=datasources",
    "/resources2/?format=json&size=0&type=results",
    "/resources2/?format=json&size=0&type=results&fq=resultbestaccessright%20exact%20%22Open%20Access%22"
  ];


  constructor(private transferState: TransferState, @Inject(PLATFORM_ID) private platformId: any) {}

  public intercept(request: HttpRequest<any>, next: HttpHandler): any {
    // console.debug(new Date());

    if(this.checkForCachedRequests(request.url)) {
      const dupReq = request.clone({url: properties.cacheUrl + encodeURIComponent(request.url)});
      return next.handle(dupReq);
    } else {
      return next.handle(request);
    }
  }

  public checkForCachedRequests(url){
    // Do not call cache when it is not enabled at all.
    // This property is not correctly used here. A new property should be introduced. For now it is ok.
    if (!properties.useLongCache) {
      return false;
    }
    if(url.indexOf("sf=") !== -1 || url.indexOf("refine=true") !== -1 || url.indexOf("/count?format=json") !== -1 || url.indexOf("relresulttype%3Dpublication") !== -1 ||
      (url.indexOf("?format=json&size=0&type=") !== -1 && url.indexOf("query=") == -1)) {
      return this.cachingRequests.some(partUrl => (url.indexOf(partUrl) !== -1));
    }
    return false;
  }

}
