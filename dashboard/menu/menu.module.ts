import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {MenuComponent} from './menu.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AlertModalModule} from '../../utils/modal/alertModal.module';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {AdminToolServiceModule} from "../../services/adminToolService.module";
import {InputModule} from "../../sharedComponents/input/input.module";
import {PageContentModule} from "../sharedComponents/page-content/page-content.module";
import {AdminTabsModule} from "../sharedComponents/admin-tabs/admin-tabs.module";
import {MenuRoutingModule} from "./menu-routing.module";
import {SearchInputModule} from "../../sharedComponents/search-input/search-input.module";
import {IconsModule} from "../../utils/icons/icons.module";
import {LoadingModule} from "../../utils/loading/loading.module";
import {TransitionGroupModule} from '../../utils/transition-group/transition-group.module';
import {LogoUrlPipeModule} from '../../utils/pipes/logoUrlPipe.module';

@NgModule({
  imports: [
    MenuRoutingModule,
    CommonModule, RouterModule, FormsModule, AdminToolServiceModule,
    AlertModalModule, ReactiveFormsModule, MatSlideToggleModule, InputModule, PageContentModule, AdminTabsModule, SearchInputModule, IconsModule, LoadingModule,
		TransitionGroupModule, LogoUrlPipeModule
  ],
    declarations: [MenuComponent],
    exports: [MenuComponent]
})
export class MenuModule {
}
