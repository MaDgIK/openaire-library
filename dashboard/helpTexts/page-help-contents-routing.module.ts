import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';
import {PageHelpContentsComponent} from "./page-help-contents.component";

@NgModule({
    imports: [
        RouterModule.forChild([
            { path: '',  component: PageHelpContentsComponent}
        ])
    ]
})
export class PageHelpContentsRoutingModule { }
