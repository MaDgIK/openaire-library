import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {PageContentFormComponent} from './page-help-content-form.component';


@NgModule({
    imports: [
        RouterModule.forChild([
            { path: '', component: PageContentFormComponent}
        ])
    ]
})
export class PageHelpContentFormRoutingModule { }
