import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AlertModalModule} from '../../utils/modal/alertModal.module';
import {PageHelpContentsComponent} from './page-help-contents.component';
import {SafeHtmlPipeModule} from '../../utils/pipes/safeHTMLPipe.module';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {AdminToolServiceModule} from '../../services/adminToolService.module';
import {InputModule} from '../../sharedComponents/input/input.module';
import {PageHelpContentsRoutingModule} from './page-help-contents-routing.module';
import {PageContentModule} from '../sharedComponents/page-content/page-content.module';
import {SearchInputModule} from '../../sharedComponents/search-input/search-input.module';
import {IconsModule} from '../../utils/icons/icons.module';
import {LoadingModule} from '../../utils/loading/loading.module';
import {HTMLToStringPipeModule} from '../../utils/pipes/HTMLToStringPipe.module';

@NgModule({
  imports: [
    PageHelpContentsRoutingModule,
    CommonModule, RouterModule, FormsModule, SafeHtmlPipeModule,
    AlertModalModule, ReactiveFormsModule, MatSlideToggleModule, AdminToolServiceModule, InputModule, PageContentModule,
    SearchInputModule, IconsModule, LoadingModule, HTMLToStringPipeModule
  ],
    declarations: [
        PageHelpContentsComponent
    ],
    exports: [PageHelpContentsComponent]
})
export class PageHelpContentsModule {}
