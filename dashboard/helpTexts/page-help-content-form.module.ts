import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AlertModalModule} from '../../utils/modal/alertModal.module';
import {SafeHtmlPipeModule} from '../../utils/pipes/safeHTMLPipe.module';
import {CKEditorModule} from 'ng2-ckeditor';
import {PageContentFormComponent} from './page-help-content-form.component';
import {PageHelpContentFormRoutingModule} from './page-help-content-form-routing.module';
import {AdminToolServiceModule} from '../../services/adminToolService.module';
import {InputModule} from '../../sharedComponents/input/input.module';
import {IconsModule} from '../../utils/icons/icons.module';
import {PageContentModule} from '../sharedComponents/page-content/page-content.module';
import {RouterModule} from '@angular/router';
import {LoadingModule} from '../../utils/loading/loading.module';
import {MatSlideToggleModule} from "@angular/material/slide-toggle";

@NgModule({
  imports: [
    PageHelpContentFormRoutingModule,
    CommonModule, FormsModule, RouterModule,
    SafeHtmlPipeModule, CKEditorModule,
    AlertModalModule, ReactiveFormsModule, AdminToolServiceModule, InputModule, IconsModule, PageContentModule, LoadingModule, MatSlideToggleModule
  ],
  declarations: [PageContentFormComponent],
  exports: [PageContentFormComponent]
})
export class PageHelpContentFormModule {
}
