import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RoleUsersComponent} from './role-users.component';
import {ReactiveFormsModule} from '@angular/forms';
import {AlertModalModule} from "../../../utils/modal/alertModal.module";
import {LoadingModule} from "../../../utils/loading/loading.module";
import {IconsModule} from "../../../utils/icons/icons.module";
import {InputModule} from "../../../sharedComponents/input/input.module";
import {PageContentModule} from "../../sharedComponents/page-content/page-content.module";
import {SafeHtmlPipeModule} from "../../../utils/pipes/safeHTMLPipe.module";
import {NotifyFormModule} from "../../../notifications/notify-form/notify-form.module";
import {NoLoadPaging} from "../../../searchPages/searchUtils/no-load-paging.module";
import {SearchInputModule} from "../../../sharedComponents/search-input/search-input.module";
import {PagingModule} from "../../../utils/paging.module";

@NgModule({
  imports: [CommonModule, AlertModalModule, ReactiveFormsModule, LoadingModule, IconsModule, InputModule, PageContentModule, SafeHtmlPipeModule, NotifyFormModule, NoLoadPaging, SearchInputModule, PagingModule],
  declarations: [RoleUsersComponent],
  exports: [RoleUsersComponent]
})
export class RoleUsersModule {}
