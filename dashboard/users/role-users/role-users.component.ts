import {
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {UntypedFormArray, UntypedFormBuilder, UntypedFormGroup, ValidatorFn, Validators} from '@angular/forms';
import {AlertModal} from "../../../utils/modal/alert";
import {UserRegistryService} from "../../../services/user-registry.service";
import {EnvProperties} from "../../../utils/properties/env-properties";
import {properties} from "../../../../../environments/environment";
import {Role, RoleUtils, Session, User} from "../../../login/utils/helper.class";
import {UserManagementService} from "../../../services/user-management.service";
import {Router} from "@angular/router";
import {StringUtils} from "../../../utils/string-utils.class";
import {NotificationService} from "../../../notifications/notification.service";
import {forkJoin, of, Subscription} from "rxjs";
import {NotificationHandler} from "../../../utils/notification-handler";
import {ClearCacheService} from "../../../services/clear-cache.service";
import {catchError, map, tap} from "rxjs/operators";
import {InputComponent} from "../../../sharedComponents/input/input.component";
import {StakeholderUtils} from "../../../monitor-admin/utils/indicator-utils";

class InvitationResponse {
  email: string;
  invitation: any;

  constructor(email, invitation) {
    this.email = email;
    this.invitation = invitation;
  }
}

@Component({
  selector: 'role-users',
  templateUrl: 'role-users.component.html'
})
export class RoleUsersComponent implements OnInit, OnDestroy, OnChanges {
  
  @Input()
  public id: string;
  @Input()
  set type(type: string) {
    this._type = Role.mapType(type);
  }
  @Input()
  public name: string;
  @Input()
  public link: string;
  @Input()
  public role: "member" | "manager" = "manager";
  @Input()
  public deleteAuthorizationLevel: 'curator' | 'manager' = 'curator';
  @Input()
  public inviteAuthorizationLevel: 'curator' | 'manager' = 'manager';
  @Input()
  public message: string = null;
  @Input()
  public emailComposer: Function;
  @Input()
  public notificationFn: Function;
  @Input()
  public inviteDisableMessage: string;
  public user: User = null;
  public active: any[] = [];
  public showActive: any[] = [];
  public pending: any[];
  public showPending: any[] = [];
  public showCurrent: boolean = true;
  public subs: any[] = [];
  public loadActive: boolean = true;
  public loadPending: boolean = true;
  public selectedUser: string = null;
  public emailsForm: UntypedFormArray;
  public validators: ValidatorFn[] = [Validators.email];
  public properties: EnvProperties = properties;
  public exists: boolean = true;
  public roleFb: UntypedFormGroup;
  /** Paging */
  activePage: number = 1;
  pendingPage: number = 1;
  pageSize: number = 10;
  /** Search */
  filterForm: UntypedFormGroup;
  @ViewChild('inviteModal') inviteModal: AlertModal;
  @ViewChild('emailInput') emailInput: InputComponent;
  @ViewChild('deleteModal') deleteModal: AlertModal;
  @ViewChild('deletePendingModal') deletePendingModal: AlertModal;
  @ViewChild('createRoleModal') createRoleModal: AlertModal;
  public stakeholderUtils: StakeholderUtils = new StakeholderUtils();
  public roleUtils: RoleUtils = new RoleUtils();
  private _type: string;
  
  constructor(private userRegistryService: UserRegistryService,
              private userManagementService: UserManagementService,
              private clearCacheService: ClearCacheService,
              private notificationService: NotificationService,
              private cdr: ChangeDetectorRef,
              private fb: UntypedFormBuilder) {
  }
  
  ngOnInit() {
    this.initForm();
    this.updateLists();
    this.userManagementService.getUserInfo().subscribe(user => {
      this.user = user;
    });
  }
  
  ngOnChanges(changes: SimpleChanges) {
    if (changes.role) {
      this.unsubscribe();
      this.initForm();
      this.updateLists();
    }
  }
  
  ngOnDestroy() {
    this.unsubscribe();
  }
  
  unsubscribe() {
    this.subs.forEach(sub => {
      if (sub instanceof Subscription) {
        sub.unsubscribe();
      }
    });
  }
  
  initForm() {
    this.filterForm = this.fb.group({
      active: this.fb.control(''),
      pending: this.fb.control('')
    });
    this.subs.push(this.filterForm.get('active').valueChanges.subscribe(value => {
      this.filterActiveBySearch(value);
    }));
    this.subs.push(this.filterForm.get('pending').valueChanges.subscribe(value => {
      this.filterPendingBySearch(value);
    }));
  }
  
  updateLists() {
    this.loadActive = true;
    this.loadPending = true;
    this.subs.push(this.userRegistryService.getActive(this._type, this.id, this.role, true).subscribe(users => {
      this.active = users;
      this.filterActiveBySearch(this.filterForm.value.active);
      this.loadActive = false;
      this.exists = true;
    }, error => {
      this.active = [];
      this.showActive = [];
      if (error.status === 404) {
        this.exists = false;
      }
      this.loadActive = false;
    }));
    this.subs.push(this.userRegistryService.getPending(this._type, this.id, this.role, true).subscribe(users => {
      this.pending = users;
      this.filterPendingBySearch(this.filterForm.value.pending);
      this.loadPending = false;
    }, error => {
      this.pending = [];
      this.loadPending = false;
    }));
  }
  
  get currentActivePage(): any[] {
    if (this.showActive) {
      return this.showActive.slice((this.activePage - 1) * this.pageSize, this.activePage * this.pageSize);
    } else {
      return [];
    }
  }
  
  get currentPendingPage(): any[] {
    if (this.showPending) {
      return this.showPending.slice((this.pendingPage - 1) * this.pageSize, this.pendingPage * this.pageSize);
    } else {
      return [];
    }
  }
  
  openDeleteModal(item: any) {
    if (this.showCurrent) {
      this.selectedUser = item.email;
      this.deleteModal.alertTitle = 'Delete ' + this.roleUtils.roles[this.role];
      this.deleteModal.open();
    } else {
      this.selectedUser = item;
      this.deletePendingModal.alertTitle = 'Cancel invitation';
      this.deletePendingModal.open();
    }
  }
  
  openInviteModal() {
    this.inviteModal.alertTitle = 'Invite ' + this.roleUtils.roles[this.role];
    this.inviteModal.okButtonLeft = false;
    this.inviteModal.okButtonText = 'Send';
    this.emailsForm = this.fb.array([], Validators.required);
    this.cdr.detectChanges();
    this.inviteModal.open();
  }
  
  openCreateRoleModal() {
    this.createRoleModal.alertTitle = 'Create group';
    this.createRoleModal.okButtonLeft = false;
    this.createRoleModal.okButtonText = 'Create';
    this.roleFb = this.fb.group({
      name: this.fb.control(Role.roleName(this._type, this.id), Validators.required),
      description: this.fb.control(Role.roleName(this._type, this.id), Validators.required)
    });
    setTimeout(() => {
      this.roleFb.get('name').disable();
      this.roleFb.get('description').disable();
    }, 0);
    this.createRoleModal.open();
  }
  
  deleteActive() {
    this.loadActive = true;
    this.subs.push(this.userRegistryService.remove(this._type, this.id, this.selectedUser, this.role).subscribe(() => {
      this.active = this.active.filter(user => user.email != this.selectedUser);
      if (this.currentActivePage.length === 0) {
        this.activePage = 1;
      }
      this.filterActiveBySearch(this.filterForm.value.active);
      this.userManagementService.updateUserInfo();
      this.clearCacheService.purgeBrowserCache(this.role + 's of ' + this.id + ' have been updated',this.id);
      NotificationHandler.rise(this.selectedUser + ' <b>is no longer</b> ' + this.role + ' of ' + this.name + ' Dashboard');
      this.loadActive = false;
    }, error => {
      NotificationHandler.rise('An error has occurred. Please try again later', 'danger');
      this.loadActive = false;
    }));
  }
  
  deletePending() {
    this.loadPending = true;
    this.subs.push(this.userRegistryService.cancelInvitation(this._type, this.id, this.selectedUser, this.role).subscribe(() => {
      this.pending = this.pending.filter(user => user != this.selectedUser);
      this.filterPendingBySearch(this.filterForm.value.pending);
      if (this.currentPendingPage.length === 0) {
        this.pendingPage = 1;
      }
      NotificationHandler.rise(StringUtils.capitalize(this.roleUtils.roles[this.role]) + ' invitation to ' + this.selectedUser + ' has been <b>canceled</b>');
      this.loadPending = false;
    }, error => {
      NotificationHandler.rise('An error has occurred. Please try again later', 'danger');
      this.loadPending = false;
    }));
  }

  get valid() {
    return this.emailsForm && this.emailsForm.valid || (this.emailInput && this.emailInput.searchControl.getRawValue() && this.emailInput.searchControl.valid);
  }

  invite() {
    if(this.emailInput.searchControl.getRawValue() && this.emailInput.searchControl.valid) {
      this.emailInput.add(null,true);
    }
    this.showCurrent = false;
    this.loadPending = true;
    let current = null;
    let invitations = (<Array<any>>this.emailsForm.value).map(email => {
      current = email;
      return this.userRegistryService.invite(this._type, this.id, {
        link: this.link,
        email: this.emailComposer(this.name, email, this.role)
      }, this.role).pipe(catchError(error => {
        return of(null);
      }), map(invitation => new InvitationResponse(email, invitation)));
    });
    this.subs.push(forkJoin(invitations).subscribe(responses => {
      let notifications = responses.map(response => {
        if(response.invitation) {
          NotificationHandler.rise(StringUtils.capitalize(this.role) + ' invitation to ' + response.email + ' has been <b>sent</b>');
          if (!this.pending.includes(response.email)) {
            this.pending.push(response.email);
          }
          if(this.notificationFn && properties.notificationsAPIURL) {
            return this.notificationService.sendNotification(this.notificationFn(this.name, response.email, this.role, response.invitation)).pipe(map(notification => {
              return notification;
            }), tap(() => {
              NotificationHandler.rise('A notification has been <b>sent</b> successfully to ' + response.email);
            }), catchError( error => {
              NotificationHandler.rise('An error has occurred while sending a notification to ' + response.email + '. Please try again later', 'danger');
              return of(null);
            }));
          } else {
           return of(null);
          }
        } else {
          NotificationHandler.rise('An error has occurred while sending the invitation mail to ' +
              response.email + '.Check if the user is already a ' + this.roleUtils.roles[this.role] + ' or try again later', 'danger');
          return of(null);
        }
      });
      this.subs.push(forkJoin(notifications).subscribe(() => {
        this.pendingPage = Math.ceil(this.pending.length / this.pageSize);
        this.filterPendingBySearch(this.filterForm.value.pending);
        this.loadPending = false;
      }))
    }));
  }
  
  createGroup() {
    this.loadActive = true;
    this.loadPending = true;
    this.userRegistryService.createRole(this._type, this.id).subscribe(() => {
      NotificationHandler.rise('Group has been <b> successfully created</b>');
      this.updateLists();
    }, error => {
      if(error.status === 409) {
        NotificationHandler.rise('Group already exists. You can try to invite a ' + this.roleUtils.roles[this.role] + ' instead.', 'warning');
        this.updateLists();
      } else {
        NotificationHandler.rise('An error has occurred. Please try again later', 'danger');
        this.loadActive = false;
        this.loadPending = false;
      }
    });
  }
  
  public get canDelete() {
    return (this.deleteAuthorizationLevel === 'curator'?this.isCurator:this.isManager);
  }

  public get canInvite() {
    return this.exists && (this.inviteAuthorizationLevel === 'curator'?this.isCurator:this.isManager);
  }
  
  public isMe(userId: string) {
    return userId && userId.includes(this.user.id) && !this.isCurator;
  }
  
  public get isManager(): boolean {
    return this.isCurator || !!Session.isManager(this._type, this.id, this.user);
  }
  
  public get isCurator(): boolean {
    return this.isPortalAdmin || !!Session.isCurator(this._type, this.user);
  }
  
  public get isPortalAdmin(): boolean {
    return !!Session.isPortalAdministrator(this.user);
  }
  
  updateActivePage(event: any) {
    this.activePage = event.value;
  }
  
  updatePendingPage(event: any) {
    this.pendingPage = event.value;
  }
  
  private filterActiveBySearch(value: any) {
    this.showActive = this.active.filter(active => !value || active.email.includes(value));
    this.activePage = 1;
  }
  
  private filterPendingBySearch(value: any) {
    this.showPending = this.pending.filter(pending => !value || pending.includes(value));
    this.pendingPage = 1;
  }
}
