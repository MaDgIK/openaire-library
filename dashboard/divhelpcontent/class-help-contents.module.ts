import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {IsCommunity} from '../../connect/communityGuard/isCommunity.guard';
import {ConnectAdminLoginGuard} from '../../connect/communityGuard/connectAdminLoginGuard.guard';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AlertModalModule} from '../../utils/modal/alertModal.module';
import {SafeHtmlPipeModule} from '../../utils/pipes/safeHTMLPipe.module';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {AdminToolServiceModule} from '../../services/adminToolService.module';
import {InputModule} from '../../sharedComponents/input/input.module';
import {SearchInputModule} from '../../sharedComponents/search-input/search-input.module';
import {IconsModule} from '../../utils/icons/icons.module';
import {LoadingModule} from '../../utils/loading/loading.module';
import {HTMLToStringPipeModule} from '../../utils/pipes/HTMLToStringPipe.module';
import {ClassHelpContentsRoutingModule} from './class-help-contents-routing.module';
import {ClassHelpContentsComponent} from './class-help-contents.component';
import {PageContentModule} from '../sharedComponents/page-content/page-content.module';

@NgModule({
  imports: [
    ClassHelpContentsRoutingModule,
    CommonModule, RouterModule, FormsModule, SafeHtmlPipeModule,
    AlertModalModule, ReactiveFormsModule, MatSlideToggleModule, AdminToolServiceModule, InputModule,
    SearchInputModule, IconsModule, LoadingModule, HTMLToStringPipeModule, PageContentModule
  ],
    declarations: [
        ClassHelpContentsComponent
    ],
    providers: [IsCommunity, ConnectAdminLoginGuard],
    exports: [ClassHelpContentsComponent]
})
export class ClassHelpContentsModule {}
