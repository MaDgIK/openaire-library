import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';
import {ClassHelpContentsComponent} from './class-help-contents.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            { path: '',  component: ClassHelpContentsComponent}
        ])
    ]
})
export class ClassHelpContentsRoutingModule { }
