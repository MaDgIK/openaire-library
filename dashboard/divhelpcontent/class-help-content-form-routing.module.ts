import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {ClassContentFormComponent} from './class-help-content-form.component';


@NgModule({
    imports: [
        RouterModule.forChild([
            { path: '', component: ClassContentFormComponent}
        ])
    ]
})
export class ClassHelpContentFormRoutingModule { }
