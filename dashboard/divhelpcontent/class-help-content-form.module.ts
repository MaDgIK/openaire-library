import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AlertModalModule} from '../../utils/modal/alertModal.module';
import {SafeHtmlPipeModule} from '../../utils/pipes/safeHTMLPipe.module';
import {CKEditorModule} from 'ng2-ckeditor';
import {AdminToolServiceModule} from '../../services/adminToolService.module';
import {InputModule} from '../../sharedComponents/input/input.module';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import {IconsModule} from '../../utils/icons/icons.module';
import {RouterModule} from '@angular/router';
import {LoadingModule} from '../../utils/loading/loading.module';
import {ClassContentFormComponent} from './class-help-content-form.component';
import {ClassHelpContentFormRoutingModule} from './class-help-content-form-routing.module';
import {PageContentModule} from '../sharedComponents/page-content/page-content.module';

@NgModule({
  imports: [
    ClassHelpContentFormRoutingModule,
    CommonModule, FormsModule, RouterModule,
    SafeHtmlPipeModule, CKEditorModule,
    AlertModalModule, ReactiveFormsModule, AdminToolServiceModule, InputModule, MatSlideToggleModule, IconsModule, LoadingModule, PageContentModule
  ],
    declarations: [
      ClassContentFormComponent
    ],
    exports: [ClassContentFormComponent]
})
export class ClassHelpContentFormModule {}
