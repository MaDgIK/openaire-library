import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {UntypedFormBuilder, UntypedFormControl, UntypedFormGroup} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {HelpContentService} from '../../services/help-content.service';
import {PageHelpContentFilterOptions} from '../../utils/entities/adminTool/page-help-content';
import {Page} from '../../utils/entities/adminTool/page';
import {Portal} from '../../utils/entities/adminTool/portal';
import {EnvProperties} from '../../utils/properties/env-properties';
import {Session} from '../../login/utils/helper.class';
import {LoginErrorCodes} from '../../login/utils/guardHelper.class';
import {HelperFunctions} from '../../utils/HelperFunctions.class';
import {Subscriber} from 'rxjs';
import {properties} from '../../../../environments/environment';
import {DomSanitizer} from '@angular/platform-browser';
import {SearchInputComponent} from '../../sharedComponents/search-input/search-input.component';
import {CheckDivHelpContent, DivHelpContent} from '../../utils/entities/adminTool/div-help-content';
import {ClearCacheService} from "../../services/clear-cache.service";
import {NotificationHandler} from "../../utils/notification-handler";

@Component({
  selector: 'class-help-contents',
  templateUrl: './class-help-contents.component.html',
})
export class ClassHelpContentsComponent implements OnInit {
  @ViewChild('AlertModalDeletePageHelpContents') alertModalDeletePageHelpContents;
  private selectedPageContents: string[] = [];
  public checkboxes: CheckDivHelpContent[] = [];
  public divHelpContents: DivHelpContent[] = [];
  public formGroup: UntypedFormGroup;
  public pages: Page[];
  public checkboxAll: boolean = false;
  public filters: PageHelpContentFilterOptions = {id: '', active: null, text: new RegExp('')};
  public keyword: string = '';
  public counter = {all: 0, active: 0, inactive: 0};
  public communities: Portal[] = [];
  public portal: string;
  public selectedPageId: string;
  public community: Portal;
  public page: Page;
  public properties: EnvProperties = properties;
  public showLoading: boolean = true;
  public filterForm: UntypedFormControl;
  private subscriptions: any[] = [];
  
  constructor(private element: ElementRef, private route: ActivatedRoute, private router: Router,
              private _helpService: HelpContentService, private _fb: UntypedFormBuilder, private sanitizer: DomSanitizer,
              private _clearCacheService: ClearCacheService) {
  }
  
  ngOnInit() {
    this.filterForm = this._fb.control('');
    this.subscriptions.push(this.filterForm.valueChanges.subscribe(value => {
      this.filterBySearch(value);
    }));
    this.subscriptions.push(this.route.params.subscribe(params => {
      this.portal = (this.route.snapshot.data.portal) ? this.route.snapshot.data.portal : this.route.snapshot.params[this.route.snapshot.data.param];
      this.subscriptions.push(this.route.queryParams.subscribe(params => {
        HelperFunctions.scroll();
        this.selectedPageId = params['pageId'];
        if (this.portal && this.selectedPageId) {
          this.getPage(this.selectedPageId);
        }
        if (!this.selectedPageId) {
          this.router.navigate(['../pages'], {relativeTo: this.route});
        }
      }));
    }));
  }
  
  ngOnDestroy(): void {
    this.subscriptions.forEach(value => {
      if (value instanceof Subscriber) {
        value.unsubscribe();
      } else if (value instanceof Function) {
        value();
      }
    });
  }
  
  getPage(pageId: string) {
    this.showLoading = true;
    this.subscriptions.push(this._helpService.getPageByPortal(pageId, this.portal).subscribe(
      page => {
        if (this.properties.adminToolsPortalType != page.portalType) {
          this.router.navigate(['./pageContents'], {queryParams: {'communityId': this.portal}});
        } else {
          this.page = page;
          this.getPageHelpContents(this.portal);
        }
      },
      error => this.handleError('System error retrieving page', error)));
  }
  
  
  public countClassHelpContents() {
    this.counter = {all: 0, active: 0, inactive: 0};
    let filter = Object.assign({}, this.filters);
    filter.active = null;
    this.divHelpContents.forEach(_ => {
      if (this.filterPageHelpContent(_, filter)) {
        if (_.isActive == true) {
          this.counter.active++;
        } else {
          this.counter.inactive++;
        }
      }
    });
    this.counter.all = this.counter.active + this.counter.inactive;
  }
  
  getPageHelpContents(community_pid: string) {
    this.subscriptions.push(this._helpService.getCommunityDivHelpContents(community_pid, this.selectedPageId).subscribe(
      pageHelpContents => {
        this.divHelpContents = pageHelpContents as Array<DivHelpContent>;
        this.counter.all = this.divHelpContents.length;
        this.checkboxes = [];
        
        for (let i = this.divHelpContents.length - 1; i >= 0; i -= 1) {
          this.checkboxes.unshift(<CheckDivHelpContent>{divHelpContent: this.divHelpContents[i], checked: false});
        }
        
        this.countClassHelpContents();
        
        this.showLoading = false;
      },
      error => this.handleError('System error retrieving page contents', error)));
  }
  
  public applyCheck(flag: boolean) {
    this.checkboxes.forEach(_ => _.checked = flag);
    this.checkboxAll = false;
  }
  
  public getSelectedPageHelpContents(): string[] {
    return this.checkboxes.filter(pageHelpContent => pageHelpContent.checked == true)
      .map(checkedPageHelpContent => checkedPageHelpContent.divHelpContent).map(res => res._id);
  }
  
  public confirmDeletePageHelpContent(id: string) {
    this.selectedPageContents = [id];
    this.confirmModalOpen();
  }
  
  private confirmModalOpen() {
    this.alertModalDeletePageHelpContents.alertTitle = 'Delete Confirmation';
    this.alertModalDeletePageHelpContents.message = 'Are you sure you want to delete the selected class help text?';
    this.alertModalDeletePageHelpContents.okButtonText = 'Yes';
    this.alertModalDeletePageHelpContents.open();
  }
  
  public confirmedDeletePageHelpContents(data: any) {
    this.showLoading = true;
    this.subscriptions.push(this._helpService.deleteDivHelpContents(this.selectedPageContents, this.portal).subscribe(
      _ => {
        this.deletePageHelpContentsFromArray(this.selectedPageContents);
        NotificationHandler.rise('Page content(s) has been <b>successfully deleted</b>');
        this.showLoading = false;
        this._clearCacheService.purgeBrowserCache("Class text texts deleted",this.portal);
      },
      error => this.handleUpdateError('System error deleting the selected page content(s)', error)
    ));
  }
  
  private deletePageHelpContentsFromArray(ids: string[]): void {
    for (let id of ids) {
      let i = this.checkboxes.findIndex(_ => _.divHelpContent._id == id);
      let j = this.divHelpContents.findIndex(_ => _._id == id);
      this.checkboxes.splice(i, 1);
      this.divHelpContents.splice(j, 1);
    }
    this.countClassHelpContents();
  }
  
  public editPageHelpContent(id: string) {
    if (this.selectedPageId) {
      this.router.navigate(['edit/'], {
        queryParams: {
          'pageContentId': id,
          'pageId': this.selectedPageId
        }, relativeTo: this.route
      });
    } else {
      this.router.navigate(['edit/'], {
        queryParams: {
          'pageContentId': id,
        }, relativeTo: this.route
      });
    }
  }
  
  public togglePageHelpContents(status: boolean, ids: string[]) {
    this.subscriptions.push(this._helpService.toggleDivHelpContents(ids, status, this.portal).subscribe(
      () => {
        for (let id of ids) {
          let i = this.checkboxes.findIndex(_ => _.divHelpContent._id == id);
          this.checkboxes[i].divHelpContent.isActive = status;
        }
        this.countClassHelpContents();
        this.applyCheck(false);
        NotificationHandler.rise('Page content(s) has been <b>successfully updated</b>');
        this._clearCacheService.purgeBrowserCache("class help text's status changed",this.portal);
      },
      error => this.handleUpdateError('System error changing the status of the selected page content(s)', error)
    ));
  }
  
  
  public filterPageHelpContent(divHelpContent: DivHelpContent, filters: PageHelpContentFilterOptions): boolean {
    let activeFlag = filters.active == null || divHelpContent.isActive == filters.active;
    let textFlag = filters.text.toString() == '' || (divHelpContent.content && divHelpContent.content.match(filters.text) != null);
    return activeFlag && textFlag;
  }
  
  
  public applyFilter() {
    this.checkboxes = [];
    this.divHelpContents.filter(item => this.filterPageHelpContent(item, this.filters)).forEach(
      _ => {
        this.checkboxes.push(<CheckDivHelpContent>{divHelpContent: _, checked: false});
      }
    );
  }
  
  public filterBySearch(text: string) {
    this.filters.text = new RegExp(text, 'i');
    this.applyFilter();
  }
  
  handleError(message: string, error = null) {
    if(error) {
      console.log('Server responded: ' + error);
    }
    NotificationHandler.rise(message, 'danger');
    this.showLoading = false;
  }
  
  handleUpdateError(message: string, error = null) {
    if(error) {
      console.log('Server responded: ' + error);
    }
    NotificationHandler.rise(message, 'danger');
    this.showLoading = false;
  }
  
  public newPageContent() {
    this.router.navigate(['edit'], {
      queryParams: {
        pageId: this.selectedPageId
      }, relativeTo: this.route
    });
  }
  
  selectAll() {
    let checked = (this.getSelectedPageHelpContents().length != this.checkboxes.length);
    for (let check of this.checkboxes) {
      check.checked = checked;
    }
  }
}
