import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PluginEditWrapperComponent} from "./plugin-edit-wrapper.component";
import {PluginOpenaireProductsFormComponent} from "../components/openaireProducts/plugin-openaire-products.form.component";
import {PluginFieldEditModule} from "../utils/plugin-field-edit.module";
import {PluginDiscoverBySubcommunityFormComponent} from "../components/discover-by-subcommunity/plugin-discover-by-subcommunity.form.component";
import {PluginFeaturedDatasetsFormComponent} from "../components/featured-datasets/plugin-featured-datasets.form.component";
import {PluginGatewayInformationFormComponent} from "../components/gateway-information/plugin-gateway-information.form.component";
import {PluginSearchDepositLinkFormComponent} from "../components/search-deposit-link/plugin-search-deposit-link.form.component";
import {PluginOrganizationsFormComponent} from "../components/organizations/plugin-organizations.form.component";
import {IconsModule} from "../../../utils/icons/icons.module";
import {PluginSuggestedRepositoriesFormComponent} from "../components/suggested-repositories/plugin-suggested-repositories.form.component";
import {PluginGraphInfoFormComponent} from "../components/paragraph-info/plugin-graph-info.form.component";
import {PluginStatsFormComponent} from "../components/stats/plugin-stats.form.component";
import {PluginSearchBarFormComponent} from "../components/search-bar/plugin-search-bar.form.component";
import {PluginCardInfoFormComponent} from "../components/card-info/plugin-card-info-form.component";
import {CKEditorModule} from "ng2-ckeditor";
import {InputModule} from "../../../sharedComponents/input/input.module";
import {PluginHtmlSectionFormComponent} from "../components/html-section/plugin-html-section.form.component";

@NgModule({
  imports: [
    CommonModule, RouterModule,
    ReactiveFormsModule, InputModule,
    FormsModule, PluginFieldEditModule, IconsModule, CKEditorModule,
  ],
  declarations: [PluginEditWrapperComponent, PluginOpenaireProductsFormComponent, PluginDiscoverBySubcommunityFormComponent, PluginDiscoverBySubcommunityFormComponent, PluginDiscoverBySubcommunityFormComponent, PluginDiscoverBySubcommunityFormComponent, PluginDiscoverBySubcommunityFormComponent, PluginDiscoverBySubcommunityFormComponent, PluginFeaturedDatasetsFormComponent, PluginGatewayInformationFormComponent, PluginSearchDepositLinkFormComponent, PluginOrganizationsFormComponent, PluginSuggestedRepositoriesFormComponent, PluginGraphInfoFormComponent, PluginStatsFormComponent, PluginSearchBarFormComponent, PluginCardInfoFormComponent, PluginHtmlSectionFormComponent],
  exports: [PluginEditWrapperComponent]

})
export class PluginEditWrapperModule {

}
