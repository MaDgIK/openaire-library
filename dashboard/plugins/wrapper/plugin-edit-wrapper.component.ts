import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Plugin} from "../../../utils/entities/adminTool/plugin";
import {PluginTemplate} from "../../../utils/entities/adminTool/pluginTemplate";
import {PluginEditEvent} from "../utils/base-plugin.form.component";
import {PluginUtils} from "../utils/pluginUtils";

@Component({
  selector: 'plugin-wrapper-form',
  template: `
        <div class="uk-margin-small-right uk-margin-top" *ngIf="pluginObject">
          <ng-container *ngIf="pluginTemplate.code  && pluginUtils.availablePluginCodes.indexOf(pluginTemplate.code) != -1; else noplugin">

          <ng-container *ngIf="pluginTemplate.code == 'openaire-products'">
          <plugin-openaire-products-form [pluginTemplate]="pluginTemplate" [plugin]="plugin" [pluginObject]="pluginObject" (valuesChanged)="changed.emit($event)" [editTemplate]="editTemplate"></plugin-openaire-products-form>
        </ng-container>
        <ng-container *ngIf="pluginTemplate.code == 'discover-by-subcommunity'">
          <plugin-discover-by-subcommunity-form [plugin]="plugin" [pluginTemplate]="pluginTemplate" [pluginObject]="pluginObject"  (valuesChanged)="changed.emit($event)" [editTemplate]="editTemplate"></plugin-discover-by-subcommunity-form>
        </ng-container>
        <ng-container *ngIf="pluginTemplate.code == 'gateway-information'">
          <plugin-gateway-information-form [plugin]="plugin" [pluginTemplate]="pluginTemplate" [pluginObject]="pluginObject"  (valuesChanged)="changed.emit($event)" [editTemplate]="editTemplate"></plugin-gateway-information-form>
        </ng-container>
        <ng-container *ngIf="pluginTemplate.code == 'search-deposit-link'">
          <plugin-search-deposit-link-form [plugin]="plugin" [pluginTemplate]="pluginTemplate" [pluginObject]="pluginObject"  (valuesChanged)="changed.emit($event)" [editTemplate]="editTemplate" [editSubmenuOpen]="editSubmenuOpen"></plugin-search-deposit-link-form>
        </ng-container>
        <ng-container *ngIf="pluginTemplate.code == 'suggested-repositories'">
          <plugin-suggested-repositories-form [plugin]="plugin" [pluginTemplate]="pluginTemplate" [pluginObject]="pluginObject"  (valuesChanged)="changed.emit($event)" [editTemplate]="editTemplate"></plugin-suggested-repositories-form>
        </ng-container>
        <ng-container *ngIf="pluginTemplate.code == 'featured-datasets'">
          <plugin-featured-datasets-form [plugin]="plugin" [pluginTemplate]="pluginTemplate" [pluginObject]="pluginObject"  (valuesChanged)="changed.emit($event)" [editTemplate]="editTemplate"></plugin-featured-datasets-form>
        </ng-container>
          <ng-container *ngIf="pluginTemplate.code == 'organizations'">
            <plugin-organizations-form [plugin]="plugin" [pluginTemplate]="pluginTemplate" [pluginObject]="pluginObject"  (valuesChanged)="changed.emit($event)" [editTemplate]="editTemplate"></plugin-organizations-form>
          </ng-container>
          <ng-container *ngIf="pluginTemplate.code == 'graph-info'  || pluginTemplate.code == 'paragraph-info'">
            <plugin-graph-info-form [plugin]="plugin" [pluginTemplate]="pluginTemplate" [pluginObject]="pluginObject"  (valuesChanged)="changed.emit($event)" [editTemplate]="editTemplate"></plugin-graph-info-form>
          </ng-container>
            <ng-container *ngIf="pluginTemplate.code == 'stats'">
              <plugin-stats-form [plugin]="plugin" [pluginTemplate]="pluginTemplate" [pluginObject]="pluginObject"  (valuesChanged)="changed.emit($event)" [editTemplate]="editTemplate"></plugin-stats-form>
            </ng-container>
            <ng-container *ngIf="pluginTemplate.code == 'search-bar'">
              <plugin-search-bar-form [plugin]="plugin" [pluginTemplate]="pluginTemplate" [pluginObject]="pluginObject"  (valuesChanged)="changed.emit($event)" [editTemplate]="editTemplate"></plugin-search-bar-form>
            </ng-container>
            <ng-container *ngIf="pluginTemplate.code == 'card-info' || pluginTemplate.code == 'learn-and-connect' || pluginTemplate.code == 'how-to-use'">
              <plugin-card-info-form [plugin]="plugin" [pluginTemplate]="pluginTemplate" [pluginObject]="pluginObject"  (valuesChanged)="changed.emit($event)" [editTemplate]="editTemplate" [editSubmenuOpen]="editSubmenuOpen"></plugin-card-info-form>
            </ng-container>
            <ng-container *ngIf="pluginTemplate.code == 'html-section'">
              <plugin-html-section-form [plugin]="plugin" [pluginTemplate]="pluginTemplate" [pluginObject]="pluginObject"  (valuesChanged)="changed.emit($event)" [editTemplate]="editTemplate"></plugin-html-section-form>
            </ng-container>
        </ng-container>
          <ng-template #noplugin>
            <div   class="uk-text-muted uk-text-center">
              No plugin available
            </div>
          </ng-template>
        </div>
      
  `,
  styleUrls: ["edit-plugin.css"]
})
export class PluginEditWrapperComponent implements OnInit {

  @Input() plugin:Plugin;
  @Input() pluginTemplate:PluginTemplate;
  @Input() editTemplate:boolean;
  @Input() pluginObject;
  @Output() changed:EventEmitter<PluginEditEvent> = new EventEmitter();
  @Input() editSubmenuOpen;
  pluginUtils = new PluginUtils();

  ngOnInit(): void {
  }

}
