import {Component, Input, OnInit} from '@angular/core';
import {Plugin} from "../../../utils/entities/adminTool/plugin";
import {PluginTemplate} from "../../../utils/entities/adminTool/pluginTemplate";
import {PluginUtils} from "../utils/pluginUtils";

@Component({
  selector: 'plugin-wrapper',
  template: `
    <ng-container *ngIf="pluginTemplate && pluginObject">
      <div *ngIf="pluginTemplate.code  && pluginUtils.availablePluginCodes.indexOf(pluginTemplate.code) != -1; else noplugin">
        <ng-container *ngIf="pluginTemplate.code == 'openaire-products'">
          <plugin-openaire-products [plugin]="plugin" [pluginTemplate]="pluginTemplate" [pluginObject]="pluginObject"  [previewInAdmin]="previewInAdmin" ></plugin-openaire-products>
        </ng-container>
        <ng-container *ngIf="pluginTemplate.code == 'discover-by-subcommunity'">
          <plugin-discover-by-subcommunity [plugin]="plugin" [pluginTemplate]="pluginTemplate" [pluginObject]="pluginObject"  [previewInAdmin]="previewInAdmin" ></plugin-discover-by-subcommunity>
        </ng-container>
        <ng-container *ngIf="pluginTemplate.code == 'gateway-information'">
          <plugin-gateway-information [plugin]="plugin" [pluginTemplate]="pluginTemplate" [pluginObject]="pluginObject"  [previewInAdmin]="previewInAdmin" ></plugin-gateway-information>
        </ng-container>
        <ng-container *ngIf="pluginTemplate.code == 'search-deposit-link'">
          <plugin-search-deposit-link [plugin]="plugin" [pluginTemplate]="pluginTemplate" [pluginObject]="pluginObject"  [previewInAdmin]="previewInAdmin" ></plugin-search-deposit-link>
        </ng-container>
        <ng-container *ngIf="pluginTemplate.code == 'suggested-repositories'">
          <plugin-suggested-repositories [plugin]="plugin" [pluginTemplate]="pluginTemplate" [pluginObject]="pluginObject"  [previewInAdmin]="previewInAdmin" ></plugin-suggested-repositories>
        </ng-container>
        <ng-container *ngIf="pluginTemplate.code == 'featured-datasets'">
          <plugin-featured-datasets [plugin]="plugin" [pluginTemplate]="pluginTemplate" [pluginObject]="pluginObject"  [previewInAdmin]="previewInAdmin" ></plugin-featured-datasets>
        </ng-container>
        <ng-container *ngIf="pluginTemplate.code == 'organizations'">
          <plugin-organizations [plugin]="plugin" [pluginTemplate]="pluginTemplate" [pluginObject]="pluginObject"  [previewInAdmin]="previewInAdmin" ></plugin-organizations>
        </ng-container>
        <ng-container *ngIf="pluginTemplate.code == 'graph-info' || pluginTemplate.code == 'paragraph-info'">
          <plugin-graph-info [plugin]="plugin" [pluginTemplate]="pluginTemplate" [pluginObject]="pluginObject"  [previewInAdmin]="previewInAdmin" ></plugin-graph-info>
        </ng-container>
        <ng-container *ngIf="pluginTemplate.code == 'stats'">
          <plugin-stats [plugin]="plugin" [pluginTemplate]="pluginTemplate" [pluginObject]="pluginObject"   [previewInAdmin]="previewInAdmin" ></plugin-stats>
        </ng-container>
        <ng-container *ngIf="pluginTemplate.code == 'search-bar'">
          <plugin-search-bar [plugin]="plugin" [pluginTemplate]="pluginTemplate" [pluginObject]="pluginObject"   [previewInAdmin]="previewInAdmin" ></plugin-search-bar>
        </ng-container>
        <ng-container *ngIf="pluginTemplate.code == 'card-info' || pluginTemplate.code == 'learn-and-connect' || pluginTemplate.code == 'how-to-use'">
          <plugin-card-info [plugin]="plugin" [pluginTemplate]="pluginTemplate" [pluginObject]="pluginObject"   [previewInAdmin]="previewInAdmin" ></plugin-card-info>
        </ng-container>
        <ng-container *ngIf="pluginTemplate.code == 'html-section'">
          <plugin-html-section [plugin]="plugin" [pluginTemplate]="pluginTemplate" [pluginObject]="pluginObject"   [previewInAdmin]="previewInAdmin" ></plugin-html-section>
        </ng-container>
      </div>
      <ng-template #noplugin>
        <div   class="uk-text-muted uk-text-center">
          No plugin available
        </div>
      </ng-template>
    </ng-container>
  `
})
export class PluginWrapperComponent implements OnInit {
  @Input() plugin:Plugin;
  @Input() pluginObject;
  @Input() pluginTemplate:PluginTemplate;
  @Input() previewInAdmin:boolean = false;
  pluginUtils = new PluginUtils();
  ngOnInit(): void {

  }
}
