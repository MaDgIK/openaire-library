import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {PluginWrapperComponent} from "./plugin-wrapper.component";
import {PluginOpenaireProductsModule} from "../components/openaireProducts/plugin-openaire-products.module";
import {PluginDiscoverBySubcommunityModule} from '../components/discover-by-subcommunity/plugin-discover-by-subcommunity.module';
import {PluginGatewayInformationModule} from '../components/gateway-information/plugin-gateway-information.module';
import {PluginSearchDepositLinkModule} from '../components/search-deposit-link/plugin-search-deposit-link.module';
import {PluginSuggestedRepositoriesModule} from '../components/suggested-repositories/plugin-suggested-repositories.module';
import {PluginFeaturedDatasetsModule} from '../components/featured-datasets/plugin-featured-datasets.module';
import {PluginOrganizationsModule} from "../components/organizations/plugin-organizations.module";
import {PluginStatsModule} from "../components/stats/plugin-stats.module";
import {PluginSearchBarModule} from "../components/search-bar/plugin-search-bar.module";
import {PluginCardInfoModule} from "../components/card-info/plugin-card-info.module";
import {PluginHtmlSectionModule} from "../components/html-section/plugin-html-section.module";
import {PluginGraphInfoModule} from "../components/paragraph-info/plugin-graph-info.module";

@NgModule({
  imports: [
    CommonModule, RouterModule, FormsModule, PluginOpenaireProductsModule,
    PluginDiscoverBySubcommunityModule, PluginSearchDepositLinkModule, PluginSuggestedRepositoriesModule, PluginFeaturedDatasetsModule, PluginGatewayInformationModule, PluginOrganizationsModule, PluginHtmlSectionModule, PluginStatsModule, PluginSearchBarModule, PluginCardInfoModule, PluginGraphInfoModule
  ],
  declarations: [PluginWrapperComponent],
  exports: [PluginWrapperComponent]

})
export class PluginWrapperModule {

}
