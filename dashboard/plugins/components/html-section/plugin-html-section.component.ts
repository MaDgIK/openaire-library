import {Component} from '@angular/core';
import {PluginBaseComponent, PluginBaseInfo, PluginURL} from "../../utils/base-plugin.component";

export class HTMLSection extends PluginBaseInfo{
  title:string ="Lorem ipsum"
  html:string = `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`;
  fullWidth:boolean = true;
  compare(oldObject): any {
    let newObj=   super.compare(oldObject);
    return newObj;
  }
}
@Component({
  selector: 'plugin-html-section',
  template: `
    <div [class]="pluginObject.fullWidth?'':'uk-container uk-container-large'">
      <h2>{{pluginObject.title}}</h2>
      <div  [innerHTML]="pluginObject.html">
      </div>
    </div>
  `
})
export class PluginHtmlSectionComponent extends PluginBaseComponent<HTMLSection>{
  constructor() {
    super();
  }

}
