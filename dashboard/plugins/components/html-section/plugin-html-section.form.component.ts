import {Component} from '@angular/core';
import {PluginBaseFormComponent, PluginEditEvent} from "../../utils/base-plugin.form.component";
import {HTMLSection} from "./plugin-html-section.component";

@Component({
  selector: 'plugin-html-section-form',
  template: `
    <div *ngIf="pluginObject" class="uk-padding-xsmall">
      <plugin-field-edit [value]="pluginObject.title"
                         type="text" field="title" (changed)="valueChanged($event)"></plugin-field-edit>
      
      <div class="uk-margin-top">
        <plugin-field-edit [value]="pluginObject.html" [switchToHTMLEditor]="true"
                           type="textarea" field="html" (changed)="valueChanged($event)"></plugin-field-edit>
      </div>
      <div class="uk-margin-xsmall-bottom uk-margin-xsmall-top">
        <plugin-field-edit [value]=" pluginObject.fullWidth"
                           type="checkbox" field="fullWidth" (editClicked)="pluginEditEvent = $event"
                           (changed)="valueChanged($event)">
        </plugin-field-edit>Full width
      </div>
    </div>
      
  `,


})
export class PluginHtmlSectionFormComponent extends PluginBaseFormComponent<HTMLSection> {
  constructor() {
    super()

  }
}
