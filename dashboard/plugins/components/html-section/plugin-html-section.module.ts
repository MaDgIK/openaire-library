import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {PluginsService} from "../../../../services/plugins.service";
import {PluginFieldEditModule} from "../../utils/plugin-field-edit.module";
import {InputModule} from "../../../../sharedComponents/input/input.module";
import {PluginHtmlSectionComponent} from "./plugin-html-section.component";

@NgModule({
  imports: [
    CommonModule, RouterModule, FormsModule, PluginFieldEditModule, InputModule
  ],
  providers: [
    PluginsService
  ],
  declarations: [PluginHtmlSectionComponent],
  exports: [PluginHtmlSectionComponent]
})
export class PluginHtmlSectionModule {

}
