import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {PluginsService} from "../../../../services/plugins.service";
import {PluginFieldEditModule} from "../../utils/plugin-field-edit.module";
import {InputModule} from "../../../../sharedComponents/input/input.module";
import {PluginStatsComponent} from "./plugin-stats.component";
import {MonitorModule} from "./monitor.module";

@NgModule({
  imports: [
    CommonModule, RouterModule, FormsModule, PluginFieldEditModule, InputModule, MonitorModule
  ],
  providers: [
    PluginsService
  ],
  declarations: [PluginStatsComponent],
  exports: [PluginStatsComponent]
})
export class PluginStatsModule {

}
