import {ChangeDetectorRef, Component, Input} from "@angular/core";
import {MonitorIndicatorStakeholderBaseComponent} from "src/app/openaireLibrary/monitor/monitor-indicator-stakeholder-base.component";
import {map} from "rxjs/operators";
import {StatisticsService} from "../../../../monitor-admin/utils/services/statistics.service";
import {ActivatedRoute, Router} from "@angular/router";
import {PiwikService} from "../../../../utils/piwik/piwik.service";
import {DomSanitizer, Meta, Title} from "@angular/platform-browser";
import {SEOService} from "../../../../sharedComponents/SEO/SEO.service";
import {SearchResearchResultsService} from "../../../../services/searchResearchResults.service";
import {HttpClient} from "@angular/common/http";
import {IndicatorPath, Section, Visibility} from "../../../../monitor/entities/stakeholder";
import {LayoutService} from "../../../sharedComponents/sidebar/layout.service";
import {CommunityService} from "../../../../connect/community/community.service";
import {PluginStats} from "./plugin-stats.component";

@Component({
  selector: 'plugin-stats-monitor',
  templateUrl: 'monitor.component.html'
})
export class MonitorComponent extends MonitorIndicatorStakeholderBaseComponent {
  activeChartSectionIndex: number = 0;
  stakeholder = null;
  @Input() pluginObject: PluginStats;
  profiles;
  constructor(protected _route: ActivatedRoute,
              protected _router: Router,
              protected _meta: Meta,
              protected _title: Title,
              protected _piwikService: PiwikService,
              protected seoService: SEOService,
              protected sanitizer: DomSanitizer,
              protected cdr: ChangeDetectorRef,
              protected layoutService: LayoutService,
              protected statisticsService: StatisticsService,
              protected searchResearchResultsService: SearchResearchResultsService,
              private communityService: CommunityService,
              private http: HttpClient) {
    super();
  }


  ngOnInit() {
    super.ngOnInit();
    this.requireLogin = false;
    this.subscriptions.push(this.communityService.getCommunityAsObservable().subscribe(communityInfo => {

      if (communityInfo) {
        this.subscriptions.push(this.http.get(this.properties.monitorStatsFrameUrl+"/schema/profiles").subscribe((profiles:any[]) => {
          this.profiles = profiles.map( (profile) => profile.name);
          let profile = PluginStats.getDefaultProfile();
          if(this.profiles.indexOf("monitor_"+communityInfo.communityId.replace("-","_"))!=-1){
            profile = this.profiles[this.profiles.indexOf("monitor_"+communityInfo.communityId.replace("-","_"))];
          }else if(communityInfo.type == "ri" && this.profiles.indexOf("monitor_ris_tail")!=-1){
            profile = "monitor_ris_tail";
          }
          this.init(communityInfo, profile);
        }, error => {
          let profile =
          this.init(communityInfo, PluginStats.getDefaultProfile());
        }))


      }
    }));
  }
  public init(communityInfo, profile){
    console.log(profile)
    this.loading = true;
    this.stakeholder = PluginStats.getMockStakeholder();
    this.stakeholder.statsProfile = profile;
    this.stakeholder.index_id = communityInfo.communityId
    this.stakeholder.index_name = communityInfo.title;
    this.stakeholder.index_shortName = communityInfo.shortTitle;
    this.title = this.stakeholder.name;
    this.description = this.stakeholder.name;
    this.loading = true;
    this.activeTopic = null;
    this.activeCategory = null;
    this.activeSubCategory = null;
    this.numberResults = new Map<string, number>();
    this.setView({});
    let ids = [];
    for (let section of this.activeSubCategory.charts) {
      for (let indicator of section.indicators) {
        ids.push(indicator._id)
      }
    }
  }
  public showSection(section: Section): boolean {
    for (let indicator of section.indicators) {
      if (this.showIndicator(indicator)) {
        return true;
      }
    }
    return false;

  }

  public showIndicator(indicator): boolean {
    return this.pluginObject.disabledIndicators.indexOf(indicator._id) == -1;
  }
}
