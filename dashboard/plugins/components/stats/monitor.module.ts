import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {MonitorComponent} from "./monitor.component";
import {PageContentModule} from "../../../../dashboard/sharedComponents/page-content/page-content.module";
import {SliderTabsModule} from "../../../../sharedComponents/tabs/slider-tabs.module";
import {NumberRoundModule} from "../../../../utils/pipes/number-round.module";
import {IconsModule} from "../../../../utils/icons/icons.module";
import {ClickModule} from "../../../../utils/click/click.module";
import {RangeFilterModule} from "../../../../utils/rangeFilter/rangeFilter.module";
import {SearchFilterModule} from "../../../../searchPages/searchUtils/searchFilter.module";
import {IconsService} from "../../../../utils/icons/icons.service";
import {filters} from "../../../../utils/icons/icons";
import {SliderUtilsModule} from "../../../../sharedComponents/slider-utils/slider-utils.module";
import {LoadingModule} from "../../../../utils/loading/loading.module";


@NgModule({
  imports: [CommonModule, PageContentModule, SliderTabsModule, NumberRoundModule, IconsModule, ClickModule, RangeFilterModule, SearchFilterModule, SliderUtilsModule, LoadingModule],
  declarations: [MonitorComponent],
  exports: [
    MonitorComponent
  ]
})
export class MonitorModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([ filters]);
  }
}
