import {Component} from '@angular/core';
import {PluginBaseFormComponent, PluginEditEvent} from "../../utils/base-plugin.form.component";
import {PluginStats} from "./plugin-stats.component";

@Component({
  selector: 'plugin-stats-form',
  template: `
    <div *ngIf="pluginObject" class="uk-padding-xsmall">
      <plugin-field-edit [value]="pluginObject.title"
                         type="text" field="title" (changed)="valueChanged($event)"></plugin-field-edit>
      <div class=" uk-margin-top uk-text-meta uk-text-xsmall">External Link</div>
      <div class="uk-margin-small-top">
      <plugin-field-edit [value]="pluginObject.url.url"
                         type="text" field="url" (changed)="urlValueChanged($event)"></plugin-field-edit>
      </div>
      <div class="uk-margin-small-top uk-margin-small-top">
      <plugin-field-edit [value]="pluginObject.url.linkText"
                         type="text" field="linkText" (changed)="urlValueChanged($event)"></plugin-field-edit>
      </div>
<!--      <div class=" uk-margin-top uk-text-meta uk-text-xsmall"> Number indicators</div>-->
       
<!--      <ng-container *ngFor="let number of stakeholder.topics[0].category[0].subcategory[0].numbers">-->

<!--        <div class="uk-grid uk-grid-small uk-margin-xsmall-top">-->
<!--          <div class="uk-text-small uk-width-3-4">{{service.name}}</div>-->
<!--          <div class=" uk-width-1-4">-->
<!--            <plugin-field-edit  [value]=" pluginObject.serviceIdsArray.indexOf(service.id)!=-1"-->
<!--                                type="boolean" field="serviceIdsArray"    (changed)="serviceChanged(service.id,$event)" >-->
<!--            </plugin-field-edit>-->
<!--          </div>-->
<!--        </div>-->
<!--      </ng-container>-->

      <div class=" uk-margin-medium-top uk-text-meta uk-text-xsmall"> Chart indicators</div>
      <ng-container *ngFor="let section of stakeholder.topics[0].categories[0].subCategories[0].charts">

        <ng-container *ngFor="let indicator of section.indicators">
          <div class=" uk-margin-xsmall-top">
            <plugin-field-edit [value]=" pluginObject.disabledIndicators.indexOf(indicator._id)==-1"
                               type="checkbox" field="sdgs" (editClicked)="pluginEditEvent = $event"
                               (changed)="indicatorsChanged(indicator._id,$event)">
            </plugin-field-edit>
            {{decode(indicator.indicatorPaths[0].name?indicator.name:(indicator.indicatorPaths[0].parameters['title'] ))}}
          <span class=" uk-text-xsmall"> 
            {{ decode(indicator.indicatorPaths[0].parameters['subtitle']? ' ' + indicator.indicatorPaths[0].parameters['subtitle']:'')}}</span>
         
        </div>
        </ng-container>
      </ng-container>
    </div>     
<!-- -->  `,


})
export class PluginStatsFormComponent extends PluginBaseFormComponent<PluginStats> {
  stakeholder = PluginStats.getMockStakeholder();
  constructor() {
    super()
  }

  indicatorsChanged(id,$event:PluginEditEvent ){
    console.log("before", id,this.pluginObject.disabledIndicators)
    let index = this.pluginObject.disabledIndicators.indexOf(id);
    if(index !=-1){
      this.pluginObject.disabledIndicators.splice(index,1);
    }else{
      this.pluginObject.disabledIndicators.push(id);
    }
    $event.value =this.pluginObject.disabledIndicators;
    console.log("after",this.pluginObject.disabledIndicators)
    this.valuesChanged.emit({field:$event.field, value: $event.value, type: 'parent'})
  }

  urlValueChanged($event:PluginEditEvent){
    console.log($event.field,$event.value)
    this.pluginObject['url'][$event.field]=$event.value;
    $event.field = "url";
    $event.value = this.pluginObject['url'];
    this.valuesChanged.emit($event)
  }
  decode(text){
    return text? decodeURIComponent(text):"";
  }
}
