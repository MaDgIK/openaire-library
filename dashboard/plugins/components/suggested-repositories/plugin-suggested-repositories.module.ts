import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {PluginsService} from "../../../../services/plugins.service";
import {IconsModule} from "../../../../utils/icons/icons.module";
import {NumberRoundModule} from "../../../../utils/pipes/number-round.module";
import {IconsService} from "../../../../utils/icons/icons.service";
import {SearchResearchResultsServiceModule} from "../../../../services/searchResearchResultsService.module";
import {PluginFieldEditModule} from "../../utils/plugin-field-edit.module";
import {PluginSuggestedRepositoriesComponent} from './plugin-suggested-repositories.component';

@NgModule({
  imports: [
    CommonModule, RouterModule, FormsModule, IconsModule, NumberRoundModule, SearchResearchResultsServiceModule, PluginFieldEditModule
  ],
  providers:[PluginsService],
  declarations: [PluginSuggestedRepositoriesComponent],
  exports: [PluginSuggestedRepositoriesComponent]
})
export class PluginSuggestedRepositoriesModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([])
  }
}
