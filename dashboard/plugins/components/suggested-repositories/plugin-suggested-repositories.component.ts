import {Component} from '@angular/core';
import {PluginBaseComponent, PluginBaseInfo} from "../../utils/base-plugin.component";
import {SearchCommunityDataprovidersService} from "../../../../connect/contentProviders/searchDataproviders.service";
import {ConfigurationService} from "../../../../utils/configuration/configuration.service";
import {CommunityService} from "../../../../connect/community/community.service";
export class PluginSuggestedRepositories extends PluginBaseInfo{
  title:string ="Suggested repositories & journals";
  description:string = "";
  compare(oldObject): any {
    return super.compare(oldObject);
  }
}
@Component({
  selector: 'plugin-suggested-repositories',
  templateUrl: 'plugin-suggested-repositories.component.html'
})
export class PluginSuggestedRepositoriesComponent extends PluginBaseComponent<PluginSuggestedRepositories>{
  portal;
  community;
  contentProviders;
  slides = 0;
  constructor(private searchCommunityDataprovidersService: SearchCommunityDataprovidersService,
              private config: ConfigurationService,
              private communityService: CommunityService) {
    super()
    this.subscriptions.push(this.config.portalAsObservable.subscribe(
      res => {
        this.portal = res;
      },
      error => {
        console.log(error);
      }
    ));
    this.subscriptions.push(this.communityService.getCommunityAsObservable().subscribe(
      community => {
        this.community = community;
        if (community) {
          this.subscriptions.push(this.searchCommunityDataprovidersService.searchDataproviders(this.properties, this.community.communityId, true).subscribe(
            res => {
              this.slides =1;
              this.contentProviders = res;
              if(this.contentProviders.length > 2){
                this.slides = parseInt('' + (this.contentProviders.length/2));
              }
              if(this.slides< (this.contentProviders.length / 2)){
                this.slides++;
              }
            },
            error => {
              console.log(error);
            }
          ));

        }
      }
    ));

  }
}
