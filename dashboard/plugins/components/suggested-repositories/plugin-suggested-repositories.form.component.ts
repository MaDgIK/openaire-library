import {Component} from '@angular/core';
import {PluginBaseFormComponent} from "../../utils/base-plugin.form.component";
import {PluginSuggestedRepositories} from "./plugin-suggested-repositories.component";

@Component({
  selector: 'plugin-suggested-repositories-form',
  template: `
    <div *ngIf="pluginObject" class="uk-padding-xsmall">
      <plugin-field-edit [value]="pluginObject.title"
                         type="text" field="title" (changed)="valueChanged($event)"></plugin-field-edit>
      <plugin-field-edit [value]="pluginObject.description"
                         type="textarea" field="description" (changed)="valueChanged($event)" [switchToHTMLEditor]="true"></plugin-field-edit>
      <div class="uk-alert uk-alert-warning uk-text-small"> Manage the content providers list in 
        <a routerLink="../../../config/content-providers" target="_blank">community content config</a> by adding them in deposit.</div>
    </div>
  `,


})
export class PluginSuggestedRepositoriesFormComponent extends PluginBaseFormComponent<PluginSuggestedRepositories> {
  constructor() {
    super()

  }

}
