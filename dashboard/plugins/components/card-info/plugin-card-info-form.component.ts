import {Component, Input, SimpleChanges} from '@angular/core';
import {PluginBaseFormComponent, PluginEditEvent} from "../../utils/base-plugin.form.component";
import {PluginCardInfo} from "./plugin-card-info.component";

@Component({
  selector: 'plugin-card-info-form',
  template: `
    <div *ngIf="pluginObject" class="uk-padding-xsmall">
      <ng-container *ngIf="selectedIndex == -1">
        <plugin-field-edit [value]=" pluginObject.title"
                           type="text" field="title" (changed)="valueChanged($event)"></plugin-field-edit>
        <plugin-field-edit [value]=" pluginObject.description"
                           type="textarea" field="description" (changed)="valueChanged($event)"></plugin-field-edit>
        <hr class="uk-margin-left">
        <div class="uk-margin-top uk-text-muted">
          Cards:
        </div>

        <ng-container *ngFor="let card of  pluginObject.cardInfoArray;  let i = index">
          <div class="uk-grid uk-grid-small uk-margin-xsmall-top">
            <div *ngIf="selectedIndex != i" class="uk-text-small uk-width-expand">
              <plugin-field-edit [value]=" pluginObject.cardInfoArray[i].show" type="checkbox" field="cardInfoArray"
                                 (editClicked)="pluginEditEvent = $event"
                                 (changed)="cardShowChanged(i,$event)"></plugin-field-edit>
              {{card.title}}
            </div>

            <div class="uk-padding-remove-left uk-margin-medium-right">
              <button
                  class="uk-button uk-button-link uk-flex uk-flex-middle" (click)="edit(i)">
                <icon name="edit" [flex]="true"></icon>
              </button>
            </div>
          </div>

        </ng-container>
      </ng-container>
      <ng-container *ngIf="selectedIndex > -1">
        <div *ngIf="editTemplate" class="back uk-margin-bottom">
          <a (click)="close()" class="uk-flex uk-flex-middle uk-flex-center">
            <div class="uk-width-auto">
              <icon name="west" ratio="1.3"
                    [flex]="true"></icon>
            </div>
            <span class="uk-text-small">Plugin Options</span>
          </a>

        </div>
        <!--<div class="uk-text-small">
          Enable
          <plugin-field-edit [value]=" pluginObject.cardInfoArray[selectedIndex].show"
                             type="boolean" field="cardInfoArray" (editClicked)="pluginEditEvent = $event"
                             (changed)="cardShowChanged(selectedIndex,$event)">

          </plugin-field-edit>
        </div>-->
        <div class="uk-margin-top">
          <plugin-field-edit [value]="pluginObject.cardInfoArray[selectedIndex].title"
                             type="text" field="title"
                             (changed)="cardValueChanged(selectedIndex, $event)"></plugin-field-edit>
        </div>
        <div class="uk-margin-top">
          <plugin-field-edit [value]="  pluginObject.cardInfoArray[selectedIndex].description"
                             type="textarea" field="description"
                             (changed)="cardValueChanged(selectedIndex, $event)"></plugin-field-edit>
        </div>
        <hr class="uk-margin-left">
        <div class="uk-margin-top" title="Use material icons to update the card icon">
          <div class="uk-width-1-1 uk-text-right">
            <a href="https://fonts.google.com/icons?icon.set=Material+Icons" target="_blank" class="uk-text-xsmall uk-text-right custom-external">More options</a>
          </div>
          <plugin-field-edit [value]="  pluginObject.cardInfoArray[selectedIndex].icon"
                             type="text" field="icon" placeholder="Material icon"
                             (changed)="cardValueChanged(selectedIndex, $event)"></plugin-field-edit>

        </div>
        <ng-container *ngFor="let cardUrl of pluginObject.cardInfoArray[selectedIndex].urlsArray; let j = index ">
          <div class=" uk-margin-top uk-text-meta uk-text-xsmall"> Link #{{j + 1}}</div>
          <div class="uk-margin-small-top">
            <plugin-field-edit [value]=" cardUrl.url"
                               type="text" field="url"
                               (changed)="cardUrlValueChanged(selectedIndex, j, $event)"></plugin-field-edit>
          </div>
          <div class="uk-margin-top">
            <plugin-field-edit [value]="  cardUrl.linkText"
                               type="text" field="linkText" placeholder="Link text"
                               (changed)="cardUrlValueChanged(selectedIndex, j, $event)"></plugin-field-edit>
          </div>
        </ng-container>
      </ng-container>
    </div>

  `,


})
export class PluginCardInfoFormComponent extends PluginBaseFormComponent<PluginCardInfo> {
  selectedIndex = -1;
  @Input() editSubmenuOpen;

  constructor() {
    super()
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.editSubmenuOpen == false && this.selectedIndex > -1) {
      this.close();
    }
  }

  cardShowChanged(i, $event: PluginEditEvent) {
    this.pluginObject.cardInfoArray[i].show = $event.value;
    $event.value = this.pluginObject.cardInfoArray;
    this.valuesChanged.emit({field: $event.field, value: $event.value, type: 'parent'})
  }

  cardValueChanged(i, $event: PluginEditEvent) {
    this.pluginObject.cardInfoArray[i][$event.field] = $event.value;
    $event.value = this.pluginObject.cardInfoArray
    this.valuesChanged.emit({field: "cardInfoArray", value: $event.value, type: 'parent'})
  }

  cardUrlValueChanged(i, j, $event: PluginEditEvent) {
    if (this.editTemplate) {
      this.pluginObject.cardInfoArray[i].urlsArray[j][$event.field] = $event.value;
      $event.value = this.pluginObject.cardInfoArray;
    } else {
      this.pluginObject.cardInfoArray[i].urlsArray[j][$event.field] = $event.value;
      $event.value = this.pluginObject.cardInfoArray;
    }

    this.valuesChanged.emit({field: "cardInfoArray", value: $event.value, type: 'parent'})
  }

  edit(i) {
    this.selectedIndex = i;
    this.toggleSubMenu(true);
  }

  close() {
    this.selectedIndex = -1;
    this.toggleSubMenu(false);
  }
}
