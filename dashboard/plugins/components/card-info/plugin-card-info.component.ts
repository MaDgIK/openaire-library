import {Component} from '@angular/core';
import {PluginBaseComponent, PluginBaseInfo, PluginInfoCards, PluginURL} from "../../utils/base-plugin.component";
export class PluginCardInfo extends PluginBaseInfo{
  title:string ="Lorem ipsum";
  description:string = "Lorem ipsum";
  cardInfoArray: PluginInfoCards[] = [
    {title: "Card 1", description: "Lorem ipsum", icon:"description", urlsArray:[ new PluginURL("https://exapmle.com","View all")], show:true},
    {title: "Card 2", description: "Lorem ipsum", icon:"description", urlsArray:[ new PluginURL("https://exapmle.com","")/*,  new PluginURL("","Guide for the managers")*/], show:true},
    {title: "Card 3", description: "Lorem ipsum", icon:"description", urlsArray:[ new PluginURL("https://exapmle.com","View all")], show:true}
  ];
  compare(oldObject){
   oldObject =  super.compare(oldObject)
    return oldObject;
  }
}
@Component({
  selector: 'plugin-card-info',
  templateUrl: 'plugin-card-info.component.html'
})
export class PluginCardInfoComponent extends PluginBaseComponent<PluginCardInfo>{
  constructor() {
    super()
  }
}
