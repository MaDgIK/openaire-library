import {Component, SimpleChanges} from '@angular/core';
import {PluginBaseComponent, PluginBaseInfo} from "../../utils/base-plugin.component";
import {HttpClient} from "@angular/common/http";

export class PluginOpenAIREProducts extends  PluginBaseInfo{
  title:string ="OpenAIRE services for your community";
  serviceIdsArray = ["openaire.argos","cern.zenodo","athenarc.amnesia"];
  compare(oldObject): any {
    return super.compare(oldObject);
  }
}
@Component({
  selector: 'plugin-openaire-products',
  template: `
    <div *ngIf="pluginObject " class="uk-container uk-section">
        <h3>{{pluginObject.title}} </h3>

      <div *ngIf="services && servicesToShow" uk-slider class="uk-slider">
        <ul *ngIf="slides" class="uk-slider-items" uk-height-match="target: .uk-card; row: false">
          <li *ngFor="let slide of [].constructor(slides); let i=index" class="uk-width-1-1 uk-padding">
            <div class="uk-grid uk-child-width-1-3@m uk-child-width-1-1" uk-grid uk-scrollspy="target: [uk-scrollspy-class]; cls: uk-animation-fade; repeat: true">
              <div *ngFor="let service of servicesToShow.slice((i)*slideItems,(i+1)*slideItems)" uk-scrollspy-class>
                <div class="uk-card uk-card-default ">
                  <div class="uk-card-media-top">
                    <img [src]="service.logo" width="100%" height="" alt="" class="uk-height-max-small">
                  </div>
                  <div class=" uk-height-max-large uk-padding-small">
                    <div class="uk-text-primary">{{service.name}}</div>
                    <div class="uk-h5 uk-margin-remove-vertical">{{service.tagline}}</div>
                    <div [innerHTML]="service.description" class="uk-text-truncate uk-text-small uk-text-meta"></div>
                    <a class="uk-link-text" [href]="service.webpage" target="_blank">Read more</a>
                  </div>
                </div>
              </div>

            </div>
          </li>
        </ul>
        <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin-medium-top"></ul>
      </div>
      <div *ngIf="showErrorMessage"  class="uk-text-muted uk-text-center">
        No products info available
        
      </div>
    </div> 
  
  `,


})
export class PluginOpenaireProductsComponent extends PluginBaseComponent<PluginOpenAIREProducts>{
  services = null;
  excludedServiceIds = ["openaire_login","research_community_dashboard"]
  servicesToShow = null;
  slides = 1;
  slideItems = 3;
  api= "https://catalogue.openaire.eu/api/catalogue-resources?from=0&quantity=100&order=asc&orderField=name";
  showErrorMessage = false;
  constructor(private http:HttpClient) {
    super()
  }
  ngOnInit(): void {
    super.ngOnInit();
    if(!this.services) {
      this.subscriptions.push(this.http.get(  this.properties.cacheUrl + encodeURIComponent(this.api)).subscribe(res =>{
        this.services = res["results"].map( x=> {
          if(x.id.indexOf("openaire.")!=-1){
            x.id = x.id.split("openaire.")[1]
          }
          return x;
        });
        this.services = this.services.filter(x=> this.excludedServiceIds.indexOf(x.id) ==-1);
        this.calculatePages();
      }, error => {
        this.showErrorMessage = true;
      }))
    }else{
      this.calculatePages();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if(this.services) {
      this.calculatePages();
    }
  }
  calculatePages(){
    this.slides = 1;
    this.servicesToShow = this.services.filter(x => this.pluginObject.serviceIdsArray.indexOf(x.id) != -1);
    if (this.servicesToShow.length > this.slideItems) {
      this.slides = parseInt('' + (this.servicesToShow.length / this.slideItems));
      if(this.slides< (this.servicesToShow.length / this.slideItems)){
        this.slides++;
      }
    }
  }
}
