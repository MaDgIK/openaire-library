import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {PluginsService} from "../../../../services/plugins.service";
import {IconsModule} from "../../../../utils/icons/icons.module";
import {NumberRoundModule} from "../../../../utils/pipes/number-round.module";
import {IconsService} from "../../../../utils/icons/icons.service";
import {SearchResearchResultsServiceModule} from "../../../../services/searchResearchResultsService.module";
import {PluginOpenaireProductsComponent} from "./plugin-openaire-products.component";
import {PluginFieldEditModule} from "../../utils/plugin-field-edit.module";

@NgModule({
  imports: [
    CommonModule, RouterModule, FormsModule, IconsModule, NumberRoundModule, SearchResearchResultsServiceModule, PluginFieldEditModule
  ],
  providers:[PluginsService],
    declarations: [PluginOpenaireProductsComponent],
    exports: [PluginOpenaireProductsComponent]
})
export class PluginOpenaireProductsModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([])
  }
}
