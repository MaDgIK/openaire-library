import {Component} from '@angular/core';
import {PluginGatewayInformation} from "./plugin-gateway-information.component";
import {OpenaireEntities} from "../../../../utils/properties/searchFields";
import {PluginBaseFormComponent} from "../../utils/base-plugin.form.component";

@Component({
  selector: 'plugin-gateway-information-form',
  template: `
    <div *ngIf="pluginObject" class="uk-padding-xsmall">
      <plugin-field-edit [value]="pluginObject.title"
                         type="text" field="title" (changed)="valueChanged($event)"></plugin-field-edit>
      <div class="uk-margin-top uk-text-meta uk-text-small uk-margin-small-bottom">
        Show or hide the following information:
      </div>
      <div class="uk-margin-top uk-text-meta uk-text-xsmall ">
        Community info
      </div>
      
      <div class="uk-grid uk-child-width-1-1 uk-text-small  uk-hr ">
        <div class="uk-margin-xsmall-bottom uk-margin-xsmall-top">
          <plugin-field-edit [value]=" pluginObject.showTitle"
                             type="checkbox" field="showTitle" (editClicked)="pluginEditEvent = $event"
                             (changed)="valueChanged($event)">

          </plugin-field-edit> Title

        </div>
        <div class="uk-margin-xsmall-bottom uk-margin-xsmall-top">
          <plugin-field-edit [value]=" pluginObject.showShortTitle"
                             type="checkbox" field="showShortTitle" (editClicked)="pluginEditEvent = $event"
                             (changed)="valueChanged($event)">

          </plugin-field-edit> Short title

        </div>
        <div class="uk-margin-xsmall-bottom uk-margin-xsmall-top">
          <plugin-field-edit [value]=" pluginObject.description"
                             type="checkbox" field="description" (editClicked)="pluginEditEvent = $event"
                             (changed)="valueChanged($event)">

          </plugin-field-edit> Description

        </div>
        <div class="uk-margin-xsmall-bottom uk-margin-xsmall-top">
          <plugin-field-edit [value]=" pluginObject.curators"
                             type="checkbox" field="curators" (editClicked)="pluginEditEvent = $event"
                             (changed)="valueChanged($event)">

          </plugin-field-edit>Curated by
          
        </div>
        <div class="uk-margin-xsmall-bottom">
          <plugin-field-edit [value]=" pluginObject.date"
                             type="checkbox" field="date" (editClicked)="pluginEditEvent = $event"
                             (changed)="valueChanged($event)">
          </plugin-field-edit> Created
          
        </div>
        <div class="uk-margin-xsmall-bottom">
          
          <plugin-field-edit [value]=" pluginObject.projects"
                             type="checkbox" field="projects" (editClicked)="pluginEditEvent = $event"
                             (changed)="valueChanged($event)">
          </plugin-field-edit> 
          {{openaireEntities.PROJECTS}}
        </div>
        <div class="uk-margin-xsmall-bottom">
          
          <plugin-field-edit [value]=" pluginObject.communities"
                             type="checkbox" field="communities" (editClicked)="pluginEditEvent = $event"
                             (changed)="valueChanged($event)">
          </plugin-field-edit>
          Linked Zenodo communities
        </div>
        <div class="uk-margin-xsmall-bottom">
          
          <plugin-field-edit [value]=" pluginObject.datasources"
                             type="checkbox" field="datasources" (editClicked)="pluginEditEvent = $event"
                             (changed)="valueChanged($event)">

          </plugin-field-edit>
          {{openaireEntities.DATASOURCES}}
        </div>
        <div class="uk-margin-xsmall-bottom">
          
          <plugin-field-edit [value]=" pluginObject.subjects"
                             type="checkbox" field="subjects" (editClicked)="pluginEditEvent = $event"
                             (changed)="valueChanged($event)">
          </plugin-field-edit>
          Subjects
        </div>
        <div class="uk-alert uk-alert-warning uk-text-small uk-padding-xsmall  uk-margin-medium-left "> Manage community info  <a routerLink="../../../info/profile" target="_blank">here</a>.</div>
        <div class="uk-margin-top uk-text-meta uk-text-xsmall">
          Pages & menus
        </div>
        <div class="uk-margin-xsmall-bottom uk-hr uk-margin-xsmall-top">
          <plugin-field-edit [value]=" pluginObject.searchbar"
                             type="checkbox" field="searchbar" (editClicked)="pluginEditEvent = $event"
                             (changed)="valueChanged($event)">
          </plugin-field-edit>
          Search bar
        </div>
        <div class="uk-margin-xsmall-bottom">
          <plugin-field-edit [value]=" pluginObject.fos"
                             type="checkbox" field="fos" (editClicked)="pluginEditEvent = $event"
                             (changed)="valueChanged($event)">
          </plugin-field-edit>
          Browse by FOS
        </div>
        <div class="uk-margin-xsmall-bottom">
          <plugin-field-edit [value]=" pluginObject.sdgs"
                             type="checkbox" field="sdgs" (editClicked)="pluginEditEvent = $event"
                             (changed)="valueChanged($event)">
          </plugin-field-edit>
          Browse by SDGs
        </div>
        <div class="uk-margin-xsmall-bottom">
          <plugin-field-edit [value]=" pluginObject.publications"
                             type="checkbox" field="publications" (editClicked)="pluginEditEvent = $event"
                             (changed)="valueChanged($event)">
          </plugin-field-edit>
          {{openaireEntities.PUBLICATIONS}}
        </div>
        <div class="uk-margin-xsmall-bottom">
          
          <plugin-field-edit [value]=" pluginObject.datasets"
                             type="checkbox" field="datasets" (editClicked)="pluginEditEvent = $event"
                             (changed)="valueChanged($event)">
          </plugin-field-edit>
          {{openaireEntities.DATASETS}}
        </div>
        <div class="uk-margin-xsmall-bottom">
          
          <plugin-field-edit [value]=" pluginObject.software"
                             type="checkbox" field="software" (editClicked)="pluginEditEvent = $event"
                             (changed)="valueChanged($event)">
          </plugin-field-edit>
          {{openaireEntities.SOFTWARE}}
        </div>
        <div class="uk-margin-xsmall-bottom">
          
          <plugin-field-edit [value]=" pluginObject.other"
                             type="checkbox" field="other" (editClicked)="pluginEditEvent = $event"
                             (changed)="valueChanged($event)">
          </plugin-field-edit>
          {{openaireEntities.OTHER}}
        </div>
      </div>
      
      <div class="uk-alert uk-alert-warning uk-text-small uk-padding-xsmall"> If some information is enabled here, 
        but still not visible, please check related <a routerLink="../../entities" target="_blank">entity</a> or <a routerLink="../../pages" target="_blank">page</a>.
      </div>
      <div class="uk-alert uk-alert-warning uk-text-small uk-padding-xsmall">
        Change the custom section background options  <a routerLink="../../../customize-layout" target="_blank"> here.</a>
      </div>
    </div>
  `,


})
export class PluginGatewayInformationFormComponent extends PluginBaseFormComponent<PluginGatewayInformation> {
  openaireEntities= OpenaireEntities;
  constructor() {
    super()

  }

}
