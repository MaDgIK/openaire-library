import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {PluginsService} from "../../../../services/plugins.service";
import {IconsModule} from "../../../../utils/icons/icons.module";
import {NumberRoundModule} from "../../../../utils/pipes/number-round.module";
import {IconsService} from "../../../../utils/icons/icons.service";
import {SearchResearchResultsServiceModule} from "../../../../services/searchResearchResultsService.module";
import {PluginFieldEditModule} from "../../utils/plugin-field-edit.module";
import {PluginGatewayInformationComponent} from './plugin-gateway-information.component';
import {CuratorsModule} from '../../../../connect/components/curators/curators.module';
import {SearchCommunityProjectsService} from '../../../../../openaireLibrary/connect/projects/searchProjects.service';
import {SearchCommunityDataprovidersService} from '../../../../../openaireLibrary/connect/contentProviders/searchDataproviders.service';
import {ZenodoCommunitiesService} from '../../../../../openaireLibrary/connect/zenodoCommunities/zenodo-communities.service';
import {AdvancedSearchInputModule} from "../../../../sharedComponents/advanced-search-input/advanced-search-input.module";
import {EntitiesSelectionModule} from "../../../../searchPages/searchUtils/entitiesSelection.module";
import {QuickSelectionsModule} from "../../../../searchPages/searchUtils/quick-selections.module";
import {InputModule} from "../../../../sharedComponents/input/input.module";
import {CustomizationServiceModule} from "../../../../services/customizationService.module";

@NgModule({
  imports: [
    CommonModule, RouterModule, FormsModule, IconsModule, NumberRoundModule, SearchResearchResultsServiceModule, PluginFieldEditModule,
    CuratorsModule, AdvancedSearchInputModule, EntitiesSelectionModule, QuickSelectionsModule, InputModule, CustomizationServiceModule
  ],
  providers: [
    PluginsService, SearchCommunityProjectsService, SearchCommunityDataprovidersService, ZenodoCommunitiesService
  ],
  declarations: [PluginGatewayInformationComponent],
  exports: [PluginGatewayInformationComponent]
})
export class PluginGatewayInformationModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([])
  }
}
