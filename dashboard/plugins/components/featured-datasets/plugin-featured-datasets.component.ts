import {Component, SimpleChanges} from '@angular/core';
import {PluginBaseComponent, PluginBaseInfo} from "../../utils/base-plugin.component";
import {FetchResearchResults} from "../../../../utils/fetchEntitiesClasses/fetchResearchResults.class";
import {SearchResearchResultsService} from "../../../../services/searchResearchResults.service";
import {properties} from "../../../../../../environments/environment";
import {ResultPreview} from "../../../../utils/result-preview/result-preview";

export class PluginFeaturedDatasets extends PluginBaseInfo{
  title: string = "Featured datasets";
  textLine1: string = "Here are listed some of the most important energy datasets as selected by energy experts.";
  textLine2: string = "Check them if you want to easily explore and visualize the European energy landscape, using only well-known datasets which you can trust.";

}

@Component({
  selector: 'plugin-featured-datasets',
  templateUrl: 'plugin-featured-datasets.component.html'
})

export class PluginFeaturedDatasetsComponent extends PluginBaseComponent<PluginFeaturedDatasets> {
  default = new PluginFeaturedDatasets();
  // public fetchFeaturedDatasets: FetchResearchResults;
  page = 1;
  size = 3;
  slides = 1;
  slideItems = 3;
  totalNum = null;
  results: ResultPreview[] = null;
  constructor(private _searchResearchResultsService: SearchResearchResultsService) {
    super()
    // this.fetchFeaturedDatasets = new FetchResearchResults(this._searchResearchResultsService);
    // this.fetchFeaturedDatasets.searchUtils.size = this.size;
    // this.fetchFeaturedDatasets.getAllResultsForCommunity("dataset", "enermaps", 1, 50, this.properties, "enermaps::selection");
    this.subscriptions.push(this._searchResearchResultsService.advancedSearchResults("dataset", null, 1, 50, null,  properties, "&type=results&fq=communityid=enermaps&fq=categoryid=" + encodeURIComponent("enermaps::selection")).subscribe(
      data => {
        console.log(data);

        this.totalNum = data[0];
         this.results = data[1].map( res => {
           let resultPreview = ResultPreview.searchResultConvert(res, 'dataset')
           resultPreview.hostedBy_collectedFrom = null;
           resultPreview.measure = null;
           return resultPreview
         }) ;
        console.log(this.totalNum = data[0], this.results);

        if(this.results && this.results.length>0) {
           this.calculatePages();
         }
      },
      err => {

      }
    ));
  }
/*  ngOnChanges(changes: SimpleChanges) {
    if(this.results) {
      this.calculatePages();
    }
  }*/
  calculatePages(){
    this.slides = 1;
    if (this.results.length > this.slideItems) {
      this.slides = parseInt('' + (this.results.length / this.slideItems));
      if(this.slides< (this.results.length / this.slideItems)){
        this.slides++;
      }
    }
  }

}
