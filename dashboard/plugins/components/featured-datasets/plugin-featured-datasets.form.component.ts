import {Component} from '@angular/core';
import {OpenaireEntities} from "../../../../utils/properties/searchFields";
import {PluginFeaturedDatasets} from "./plugin-featured-datasets.component";
import {PluginBaseFormComponent} from "../../utils/base-plugin.form.component";

@Component({
  selector: 'plugin-featured-datasets-form',
  template: `
    <div *ngIf="pluginObject" class="uk-padding-xsmall">
      <div class="uk-margin-bottom">

        <plugin-field-edit [value]="pluginObject.title"
                           type="text" field="title" (changed)="valueChanged($event)"></plugin-field-edit>
      </div>
      <div class="uk-margin-bottom">
        <plugin-field-edit [value]="pluginObject.textLine1"
                           type="text" field="textLine1" (changed)="valueChanged($event)"></plugin-field-edit>
      </div>
      <div class="uk-margin-bottom">
        <plugin-field-edit [value]="pluginObject.textLine2"
                           type="text" field="textLine2" (changed)="valueChanged($event)"></plugin-field-edit>
      </div>
    </div>
  `,


})
export class PluginFeaturedDatasetsFormComponent extends PluginBaseFormComponent<PluginFeaturedDatasets> {
  selectedIndex = null;
  openaireEntities = OpenaireEntities;

  constructor() {
    super()

  }

}
