import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {PluginsService} from "../../../../services/plugins.service";
import {IconsModule} from "../../../../utils/icons/icons.module";
import {NumberRoundModule} from "../../../../utils/pipes/number-round.module";
import {IconsService} from "../../../../utils/icons/icons.service";
import {SearchResearchResultsServiceModule} from "../../../../services/searchResearchResultsService.module";
import {PluginFieldEditModule} from "../../utils/plugin-field-edit.module";
import {PluginFeaturedDatasetsComponent} from './plugin-featured-datasets.component';
import {NoLoadPaging} from "../../../../searchPages/searchUtils/no-load-paging.module";
import {SearchResultsModule} from "../../../../searchPages/searchUtils/searchResults.module";
import {ResultPreviewModule} from "../../../../utils/result-preview/result-preview.module";
import {PagingModule} from "../../../../utils/paging.module";

@NgModule({
  imports: [
    CommonModule, RouterModule, FormsModule, IconsModule, NumberRoundModule, SearchResearchResultsServiceModule,   ResultPreviewModule, PagingModule
  ],
  providers:[PluginsService],
  declarations: [PluginFeaturedDatasetsComponent],
  exports: [PluginFeaturedDatasetsComponent]
})
export class PluginFeaturedDatasetsModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([])
  }
}
