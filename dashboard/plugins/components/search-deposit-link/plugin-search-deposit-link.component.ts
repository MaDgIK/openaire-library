import {Component} from '@angular/core';
import {PluginBaseComponent, PluginBaseInfo, PluginInfoCards, PluginURL} from "../../utils/base-plugin.component";
import {OpenaireEntities} from "../../../../utils/properties/searchFields";
import {properties} from "../../../../../../environments/environment";
import {CustomizationService} from "../../../../services/customization.service";
import {CommunityService} from "../../../../connect/community/community.service";
export class PluginSearchDepositLink extends PluginBaseInfo{
  title:string ="Search, link and deposit your research in one place";
  cardInfoArray: PluginInfoCards[] = [
    {tag: "SEARCH & BROWSE", title: "Discover research products in your community.", description:
        "A view of the OpenAIRE Graph, configured by experts of your research community, who want to help you out with the data and literature deluge.",
      urlsArray:[ new PluginURL("/search/find/research-outcomes","Start searching", null, true)],
      image:'https://' + (properties.environment == 'production'?'':'beta.')
        + 'connect.openaire.eu/assets/connect-assets/home/4.png',show:true},
    {tag: "DEPOSIT YOUR RESEARCH OUTCOME", title: "Publish your research in Open Access.", description:""
      , urlsArray:[ new PluginURL("/participate/deposit/learn-how","Deposit your research", null, true)],
      image:'https://' + (properties.environment == 'production'?'':'beta.')
        + 'connect.openaire.eu/assets/connect-assets/home/1.png',show:true},
    {tag: "LINK YOUR RESEARCH", title: "Contribute to your community.", description:""
      , urlsArray:[ new PluginURL("/participate/claim","Link your Research searching", null, true)],
      image:'https://' + (properties.environment == 'production'?'':'beta.')
        + 'connect.openaire.eu/assets/connect-assets/home/2.png',show:true},
    ];

  compare(oldObject): any {
    let newObj=   super.compare(oldObject);
    return newObj;
  }
}
@Component({
  selector: 'plugin-search-deposit-link',
  templateUrl: 'plugin-search-deposit-link.component.html',
  styleUrls:[`plugin-search-deposit-link.component.less`]
})
export class PluginSearchDepositLinkComponent extends PluginBaseComponent<PluginSearchDepositLink>{
  entities= OpenaireEntities;

  constructor(private communityService: CommunityService,  protected layoutService: CustomizationService) {
    super()
  }
  ngOnInit() {
    this.subscriptions.push(this.communityService.getCommunityAsObservable().subscribe(
      community => {
        if(community) {
          this.getLayout(community.communityId);
        }
      }));
  }

  get activeCards(){
    return this.pluginObject.cardInfoArray.filter( card => card.show);
  }
}
