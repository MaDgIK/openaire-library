import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {PluginsService} from "../../../../services/plugins.service";
import {IconsModule} from "../../../../utils/icons/icons.module";
import {NumberRoundModule} from "../../../../utils/pipes/number-round.module";
import {IconsService} from "../../../../utils/icons/icons.service";
import {SearchResearchResultsServiceModule} from "../../../../services/searchResearchResultsService.module";
import {PluginFieldEditModule} from "../../utils/plugin-field-edit.module";
import {PluginSearchDepositLinkComponent} from './plugin-search-deposit-link.component';
import {SliderUtilsModule} from "../../../../sharedComponents/slider-utils/slider-utils.module";
import {PluginUrlComponent} from "../../utils/pluginUrl.component";

@NgModule({
  imports: [
    CommonModule, RouterModule, FormsModule, IconsModule, NumberRoundModule, SearchResearchResultsServiceModule,
    PluginFieldEditModule, SliderUtilsModule, PluginUrlComponent
  ],
  providers:[PluginsService],
  declarations: [PluginSearchDepositLinkComponent],
  exports: [PluginSearchDepositLinkComponent]
})
export class PluginSearchDepositLinkModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([])
  }
}
