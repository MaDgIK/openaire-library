import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {PluginsService} from "../../../../services/plugins.service";
import {PluginFieldEditModule} from "../../utils/plugin-field-edit.module";
import {InputModule} from "../../../../sharedComponents/input/input.module";
import {PluginGraphInfoComponent} from "./plugin-graph-info.component";
import {PluginUrlComponent} from "../../utils/pluginUrl.component";

@NgModule({
  imports: [
    CommonModule, RouterModule, FormsModule, PluginFieldEditModule, InputModule, PluginUrlComponent
  ],
  providers: [
    PluginsService
  ],
  declarations: [PluginGraphInfoComponent],
  exports: [PluginGraphInfoComponent]
})
export class PluginGraphInfoModule {

}
