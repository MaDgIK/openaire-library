import {Component} from '@angular/core';
import {PluginBaseComponent, PluginBaseInfo, PluginURL} from "../../utils/base-plugin.component";

export class ParagraphInfo extends PluginBaseInfo{
  title:string ="Lorem ipsum"
  paragraph1:string = `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`;
  paragraph2:string = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
  url:PluginURL= new PluginURL("https://example.com","Lorem ipsum")
  image:string = "https://admin.connect.openaire.eu/assets/common-assets/placeholder.png"
  compare(oldObject): any {
    let newObj=   super.compare(oldObject);
    return newObj;
  }
}
@Component({
  selector: 'plugin-graph-info',
  templateUrl: 'plugin-graph-info.component.html'
})
export class PluginGraphInfoComponent extends PluginBaseComponent<ParagraphInfo>{
  constructor() {
    super();
  }

}
