import {Component} from '@angular/core';
import {PluginBaseFormComponent, PluginEditEvent} from "../../utils/base-plugin.form.component";
import {ParagraphInfo} from "./plugin-graph-info.component";

@Component({
  selector: 'plugin-graph-info-form',
  template: `
    <div *ngIf="pluginObject" class="uk-padding-xsmall">
      <plugin-field-edit [value]="pluginObject.title"
                         type="text" field="title" (changed)="valueChanged($event)"></plugin-field-edit>
      <div *ngIf="pluginTemplate.custom" class="uk-margin-top">
      <plugin-field-edit  [value]="pluginObject.image"
                         type="text" field="image" (changed)="valueChanged($event)"></plugin-field-edit>
      </div>
      <div class="uk-margin-top">
        <plugin-field-edit [value]="pluginObject.paragraph1" [switchToHTMLEditor]="true"
                           type="textarea" field="paragraph1" (changed)="valueChanged($event)"></plugin-field-edit>
      </div>
      <div class="uk-margin-top">
        <plugin-field-edit [value]="pluginObject.paragraph2" class="uk-margin-xsmall-top" [switchToHTMLEditor]="true"
                           type="textarea" field="paragraph2" (changed)="valueChanged($event)"></plugin-field-edit>
      </div>
      <div class=" uk-margin-top uk-text-meta uk-text-xsmall"> Link</div>
      <div class="uk-margin-small-top">
        <plugin-field-edit [value]=" pluginObject.url.url"
                           type="text" field="url"
                           (changed)="urlValueChanged($event)"></plugin-field-edit>
      </div>
      <div class="uk-margin-top">
        <plugin-field-edit [value]="  pluginObject.url.linkText"
                           type="text" field="linkText" placeholder="Link text"
                           (changed)="urlValueChanged($event)"></plugin-field-edit>
      </div>
    </div>
  `,


})
export class PluginGraphInfoFormComponent extends PluginBaseFormComponent<ParagraphInfo> {
  constructor() {
    super()

  }

  urlValueChanged($event: PluginEditEvent) {

    this.pluginObject.url[$event.field] = $event.value;
    $event.value = this.pluginObject.url;
    this.valuesChanged.emit({field: "url", value: $event.value, type: 'parent'})
  }

}
