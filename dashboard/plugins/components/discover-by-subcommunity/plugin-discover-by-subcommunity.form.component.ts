import {Component} from '@angular/core';
import {PluginDiscoverBySubcommunity} from "./plugin-discover-by-subcommunity.component";
import {PluginBaseFormComponent} from "../../utils/base-plugin.form.component";

@Component({
  selector: 'plugin-discover-by-subcommunity-form',
  template: `
    <div *ngIf="pluginObject" class="uk-padding-xsmall">
          <plugin-field-edit [value]="pluginObject.title" 
                             type="text" field="title"   (changed)="valueChanged($event)" ></plugin-field-edit>
    </div>
  
  `,


})
export class PluginDiscoverBySubcommunityFormComponent extends PluginBaseFormComponent<PluginDiscoverBySubcommunity> {
  constructor() {
    super()
  }
}
