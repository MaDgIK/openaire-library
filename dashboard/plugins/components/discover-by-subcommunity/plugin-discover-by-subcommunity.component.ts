import {Component} from '@angular/core';
import {PluginBaseComponent, PluginBaseInfo} from "../../utils/base-plugin.component";
import {HttpClient} from "@angular/common/http";
import {CommunityService} from "../../../../connect/community/community.service";
export class PluginDiscoverBySubcommunity extends PluginBaseInfo{
  title:string ="Discover content by sub-community";
}
@Component({
  selector: 'plugin-discover-by-subcommunity',
  templateUrl: 'plugin-discover-by-subcommunity.component.html'
})
export class PluginDiscoverBySubcommunityComponent extends PluginBaseComponent<PluginDiscoverBySubcommunity>{
  subcommunities = null;
  community;
  constructor(http:HttpClient, private communityService: CommunityService) {
    super()
    this.subscriptions.push(this.communityService.getCommunityAsObservable().subscribe(
      community => {
        this.community = community;
        if(community) {
          this.subscriptions.push(http.get(this.properties.communityAPI + community.communityId
            + "/subcommunities?all=false").subscribe(res => {
            console.log(res)
            this.subcommunities = res ? res : [] /* = res.splice(0,15)*/;

          }));
        }
      }));

  }
}
