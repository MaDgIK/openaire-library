import {Component} from '@angular/core';
import {OpenaireEntities} from "../../../../utils/properties/searchFields";
import {PluginBaseFormComponent} from "../../utils/base-plugin.form.component";
import {PluginSearchBar} from "./plugin-search-bar.component";

@Component({
  selector: 'plugin-search-bar-form',
  template: `
    <div *ngIf="pluginObject" class="uk-padding-xsmall">
      <plugin-field-edit [value]="pluginObject.alternativeTitle"
                         type="text" field="alternativeTitle" placeholder="Alternative title" (changed)="valueChanged($event)"></plugin-field-edit>
      <div class="uk-margin-top uk-text-meta uk-text-small uk-margin-small-bottom">
        Show or hide the following information:
      </div>
      <div class="uk-grid uk-child-width-1-1 uk-text-small  uk-hr ">
        <div class="uk-margin-xsmall-bottom uk-margin-xsmall-top">
          <plugin-field-edit [value]=" pluginObject.showTitle"
                             type="checkbox" field="showTitle" (editClicked)="pluginEditEvent = $event"
                             (changed)="valueChanged($event)">

          </plugin-field-edit> Title

        </div>
        <div class="uk-margin-xsmall-bottom uk-margin-xsmall-top">
          <plugin-field-edit [value]=" pluginObject.showShortTitle"
                             type="checkbox" field="showShortTitle" (editClicked)="pluginEditEvent = $event"
                             (changed)="valueChanged($event)">

          </plugin-field-edit> Short title

        </div>

        <div class="uk-alert uk-alert-warning uk-text-small uk-padding-xsmall  uk-margin-medium-left "> Manage community info  <a routerLink="../../../info/profile" target="_blank">here</a>.</div>
      </div>
      </div>
  `,


})
export class PluginSearchBarFormComponent extends PluginBaseFormComponent<PluginSearchBar> {
  openaireEntities= OpenaireEntities;
  constructor() {
    super()

  }

}
