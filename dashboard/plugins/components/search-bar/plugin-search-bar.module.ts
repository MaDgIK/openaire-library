import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {PluginsService} from "../../../../services/plugins.service";
import {IconsModule} from "../../../../utils/icons/icons.module";
import {NumberRoundModule} from "../../../../utils/pipes/number-round.module";
import {SearchResearchResultsServiceModule} from "../../../../services/searchResearchResultsService.module";
import {PluginFieldEditModule} from "../../utils/plugin-field-edit.module";
import {CuratorsModule} from '../../../../connect/components/curators/curators.module';
import {AdvancedSearchInputModule} from "../../../../sharedComponents/advanced-search-input/advanced-search-input.module";
import {EntitiesSelectionModule} from "../../../../searchPages/searchUtils/entitiesSelection.module";
import {QuickSelectionsModule} from "../../../../searchPages/searchUtils/quick-selections.module";
import {InputModule} from "../../../../sharedComponents/input/input.module";
import {PluginSearchBarComponent} from "./plugin-search-bar.component";

@NgModule({
  imports: [
    CommonModule, RouterModule, FormsModule, IconsModule, NumberRoundModule, SearchResearchResultsServiceModule, PluginFieldEditModule,
    CuratorsModule, AdvancedSearchInputModule, EntitiesSelectionModule, QuickSelectionsModule, InputModule,
  ],
  providers: [
    PluginsService
  ],
  declarations: [PluginSearchBarComponent],
  exports: [PluginSearchBarComponent]
})
export class PluginSearchBarModule {

}
