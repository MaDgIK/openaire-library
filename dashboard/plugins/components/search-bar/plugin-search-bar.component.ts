import {ChangeDetectorRef, Component} from '@angular/core';
import {PluginBaseComponent, PluginBaseInfo} from "../../utils/base-plugin.component";
import {OpenaireEntities} from "../../../../utils/properties/searchFields";
import {CustomizationService} from "../../../../services/customization.service";
import {CommunityService} from "../../../../connect/community/community.service";
import {Filter} from "../../../../searchPages/searchUtils/searchHelperClasses.class";
import {Router} from "@angular/router";
import {RouterHelper} from "../../../../utils/routerHelper.class";

export class PluginSearchBar extends PluginBaseInfo{
  alternativeTitle:string ="";
  showTitle:boolean = true;
  showShortTitle:boolean = false;

  compare(oldObject): any {
    let newObj=   super.compare(oldObject);
    return newObj;
  }
}
@Component({
  selector: 'plugin-search-bar',
  template: `
    <div *ngIf="!community " class="uk-text-meta uk-text-center">
      No community info available
    </div>
    <div *ngIf="community "   class="generalSearchForm heroBackground"   [class.uk-light]="!this.fontsDarkMode"  [style]="style">
   
        <div class="uk-container uk-container-large uk-flex uk-flex-center">
          <div class="uk-width-2-3@m uk-width-1-2@l uk-margin-large-top uk-margin-large-bottom">
            <h1 *ngIf="pluginObject.showShortTitle" class="uk-text-center uk-h2 uk-margin-remove">
              {{community.displayShortTitle}}
            </h1>
            <h1 *ngIf="pluginObject.alternativeTitle.length > 0"
                class="uk-text-center uk-h2 uk-margin-remove">
              {{pluginObject.alternativeTitle}}
            </h1>
            <div *ngIf="pluginObject.showTitle"
                 class="uk-text-center uk-margin-top">
              {{community.displayTitle}}
            </div>
            
            <div [class.uk-invisible]="disableSelect" class="uk-margin-medium-top">
              <advanced-search-input #advanced (searchEmitter)="goTo(true)">
                <entities-selection class="uk-width-1-3" [simpleView]="true" currentEntity="result" [selectedEntity]="selectedEntity"  
                                    (selectionChange)="entityChanged($event);advanced.focusNext(input, $event)" (disableSelectEmitter)="disableSelectChange($event)"
                                    [onChangeNavigate]="false"></entities-selection>
                <div input #input class="uk-width-expand"  placeholder="Scholarly works" [searchable]="true" [hint]="'Search in OpenAIRE'" [(value)]="keyword"></div>
              </advanced-search-input>
            </div>
          </div>
        </div>
      </div>

  `
})
export class PluginSearchBarComponent extends PluginBaseComponent<PluginSearchBar>{
  disableSelect: boolean = true;
  openaireEntities= OpenaireEntities;

  selectedEntity = 'result';
  selectedEntitySimpleUrl;
  selectedEntityAdvancedUrl;
  keyword: string = "";
  // customFilter;
  placeholderText = "Search by title, author, abstract, DOI, orcid... ";
  resultTypes: Filter = {
    values: [],
    filterId: "type",
    countSelectedValues: 0,
    filterType: 'checkbox',
    originalFilterId: "",
    valueIsExact: true,
    title: "Type",
    filterOperator: "or"
  };
  community = null;

  public routerHelper: RouterHelper = new RouterHelper();

  constructor(private communityService: CommunityService,  protected layoutService: CustomizationService, private cdr: ChangeDetectorRef, private _router: Router,) {
    super()

    this.subscriptions.push(this.communityService.getCommunityAsObservable().subscribe(
      community => {
        this.community = community;
        if(community) {
          this.subscriptions.push(this.communityService.getCommunityAsObservable().subscribe(
            community => {
              this.getLayout(community.communityId);

            }));
        }
      }
    ));
  }
  ngOnInit() {
    this.subscriptions.push(this.communityService.getCommunityAsObservable().subscribe(
      community => {
        if(community) {
          this.getLayout(community.communityId);
        }
      }));
  }

  goTo(simple: boolean) {
    let url = (simple) ? this.selectedEntitySimpleUrl : this.selectedEntityAdvancedUrl;
    let parameterNames = [];
    let parameterValues = [];


    if (this.keyword.length > 0) {
      parameterNames.push("fv0");
      parameterValues.push(this.keyword);
      parameterNames.push("f0");
      parameterValues.push("q");
    }
    this._router.navigate([url], {queryParams: this.routerHelper.createQueryParams(parameterNames, parameterValues)});
  }
  disableSelectChange(event: boolean) {
    this.disableSelect = event;
    this.cdr.detectChanges();
  }
  entityChanged($event) {
    this.selectedEntity = $event.entity;
    this.selectedEntitySimpleUrl = $event.simpleUrl;
    this.selectedEntityAdvancedUrl = $event.advancedUrl;
    if (this.selectedEntity == 'result') {
      this.placeholderText = "Search by title, author, abstract, DOI, orcid... ";
    } else if (this.selectedEntity == 'project') {
      this.placeholderText = "Search by project title, grant id, funder...";
    } else if (this.selectedEntity == 'dataprovider') {
      this.placeholderText = "Search by name...";
    } else {
      this.placeholderText = "Search community content";
    }
  }
}
