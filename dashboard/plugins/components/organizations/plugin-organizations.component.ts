import {Component} from '@angular/core';
import {PluginBaseComponent, PluginBaseInfo} from "../../utils/base-plugin.component";
import {ConfigurationService} from "../../../../utils/configuration/configuration.service";

export class PluginOrganizations extends PluginBaseInfo{
  title: string = "Supporting <br> Organizations";
  maxOrganizationsToDisplay: number = 9;

}

@Component({
  selector: 'plugin-organizations',
  templateUrl: 'plugin-organizations.component.html'
})

export class PluginOrganizationsComponent extends PluginBaseComponent<PluginOrganizations> {
  portal;
  constructor(private config: ConfigurationService) {
    super()
    this.subscriptions.push(this.config.portalAsObservable.subscribe(
      res => {
        this.portal = res;
      },
      error => {
        console.log(error);
      }
    ));
  }

  isRouteEnabled(route: string) {
    return this.portal && this.portal.pages.some(x => x['route'] == route && x['isEnabled'] === true);
  }


}
