import {Component} from '@angular/core';
import {PluginOrganizations} from "./plugin-organizations.component";
import {PluginBaseFormComponent} from "../../utils/base-plugin.form.component";
import {Validators} from "@angular/forms";

@Component({
  selector: 'plugin-organizations-form',
  template: `
    <div *ngIf="pluginObject" class="uk-padding-xsmall">
      <plugin-field-edit [value]="pluginObject.title"
                         type="text" field="title" (changed)="valueChanged($event)"></plugin-field-edit>
      <div class="uk-margin-top">
        <plugin-field-edit [value]="pluginObject.maxOrganizationsToDisplay" placeholder="MAX ORGANIZATIONS TO DISPLAY"
                           type="number" field="maxOrganizationsToDisplay" (changed)="valueChanged($event)" [validators]="validators"></plugin-field-edit>

        <div class="uk-alert uk-alert-warning uk-text-small"> Manage community organizations  <a routerLink="../../../info/organizations" target="_blank">here</a>.</div>
      </div>
    </div>
  `,


})
export class PluginOrganizationsFormComponent extends PluginBaseFormComponent<PluginOrganizations> {
  validators = [Validators.min(0), Validators.max(15)]
  constructor() {
    super()

  }

}
