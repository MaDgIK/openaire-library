import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {PluginsService} from "../../../../services/plugins.service";
import {IconsService} from "../../../../utils/icons/icons.service";
import {PluginOrganizationsComponent} from './plugin-organizations.component';
import {AffiliationsModule} from "../../../../connect/affiliations/affiliations.module";

@NgModule({
  imports: [
    CommonModule, RouterModule, FormsModule, AffiliationsModule,
  ],
  providers:[PluginsService],
  declarations: [PluginOrganizationsComponent],
  exports: [PluginOrganizationsComponent]
})
export class PluginOrganizationsModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([])
  }
}
