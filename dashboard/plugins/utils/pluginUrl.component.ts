import {Component, Input} from '@angular/core';
import {PluginURL} from "./base-plugin.component";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";


@Component({
  imports:[CommonModule, RouterModule],
  selector: 'plugin-url',
  template: `
    <span title="{{previewInAdmin?'Note: links are disabled in administration dashboard':''}}">
    <a *ngIf="url.route"
       [class]="classAttribute"
       [routerLink]="url.url" [class.uk-disabled]=previewInAdmin>
      {{url.linkText}}
    </a>
    <a *ngIf="!url.route" [href]="url.url" [class.uk-hidden]="!(url.url && url.url.length > 0)"
       [class]="classAttribute"
       [target]="url.target" [class.uk-disabled]=previewInAdmin>
      {{url.linkText}}
    </a>
      </span>
  `,

  standalone :true

})
export class PluginUrlComponent {

  @Input() url:PluginURL;
  @Input() previewInAdmin:boolean;
  @Input() classAttribute;
  htmlEditorView = false;


}
