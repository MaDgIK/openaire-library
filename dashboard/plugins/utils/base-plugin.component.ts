import {Directive, Input, OnDestroy} from '@angular/core';
import {Plugin} from "../../../utils/entities/adminTool/plugin";
import {PluginTemplate} from "../../../utils/entities/adminTool/pluginTemplate";
import {EnvProperties} from "../../../utils/properties/env-properties";
import {properties} from 'src/environments/environment';
import {Subscriber} from "rxjs";
import {CustomizationOptions, Layout} from "../../../connect/community/CustomizationOptions";
import {CustomizationService} from "../../../services/customization.service";

export class PluginBaseInfo {
  title: string = "Lorem ipsum";

  constructor() {
  }

  compare(oldObject) {
    if (!oldObject) {
      oldObject = Object.assign(this)
    } else {
      for (let attrKey of Object.keys(this)) {
        if (!oldObject[attrKey] && oldObject[attrKey] != false) {
          if (typeof this[attrKey] === "string" || typeof this[attrKey] === "boolean") {
            oldObject[attrKey] = this[attrKey];
          } else {
            oldObject[attrKey] = Object.assign(this[attrKey])
          }
        }
      }
    }
    return oldObject;
  }
}

export class PluginURL {
  url: string;
  linkText: string;
  target: string;
  route: boolean;

  constructor(url, linkText, target = "_blank", route = false) {
    this.url = url;
    this.linkText = linkText;
    this.target = target;
    this.route = route;
  }

}

export class PluginInfoCards {
  tag?: string;
  title: string;
  description: string;
  urlsArray: PluginURL[];
  image?: string;
  icon?:string;
  show: boolean;
}

@Directive()
export abstract class PluginBaseComponent<T extends PluginBaseInfo> implements OnDestroy {
  public properties: EnvProperties = properties;
  @Input() plugin: Plugin;
  @Input() pluginTemplate: PluginTemplate;
  @Input() pluginObject: T;
  @Input() previewInAdmin: boolean = false;
  subscriptions = [];

  customizationOptions: CustomizationOptions;
  style:string ='';
  fontsDarkMode:boolean = true;
  protected layoutService: CustomizationService
  constructor( ) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscriber) {
        subscription.unsubscribe()
      } else if (subscription instanceof Function) {
        subscription();
      } else if (typeof IntersectionObserver !== 'undefined' && subscription instanceof IntersectionObserver) {
        subscription.disconnect();
      } else if (typeof ResizeObserver !== 'undefined' && subscription instanceof ResizeObserver) {
        subscription.disconnect();
      }
    });
  }

  isVisible(field) {
    return (this.plugin && this.pluginObject && this.pluginObject[field] == true) /*  plugin is on anyway */
      || (!this.plugin && this.pluginTemplate && this.pluginObject && this.pluginObject[field] == true)  /* template is on */
  }

  getLayout(communityId) {
    if (this.previewInAdmin) {
      let defaultCustomizationOptions = new CustomizationOptions(CustomizationOptions.getIdentity(communityId).mainColor, CustomizationOptions.getIdentity(communityId).secondaryColor);
      this.subscriptions.push(this.layoutService.getLayout(this.properties, communityId).subscribe(layout => {
        layout = (layout ? layout : new Layout(communityId, defaultCustomizationOptions));
        this.customizationOptions = (layout ? CustomizationOptions.checkForObsoleteVersion(layout.layoutOptions, communityId) : Object.assign({}, defaultCustomizationOptions));
        this.setStyle()
      }, error => {
        this.customizationOptions = defaultCustomizationOptions;
        this.setStyle();
      }));
    }
  }

  setStyle() {
    this.style = `background-color: ` + this.customizationOptions.backgrounds.form.color + `;
      background-image: ` + (this.customizationOptions.backgrounds.form.imageFile ? (Layout.getUrl(properties.utilsService + '/download/' + this.customizationOptions.backgrounds.form.imageFile)) : 'none') + `;
    background-position:` + this.customizationOptions.backgrounds.form.position + `;`
    this.fontsDarkMode = this.customizationOptions.backgrounds.form.fontsDarkMode;

  }
}
