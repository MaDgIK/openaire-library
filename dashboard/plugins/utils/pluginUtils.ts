import {Option} from "../../../sharedComponents/input/input.component";
import {PluginOpenAIREProducts} from "../components/openaireProducts/plugin-openaire-products.component";
import {PluginDiscoverBySubcommunity} from "../components/discover-by-subcommunity/plugin-discover-by-subcommunity.component";
import {PluginBaseInfo, PluginInfoCards, PluginURL} from "./base-plugin.component";
import {PluginGatewayInformation} from "../components/gateway-information/plugin-gateway-information.component";
import {PluginFeaturedDatasets} from "../components/featured-datasets/plugin-featured-datasets.component";
import {PluginSearchDepositLink} from "../components/search-deposit-link/plugin-search-deposit-link.component";
import {PluginOrganizations} from "../components/organizations/plugin-organizations.component";
import {PluginSuggestedRepositories} from "../components/suggested-repositories/plugin-suggested-repositories.component";
import {ParagraphInfo} from "../components/paragraph-info/plugin-graph-info.component";
import {PluginStats} from "../components/stats/plugin-stats.component";
import {PluginCardInfo} from "../components/card-info/plugin-card-info.component";
import {PluginSearchBar} from "../components/search-bar/plugin-search-bar.component";
import {HTMLSection} from "../components/html-section/plugin-html-section.component";

export class PluginUtils{
  public attrTypeOptions: Option[] = [
    {label:"Text", value:"text"},
    {label:"HTML", value:"HTML"},
    {label:"Boolean", value:"boolean"},
    {label:"URL", value:"URL"},
  ];

  public availablePluginCodesOptions: Option[] = [
    {label:"OpenAIRE products", value:"openaire-products"},
    {label:"Discover by subcommunity", value:"discover-by-subcommunity"},
    {label:"Gateway information", value:"gateway-information"},
    {label:"Search/ deposit/ link", value:"search-deposit-link"},
    {label:"Learn and Connect", value:"learn-and-connect"},
    {label:"How to use", value:"how-to-use"},
    {label:"Suggested Repositories", value:"suggested-repositories"},
    {label:"Featured Datasets", value:"featured-datasets"},
    {label:"Organizations", value:"organizations"},
    {label:"Graph info", value:"graph-info"},
    {label:"Statistics", value:"stats"},
    {label:"Search Bar", value:"search-bar"},
    {label:"Card info", value:"card-info"},
    {label:"Paragraph info", value:"paragraph-info"},
    {label:"HTML section", value:"html-section"},
  ];
  public customOptions: Option[] = [
    {label:"Yes", value:"true"},
    {label:"no", value:"false"}
  ];
  public availablePluginCodes: string[] = ["openaire-products", "discover-by-subcommunity", "gateway-information", "search-deposit-link", "learn-and-connect", "how-to-use", "suggested-repositories", "featured-datasets", "organizations", "graph-info",  "organizations", "stats", "search-bar","card-info", "paragraph-info", "html-section"];
  public placementsOptions: Option[] = [
    {label:"Right", value:"right"},
    {label:"Top", value:"top"},
    // {label:"Top Right", value:"top-right"},
    // {label:"Center", value:"center"},
    {label:"Bottom", value:"bottom"},
    {label:"Left", value:"left"},
  ];
  public planOptions: Option[] = [
    {value: 'Default', label: 'Default'},
    {value: 'Standard', label: 'Standard'},
    {value: 'Advanced', label: 'Advanced'},
    {value: 'Premium', label: 'Premium'},
    {value: 'National', label: 'National'}
  ];


  public static initializeObjectAndCompare(code, oldObject = null){
    switch(code) {
      case 'openaire-products': {
        return (new PluginOpenAIREProducts()).compare(oldObject);
      }
      case 'discover-by-subcommunity': {
        return (new PluginDiscoverBySubcommunity()).compare(oldObject);
      }
      case 'gateway-information': {
        return (new PluginGatewayInformation()).compare(oldObject);
      }
      case 'search-deposit-link': {
        return (new PluginSearchDepositLink()).compare(oldObject);
      }
      case 'suggested-repositories': {
        return (new PluginSuggestedRepositories()).compare(oldObject);
      }
      case 'featured-datasets': {
        return (new PluginFeaturedDatasets()).compare(oldObject);
      }
      case 'organizations': {
        return (new PluginOrganizations()).compare(oldObject);
      }
      case 'graph-info': {
        return (new PluginGraphInfo()).compare(oldObject);
      }
      case 'paragraph-info': {
        return (new ParagraphInfo()).compare(oldObject);
      }
      case 'html-section': {
        return (new HTMLSection()).compare(oldObject);
      }
      case 'stats': {
        return (new PluginStats()).compare(oldObject);
      }
      case 'learn-and-connect': {
        return (new PluginCardInfo()).compare(oldObject);
      }
      case 'how-to-use': {
        return (new PluginCardInfo()).compare(oldObject);
      }
      case 'card-info': {
        return (new PluginCardInfo()).compare(oldObject);
      }
      case 'search-bar': {
        return (new PluginSearchBar()).compare(oldObject);
      }
      default: {
        return (new PluginBaseInfo()).compare(oldObject);
      }
    }
  }
  //
  public static updateExistingObject(oldObject, object ){
    if(!oldObject) {
      oldObject = Object.assign(object)
    }else{
      for (let attrKey of Object.keys(object)) {
        if (!oldObject[attrKey] && oldObject[attrKey] != false) {
          oldObject[attrKey] = Object.assign(object[attrKey])
        }
      }
    }
    return oldObject;
  }
}

export class PluginHowToUse extends PluginCardInfo{
  title:string ="How to use the gateway?";
  cardInfoArray: PluginInfoCards[] = [
    {title: "Tutorials", description: "Mini-video tutorials for gateway managers", urlsArray:[ new PluginURL("https://www.youtube.com/playlist?list=PL0c4IRNUxuKcyRUQ_J9BH_EE1amXU6kgp","View all")], show:true},
    {title: "Guides", description: "Textual guides on gateway functionalities.", urlsArray:[ new PluginURL("https://www.openaire.eu/research-community-gateway-guide","Guide for the users")/*,  new PluginURL("","Guide for the managers")*/], show:true},
    {title: "Webinars", description: "Recordings and slides of webinars on different aspects of Open Science.", urlsArray:[ new PluginURL("","View all")], show:true}
  ];
  compare(oldObject){
    oldObject =  super.compare(oldObject)

    for(let card of this.cardInfoArray){

    }
    return oldObject;
  }
}

export class PluginLearnAndConnect extends PluginCardInfo{
  title:string ="Learn & Connect with Open Science";
  cardInfoArray:PluginInfoCards[] = [
    {title: "OS Practices", description: "Open Science best practices for your community, policies and mandates.",
      urlsArray:[ new PluginURL("","")], show:true},
    {title: "OS Guides for beginners", description: "New to Open Science? Learn the basics!",urlsArray:[ new PluginURL("https://www.openaire.eu/guides","Learn more")], show:true},
    {title: "Webinars", description: "Recordings and slides of webinars on different aspects of Open Science.",urlsArray:[ new PluginURL("https://www.openaire.eu/support/webinars","Learn more")], show:true}
  ];
}


export class PluginGraphInfo extends ParagraphInfo{
  title:string ="How? It's about open data and collaboration"
  paragraph1:string = `This gateway is built on the OpenAIRE Graph, one of the largest open scholarly record collections worldwide. Conceived as a public and transparent good, populated out of data sources trusted by scientists, the OpenAIRE Graph brings discovery, monitoring, and assessment of science back in the hands of the scientific community.`;
  paragraph2:string = "Within a constantly emerging scholarly communication environment, the OpenAIRE Graph is a moving target, continuously integrating new sources, new types of research objects, and embedding impact and usage indicators. We therefore welcome the community to work with us to improve all its aspects: its coverage (geographic and thematic), quality (disambiguation and semantics) and access (APIs).";
  url:PluginURL= new PluginURL("https://graph.openaire.eu","Learn more")
  image:string = "assets/common-assets/common/graph-nodes.svg"
}
