import {Directive, EventEmitter, Input, OnDestroy, Output} from '@angular/core';
import {Plugin} from "../../../utils/entities/adminTool/plugin";
import {PluginTemplate} from "../../../utils/entities/adminTool/pluginTemplate";
import {EnvProperties} from "../../../utils/properties/env-properties";
import {properties} from 'src/environments/environment';
import {PluginBaseComponent, PluginBaseInfo} from "./base-plugin.component";

export class PluginEditEvent {
  field:string;
  type:"text" | "HTML" | "boolean" | 'parent' |'open-submenu' | 'close-submenu';
  value?:any;
  constructor(field: string, type: "text" | "HTML" | "boolean" | 'parent' |'open-submenu' | 'close-submenu', value= null) {
    this.field = field;
    this.type = type;
    this.value = value;
  }
}

@Directive()
 export abstract class PluginBaseFormComponent<T extends PluginBaseInfo> extends PluginBaseComponent<T> implements  OnDestroy {
  public properties: EnvProperties = properties;
  @Input() editMode =false;
  @Input() plugin:Plugin;
  @Input() pluginTemplate:PluginTemplate;
  @Input() editTemplate:boolean = false;
  @Input() pluginObject:T;
  @Output() valuesChanged:EventEmitter<PluginEditEvent> = new EventEmitter<any>();
  subscriptions = [];
  pluginEditEvent:PluginEditEvent;

  valueChanged($event:PluginEditEvent){
    this.pluginObject[$event.field]=$event.value;
    this.valuesChanged.emit($event)
  }
  toggleSubMenu(open:boolean){
    this.valuesChanged.emit(new PluginEditEvent(null,open?'open-submenu':'close-submenu'))
  }
  isVisible(field){
    return (this.plugin && this.pluginObject && this.pluginObject[field] == true) /*  plugin is on anyway */
      || (!this.plugin &&  this.pluginTemplate && this.pluginObject && this.pluginObject[field] == true)  /* template is on */
  }

}
