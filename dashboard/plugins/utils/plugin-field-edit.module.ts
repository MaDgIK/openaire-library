import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PluginFieldEditComponent} from "./plugin-field-edit.component";
import {InputModule} from "../../../sharedComponents/input/input.module";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {CKEditorModule} from "ng2-ckeditor";
import {AlertModalModule} from "../../../utils/modal/alertModal.module";

@NgModule({
  imports: [
    CommonModule, RouterModule, FormsModule, InputModule, MatSlideToggleModule, CKEditorModule, AlertModalModule, FormsModule, ReactiveFormsModule
  ],
    declarations: [PluginFieldEditComponent],
    exports: [PluginFieldEditComponent]
})
export class PluginFieldEditModule {
  /*constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([])
  }*/
}
