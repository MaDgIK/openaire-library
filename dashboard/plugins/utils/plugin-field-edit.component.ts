import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {PluginEditEvent} from "./base-plugin.form.component";
import {AlertModal} from "../../../utils/modal/alert";
import {UntypedFormBuilder, UntypedFormGroup} from "@angular/forms";


@Component({
  selector: 'plugin-field-edit',
  template: `
    <ng-container *ngIf="type=='boolean'">
      <mat-slide-toggle [checked]="value" (change)="updateObject($event.checked)"></mat-slide-toggle>
    </ng-container>
    <ng-container *ngIf="type!='boolean'">
      <input *ngIf="type == 'checkbox'" [(ngModel)]="value" [checked]="value" (ngModelChange)="updateObject($event)"
             type="checkbox" class="uk-checkbox">
      <div *ngIf="type == 'text'" input [value]="value" [placeholder]="placeholder?placeholder:field.toUpperCase()"
           type="text" (valueChange)="updateObject($event)" inputClass=" border-bottom "></div>
      <div *ngIf="type == 'number'" input [value]="value" [placeholder]="placeholder?placeholder:field.toUpperCase()"
           type="text" (valueChange)="updateObject($event)" inputClass=" border-bottom " [validators]="validators" ></div>
      <div *ngIf="type == 'textarea'" class="uk-grid">
        <ng-container *ngIf="switchToHTMLEditor ">
          <a class="uk-text-xsmall uk-text-right uk-margin-top uk-width-1-1" (click)="openHTMLEditor()"><span
              uk-icon="icon:code; ratio:0.5"></span> open editor</a>
        </ng-container>
        <div class="uk-width-1-1">
          <div input [value]="value" [placeholder]="placeholder?placeholder:field.toUpperCase()" type="textarea"
               [rows]="5" (valueChange)="updateObject($event)" inputClass=" uk-padding-remove  "
               style="min-height: 100px; margin-left:-30px " class="uk-margin-top"></div>
        </div>

      </div>
    </ng-container>
    <modal-alert #HTMLEditorModal (alertOutput)="closeHTMLEditor()" [large]="true"
                 classTitle="uk-background-primary uk-light">
      <ckeditor [readonly]="false"
                debounce="100"
                [formControl]="HTMLForm.get('value')"
                [config]="{ extraAllowedContent: '* [uk-*](*) ; span', disallowedContent: 'script; *[on*]', 
                                      removeButtons:  'Save,NewPage,DocProps,Preview,Print,' +
                                                      'Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,' +
                                                      'CreateDiv,Flash,PageBreak,' +
                                                      'Subscript,Superscript,Anchor,Smiley,Iframe,Styles,Font,About,Language,JustifyLeft,JustifyRight,JustifyCenter,JustifyBlock,FontSize,TextColor,BGColor',
                                      extraPlugins: 'divarea'}">
      </ckeditor>
    </modal-alert>
  `,
  styles: [`
      :host >>> textarea.input {
      border: solid 1px #3e3e3e3e !important;
      }
  `]

})
export class PluginFieldEditComponent {
  @Input() type;
  @Input() value;
  @Input() field;
  @Input() placeholder;
  @Input() switchToHTMLEditor;
  @Output() editClicked: EventEmitter<PluginEditEvent> = new EventEmitter<PluginEditEvent>();
  @Output() changed: EventEmitter<PluginEditEvent> = new EventEmitter();
  @Input() validators;
  htmlEditorView = false;
  @ViewChild('HTMLEditorModal') HTMLEditorModal: AlertModal;
  HTMLForm: UntypedFormGroup = this._fb.group({
    value: this._fb.control("")
  });

  constructor(private _fb: UntypedFormBuilder) {
  }

  updateObject(value) {
    console.log(value)
    this.value = value;
    this.changed.emit({field: this.field, value: this.value, type: this.type});
  }

  openHTMLEditor() {
    this.HTMLForm.get('value').setValue(this.value)
    this.htmlEditorView = true;
    this.HTMLEditorModal.alertTitle = "HTML Editor";
    this.HTMLEditorModal.open();
  }

  closeHTMLEditor() {
    this.htmlEditorView = false;
    this.updateObject(this.HTMLForm.get('value').value)
  }
}
