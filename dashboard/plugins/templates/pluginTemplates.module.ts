import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AlertModalModule} from '../../../utils/modal/alertModal.module';
import {PluginTemplatesComponent} from './pluginTemplates.component';
import {AdminToolServiceModule} from "../../../services/adminToolService.module";
import {InputModule} from "../../../sharedComponents/input/input.module";


import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatFormFieldModule} from "@angular/material/form-field";


import {MatChipsModule} from '@angular/material/chips';
import {AdminTabsModule} from "../../sharedComponents/admin-tabs/admin-tabs.module";
import {PageContentModule} from "../../sharedComponents/page-content/page-content.module";
import {PluginTemplatesRoutingModule} from "./pluginTemplates-routing.module";
import {SearchInputModule} from "../../../sharedComponents/search-input/search-input.module";
import {IconsModule} from "../../../utils/icons/icons.module";
import {LoadingModule} from "../../../utils/loading/loading.module";
import {PluginsService} from "../../../services/plugins.service";
import {CKEditorModule} from "ng2-ckeditor";
import {PluginWrapperModule} from "../wrapper/plugin-wrapper.module";
import {PluginEditWrapperModule} from "../wrapper/plugin-edit-wrapper.module";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";

@NgModule({
  imports: [
    CommonModule, RouterModule, FormsModule,
    AlertModalModule, ReactiveFormsModule, AdminToolServiceModule, InputModule, MatAutocompleteModule, MatFormFieldModule, MatChipsModule,
    MatCheckboxModule, AdminTabsModule, PageContentModule, PluginTemplatesRoutingModule, SearchInputModule, IconsModule, LoadingModule, CKEditorModule, PluginWrapperModule, PluginEditWrapperModule, MatSlideToggleModule
  ],
  providers:[PluginsService],
    declarations: [PluginTemplatesComponent],
    exports: [PluginTemplatesComponent]
})
export class PluginTemplatesModule {}
