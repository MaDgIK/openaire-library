import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {PluginTemplatesComponent} from "./pluginTemplates.component";


@NgModule({
    imports: [
        RouterModule.forChild([
            { path: '',  component: PluginTemplatesComponent}
        ])
    ]
})
export class PluginTemplatesRoutingModule { }
