import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {PluginsComponent} from "./plugins.component";


@NgModule({
    imports: [
        RouterModule.forChild([
            { path: '',  component: PluginsComponent},
            { path: ':templateCode',  component: PluginsComponent, data: {templateView:true}}
        ])
    ]
})
export class PluginsRoutingModule { }
