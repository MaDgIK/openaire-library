import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {HelpContentService} from "../../services/help-content.service";
import {UntypedFormBuilder, ValidatorFn} from "@angular/forms";
import {Page} from "../../utils/entities/adminTool/page";
import {EnvProperties} from '../../utils/properties/env-properties';
import {HelperFunctions} from "../../utils/HelperFunctions.class";
import {Subscriber} from "rxjs";
import {properties} from "../../../../environments/environment";
import {PortalUtils} from "../portal/portalHelper";
import {Option} from "../../sharedComponents/input/input.component";
import {Title} from "@angular/platform-browser";
import {ClearCacheService} from "../../services/clear-cache.service";
import {NotificationHandler} from "../../utils/notification-handler";
import {PluginsService} from "../../services/plugins.service";
import {Plugin} from "../../utils/entities/adminTool/plugin";
import {StringUtils} from "../../utils/string-utils.class";
import {Portal} from "../../utils/entities/adminTool/portal";
import {PluginTemplate} from "../../utils/entities/adminTool/pluginTemplate";
import {PluginUtils} from "./utils/pluginUtils";
import {CommunityService} from "../../connect/community/community.service";
import {CommunityInfo} from "../../connect/community/communityInfo";
import {AlertModal} from "../../utils/modal/alert";
import {Session} from "../../login/utils/helper.class";
import {UserManagementService} from "../../services/user-management.service";

@Component({
  selector: 'plugins',
  templateUrl: './plugins.component.html',
})
export class PluginsComponent implements OnInit {
  public pluginsByPlacement: Map<string, { plugin: Plugin, template: PluginTemplate, openPreview: boolean }[]> = new Map();
  public plugins: Plugin[] = [];
  public pluginTemplates: PluginTemplate[] = [];
  public selectedTemplate: PluginTemplate = null;
  public selectedPlugin: Plugin = null;
  // public editView = false;
  // public selectTemplateView = false;
  // public templateForm: UntypedFormGroup;
  // public pagesCtrl: UntypedFormArray;
  urlValidator: ValidatorFn = StringUtils.urlValidator;
  private searchText: RegExp = new RegExp('');
  public keyword: string = "";
  public properties: EnvProperties = properties;
  // public formPages: Page[] = [];
  public showLoading: boolean = true;
  private subscriptions: any[] = [];
  public allPages: Option[] = [];
  public pluginUtils = new PluginUtils();
  selectedCommunityPid = null;
  public portalUtils: PortalUtils = new PortalUtils();
  private index: number;
  public portal: string;
  public selectedPageId: string;
  public community: Portal;
  public page: Page;
  public templateView = false;
  // public templateCode: string = null;
  // public template;
  public selectedPlacementView = "all";
  public selectedPluginIndex = null;
  public sinlgePlacementAvailable = false;
  communityInfo: CommunityInfo = null;
  // editSubmenuOpen = false;
  filterActive = false;
  @ViewChild('deleteModal') deleteModal: AlertModal;
  clearCacheRequests = [];
  user;
  constructor(private element: ElementRef, private route: ActivatedRoute, private _router: Router,
              private communityService: CommunityService,
              private title: Title, private _helpContentService: HelpContentService,
              private _pluginsService: PluginsService, private _fb: UntypedFormBuilder,
              private _clearCacheService: ClearCacheService, private _userManagementService:UserManagementService) {
  }

  ngOnInit() {
    this.title.setTitle('Administrator Dashboard | Plugins');
    this.subscriptions.push(this.communityService.getCommunityAsObservable().subscribe(
      community => {
        this.communityInfo = community;
      }));
    this.subscriptions.push(this._userManagementService.getUserInfo().subscribe( user =>{
      this.user = user;
    }))
    this.subscriptions.push(this.route.params.subscribe(params => {

      this.portal = (this.route.snapshot.data.portal) ? this.route.snapshot.data.portal : this.route.snapshot.params[this.route.snapshot.data.param];
      this.selectedCommunityPid = params.community;

      //initiate requests for clear cache prompt
      this.clearCacheRequests = [];
      if (properties.deleteBrowserCacheUrl) {
        this.clearCacheRequests.push(properties.deleteBrowserCacheUrl + "/" + this.selectedCommunityPid)
      }
      if (this.properties.deleteCacheUrl) {
      this.clearCacheRequests.push(properties.deleteCacheUrl + "?url=" + encodeURIComponent(properties.utilsService +
        "/portals/countResults?field=communityId&value=" + encodeURIComponent(this.selectedCommunityPid)));
      }
      console.log(this.clearCacheRequests)
      this.subscriptions.push(this.route.queryParams.subscribe(params => {
        HelperFunctions.scroll();
        this.selectedPageId = params['pageId'];
        if (this.portal && this.selectedPageId) {
          this.getPage(this.selectedPageId);
        }
        if (!this.selectedPageId && !this.templateView) {
          this._router.navigate(['../pages'], {relativeTo: this.route});
        }
      }));
    }));

  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(value => {
      if (value instanceof Subscriber) {
        value.unsubscribe();
      } else if (value instanceof Function) {
        value();
      }
    });
  }

  getTemplateById(id) {
    for (let template of this.pluginTemplates) {
      if (template._id == id) {
        return template;
      }
    }
    return null;
  }

  getPage(pageId: string) {
    this.showLoading = true;
    this.subscriptions.push(this._helpContentService.getPageByPortal(pageId, this.portal).subscribe(
      page => {
        if (this.properties.adminToolsPortalType != page.portalType) {
          this._router.navigate(['./pageContents']);
        } else {
          this.page = page;
          this.getPagePlugins();
        }
      },
      error => this.handleError('System error retrieving page', error)));
  }

  getPagePlugins() {
    this.showLoading = true;
    this.subscriptions.push(this._pluginsService.getPluginTemplatesByPage(this.selectedCommunityPid, this.selectedPageId).subscribe(
      templates => {
        this.pluginTemplates = templates;

        this.subscriptions.push(this._pluginsService.getPluginsByPage(this.selectedCommunityPid, this.selectedPageId).subscribe(
          plugins => {
            this.plugins = plugins;
            this.pluginsByPlacement = new Map();
            for (let pos of this.pluginUtils.placementsOptions) {
              this.pluginsByPlacement.set(pos.value, []);
            }
            let self = this;
            this.pluginTemplates.forEach(_ => {
              let plugin: Plugin = null;
              if(!_.custom) {


                for (let pl of plugins) {
                  if (pl.templateId == _._id) {
                    plugin = pl;
                  }
                }
                if (!plugin) {
                  plugin = new Plugin(this.selectedPageId, this.selectedCommunityPid, _);
                  this.plugins.push(plugin);
                }

                if (plugin) {
                  plugin.object = PluginUtils.initializeObjectAndCompare(_.code, plugin.object)
                  this.pluginsByPlacement.get(plugin.placement).push({plugin: plugin, template: _, openPreview: false});
                }
              }
            });
                 //add custom plugins in the list
                 this.plugins.forEach(_ => {

                   if(_.custom){
                     let customTemplate = null;
                     this.pluginTemplates.forEach(template => {
                       if (_.templateId == template._id) {
                         customTemplate = template;
                       }
                     });
                       if(customTemplate && customTemplate.custom){
                         this.pluginsByPlacement.get(customTemplate.placement).push({plugin: _, template: customTemplate, openPreview: false});
                       }
                       }
                     });
            let availablePlacements = [];
            for (let placement of this.pluginUtils.placementsOptions) {
              if (this.pluginsByPlacement.get(placement.value).length > 0) {
                availablePlacements.push(placement.value);
              }
              this.pluginsByPlacement.get(placement.value).sort(function (a, b) {
                return a.plugin.order - b.plugin.order;
              })
            }
            if (availablePlacements.length == 1) {
              this.selectedPlacementView = availablePlacements[0];
              this.sinlgePlacementAvailable = true
            }
            this.showLoading = false;
          },
          error => this.handleError('System error retrieving plugins', error)));

      },
      error => this.handleError('System error retrieving templates', error)));
  }

  public savePlugin(plugin, update, index) {
    this.subscriptions.push(this._pluginsService.savePlugin(plugin, this.selectedCommunityPid).subscribe(
      saved => {
        this.savedSuccessfully(saved, update, index);
        this.selectedTemplate = null;
        this.selectedPlugin = null;
        this.clearCache();
        if (plugin.custom) {
          this._router.navigate(["./edit"], {
            queryParams: {
              'pageId': this.selectedPageId,
              pluginId: saved._id,
              templateId: saved.templateId
            }, relativeTo: this.route
          })
        }
      },
      error => this.handleUpdateError("System error creating template", error)
    ));
  }

  public savedSuccessfully(plugin: Plugin, update: boolean, index) {
    console.log(plugin.placement, index, update)
    if (update) {
      this.pluginsByPlacement.get(plugin.placement)[index].plugin = plugin;
    } else {
      this.plugins.push(plugin);
    }
    this.showLoading = false;
  }

  public filterPlugins(plugin: Plugin, template: PluginTemplate): boolean {
    let values = [];
    for (let key of this.getKeys(plugin.settingsValues)) {
      values.push(plugin.settingsValues[key]);
    }
    return this.searchText.toString() == '' || (plugin.templateCode + ' ' + values.join(' ') + (template ? (template.name + ' ' + template.description) : '')).match(this.searchText) != null;
  }


  handleUpdateError(message: string, error = null) {
    if (error) {
      console.log('Server responded: ' + error);
    }
    NotificationHandler.rise(message, 'danger');
    this.showLoading = false;
  }

  handleError(message: string, error = null) {
    if (error) {
      console.log('Server responded: ' + error);
    }
    NotificationHandler.rise(message, 'danger');
    this.showLoading = false;
  }

  getPages() {
    this.showLoading = true;
    this.subscriptions.push(this._helpContentService.getAllPages().subscribe(
      pages => {
        this.allPages = [];
        pages.forEach(page => {
          this.allPages.push({
            label: page.name + " [" + page.portalType + "]",
            value: page
          });
        });
        this.showLoading = false;
      },
      error => this.handleError('System error retrieving pages', error)
    ));
  }

  attributeTypeChanged(form) {
    let type = form.get("value").get("type");
    form.get("value").setValue("");
    if (type == "boolean") {
      form.get("value").setValue(false);
    }
  }


  getKeys(obj) {
    return obj ? Object.keys(obj) : [];
  }


  public togglePlugin(status: boolean, id: string, i, placement) {
    this.index = i;
    this.selectedTemplate = this.pluginsByPlacement.get(placement)[i].template;
    if (id) {
      this.subscriptions.push(this._pluginsService.togglePlugin(id, status, this.selectedCommunityPid).subscribe(
        () => {

          this.pluginsByPlacement.get(placement)[i].plugin.active = status;
          this.clearCache();
        },
        error => this.handleUpdateError('System error changing the status of Plugin', error)
      ));
    } else {
      let plugin = this.pluginsByPlacement.get(placement)[i].plugin;
      plugin.active = status;
      this.savePlugin(plugin, true, i);
    }
  }

  public swap(pluginToMoveUp, pluginToMoveDown, placement) {
    this.showLoading = true;
    let moveUpGroup = this.pluginsByPlacement.get(placement)[pluginToMoveUp];
    let moveDownGroup = this.pluginsByPlacement.get(placement)[pluginToMoveDown];
    this.pluginsByPlacement.get(placement)[pluginToMoveUp] = moveDownGroup;
    this.pluginsByPlacement.get(placement)[pluginToMoveDown] = moveUpGroup;
    this.move(moveUpGroup.plugin, true, pluginToMoveDown, placement);
    this.move(moveDownGroup.plugin, false, pluginToMoveUp, placement);
    this.showLoading = false;
  }

  public move(plugin: Plugin, up: boolean, index, placement) {
    if (plugin._id) {
      this.subscriptions.push(this._pluginsService.updatePluginOrder(plugin, up ? -1 : 1, this.selectedCommunityPid).subscribe(
        saved => {
          this.pluginsByPlacement.get(placement)[index].plugin = saved;
          this.clearCache();
        },
        error => this.handleUpdateError("System error saving plugin", error)
      ));
    } else {

      plugin.order = plugin.order + (up ? -1 : 1)
      this.savePlugin(plugin, true, index);
    }


  }

  clearCache() {
    // this._clearCacheService.clearCacheInRoute(null, this.selectedCommunityPid, this.getPageById(this.selectedPageId).route)
    this._clearCacheService.purgeBrowserCache(null, this.selectedCommunityPid)
  }

  addNewCustomPlugin(template: PluginTemplate) {
    let plugin = new Plugin(this.page._id, this.selectedCommunityPid, template);
    plugin.order = this.pluginsByPlacement.get(this.selectedPlacementView).length;
    plugin.object = PluginUtils.initializeObjectAndCompare(template.code, null);
    plugin.custom = true;
    this.savePlugin(plugin, false, this.pluginsByPlacement.get(this.selectedPlacementView).length);
  }

  promtToDelete(i, placement) {
    this.selectedPlugin = this.pluginsByPlacement.get(placement)[i].plugin;
    this.selectedPlacementView = placement;
    this.selectedPluginIndex = i;
    this.deleteModal.alertTitle = 'Delete Confirmation';
    this.deleteModal.message = 'Are you sure you want to delete the selected plugin?';
    this.deleteModal.okButtonText = 'Yes';
    this.deleteModal.open();

  }

  confirmDelete() {
    this.showLoading = true;

    this.subscriptions.push(this._pluginsService.deletePlugin(this.selectedPlugin._id).subscribe(
      deleted => {
        this.pluginsByPlacement.get(this.selectedPlacementView).splice(this.selectedPluginIndex, 1);
        this.clearCache();
      },
      error => this.handleUpdateError("System error creating template", error)
    ));
    this.showLoading = false;
  }

  showClearCache(){
    return this.user && (Session.isPortalAdministrator(this.user) || Session.isCommunityCurator(this.user));
  }
}
