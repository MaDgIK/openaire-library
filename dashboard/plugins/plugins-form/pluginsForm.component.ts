import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {HelpContentService} from "../../../services/help-content.service";
import {FormArray, UntypedFormBuilder, UntypedFormGroup, ValidatorFn, Validators} from "@angular/forms";
import {Page} from "../../../utils/entities/adminTool/page";
import {EnvProperties} from '../../../utils/properties/env-properties';
import {HelperFunctions} from "../../../utils/HelperFunctions.class";
import {Subscriber} from "rxjs";
import {properties} from "../../../../../environments/environment";
import {Title} from "@angular/platform-browser";
import {ClearCacheService} from "../../../services/clear-cache.service";
import {NotificationHandler} from "../../../utils/notification-handler";
import {PluginsService} from "../../../services/plugins.service";
import {Plugin} from "../../../utils/entities/adminTool/plugin";
import {StringUtils} from "../../../utils/string-utils.class";
import {Portal} from "../../../utils/entities/adminTool/portal";
import {PluginTemplate} from "../../../utils/entities/adminTool/pluginTemplate";
import {PluginUtils} from "../utils/pluginUtils";
import {CommunityService} from "../../../connect/community/community.service";
import {CommunityInfo} from "../../../connect/community/communityInfo";
import {PluginEditEvent} from "../utils/base-plugin.form.component";
import {AlertModal} from "../../../utils/modal/alert";

@Component({
  selector: 'plugins-form',
  templateUrl: './pluginsForm.component.html',
})
export class PluginsFormComponent implements OnInit {
  public selectedTemplate: PluginTemplate = null;
  public selectedPlugin: Plugin = null;
  public templateForm: UntypedFormGroup;
  urlValidator: ValidatorFn = StringUtils.urlValidator;
  public properties: EnvProperties = properties;
  public showLoading: boolean = true;
  private subscriptions: any[] = [];
  public pluginUtils = new PluginUtils();
  selectedCommunityPid = null;
  public portal: string;
  public selectedPageId: string;
  public selectedTemplateId: string;
  public selectedPluginId: string;
  public community: Portal;
  public page: Page;
  public template;
  public selectedPlacementView = "all";
  communityInfo:CommunityInfo = null;
  editSubmenuOpen = false;
  @ViewChild('backAlert') backAlert: AlertModal;

  constructor(private element: ElementRef, private route: ActivatedRoute, private _router: Router,
              private communityService: CommunityService,
              private title: Title, private _helpContentService: HelpContentService,
              private _pluginsService: PluginsService, private _fb: UntypedFormBuilder,
              private _clearCacheService: ClearCacheService) {
  }

  ngOnInit() {
    this.title.setTitle('Administrator Dashboard | Plugins');
    this.subscriptions.push(this.communityService.getCommunityAsObservable().subscribe(
      community => {
        this.communityInfo = community;
      }));
    this.subscriptions.push(this.route.params.subscribe(params => {

      this.portal = (this.route.snapshot.data.portal) ? this.route.snapshot.data.portal : this.route.snapshot.params[this.route.snapshot.data.param];
      this.selectedCommunityPid = params.community;

      this.subscriptions.push(this.route.queryParams.subscribe(params => {
        HelperFunctions.scroll();
        this.selectedPageId = params['pageId'];
        this.selectedPluginId = params['pluginId'];
        this.selectedTemplateId = params['templateId'];

        if (this.portal && this.selectedPageId) {
          this.getPage(this.selectedPageId);
        }
        if (!this.selectedPageId) {
          this._router.navigate(['../'], {relativeTo: this.route});
        }
      }));
    }));

  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(value => {
      if (value instanceof Subscriber) {
        value.unsubscribe();
      } else if (value instanceof Function) {
        value();
      }
    });
  }

  getPage(pageId: string) {
    this.showLoading = true;
    this.subscriptions.push(this._helpContentService.getPageByPortal(pageId, this.portal).subscribe(
      page => {
        if (this.properties.adminToolsPortalType != page.portalType) {
          this._router.navigate(['..'],{ relativeTo: this.route});
        } else {
          this.page = page;
          this.getPluginAndTemplate();
        }
      },
      error => this.handleError('System error retrieving page', error)));
  }

  getPluginAndTemplate(){
    if(this.selectedTemplateId){
      this.subscriptions.push(this._pluginsService.getPluginTemplateById(this.selectedTemplateId).subscribe(template => {
        this.selectedTemplate = template;
        if(this.selectedPluginId){
          this.subscriptions.push(this._pluginsService.getPluginById(this.selectedPluginId).subscribe(plugin => {
            this.selectedPlugin = plugin;
            this.edit(this.selectedPlugin, this.selectedTemplate);
          }));
        }else{
          this.selectedPlugin = new Plugin(this.page._id,this.selectedCommunityPid, template);
          // this.selectedPlugin.order = this.pluginsByPlacement.get(this.selectedPlacementView).length;
          this.selectedPlugin.object = PluginUtils.initializeObjectAndCompare(template.code,this.selectedPlugin.object);
          this.edit(this.selectedPlugin, this.selectedTemplate);

        }
      }, error =>{
        this._router.navigate(['../'], {queryParams: {pageId:this.selectedPageId}, relativeTo: this.route});
      }));

    }else{
      this._router.navigate(['../'], {queryParams: {pageId:this.selectedPageId}, relativeTo: this.route});
    }
  }
  public edit(plugin:Plugin, template:PluginTemplate) {
    this.templateForm = this._fb.group({
      _id: this._fb.control(plugin._id),
      pid: this._fb.control(plugin.pid),
      page: this._fb.control(plugin.page),
      templateCode: this._fb.control(plugin.templateCode, Validators.required),
      templateId: this._fb.control(plugin.templateId, Validators.required),
      placement: this._fb.control(plugin.placement),
      order: this._fb.control(plugin.order),
      active: this._fb.control(plugin.active),
      values: this._fb.array([]),
    });

    if (template.settings) {
      for (let attrKey of Object.keys(template.settings)) {
        (this.templateForm.get("values") as FormArray).push(this._fb.group({
            'key': this._fb.control(attrKey),
            'value': this._fb.control(plugin.settingsValues[attrKey]?plugin.settingsValues[attrKey]:template.settings[attrKey].value)
          }
        ));
      }
    }
    this.showLoading = false;
  }

  public saveConfirmed() {
    this.showLoading = true;
    let plugin: Plugin = <Plugin>this.templateForm.getRawValue();
    plugin.object = this.selectedPlugin.object;
    plugin.settingsValues = new Map<string, string>();
    for (let fields of this.templateForm.getRawValue().values) {
      plugin.settingsValues[fields.key] = fields.value;
    }
    let update = (plugin._id) ? true : false;
    this.savePlugin(plugin,update)
  }
  public savePlugin(plugin, update){
    this.subscriptions.push(this._pluginsService.savePlugin(plugin, this.selectedCommunityPid ).subscribe(
      saved => {
        this._clearCacheService.purgeBrowserCache(null, this.selectedCommunityPid)
        if(!plugin._id){
          this._router.navigate([], {
            queryParams: { ['pluginId']: saved._id },
            queryParamsHandling: 'merge', // Keeps existing parameters
          });
        }
        this.edit(saved, this.selectedTemplate)

      },
      error => this.handleUpdateError("System error creating template", error)
    ));
  }




  handleUpdateError(message: string, error = null) {
    if (error) {
      console.log('Server responded: ' + error);
    }
    NotificationHandler.rise(message, 'danger');
    this.showLoading = false;
  }

  handleError(message: string, error = null) {
    if (error) {
      console.log('Server responded: ' + error);
    }
    NotificationHandler.rise(message, 'danger');
    this.showLoading = false;
  }

  get attrFormArray() {

    return this.templateForm.get("values") as FormArray;

  }

  attributeTypeChanged(form) {
    let type = form.get("value").get("type");
    form.get("value").setValue("");
    if (type == "boolean") {
      form.get("value").setValue(false);
    }
  }


  getKeys(obj) {
    return obj?Object.keys(obj):[];
  }

  reset() {
    this.edit(this.selectedPlugin, this.selectedTemplate)
  }

  pluginFieldChanged($event:PluginEditEvent){
    if($event.type == "open-submenu"){
      this.editSubmenuOpen = true;
      return;
    }
    if($event.type == "close-submenu"){

      this.editSubmenuOpen = false;
      return;
    }
    this.selectedPlugin.object[$event.field]=$event.value;
    this.templateForm.markAsDirty();
  }

  promtToGoBack() {
    if(this.templateForm.dirty) {
      this.backAlert.alertTitle = 'Leave page';
      this.backAlert.message = 'Are you sure you want to leave the page?';
      this.backAlert.okButtonText = 'Yes';
      this.backAlert.open();
    }else{
      this.confirmGoBack();
    }

  }

  confirmGoBack() {
    this._router.navigate(["../"],{queryParams:{pageId:this.selectedPageId},relativeTo:this.route})
  }
}
