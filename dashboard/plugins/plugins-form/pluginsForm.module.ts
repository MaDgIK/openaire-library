import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AdminToolServiceModule} from "../../../services/adminToolService.module";
import {InputModule} from "../../../sharedComponents/input/input.module";


import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatFormFieldModule} from "@angular/material/form-field";


import {MatChipsModule} from '@angular/material/chips';
import {AdminTabsModule} from "../../sharedComponents/admin-tabs/admin-tabs.module";
import {PageContentModule} from "../../sharedComponents/page-content/page-content.module";
import {SearchInputModule} from "../../../sharedComponents/search-input/search-input.module";
import {IconsModule} from "../../../utils/icons/icons.module";
import {LoadingModule} from "../../../utils/loading/loading.module";
import {PluginsService} from "../../../services/plugins.service";
import {CKEditorModule} from "ng2-ckeditor";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {PluginWrapperModule} from "../wrapper/plugin-wrapper.module";
import {SideBarModule} from "../../sharedComponents/sidebar/sideBar.module";
import {PluginEditWrapperModule} from "../wrapper/plugin-edit-wrapper.module";
import {PluginsFormComponent} from "./pluginsForm.component";
import {PluginsFormRoutingModule} from "./pluginsForm-routing.module";
import {AlertModalModule} from "../../../utils/modal/alertModal.module";


@NgModule({
  imports: [
    CommonModule, RouterModule, FormsModule,
    ReactiveFormsModule, AdminToolServiceModule, InputModule, MatAutocompleteModule, MatFormFieldModule, MatChipsModule,
    MatCheckboxModule, AdminTabsModule, PageContentModule, PluginsFormRoutingModule, SearchInputModule, IconsModule, LoadingModule, CKEditorModule,
    MatSlideToggleModule, PluginWrapperModule, SideBarModule, PluginEditWrapperModule, AlertModalModule
  ],
  providers:[PluginsService],
  declarations: [PluginsFormComponent],
    exports: [PluginsFormComponent]
})
export class PluginsFormModule {}
