import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {PluginsFormComponent} from "./pluginsForm.component";


@NgModule({
    imports: [
        RouterModule.forChild([
            { path: '',  component: PluginsFormComponent}
        ])
    ]
})
export class PluginsFormRoutingModule { }
