import {Component, Input, ViewChild} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {AlertModal} from "../../utils/modal/alert";
import {AlertModalModule} from "../../utils/modal/alertModal.module";
import {CommonModule} from "@angular/common";

@Component({
  standalone: true,
  selector: 'clear-cache',
  imports: [AlertModalModule, CommonModule],
  template: `
    <ng-container *ngIf="requests && requests.length > 0">
    <div class="uk-alert uk-padding-xsmall uk-text-small uk-text-center">
      Made changes but can't see them? 
      <a (click)="promtToClear()">Try to clear the cache</a> to resolve the issue.
    </div>
    
    <modal-alert #deleteCacheModal [overflowBody]="false" (alertOutput)="clear()"
                 classTitle="uk-background-primary uk-light"></modal-alert>

    </ng-container>
  `,
})
export class ClearCacheComponent {
  @Input() requests:string[] ;
  @ViewChild('deleteCacheModal') deleteCacheModal: AlertModal;

  constructor(private http: HttpClient ) {
  }
  promtToClear(){
    this.deleteCacheModal.alertTitle = 'Confirm Cache Clear';
    this.deleteCacheModal.message = 'Are you sure you want to clear the service cache?';
    this.deleteCacheModal.okButtonText = 'Yes';
    this.deleteCacheModal.open();

  }

  clear(){
    for( let request of this.requests){
      this.http.get(request).subscribe( res => {
        console.log( res);
      });
    }
  }


}
