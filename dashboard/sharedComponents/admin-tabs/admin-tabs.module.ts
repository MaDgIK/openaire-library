import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdminTabsComponent} from "./admin-tabs.component";
import {RouterModule} from "@angular/router";

@NgModule({
  declarations: [AdminTabsComponent],
  imports: [
    CommonModule, RouterModule
  ],
  exports:[AdminTabsComponent]
})
export class AdminTabsModule {
}
