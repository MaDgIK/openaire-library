import {Component, Input, OnInit} from '@angular/core';
import {Session, User} from "../../../login/utils/helper.class";
import {UserManagementService} from "../../../services/user-management.service";
import {Subscriber} from "rxjs";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'admin-tabs',
  template: `    
    <ul class="uk-tab uk-margin-remove-bottom uk-margin-medium-top uk-flex uk-flex-center uk-flex-left@m" uk-tab>
      <li *ngIf="isPortalAdmin && !portal" [class.uk-active]="tab === 'portal'"><a routerLink="../portals">Portals</a></li>
      <li [class.uk-active]="tab === 'page'"><a  routerLink="../pages">Pages</a></li>
      <li [class.uk-active]="tab === 'entity'"><a  routerLink="../entities">Entities</a></li>
	  <li *ngIf="portal && type === 'community'" [class.uk-active]="tab === 'menu'"><a  routerLink="../menu">Menus</a></li>
      <li *ngIf="isPortalAdmin  && !portal" [class.uk-active]="tab === 'class'"><a routerLink="../classes">Classes</a></li>
      <li *ngIf="isPortalAdmin  && !portal" [class.uk-active]="tab === 'template'"><a routerLink="../templates">Templates</a></li>
      <li *ngIf="isPortalAdmin  && portal=='connect'"  [class.uk-active]="tab === 'customization'"><a routerLink="../customization">Customization</a></li>
    </ul>
  `
})
export class AdminTabsComponent implements OnInit {
  @Input()
  public type: 'community' | 'stakeholder' = null;
  @Input()
  portal = null;
  @Input()
  public user: User;
  @Input()
  public tab: "portal" | "page" | "entity" | "menu" | "class" | "customization" | "template" = 'page';
  private subscriptions: any[] = [];
  
  constructor(private route: ActivatedRoute, private userManagementService: UserManagementService) {
  }

  ngOnInit() {
    this.subscriptions.push(this.userManagementService.getUserInfo().subscribe(user => {
      this.user = user;
    }));
  }
  
  ngOnDestroy(): void {
    this.subscriptions.forEach(value => {
      if (value instanceof Subscriber) {
        value.unsubscribe();
      } else if (value instanceof Function) {
        value();
      }
    });
  }

  public get isPortalAdmin() {
    return Session.isPortalAdministrator(this.user) || Session.isCurator(this.type, this.user);
  }

}
