import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {RouterModule} from "@angular/router";

import {SideBarComponent} from './sideBar.component';
import {UrlPrefixModule} from "../../../utils/pipes/url-prefix.module";
import {IconsModule} from "../../../utils/icons/icons.module";
import {SidebarContentComponent} from "./sidebar-content.component";

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    UrlPrefixModule,
    IconsModule
  ],
  declarations: [
    SideBarComponent,
    SidebarContentComponent
  ],
  providers: [],
  exports: [
    SideBarComponent,
    SidebarContentComponent
  ]
})
export class SideBarModule {
}
