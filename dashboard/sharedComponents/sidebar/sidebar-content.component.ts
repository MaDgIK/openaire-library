import {Component} from "@angular/core";
import {LayoutService} from "./layout.service";

@Component({
  selector: '[sidebar-content]',
  template: `
    <div id="sidebar_toggle" (click)="toggleOpen()"></div>
    <div id="sidebar_content" (mouseenter)="onMouseEnter()" (mouseleave)="onMouseLeave()">
      <ng-content></ng-content>
    </div>
  `
})
export class SidebarContentComponent {
  
  constructor(private layoutService: LayoutService) {
  }

  onMouseEnter() {
    this.layoutService.setHover(true);
  }

  onMouseLeave() {
    this.layoutService.setHover(false);
  }

  public toggleOpen() {
    this.layoutService.setOpen(!this.layoutService.open);
  }
}
