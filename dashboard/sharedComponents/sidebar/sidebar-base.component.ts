import {ChangeDetectorRef, Directive, OnInit} from "@angular/core";
import {BaseComponent} from "../../../sharedComponents/base/base.component";
import {MenuItem} from "../../../sharedComponents/menu";
import {LayoutService} from "./layout.service";

@Directive()
export class SidebarBaseComponent extends BaseComponent implements OnInit {
  hasSidebar: boolean = false;
  hasInternalSidebar: boolean = false;
  hasAdminMenu: boolean = false;
  /**
   * Menu Items
   * */
  sideBarItems: MenuItem[] = [];
  adminMenuItems: MenuItem[] = [];
  backItem: MenuItem = null;

  protected layoutService: LayoutService;
  protected cdr: ChangeDetectorRef;

  constructor() {
    super();
  }

  ngOnInit() {
    this.subscriptions.push(this.layoutService.hasSidebar.subscribe(hasSidebar => {
      this.hasSidebar = hasSidebar;
      this.cdr.detectChanges();
    }));
    this.subscriptions.push(this.layoutService.hasInternalSidebar.subscribe(hasInternalSidebar => {
      this.hasInternalSidebar = hasInternalSidebar;
      this.cdr.detectChanges();
    }));
    this.subscriptions.push(this.layoutService.hasAdminMenu.subscribe(hasAdminMenu => {
      this.hasAdminMenu = hasAdminMenu;
      this.cdr.detectChanges();
    }));
    this.layoutService.setOpen(true);
  }

  public get open() {
    return this.layoutService.open;
  }

  public get hover() {
    return this.layoutService.hover;
  }
}
