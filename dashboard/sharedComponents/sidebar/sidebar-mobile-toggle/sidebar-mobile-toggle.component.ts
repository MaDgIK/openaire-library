import {Component, OnDestroy, OnInit} from "@angular/core";
import {LayoutService, SidebarItem} from "../layout.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'sidebar-mobile-toggle',
  template: `
    <a *ngIf="activeSidebarItem" href="#sidebar_offcanvas" class="sidebar_mobile_toggle uk-link-reset uk-width-3-5 uk-flex uk-flex-middle" uk-toggle>
      <div *ngIf="activeSidebarItem.icon && (activeSidebarItem.icon.svg || activeSidebarItem.icon.name)" class="uk-width-auto">
        <icon class="menu-icon" [customClass]="activeSidebarItem.icon.class" [name]="activeSidebarItem.icon.name" ratio="0.8" [svg]="activeSidebarItem.icon.svg" [flex]="true"></icon>
      </div>
      <span class="uk-width-expand uk-text-truncate uk-margin-small-left uk-text-bolder">
          {{activeSidebarItem.name}}
        <span *ngIf="activeSidebarItem.subItem">- {{activeSidebarItem.subItem.name}}</span>
      </span>
      <div class="uk-width-auto uk-margin-small-left">
        <icon name="arrow_right" ratio="1.4"  [flex]="true"></icon>
      </div>
    </a>
  `
})
export class SidebarMobileToggleComponent implements OnInit, OnDestroy {
  public activeSidebarItem: SidebarItem;
  private subscriptions: any[] = [];
  
  constructor(private layoutService: LayoutService) {
  }
  
  ngOnInit() {
    this.subscriptions.push(this.layoutService.activeSidebarItem.subscribe(activeSidebarItem => {
      this.activeSidebarItem = activeSidebarItem;
    }));
  }
  
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if(subscription instanceof Subscription) {
        subscription.unsubscribe();
      }
    });
  }
}
