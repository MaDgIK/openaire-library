import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {SidebarMobileToggleComponent} from "./sidebar-mobile-toggle.component";
import {IconsModule} from "../../../../utils/icons/icons.module";

@NgModule({
  imports: [CommonModule, IconsModule],
  declarations: [SidebarMobileToggleComponent],
  exports: [SidebarMobileToggleComponent]
})
export class SidebarMobileToggleModule {}
