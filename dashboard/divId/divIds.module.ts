import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AlertModalModule} from '../../utils/modal/alertModal.module';
import {DivIdsComponent} from './divIds.component';
import {AdminToolServiceModule} from "../../services/adminToolService.module";
import {InputModule} from "../../sharedComponents/input/input.module";
import {AdminTabsModule} from "../sharedComponents/admin-tabs/admin-tabs.module";
import {PageContentModule} from "../sharedComponents/page-content/page-content.module";
import {ClassesRoutingModule} from "./classes-routing.module";
import {SearchInputModule} from "../../sharedComponents/search-input/search-input.module";
import {IconsModule} from "../../utils/icons/icons.module";
import {LoadingModule} from "../../utils/loading/loading.module";

@NgModule({
  imports: [
    ClassesRoutingModule,
    CommonModule, RouterModule, FormsModule,
    AlertModalModule, ReactiveFormsModule, AdminToolServiceModule, InputModule,
    AdminTabsModule, PageContentModule, SearchInputModule, IconsModule, LoadingModule
  ],
    declarations: [DivIdsComponent],
    exports: [DivIdsComponent]
})
export class DivIdsModule {}
