import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {DivIdsComponent} from "./divIds.component";


@NgModule({
    imports: [
        RouterModule.forChild([
            { path: '',  component: DivIdsComponent}
        ])
    ]
})
export class ClassesRoutingModule { }
