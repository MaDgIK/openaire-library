import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {PortalsComponent} from "./portals.component";

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '',  component: PortalsComponent}
    ])
  ]
})
export class PortalsRoutingModule { }
