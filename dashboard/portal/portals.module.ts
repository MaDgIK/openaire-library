import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PortalsComponent} from './portals.component';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AlertModalModule} from '../../utils/modal/alertModal.module';
import {AdminToolServiceModule} from "../../services/adminToolService.module";
import {InputModule} from "../../sharedComponents/input/input.module";
import {AdminTabsModule} from "../sharedComponents/admin-tabs/admin-tabs.module";
import {PageContentModule} from "../sharedComponents/page-content/page-content.module";
import {PortalsRoutingModule} from "./portals-routing.module";
import {IconsModule} from "../../utils/icons/icons.module";
import {SearchInputModule} from "../../sharedComponents/search-input/search-input.module";
import {LoadingModule} from "../../utils/loading/loading.module";

@NgModule({
  imports: [
    PortalsRoutingModule,
    CommonModule, FormsModule, AlertModalModule,
    ReactiveFormsModule,
    RouterModule, AdminToolServiceModule, InputModule, AdminTabsModule, PageContentModule, IconsModule, SearchInputModule, LoadingModule
  ],
  declarations: [PortalsComponent],
  exports: [PortalsComponent]
})
export class PortalsModule {
}
