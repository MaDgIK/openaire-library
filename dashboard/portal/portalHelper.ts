import {Option} from "../../sharedComponents/input/input.component";
import {StakeholderConfiguration} from "../../monitor-admin/utils/indicator-utils";

export class PortalUtils {

  portalTypes: Option[] = [
    {value: 'explore', label: 'Explore Portal'},
    {value: 'connect', label: 'Connect portal'},
    {value: 'monitor', label: 'Monitor portal'},
    {value: 'community', label: 'Community Gateway'},
    {value: 'funder', label: StakeholderConfiguration.ENTITIES.funder + ' ' + StakeholderConfiguration.ENTITIES.stakeholder},
    {value: 'ri', label: StakeholderConfiguration.ENTITIES.ri + ' ' + StakeholderConfiguration.ENTITIES.stakeholder},
    {value: 'organization', label: StakeholderConfiguration.ENTITIES.organization + ' ' + StakeholderConfiguration.ENTITIES.stakeholder},
    {value: 'project', label: StakeholderConfiguration.ENTITIES.project + ' ' + StakeholderConfiguration.ENTITIES.stakeholder},
    {value: 'country', label: StakeholderConfiguration.ENTITIES.country + ' ' + StakeholderConfiguration.ENTITIES.stakeholder},
  ];

}
