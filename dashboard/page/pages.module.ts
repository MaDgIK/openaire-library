import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AlertModalModule} from '../../utils/modal/alertModal.module';
import {PagesComponent} from './pages.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatChipsModule} from '@angular/material/chips';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {AdminToolServiceModule} from "../../services/adminToolService.module";
import {InputModule} from "../../sharedComponents/input/input.module";
import {AdminTabsModule} from "../sharedComponents/admin-tabs/admin-tabs.module";
import {PageContentModule} from "../sharedComponents/page-content/page-content.module";
import {PagesRoutingModule} from "./pages-routing.module";
import {SearchInputModule} from "../../sharedComponents/search-input/search-input.module";
import {IconsModule} from "../../utils/icons/icons.module";
import {LoadingModule} from "../../utils/loading/loading.module";
import {LogoUrlPipeModule} from "../../utils/pipes/logoUrlPipe.module";

@NgModule({
  imports: [
    PagesRoutingModule,
    CommonModule, RouterModule, FormsModule, AlertModalModule, ReactiveFormsModule, MatSlideToggleModule, AdminToolServiceModule, InputModule,
    MatAutocompleteModule, MatFormFieldModule, MatChipsModule, AdminTabsModule, PageContentModule, SearchInputModule, IconsModule, LoadingModule, LogoUrlPipeModule
  ],
    declarations: [PagesComponent],
    exports: [PagesComponent]
})
export class PagesModule {
}
