import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';
import {PagesComponent} from "./pages.component";


@NgModule({
    imports: [
        RouterModule.forChild([
            { path: '', component: PagesComponent}
        ])
    ]
})
export class PagesRoutingModule { }
