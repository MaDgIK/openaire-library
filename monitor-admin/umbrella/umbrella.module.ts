import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {PreviousRouteRecorder} from "../../utils/piwik/previousRouteRecorder.guard";
import {UmbrellaComponent} from "./umbrella.component";
import {PageContentModule} from "../../dashboard/sharedComponents/page-content/page-content.module";
import {IconsModule} from "../../utils/icons/icons.module";
import {AlertModalModule} from "../../utils/modal/alertModal.module";
import {LoadingModule} from "../../utils/loading/loading.module";
import {ReactiveFormsModule} from "@angular/forms";
import {InputModule} from "../../sharedComponents/input/input.module";
import {CanExitGuard} from "../../utils/can-exit.guard";
import {FullScreenModalModule} from "../../utils/modal/full-screen-modal/full-screen-modal.module";
import {LogoUrlPipeModule} from "../../utils/pipes/logoUrlPipe.module";
import {PagingModule} from "../../utils/paging.module";
import {FilteredStakeholdersComponent} from "./filtered-stakeholders.component";
import {SearchInputModule} from "../../sharedComponents/search-input/search-input.module";
import {TransitionGroupModule} from "../../utils/transition-group/transition-group.module";

@NgModule({
  declarations: [UmbrellaComponent, FilteredStakeholdersComponent],
  imports: [CommonModule, RouterModule.forChild([
    {path: '', component: UmbrellaComponent, canDeactivate: [PreviousRouteRecorder, CanExitGuard]}
  ]), PageContentModule, IconsModule, AlertModalModule, LoadingModule, ReactiveFormsModule, InputModule, FullScreenModalModule, LogoUrlPipeModule, PagingModule, SearchInputModule, TransitionGroupModule],
  exports: [UmbrellaComponent]
})
export class UmbrellaModule {}
