import {ChangeDetectorRef, Component, EventEmitter, Input, Output} from "@angular/core";
import {FilteredStakeholdersBaseComponent} from "../shared/filtered-stakeholders-base.component";

@Component({
  selector: 'filtered-stakeholders',
  template: `
      <div class="uk-section">
          <div class="uk-flex uk-flex-middle uk-flex-between uk-margin-bottom">
              <div *ngIf="stakeholderCategory" class="uk-flex uk-flex-middle">
                  <h4 class="uk-margin-remove">{{ stakeholderCategory.plural }}</h4>
                  <a *ngIf="stakeholderCategory.tooltip" class="uk-margin-small-left uk-button uk-button-link"
                     [attr.uk-tooltip]="stakeholderCategory.tooltip">
                      <icon name="info" [flex]="true" [ratio]="0.8"></icon>
                  </a>
              </div>
              <paging-no-load *ngIf="filteredStakeholders?.length > pageSize"
                              (pageChange)="updateCurrentPage($event)"
                              [currentPage]="currentPage" [size]="pageSize"
                              [totalResults]="filteredStakeholders.length">
              </paging-no-load>
          </div>
          <div class="uk-grid uk-grid-small uk-child-width-1-4@l uk-child-width-1-3@m uk-child-width-1-1 uk-grid-match"
               uk-grid>
              <div *ngFor="let stakeholder of filteredStakeholders.slice((currentPage-1)*pageSize, currentPage*pageSize)">
                  <div class="uk-card uk-card-default">
                      <div class="uk-card-body uk-position-relative">
                          <div class="uk-position-top-right uk-margin-small-right uk-margin-small-top">
                          <span class="uk-link-reset uk-flex uk-flex-middle">
                              <icon *ngIf="showVisibility" [flex]="true"
                                    [name]="stakeholderUtils.visibilityIcon.get(stakeholder.visibility)"
                                    ratio="0.6"></icon>
                          </span>
                          </div>
                          <div class="uk-display-block uk-text-center">
                              <div class="titleContainer uk-h6 uk-margin-remove-bottom uk-margin-top multi-line-ellipsis lines-2">
                                  <p *ngIf="stakeholder.name" class="uk-margin-remove">
                                      {{ stakeholder.name }}
                                  </p>
                              </div>
                              <div class="logoContainer uk-margin-top uk-flex uk-flex-column uk-flex-center uk-flex-middle">
                                  <img [src]="stakeholder | logoUrl" class="uk-blend-multiply"
                                       style="max-height: 60px;">
                              </div>
                          </div>
                      </div>
                      <div *ngIf="(add && !stakeholder['added']) || (remove && stakeholder['added'])" class="uk-card-footer uk-padding-remove-vertical">
                          <div class="uk-grid uk-grid-small uk-flex-nowrap uk-grid-divider uk-flex-right" uk-grid>
                              <div *ngIf="add && !stakeholder['added']">
                                  <div class="uk-padding-small uk-padding-remove-horizontal">
                                      <button class="uk-button uk-button-link uk-flex uk-flex-middle"
                                              (click)="added.emit(stakeholder._id)">
                                          <icon name="add" [flex]="true"></icon>
                                          <span class="uk-margin-xsmall-left">Add</span>
                                      </button>
                                  </div>
                              </div>
                              <div *ngIf="remove && stakeholder['added']">
                                  <div class="uk-padding-small uk-padding-remove-horizontal">
                                      <button class="uk-button uk-button-link uk-flex uk-flex-middle"
                                              (click)="removed.emit(stakeholder._id)">
                                          <icon name="delete" [flex]="true"></icon>
                                          <span class="uk-margin-xsmall-left">Remove</span>
                                      </button>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div *ngIf="filteredStakeholders.length === 0">
              <div class="uk-position-relative uk-section">
                  <h6 class="uk-position-center">No {{ entities.stakeholders }} have been found.</h6>
              </div>
          </div>
      </div>
  `
})
export class FilteredStakeholdersComponent extends FilteredStakeholdersBaseComponent {
  @Output()
  public added: EventEmitter<string> = new EventEmitter<string>();
  @Output()
  public removed: EventEmitter<string> = new EventEmitter<string>();
  @Input()
  public add: boolean = false;
  @Input()
  public remove: boolean = false;

  constructor(protected cdr: ChangeDetectorRef) {
    super();
  }
}
