import {Component, OnInit, QueryList, ViewChildren} from "@angular/core";
import {ManageStakeholders, StakeholderType} from "../../monitor/entities/stakeholder";
import {Session} from "../../login/utils/helper.class";
import {StakeholderBaseComponent} from "../utils/stakeholder-base.component";
import {StakeholderService} from "../../monitor/services/stakeholder.service";
import {UserManagementService} from "../../services/user-management.service";
import {ActivatedRoute} from "@angular/router";
import {Title} from "@angular/platform-browser";
import {Option} from "../../sharedComponents/input/input.component";
import {zip} from "rxjs";
import {StringUtils} from "../../utils/string-utils.class";
import {StakeholderCategory} from "../utils/indicator-utils";
import {ManageStakeholdersComponent} from "./manageStakeholders.component";

@Component({
  selector: `manage-all`,
  template: `
      <div page-content>
          <div header class="uk-margin-bottom">
              <sidebar-mobile-toggle class="uk-margin-top uk-hidden@m uk-display-block"></sidebar-mobile-toggle>
              <div *ngIf="stakeholderCategories.length > 1" class="uk-margin-remove-bottom uk-margin-medium-top">
                  <slider-tabs [type]="'dynamic'" (activeEmitter)="active = $event">
                      <slider-tab *ngFor="let category of stakeholderCategories" [tabTitle]="category.plural"
                                  [tabId]="category.value" [active]="active === category.value"></slider-tab>
                  </slider-tabs>
              </div>
          </div>
          <div actions>
              <div class="uk-section-xsmall">
                  <div class="uk-flex uk-flex-center uk-flex-wrap uk-flex-middle uk-flex-between@m">
                      <div class="uk-width-medium">
                          <div input type="select" placeholder="Type" [disabled]="loading"
                               [options]="types" [(value)]="type">
                          </div>
                      </div>
                      <div search-input [(value)]="keyword" [expandable]="true"
                           [disabled]="loading" [placeholder]="'Search ' + entities.stakeholders" searchInputClass="outer"
                           class="uk-width-1-3@xl uk-width-2-5@l uk-width-1-2@m uk-width-1-1 uk-flex uk-flex-right"></div>
                  </div>
              </div>
          </div>
          <div inner>
              <div *ngIf="loading" class="uk-margin-medium-top uk-padding-large message">
                  <loading></loading>
              </div>
              <div *ngIf="!loading" uk-height-match="target: .titleContainer; row: false">
                  <div uk-height-match="target: .logoContainer; row: false">
                      <div *ngIf="!isManager()" class="message">
                          <h4 class="uk-text-center">
                              No profiles to manage yet
                          </h4>
                      </div>
                      <ng-container *ngIf="isManager()">
                          <manage-stakeholders *ngFor="let category of categories"
                                               [(aliases)]="aliases"  [stakeholderCategory]="category"
                                               [user]="user" [keyword]="keyword" [type]="type"
                                               [defaultStakeholders]="manageStakeholders.templates"
                                               [(stakeholders)]="manageStakeholders[category.value]">
                          </manage-stakeholders>
                      </ng-container>
                  </div>
              </div>
          </div>
      </div>
  `
})
export class ManageAllComponent extends StakeholderBaseComponent implements OnInit {
  public loading: boolean = true;
  public stakeholderCategories: StakeholderCategory[] = this.stakeholderUtils.stakeholderCategories;
  public active: 'all' | 'templates' | 'standalone' | 'umbrella' | 'dependent' = 'all';
  public aliases: string[];
  public manageStakeholders: ManageStakeholders;
  public user = null;
  /**
   * Filters
   */
  public keyword: string;
  public type: StakeholderType | 'all' = 'all';
  public types: Option[] = [{value: 'all', label: 'All'}].concat(this.stakeholderUtils.types);
  @ViewChildren(ManageStakeholdersComponent) manageStakeholdersList: QueryList<ManageStakeholdersComponent>;

  public readonly StringUtils = StringUtils;

  constructor(private stakeholderService: StakeholderService,
              private userManagementService: UserManagementService,
              protected _route: ActivatedRoute,
              protected _title: Title) {
    super();
  }

  ngOnInit(): void {
    this.title = 'Manage Profiles';
    this.setMetadata();
    this.subscriptions.push(this.userManagementService.getUserInfo().subscribe(user => {
      this.user = user;
      if(!this.isManager()) {
        this.stakeholderCategories = [];
      } else if(!this.isCurator()) {
        this.stakeholderCategories = this.stakeholderCategories.filter(category => category.value !== 'templates');
      }
      this.active = this.stakeholderCategories[0].value;
    }));
    let data = zip(
        this.stakeholderService.getMyStakeholders(),
        this.stakeholderService.getAlias()
    );
    this.subscriptions.push(data.subscribe(res => {
      this.manageStakeholders = res[0];
      this.aliases = res[1];
      this.loading = false;
    }, error => {
      this.loading = false;
    }));
  }

  public isManager(): boolean {
    return this.isCurator() || (Session.isKindOfMonitorManager(this.user));
  }

  public isCurator(): boolean {
    return this.isAdmin() || Session.isMonitorCurator(this.user);
  }

  public isAdmin(): boolean {
    return Session.isPortalAdministrator(this.user);
  }

  get categories(): StakeholderCategory[] {
    return this.stakeholderCategories.filter(category => category.value !== 'all' &&
        (this.active === 'all' || category.value === this.active));
  }
}
