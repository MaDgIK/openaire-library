import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {InputModule} from "../../sharedComponents/input/input.module";
import {LoadingModule} from "../../utils/loading/loading.module";
import {AlertModalModule} from "../../utils/modal/alertModal.module";
import {ReactiveFormsModule} from "@angular/forms";
import {IconsModule} from "../../utils/icons/icons.module";
import {IconsService} from "../../utils/icons/icons.service";
import {earth, incognito, restricted} from "../../utils/icons/icons";
import {PageContentModule} from "../../dashboard/sharedComponents/page-content/page-content.module";
import {LogoUrlPipeModule} from "../../utils/pipes/logoUrlPipe.module";
import {SearchInputModule} from "../../sharedComponents/search-input/search-input.module";
import {
  SidebarMobileToggleModule
} from "../../dashboard/sharedComponents/sidebar/sidebar-mobile-toggle/sidebar-mobile-toggle.module";
import {SliderTabsModule} from "../../sharedComponents/tabs/slider-tabs.module";
import {EditStakeholderModule} from "../general/edit-stakeholder/edit-stakeholder.module";
import {PagingModule} from "../../utils/paging.module";
import {PreviousRouteRecorder} from "../../utils/piwik/previousRouteRecorder.guard";
import {ManageAllComponent} from "./manage-all.component";
import {ManageStakeholdersComponent} from "./manageStakeholders.component";

@NgModule({
  declarations: [ManageAllComponent, ManageStakeholdersComponent],
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: ManageAllComponent,
        canDeactivate: [PreviousRouteRecorder]
      }
    ]),
    CommonModule,
    RouterModule,
    InputModule,
    LoadingModule,
    AlertModalModule,
    ReactiveFormsModule,
    EditStakeholderModule,
    IconsModule,
    PageContentModule,
    LogoUrlPipeModule,
    SearchInputModule,
    SidebarMobileToggleModule,
    SliderTabsModule,
    PagingModule
  ],
  providers: [],
  exports: [ManageAllComponent]
})
export class ManageStakeholdersModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([earth, incognito, restricted]);
  }
}
