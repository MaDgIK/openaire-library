import {ChangeDetectorRef, Component, EventEmitter, Input, Output, ViewChild} from "@angular/core";
import {StakeholderService} from "../../monitor/services/stakeholder.service";
import {Stakeholder, Visibility} from "../../monitor/entities/stakeholder";
import {AlertModal} from "../../utils/modal/alert";
import {Session} from "../../login/utils/helper.class";
import {EditStakeholderComponent} from "../general/edit-stakeholder/edit-stakeholder.component";
import {CacheIndicatorsService} from "../utils/cache-indicators/cache-indicators.service";
import {NotificationHandler} from "../../utils/notification-handler";
import {FilteredStakeholdersBaseComponent} from "../shared/filtered-stakeholders-base.component";


declare var UIkit;

@Component({
  selector: 'manage-stakeholders',
  templateUrl: "manageStakeholders.component.html",
  styleUrls: ["manageStakeholders.component.less"]
})
export class ManageStakeholdersComponent extends FilteredStakeholdersBaseComponent {
  @Input()
  public defaultStakeholders: Stakeholder[];
  /**
   * List of aliases
   * */
  @Input()
  public aliases: string[];
  @Output()
  public aliasesChange = new EventEmitter<string[]>();
  /**
   * Edit/Create/Delete
   * */
  public message: string;
  public deleteLoading: boolean = false;
  public stakeholder: Stakeholder;
  public index: number;
  @Input()
  public user = null;
  public callback: Function;
  /**
   * Grid or List View
   */
  @ViewChild('editStakeholderModal', { static: true }) editStakeholderModal: AlertModal;
  @ViewChild('deleteStakeholderModal', { static: true }) deleteStakeholderModal: AlertModal;
  @ViewChild('editStakeholderComponent', { static: true }) editStakeholderComponent: EditStakeholderComponent;
  
  constructor(private stakeholderService: StakeholderService,
              private cacheIndicatorsService: CacheIndicatorsService,
              protected cdr: ChangeDetectorRef) {
    super();
  }
  
  ngOnInit(): void {
    super.ngOnInit();
    if(this.stakeholderCategory.value === 'templates') {
      this.message = 'Create a new ' + this.stakeholderCategory.name + ' profile.';
    } else {
      this.message = 'Create a new ' + this.stakeholderCategory.name +
          ' profile by selecting a type and generate indicators based on a default or a blank profile.';
    }
  }
  
  hide(element: any) {
    UIkit.dropdown(element).hide();
  }

  changed() {
    this.aliasesChange.emit(this.aliases);
    super.changed();
  }
  
  public editStakeholder(stakeholder: Stakeholder = null) {
    this.index = (stakeholder) ? this.stakeholders.findIndex(value => value._id === stakeholder._id) : -1;
    if (!stakeholder) {
      this.stakeholder = new Stakeholder(null, null, null,
        null, null, null, null, null);
    } else {
      this.stakeholder = stakeholder;
    }
    this.editStakeholderComponent.init(this.stakeholder, this.aliases, this.defaultStakeholders, this.stakeholderCategory, this.index === -1);
    if (this.index !== -1) {
      this.callback = (stakeholder: Stakeholder) => {
        let index: number = this.aliases.findIndex(value => value == this.stakeholders[this.index].alias);
        this.stakeholders[this.index] = stakeholder;
        if(index != -1) {
          this.aliases[index] = stakeholder.alias;
        }
				this.editStakeholderModal.cancel();
      };
      this.editStakeholderModal.alertTitle = 'Edit ' + this.stakeholder.name;
      this.editStakeholderModal.okButtonText = 'Save Changes';
    } else {
      this.callback = (stakeholder: Stakeholder) => {
        this.stakeholders.push(stakeholder);
        this.aliases.push(stakeholder.alias)
        this.changed();
				this.editStakeholderModal.cancel();
      };
      this.editStakeholderModal.alertTitle = 'Create a new ' + this.stakeholderCategory.name + ' profile';
      this.editStakeholderModal.okButtonText = 'Create';
    }
    this.editStakeholderModal.cancelButtonText = 'Cancel';
    this.editStakeholderModal.okButtonLeft = false;
    this.editStakeholderModal.alertMessage = false;
		this.editStakeholderModal.stayOpen = true;
    this.editStakeholderModal.open();
  }
  
  public createReport(stakeholder: Stakeholder) {
    this.cacheIndicatorsService.createReport(stakeholder.alias).subscribe(report => {
      NotificationHandler.rise('A caching process for ' + stakeholder.name + ' has been started.' )
    }, error => {
      console.log(error);
      NotificationHandler.rise(error.message(), 'danger');
    });
  }
  
  public deleteStakeholderOpen(stakeholder: Stakeholder) {
    this.stakeholder = stakeholder;
    this.deleteStakeholderModal.alertTitle = 'Delete ' + this.stakeholder.index_name;
    this.deleteStakeholderModal.cancelButtonText = 'No';
    this.deleteStakeholderModal.okButtonText = 'Yes';
		this.deleteStakeholderModal.alertMessage = false;
		this.deleteStakeholderModal.stayOpen = true;
    this.deleteStakeholderModal.open();
  }
  
  public deleteStakeholder() {
		this.deleteLoading = true;
    this.index = (this.stakeholder) ? this.stakeholders.findIndex(value => value._id === this.stakeholder._id) : -1;
    this.subscriptions.push(this.stakeholderService.deleteElement([this.stakeholder._id]).subscribe(() => {
      UIkit.notification(this.stakeholder.name+ ' has been <b>successfully deleted</b>', {
        status: 'success',
        timeout: 6000,
        pos: 'bottom-right'
      });
      this.stakeholders.splice(this.index, 1);
      this.aliases = this.aliases.filter(item => item !== this.stakeholder.alias);
      this.changed();
			this.deleteLoading = false;
			this.deleteStakeholderModal.cancel();
    }, error => {
      UIkit.notification('An error has occurred. Please try again later', {
        status: 'danger',
        timeout: 6000,
        pos: 'bottom-right'
      });
			this.deleteLoading = false;
			this.deleteStakeholderModal.cancel();
    }));
  }
  
  changeStakeholderStatus(stakeholder: Stakeholder, visibility: Visibility) {
    let path = [
      stakeholder._id
    ];
    this.subscriptions.push(this.stakeholderService.changeVisibility(path, visibility).subscribe(returnedElement => {
      stakeholder.visibility = returnedElement.visibility;
      UIkit.notification(stakeholder.name+ '\'s status has been <b>successfully changed</b> to ' + stakeholder.visibility.toLowerCase(), {
        status: 'success',
        timeout: 6000,
        pos: 'bottom-right'
      });
    }, error => {
      UIkit.notification('An error has occurred. Please try again later', {
        status: 'danger',
        timeout: 6000,
        pos: 'bottom-right'
      });
    }));
  }

  public isManager(stakeholder: Stakeholder): boolean {
    return this.isCurator() || (Session.isManager(stakeholder.type, stakeholder.alias, this.user));
  }

  public isCurator(): boolean {
    return this.isAdmin() || Session.isMonitorCurator(this.user);
  }

  public isAdmin(): boolean {
    return Session.isPortalAdministrator(this.user);
  }
}
