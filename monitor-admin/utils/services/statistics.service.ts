import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {SourceType} from "../../../monitor/entities/stakeholder";
import {IndicatorUtils} from "../indicator-utils";


@Injectable({
  providedIn: 'root'
})
export class StatisticsService {
  
  indicatorsUtils = new IndicatorUtils();
  
  constructor(private http: HttpClient) {}
  
  getNumbers(source: SourceType, url: string): Observable<any> {
    if (source !== null) {
      return this.http.get<any>(this.indicatorsUtils.getNumberUrl(source, url));
    } else {
      return this.http.get<any>(url);
    }
  }
}
