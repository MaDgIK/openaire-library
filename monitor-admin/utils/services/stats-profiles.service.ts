import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {properties} from "src/environments/environment";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class StatsProfilesService {

  constructor(private http: HttpClient) {
  }

  getStatsProfiles(): Observable<string[]> {
    return this.http.get<any[]>(properties.monitorStatsFrameUrl + 'schema/profiles')
      .pipe(map(profiles => profiles.map(profile => profile.name)));
  }
}
