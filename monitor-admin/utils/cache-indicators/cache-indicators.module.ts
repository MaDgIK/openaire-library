import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {CacheIndicatorsComponent} from "./cache-indicators.component";
import {IconsModule} from "../../../utils/icons/icons.module";

@NgModule({
  imports: [CommonModule, IconsModule],
  declarations: [CacheIndicatorsComponent],
  exports: [CacheIndicatorsComponent]
})
export class CacheIndicatorsModule {}
