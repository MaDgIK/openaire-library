import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {properties} from "src/environments/environment";
import {CustomOptions} from "../../../services/servicesUtils/customOptions.class";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class CacheIndicatorsService {

  constructor(private http: HttpClient) {
  }

  createReport(alias: string) {
    return this.http.post<any>(properties.domain + properties.baseLink + '/cache/' + alias, {}, CustomOptions.registryOptions())
        .pipe(map(res => res.report));
  }
  
  getReport(alias: string) {
    return this.http.get<any>(properties.domain + properties.baseLink + '/cache/' + alias, CustomOptions.registryOptions())
        .pipe(map(res => res.report));
  }
}
