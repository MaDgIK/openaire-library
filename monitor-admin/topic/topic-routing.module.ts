import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {PreviousRouteRecorder} from '../../utils/piwik/previousRouteRecorder.guard';
import {TopicComponent} from "./topic.component";
import {CanExitGuard} from "../../utils/can-exit.guard";

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: TopicComponent,
        canDeactivate: [PreviousRouteRecorder, CanExitGuard]
      }
    ])
  ]
})
export class TopicRoutingModule {
}
