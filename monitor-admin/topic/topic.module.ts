import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {TopicComponent} from "./topic.component";
import {TopicRoutingModule} from "./topic-routing.module";
import {RouterModule} from "@angular/router";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {IndicatorsComponent} from "./indicators.component";
import {AlertModalModule} from "../../utils/modal/alertModal.module";
import {InputModule} from "../../sharedComponents/input/input.module";
import {ClickModule} from "../../utils/click/click.module";
import {IconsService} from "../../utils/icons/icons.service";
import {earth, incognito, restricted} from "../../utils/icons/icons";
import {IconsModule} from "../../utils/icons/icons.module";
import {PageContentModule} from "../../dashboard/sharedComponents/page-content/page-content.module";
import {LoadingModule} from "../../utils/loading/loading.module";
import {NotifyFormModule} from "../../notifications/notify-form/notify-form.module";
import {LogoUrlPipeModule} from "../../utils/pipes/logoUrlPipe.module";
import {TransitionGroupModule} from "../../utils/transition-group/transition-group.module";
import {NumberRoundModule} from "../../utils/pipes/number-round.module";
import {SideBarModule} from "../../dashboard/sharedComponents/sidebar/sideBar.module";
import {SidebarMobileToggleModule} from "../../dashboard/sharedComponents/sidebar/sidebar-mobile-toggle/sidebar-mobile-toggle.module";
import {SliderTabsModule} from "../../sharedComponents/tabs/slider-tabs.module";

@NgModule({
  imports: [
    CommonModule, TopicRoutingModule, ClickModule, RouterModule, FormsModule, AlertModalModule,
    ReactiveFormsModule, InputModule, IconsModule, PageContentModule, LoadingModule, NotifyFormModule, LogoUrlPipeModule, TransitionGroupModule, NumberRoundModule, SideBarModule, SidebarMobileToggleModule, SliderTabsModule
  ],
  declarations: [
    TopicComponent, IndicatorsComponent
  ],
  providers: [],
  exports: [
    TopicComponent
  ]
})
export class TopicModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([earth, incognito, restricted]);
  }
}
