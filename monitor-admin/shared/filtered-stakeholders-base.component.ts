import {
  ChangeDetectorRef,
  Directive,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges
} from "@angular/core";
import {StakeholderBaseComponent} from "../utils/stakeholder-base.component";
import {StakeholderCategory} from "../utils/indicator-utils";
import {Stakeholder, StakeholderType} from "../../monitor/entities/stakeholder";

@Directive()
export class FilteredStakeholdersBaseComponent extends StakeholderBaseComponent implements OnInit, OnChanges {
  /**
   * Filtering
   * */
  @Input()
  public keyword: string;
  @Input()
  public type: StakeholderType | 'all' = 'all';
  /** Stakeholders */
  @Input()
  public stakeholderCategory: StakeholderCategory;
  @Input()
  public stakeholders: Stakeholder[];
  @Output()
  public stakeholdersChange: EventEmitter<Stakeholder[]> = new EventEmitter<Stakeholder[]>();
  /**
   *  Filtered Stakeholders
   */
  public filteredStakeholders: Stakeholder[] = [];
  public currentPage: number = 1;
  public pageSize: number = 16;

  protected cdr: ChangeDetectorRef

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.filterByKeyword();
    this.filterByType();
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes.type) {
      this.filterByType();
    }
    if(changes.keyword) {
      this.filterByKeyword();
    }
    if(changes.stakeholders) {
      this.filterByKeyword();
      this.filterByType();
      this.cdr.detectChanges();
    }
  }

  changed() {
    this.stakeholdersChange.emit(this.stakeholders);
    this.filterByType();
    this.filterByKeyword();
  }

  public filterByType() {
    if(this.type == 'all') {
      this.filteredStakeholders = this.stakeholders;
    } else {
      this.filteredStakeholders = this.stakeholders.filter(item => item.type == this.type);
    }
    this.currentPage = 1;
  }

  public filterByKeyword(): void{
    if (!this.keyword) {
      this.filteredStakeholders = this.stakeholders;
    } else {
      this.filteredStakeholders = this.stakeholders.filter(stakeholder =>
          stakeholder.name?.toLowerCase().includes(this.keyword.toLowerCase()) ||
          stakeholder.alias?.toLowerCase().includes(this.keyword.toLowerCase()) ||
          stakeholder.index_id?.toLowerCase().includes(this.keyword.toLowerCase()) ||
          stakeholder.index_shortName?.toLowerCase().includes(this.keyword.toLowerCase()) ||
          stakeholder.index_name?.toLowerCase().includes(this.keyword.toLowerCase())
      );
    }
    this.currentPage = 1;
  }

  public updateCurrentPage($event) {
    this.currentPage = $event.value;
  }
}
