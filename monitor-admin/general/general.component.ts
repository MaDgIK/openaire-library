import {ChangeDetectorRef, Component, OnInit, ViewChild} from "@angular/core";
import {StakeholderService} from "../../monitor/services/stakeholder.service";
import {Stakeholder} from "../../monitor/entities/stakeholder";
import {zip} from "rxjs";
import {EditStakeholderComponent} from "./edit-stakeholder/edit-stakeholder.component";
import {Title} from "@angular/platform-browser";
import {BaseComponent} from "../../sharedComponents/base/base.component";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'general',
  templateUrl: "./general.component.html"
})
export class GeneralComponent extends BaseComponent implements OnInit {
  public stakeholder: Stakeholder;
  public alias: string[];
  public loading: boolean = false;
  @ViewChild('editStakeholderComponent') editStakeholderComponent: EditStakeholderComponent;

  constructor(private stakeholderService: StakeholderService,
              private cdr: ChangeDetectorRef,
              protected _route: ActivatedRoute,
              protected _title: Title) {
    super();
  }

  ngOnInit() {
    this.loading = true;
    this.subscriptions.push(this.stakeholderService.getStakeholderAsObservable().subscribe(stakeholder => {
      this.stakeholder = stakeholder;
      this.cdr.detectChanges();
      if(this.stakeholder) {
        this.title = this.stakeholder.name + " | General";
        this.setMetadata();
        this.subscriptions.push(this.stakeholderService.getAlias().subscribe(alias => {
          this.alias = alias;
          this.reset();
          this.loading = false;
        }));
      }
    }));
  }

  public reset() {
    this.editStakeholderComponent.init(this.stakeholder, this.alias, [], null, false, true)
  }

  public save() {
    this.loading = true;
    this.editStakeholderComponent.save(stakeholder => {
      this.stakeholder = stakeholder;
      this.stakeholderService.setStakeholder(this.stakeholder);
      this.reset();
      this.loading = false;
    }, (error) => {
      console.error(error);
      this.loading = false;
    });
  }
}
