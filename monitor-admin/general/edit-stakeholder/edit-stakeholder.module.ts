import {NgModule} from "@angular/core";
import {EditStakeholderComponent} from "./edit-stakeholder.component";
import {CommonModule} from "@angular/common";
import {InputModule} from "../../../sharedComponents/input/input.module";
import {ReactiveFormsModule} from "@angular/forms";
import {IconsModule} from "../../../utils/icons/icons.module";
import {NotifyFormModule} from "../../../notifications/notify-form/notify-form.module";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";

@NgModule({
  imports: [CommonModule, InputModule, ReactiveFormsModule, IconsModule, NotifyFormModule, MatSlideToggleModule],
  declarations: [EditStakeholderComponent],
  exports: [EditStakeholderComponent]
})
export class EditStakeholderModule {}
