import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {PreviousRouteRecorder} from '../../utils/piwik/previousRouteRecorder.guard';
import {GeneralComponent} from "./general.component";

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: GeneralComponent,
        canDeactivate: [PreviousRouteRecorder],
        data: {hasSidebar: true}
      }
    ])
  ]
})
export class GeneralRoutingModule {
}
