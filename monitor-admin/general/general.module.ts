import {NgModule} from "@angular/core";
import {GeneralComponent} from "./general.component";
import {GeneralRoutingModule} from "./general-routing.module";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {InputModule} from "../../sharedComponents/input/input.module";
import {LoadingModule} from "../../utils/loading/loading.module";
import {AlertModalModule} from "../../utils/modal/alertModal.module";
import {ReactiveFormsModule} from "@angular/forms";
import {EditStakeholderModule} from "./edit-stakeholder/edit-stakeholder.module";
import {PageContentModule} from "../../dashboard/sharedComponents/page-content/page-content.module";
import {LogoUrlPipeModule} from "../../utils/pipes/logoUrlPipe.module";
import {
  SidebarMobileToggleModule
} from "../../dashboard/sharedComponents/sidebar/sidebar-mobile-toggle/sidebar-mobile-toggle.module";

@NgModule({
  declarations: [GeneralComponent],
  imports: [
    GeneralRoutingModule,
    CommonModule,
    RouterModule,
    InputModule,
    LoadingModule,
    AlertModalModule,
    ReactiveFormsModule,
    EditStakeholderModule,
    PageContentModule,
    LogoUrlPipeModule,
    SidebarMobileToggleModule
  ],
  providers: [],
  exports: [GeneralComponent]
})
export class GeneralModule {

}
