import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RoleVerificationComponent} from "./role-verification.component";
import {AlertModalModule} from "../utils/modal/alertModal.module";
import {ReactiveFormsModule} from "@angular/forms";
import {LoadingModule} from "../utils/loading/loading.module";
import {InputModule} from "../sharedComponents/input/input.module";
import {EmailService} from "../utils/email/email.service";
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [CommonModule, AlertModalModule, ReactiveFormsModule, LoadingModule, InputModule, RouterModule],
  declarations: [RoleVerificationComponent],
  exports: [RoleVerificationComponent],
  providers: [EmailService]
})
export class RoleVerificationModule {}
