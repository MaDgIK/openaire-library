import {Component, Input, ViewChild} from '@angular/core';

import {AlertModal} from '../../utils/modal/alert';

@Component({
    selector: 'start-over',
    template: `
    <!--button   (click)="confirmOpen()"  class="uk-button portal-button uk-align-left"  >  <span  class="uk-icon"><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" icon="refresh" ratio="1"><path fill="none" stroke="#000" stroke-width="1.1" d="M17.08,11.15 C17.09,11.31 17.1,11.47 17.1,11.64 C17.1,15.53 13.94,18.69 10.05,18.69 C6.16,18.68 3,15.53 3,11.63 C3,7.74 6.16,4.58 10.05,4.58 C10.9,4.58 11.71,4.73 12.46,5"></path><polyline fill="none" stroke="#000" points="9.9 2 12.79 4.89 9.79 7.9"></polyline></svg></span> Clear All</button-->

    <span  (click)="confirmOpen()" [attr.uk-tooltip]="'title:<div  >Remove '+message+'</div>'" aria-expanded="false" class="uk-width-small" [style]="'cursor: '+ ((results.length > 0)?'pointer;':'not-allowed;')" [style.cursor]="((results.length> 0)?'pointer':'not-allowed')">
      <span class="uk-icon"  >
        <svg width="16" height="16" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" icon="trash" ratio="0.8"><polyline fill="none" stroke="#000" points="6.5 3 6.5 1.5 13.5 1.5 13.5 3"></polyline><polyline fill="none" stroke="#000" points="4.5 4 4.5 18.5 15.5 18.5 15.5 4"></polyline><rect x="8" y="7" width="1" height="9"></rect><rect x="11" y="7" width="1" height="9"></rect><rect x="2" y="3" width="16" height="1"></rect></svg>
      </span> Remove all
    </span>

    <modal-alert (alertOutput)="confirmClose($event)">
    </modal-alert>
    `,

})
export class StartOverComponent {
  constructor () {
  }
  ngOnInit() {

  }

  @Input() public type:string;
  @Input() public linkTo:string;
  @Input() public results;
  @Input() public message = "selected entities";

  @ViewChild(AlertModal) alertApplyAll;
  @Input()  localStoragePrefix:string = "";

  confirmOpen(){
    if( this.results.length == 0 ){
      return;
    }
    this.alertApplyAll.cancelButton = true;
    this.alertApplyAll.okButton = true;
    this.alertApplyAll.alertTitle = "Remove entities";
    this.alertApplyAll.message = "Do you want to remove " + this.message + "?";
    this.alertApplyAll.okButtonText = "Yes";
    this.alertApplyAll.cancelButtonText = "No";
    this.alertApplyAll.open();
    this.alertApplyAll.okButtonLeft = false;
  }
   confirmClose(data){
     this.startOver();
  }
  startOver(){
    if(this.type != null && this.linkTo != null){
      //console.log("inline");
      //TODO
      // if(this.linkTo == "project"){
      //   this.projects.splice(0, this.projects.length);
      // }else if(this.linkTo == "context"){
      //   this.contexts.splice(0, this.contexts.length);
      // }else if(this.linkTo == "entity"){
      //     this.results.splice(0, this.results.length);
      // }
      }else{
        this.results.splice(0, this.results.length);
      }
      localStorage.removeItem(this.localStoragePrefix);
  }

}
