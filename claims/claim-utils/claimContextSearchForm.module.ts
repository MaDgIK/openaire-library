import { NgModule } from '@angular/core';

import { SharedModule } from '../../../openaireLibrary/shared/shared.module';
import { ClaimContextSearchFormComponent } from './claimContextSearchForm.component';
 import {StaticAutocompleteModule} from '../../utils/staticAutoComplete/staticAutoComplete.module';
 import { RouterModule } from '@angular/router';
 import {HelperModule} from '../../utils/helper/helper.module';
import {ClaimProjectsSearchFormModule} from "./claimProjectSearchForm.module";
import {AdvancedSearchInputModule} from "../../sharedComponents/advanced-search-input/advanced-search-input.module";
import {InputModule} from "../../sharedComponents/input/input.module";
import {CommunitiesService} from "../../connect/communities/communities.service";
import {LogoUrlPipeModule} from "../../utils/pipes/logoUrlPipe.module";
import {IconsModule} from "../../utils/icons/icons.module";
import {AlertModalModule} from "../../utils/modal/alertModal.module";

@NgModule({
  imports: [
    SharedModule, RouterModule,
    StaticAutocompleteModule,
    HelperModule, ClaimProjectsSearchFormModule, AdvancedSearchInputModule, InputModule, LogoUrlPipeModule, IconsModule, AlertModalModule

  ],
  providers: [CommunitiesService],
  declarations: [
    ClaimContextSearchFormComponent
  ], exports: [ClaimContextSearchFormComponent ]
})
export class ClaimContextSearchFormModule { }
