import { NgModule } from '@angular/core';

import { SharedModule } from '../../../openaireLibrary/shared/shared.module';
import { CommonModule }        from '@angular/common';

import {ClaimResultsComponent} from './claimResults.component';
import {ClaimEntitiesMetadataModule} from "../linking/selected/ClaimEntitiesMetadata.module";
import {IconsModule} from "../../utils/icons/icons.module";

 @NgModule({
   imports: [
     SharedModule, CommonModule, ClaimEntitiesMetadataModule, IconsModule
  
   ],
  providers:[
  ],
  declarations: [
    ClaimResultsComponent

  ],
  exports: [ClaimResultsComponent ]
})
export class ClaimResultsModule { }
