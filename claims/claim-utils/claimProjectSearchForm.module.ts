import {NgModule} from '@angular/core';

import {SharedModule} from '../../shared/shared.module';
import {CommonModule} from '@angular/common';

import {ClaimProjectsSearchFormComponent} from './claimProjectSearchForm.component';
import {ProjectServiceModule} from '../../landingPages/project/projectService.module';
import {ProjectsServiceModule} from '../../services/projectsService.module';
import {EntitiesAutocompleteModule} from '../../utils/entitiesAutoComplete/entitiesAutoComplete.module';
import {HelperModule} from '../../utils/helper/helper.module';

import {ClaimResultsModule} from './claimResults.module';

import {PagingModule} from '../../utils/paging.module';
import {SearchFilterModule} from '../../searchPages/searchUtils/searchFilter.module';
import {RangeFilterModule} from "../../utils/rangeFilter/rangeFilter.module";
import {AdvancedSearchInputModule} from "../../sharedComponents/advanced-search-input/advanced-search-input.module";
import {InputModule} from "../../sharedComponents/input/input.module";
import {DropdownFilterModule} from "../../utils/dropdown-filter/dropdown-filter.module";
import {LoadingModule} from "../../utils/loading/loading.module";

@NgModule({
  imports: [
    SharedModule, CommonModule,
    // LoadingModalModule,
    ProjectServiceModule, ProjectsServiceModule, EntitiesAutocompleteModule, HelperModule,
    PagingModule, SearchFilterModule, ClaimResultsModule, RangeFilterModule, AdvancedSearchInputModule, InputModule, DropdownFilterModule, LoadingModule
  ],
  providers:[
  ],
   declarations: [
     ClaimProjectsSearchFormComponent,
   ],
   exports: [ClaimProjectsSearchFormComponent]
 })
export class ClaimProjectsSearchFormModule { }
