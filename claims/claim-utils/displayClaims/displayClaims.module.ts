import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatChipsModule } from '@angular/material/chips';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import {ClaimServiceModule} from '../service/claimsService.module';
import {DisplayClaimsComponent} from './displayClaims.component';
import {LoadingModalModule} from '../../../utils/modal/loadingModal.module';
import {AlertModalModule} from '../../../utils/modal/alertModal.module';
import {ClaimEntityFormatterModule} from '../entityFormatter/claimEntityFormatter.module';
import {PagingModule } from '../../../utils/paging.module';
import {HelperModule} from '../../../utils/helper/helper.module';
import {Schema2jsonldModule} from '../../../sharedComponents/schema2jsonld/schema2jsonld.module';
import { SEOServiceModule } from '../../../sharedComponents/SEO/SEOService.module';
import {IndexInfoServiceModule}  from "../../../utils/indexInfoService.module";
import {SearchInputModule} from '../../../sharedComponents/search-input/search-input.module';
import {InputModule} from '../../../sharedComponents/input/input.module';
import {LoadingModule} from '../../../utils/loading/loading.module';
import {NoLoadPaging} from "../../../searchPages/searchUtils/no-load-paging.module";
import {IconsModule} from "../../../utils/icons/icons.module";
import {DropdownFilterModule} from "../../../utils/dropdown-filter/dropdown-filter.module";
import {IconsService} from "../../../utils/icons/icons.service";
import {link} from "../../../utils/icons/icons";

@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule, ClaimServiceModule, LoadingModalModule, AlertModalModule,
    ClaimEntityFormatterModule, PagingModule, HelperModule, Schema2jsonldModule, SEOServiceModule,
    IndexInfoServiceModule, MatSelectModule, SearchInputModule, MatAutocompleteModule, MatChipsModule, MatFormFieldModule, MatSlideToggleModule, InputModule, LoadingModule, NoLoadPaging, IconsModule, DropdownFilterModule
  
  ],
  declarations: [
    DisplayClaimsComponent

  ],
  exports: [
    DisplayClaimsComponent
   ]
})
export class DisplayClaimsModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([link])
  }
}
