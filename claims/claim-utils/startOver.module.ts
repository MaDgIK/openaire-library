import { NgModule } from '@angular/core';
import { SharedModule } from '../../../openaireLibrary/shared/shared.module';
import { CommonModule }        from '@angular/common';
import {StartOverComponent} from './startOver.component';
import {AlertModalModule} from '../../utils/modal/alertModal.module';

 @NgModule({
  imports: [
    SharedModule, CommonModule, AlertModalModule
  ],
  declarations: [
     StartOverComponent
  ],
  exports: [StartOverComponent ]
})
export class StartOverModule { }
