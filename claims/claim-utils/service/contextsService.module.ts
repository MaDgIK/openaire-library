import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';

import {ContextsService} from './contexts.service';


@NgModule({
  imports: [
    CommonModule, FormsModule
  ],
  declarations: [
  ],
  providers:[
  ContextsService
],
  exports: [
    ]
})
export class ContextsServiceModule { }
