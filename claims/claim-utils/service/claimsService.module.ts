import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';

import {ClaimsService} from './claims.service';


@NgModule({
  imports: [
    CommonModule, FormsModule
  ],
  declarations: [
  ],
  providers:[
  ClaimsService,

],
  exports: [
    ]
})


export class ClaimServiceModule { }
