import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';

import {SearchDataciteService} from './searchDatacite.service';


@NgModule({
  imports: [
    CommonModule, FormsModule
  ],
  declarations: [
  ],
  providers:[
  SearchDataciteService
],
  exports: [
    ]
})
export class SearchDataciteServiceModule { }
