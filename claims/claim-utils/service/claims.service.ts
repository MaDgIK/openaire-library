import {throwError as observableThrowError} from 'rxjs';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {CustomOptions} from '../../../services/servicesUtils/customOptions.class';

@Injectable()
export class ClaimsService {
  constructor(private http: HttpClient ) {
  }

  private getClaimRequest(size : number, page : number, url :string, fromCache:boolean):any {
       return this.http.get(url, CustomOptions.getAuthOptionsWithBody());
  }
  getClaims( size : number, page : number, keyword:string, sortby: string, descending: boolean, types: string, apiUrl:string, mine:boolean):any {
    let url = apiUrl +"claims"+"?offset="+(size*(page-1) + "&limit="+size)+"&keyword="+keyword+"&sortby="+sortby+"&descending="+descending+(types.length>0?"&"+types:types) + (mine?"&mine=true":"") + (mine?"&mine=true":"");
     return this.getClaimRequest(size,page,url,true);

  }
  getClaimsByUser( size : number, page : number, user:string, keyword:string, sortby: string, descending: boolean, types: string, apiUrl:string):any {
    //console.info('ClaimsService: getClaims for user : '+user);
    let url = apiUrl +"users/claims"+"?offset="+(size*(page-1) + "&limit="+size)+"&keyword="+keyword+"&sortby="+sortby+"&descending="+descending+(types.length>0?"&"+types:types);
     return this.getClaimRequest(size,page,url,false);

  }
  getClaimsBycontext( size : number, page : number, contextId:string, keyword:string, sortby: string, descending: boolean, types: string , apiUrl:string, mine:boolean):any {
    //console.info('ClaimsService: getClaims for context : '+contextId);
    let url = apiUrl +"contexts/"+contextId+"/claims"+"?offset="+(size*(page-1) + "&limit="+size)+"&keyword="+keyword+"&sortby="+sortby+"&descending="+descending+(types.length>0?"&"+types:types);
     return this.getClaimRequest(size,page,url,true);

  }
  getClaimsByResult( size : number, page : number, resultId:string, keyword:string, sortby: string, descending: boolean, types: string, apiUrl:string ):any {
    //console.info('ClaimsService: getClaims for entity : '+resultId);
    let url = apiUrl +"results/"+resultId+"/claims"+"?offset="+(size*(page-1) + "&limit="+size)+"&keyword="+keyword+"&sortby="+sortby+"&descending="+descending+(types.length>0?"&"+types:types);
     return this.getClaimRequest(size,page,url,true);

  }
  getClaimsByProject( size : number, page : number, projectId:string, keyword:string, sortby: string, descending: boolean, types: string, apiUrl:string ):any {
    //console.info('ClaimsService: getClaims for project : '+projectId);
    let url = apiUrl +"projects/"+projectId+"/claims"+"?offset="+(size*(page-1) + "&limit="+size)+"&keyword="+keyword+"&sortby="+sortby+"&descending="+descending+(types.length>0?"&"+types:types);
     return this.getClaimRequest(size,page,url,true);
  }
  getClaimsByOrganization( size : number, page : number, organizationId:string, keyword:string, sortby: string, descending: boolean, types: string, apiUrl:string, mine:boolean):any {
    let url = apiUrl +"organizations/"+organizationId+"/claims"+"?offset="+(size*(page-1) + "&limit="+size)+"&keyword="+keyword+"&sortby="+sortby+"&descending="+descending+(types.length>0?"&"+types:types) + (mine?"&mine=true":"");
    return this.getClaimRequest(size,page,url,true);
  }

  deleteClaimById(claimId:string , apiUrl:string):any{
    //console.warn('Trying to delete claim with id : '+claimId);
    let url = apiUrl +"claims/"+claimId;
      return this.http.delete( url,  CustomOptions.getAuthOptionsWithBody())//.map(request => <any> request.json())
      .pipe(catchError(this.handleError));

  }
  deleteBulk(claimIds:string[], apiUrl:string):any{

    //console.warn('Trying to delete claims with ids : '+claimIds);
     var url = "";

    for(var claimId of claimIds){
      url=url+(url.length >0 ?"&":"")+"claimId="+claimId;
    }
    url= apiUrl +"claims/bulk?"+url;

      return this.http.delete( url,  CustomOptions.getAuthOptionsWithBody())//.map(request => <any> request.json())
      .pipe(catchError(this.handleError));

  }
  insertBulkClaims(claims, apiUrl:string):any{
    // console.warn('Trying toinsert claims  : '+claims);
    let url = apiUrl +"claims/bulk";
    let body = JSON.stringify( claims );
    return this.http.post(url, body,  CustomOptions.getAuthOptionsWithBody())
                          .pipe(catchError(this.handleError));

    }

    insertDirectRecords(records, apiUrl:string):any{
      //console.warn('Trying to feedrecords  : '+records);
      let url = apiUrl +"feed/bulk";
      let body = JSON.stringify( records );
      return this.http.post(url, body, CustomOptions.getAuthOptionsWithBody())
                            .pipe(catchError(this.handleError));

      }
  getStatus(jobId, apiUrl:string):any{

    let url = apiUrl +"jobStatus/" + jobId;

    return this.http.get(url,CustomOptions.getAuthOptionsWithBody())
      .pipe(catchError(this.handleError));

  }
  private handleError (error: Response) {
    // in a real world app, we may send the error to some remote logging infrastructure
    // instead of just logging it to the console
    console.log(error);
    return observableThrowError(error  || 'Server error');
  }

}
