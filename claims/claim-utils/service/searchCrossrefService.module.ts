import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';

import {SearchCrossrefService} from './searchCrossref.service';


@NgModule({
  imports: [
    CommonModule, FormsModule
  ],
  declarations: [
  ],
  providers:[
  SearchCrossrefService
],
  exports: [
    ]
})
export class SearchCrossrefServiceModule { }
