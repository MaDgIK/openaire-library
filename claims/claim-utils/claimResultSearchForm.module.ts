import { NgModule } from '@angular/core';

import { SharedModule } from '../../../openaireLibrary/shared/shared.module';
import { CommonModule }        from '@angular/common';
import { MatSelectModule } from "@angular/material/select";
import {ClaimResultSearchFormComponent} from './claimResultSearchForm.component';
import {ClaimResultsModule} from './claimResults.module';

import {SearchDataciteServiceModule} from './service/searchDataciteService.module';

import {SearchCrossrefServiceModule} from './service/searchCrossrefService.module';
import {SearchOrcidService} from './service/searchOrcid.service';

import {SearchResearchResultsServiceModule} from '../../services/searchResearchResultsService.module';
import {PagingModule } from '../../utils/paging.module';
import {HelperModule} from '../../utils/helper/helper.module';
import {SearchFilterModule} from '../../searchPages/searchUtils/searchFilter.module';
import {QuickSelectionsModule} from "../../searchPages/searchUtils/quick-selections.module";
import {RangeFilterModule} from "../../utils/rangeFilter/rangeFilter.module";
import {ClaimProjectsSearchFormModule} from "./claimProjectSearchForm.module";
import {AdvancedSearchInputModule} from "../../sharedComponents/advanced-search-input/advanced-search-input.module";
import {InputModule} from "../../sharedComponents/input/input.module";
import {SearchInputModule} from "../../sharedComponents/search-input/search-input.module";
import {DropdownFilterModule} from "../../utils/dropdown-filter/dropdown-filter.module";
import {LoadingModule} from "../../utils/loading/loading.module";

 @NgModule({
   imports: [SharedModule, CommonModule, SearchResearchResultsServiceModule, PagingModule, SearchCrossrefServiceModule,
     SearchDataciteServiceModule, HelperModule, SearchFilterModule, ClaimResultsModule, MatSelectModule, QuickSelectionsModule, RangeFilterModule, ClaimProjectsSearchFormModule, AdvancedSearchInputModule, InputModule, SearchInputModule, DropdownFilterModule, LoadingModule],
  providers:[
     SearchOrcidService
  ],
  declarations: [
     ClaimResultSearchFormComponent

  ],
  exports: [ClaimResultSearchFormComponent ]
})
export class ClaimResultSearchFormModule { }
