import {Component, ElementRef, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {SearchProjectsService} from '../../services/searchProjects.service';
import {ProjectService} from '../../landingPages/project/project.service';
import {ClaimEntity, ClaimProject} from './claimHelper.class';
import {EnvProperties} from '../../utils/properties/env-properties';
import {ErrorCodes} from '../../utils/properties/errorCodes';
import {StringUtils} from "../../utils/string-utils.class";
import {Filter, Value} from "../../searchPages/searchUtils/searchHelperClasses.class";
import {RangeFilter} from "../../utils/rangeFilter/rangeFilterHelperClasses.class";
import {OpenaireEntities, SearchFields} from "../../utils/properties/searchFields";
import {NewSearchPageComponent} from "../../searchPages/searchUtils/newSearchPage.component";
import {Subscriber} from "rxjs";
import { properties } from 'src/environments/environment';

declare var UIkit:any;

@Component({
    selector: 'claim-projects-search-form',

    templateUrl:'claimProjectSearchForm.component.html',

})
export class ClaimProjectsSearchFormComponent {
  public query = '';
  @Input() public centerAlign: boolean = false;
  @Input() public selectedProjects:ClaimEntity[]=[] ;
  public elementRef;

  @Output() projectSelected = new EventEmitter();
  public properties:EnvProperties = properties;
  @Input() public inlineClaim:boolean=false;
  @Input()  localStoragePrefix:string = "";
  @Input() basketLimit;
  @Input() showOptions;
  public errorCodes:ErrorCodes = new ErrorCodes();
  public projects:string[];
  public warningMessage = "";
  openaireResults:ClaimEntity[] = [];
  openaireResultsNum: number = -1;
  openaireResultsPage : number = 1;
  openaireResultsStatus:number = this.errorCodes.NONE;
  page : number = 1;
  size:number = 5;
  keyword:string = '';
  showResults = false;

  public refineFields: string[] = ["funder"];
  public refineFieldsQuery: string = "&refine=true&fields=funder";
  filters = [];
  prevFilters = [];
  public rangeFilters: RangeFilter[] = [];
  public searchFields: SearchFields = new SearchFields();
  public rangeFields:string[][] = this.searchFields.PROJECT_RANGE_FIELDS;
  openaireEntities = OpenaireEntities;
  sub;

  selectedFunder = null;
  funderOptions = [];
  isNoProjectFunder = false;

  constructor(private _service: ProjectService, private _projectService: SearchProjectsService, myElement: ElementRef) {
      this.elementRef = myElement;
      this.rangeFilters = RangeFilter.parse(this.rangeFields,"project");
      this.getFunders();
  }
  ngOnDestroy() {
    if (this.sub  instanceof Subscriber) {
      this.sub.unsubscribe();
    }
  }
  getFunders(){
    this.openaireResultsStatus = this.errorCodes.LOADING;
    this.showResults = true;
    this.sub = this._projectService.advancedSearchProjects("", 1, 0, this.properties,
        this.refineFieldsQuery, this.refineFields, "&type=projects&sf=funder", false, false).subscribe(
      data => {
        let option = {value : null, label: "No funder selected"};
        this.funderOptions.push(option);
        for(let v of data[2][0].values){
          let option = {value : v, label: v.name};
          this.funderOptions.push(option);
        }
        this.openaireResultsStatus = this.errorCodes.DONE;
      }, error =>{
        this.openaireResultsStatus = this.errorCodes.ERROR;
      })
  }

  funderChanged(value){
    this.keyword = ""
    this.selectedFunder = value;
    this.isNoProjectFunder = this.selectedFunder && this.selectedFunder.number == 1;
    this.openaireResults = [];
    this.openaireResultsNum = -1;
    if(this.isNoProjectFunder){
      this.showResults = true;
      this.search(1,1);
    }else{
      this.openaireResults = [];
    }
  }
  search(page,size) {
   /* if (this.keyword.length == 0) {
      this.showResults = false;
      return;
    }*/
    this.showResults = true;
    this.openaireResults = [];
    this.openaireResultsStatus = this.errorCodes.LOADING;
    this.prevFilters = this.filters;

    //searchProjects (params: string, refineParams:string, page: number, size: number, refineFields:string[] , properties:EnvProperties ):any {
    this.sub = this._projectService.advancedSearchProjects(this.createOpenaireQueryParams(), page, size, this.properties,   this.createOpenaireRefineQuery(), [], null, false, false).subscribe(
      // this.sub = this._projectService.searchProjects(this.createOpenaireQueryParams(),(page==1)? this.refineFieldsQuery:null, page, size, (page==1)?this.refineFields:[], this.properties).subscribe(
      data => {
        if (data != null) {
          this.openaireResultsPage = page;
          this.openaireResultsNum = data[0];
          this.openaireResults = ClaimProjectsSearchFormComponent.openaire2ClaimEntity(data[1], this.properties);
          if (data[2] && data[2].length > 0) {
            this.filters = this.checkSelectedFilters(data[2], this.prevFilters);
          }

          this.openaireResultsStatus = this.errorCodes.DONE;
          if (this.openaireResultsNum == 0) {
            this.openaireResultsStatus = this.errorCodes.NONE;
            this.filters = this.checkSelectedFilters([], this.prevFilters);
          }
         /* if(this.isNoProjectFunder && this.claimResultsComponent){
            this.claimResultsComponent.add(this.openaireResults[0])
          }*/
        } else {
          this.openaireResultsStatus = this.errorCodes.ERROR;
        }
      },
      err => {
        this.openaireResultsStatus = this.errorCodes.ERROR;
        //console.log(err.status);
        ClaimProjectsSearchFormComponent.handleError("Error getting projects by keyword: "+this.keyword, err);
      }
    );
  }
   remove(item){
    let index:number =this.selectedProjects.indexOf(item);
     if (index > -1) {
        this.selectedProjects.splice(index, 1);
    }

  }
   static openaire2ClaimEntity(items, properties:EnvProperties){
    const projects: ClaimEntity[] = [];
    for(const item of items){
      const entity: ClaimEntity = new ClaimEntity();
      entity.project = new ClaimProject();
      entity.project.funderId = item.funderId;
      entity.project.funderShortname = item.funderShortname?item.funderShortname:(entity.project.funderId?entity.project.funderId.split("::")[1]:"");
      entity.project.funderName = item.funderName;
      entity.id = item.id;
      entity.project.url = (item.code !="unidentified") ? properties.searchLinkToProject + entity.id : null;
      entity.title = item.title.name;
      entity.project.acronym = item.acronym;
      entity.project.startDate = item.startYear;
      entity.project.endDate = item.endYear;
      entity.project.code = item.code;
      entity.project.jurisdiction = item.jurisdiction;
      entity.project.fundingLevel0 = item.fundingLevel0;
      entity.type="project";
      projects.push(entity);
    }
    return projects;
  }
  private openaireResultsPageChange($event) {
     this.openaireResultsPage=$event.value;
     this.openaireResults = [];
     this.search(this.openaireResultsPage,this.size);
  }
  private static handleError(message: string, error) {
      console.error("Claim project search form (component): "+message, error);
  }


  createOpenaireQueryParams(): string {
    let query = "";
    if (this.keyword.length > 0) {
      // query += "q=" + StringUtils.quote(StringUtils.URIEncode(this.keyword));
      query += StringUtils.quote(StringUtils.URIEncode(this.keyword));
    }
    return query;
  }

  createOpenaireRefineQuery(): string {
    let allFqs = "";
    for (let filter of this.filters) {
      if (filter.countSelectedValues > 0) {
        let count_selected = 0;
        let fq = "";
        for (let value of filter.values) {
          if (value.selected == true) {
            count_selected++;
            fq += (fq.length > 0 ? " " + filter.filterOperator + " " : "") + filter.filterId + " exact " + (StringUtils.quote(value.id));
          }
        }
        if (count_selected > 0) {
          fq = "&fq=" + StringUtils.URIEncode(fq);
          allFqs += fq;
        }
      }
    }
    if(this.selectedFunder){
      allFqs += "&fq=" + StringUtils.URIEncode( "funder exact " + (StringUtils.quote(this.selectedFunder.id)));
    }
    if(!this.isNoProjectFunder || !this.selectedFunder){
      allFqs += '&fq=(projectcode<>"unidentified")'
    }
    for (let i = 0; i < this.rangeFilters.length; i++) {
      let filter = this.rangeFilters[i];
      //selectedFromValue, selectedToValue, equalityOp, equalityOpFrom, equalityOpTo, filterOp ){
      allFqs += NewSearchPageComponent.createRangeFilterQuery(this.rangeFields[i], filter.selectedFromValue, filter.selectedToValue, " within ", ">=", "<=", "and")
    }
    return allFqs + "&type=projects";
  }

  public yearChanged() {
    this.search(this.page, this.size);

  }

  filterChanged($event) {
    this.search(this.page, this.size);

  }
  public checkSelectedFilters(filters:Filter[], prevFilters:Filter[]){
    for(let i=0; i< filters.length ; i++){
      let filter:Filter = filters[i];
      filter.countSelectedValues = 0;
      let prevFilterSelectedValues:string[] = [];
      for(let j=0; j< prevFilters.length ; j++){
        if(filters[i].filterId == prevFilters[j].filterId){
          if(prevFilters[j].countSelectedValues >0){
            for(let filterValue of prevFilters[j].values) {
              if(filterValue.selected){
                prevFilterSelectedValues.push(filterValue.id);
              }
            }

          }
          break;
        }
      }
      for(let filterValue of filter.values) {
        if(prevFilterSelectedValues.indexOf(filterValue.id) > -1) {
          filterValue.selected = true;
          filter.countSelectedValues++;
        }

      }
      filter.countAllValues = filter.values.length;
    }
    if(filters.length == 0 ){
      for(let j=0; j< prevFilters.length ; j++) {
        let filter = Object.assign({}, prevFilters[j]);
        filter.values = [];
        for (let filterValue of prevFilters[j].values) {
          if (filterValue.selected) {
            filterValue.number = 0;
            filter.values.push(filterValue);
          }
        }
        filter.countAllValues = filter.values.length;
        filters.push(filter)
      }
    }
    return filters;
  }
  totalPages(totalResults: number): number {
    let totalPages:any = totalResults/(this.size);
    if(!(Number.isInteger(totalPages))) {
      totalPages = (parseInt(totalPages, 10) + 1);
    }
    return totalPages;
  }

  getSelectedValues(filter): any {
    var selected = [];
    if (filter.countSelectedValues > 0) {
      for (var i = 0; i < filter.values.length; i++) {
        if (filter.values[i].selected) {
          selected.push(filter.values[i]);
        }
      }
    }
    return selected;

  }
  dateFilterChanged(filter:RangeFilter) {
    if (filter.selectedFromValue && filter.selectedToValue) {
      filter.selectedFromAndToValues = filter.selectedFromValue + "-" + filter.selectedToValue;
    } else if (filter.selectedFromValue) {
      filter.selectedFromAndToValues = "From " + filter.selectedFromValue;
    } else if (filter.selectedToValue) {
      filter.selectedFromAndToValues = "Until " + filter.selectedToValue;
    }
    this.filterChanged(null);
  }

  private removeFilter(value: Value, filter: Filter) {
    filter.countSelectedValues--;
    if (value.selected == true) {
      value.selected = false;
    }
    // this.search(false);
    this.filterChanged(null);
  }

  public countFilters(): number {
    let filters = 0;
    for (let filter of this.filters) {
      if (filter.countSelectedValues > 0) {
        filters += filter.countSelectedValues;
      }
    }
    for (let filter of this.rangeFilters) {
      if (filter.selectedFromValue || filter.selectedToValue) {
        filters += 1;
      }
    }
    return filters;
  }

  private clearFilters() {
    for (let i = 0; i < this.filters.length; i++) {
      for (let j = 0; j < this.filters[i].countSelectedValues; j++) {
        if (this.filters[i].values[j].selected) {
          this.filters[i].values[j].selected = false;
        }
        this.filters[i].countSelectedValues = 0;
      }
    }
    for(let filter of this.rangeFilters){
      this.removeRangeFilter(filter);
    }
    this.filterChanged(null);
  }
  public removeRangeFilter(filter: RangeFilter) {
    filter.selectedFromValue = null;
    filter.selectedToValue = null;
    filter.selectedFromAndToValues = null;
    this.filterChanged(null);
  }
}
