export class ClaimsProperties{
  ALLOW_ORGANIZATION_LINKING:boolean = false;
  SELECT_ENTITIES = {
    projects:"Funding"
  }
  INLINE_ENTITY = {
    show: true,
    guideText : null
  }
  BASKET ={
    source_title: "Source",
    target_title: "Link source to"
  }
  METADATA_PREVIEW ={
    source_title: "Source",
    target_title: "Link to",
    edit_source_title: "Edit",
    edit_target_title: "Edit",
    edit_target_icon: "edit"
  }


}
