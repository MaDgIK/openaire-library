import {Component, Input} from '@angular/core';
import {RouterHelper} from '../../../utils/routerHelper.class';

//Usage Example "<publication-title [title]="X" [url]="X" > </publication-title>"

@Component({
  selector: 'publication-title',
  template: `
    <ng-container *ngIf="entity.openaireId">
     <h6 class="uk-margin-remove multi-line-ellipsis lines-2">
       <p class="uk-margin-remove">
         <a *ngIf="!externalPortalUrl" [queryParams]="routerHelper.createQueryParam(param,entity.openaireId)"
            [routerLink]="path" class="uk-link uk-link-heading" [class.uk-disabled]="!linkAvailable">{{entity.title?entity.title:"[No title available]"}}</a>
         <a *ngIf="externalPortalUrl" [href]="externalPortalUrl + path+'?'+param+'='+entity.openaireId" class="uk-link uk-link-heading custom-external" [class.uk-disabled]="!linkAvailable">{{entity.title?entity.title:"[No title available]"}}</a>
       </p>
     </h6>
    </ng-container>
  `
})

export class PublicationTitleFormatter {
  @Input() param: string;
  @Input() path: string;
  @Input() entity: any;
  @Input() externalPortalUrl: string = null;
  @Input() linkAvailable: boolean = true;
  public routerHelper: RouterHelper = new RouterHelper();
}
