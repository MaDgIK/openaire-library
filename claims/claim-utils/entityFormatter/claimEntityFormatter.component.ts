import {Component, Input} from '@angular/core';
import {EnvProperties} from '../../../utils/properties/env-properties';
import {ClaimDBContext, ClaimDBOrganization, ClaimDBProject, ClaimDBResult} from "../claimHelper.class";
import {OpenaireEntities} from "../../../utils/properties/searchFields";
import {StringUtils} from "../../../utils/string-utils.class";

//Usage Example "<claim-entity [entity]="" [type]="" > </claim-entity>"

//externalUrl
@Component({
  selector: 'claim-entity',
  template: `
    <div *ngIf="type == 'publication' || type == 'dataset' || type == 'software' || type == 'other'" 
         [attr.uk-tooptip]="getEntityName(type)">
      <div *ngIf="source" class="uk-text-small">
        {{getEntityName(type)}}
      </div>
      <div>
        <span *ngIf="!source" class="uk-text-meta uk-text-small uk-margin-small-right uk-text-nowrap">Link to:</span>
        <publication-title [entity]="entity" param="id"
                           path="/search/result" [externalPortalUrl]=externalPortalUrl [linkAvailable]="linkAvailable"></publication-title>
      </div>
    </div>
    <div *ngIf="type == 'project'" [attr.uk-tooptip]="getEntityName(type)">
      <span class="uk-text-meta uk-text-small uk-margin-small-right uk-text-nowrap">Link to:</span>
      <project-title [project]="entity" [searchLink]=properties.searchLinkToProject
                     [externalPortalUrl]=externalPortalUrl></project-title>
    </div>
    <div *ngIf="type == 'organization'" [attr.uk-tooptip]="getEntityName(type)">
      <span class="uk-text-meta uk-text-small uk-margin-small-right uk-text-nowrap">Link to:</span>
      <organization-title [organization]="entity"></organization-title>
    </div>
    <div *ngIf="type == 'context'">
      <span class="uk-text-meta uk-text-small uk-margin-small-right uk-text-nowrap">Link to:</span>
      <h6 class="uk-h6 uk-margin-remove">
        <span class="uk-text-truncate" uk-tooltip="Concept">{{entity.title}}</span>
      </h6>
    </div>
  `
})

export class ClaimEntityFormatter {
  @Input() entity: ClaimDBResult | ClaimDBContext | ClaimDBProject | ClaimDBOrganization;
  @Input() type: string;
  @Input() properties: EnvProperties;
  @Input() externalPortalUrl: string = null;
  @Input() source: boolean = true;
  @Input() linkAvailable: boolean = true;
  public openAIREEntities = OpenaireEntities;

  constructor() {
  }
  
  public getEntityName(entityType:string) {
    return StringUtils.getEntityName(entityType);
  }
  
}

