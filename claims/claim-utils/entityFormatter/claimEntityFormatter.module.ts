import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { RouterModule } from '@angular/router';

import {ProjectTitleFormatter} from './projectTitleFormatter.component';
import {PublicationTitleFormatter} from './publicationTitleFormatter.component';
import {ClaimEntityFormatter} from './claimEntityFormatter.component';
import {OrganizationTitleFormatterComponent} from "./organizationTitleFormatter.component";

 @NgModule({
  imports: [
    CommonModule, RouterModule
   ],
  declarations: [
    ProjectTitleFormatter, PublicationTitleFormatter, ClaimEntityFormatter, OrganizationTitleFormatterComponent

  ],
  providers:    [   ],
  exports: [
    ProjectTitleFormatter, PublicationTitleFormatter, ClaimEntityFormatter, OrganizationTitleFormatterComponent

   ]
})
export class ClaimEntityFormatterModule { }
