import {Component, Input} from '@angular/core';
import {RouterHelper} from '../../../utils/routerHelper.class';
import {properties} from "../../../../../environments/environment";

//Usage Example "<project-title [project]="X" > </project-title>"

@Component({
  selector: 'project-title',
  template: `
      <h6 class="uk-margin-remove multi-line-ellipsis lines-2">
        <p class="uk-margin-remove">
          <a *ngIf="externalPortalUrl" [href]="externalPortalUrl + projectUrl + '?projectId='+project['openaireId']" class="uk-link uk-link-heading" [class.uk-disabled]="project.name == 'unidentified'">
            <span *ngIf="project['name'] != 'unidentified'">{{(project['acronym'] ? ('[' + project['acronym'] + '] ') : '')}}{{project['name']}}</span>
            <span *ngIf="project['name'] == 'unidentified'">{{project['funderName']}}</span>
          </a>
          <a *ngIf="!externalPortalUrl" [routerLink]="projectUrl"  [queryParams]="routerHelper.createQueryParam('projectId',project['openaireId'])" class="uk-link uk-link-heading" [class.uk-disabled]="project.name == 'unidentified'">
            <span *ngIf="project['name'] != 'unidentified'">{{(project['acronym'] ? ('[' + project['acronym'] + '] ') : '')}}{{project['name']}}</span>
            <span *ngIf="project['name'] == 'unidentified'">{{project['funderName']}} </span>
          </a>
        </p>
      </h6>
    <span *ngIf="project['funderName'] && project['name'] != 'unidentified'" class="uk-margin-small-top">
      <span class="uk-text-meta">Funder: </span>{{project['funderName']}}
    </span>
  `
})

export class ProjectTitleFormatter {
  @Input() project: any;
  @Input() searchLink: string;
  @Input() externalPortalUrl: string = null;
  public url: string;
  public routerHelper: RouterHelper = new RouterHelper();
  public projectUrl = properties.searchLinkToProject.split('?')[0];

  constructor() {
  }

  ngOnInit() {
    this.url = this.searchLink + "?projectId=" + this.project["openaireId"];
    console.log(this.project)
  }
}
