import {Component, Input} from '@angular/core';

@Component({
  selector: 'organization-title',
  template: `
       <h6 *ngIf="organization" class="uk-margin-remove multi-line-ellipsis lines-2">
        <p class="uk-margin-remove">
            {{(organization.shortName ? ('[' + organization.shortName + '] ') : '')}}{{organization.name}}
        </p>
      </h6>
  `
})

export class OrganizationTitleFormatterComponent {
  @Input() organization: any;
  public url: string;

}
