import { NgModule } from '@angular/core';

import { SharedModule } from '../../../openaireLibrary/shared/shared.module';
import { ClaimsAdminComponent } from './claimsAdmin.component';
import {DisplayClaimsModule} from '../claim-utils/displayClaims/displayClaims.module';

@NgModule({
  imports: [
    SharedModule,
    DisplayClaimsModule

  ],
    providers:[],
  declarations: [
    ClaimsAdminComponent
  ],
  exports:[ClaimsAdminComponent]
})
export class ClaimsAdminModule { }
