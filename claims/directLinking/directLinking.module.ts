import { NgModule } from '@angular/core';

import { SharedModule } from '../../../openaireLibrary/shared/shared.module';
import { DirectLinkingComponent } from './directLinking.component';

import {EntitySearchServiceModule} from '../../utils/entitiesAutoComplete/entitySearchService.module';
import {SearchResearchResultsServiceModule} from '../../services/searchResearchResultsService.module';
import {Schema2jsonldModule} from '../../sharedComponents/schema2jsonld/schema2jsonld.module';
import { SEOServiceModule } from '../../sharedComponents/SEO/SEOService.module';
import {LinkingGenericModule} from '../linking/linkingGeneric.module';
import {LoadingModule} from "../../utils/loading/loading.module";

@NgModule({
  imports: [
    SharedModule,
    EntitySearchServiceModule, SearchResearchResultsServiceModule,
    Schema2jsonldModule, SEOServiceModule, LinkingGenericModule, LoadingModule
  ],
  providers:[],
  declarations: [
    DirectLinkingComponent
  ], exports:[DirectLinkingComponent]
})
export class DirectLinkingModule { }
