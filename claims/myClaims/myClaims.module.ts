import { NgModule } from '@angular/core';

import { SharedModule } from '../../../openaireLibrary/shared/shared.module';
import { MyClaimsComponent } from './myClaims.component';
import {DisplayClaimsModule} from '../claim-utils/displayClaims/displayClaims.module';

@NgModule({
  imports: [
    SharedModule,
    DisplayClaimsModule

  ],
  providers:[],
  declarations: [
    MyClaimsComponent
  ], exports: [MyClaimsComponent]
})
export class MyClaimsModule { }
