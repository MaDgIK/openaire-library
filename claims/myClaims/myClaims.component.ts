import {Component, Input} from '@angular/core';
import {User} from "../../login/utils/helper.class";
import {UserManagementService} from "../../services/user-management.service";
import {LoginErrorCodes} from "../../login/utils/guardHelper.class";
import {Router} from "@angular/router";
import {Subscriber} from "rxjs";

@Component({
  selector: 'my-claims',
  template: `
    <div class="uk-section">
      <div class="uk-container uk-container-large">
        <h1 class="uk-h2 uk-margin-remove">
          My links
        </h1>
        <div class="uk-text-meta">
          Manage your links in OpenAIRE
        </div>
        <div class="uk-margin-top">
          <displayClaims *ngIf="user" [user]="user" [enableDelete]=true [myClaims]=true [isAdmin]=false [showUserEmail]=false
                          [claimsInfoURL]=claimsInfoURL [communityId]=communityId
                          pageTitle="My links">
          </displayClaims>
        </div>
      </div>
    </div>
  `

})
export class MyClaimsComponent {
  @Input() claimsInfoURL: string;
  @Input() communityId:string;
  public user: User = null;

  constructor(private userManagementService: UserManagementService, private _router: Router) {}

  sub;
  ngOnDestroy() {
    if (this.sub instanceof Subscriber) {
      this.sub.unsubscribe();
    }
  }
  ngOnInit() {
    this.sub = this.userManagementService.getUserInfo().subscribe(user => {
      this.user = user;
      if (!user) {
        this._router.navigate(['/user-info'], {
          queryParams: {
            "errorCode": LoginErrorCodes.NOT_VALID,
            "redirectUrl": this._router.url
          }
        });
      }
    });
  }

}
