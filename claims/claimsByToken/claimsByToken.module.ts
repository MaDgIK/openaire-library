/*
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import {DataTablesModule} from 'angular-datatables';

import { SharedModule } from '../../shared/shared.module';
import { ClaimsByTokenComponent } from './claimsByToken.component';
import { ClaimsByTokenService } from './claimsByToken.service';
import {ClaimEntityFormatterModule} from '../claim-utils/entityFormatter/claimEntityFormatter.module';
import {SelectModalModule} from '../../utils/modal/selectModal.module';
import {LoadingModalModule} from '../../utils/modal/loadingModal.module';

import {LoginGuard} from'../../login/loginGuard.guard';

import {PagingModule} from '../../utils/paging.module';
import {ClaimsDatatablePipe} from '../../utils/pipes/claimsDatatable.pipe';
import {PreviousRouteRecorder} from '../../utils/piwik/previousRouteRecorder.guard';
import {IsRouteEnabled} from '../../error/isRouteEnabled.guard';
import {ErrorMessagesModule}    from '../../utils/errorMessages.module';

@NgModule({
  imports: [
    RouterModule,
    DataTablesModule,
    SharedModule,
    ClaimEntityFormatterModule,
    SelectModalModule,
    LoadingModalModule,
    PagingModule,
    ErrorMessagesModule
  ],
  providers:[
    ClaimsByTokenService,
    LoginGuard, PreviousRouteRecorder, IsRouteEnabled
  ],
  declarations: [
    ClaimsByTokenComponent, ClaimsDatatablePipe
  ],
  exports: [
      ClaimsByTokenComponent, ClaimsDatatablePipe
    ]
})
export class ClaimsByTokenModule { }
*/
