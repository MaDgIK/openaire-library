import { NgModule } from '@angular/core';

import { SharedModule } from '../../../../openaireLibrary/shared/shared.module';
import {LoadingModalModule} from '../../../utils/modal/loadingModal.module';
import {BulkClaimComponent} from './bulkClaim.component';
import {SearchCrossrefServiceModule} from '../../claim-utils/service/searchCrossrefService.module';
import {SearchDataciteServiceModule} from '../../claim-utils/service/searchDataciteService.module';
import {HelperModule} from '../../../utils/helper/helper.module';

@NgModule({
  imports: [
    SharedModule, LoadingModalModule, SearchCrossrefServiceModule,SearchDataciteServiceModule, HelperModule
  ],
  declarations: [
BulkClaimComponent
  ], exports:[ BulkClaimComponent]
})
export class BulkClaimModule { }
