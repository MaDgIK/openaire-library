import {ChangeDetectorRef, Component, Input, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Meta, Title} from '@angular/platform-browser';
import {EnvProperties} from '../../utils/properties/env-properties';
import {ClaimEntity, ShowOptions} from '../claim-utils/claimHelper.class';
import {EntitiesSearchService} from '../../utils/entitiesAutoComplete/entitySearch.service';
import {SEOService} from '../../sharedComponents/SEO/SEO.service';
import {AlertModal} from "../../utils/modal/alert";
import {HelperFunctions} from "../../utils/HelperFunctions.class";
import {HelperService} from "../../utils/helper/helper.service";
import {PiwikService} from "../../utils/piwik/piwik.service";
import {Subscriber} from "rxjs";
import {properties} from "../../../../environments/environment";
import {Breadcrumb} from "../../utils/breadcrumbs/breadcrumbs.component";
import {OpenaireEntities} from "../../utils/properties/searchFields";
import {StringUtils} from "../../utils/string-utils.class";
import {RouterHelper} from "../../utils/routerHelper.class";
import { Location } from '@angular/common';
import {LoginErrorCodes} from "../../login/utils/guardHelper.class";
import {UserManagementService} from "../../services/user-management.service";
import {ClaimsProperties} from "../claim-utils/claims.properties";

@Component({
    selector: 'linking-generic',
    templateUrl: 'linkingGeneric.component.html'
})
export class LinkingGenericComponent {
  @Input() pageTitle: string = "Create links between research objects";
  piwiksub:any;

  @Input() communityId:string= null;
  sourceType:string;
  targetType:string;
  step:number = 1;
  @Input() results:ClaimEntity[] = [];
  @Input() inlineEntity:ClaimEntity = null;
  basketLimit =100;

  @Input() showOptions:ShowOptions = new ShowOptions();
  //show values: source, result, project, context, claim
  // linkTo /values: result, project, context
  // show linkToEntities /values: result, project, context

  @Input() sources:ClaimEntity[] =[];
  properties:EnvProperties = properties;
  public openaireEntities = OpenaireEntities;
  @Input() localStoragePrefix:string = "linking_";
  url=null;
  @ViewChild(AlertModal) alert;
  public pageContents = null;
  @Input() breadcrumbs: Breadcrumb[] = [];
  public routerHelper: RouterHelper = new RouterHelper();
  @Input() claimsProperties:ClaimsProperties = new ClaimsProperties();
  constructor (private _router: Router,  private route: ActivatedRoute, private entitySearch:EntitiesSearchService,
               private _meta: Meta, private _title: Title,  private _piwikService:PiwikService,
               private seoService: SEOService, private helper: HelperService, private cdr: ChangeDetectorRef,
               private location: Location, private userManagementService: UserManagementService) {
  }
  subscriptions = [];

  ngOnInit() {
   /* if(!this.claimsProperties){
      this.claimsProperties = new ClaimsProperties();
    }*/
    this.subscriptions.push(this.userManagementService.getUserInfo().subscribe(user => {
      if (!user) {
        this.saveStateAndRedirectLogin();
      }
    }));
      if(this.breadcrumbs.length === 0) {
      this.breadcrumbs.push({name: 'home', route: '/'});
      this.breadcrumbs.push({name: "Link", route: null});
    }
    this.showOptions.show = 'source';
    this.showOptions.initSelectOptions(this.claimsProperties);
    if(this.inlineEntity){
      this.showOptions.showLinkTo();
      // this.showOptions.basketShowLinksTo = true;
      this.showOptions.show = this.showOptions.linkTo;
    }

       this.url = this.properties.domain + this.properties.baseLink+this._router.url;

        var description = "Linking is a functionality provided by OpenAIRE, in order to link research results with a project, a research community or other research results.";
        this.updateTitle(this.pageTitle);
        this.updateDescription(description);
        this.updateUrl(this.url);

        this.seoService.createLinkForCanonicalURL(this.url, false);

        this.subscriptions.push(this._piwikService.trackView(this.properties, this.pageTitle).subscribe());

      if(properties.adminToolsPortalType !== "explore") {
        this.helper.getPageHelpContents(this.properties, this.communityId, this._router.url).subscribe(contents => {
          this.pageContents = contents;
        })
      }


      if( typeof localStorage !== 'undefined') {
        this.localStoragePrefix +=(this.communityId?this.communityId+"_":"");
        if(localStorage.getItem(this.localStoragePrefix + "results")){
          this.results  = JSON.parse(localStorage.getItem(this.localStoragePrefix + "results"));
        }

        if(localStorage.getItem(this.localStoragePrefix + "sources")){
          this.sources  = JSON.parse(localStorage.getItem(this.localStoragePrefix + "sources"));
        }
        if(localStorage.getItem(this.localStoragePrefix + "claimsJob")){
          let job  = JSON.parse(localStorage.getItem(this.localStoragePrefix + "claimsJob"));
          if(job.status != "COMPLETE"){
            this.showOptions.show = 'claim';          }
        }
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscriber) {
        subscription.unsubscribe();
      }
    });
  }

  openSelectionModal() {
    this.alert.cancelButton = false;
    this.alert.okButton = false;
    this.alert.alertTitle = "Select the type of Entity to Link to your sources";
    // this.alert.message = "<div>All the links you provided will be published in the OpenAIRE platform. " +
    //   "<br>Make sure you have checked all the information you provided. In some cases some links take more time to be published. " +
    //   "<br>For more information please check the linking status in My-Links page. " +
    //   "<br><br>Do you confirm the information you provide are valid?</div>";

    this.alert.open();
  }

  closeSelectionModal(show:string=null) {
    if(show){
      this.showOptions.show = show;
      this.showOptions.basketShowSources=false;
      this.showOptions.basketShowLinksTo=true;
    }
    this.alert.cancel();
    this.scrollUp();

  }
  scrollUp(){
    HelperFunctions.scroll();
  }


  private updateDescription(description:string) {
    this._meta.updateTag({content:description},"name='description'");
    this._meta.updateTag({content:description},"property='og:description'");
  }
  private updateTitle(title:string) {
    var _prefix ="";
    if(!this.communityId) {
      _prefix ="OpenAIRE | ";
    }
    var _title = _prefix + ((title.length> 50 ) ?title.substring(0,50):title);
    this._title.setTitle(_title);
    this._meta.updateTag({content:_title},"property='og:title'");
  }
  private updateUrl(url:string) {
    this._meta.updateTag({content:url},"property='og:url'");
  }

  stepHasChanged(stepId){
    
    if(stepId == 'source'){
      // console.log("show source")
      this.showOptions.showSource();
    }else if(stepId == 'target'){
      // console.log("show target")
      this.showOptions.show = this.showOptions.linkTo;
      this.showOptions.showLinkTo();
    }else if(stepId == 'claim'){
      // console.log("show target")
      this.showOptions.show = 'claim';
    }
    this.cdr.detectChanges();
    HelperFunctions.scroll(true);
    // console.log('stepHasChanged', stepId, this.showOptions.show)
  }

  stepStatus(stepId){
    if(stepId == 'source'){
      if(this.showOptions.show == 'source'){
        return 'active';
      }else if(this.sources.length > 0){
        return 'done';
      }else{
        return 'default';
      }
    }else if(stepId=='target'){
      if(this.showOptions.show != 'source' && this.showOptions.show != 'claim'){
        return 'active';
      }else if(this.results.length > 0){
        return 'done';
      }else if(this.sources.length == 0 && !this.inlineEntity){
        return 'disabled';
      }else{
        return 'default';
      }
    }else if(stepId=='claim'){
      if(this.showOptions.show == 'claim'){
        return 'active';
      }else if(this.results.length > 0 && (this.inlineEntity || this.sources.length > 0)){
        return  'default';
      }else if(!(this.results.length > 0 && (this.inlineEntity || this.sources.length > 0))){
        return 'disabled';
      }
    }
  }

  private getEntityName (entityType:string, plural:boolean) {
    return StringUtils.getEntityName(entityType, plural);
  }

  back(): void {
    if(typeof document !== 'undefined') {
      this.location.back();
    }
  }
  saveStateAndRedirectLogin() {

    if (this.results != null) {
      localStorage.setItem(this.localStoragePrefix + "results", JSON.stringify(this.results));
    }
    if (this.sources != null) {
      localStorage.setItem(this.localStoragePrefix + "sources", JSON.stringify(this.sources));
    }

    this._router.navigate(['/user-info'], {
      queryParams: {
        "errorCode": LoginErrorCodes.NOT_VALID,
        "redirectUrl": this._router.url
      }
    });
  }
}
