import { NgModule } from '@angular/core';

import { SharedModule } from '../../../../openaireLibrary/shared/shared.module';
import {AlertModalModule} from '../../../utils/modal/alertModal.module';
import {LoadingModalModule} from '../../../utils/modal/loadingModal.module';
import {ClaimInsertComponent} from './insertClaim.component';
import {ClaimServiceModule} from '../../claim-utils/service/claimsService.module';
import {IconsModule} from "../../../utils/icons/icons.module";
import {LogServiceModule} from "../../../utils/log/LogService.module";

@NgModule({
  imports: [
    SharedModule, AlertModalModule, LoadingModalModule, ClaimServiceModule, IconsModule, LogServiceModule
  ],
  declarations: [ClaimInsertComponent],
   exports:[ ClaimInsertComponent]
})
export class InsertClaimsModule { }
