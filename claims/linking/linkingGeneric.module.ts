import {NgModule} from '@angular/core';
import {MatSelectModule} from "@angular/material/select";

import {SharedModule} from '../../../openaireLibrary/shared/shared.module';
import {SelectedProjectsModule} from './selected/selectedProjects.module';
import {SelectedContextsModule} from './selected/selectedContexts.module';
import {SelectedPublicationsModule} from './selected/selectedResults.module';
import {LinkingGenericComponent} from './linkingGeneric.component';
import {StartOverModule} from '../claim-utils/startOver.module';
import {ClaimContextSearchFormModule} from '../claim-utils/claimContextSearchForm.module';
import {ClaimProjectsSearchFormModule} from '../claim-utils/claimProjectSearchForm.module';
import {BulkClaimModule} from './bulkClaim/bulkClaim.module';
import {ClaimResultSearchFormModule} from '../claim-utils/claimResultSearchForm.module';
import {HelperModule} from '../../utils/helper/helper.module';
import {Schema2jsonldModule} from '../../sharedComponents/schema2jsonld/schema2jsonld.module';
import {SEOServiceModule} from '../../sharedComponents/SEO/SEOService.module';
import {MetadataPreviewModule} from './selected/metadataPreview.module';
import {ClaimEntitiesMetadataModule} from "./selected/ClaimEntitiesMetadata.module";
import {AlertModalModule} from '../../utils/modal/alertModal.module';
import {BreadcrumbsModule} from "../../utils/breadcrumbs/breadcrumbs.module";
import {StepperModule} from "../../sharedComponents/stepper/stepper.module";
import {IconsModule} from "../../utils/icons/icons.module";
import {IconsService} from "../../utils/icons/icons.service";
import {link} from "../../utils/icons/icons";

@NgModule({
  imports: [
    SharedModule, SelectedProjectsModule, SelectedContextsModule,
    SelectedPublicationsModule,
    StartOverModule,
    ClaimContextSearchFormModule, ClaimProjectsSearchFormModule, BulkClaimModule, ClaimResultSearchFormModule,
    HelperModule, Schema2jsonldModule, SEOServiceModule, MetadataPreviewModule, ClaimEntitiesMetadataModule, AlertModalModule,
    MatSelectModule, BreadcrumbsModule, StepperModule, IconsModule
  ],
  providers: [],
  declarations: [
    LinkingGenericComponent
  ], exports: [
    LinkingGenericComponent]
})
export class LinkingGenericModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([link]);
  }
}
