
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../../../openaireLibrary/shared/shared.module';
import {ClaimSelectedProjectsComponent} from './selectedProjects.component';

 @NgModule({
  imports: [
    SharedModule, RouterModule,
  ],
  declarations: [
    ClaimSelectedProjectsComponent
  ], exports:[ClaimSelectedProjectsComponent]
})
export class SelectedProjectsModule { }
