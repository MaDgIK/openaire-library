import {Component, Input,Output, EventEmitter} from '@angular/core';
import {ClaimContext} from '../../claim-utils/claimHelper.class';

@Component({
    selector: 'claim-selected-contexts',
    template: `

        <!-- Contexts -->

            <div class="" >


              <div  *ngIf="contexts.length > 0 "  class="    ">
                  <!--h5 class=" uk-margin uk-h5 uk-text-primary">Selected Communities ({{contexts.length | number}}) </h5-->

                  <ul class="uk-list uk-list-divider">
                    <li class="list-group-item" *ngFor="let context of contexts" >
                      <span *ngIf="context.community != context.concept.label">{{context.community }} > {{context.category}} ></span><span> {{context.concept.label}} </span>
                      <span (click)="removeContext(context)" aria-hidden="true" class="uk-icon-button icon-button-small ">
                        <span class="uk-icon">
                          <svg width="16" height="16" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" icon="close" ratio="0.8"><path fill="none" stroke="#000" stroke-width="1.06" d="M16,16 L4,4"></path><path fill="none" stroke="#000" stroke-width="1.06" d="M16,4 L4,16"></path></svg>
                        </span>
                      </span>

                    </li>
                  </ul>
                </div>
                  <!--div  *ngIf="contexts.length == 0 "  class="uk-alert no-selected-message uk-text-center">You have not selected any communities</div-->
              </div>



        `
})
export class ClaimSelectedContextsComponent {
   ngOnInit() {
     var myDate = new Date();
     this.todayDate=( myDate.getFullYear()+ "-" +myDate.getMonth() + 1) + "-" + myDate.getDate() ;
     this.nextDate= ( (myDate.getFullYear()+100)+ "-" +myDate.getMonth() + 1) + "-" + myDate.getDate() ;
     //2015-05-01
    //  if(this.linkType == "context"){
       this.showsearch = true
    //  }else{
    //    this.showsearch = false;
    //  }
}


  @Input() contexts:ClaimContext[];
  //The following need to be kept in case we have to save the current state
  @Input() public projects;
  @Input() public results;
  @Input() componentClass:string = ""; //"" or "col-sm-6" for horizontal display (besides projects)
  @Input() show='home';
  @Input() title='Communities';
  @Input() linkType:string = "project";

  @Input() hideType;
  @Input() bulkMode:boolean = false;
  @Output() showChange = new EventEmitter();
  @Input()  localStoragePrefix:string = "";
  showsearch:boolean = false;

  todayDate = '';
  nextDate = '';

  removeContext(item:any){
    var index:number =this.contexts.indexOf(item);
     if (index > -1) {
        this.contexts.splice(index, 1);
        if(this.contexts != null){
          localStorage.setItem(this.localStoragePrefix + "contexts", JSON.stringify(this.contexts));
        }
      }

    }

}
