import {Component, Input, Output, EventEmitter} from '@angular/core';
import {ClaimProject} from '../../claim-utils/claimHelper.class';
import {RouterHelper} from '../../../utils/routerHelper.class';
import {properties} from "../../../../../environments/environment";

@Component({
  selector: 'claim-selected-projects',
  template: `
    <div class="">
      <div>
        <ul class="uk-list uk-list-divider">
          <li class="list-group-item" *ngFor="let project of projects">
            <a [queryParams]="routerHelper.createQueryParam('projectId',project.projectId)"
               routerLinkActive="router-link-active"
               [routerLink]="projectUrl">{{project.funderName}}
              | {{project.projectName}} {{(project.projectAcronym) ? '(' + project.projectAcronym + ')' : ''}}
              <!--[{{project.startDate}} - {{project.endDate}}]--></a>
            <span (click)="removeProject(project)" aria-hidden="true" class="uk-icon-button icon-button-small ">
              <span class="uk-icon">
                <svg width="16" height="16" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" icon="close"
                     ratio="0.8"><path fill="none" stroke="#000" stroke-width="1.06" d="M16,16 L4,4"></path><path
                    fill="none" stroke="#000" stroke-width="1.06" d="M16,4 L4,16"></path></svg>
              </span>
            </span>
          </li>
        </ul>
      </div>
      <!--div *ngIf="projects.length == 0 " class="uk-alert no-selected-message uk-text-center">You have not selected any projects</div-->
    </div>`
})
export class ClaimSelectedProjectsComponent {
  
  
  ngOnInit() {
    var myDate = new Date();
    this.todayDate = (myDate.getFullYear() + "-" + myDate.getMonth() + 1) + "-" + myDate.getDate();
    this.nextDate = ((myDate.getFullYear() + 100) + "-" + myDate.getMonth() + 1) + "-" + myDate.getDate();
    // if(this.linkType == "project"){
    this.showsearch = true
    // }else{
    //   this.showsearch = false;
    // }
    //2015-05-01
  }
  
  
  @Input() projects: ClaimProject[];
  @Input() show = 'home';
  @Input() title = 'Projects';
  @Input() linkType: string = "project";
  @Input() hideType;
  @Input() bulkMode: boolean = false;
  @Input() linkToResults: boolean = true;
  @Output() projectsChange = new EventEmitter();
  @Output() showChange = new EventEmitter();
  showsearch: boolean = false;
  @Input() localStoragePrefix: string = "";
  todayDate = '';
  nextDate = '';
  public routerHelper: RouterHelper = new RouterHelper();
  public projectUrl = properties.searchLinkToProject.split('?')[0];
  
  removeProject(item: any) {
    var index: number = this.projects.indexOf(item);
    if (index > -1) {
      this.projects.splice(index, 1);
      if (this.projects != null) {
        localStorage.setItem(this.localStoragePrefix + "projects", JSON.stringify(this.projects));
      }
    }
    this.projectsChange.emit({
      value: this.projects
    });
  }
}
