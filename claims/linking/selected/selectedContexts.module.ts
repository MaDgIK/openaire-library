import { NgModule } from '@angular/core';

import { SharedModule } from '../../../../openaireLibrary/shared/shared.module';
import {ClaimSelectedContextsComponent} from './selectedContexts.component';

 @NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    ClaimSelectedContextsComponent
  ], exports:[ClaimSelectedContextsComponent]
})
export class SelectedContextsModule { }
