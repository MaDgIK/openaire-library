import {Component, Input} from '@angular/core';
import {ClaimEntity} from '../../claim-utils/claimHelper.class';

@Component({
  selector: 'claim-project-metadata',
  template: `
    <ng-container *ngIf="entity.type == 'project' && entity.project">
      <div *ngIf="!shortVersion" class="claim-entity-metadata uk-flex-inline uk-flex-wrap uk-text-xsmall uk-text-emphasis"
        [class.uk-margin-xsmall-top]="!shortVersion">
        <span  class="uk-text-capitalize">
          {{entity.type == 'project' && entity.project.code == 'unidentified'?'funder':entity.type}}
        </span>
        <span *ngIf="entity.project.startDate || entity.project.endDate">
          {{(entity.project.startDate) ? entity.project.startDate : 'Unknown'}}{{'-' + ((entity.project.endDate) ? entity.project.endDate : 'Unknown')}}
        </span>
      </div>
      <div *ngIf="shortVersion && entity.type == 'project' && entity.project.code == 'unidentified'" class="claim-entity-metadata uk-flex-inline uk-flex-wrap uk-text-xsmall uk-text-emphasis"
           [class.uk-margin-xsmall-top]="!shortVersion">
        <span  class="uk-text-capitalize">
          funder
        </span>
         
      </div>
      <div *ngIf="entity.project.code!='unidentified'" class="uk-text-small uk-flex uk-flex-wrap" [style.grid-gap]="shortVersion?'0px':'10px'"
        [class.uk-margin-small-top]="!shortVersion">
        <div *ngIf="entity.project.code">
          <span class="uk-text-meta">Project Code: </span>{{entity.project.code}}
        </div>
        <div *ngIf="entity.project.funderName || entity.project.funderShortname">
          <span class="uk-text-meta">Funding: </span>{{entity.project.funderName?entity.project.funderName:entity.project.funderShortname}}
        </div>
        
      </div>
      </ng-container>
  `,
  styleUrls: ['ClaimEntityMetadata.component.less']


})
export class ClaimEntityProjectMetadataComponent {
  @Input() entity: ClaimEntity;
  @Input() slice: boolean = false;
  @Input() sliceSize: number = 45;
  @Input() shortVersion: boolean = false;

  ngOnInit() {
  }

  sliceArray(array): string {
    if (this.slice) {
      let sliced = array.slice(0, this.sliceSize);
      return sliced.join("; ") + (array.length>this.sliceSize ? "...":"");
    }
    return array.join("; ");
  }

  addStringToNumber(str: string, num: number) {
    return (+str) + num;
  }

  getProjectDurationMessage(result: ClaimEntity) {
    if(!result.warningMessages){
      return null;
    }
    for (let message of result.warningMessages) {
      if (message.type == "projectDuration") {
        return "Should be from " + message.projectInfo.startDate + ((message.projectInfo.endDate) ? (" to " + ((5 + +message.projectInfo.endDate))) : "");
      }
    }
    return null;
  }

  // getEmbargoEndDateMessage(result: ClaimEntity) {
  //   for (var message of result.warningMessages) {
  //     if (message.type == "embargoEndDate") {
  //       return "Embargo end date must be later than published date";
  //     }
  //   }
  //   return null;
  // }

}
