import {Component, Input} from '@angular/core';
import {ClaimEntity} from '../../claim-utils/claimHelper.class';

@Component({
  selector: 'claim-title',
  template: `
    <div class="uk-grid uk-flex uk-flex-middle">
      <span *ngIf="showIcon"  class="uk-flex uk-margin-xsmall-right">
        <icon *ngIf="entity.result" class=" uk-text-small uk-text-meta" name="insert_drive_file">
          
        </icon>
        <icon *ngIf="entity.project && entity.project.code !='unidentified'" class=" uk-text-small uk-text-meta" name="assignment_turned_in">
        </icon>
        <icon *ngIf="entity.project && entity.project.code =='unidentified'" class="uk-text-small uk-text-meta" name="coins" [ratio]="1.4">
        </icon>
        <icon *ngIf="entity.organization" class=" uk-text-small uk-text-meta" name="account_balance">
        </icon>
        <icon *ngIf="entity.type=='community'" class=" uk-text-small uk-text-meta" style="margin-right: 2px;" name="people">
        </icon>
      </span>
      <div class="uk-width-expand multi-line-ellipsis lines-3" style="word-break: break-word;"
        [class.uk-padding-remove-left]="showIcon" [class.uk-text-truncate]="shortVersion">
        <div class="uk-margin-remove uk-text-break uk-inline-block"
          [class.uk-h6]="!shortVersion" [class.uk-text-bold]="shortVersion">
          <a *ngIf="entity.result && entity.result.url" target="_blank" [href]="entity.result.url"
            class="uk-link uk-text-decoration-none uk-width-expand">
            {{entity.title ? sliceString(entity.title) : '[No title available]'}}
          </a>
          <span *ngIf="(entity.result && !entity.result.url)">
            {{entity.title ? sliceString(entity.title) : '[No title available]'}}
          </span>
          <span *ngIf="entity.type=='project' && entity.project && entity.project.code =='unidentified'">
            <span *ngIf="!shortVersion"> 
                {{(entity.project.funderShortname ? '[' + entity.project.funderShortname + '] ' : '') + entity.project.funderName}}
              </span>
              <span *ngIf="shortVersion">
                {{sliceString(entity.project.funderName)}}  
              </span>
          </span>
          <span *ngIf="entity.type=='project' && entity.project && entity.project.code!='unidentified'">
            <a *ngIf="entity.project && entity.project.url" target="_blank" [href]="entity.project.url"
              class="uk-link uk-text-decoration-none uk-width-expand">
              <span *ngIf="!shortVersion">
                {{(entity.project.acronym ? '[' + entity.project.acronym + '] ' : '') + entity.title}}
              </span>
              <span *ngIf="shortVersion">
                {{(entity.project.acronym ?  sliceString(entity.project.acronym):sliceString(entity.title))}}  
              </span>
            </a>
            <span *ngIf="(entity.project && !entity.project.url)">
              <span *ngIf="!shortVersion"> 
                {{(entity.project.acronym ? '[' + entity.project.acronym + '] ' : '') + entity.title}}
              </span>
              <span *ngIf="shortVersion">
                {{(entity.project.acronym ?  sliceString(entity.project.acronym):sliceString(entity.title))}}  
              </span>
            </span>
          </span>
          <span *ngIf="entity.type=='organization' && entity.organization">             
                {{sliceString(entity.title)}}
          </span>
          <span *ngIf="entity.type=='community' && entity.context">
            <span *ngIf=" entity.context.community != entity.context.concept.label">
              {{entity.context.community }} > {{entity.context.category}} >
            </span>
            <span>
              {{entity.context.concept.label}}
            </span>
          </span>
        </div>
      </div>
    </div>
  `


})
export class ClaimEntityTitleComponent {
  @Input() entity: ClaimEntity;
  @Input() slice:boolean = false;
  @Input() sliceSize:number = 45;
  @Input() shortVersion: boolean = false;
  @Input() showIcon: boolean = false;

  ngOnInit() {
  }

  sliceString(mystr:string): string {
    if(this.slice){
       // return StringUtils.sliceString(mystr,this.sliceSize);
    }
    return mystr;
  }

}
