import {NgModule} from '@angular/core';
import {SharedModule} from '../../../../openaireLibrary/shared/shared.module';
import {ClaimEntityTitleComponent} from "./ClaimEntityTitle.component";
import {ClaimEntityResultMetadataComponent} from "./ClaimEntityResultMetadata.component";
import {ClaimEntityProjectMetadataComponent} from "./ClaimEntityProjectMetadata.component";
import {IconsService} from "../../../utils/icons/icons.service";
import {coins} from "../../../utils/icons/icons";
import {IconsModule} from "../../../utils/icons/icons.module";

@NgModule({
  imports: [
    SharedModule,
    IconsModule
  ],
  declarations: [
     ClaimEntityTitleComponent, ClaimEntityResultMetadataComponent, ClaimEntityProjectMetadataComponent
  ], exports:[ ClaimEntityTitleComponent, ClaimEntityResultMetadataComponent, ClaimEntityProjectMetadataComponent]
})
export class ClaimEntitiesMetadataModule {
  constructor(private _iconsService:IconsService) {
    this._iconsService.registerIcons([coins])
  }
}
