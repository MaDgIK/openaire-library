import {Component, Input} from '@angular/core';
import {ClaimEntity} from '../../claim-utils/claimHelper.class';
declare var UIkit: any;
@Component({
  selector: 'claim-selected-results',
  templateUrl: 'selectedResults.component.html'

})
export class ClaimSelectedResultsComponent {
  @Input() results: ClaimEntity[];
  @Input() sectionTitle: string = "Research Results";
  @Input() localStoragePrefix: string = "";
  @Input() enableRemove:boolean = true;
  @Input() type: "source" | "target";
  @Input() shortVersion:boolean = true;
  ngOnInit() {

  }

  remove(item: any) {
    const index: number = this.results.indexOf(item);
    if (index > -1) {
      this.results.splice(index, 1);
      if (this.results != null) {
        localStorage.setItem(this.localStoragePrefix, JSON.stringify(this.results));
      }
      UIkit.notification(item.type + ' removed from your basket!', {
        status: 'warning',
        timeout: 4000,
        pos: 'bottom-right'
      });
    }

  }

}
