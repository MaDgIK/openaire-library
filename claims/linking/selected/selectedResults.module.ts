import { NgModule } from '@angular/core';

import { SharedModule } from '../../../../openaireLibrary/shared/shared.module';
import {ClaimSelectedResultsComponent} from './selectedResults.component';
import {AlertModalModule} from '../../../utils/modal/alertModal.module';
import { MyDatePickerModule } from '../../../utils/my-date-picker/my-date-picker.module';
import {ClaimEntitiesMetadataModule} from "./ClaimEntitiesMetadata.module";

@NgModule({
  imports: [
    SharedModule,
    AlertModalModule,
     MyDatePickerModule,
    ClaimEntitiesMetadataModule
  ],
  declarations: [
    ClaimSelectedResultsComponent
  ], exports:[ClaimSelectedResultsComponent]
})
export class SelectedPublicationsModule { }
