import {NgModule} from '@angular/core';
import {SharedModule} from '../../../../openaireLibrary/shared/shared.module';
import {MetadataPreviewComponent} from './metadataPreview.component';
import {AlertModalModule} from '../../../utils/modal/alertModal.module';
import {ClaimEntitiesMetadataModule} from "./ClaimEntitiesMetadata.module";
import {InsertClaimsModule} from "../insertClaim/insertClaim.module";
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import {HelperModule} from "../../../utils/helper/helper.module";
import {SelectedPublicationsModule} from "./selectedResults.module";
import {IconsModule} from "../../../utils/icons/icons.module";

@NgModule({
  imports: [
    SharedModule,
    AlertModalModule,
    ClaimEntitiesMetadataModule,
    InsertClaimsModule,
    MatDatepickerModule, MatNativeDateModule, MatFormFieldModule, MatInputModule, MatSelectModule,
    HelperModule, SelectedPublicationsModule, IconsModule
  ],
  declarations: [MetadataPreviewComponent],
  exports:[MetadataPreviewComponent]
})
export class MetadataPreviewModule { }
