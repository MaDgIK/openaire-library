import {Component, Input} from '@angular/core';
import {ClaimEntity} from '../../claim-utils/claimHelper.class';

@Component({
  selector: 'claim-result-metadata',
  template: `
    <div *ngIf="entity.result">
      <!-- display all existing data similar to entity-metadata -->
      <div *ngIf="!shortVersion" class="claim-entity-metadata uk-flex-inline uk-flex-wrap uk-text-xsmall uk-text-emphasis uk-margin-xsmall-top">
        <span class="uk-text-capitalize uk-text-italic">
          {{(!entity.result) ? entity.type : ((entity.result && entity.result.source == 'openaire') ? entity.type : (entity.result && entity.result.source + ' result'))}}
        </span>
        <span *ngIf="entity.result.publisher!=null">
          {{entity.result.publisher}}
				</span>
        <span *ngIf="entity.result.date">
					<span [class]="(getProjectDurationMessage(entity)?'uk-text-warning':'')">{{entity.result.date}}</span>
				</span>
        <span *ngIf="entity.result.journal!=null">
					{{entity.result.journal}}
				</span>
        <div *ngIf="entity.result.editors && entity.result.editors.length > 0">
          {{sliceArray(entity.result.editors)}}
        </div>
      </div>
      <div *ngIf="entity.result.authors && entity.result.authors.length > 0" class="uk-flex uk-flex-wrap uk-text-small uk-text-emphasis uk-text-italic"
        [class.uk-margin-small-top]="!shortVersion">
        {{sliceArray(entity.result.authors)}}
      </div>
      <div class="uk-text-small uk-margin-top">
        <span *ngIf="getProjectDurationMessage(entity)" [class]="(getProjectDurationMessage(entity)?'uk-text-warning':'')">
          {{getProjectDurationMessage(entity)}}
				</span>
      </div>
    </div>
  `,
  styleUrls: ['ClaimEntityMetadata.component.less']


})
export class ClaimEntityResultMetadataComponent {
  @Input() entity: ClaimEntity;
  @Input() slice: boolean = false;
  @Input() sliceSize: number = 45;
  @Input() shortVersion: boolean = false;

  ngOnInit() {
  }

  sliceArray(array): string {
    if (this.slice) {
      let sliced = array.slice(0, this.sliceSize);
      return sliced.join("; ") + (array.length>this.sliceSize ? "...":"");
    }
    return array.join("; ");
  }

  getProjectDurationMessage(result: ClaimEntity) {
    if(!result.warningMessages){
      return null;
    }
    for (let message of result.warningMessages) {
      if (message.type == "projectDuration") {
        return "Should be from " + message.projectInfo.startDate + ((message.projectInfo.endDate) ? (" to " + ((5 + +message.projectInfo.endDate))) : "");
      }
    }
    return null;
  }


}
