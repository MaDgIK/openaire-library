import {ResultLandingInfo} from "../utils/entities/resultLandingInfo";
import {ResultPreview} from "../utils/result-preview/result-preview";
import {StringUtils} from "../utils/string-utils.class";

export class WorkV3_0 {
  // automatically filled by orcid?
  "created-date" ?: CreatedDateV3_0;
  "last-modified-date" ?: LastModifiedDateV3_0;
  source ?: SourceV3_0;

  "put-code" ?: number;
  path ?: string;   // not sent
  title ?: WorkTitleV3_0;
  "journal-title" ?: TitleV3_0;
  "short-description" ?: string;
  citation ?: Citation; // not sent
  type ?: string;// = ['annotation', 'artistic-performance', 'book-chapter', 'book-review', 'book', 'conference-abstract', 'conference-paper', 'conference-poster', 'data-set', 'dictionary-entry', 'disclosure', 'dissertation-thesis', 'edited-book', 'encyclopedia-entry', 'invention', 'journal-article', 'journal-issue', 'lecture-speech', 'license', 'magazine-article', 'manual', 'newsletter-article', 'newspaper-article', 'online-resource', 'other', 'patent', 'physical-object', 'preprint', 'registered-copyright', 'report', 'research-technique', 'research-tool', 'software', 'spin-off-company', 'standards-and-policy', 'supervised-student-publication', 'technical-standard', 'test', 'trademark', 'translation', 'website', 'working-paper', 'undefined'],
  "publication-date" ?: PublicationDateV3_0;
  "external-ids" ?: ExternalIDsV3_0;
  url ?: UrlV3_0; // not sent - maybe url to landing with ?pid=...?
  contributors ?: WorkContributorsV3_0;
  "language-code" ?: string; // not sent // = ['ab', 'aa', 'af', 'ak', 'sq', 'am', 'ar', 'an', 'hy', 'as', 'av', 'ae', 'ay', 'az', 'bm', 'ba', 'eu', 'be', 'bn', 'bh', 'bi', 'bs', 'br', 'bg', 'my', 'ca', 'ch', 'ce', 'zh_CN', 'zh_TW', 'cu', 'cv', 'kw', 'co', 'cr', 'hr', 'cs', 'da', 'dv', 'nl', 'dz', 'en', 'eo', 'et', 'ee', 'fo', 'fj', 'fi', 'fr', 'fy', 'ff', 'gl', 'lg', 'ka', 'de', 'el', 'kl', 'gn', 'gu', 'ht', 'ha', 'iw', 'hz', 'hi', 'ho', 'hu', 'is', 'io', 'ig', 'in', 'ia', 'ie', 'iu', 'ik', 'ga', 'it', 'ja', 'jv', 'kn', 'kr', 'ks', 'kk', 'km', 'ki', 'rw', 'ky', 'kv', 'kg', 'ko', 'ku', 'kj', 'lo', 'la', 'lv', 'li', 'ln', 'lt', 'lu', 'lb', 'mk', 'mg', 'ms', 'ml', 'mt', 'gv', 'mi', 'mr', 'mh', 'mo', 'mn', 'na', 'nv', 'ng', 'ne', 'nd', 'se', 'no', 'nb', 'nn', 'ny', 'oc', 'oj', 'or', 'om', 'os', 'pi', 'pa', 'fa', 'pl', 'pt', 'ps', 'qu', 'rm', 'ro', 'rn', 'ru', 'sm', 'sg', 'sa', 'sc', 'gd', 'sr', 'sn', 'ii', 'sd', 'si', 'sk', 'sl', 'so', 'nr', 'st', 'es', 'su', 'sw', 'ss', 'sv', 'tl', 'ty', 'tg', 'ta', 'tt', 'te', 'th', 'bo', 'ti', 'to', 'ts', 'tn', 'tr', 'tk', 'tw', 'ug', 'uk', 'ur', 'uz', 've', 'vi', 'vo', 'wa', 'cy', 'wo', 'xh', 'ji', 'yo', 'za', 'zu'],
  country ?: CountryV3_0; // not sent
  visibility ?: string; // always public? // = ['limited', 'registered-only', 'public']

  private static mapType(type: string): string {
    if(type == "Annotation") {
      return "annotation";
    } else if(type == "Article" || type == "publication") {
      return "journal-article";
    } else if(type == "Book") {
      return "book";
    } else if(type == "Clinical Trial") {
      return "test";
    } else if(type == "Dataset" || type == "dataset") {
      return "data-set";
    } else if(type == "Doctoral thesis") {
      return "dissertation-thesis";
    // } else if(type == "Film") {
    //   return "artistic-performance";
    } else if(type == "Lecture") {
      return "lecture-speech";
    } else if(type == "Newsletter") {
      return "newsletter-article";
    } else if(type == "Part of book or chapter of book") {
      return "book-chapter";
    } else if(type == "Patent") {
      return "patent";
    } else if(type == "PhysicalObject") {
      return "physical-object";
    } else if(type == "Preprint") {
      return "preprint";
    } else if(type == "Report") {
      return "report";
    } else if(type == "Research") {
      return "working-paper";
    } else if(type == "Review") {
      return "book-review";
    } else if(type == "Software" || type == "software") {
      return "software";
    }
    return "other";
  }

  public static resultLandingInfoConvert(resultLandingInfo: ResultLandingInfo, putCode: string) : WorkV3_0 {
    let description_limit: number = 5000;

    let work: WorkV3_0 = {};
    // let work = {};

    // created-date (CreatedDateV3_0, optional),
    // last-modified-date (LastModifiedDateV3_0, optional),
    //
    // work["source"] = {
    //   "source-orcid": {
    //     "uri": "https://sandbox.orcid.org/0000-0001-9541-4617",
    //     "path": "0000-0001-9541-4617",
    //     "host": "sandbox.orcid.org"
    //   }
    // };
    // work["source-client-id"] = null;
    // work["source-name"] = {
    //     "value": "OpenAIRE OpenAIRE"
    // };
    // work["assertion-origin-orcid"] = null;
    // work["assertion-origin-client-id"] = null;
    // work["assertion-origin-name"] = null;
    //
    if(putCode) {
      work['put-code'] = +putCode;
    }
    //   path (string, optional),

    if(resultLandingInfo.title || resultLandingInfo.subtitle) {
      let title = {};
      if(resultLandingInfo.title ) {
        title['title'] = {"value": resultLandingInfo.title};
      }
      if(resultLandingInfo.subtitle) {
        title['subtitle'] = { "value": resultLandingInfo.subtitle };
      }
      // translated-title
      work['title'] = title;
    }

    if(resultLandingInfo.journal && resultLandingInfo.journal.journal) {
      work['journal-title'] = { "value": resultLandingInfo.journal.journal };
    }

    if(resultLandingInfo.description && resultLandingInfo.description.length < description_limit) {
      resultLandingInfo.description = StringUtils.HTMLToString(resultLandingInfo.description);
      work['short-description'] = resultLandingInfo.description.substring(0, description_limit-1);
    }

    // citation (Citation, optional),

    if(resultLandingInfo.types) {
      for(let type of resultLandingInfo.types) {
        work['type'] = this.mapType(type);
        if(work['type'] != "other") {
          break;
        }
      }
      // work['type'] = this.mapType(resultLandingInfo.types[0]);
    }
    if(!work['type']) {
      work['type'] = this.mapType(resultLandingInfo.resultType);
    }

    if(resultLandingInfo.date || resultLandingInfo.dateofacceptance) {
      if(resultLandingInfo.dateofacceptance) {
        work['publication-date'] = {
          "year": { "value": ('0' + resultLandingInfo.dateofacceptance.getFullYear()).slice(-4) },
          "month": { "value": ('0' + (resultLandingInfo.dateofacceptance.getMonth() + 1)).slice(-2) },
          "day": { "value": ('0' + resultLandingInfo.dateofacceptance.getDate()).slice(-2) }
        }
      } else {
        work['publication-date'] = {
          "year": { "value": resultLandingInfo.date+"" }
        }
      }
    }

    if(resultLandingInfo.identifiers && resultLandingInfo.identifiers.size > 0) {
      work['external-ids'] = { 'external-id': [] };
      resultLandingInfo.identifiers.forEach(((values, key) => {
        values.forEach(value => {
          work['external-ids']['external-id'].push(
            {
              "external-id-type": key,
              "external-id-value": value,
              "external-id-relationship": "self"
            }
          )
        })
      }))
    } else {
      work['external-ids'] = { 'external-id': [{
          "external-id-type": "source-work-id",
          "external-id-value": resultLandingInfo.relcanId,
          "external-id-relationship": "self",
          "external-id-url": {
            "value": "https://explore.openaire.eu/search/"+resultLandingInfo.resultType+"?id="+resultLandingInfo.relcanId
          },
        }] };
    }

    // url (UrlV3_0, optional),

    if(resultLandingInfo.authors) {
      work['contributors'] = { 'contributor': [] };
      resultLandingInfo.authors.forEach((author, index) => {
        let contributor = {};
        if(author.orcid) {
          contributor["contributor-orcid"] =
            {
              // "uri": "string",
              "path": author.orcid.toUpperCase()
              // "host": "string"
            }
        }
        if(author.fullName) {
          contributor["credit-name"] =
            {
              "value": author.fullName
            }
        }
        // "contributor-email": {
        //   "value": "string"
        // },
        contributor["contributor-attributes"] = {
          "contributor-sequence": index == 0 ? "first" : "additional",
          "contributor-role": "author"
        };

        work['contributors']['contributor'].push(contributor);
      })
    }

    if(resultLandingInfo.languages) {
      // work["language-code"] = "";
    }

    if(resultLandingInfo.countries) {
      // work["country"] = {
      //   "value": "AF"
      // }
    }

    // visibility (string, optional) = ['limited', 'registered-only', 'public']

    return work;
  }
}

export interface CreatedDateV3_0 {
  value ?: string;//read only)
}

export interface LastModifiedDateV3_0 {
  value ?: string;//read only)
}

export interface SourceV3_0 {
  "source-orcid" ?: SourceOrcidV3_0;
  "source-client-id" ?: SourceClientIdV3_0;
  "source-name" ?: SourceNameV3_0;
  "assertion-origin-orcid" ?: SourceOrcidV3_0;
  "assertion-origin-client-id" ?: SourceClientIdV3_0;
  "assertion-origin-name" ?: SourceNameV3_0;
}

export interface WorkTitleV3_0 {
  title ?: TitleV3_0;
  subtitle ?: SubtitleV3_0;
  "translated-title" ?: TranslatedTitleV3_0;
}

export interface TitleV3_0 {
  value ?: string;
}

export interface Citation {
  "citation-type": string;//= ['formatted-unspecified', 'bibtex', 'formatted-apa', 'formatted-harvard', 'formatted-ieee', 'formatted-mla', 'formatted-vancouver', 'formatted-chicago', 'ris'],
  "citation-value": string;
}

export interface PublicationDateV3_0 {
  year: YearV3_0;
  month ?: MonthV3_0;
  day ?: DayV3_0;
}

export interface ExternalIDsV3_0 {
  "external-id" ?: ExternalIDV3_0[];
}

export interface UrlV3_0 {
  value ?: string;
}

export interface WorkContributorsV3_0 {
  contributor ?: ContributorV3_0[];
}

export interface CountryV3_0 {
  value ?: string;// = ['AF', 'AX', 'AL', 'DZ', 'AS', 'AD', 'AO', 'AI', 'AQ', 'AG', 'AR', 'AM', 'AW', 'AU', 'AT', 'AZ', 'BS', 'BH', 'BD', 'BB', 'BY', 'BE', 'BZ', 'BJ', 'BM', 'BT', 'BO', 'BQ', 'BA', 'BW', 'BV', 'BR', 'IO', 'BN', 'BG', 'BF', 'BI', 'KH', 'CM', 'CA', 'CV', 'KY', 'CF', 'TD', 'CL', 'CN', 'CX', 'CC', 'CO', 'KM', 'CG', 'CD', 'CK', 'CR', 'CI', 'HR', 'CU', 'CW', 'CY', 'CZ', 'DK', 'DJ', 'DM', 'DO', 'EC', 'EG', 'SV', 'GQ', 'ER', 'EE', 'ET', 'FK', 'FO', 'FJ', 'FI', 'FR', 'GF', 'PF', 'TF', 'GA', 'GM', 'GE', 'DE', 'GH', 'GI', 'GR', 'GL', 'GD', 'GP', 'GU', 'GT', 'GG', 'GN', 'GW', 'GY', 'HT', 'HM', 'VA', 'HN', 'HK', 'HU', 'IS', 'IN', 'ID', 'IR', 'IQ', 'IE', 'IM', 'IL', 'IT', 'JM', 'JP', 'JE', 'JO', 'KZ', 'KE', 'KI', 'KP', 'KR', 'KW', 'KG', 'LA', 'LV', 'LB', 'LS', 'LR', 'LY', 'LI', 'LT', 'LU', 'MO', 'MK', 'MG', 'MW', 'MY', 'MV', 'ML', 'MT', 'MH', 'MQ', 'MR', 'MU', 'YT', 'MX', 'FM', 'MD', 'MC', 'MN', 'ME', 'MS', 'MA', 'MZ', 'MM', 'NA', 'NR', 'NP', 'NL', 'NC', 'NZ', 'NI', 'NE', 'NG', 'NU', 'NF', 'MP', 'NO', 'OM', 'PK', 'PW', 'PS', 'PA', 'PG', 'PY', 'PE', 'PH', 'PN', 'PL', 'PT', 'PR', 'QA', 'RE', 'RO', 'RU', 'RW', 'BL', 'SH', 'KN', 'LC', 'MF', 'PM', 'VC', 'WS', 'SM', 'ST', 'SA', 'SN', 'RS', 'SC', 'SL', 'SG', 'SX', 'SK', 'SI', 'SB', 'SO', 'ZA', 'GS', 'SS', 'ES', 'LK', 'SD', 'SR', 'SJ', 'SZ', 'SE', 'CH', 'SY', 'TJ', 'TZ', 'TH', 'TL', 'TG', 'TK', 'TO', 'TT', 'TN', 'TR', 'TM', 'TC', 'TV', 'UG', 'UA', 'AE', 'GB', 'US', 'UM', 'UY', 'UZ', 'VU', 'VE', 'VN', 'VG', 'VI', 'WF', 'EH', 'YE', 'ZM', 'ZW', 'TW', 'XK']
}

export interface SourceOrcidV3_0 {
  uri ?: string;
  path ?: string;
  host ?: string;
}

export interface SourceClientIdV3_0 {
  uri ?: string;
  path ?: string;
  host ?: string;
}

export interface SourceNameV3_0 {
  value ?: string;
}

export interface SubtitleV3_0 {
  value ?: string;
}

export interface TranslatedTitleV3_0 {
  value ?: string;
  "language-code": string;// = ['ab', 'aa', 'af', 'ak', 'sq', 'am', 'ar', 'an', 'hy', 'as', 'av', 'ae', 'ay', 'az', 'bm', 'ba', 'eu', 'be', 'bn', 'bh', 'bi', 'bs', 'br', 'bg', 'my', 'ca', 'ch', 'ce', 'zh_CN', 'zh_TW', 'cu', 'cv', 'kw', 'co', 'cr', 'hr', 'cs', 'da', 'dv', 'nl', 'dz', 'en', 'eo', 'et', 'ee', 'fo', 'fj', 'fi', 'fr', 'fy', 'ff', 'gl', 'lg', 'ka', 'de', 'el', 'kl', 'gn', 'gu', 'ht', 'ha', 'iw', 'hz', 'hi', 'ho', 'hu', 'is', 'io', 'ig', 'in', 'ia', 'ie', 'iu', 'ik', 'ga', 'it', 'ja', 'jv', 'kn', 'kr', 'ks', 'kk', 'km', 'ki', 'rw', 'ky', 'kv', 'kg', 'ko', 'ku', 'kj', 'lo', 'la', 'lv', 'li', 'ln', 'lt', 'lu', 'lb', 'mk', 'mg', 'ms', 'ml', 'mt', 'gv', 'mi', 'mr', 'mh', 'mo', 'mn', 'na', 'nv', 'ng', 'ne', 'nd', 'se', 'no', 'nb', 'nn', 'ny', 'oc', 'oj', 'or', 'om', 'os', 'pi', 'pa', 'fa', 'pl', 'pt', 'ps', 'qu', 'rm', 'ro', 'rn', 'ru', 'sm', 'sg', 'sa', 'sc', 'gd', 'sr', 'sn', 'ii', 'sd', 'si', 'sk', 'sl', 'so', 'nr', 'st', 'es', 'su', 'sw', 'ss', 'sv', 'tl', 'ty', 'tg', 'ta', 'tt', 'te', 'th', 'bo', 'ti', 'to', 'ts', 'tn', 'tr', 'tk', 'tw', 'ug', 'uk', 'ur', 'uz', 've', 'vi', 'vo', 'wa', 'cy', 'wo', 'xh', 'ji', 'yo', 'za', 'zu']
}

export interface YearV3_0 {
  value ?: string;
}

export interface MonthV3_0 {
  value ?: string;
}

export interface DayV3_0 {
  value ?: string;
}

export interface ExternalIDV3_0 {
  "external-id-type": string;
  "external-id-value": string;
  "external-id-normalized"?: TransientNonEmptyString;
  "external-id-normalized-error"?: TransientError;
  "external-id-url"?: UrlV3_0;
  "external-id-relationship"?: string;// = ['part-of', 'self', 'version-of']}
}

export interface ContributorV3_0 {
  "contributor-orcid" ?: ContributorOrcidV3_0;
  "credit-name" ?: CreditNameV3_0;
  "contributor-email" ?: ContributorEmailV3_0;
  "contributor-attributes" ?: ContributorAttributesV3_0;
}

export interface TransientNonEmptyString {
  value ?: string;
  transient ?: boolean;
}

export interface TransientError {
  "error-code": string;
  "error-message": string;
  transient: boolean;
}

export interface ContributorOrcidV3_0 {
  uri ?: string;
  path ?: string;
  host ?: string;
}

export interface CreditNameV3_0 {
  value ?: string;
}

export interface ContributorEmailV3_0 {
  value ?: string;
}

export interface ContributorAttributesV3_0 {
  "contributor-sequence": string;// = ['first', 'additional'],
  "contributor-role": string;// = ['author', 'assignee', 'editor', 'chair-or-translator', 'co-investigator', 'co-inventor', 'graduate-student', 'other-inventor', 'principal-investigator', 'postdoctoral-researcher', 'support-staff']
}
