import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {LoginGuard} from "../login/loginGuard.guard";
import {PreviousRouteRecorder} from "../utils/piwik/previousRouteRecorder.guard";
import {OrcidComponent} from './orcid.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '', component: OrcidComponent,
        canActivate: [LoginGuard],
        canDeactivate: [PreviousRouteRecorder] }
    ])
  ]
})
export class OrcidRoutingModule { }
