import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {ResultPreviewModule} from "../../utils/result-preview/result-preview.module";
import {ErrorMessagesModule} from "../../utils/errorMessages.module";
import {MyOrcidLinksComponent} from "./myOrcidLinks.component";
import {SearchResearchResultsServiceModule} from "../../services/searchResearchResultsService.module";
import {SearchMyOrcidResultsModule} from "./searchMyOrcidResults.module";
import {AlertModalModule} from "../../utils/modal/alertModal.module";
import {MyOrcidLinksRoutingModule} from "./myOrcidLinks-routing.module";

@NgModule({
  imports: [
    CommonModule, FormsModule,
    MyOrcidLinksRoutingModule,
    RouterModule, ErrorMessagesModule,
    ResultPreviewModule, SearchResearchResultsServiceModule, SearchMyOrcidResultsModule,
    AlertModalModule
  ],
  declarations: [
    MyOrcidLinksComponent
  ],
  providers:[
  ],
  exports: [
    MyOrcidLinksComponent
  ]
})
export class MyOrcidLinksModule { }
