import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {LoginGuard} from "../../login/loginGuard.guard";
import {PreviousRouteRecorder} from "../../utils/piwik/previousRouteRecorder.guard";
import {MyOrcidLinksComponent} from "./myOrcidLinks.component";

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '', component: MyOrcidLinksComponent,
        canActivate: [LoginGuard], canDeactivate: [PreviousRouteRecorder]
      }

    ])
  ]
})
export class MyOrcidLinksRoutingModule { }
