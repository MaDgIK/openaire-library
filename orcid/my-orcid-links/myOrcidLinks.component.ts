import {Component, Input, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {ErrorCodes} from "../../utils/properties/errorCodes";
import {ErrorMessagesComponent} from "../../utils/errorMessages.component";
import {SearchUtilsClass} from "../../searchPages/searchUtils/searchUtils.class";
import {RouterHelper} from "../../utils/routerHelper.class";
import {EnvProperties} from "../../utils/properties/env-properties";
import {properties} from "../../../../environments/environment";
import {OrcidService} from "../orcid.service";
import {Identifier, StringUtils} from "../../utils/string-utils.class";
import {SearchResearchResultsService} from "../../services/searchResearchResults.service";
import {SearchResult} from "../../utils/entities/searchResult";
import {ResultPreview} from "../../utils/result-preview/result-preview";
import {Meta, Title} from "@angular/platform-browser";
import {UserManagementService} from "../../services/user-management.service";
import {PiwikService} from "../../utils/piwik/piwik.service";
import {IndexInfoService} from "../../utils/indexInfo.service";
import {OpenaireEntities} from "../../utils/properties/searchFields";

declare var UIkit: any;

@Component({
  selector: 'my-orcid-links',
  template: `
    
    <div class="uk-container uk-container-large">
      <div class="uk-margin-medium-bottom">
        <div class="uk-margin-top">
          <div class="uk-grid uk-flex uk-flex-middle">
            <div class="uk-article-title custom-article-title uk-width-auto">
              My ORCID links
            </div>
            <span *ngIf="authorNameParam" class="uk-width-1-1 uk-width-expand@m">
              <a *ngIf="!properties.orcidDiscoverLinksPage" class="uk-button uk-button-text uk-align-left uk-align-right@m" [queryParams]="authorNameParam" 
                  [routerLink]="properties.searchLinkToAdvancedResults" routerLinkActive="router-link-active">
                Discover {{openaireEntities.RESULTS | lowercase}} related to you
              </a>
              <a *ngIf="properties.orcidDiscoverLinksPage" class="uk-button uk-button-text uk-align-left uk-align-right@m" 
                 [routerLink]="properties.orcidDiscoverLinksPage" routerLinkActive="router-link-active">
                Discover {{openaireEntities.RESULTS | lowercase}} related to you
              </a>
            </span>
          </div>
          
          <div class="uk-margin-top">
            <span>
              Did you link a result with your ORCID <img src="assets/common-assets/common/ORCIDiD_icon16x16.png" alt="" loading="lazy">
              but the <span class="text-orcid">green icon</span> is missing?
              <br>
              No worries! It will appear, as soon as we synchronize again with ORCID data.
              <span *ngIf="lastOrcidUpdateDate">Latest synchronization was on {{lastOrcidUpdateDate | date}}. </span>
              <a href="https://www.openaire.eu/openaire-explore-integration-with-the-orcid-search-and-link-wizard"
                     target="_blank" class="uk-display-inline-block">
                Read more  <span class="custom-external custom-icon space"></span>
              </a>
            </span>
          </div>
          
          <div *ngIf="showErrorMessage" class="uk-animation-fade uk-alert uk-alert-warning uk-margin-large-top" role="alert">
            An Error Occurred
          </div>
          <div *ngIf="!showLoading && !orcidQuery && !requestGrant && !showErrorMessage" 
                class="uk-animation-fade uk-text-meta uk-text-large uk-text-center uk-margin-large-top">
            No ORCID links found
          </div>
          <div *ngIf="requestGrant" class="uk-margin-large-top">
            <div class="uk-text-center uk-padding-small">
              <div>
                This is a private page, only for users that have connected their ORCID iD with OpenAIRE.
                Please grant OpenAIRE to access and update your ORCID works.
              </div>
  
              <div class="uk-margin-medium-top uk-align-center">
                <button (click)="openGrantWindow()" type="submit"
                        class="uk-button uk-padding-small uk-padding-remove-vertical uk-margin-left uk-button-primary">
                  <span>Grant OpenAIRE</span>
                </button>
              </div>
            </div>
          </div>
        </div>
  
        <div *ngIf="orcidQuery" class="tm-middle" id="tm-main">
          <div uk-grid="" class="uk-grid uk-grid-stack">
            <div class="tm-main uk-width-1-1@s uk-width-1-1@m uk-width-1-1@l uk-row-first uk-first-column">
              <div class="uk-container uk-container-large">
                <my-orcid-result [previewResults]="currentResults" 
                                 [status]="searchUtils.status" 
                                 [type]="resultType" 
                                 [properties]=properties
                                 [currentPage]="currentPage" 
                                 [totalResults]="totalOrcidResults" 
                                 [resultsPerPage]="resultsPerPage"
                                 (pageChange)="pageChanged($event)">
                </my-orcid-result>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    `
})
export class MyOrcidLinksComponent {
  public resultType: string = "result";

  private errorCodes: ErrorCodes;
  private errorMessages: ErrorMessagesComponent;

  public results:Map<number,{"work":{},results:ResultPreview[]}[]> = new Map();
  public currentResults:{"work":{},results:ResultPreview[]}[] = [];
  public totalResults:number  = 0 ;
  public baseUrl:string;
  public searchUtils:SearchUtilsClass = new SearchUtilsClass();
  public subscriptions: any[] = [];
  public _location:Location;

  public disableForms: boolean = false;
  public loadPaging: boolean = true;
  public oldTotalResults: number = 0;
  pagingLimit = 0;

  properties:EnvProperties = properties;
  public openaireEntities = OpenaireEntities;
  public lastOrcidUpdateDate: string = "";

  @Input() public communityId: string = null;

  public routerHelper:RouterHelper = new RouterHelper();
  parameters = {};
  keyword  = "";

  identifiers: string[] = [];
  works: any[] = [];

  orcidQuery: string = "";
  typeQuery: string = "";

  public showLoading: boolean = true;

  currentPage: number = 1;
  totalOrcidResults: number;
  resultsPerPage: number = 5;

  public requestGrant: boolean = false;
  public window: any;
  private tokenUrl: string;
  private clientId: string = "APP-A5M3KTX6NCN67L91";

  public authorNameParam: any = null;

  public showErrorMessage: boolean = false;

  constructor (private route: ActivatedRoute, private router: Router,
               private _piwikService:PiwikService,
               private _orcidService: OrcidService,
               private indexInfoService: IndexInfoService,
               private _searchResearchResultsService: SearchResearchResultsService,
               private userManagementService: UserManagementService,
               private _meta: Meta, private _title: Title
  ) {
    this.errorCodes = new ErrorCodes();
    this.errorMessages = new ErrorMessagesComponent();
    this.searchUtils.status = this.errorCodes.LOADING;
    this.searchUtils.page =1;

    if(typeof document !== 'undefined') {
      this.tokenUrl = properties.orcidTokenURL
        + "client_id=" + properties.orcidClientId
        // + "&response_type=code&scope=/activities/update"
        // + "&response_type=code&scope=/authenticate /activities/update /person/update /read-limited"
        + "&response_type=code&scope=/activities/update /read-limited"
        + "&redirect_uri="+location.origin+"/orcid?source=openaire";

      this.indexInfoService.getLastOrcidUpdateDate(properties).subscribe(
        date => {
          this.lastOrcidUpdateDate = date;
        },
        error => { console.error("Error in fetching last orcid update date ",error); }
      )
    }
  }

  public ngOnInit() {
    this.baseUrl = this.properties.myOrcidLinksPage;
    this.pagingLimit = this.properties.pagingLimit;
    var description = "Openaire, ORCID linking, publication, research data, software, other research product";
    this.updateTitle("My ORCID Links");
    this.updateDescription(description);
    this.updateUrl( properties.domain + properties.baseLink + this.route.url);
    this.subscriptions.push(this._piwikService.trackView(this.properties, "My ORCID links").subscribe());
    this.typeQuery = "&type=results";
    this.getLocalWorks();
    this.getPersonalDetails();
  }

  public ngOnDestroy() {
    for(let sub of this.subscriptions){
      sub.unsubscribe();
    }
  }

  private getPersonalDetails() {
    //get author name
    this.subscriptions.push(this._orcidService.getPersonalDetails().subscribe(
      details => {
        let author: string = "";

        if(details && details['name']) {
          let name: string = details['name'];
          if(name['given-names'] && name['given-names']['value']) {
            author = name['given-names']['value'];
          }
          if(name['family-name'] && name['family-name']['value']) {
            author += (author ? " " : "") + name['family-name']['value'];
          }
          this.authorNameParam = this.routerHelper.createQueryParams(['f0', 'fv0'], ['resultauthor', author]);
        }
      },
      error => {
        console.error("Error getting personal details", error);
        this.subscriptions.push(this.userManagementService.getUserInfo().subscribe(user => {
          if (user) {
            this.authorNameParam = this.routerHelper.createQueryParams(['f0', 'fv0'], ['resultauthor', user.fullname]);
          }
        }));
      }
    ));
  }

  openGrantWindow() {
    this.window = window.open(this.tokenUrl, '_blank',
      'location=yes,height=700,width=540,left=500,top=100,scrollbars=yes,status=yes');

    let self = this;
    window.onmessage = function (ev) {
      if (ev.isTrusted && ev.origin == location.origin && ev.data == 'success') {
        self.requestGrant = false;
        UIkit.notification({
          message: 'Thank you for <strong>connecting your ORCID iD</strong> with OpenAIRE!',
          status: 'success',
          timeout: 6000,
          pos: 'bottom-right'
        });
        self.getLocalWorks();
      }
    };
  }

  getLocalWorks() {
    this.showErrorMessage = false;

    this.showLoading = true;
    this.subscriptions.push(this._orcidService.getLocalWorks().subscribe(
      (response: any[]) => {

        this.works = response;//['results'];
       /* this.works=[
          { "id" : "60391ad16c2043536ca44ea4", "pids" : [ "10.5281/zeno(do.4304132", "10.5281/zenodo.3974605" ], "putCode" : "89558576", "orcid" : "0000-0002-3880-0244", "creationDate" : "2021-02-26T15:59:13.098Z", "updateDate" : "2021-02-26T15:59:13.098Z", "dashboard" : "production_explore" },
          { "id" : "60391ceb7a767127ea9a4d06", "pids" : [ "10.5281/zenodo.4095044", "10.5281/zenodo.4095043" ], "putCode" : "89559422", "orcid" : "0000-0002-3880-0244", "creationDate" : "2021-02-26T16:08:11.930Z", "updateDate" : "2021-02-26T16:27:12.835Z", "dashboard" : "production_explore" },
          { "id" : "604244fb7a767127ea9a4d0b", "pids" : [ "10.5281/zenodo.3467103", "10.5281/zenodo.3467104", "10.1007/978-3-030-30760-8_5", "1822/62856" ], "putCode" : "90020579", "orcid" : "0000-0002-3880-0244", "creationDate" : "2021-03-05T14:49:31.972Z", "updateDate" : "2022-06-20T08:28:08.539Z", "dashboard" : "production_explore" },
          { "id" : "60744bb46c20432760765b9f", "pids" : [ "10.5281/zenodo.3701379", "10.5281/zenodo.3701380" ], "putCode" : "92074390", "orcid" : "0000-0002-3880-0244", "creationDate" : "2021-04-12T13:31:32.476Z", "updateDate" : "2021-04-12T13:31:32.476Z", "dashboard" : "production_explore" },
          { "id" : "6077f0047a767112e847f9f8", "pids" : [ "10.5281/zenodo.4559726" ], "putCode" : "92235922", "orcid" : "0000-0002-3880-0244", "creationDate" : "2021-04-15T07:49:24.078Z", "updateDate" : "2021-04-21T09:43:40.147Z", "dashboard" : "production_explore" },
          { "id" : "6079752b7a767112e847fa5b", "pids" : [ "10.5281/zenodo.4304047", "10.5281/zenodo.3980491" ], "putCode" : "92304809", "orcid" : "0000-0002-3880-0244", "creationDate" : "2021-04-16T11:29:47.905Z", "updateDate" : "2021-04-16T11:29:47.905Z", "dashboard" : "production_explore" },
          { "id" : "63186abb7a767129935f495b", "pids" : [ "10.5281/zenodo.1314672", "10.5281/zenodo.1412509", "10.5281/zenodo.1314671" ], "putCode" : "118582128", "orcid" : "0000-0002-3880-0244", "creationDate" : "2022-09-07T09:56:11.556Z", "updateDate" : "2022-09-07T09:56:11.556Z", "dashboard" : "production_explore" },
          { "id" : "63186ade6c204337f56d97ca", "pids" : [ "10.5281/zenodo.6634431", "10.5281/zenodo.6385204", "10.5281/zenodo.4559725" ], "putCode" : "118582151", "orcid" : "0000-0002-3880-0244", "creationDate" : "2022-09-07T09:56:46.231Z", "updateDate" : "2022-09-07T09:56:46.231Z", "dashboard" : "production_explore" },
          { "id" : "63186ae27a767129935f495c", "pids" : [ "10.5281/zenodo.5801283", "10.5281/zenodo.3516917", "10.5281/zenodo.6616871" ], "putCode" : "118582162", "orcid" : "0000-0002-3880-0244", "creationDate" : "2022-09-07T09:56:50.660Z", "updateDate" : "2022-09-07T09:56:50.660Z", "dashboard" : "production_explore" }
        ];*/

        this.totalOrcidResults = this.works.length;//response['total'];

        this.prepareOrcidQuery();
      },
      error => {
        this.handleError(error, "Could not get locally stored user's ORCID works");
        this.showLoading = false;
      }
    ));
  }

  public prepareOrcidQuery() {

    if(Array.from(this.results.keys()).length >= this.currentPage) {
      this.currentResults = this.results[this.currentPage-1];
      return;
    }

    this.showLoading = true;

    this.orcidQuery = "";

    let from: number = (this.currentPage-1)*this.resultsPerPage;
    if(from >= this.works.length) {
      this.searchUtils.status = this.errorCodes.OUT_OF_BOUND;
    }
    let to: number = this.currentPage*this.resultsPerPage;

    if(to > this.works.length) {
      to = this.works.length;
    }

    let works = this.works.slice(from, to);

    for(let work of works) {
      if(work['pids'] && work['pids'].length > 0) {
        for (let pid of work['pids']) {
          let identifier: Identifier = Identifier.getIdentifierFromString(pid, false);
          this.orcidQuery += (this.orcidQuery ? " or " : "") + ('(pid="' + StringUtils.URIEncode(identifier.id) + '")');
        }
      } else {
        this.orcidQuery += (this.orcidQuery ? " or " : "") + ('(objIdentifier="' + StringUtils.URIEncode(work['openaireId']) + '")');
      }
    }
    this.showLoading = false;
    this._getResults(works);
  }

  public _getResults(works: any) {
    this.searchUtils.status = this.errorCodes.LOADING;
    this.disableForms = true;
    this.currentResults = [];
    this.searchUtils.totalResults = 0;
    this.subscriptions.push(this._searchResearchResultsService.searchForMyOrcidLinks(this.resultType, this.orcidQuery, this.typeQuery, 1, 50)
      .subscribe(
        data => {
          let totalResults = data[0];
          let results = data[1];

          this.resultsReturned(results, totalResults, works);
        },
        err => {
          console.error("Error getting " + this.getEntityName(this.resultType, true));
          this.showLoading = false;
          this.searchUtils.status = this.errorMessages.getErrorCode(err.status);
          this.disableForms = false;
        }
      )
    );
  }

  public resultsReturned(results: any, totalResults, works: any[]) {
    this.searchUtils.totalResults = totalResults;

    let resultsFound: Map<string[], number> = new Map<string[], number>();

    for(let work of works) {
      let relatedResults = [];
      this.currentResults.push({"work": work, "results" : relatedResults})
      results.forEach(result => {
        let identifierValues: string[] = result.identifiers ? [].concat(...Array.from(result.identifiers.values())) : [];
        if((work['pids'] && work['pids'].some(pid => identifierValues.includes(pid))) || work['openaireId'] === result.relcanId) {

          let index: number = resultsFound.get(identifierValues);
          if(!index) {
            relatedResults.push(this.getResultPreview(result));
            index = relatedResults.length - 1;
            relatedResults[index].orcidPutCodes = [];
            relatedResults[index].orcidCreationDates = [];
            relatedResults[index].orcidUpdateDates = [];
          }

          if (work['putCode']) {
            relatedResults[index].orcidPutCodes.push(work['putCode']);
          }
          if (work['creationDate']) {
            relatedResults[index].orcidCreationDates.push(work['creationDate']);
          }
          if (work['updateDate']) {
            relatedResults[index].orcidUpdateDates.push(work['updateDate']);
          }
        }
      })
    }

    this.results[this.currentPage-1] = this.currentResults;

    this.searchUtils.status = this.errorCodes.DONE;
    if (this.searchUtils.totalResults == 0) {
      this.searchUtils.status = this.errorCodes.NONE;
    }

    this.disableForms = false;
  }

  public getResultPreview(result: SearchResult): ResultPreview {
    return ResultPreview.searchResultConvert(result, (result.entityType)?result.entityType:this.resultType);
  }

  public getEntityName(entityType: string, plural: boolean): string {
    return StringUtils.getEntityName(entityType, plural);
  }

  public pageChanged($event) {
    this.currentPage = $event.value;
    this.prepareOrcidQuery();
  }

  handleError(error, errorMsg: string) {
    if(error && (error.status == "401" || error.status == "403")) {
      this.requestGrant = true;
    } else {
      this.showErrorMessage = true;
    }
    this.showLoading = false;
  }

  private updateTitle(title: string) {
    this._title.setTitle(title);
    this._meta.updateTag({content: title}, "property='og:title'");
  }

  private updateDescription(description: string) {
    this._meta.updateTag({content: description}, "name='description'");
    this._meta.updateTag({content: description}, "property='og:description'");
  }

  private updateUrl(url: string) {
    this._meta.updateTag({content: url}, "property='og:url'");
  }
}
