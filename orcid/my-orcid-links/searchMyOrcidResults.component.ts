import {Component, EventEmitter, Input, Output, SimpleChanges} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Subscriber} from "rxjs";
import {SearchResult} from "../../utils/entities/searchResult";
import {EnvProperties} from "../../utils/properties/env-properties";
import {RouterHelper} from "../../utils/routerHelper.class";
import {ErrorCodes} from "../../utils/properties/errorCodes";
import {ResultPreview} from "../../utils/result-preview/result-preview";
import {properties} from "../../../../environments/environment";
import {Session} from "../../login/utils/helper.class";
import {OrcidService} from "../orcid.service";
import {OpenaireEntities} from "../../utils/properties/searchFields";
import {HelperFunctions} from "../../utils/HelperFunctions.class";

@Component({
  selector: 'my-orcid-result',
  templateUrl:'searchMyOrcidResults.component.html'
})

export class searcMyOrcidResultsComponent {
  @Input() results: SearchResult[];
  @Input() status: number;
  @Input() type: string;
  @Input() properties:EnvProperties = properties;
  public openaireEntities = OpenaireEntities;

  @Input() previewResults:{"work":{},results:ResultPreview[]}[];

  public urlParam: string;
  public linkToAdvancedSearchPage: string;
  public errorCodes:ErrorCodes = new ErrorCodes();
  public routerHelper:RouterHelper = new RouterHelper();
  public errorMessage: string = "No results found";
  @Output() pageChange  = new EventEmitter();
  @Input() currentPage: number = 0;
  @Input() totalResults: number;
  @Input() resultsPerPage: number = 5;

  sub;
  constructor (private  route: ActivatedRoute, private orcidService: OrcidService) {}
  ngOnDestroy() {
    if (this.sub instanceof Subscriber) {
      this.sub.unsubscribe();
    }
  }
  ngOnInit() {
    if(this.type == "publication") {
      this.linkToAdvancedSearchPage = this.properties.searchLinkToAdvancedPublications;
      this.urlParam = "articleId";
    } else if(this.type == "dataset") {
      this.linkToAdvancedSearchPage = this.properties.searchLinkToAdvancedDatasets;
      this.urlParam = "datasetId";
    } else if(this.type == "software") {
      this.linkToAdvancedSearchPage = this.properties.searchLinkToAdvancedSoftware;
      this.urlParam = "softwareId";
    } else if(this.type == "other") {
      this.linkToAdvancedSearchPage = this.properties.searchLinkToAdvancedOrps;
      this.urlParam = "orpId";
    }
  }


  public  quote(params: string):string {
    return '"'+params+'"';
  }
  public getResultPreview(result: SearchResult): ResultPreview {
    return ResultPreview.searchResultConvert(result, (result.entityType)?result.entityType:this.type);
  }

  public pageChanged($event) {
    this.pageChange.emit($event);
    HelperFunctions.scroll();
  }

  public totalPages(totalResults: number): number {
    let totalPages:any = totalResults/this.resultsPerPage;
    if(!(Number.isInteger(totalPages))) {
      totalPages = (parseInt(totalPages, this.resultsPerPage) + 1);
    }
    return totalPages;
  }
}
