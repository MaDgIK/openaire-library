import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {ResultPreviewModule} from "../../utils/result-preview/result-preview.module";
import {ErrorMessagesModule} from "../../utils/errorMessages.module";
import {searcMyOrcidResultsComponent} from "./searchMyOrcidResults.component";
import {NoLoadPaging} from "../../searchPages/searchUtils/no-load-paging.module";
import {PagingModule} from "../../utils/paging.module";
import {OrcidCoreModule} from "../orcid-core.module";

@NgModule({
  imports: [
    CommonModule, FormsModule,
    RouterModule, ErrorMessagesModule,
    ResultPreviewModule, OrcidCoreModule, NoLoadPaging, PagingModule
  ],
  declarations: [
    searcMyOrcidResultsComponent
  ],
  providers:[
  ],
  exports: [
    searcMyOrcidResultsComponent
  ]
})
export class SearchMyOrcidResultsModule { }
