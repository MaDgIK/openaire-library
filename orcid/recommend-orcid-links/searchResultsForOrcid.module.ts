import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {SearchResultsForOrcidComponent} from './searchResultsForOrcid.component';
import {ResultPreviewModule} from "../../utils/result-preview/result-preview.module";
import {ErrorMessagesModule} from "../../utils/errorMessages.module";

@NgModule({
  imports: [
    CommonModule, FormsModule,
    RouterModule, ErrorMessagesModule,
    ResultPreviewModule
  ],
  declarations: [
    SearchResultsForOrcidComponent
  ],
  providers:[
  ],
  exports: [
    SearchResultsForOrcidComponent
  ]
})
export class SearchResultsForOrcidModule { }
