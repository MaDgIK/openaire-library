import {Component, Input} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Subscriber} from "rxjs";
import {SearchResult} from "../../utils/entities/searchResult";
import {EnvProperties} from "../../utils/properties/env-properties";
import {RouterHelper} from "../../utils/routerHelper.class";
import {ErrorCodes} from "../../utils/properties/errorCodes";
import {ResultPreview} from "../../utils/result-preview/result-preview";
import {properties} from "../../../../environments/environment";

@Component({
  selector: 'orcid-result',
  templateUrl:'searchResultsForOrcid.component.html'
})

export class SearchResultsForOrcidComponent {
  @Input() results: SearchResult[];
  @Input() status: number;
  @Input() type: string;
  @Input() properties:EnvProperties = properties;


  public urlParam: string;
  public linkToAdvancedSearchPage: string;
  public errorCodes:ErrorCodes = new ErrorCodes();
  public routerHelper:RouterHelper = new RouterHelper();
  public errorMessage: string = "No results found";

  sub;
  constructor (private  route: ActivatedRoute) {}
  ngOnDestroy() {
    if (this.sub instanceof Subscriber) {
      this.sub.unsubscribe();
    }
  }
  ngOnInit() {
    if(this.type == "publication") {
      this.linkToAdvancedSearchPage = this.properties.searchLinkToAdvancedPublications;
      this.urlParam = "articleId";
    } else if(this.type == "dataset") {
      this.linkToAdvancedSearchPage = this.properties.searchLinkToAdvancedDatasets;
      this.urlParam = "datasetId";
    } else if(this.type == "software") {
      this.linkToAdvancedSearchPage = this.properties.searchLinkToAdvancedSoftware;
      this.urlParam = "softwareId";
    } else if(this.type == "other") {
      this.linkToAdvancedSearchPage = this.properties.searchLinkToAdvancedOrps;
      this.urlParam = "orpId";
    } else if(this.type == "project") {
      this.linkToAdvancedSearchPage = this.properties.searchLinkToAdvancedProjects;
      this.urlParam = "projectId";
    } else if(this.type == "organization") {
      this.linkToAdvancedSearchPage = this.properties.searchLinkToAdvancedOrganizations;
      this.urlParam = "organizationId";
    } else if(this.type == "dataprovider") {
      this.linkToAdvancedSearchPage = this.properties.searchLinkToAdvancedDataProviders;
      this.urlParam = "datasourceId";
    }
  }

  public  quote(params: string):string {
    return '"'+params+'"';
  }
  public getResultPreview(result: SearchResult): ResultPreview {
    return ResultPreview.searchResultConvert(result, (result.entityType)?result.entityType:this.type);
  }

}
