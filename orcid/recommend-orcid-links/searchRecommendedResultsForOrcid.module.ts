import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';
import { RouterModule } from '@angular/router';

import {SearchRecommendedResultsForOrcidComponent} from './searchRecommendedResultsForOrcid.component';

import {SearchResultsModule } from '../../searchPages/searchUtils/searchResults.module';

import {SearchFormModule} from  '../../searchPages/searchUtils/searchForm.module';
import {SearchResearchResultsModule} from "../../searchPages/searchResearchResults.module";
import {OrcidService} from "../orcid.service";
import {SearchRecommendedResultsForOrcidRoutingModule} from "./searchRecommendedResultsForOrcid-routing.module";
// import {BreadcrumbsModule} from "../utils/breadcrumbs/breadcrumbs.module";

@NgModule({
  imports: [
    CommonModule, FormsModule,
    RouterModule,
    SearchRecommendedResultsForOrcidRoutingModule,
    SearchFormModule, SearchResearchResultsModule,
    // , BreadcrumbsModule
  ],
  declarations: [
    SearchRecommendedResultsForOrcidComponent
  ],
  providers:[OrcidService],
  exports: [
    SearchRecommendedResultsForOrcidComponent
  ]
})
export class SearchRecommendedResultsForOrcidModule { }
