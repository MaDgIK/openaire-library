import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {LoginGuard} from "../../login/loginGuard.guard";
import {PreviousRouteRecorder} from "../../utils/piwik/previousRouteRecorder.guard";
import {SearchRecommendedResultsForOrcidComponent} from "./searchRecommendedResultsForOrcid.component";

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '', component: SearchRecommendedResultsForOrcidComponent,
        canActivate: [LoginGuard],
        canDeactivate: [PreviousRouteRecorder]
      }

    ])
  ]
})
export class SearchRecommendedResultsForOrcidRoutingModule { }
