import { Component } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Subscriber, Subscription} from "rxjs";
import {OrcidService} from "./orcid.service";
import {properties} from "../../../environments/environment";
import {RouterHelper} from "../utils/routerHelper.class";
import {Meta, Title} from "@angular/platform-browser";
import {UserManagementService} from "../services/user-management.service";

@Component({
  selector: 'orcid',
  template: `    
    <div class="uk-section uk-container">
      <div *ngIf="orcidMessage">{{orcidMessage}}</div>
      <div *ngIf="message" [innerHTML]="message"></div>
      <div *ngIf="showLoading" class="uk-animation-fade uk-margin-top  uk-width-1-1" role="alert">
        <span class="loading-gif  uk-align-center"></span>
      </div>
    </div>
  `
})

export class OrcidComponent {
  public   subscriptions: Subscription[] = [];

  public showLoading: boolean = false;
  public message: string = "";
  public orcidMessage: string = "";

  public source: string = "";
  public code: string = "";
  public gotToken: boolean = false;

  public routerHelper:RouterHelper = new RouterHelper();

  constructor(private route: ActivatedRoute,
              private _router: Router,
              private orcidService: OrcidService,
              private userManagementService: UserManagementService,
              private _meta: Meta, private _title: Title) {}

  ngOnInit() {
    var description = "Openaire, ORCID";
    this.updateTitle("Connect with ORCID");
    this.updateDescription(description);
    this.updateUrl( properties.domain + properties.baseLink + this.route.url);

    this.subscriptions.push(this.route.queryParams.subscribe(params => {
      this.gotToken = false;

      this.source = params['source'];
      this.code = params['code'];
      if (this.code) {
        if(this.source == "openaire") {
          this.getToken(params['code']);
        } else {
          this.getPersonalDetails();
        }
      } else if(params['error']) {
        this.showLoading = false;
        this.orcidMessage = params['error_description'];
        this.message = "<div>An error occured while trying to grant access OpenAIRE. </div>" +
          "<div>Please close this window and try again!</div>";
      } else {
        this.message = "No code provided to connect your ORCID with OpenAIRE. Please try again!"
      }
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscriber) {
        subscription.unsubscribe();
      }
    });
  }

  // the following method uses client ID and client Secret, which are sessitive data.
  // Our API should return the response, without revealing the call to ORCID.
  private getToken(code: string) {
    this.showLoading = true;
    this.orcidService.getToken(code).subscribe(
      gotTokens => {
        this.gotToken = true;
        if(gotTokens == null || gotTokens['value'] == false) {
          this.showLoading = false;
          this.message = "<div>An error occured while trying to connect your ORCID iD with OpenAIRE. Please try again!</div>" +
            "<div class='uk-margin-small-top'>Need help? <a href='https://www.openaire.eu/support/helpdesk'>Contact us!</a></div>";
        } else {
          if(this.source == "openaire") {
            this.message = "<div>Thank you for connecting your ORCID iD with OpenAIRE!</div>" +
              "<div class='uk-margin-small-top'>This window will automatically close and you will be ready to link OpenAIRE research results with your ORCID iD.</div>";
            if(window && window.opener) {
              window.opener.postMessage("success", "*");
              window.close();
            }
            setTimeout(() => {
              this.message += "<div class='uk-margin-top'>If this window does not close automatically, please close it and continue!</div>";
            }, 3000);
          } else {
            this.message = "<div>Thank you for connecting your ORCID iD with OpenAIRE!</div>" +
              "<div class='uk-margin-small-top'>You will automatically be redirected to our advanced search page where you can link OpenAIRE research results with your ORCID iD.</div>";

            this.getPersonalDetails();
          }
          // this.message = "Thank you for connecting your ORCID iD with OpenAIRE! Please close this window and continue!";
        }
        this.showLoading = false;
      },
      error => {
        this.showLoading = false;

        this.gotToken = true;

        console.error("Error getting token from code: "+code, error);
        this.message = "An error occured while trying to connect your ORCID iD with OpenAIRE. Please try again!";
      }
    )
  }

  private getPersonalDetails() {
    //get author name
    this.subscriptions.push(this.orcidService.getPersonalDetails().subscribe(
      details => {
        let author: string = "";

        if(details && details['name']) {
          let name: string = details['name'];
          if(name['given-names'] && name['given-names']['value']) {
            author = name['given-names']['value'];
          }
          if(name['family-name'] && name['family-name']['value']) {
            author += (author ? " " : "") + name['family-name']['value'];
          }
        }

        let params = this.routerHelper.createQueryParams(['f0', 'fv0'], ['resultauthor', author]);
        this._router.navigate([properties.searchLinkToAdvancedResults], {queryParams: params});
      },
      error => {
        console.error("Error getting personal details", error);
        if(this.gotToken) {
          this.subscriptions.push(this.userManagementService.getUserInfo().subscribe(user => {
            if (user) {
              let params = this.routerHelper.createQueryParams(['f0', 'fv0'], ['resultauthor', user.fullname]);
              this._router.navigate([properties.searchLinkToAdvancedResults], {queryParams: params});
            } else {
              this._router.navigate([properties.searchLinkToAdvancedResults], {});
            }
          },
          error => {
            setTimeout(() => {
              this.message += "<div class='uk-margin-top'>If you are not authomatically redirected, please navigate to our search pages.</div>";
            }, 3000);
          }));
        } else {
          this.getToken(this.code);
        }
      }
    ));
  }

  private updateTitle(title: string) {
    this._title.setTitle(title);
    this._meta.updateTag({content: title}, "property='og:title'");
  }

  private updateDescription(description: string) {
    this._meta.updateTag({content: description}, "name='description'");
    this._meta.updateTag({content: description}, "property='og:description'");
  }

  private updateUrl(url: string) {
    this._meta.updateTag({content: url}, "property='og:url'");
  }
}
