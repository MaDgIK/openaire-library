import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {RouterModule} from '@angular/router';

import {OrcidComponent} from './orcid.component';
import {OrcidService} from './orcid.service';
import {OrcidWorkComponent} from './orcid-work.component';
import {AlertModalModule} from '../utils/modal/alertModal.module';
import {ResultLandingService} from '../landingPages/result/resultLanding.service';
import {LoadingModule} from '../utils/loading/loading.module';
import {ResultLandingUtilsModule} from '../landingPages/landing-utils/resultLandingUtils.module';
import {IconsModule} from '../utils/icons/icons.module';
import {IconsService} from "../utils/icons/icons.service";
import {orcid_add, orcid_bin} from "../utils/icons/icons";
import {FullScreenModalModule} from "../utils/modal/full-screen-modal/full-screen-modal.module";
import {LogServiceModule} from "../utils/log/LogService.module";
import {OrcidRoutingModule} from "./orcid-routing.module";

@NgModule({
  imports: [
    CommonModule, RouterModule, AlertModalModule, LoadingModule, ResultLandingUtilsModule,
    IconsModule, FullScreenModalModule, LogServiceModule
  ],
  declarations: [
    OrcidComponent,
    OrcidWorkComponent
  ],
  providers:[
    OrcidService, ResultLandingService
  ],
  exports: [
    OrcidComponent,
    OrcidWorkComponent
  ]
})


export class OrcidCoreModule{
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([orcid_add, orcid_bin])
  }
}
