import {NgModule} from '@angular/core';
import {IconsService} from "../utils/icons/icons.service";
import {orcid_add, orcid_bin} from "../utils/icons/icons";
import {OrcidRoutingModule} from "./orcid-routing.module";
import {OrcidCoreModule} from "./orcid-core.module";
import {OrcidComponent} from "./orcid.component";

@NgModule({
  imports: [OrcidCoreModule, OrcidRoutingModule],
  exports: [OrcidComponent]
})


export class OrcidModule{
}
