import { NgModule}            from '@angular/core';

import {SEOService} from './SEO.service';
@NgModule({
  imports: [
  ],
  providers:[SEOService]
})
export class SEOServiceModule { }
