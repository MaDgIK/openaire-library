import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {SliderTabsComponent} from "./slider-tabs.component";
import {SliderTabComponent} from "./slider-tab.component";
import {RouterModule} from "@angular/router";
import {SliderUtilsModule} from "../slider-utils/slider-utils.module";

@NgModule({
  imports: [CommonModule, RouterModule, SliderUtilsModule],
  declarations: [SliderTabsComponent, SliderTabComponent],
  exports: [SliderTabsComponent, SliderTabComponent]
})
export class SliderTabsModule {}

