import {Component, Input} from "@angular/core";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'slider-tab',
  template: `<ng-content />`
})
export class SliderTabComponent {
  @Input("tabTitle")
  public title: string;
  @Input("tabId")
  public id: string | number;
  @Input()
  public active: boolean = false;
  @Input()
  public disabled: boolean = false;
  @Input()
  public invisible: boolean = false;
  @Input()
  public align: 'left' | 'right' = 'left';
  @Input()
  public routerLink: any[] | string | null | undefined = null;
  @Input()
  public queryParams: any = null;
  @Input()
  public customClass: string = '';
  @Input() tabTemplate: any;
  @Input()
  public relativeTo: ActivatedRoute = null;
}
