import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {QuickContactComponent} from './quick-contact.component';
import {IconsModule} from '../../utils/icons/icons.module';
import {ContactUsModule} from "../../contact-us/contact-us.module";
import {LoadingModule} from "../../utils/loading/loading.module";

@NgModule({
  imports: [
    CommonModule, FormsModule, IconsModule, ContactUsModule, LoadingModule
  ],
  declarations: [
    QuickContactComponent
  ],
  providers:[],
  exports: [
    QuickContactComponent
  ]
})
export class QuickContactModule {
}
