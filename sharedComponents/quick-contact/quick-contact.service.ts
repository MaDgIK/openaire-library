import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";

@Injectable({
	providedIn: "root"
})
export class QuickContactService {
	private display: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

	public get isDisplayed(): Observable<boolean> {
		return this.display.asObservable();
	}

	public setDisplay(display: boolean) {
		this.display.next(display);
	}
}