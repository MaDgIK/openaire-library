import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PickIconComponent } from './pick-icon.component';



@NgModule({
  declarations: [
    PickIconComponent
  ],
  exports: [
    PickIconComponent
  ],
  imports: [
    CommonModule
  ]
})
export class PickIconModule { }
