import {Component, OnDestroy, OnInit} from '@angular/core';
import {Icon} from "../../utils/icons/icons";
import {IconsService} from "../../utils/icons/icons.service";

@Component({
  selector: 'pick-icon',
  templateUrl: './pick-icon.component.html'
})
export class PickIconComponent implements OnInit, OnDestroy{
  customIcons: Icon[] = [];

  constructor(private iconService: IconsService) {
  }

  ngOnInit() {
    this.customIcons = this.iconService.getAll();
  }

  ngOnDestroy() {
  }
}
