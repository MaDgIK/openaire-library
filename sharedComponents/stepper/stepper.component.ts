import {Component} from '@angular/core';
/*
* usage example 
* 
*  <stepper>
   <step status="active" stepId="step1" stepNumber="1"
         stepText="Step 1"
         active="step1" [showStepLine]="false"></step>
   <step status="disabled" stepId="step2" stepNumber="2"
         stepText="Step 2"
         active="step1"
         [showStepLine]="true"></step>

 </stepper>
* */
@Component({
  selector: 'stepper',
  template: `
    <div class="stepper" >
      <div class=" uk-flex uk-flex-center "  >
        <div class=" uk-grid uk-grid-small uk-flex uk-flex-bottom uk-text-small uk-width-auto">
          <ng-content></ng-content>
        </div>
      </div>
    </div>

  `

})
export class StepperComponent {
   
}
