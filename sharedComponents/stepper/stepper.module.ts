import { NgModule } from '@angular/core';
import {SharedModule} from "../../../openaireLibrary/shared/shared.module";
import {StepperComponent} from "./stepper.component";
import {StepComponent} from "./step.component";
import {StepLineComponent} from "./stepLine.component";

 

@NgModule({
  imports: [
    SharedModule,  

  ],
  declarations: [
    StepperComponent, StepComponent, StepLineComponent
  ], exports: [StepperComponent, StepComponent, StepLineComponent ]
})
export class StepperModule { }
