import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'step',
  template: `


    <div class=" uk-width-auto uk-text-center " style="position: relative; ">
      <div style="position: relative; z-index:10; min-width: 140px;" class="">
        <div class="uk-margin-small-left stepper-text uk-text-uppercase uk-margin-bottom uk-text-truncate"
             [class]="status">{{stepText}}</div>
        <div>
          <button class="uk-icon-button uk-border-circle" [class]="status" [class.uk-disabled]="status=='disabled'"
                  (click)="stepChanged.emit(stepId)">
            <span *ngIf="status != 'done'">{{stepNumber}}</span>
            <span *ngIf="status == 'done'" uk-icon="check"></span>
          </button>
        </div>
      </div>
      <step-line *ngIf="showStepLine" [status]="status" [stepNumber]="stepNumber" class="stepper-line-container"
                 [style.z-index]="(+stepNumber)"></step-line>
    </div>
  `

})
export class StepComponent {
  @Input() status: 'active' | 'default' | 'disabled' | 'done';
  @Input() stepId;
  @Input() stepNumber;
  @Input() stepText;
  @Input() active;
  @Output() stepChanged: EventEmitter<string> = new EventEmitter<string>();
  @Input() showStepLine:boolean = false;
}
