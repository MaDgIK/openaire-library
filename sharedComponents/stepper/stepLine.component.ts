import {Component, Input} from '@angular/core';

@Component({
  selector: 'step-line',
  template: `


   
    <div class="stepper-line uk-margin-bottom uk-width-medium@m uk-width-small@s" [class]="status + ' step' +  stepNumber"></div>
    `

})
export class StepLineComponent {
  @Input() status: 'active' | 'default' | 'disabled' | 'done';
  @Input() stepNumber;
  
}
