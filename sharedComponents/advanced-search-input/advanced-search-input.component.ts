import {
  AfterContentInit,
  Component,
  ContentChildren,
  EventEmitter, HostListener,
  Input,
  OnDestroy,
  Output,
  QueryList
} from "@angular/core";
import {InputComponent} from "../input/input.component";
import {EntitiesSelectionComponent} from "../../searchPages/searchUtils/entitiesSelection.component";

@Component({
  selector: 'advanced-search-input',
  template: `
    <div *ngIf="inputs.length > 0" class="search-input" [class.dark]="dark" [ngClass]="searchInputClass"
         [class.small-vertical]="smallVertical">
      <div class="uk-flex uk-flex-middle">
        <div class="uk-width-expand">
          <div class="uk-grid uk-grid-collapse inputs" [ngClass]="'uk-child-width-1-' + length" uk-grid>
            <ng-content></ng-content>
          </div>
        </div>
        <div class="uk-width-auto filters-toggle">
          <ng-content select="[filters-toggle]"></ng-content>
        </div>
        <div class="uk-width-auto" [class.uk-flex-first]="iconPosition === 'left'">
          <div class="search-icon" [class.disabled]="disabled" (click)="searchEmitter.emit()">
            <icon name="search" [flex]="true" ratio="1.3"></icon>
          </div>
        </div>
      </div>
    </div>
  `
})
export class AdvancedSearchInputComponent implements AfterContentInit, OnDestroy {
  @ContentChildren(InputComponent) inputs: QueryList<InputComponent>;
  @ContentChildren(EntitiesSelectionComponent) entities: QueryList<EntitiesSelectionComponent>;
  @Input() iconPosition: 'left' | 'right' = 'right';
  @Input() disabled: boolean = false;
  @Input() searchInputClass: string = 'inner';
  @Input() dark: boolean;
  @Input() smallVertical: boolean = false;
  @Output() searchEmitter: EventEmitter<void> = new EventEmitter<void>();
  
  @HostListener('window:keydown.enter', ['$event'])
  enter(event: KeyboardEvent) {
    let input: InputComponent = this.inputs.toArray().find(input => input.focused && input.type !== 'select');
    if (input) {
      input.focus(false, event);
      event.preventDefault();
      this.searchEmitter.emit();
    }
  }
  
  constructor() {
  }
  
  ngAfterContentInit() {
    this.init();
    this.inputs.changes.subscribe(input => {
      this.init();
    });
  }
  
  ngOnDestroy() {
  }

  init() {
    this.inputs.forEach(input => {
      input.inputClass = 'advanced-search';
    });
  }

  focusNext(input: InputComponent | EntitiesSelectionComponent, event: any = null) {
    if(!event || !event.init) {
      setTimeout(() => {
        if(input instanceof  InputComponent) {
          input.focus(true);
        } else {
          input.input.focus(true);
        }
      }, 100);
    }
  }
  
  get length() {
    return this.inputs.length + this.entities.length;
  }
}
