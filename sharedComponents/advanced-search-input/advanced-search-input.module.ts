import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {AdvancedSearchInputComponent} from "./advanced-search-input.component";
import {IconsModule} from "../../utils/icons/icons.module";

@NgModule({
  imports: [CommonModule, IconsModule],
  declarations: [AdvancedSearchInputComponent],
  exports: [AdvancedSearchInputComponent]
})
export class AdvancedSearchInputModule {

}
