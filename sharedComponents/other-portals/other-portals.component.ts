import {Component, Input} from '@angular/core';
import {EnvProperties} from "../../utils/properties/env-properties";
import {OpenaireEntities} from "../../utils/properties/searchFields";

@Component({
  selector: 'other-portals',
  templateUrl: 'other-portals.component.html'
})
export class OtherPortalsComponent {
  @Input() portal = 'connect';
  @Input() properties: EnvProperties = null;
  openaireEntities = OpenaireEntities;

  constructor() {}
}
