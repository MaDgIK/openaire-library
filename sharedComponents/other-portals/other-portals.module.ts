import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {RouterModule} from "@angular/router";

import {OtherPortalsComponent} from './other-portals.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [
    OtherPortalsComponent
  ],
  providers: [],
  exports: [
    OtherPortalsComponent
  ]
})
export class OtherPortalsModule {
}
