import {AfterViewInit, Component, ElementRef, Input, ViewChild} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {SliderContainerComponent} from "./slider-container.component";
import {SliderItemComponent} from "./slider-item.component";

export interface Link {
  routerLink?: {
    commands: string[] | string,
    queryParams?: Object,
    fragment?: string,
    relativeTo?: ActivatedRoute
  }
  href?: string
  external?: boolean
}

@Component({
  selector: 'slider-nav-item',
  template: `
    <div *ngIf="container" (click)="container.start(start)" class="uk-flex uk-flex-middle" [class.uk-active]="active">
        <div class="uk-width-expand">
            <ng-content></ng-content>
        </div>
        <div *ngIf="link" class="action">
            <a *ngIf="link?.routerLink" #linkElement class="uk-text-decoration-none" [routerLink]="link.routerLink.commands" [queryParams]="link.routerLink.queryParams"
               [fragment]="link.routerLink.fragment"  [relativeTo]="link.routerLink.relativeTo" [target]="link.external?'_blank':'_self'">
                <icon name="chevron_right" ratio="1.5" [flex]="true"></icon>
            </a>
            <a *ngIf="link?.href" #linkElement [href]="link.href" class="uk-text-decoration-none" [target]="link.external?'_blank':'_self'">
                <icon name="chevron_right" ratio="1.5"  [flex]="true"></icon>
            </a>
        </div>
    </div>
  `
})
export class SliderNavItemComponent extends SliderItemComponent implements AfterViewInit {
  @Input()
  link: Link = null;
  @Input()
  start: number;
  active: boolean = false;
  container: SliderContainerComponent;
  background: string;
  @ViewChild('linkElement') linkElement: ElementRef;

  ngAfterViewInit() {
    if(this.linkElement) {
      this.linkElement.nativeElement.style.background = this.background;
    }
  }
}
