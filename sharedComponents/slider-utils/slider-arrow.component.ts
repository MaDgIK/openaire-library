import {Component, Input} from "@angular/core";

@Component({
  selector: 'slider-arrow',
  template: `
    <div *ngIf="type" class="uk-slider-arrow uk-visible@m" [ngClass]="positionClasses" [attr.uk-slider-item]="type">
      <button class="uk-icon-button uk-icon-button-small uk-box-no-shadow uk-box-no-shadow-hover uk-border">
        <icon [name]="icon" [flex]="true" visuallyHidden="download"></icon>
      </button>
    </div>
  `
})
export class SliderArrowComponent {
  @Input()
  public type: 'previous' | 'next';


  get positionClasses() {
    return (this.type == 'previous')?'uk-position-center-left uk-slider-left':'uk-position-center-right uk-slider-right'
  }

  get icon() {
    return (this.type == 'previous')?'chevron_left':'chevron_right'
  }
}
