import {Component, ElementRef, Input} from "@angular/core";

@Component({
  selector: 'slider-item',
  template: `
    <ng-content></ng-content>
  `
})
export class SliderItemComponent {
  @Input()
  type: 'slide' | 'static' = 'slide';
  @Input()
  start: number;

  constructor(private element: ElementRef) {
  }

  init(animation: string) {
    this.element.nativeElement.classList.add(animation);
  }

  setActive(active: boolean) {
    if(active) {
      this.element.nativeElement.classList.remove('uk-hidden');
    } else {
      this.element.nativeElement.classList.add('uk-hidden');
    }
  }
}
