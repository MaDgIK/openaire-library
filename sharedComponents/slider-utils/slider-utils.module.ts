import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {SliderArrowComponent} from "./slider-arrow.component";
import {IconsModule} from "../../utils/icons/icons.module";
import {SliderNavItemComponent} from "./slider-nav-item.component";
import {SliderItemComponent} from "./slider-item.component";
import {RouterLink} from "@angular/router";
import {SliderColumnComponent} from "./slider-column.component";
import {SliderContainerComponent} from "./slider-container.component";

@NgModule({
  imports: [CommonModule, IconsModule, RouterLink],
  declarations: [SliderContainerComponent, SliderArrowComponent, SliderNavItemComponent, SliderItemComponent, SliderColumnComponent],
  exports: [SliderContainerComponent, SliderArrowComponent, SliderNavItemComponent, SliderItemComponent, SliderColumnComponent],
})
export class SliderUtilsModule {}
