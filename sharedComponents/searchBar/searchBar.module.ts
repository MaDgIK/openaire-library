import { NgModule }            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';

import { RouterModule } from "@angular/router";

import { SearchBarComponent} from './searchBar.component';
import {AdvancedSearchFormModule} from "../../searchPages/searchUtils/advancedSearchForm.module";
import {EntitiesSelectionModule} from "../../searchPages/searchUtils/entitiesSelection.module";

@NgModule({
  imports: [
    CommonModule, FormsModule,
    RouterModule, AdvancedSearchFormModule, EntitiesSelectionModule
  ],
  declarations: [
    SearchBarComponent
  ],
  providers:[],
  exports: [
    SearchBarComponent
    ]
})
export class SearchBarModule{ }
