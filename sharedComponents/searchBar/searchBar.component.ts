import {Component, Input} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {EnvProperties} from "../../utils/properties/env-properties";
import {SearchCustomFilter} from "../../searchPages/searchUtils/searchUtils.class";
import {Subscriber} from "rxjs";

@Component({
  selector: 'search-bar',
  templateUrl: 'searchBar.component.html',

})
export class SearchBarComponent {

  @Input() searchRoute: string = "/search/find";
  @Input() searchPlaceHolder: string = "Search for research results";
  @Input() entitiesSelection:boolean = true;
  @Input() properties:EnvProperties;
  keyword: string = "";
  entityType = "all";
  enableSearchbar:boolean = true;
  customFilter: SearchCustomFilter   = null;
  @Input() communityId;
  @Input() onlyresults:boolean=false;
  parameters = {};
  advancedSearchLink = null;
  subscriptions = [];
  constructor(private router: Router,
              private  route: ActivatedRoute ) {
    this.subscriptions.push(this.router.events.subscribe((e) => {
      if(e instanceof NavigationEnd){
        // console.log(e)
        this.initialize();
      }
    }));
  }
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscriber) {
        subscription.unsubscribe();
      }
    });
  }
  ngOnInit() {

    // this.activeRouteEnabled = false;
    if(this.communityId){
      this.customFilter = new SearchCustomFilter("Community", "communityId", this.communityId, "");
    }
    this.entityType = "all";
    this.subscriptions.push(this.route.queryParams.subscribe(params => {
      this.parameters = Object.assign({}, params);
      if(params["fv0"] && params["f0"] && params["f0"] == "q"){
        this.keyword =params["fv0"];
      }
      if(this.onlyresults) {
        if (params["type"] && params["type"].length > 0) {
          let types = params["type"].split(",");
          if (types.length == 1) {
            if (types.indexOf("publications") != -1) {
              this.entityType = "publications";
            } else if (types.indexOf("datasets") != -1) {
              this.entityType = "datasets";
            } else if (types.indexOf("software") != -1) {
              this.entityType = "software";
            } else if (types.indexOf("other") != -1) {
              this.entityType = "other";
            }
          }
        }
      }
    }));
    this.initialize()
  }
  initialize(){
    if(!this.onlyresults){
      let currentRoute= this.getCurrentRoute();
      if(currentRoute== this.properties.searchLinkToProjects){
        this.entityType = "project";
      }else if(currentRoute== this.properties.searchLinkToDataProviders){
        this.entityType = "dataprovider";
      }else if(currentRoute== this.properties.searchLinkToOrganizations){
        this.entityType = "organization";
      }else if(currentRoute== this.properties.searchLinkToResults){
        this.entityType = "result";
      }else {
        // not one of the search bar search pages
        this.entityType = "result";
        this.keyword = "";
      }
    }
    if(this.getCurrentRoute() == this.properties.searchLinkToAdvancedResults){
      this.enableSearchbar =  false;
    }else{
      this.enableSearchbar = true;
    }
    this.showAdvancedLink();
  }
  showAdvancedLink(){
    if(this.getCurrentRoute() == this.properties.searchLinkToResults && this.entityType == "result"){
      this.advancedSearchLink = this.properties.searchLinkToAdvancedResults;
    }else{
      this.advancedSearchLink = null;
    }
  }
  isEnabled(required, enabled) {
    if (!required) {
      return true;
    }
    for (let requiredEntity of required) {
      if (typeof enabled[requiredEntity] === "undefined" || enabled[requiredEntity] == false) {
        return false;
      }
    }
    return true;
  }
  getCurrentRoute() {
    return this.router.url.split('?')[0];
  }
  entityChanged($event){

    this.entityType = $event.entity;
    this.searchRoute = $event.simpleUrl;
    if(!this.onlyresults && this.entityType == "result") {
      this.parameters["qf"] = true;
    }
    this.showAdvancedLink();
  }
  keywordChanged(){
   if(this.getCurrentRoute()!=this.searchRoute) {
      this.parameters = {};
    }
    if ( this.keyword.length > 0) {
      this.parameters["fv0"] = this.keyword;
      this.parameters["f0"] = "q";
    }else{
      delete this.parameters['fv0'];
      delete this.parameters['f0'];
    }
    if(this.onlyresults && this.entityType != "all"){
      this.parameters["type"] = this.entityType;
    }else{
      delete this.parameters['type'];
    }
    //set true only if it is not set allready
    if(!this.parameters["qf"]) {
      this.parameters["qf"] = true;
    }
    this.router.navigate([this.searchRoute], {queryParams: this.parameters} );
  }
}
