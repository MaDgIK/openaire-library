import {Component, Input, OnDestroy, OnInit, ViewChild} from "@angular/core";
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators} from "@angular/forms";
import {Subscriber} from "rxjs";
import {StringUtils} from "../../utils/string-utils.class";
import {Email} from "../../utils/email/email";
import {Body} from "../../utils/email/body";
import {CommunityService} from "../../connect/community/community.service";
import {Composer} from "../../utils/email/composer";
import {Session, User} from "../../login/utils/helper.class";
import {EmailService} from "../../utils/email/email.service";
import {properties} from "../../../../environments/environment";
import {CommunityInfo} from "../../connect/community/communityInfo";
import {NotificationHandler} from "../../utils/notification-handler";
import {InputComponent} from "../input/input.component";

@Component({
  selector: 'subscriber-invite',
  template: `
    <div *ngIf="body" class="uk-grid uk-child-width-1-1" uk-grid [formGroup]="inviteForm">
      <div input [formInput]="inviteForm.get('name')" placeholder="From"></div>
      <div #emailInput input [formInput]="inviteForm.get('recipients')" type="chips"
           placeholder="Recipients" hint="Add a recipient" [visibleChips]="3"
           [addExtraChips]="true" [validators]="validators" [separators]="[',']">
        <div note>Separate emails with commas</div>
      </div>
      <div>
        <label class="uk-text-meta">Message *</label>
        <div class="uk-margin-small-top">
          <ckeditor *ngIf="isManager" class="form-control" formControlName="message" id="message"
                    debounce="400" (ready)="loading = false"
                    [config]="{ extraAllowedContent: '* [uk-*](*) ; span', disallowedContent: 'script; *[on*]',
                                removeButtons:  'Save,NewPage,DocProps,Preview,Print,' +
                                                'Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,' +
                                                'CreateDiv,Flash,PageBreak,' +
                                                'Subscript,Superscript,Anchor,Smiley,Iframe,Styles,Font,About,Language',
                                extraPlugins: 'divarea'}">
          </ckeditor>
          <div *ngIf="!isManager" [innerHTML]="body.paragraphs"></div>
          <div class="uk-margin-top">
            {{body.signature}}
            <span *ngIf="body.fromName == ''" class="uk-text-muted">
              <i>{{body.fromMessage}}...</i>
            </span>
            <span *ngIf="body.fromName !=''">
              {{body.fromMessage}}
              <b>{{body.fromName}}</b>
            </span><br>
            <a href="https://www.openaire.eu">www.openaire.eu</a>
          </div>
        </div>
      </div>
    </div>
  `
})
export class SubscriberInviteComponent implements OnInit, OnDestroy {
  @Input()
  public user: User;
  public community: CommunityInfo;
  public inviteForm: FormGroup;
  public email: Email;
  public body: Body;
  public validators: ValidatorFn[] = [Validators.email];
  public loading: boolean = true;
  private subscriptions: any[] = [];
  @ViewChild('emailInput') emailInput: InputComponent;

  constructor(private fb: FormBuilder,
              private emailService: EmailService,
              private communityService: CommunityService) {
  }
  
  ngOnInit() {
    this.reset();
  }
  
  ngOnDestroy() {
    this.unsubscribe();
  }
  
  unsubscribe() {
    this.subscriptions.forEach(subscription => {
      if(subscription instanceof Subscriber) {
        subscription.unsubscribe();
      }
    });
  }
  
  reset() {
    this.unsubscribe();
    this.inviteForm = this.fb.group({
      name: this.fb.control('', Validators.required),
      recipients: this.fb.array([], Validators.required),
      message: this.fb.control('', Validators.required)
    });
    this.subscriptions.push(this.communityService.getCommunityAsObservable().subscribe(community => {
      this.community = community;
      if(this.community) {
        this.inviteForm.get('name').setValue(this.user.fullname);
        this.body = Composer.initializeInvitationsBody(community.communityId, community.title, this.user.fullname);
        this.email = Composer.initializeInvitationsEmail(community.title);
        this.inviteForm.get('message').setValue(this.body.paragraphs);
      }
    }));
    if(!this.isManager) {
      this.loading = false;
    }
  }
  
  invite() {
    if(this.emailInput.searchControl.getRawValue() && this.emailInput.searchControl.valid) {
      this.emailInput.add(null,true);
    }
    this.loading = true;
    this.body.paragraphs = this.inviteForm.getRawValue().message;
    this.email.body = Composer.formatEmailBodyForInvitation(this.body);
    this.email.recipients = this.inviteForm.get('recipients').value;
    this.subscriptions.push(this.emailService.sendEmail(properties, this.email).subscribe(res => {
      if(res['success']) {
        NotificationHandler.rise('Invitation to join has been <b>sent</b>');
      } else {
        NotificationHandler.rise('An error has occurred. Please try again later', 'danger');
      }
      this.reset();
      this.loading = false;
    },error => {
      NotificationHandler.rise('An error has occurred. Please try again later', 'danger');
      this.reset();
      this.loading = false;
    }));
  }
  
  get isManager() {
    return Session.isPortalAdministrator(this.user) || Session.isCurator('community', this.user) || Session.isManager('community', this.community.communityId, this.user);
  }
  
  get valid() {
    return !this.loading && this.inviteForm && this.inviteForm.valid || (this.emailInput && this.emailInput.searchControl.getRawValue() && this.emailInput.searchControl.valid);
  }
}
