import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {SubscriberInviteComponent} from "./subscriber-invite.component";
import {InputModule} from "../input/input.module";
import {CKEditorModule} from "ng2-ckeditor";
import {ReactiveFormsModule} from "@angular/forms";
import {EmailService} from "../../utils/email/email.service";

@NgModule({
  imports: [CommonModule, InputModule, CKEditorModule, ReactiveFormsModule],
  declarations: [SubscriberInviteComponent],
  exports: [SubscriberInviteComponent],
  providers: [EmailService]
})
export class SubscriberInviteModule {

}
