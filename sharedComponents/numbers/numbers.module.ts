import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NumbersComponent} from './numbers.component';
import {IconsModule} from '../../utils/icons/icons.module';
import {IconsService} from '../../utils/icons/icons.service';
import {book, cog, database, earth} from '../../utils/icons/icons';
import {SearchResearchResultsService} from '../../services/searchResearchResults.service';
import {SearchDataprovidersService} from '../../services/searchDataproviders.service';
import {RefineFieldResultsService} from '../../services/refineFieldResults.service';
import {LoadingModule} from '../../utils/loading/loading.module';
import {RouterModule} from "@angular/router";
import {GroupedRequestsServiceModule} from "../../services/groupedRequestsService.module";

@NgModule({
  imports: [CommonModule, IconsModule, LoadingModule, RouterModule, GroupedRequestsServiceModule],
  declarations: [NumbersComponent],
  exports: [NumbersComponent],
  providers: [SearchResearchResultsService, SearchDataprovidersService, RefineFieldResultsService]
})
export class NumbersModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([book, database, cog, earth])
  }
}
