import {Component, Input} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import 'rxjs';
import {MenuItem} from './menu';
import {ConfigurationService} from '../utils/configuration/configuration.service';
import {EnvProperties} from "../utils/properties/env-properties";
import {Subscription} from "rxjs";
import {properties} from '../../../environments/environment';
import {HelperService} from "../utils/helper/helper.service";

@Component({
  selector: 'bottom',
  templateUrl: 'bottom.component.html'
})
export class BottomComponent {
  @Input() showSocialButtons: boolean = true;
  @Input() showOpenaire: boolean = true;
  @Input() showMenuItems: boolean = false;
  @Input() showCommision: boolean = true;
  @Input() assetsPath: string = 'assets/common-assets/';
  @Input() menuItems: MenuItem [];
  @Input() communityId;
  showPage = {};
  @Input() grantAdvance: boolean = true;
  @Input() properties: EnvProperties = properties;
  @Input() centered: boolean = false;
  @Input() shortView: boolean = false;
  @Input() background: string = "uk-tile-default";
  subs: Subscription[] = [];
  public divContents = null;
  
  constructor(private config: ConfigurationService, private route: ActivatedRoute,
              private helper: HelperService) {
  }
  
  ngOnInit() {
    if (!properties.footerGrantText) {
      this.getDivContents();
    }
    this.subs.push(this.route.queryParams.subscribe(params => {
      if (this.showMenuItems) {
        if (this.properties.adminToolsAPIURL && this.communityId) {
          //this.subs.push(this.config.getCommunityInformation(this.properties, this.communityId ).subscribe(data => {
          this.subs.push(this.config.portalAsObservable.subscribe(data => {
            if (data) {
              for (var i = 0; i < data['pages'].length; i++) {
                this.showPage[data['pages'][i]["route"]] = data['pages'][i]["isEnabled"];
                
              }
              // console.log(this.showPage)
            }
          }));
        }
      }
    }));
  }
  
  public ngOnDestroy() {
    for (let sub of this.subs) {
      sub.unsubscribe();
    }
  }
  
  isEnabled(required, enabled) {
    
    if (!required) {
      return true;
    }
    for (let requiredEntity of required) {
      // console.log("R "+requiredEntity +" E "  + enabled[requiredEntity])
      if (typeof enabled[requiredEntity] === "undefined" || enabled[requiredEntity] == false) {
        return false;
      }
    }
    return true;
  }
  
  private getDivContents() {
    let communityId = this.communityId;
    if (!communityId) {
      communityId = properties.adminToolsCommunity;
    }
    this.subs.push(this.helper.getDivHelpContents(this.properties, communityId, "/").subscribe(contents => {
      this.divContents = contents;
    }));
  }
}
