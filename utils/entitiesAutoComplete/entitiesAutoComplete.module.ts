import { NgModule }            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';

import {EntitiesAutocompleteComponent} from './entitiesAutoComplete.component';
import {EntitySearchServiceModule} from './entitySearchService.module';


@NgModule({
  imports: [
    CommonModule, FormsModule, EntitySearchServiceModule
  ],
  declarations: [
     EntitiesAutocompleteComponent
  ],
  exports: [
      EntitiesAutocompleteComponent
    ],
    providers:[ ]
})
export class EntitiesAutocompleteModule { }
