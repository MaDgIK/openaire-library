import { NgModule }            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';

 import {EntitiesSearchService} from './entitySearch.service';


@NgModule({
  imports: [
    CommonModule, FormsModule
  ],
  declarations: [
   ],
  exports: [
     ],
    providers:[ EntitiesSearchService]
})
export class EntitySearchServiceModule { }
