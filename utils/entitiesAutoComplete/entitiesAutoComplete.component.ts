
import {switchMap, distinctUntilChanged, debounceTime} from 'rxjs/operators';
import {Component, ElementRef, Input, Output, EventEmitter, SimpleChange} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {EntitiesSearchService} from './entitySearch.service';
import{EnvProperties} from '../properties/env-properties';
import {StringUtils} from "../string-utils.class";
import {AutoCompleteValue} from "../../searchPages/searchUtils/searchHelperClasses.class";


//Usage example
//<static-autocomplete [(filtered)] =filtered [(selected)] =selected placeHolderMessage = "Search for countries" title = "Countries:" (keywordChange)="keywordChanged($event)"></static-autocomplete>

@Component({
    selector: 'entities-autocomplete',
     host: {
       '(document:click)': 'handleClick($event)',
   },
    template: `

        <div class="custom-autocomplete" [id]=id >
          <div   *ngIf = "showSelected && selectedValue != ''">
            <div  class="uk-alert uk-alert-default uk-flex uk-flex-middle uk-padding-small uk-margin-remove" data-uk-alert=""  *ngFor="let item of selected" [title]="showItem(item)" >
              <div class="uk-width-expand uk-margin-small-right" >{{showItem(item)}} </div>
            <div (click)="remove(item)" aria-hidden="true" title="Remove selection" > <span class="clickable uk-icon">
<svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" icon="close" ratio="1"><path fill="none" stroke="#000" stroke-width="1.06" d="M16,16 L4,4"></path><path fill="none" stroke="#000" stroke-width="1.06" d="M16,4 L4,16"></path></svg>
</span> </div>
          </div>
          </div>
          <input *ngIf = "showInput" type="text" class="auto-complete-input validate filter-input input-sm form-control uk-input " [placeholder]=placeHolderMessage [(ngModel)]=keyword (keyup)=search()  (blur)="keyword = ''"  >
          <!--span [style.display]="showLoading ? 'inline' : 'none'" class="uk-alert uk-alert-primary" data-uk-alert=""> <i class="uk-icon-spinner"></i> Loading... </span>
          <span *ngIf="warningMessage.length > 0" class="uk-alert uk-alert-warning" data-uk-alert=""> {{warningMessage}} <a href="" class="uk-alert-close uk-close"></a></span-->
          <div *ngIf="focus && showInput" class="options uk-dropdown uk-open"   #optionBox
               uk-dropdown="pos: bottom-left; mode: none; stretch: true; offset: 15; flip: false; shift: false" [attr.target]="'#' + id" [attr.boundary]="'#' + id">
                <ul class="uk-nav uk-nav-autocomplete uk-autocomplete-results" >
                      <li>
                          <span *ngIf="!_search.ready && warningMessage.length ==0" class="uk-padding-small"> Loading..... </span>
                          <span *ngIf="warningMessage.length > 0" class="uk-padding-small uk-text-warning">{{warningMessage}}</span>
                          <!--<span *ngIf="results > 0"  >  {{results | number}} results found:</span>-->
                          <!--span *ngIf="results == 0 && !showLoading" class="uk-alert uk-alert-primary" data-uk-alert=""> No results found</span-->
                      </li>
                       <li      *ngFor=" let item of filtered | async">
                        <a *ngIf= "item.id !=-1 && item.id !=-2" (click)="select(item)" [title]="showItem(item)"  style="text-overflow: ellipsis; ">{{showItem(item)}}</a>
                          <span *ngIf= "item.id ==-1"  class="uk-padding-small"> No results found</span>
                          <span *ngIf= "item.id ==-2"  class="uk-padding-small uk-text-warning"> An error occured</span>
                     </li>
                </ul>

          </div>

      </div>

        `
})
export class EntitiesAutocompleteComponent {
    @Input() placeHolderMessage = "Search for entries";
    @Input() title = "Autocomplete";
    @Output() addItem = new EventEmitter(); // when selected list  changes update parent component
    @Output() selectedValueChanged = new EventEmitter(); // when changed  a method for filtering will be called
    @Output() updateValueLabel = new EventEmitter(); //when the value is id sends  an event to update the value (for meta tags)
    @Input() public list = []; // the entries resulted after filtering function
    @Input() public selected = []; // the entries selected from user
    @Input() public keywordlimit = 3; // the minimum length of keyword
    @Input() public showSelected = true; // the minimum length of keyword
    @Input() public multipleSelections:boolean = true;
    @Input() public allowDuplicates:boolean = false;
    @Input() public selectedValue:string = '';
    @Input() public keyword = '';
    @Input() public fieldId:string ;
    @Input() public properties:EnvProperties ;
    @Input() id;
    public currentFieldId: string ;
    public currentFunderId: string ;
    public warningMessage = "";
    public infoMessage = "";
    public tries = 0;
    public showInput = true;
    public sub;
    public done = false;
    public showLoading:boolean = false;
    public searchTermStream = new Subject<string>();
    filtered: Observable<{}> ;
    // public numFilteredResults:number = 0;

    @Input() public funderId:string;
    @Input() public entityType:string ;
    @Input() public depositType:string ;
    public results = 0;
    public focus:boolean  = false;
    constructor (public _search:EntitiesSearchService, private myElement: ElementRef) {
      this.currentFieldId=this.fieldId;
      this.currentFunderId=this.funderId;
      this.initialize();
    }

    ngOnChanges(changes: {[propKey: string]: SimpleChange}) {
         if(this.currentFieldId!=this.fieldId){ //this is going to be called when
          this.currentFieldId=this.fieldId;
          this.initialize();
        }else if(this.currentFunderId!=this.funderId){
          this.currentFunderId=this.funderId;
          this.initialize();
        }
    }
    private initialize(){

      this.showInput = true;
      /* if(this.entityType == "project" && this.funderId ){
        this.filtered  = this.searchTermStream.pipe(
        debounceTime(300),distinctUntilChanged(),
        switchMap((term: string) =>    {
          //console.log("funder"+this.funderId);
            var results = this._search.searchProjectsByFunder(term, (this.funderId == "0"?"":encodeURIComponent(this.funderId)), this.properties);
            this.showLoading = false;
             this.results = results.length;
            return   results;
        }),);
      }else */

        if(this.entityType == "organization" && this.depositType ){
         this.filtered  = this.searchTermStream.pipe(
        debounceTime(300),distinctUntilChanged(),
        switchMap((term: string) => {
          var results = this._search.searchByDepositType(term, this.depositType, this.properties);
          this.showLoading = false;
          this.results = results.length;
          return   results;
      }),);

      }else{

        this.filtered  = this.searchTermStream.pipe(
        debounceTime(300),
        distinctUntilChanged(),
        switchMap((term: string) => {
          var results = this._search.searchByType(StringUtils.URIEncode(term), this.entityType, this.properties);
          this.showLoading = false;
          this.results = results.length;
          return   results;
      }),);

        this.getSelectedNameFromGivenId();
      }

    }
    ngOnDestroy(){
      if(this.sub){
        this.sub.unsubscribe();
      }
    }

    search() {
      this.infoMessage = "";
      if(this.keyword == ""){
        this.tries = 0;
        this.warningMessage = "";
      } else if(this.keyword && this.keyword.length < this.keywordlimit){
        this.tries++;
        if(this.tries == this.keywordlimit -1 ){
          this.warningMessage = "Type at least " + this.keywordlimit + " characters";
          this.tries = 0;
        }
      }else{

        this.tries = 0;
        this.warningMessage = "";
        this.searchTermStream.next(this.keyword);
        // if(this.numFilteredResults ==0){
          this.showLoading = true;
          this.focus = true;
        // }
      }

    }

    remove(item:any){
      var index:number =this.checkIfExists(item,this.selected);
       if (index > -1) {
          this.selected.splice(index, 1);
      }
      if(!this.multipleSelections && this.selected.length == 0 ){
        this.showInput = true;
        this.selectedValue = "";
        this.selectedValueChanged.emit({
            value: this.selectedValue
        });
        this.updateValueLabel.emit({
          value:""
        });


      }
    }
    select(item:any){
        if(this.multipleSelections){
          var index:number =this.checkIfExists(item,this.selected);
           if (index > -1 && !this.allowDuplicates) {
              // this.keyword = "";
              // this.filtered.splice(0, this.filtered.length);
              this.focus=false;
              return;
          }
          else{
              this.selected.push(item);
              // this.keyword = "";
              // this.filtered.splice(0, this.filtered.length);
              this.addItem.emit({
                  value: item
              });
              this.focus=false;
          }
      }else{
        this.selected.splice(0, this.selected.length);
        this.selected.push(item);
        // this.filtered.splice(0, this.filtered.length);
        this.keyword = "";
        this.showInput = false;
        this.selectedValue = item.id;
        this.selectedValueChanged.emit({
            value: this.selectedValue
        });
        this.updateValueLabel.emit({
          value:this.showItem(item)
        });
        this.focus=false;

      }

    }
    private checkIfExists(item:any,list):number{

       if(item.concept && item.concept.id ){

        for (var _i = 0; _i < list.length; _i++) {
            let itemInList = list[_i];
            if(item.concept.id == itemInList.concept.id){
                 return _i;
            }
         }
      }else if(item.id){
        for (var _i = 0; _i < list.length; _i++) {
            let itemInList = list[_i];
             if(item.id == itemInList.id){
                 return _i;
            }
         }
      }
      return -1;

    }
    showItem(item:any):string{
      //console.log(item);
      if(!item){
        return "[No title available]"
      }
      else if (item.projectName || item.projectAcronym || item.code){ //project
          return ((item.projectAcronym)?"["+item.projectAcronym+"] ":"")+item.projectName+((item.code)?" ("+item.code+")":"");
       }else if (item.name){ //search
         return item.name;
      }else if( item.concept && item.concept.label){ //context
         return item.concept.label;
      }else if (item.label){ //simple
         return item.label;
      }

    }
    truncate(str:string, size:number):string{
      if(str == null){return "";}
      return (str.length > size)?str.substr(0,size)+'...':str;
    }
    private getSelectedNameFromGivenId(){
      this.showInput = true;
      if(this.selectedValue && this.selectedValue.length > 0 ) {
      this.sub = this._search.fetchByType(this.selectedValue,this.entityType, this.properties).subscribe(
        data => {
          this.selected.push( data[0]);
          this.updateValueLabel.emit({
            value:this.showItem(this.selected[0])
          });
          this.showInput = false;
         },
        err => {
          //console.log("An error occured"));
          this.handleError("Error getting results of type: "+this.entityType+" with id: "+this.selectedValue, err);
          this.selected.push( {label:this.selectedValue, id :this.selectedValue});
          this.updateValueLabel.emit({
            value:this.showItem(this.selected[0])
          });
          this.showInput = false;
        }
      );
    }
  }

    handleClick(event){
     var clickedComponent = event.target;
     var inside = false;
     do {
         if (clickedComponent === this.myElement.nativeElement) {
             inside = true;
         }
        clickedComponent = clickedComponent.parentNode;
     } while (clickedComponent);
      if(!inside){
        this.keyword = "";
        // this.numFilteredResults = 0;
        this.searchTermStream.next(this.keyword);
        this.focus=false;
      }
  }

  private handleError(message: string, error) {
      console.error("Autocomplete (component): "+message, error);
  }
}
