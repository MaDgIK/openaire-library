import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {RangeFilterComponent} from './rangeFilter.component';
import {InputModule} from "../../sharedComponents/input/input.module";
import {IconsModule} from "../icons/icons.module";

@NgModule({
  imports: [
    CommonModule, RouterModule, InputModule, ReactiveFormsModule, IconsModule
  ],
  declarations: [
    RangeFilterComponent
  ],
  providers:[
  ],
  exports: [
    RangeFilterComponent
  ]
})
export class RangeFilterModule { }
