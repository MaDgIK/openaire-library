import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {RangeFilter} from './rangeFilterHelperClasses.class';
import {Dates, StringUtils} from "../string-utils.class";
import {ActivatedRoute, Router} from "@angular/router";
import {properties} from "../../../../environments/environment";
import {UntypedFormBuilder, UntypedFormGroup} from "@angular/forms";
import {InputComponent, YearRange} from "../../sharedComponents/input/input.component";

@Component({
  selector: 'range-filter',
  templateUrl: 'rangeFilter.component.html'
})

export class RangeFilterComponent {

  @Input() filter:RangeFilter;
  @Input() isDisabled:boolean = false;
  @Input() showQuickButtons:boolean = true;
  public _maxCharacters:number =28;
  @Input() yearMin: number = Dates.yearMin;
  @Input() yearMax: number = Dates.yearMax;
  @Input() mandatoryRange:boolean = false;
  public currentYear: number = Dates.currentYear;
  public yearValidators;
  public formValidators = [StringUtils.fromYearAfterToYearValidator];
  public rangeForm: UntypedFormGroup;
  public yearRange: YearRange = {
    from: {control: 'yearFrom', placeholder: this.yearMin.toString()},
    to: {control: 'yearTo', placeholder: this.yearMax.toString()}
  }
  @Output() onFilterChange = new EventEmitter();
  @Input() actionRoute:boolean = false;
  @ViewChild('input') input: InputComponent;
  queryParams = {};
  onEnter: () => void = () => {
    if(!this.disabled) {
      this.yearChanged();
    }
  }
  
  constructor(private _router: Router, private route: ActivatedRoute, private _fb: UntypedFormBuilder) {}

  ngOnInit() {
    this.yearValidators = [StringUtils.inValidYearValidator(this.yearMin, this.yearMax)];
    this.yearRange = {
      from: {control: 'yearFrom', placeholder: this.yearMin.toString()},
      to: {control: 'yearTo', placeholder: this.yearMax.toString()}
    }
    if(this.mandatoryRange) {
      this.formValidators.push(StringUtils.rangeRequired(this.mandatoryRange));
    }
    this.rangeForm = this._fb.group({
      yearFrom: [this.filter.selectedFromValue, this.yearValidators],
      yearTo: [this.filter.selectedToValue, this.yearValidators]
    }, {validators: this.formValidators});
    this.route.queryParams.subscribe(params => {
      this.queryParams = Object.assign({}, params);
    });
  }

  public _formatTitle(title){
    return ((title.length > this._maxCharacters)?(title.substring(0,(this._maxCharacters - ('...').length))+"..."):title);
  }
  
  get disabled() {
   return this.rangeForm.invalid ||
     (!this.rangeForm.get('yearFrom').dirty && !this.rangeForm.get('yearTo').dirty) ||
     this.rangeForm.get('yearFrom').invalid || this.rangeForm.get('yearTo').invalid
  }

  yearChanged(yearsSelected: number = null){
    if(yearsSelected != null) {
      this.filter.selectedFromValue = (this.currentYear - yearsSelected) + "";
      this.filter.selectedToValue = this.currentYear + "";
      this.rangeForm.get('yearFrom').setValue(this.filter.selectedFromValue);
      this.rangeForm.get('yearTo').setValue(this.filter.selectedToValue);
    } else {
      this.filter.selectedFromValue = this.rangeForm.get('yearFrom').value;
      this.filter.selectedToValue = this.rangeForm.get('yearTo').value;
    }
    this.onFilterChange.emit({
      value: this.filter
    });
  }

  clearFilter() {
    this.filter.selectedFromValue = null;
    this.filter.selectedToValue = null;
    this.rangeForm.get('yearFrom').setValue(this.filter.selectedFromValue);
    this.rangeForm.get('yearTo').setValue(this.filter.selectedToValue);
    this.onFilterChange.emit({
      value: this.filter
    });
  }

  stringToNum(value: string): number {
    return +(value);
  }

  getRoute(){
    return properties.baseLink + this._router.url.split("?")[0];
  }
  
  getParams(year:number, selected:boolean){
    let params = Object.assign({}, this.queryParams);
    if(!selected){
      params['year'] = 'range'+(this.currentYear - year) + ':'+ this.currentYear;
    }else{
      delete params['year'];
    }

    delete params['page'];
    return params;
  }
}
