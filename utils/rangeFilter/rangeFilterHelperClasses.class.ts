import {Filter, Value} from "../../searchPages/searchUtils/searchHelperClasses.class";
import {SearchFields} from "../properties/searchFields";

export class RangeFilter{
  public title: string; // eg Publication Date Range
  public filterId: string; // type (name in url parameter)
  public originalFilterIdFrom: string; // (in index)
  public originalFilterIdTo: string; // (in index)
  public selectedFromValue: string;
  public selectedToValue: string;
  public selectedFromAndToValues: string = "";

  public static parse (fields:string[][], entityType:string,):RangeFilter[] {
    var searchFields:SearchFields = new SearchFields();
    var filters:RangeFilter[] = [];
    if(fields){
      for(let j=0; j<fields.length; j++) {

        var filter:RangeFilter = new RangeFilter();
        filter.title = searchFields.getFieldName(fields[j][0]+"-range-"+fields[j][1] ,entityType);
        filter.filterId = searchFields.getFieldParam(fields[j][0]+"-range-"+fields[j][1] ,entityType);
        filter.originalFilterIdFrom = fields[j][0];
        filter.originalFilterIdTo = fields[j][1];
        filters.push(filter);
      }
    }
    return filters;
  }
}

