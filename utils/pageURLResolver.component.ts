import {Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';


@Component({
  selector: 'page-URL-resolver',
  template: `    
     `
})

export class PageURLResolverComponent {
  parameters= {};
  constructor(private route: ActivatedRoute, private router : Router) {
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.parameters = Object.assign({}, params);
      this.navigateTO(this.router.url.split("?")[0])

    });
  }

  navigateTO(url:string){
    if( url == "/search/find/publications" ){
      this.parameters["type"]="publications";
      this.router.navigate(["/search/find/research-outcomes"],{queryParams:this.parameters})
    }else if( url == "/search/find/datasets" ){
      this.parameters["type"]="datasets";
      this.router.navigate(["/search/find/research-outcomes"],{queryParams:this.parameters})
    }else if( url == "/search/find/software" ){
      this.parameters["type"]="software";
      this.router.navigate(["/search/find/research-outcomes"],{queryParams:this.parameters})
    }else if( url == "/search/find/other" ){
      this.parameters["type"]="other";
      this.router.navigate(["/search/find/research-outcomes"],{queryParams:this.parameters})
    }else if( url == "/search/advanced/publications" ){
      this.parameters["type"]="publications";
      this.router.navigate(["/search/advanced/research-outcomes"],{queryParams:this.parameters})
    }else if( url == "/search/advanced/datasets" ){
      this.parameters["type"]="datasets";
      this.router.navigate(["/search/advanced/research-outcomes"],{queryParams:this.parameters})
    }else if( url == "/search/advanced/software" ){
      this.parameters["type"]="software";
      this.router.navigate(["/search/advanced/research-outcomes"],{queryParams:this.parameters})
    }else if( url == "/search/advanced/other" ){
      this.parameters["type"]="other";
      this.router.navigate(["/search/advanced/research-outcomes"],{queryParams:this.parameters})
    }
  }
}
