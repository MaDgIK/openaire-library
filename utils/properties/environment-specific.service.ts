import {Inject, Injectable, Optional} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Request} from 'express';
import {properties} from "../../../../environments/environment";
import {ConnectHelper} from "../../connect/connectHelper";
import {REQUEST} from '../tokens';

@Injectable({
  providedIn: 'root'
})
export class EnvironmentSpecificService {
  
  constructor(private http: HttpClient, @Optional() @Inject(REQUEST) private request: Request) {
    if(properties.adminToolsCommunity == "connect") {
      if(properties.environment == "development"){
        properties.domain = "https://beta."+(ConnectHelper.getCommunityFromDomain(properties.domain)?ConnectHelper.getCommunityFromDomain(properties.domain):"connect")+".openaire.eu";
      } else{
        properties.domain = "https://" + this.getDomain();
      }
    } else if(properties.adminToolsPortalType === 'aggregator') {
      if(properties.environment == "development"){
        properties.domain = "https://beta.canada.openaire.eu";
      } else {
        properties.domain = "https://" + this.getDomain();
      }
    }
  }
  
  private getDomain() {
    var domain = "";
    if (this.request) {
      domain = this.request.get('host').split(":")[0];
    } else {
      domain = document.location.hostname;
    }
    return domain;
  }
}
