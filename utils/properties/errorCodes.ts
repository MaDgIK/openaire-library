export class ErrorCodes {
  public LOADING = 0;
  public DONE = 1;
  public NONE = 2;
  public ERROR = 3;
  public NOT_AVAILABLE = 4;
  public OUT_OF_BOUND = 5;
  public NOT_FOUND = 6;
  public NOT_SAVED = 7;
  public FORBIDDEN = 8;
}
