export type Environment = "development" | "test" | "beta" | "production";
export type Dashboard = "explore" | "connect" | "monitor" | "aggregator" | "eosc" | "developers" | "faircore4eosc" | "irish";
export type PortalType = "explore" | "connect" | "community" | "monitor" | "funder" | "ri" | "project" | "organization" |
    "aggregator" | "eosc" | "faircore4eosc" | "country" | "datasource" | "journal" | "publisher";

export interface EnvProperties {
  environment?: Environment;
  adminToolsPortalType?: PortalType;
  dashboard?: Dashboard;
  isDashboard?: boolean;
  plan?:string;
  domain?: string;
  enablePiwikTrack?: boolean;
  useCache?: boolean;
  useLongCache?: boolean;
  showAddThis?: boolean;
  showContent?: boolean;
  enableEoscDataTransfer?:boolean;
  pdfStatisticsAPIURL?: string;
  statisticsFrameAPIURL?: string; //delete?
  statisticsFrameNewAPIURL?: string;
	bipFrameAPIURL?: string;
  useNewStatistisTool?: boolean; //delete?
  useOldStatisticsSchema?: boolean;
  monitorStatsFrameUrl?:string;
  disableFrameLoad?:boolean
  statisticsAPIURL?: string;
  impactFactorsAPIURL?: string; //???
  enermapsURL?: string;
  claimsAPIURL?: string;
  searchAPIURLLAst?: string;
  searchResourcesAPIURL?: string;
  openCitationsAPIURL?: string;
  csvAPIURL?: string;
  searchCrossrefAPIURL?: string;
  searchDataciteAPIURL?: string;
  searchOrcidURL?: string;
  orcidURL?: string;
  orcidAPIURL?: string;
  orcidTokenURL?: string;
  orcidClientId?: string;
  doiURL?: string;
  pmcURL?: string;
  pmidURL?: string;
  handleURL?: string;
  erasmusURL?: string;
  cordisURL?: string;
  openDoarURL?: string;
  r3DataURL?: string;
  swhURL?: string;
  rorURL?: string;
  isniURL?: string;
  wikiDataURL?: string;
  fundRefURL?: string;
  rridURL?: string;
  arxivURL?: string;
  fairSharingURL?: string,
  openScienceCloudURL?: string,
  eoscMarketplaceURL?: string,
  sherpaURL?: string;
  sherpaURLSuffix?: string;
  zenodo?: string;
  zenodoCommunities?: string;
  helpdesk?: string;
  helpdeskEmail?: string;
  utilsService?: string;
  vocabulariesAPI?: string;
  piwikBaseUrl?: string;
  piwikSiteId?: string;
  loginServiceURL?: string | string[];
  registryUrl?: string;
  developersApiUrl?: string,
  cookieDomain?: string;
  feedbackmail?: string;
  feedbackmailForMissingEntities?: string;
  cacheUrl?: string;
  deleteCacheUrl?: string;
  deleteBrowserCacheUrl?: string;
  monitorServiceAPIURL?: string;
  adminToolsAPIURL?: string;
  adminToolsCommunity?: string;
  useHelpTexts?:boolean;
  contextsAPI?: string;
  communityAPI?: string;
  communitiesAPI?: string;
  datasourcesAPI?: string;
  csvLimit?: number;
  pagingLimit?: number;
  resultsPerPage?: number;
  baseLink?: string;
  baseOpenaireLink?: string;
  afterLoginRedirectLink?: string;
  myClaimsLink?: string;
  searchLinkToResult?: string;
  searchLinkToPublication?: string;
  searchLinkToProject?: string;
  searchLinkToDataProvider?: string;
  searchLinkToService?: string;
  searchLinkToDataset?: string;
  searchLinkToSoftwareLanding?: string;
  searchLinkToOrp?: string;
  searchLinkToOrganization?: string;
  //Search pages
  searchLinkToAll?: string;
  searchLinkToCommunities?: string;
  searchLinkToStakeholders?: string;
  searchLinkToPublications?: string;
  searchLinkToDataProviders?: string;
  searchLinkToServices?: string;
  searchLinkToProjects?: string;
  searchLinkToDatasets?: string;
  searchLinkToSoftware?: string;
  searchLinkToOrps?: string;
  searchLinkToOrganizations?: string;
  searchLinkToCompatibleDataProviders?: string;
  searchLinkToEntityRegistriesDataProviders?: string;
  searchLinkToJournals?: string;
  searchLinkToResults?: string;
  //Advanced Search for pages
  searchLinkToAdvancedPublications?: string;
  searchLinkToAdvancedProjects?: string;
  searchLinkToAdvancedDatasets?: string;
  searchLinkToAdvancedSoftware?: string;
  searchLinkToAdvancedOrps?: string;
  searchLinkToAdvancedDataProviders?: string;
  searchLinkToAdvancedServices?: string;
  searchLinkToAdvancedOrganizations?: string;
  searchLinkToAdvancedResults?: string;
  errorLink?: string
  lastIndexInformationLink?: string;
  showLastIndexInformationLink?: boolean;
  widgetLink?: string;
  claimsInformationLink?: string;
  depositLearnHowPage?: string;
  depositSearchPage?: string;
  shareInZenodoPage?: string;
  // recaptcha site key
  reCaptchaSiteKey?: string;
  // contact us recipients
  admins?: string[];
  lastIndexUpdate?: string; //delete?
  indexInfoAPI?: string;
  altMetricsAPIURL?: string;
  b2noteAPIURL?: string; //delete?
  adminPortalURL?: string;
  sushiliteURL?: string;
  notificationsAPIURL?: string;
  myOrcidLinksPage?: string;
  footerGrantText?: string;
  egiNotebookLink?: string;
  connectPortalUrl?;
  eoscDataTransferAPI?;
  eoscDataTransferLoginUrl?;
  eoscDataTransferDestinations?;
  hasMachineCache?: boolean;

//  irish
  logFilesPath?:string;
  matomoLogFilesPath?:string;
  logServiceUrl?:string;
  zenodoDumpUrl?:string;
  openOrgsUrl?:string;
  orcidDiscoverLinksPage?:string;

  // RDGraph
  naturalLanguageSearchAPI?:string;
  availableCommunitiesAPI?:string;
  recommendationsForCommunityAPI?:string;
  recommendationsForOrcidAPI?:string;
  recommendationsForPublicationAPI?:string;
  feedbackForRecommendationAPI?:string;
  showRecommendations?: boolean;

  //connect dashboards
  forceCacheReload?:boolean;
  joomlaURL?: string;
  pidResolverParserAPI?:string;
}

export function checkPropertyValues(properties:EnvProperties){
  let keys = Object.keys(properties);
  let Warn = 0;
  // console.debug( "Environment: " +properties.environment  )
  for(let attribute of keys){

      // console.debug(attribute + ": " +properties[attribute] + " " )
      if (properties.environment == "production" &&  typeof properties[attribute] == 'string' &&  (properties[attribute].indexOf("beta.") != -1 || properties[attribute].indexOf("uoa.di.gr") != -1)) {
        console.warn("shouldn't be prod? " + attribute + ": " + properties[attribute]);
        Warn++;
      }
      if (properties.environment != "production" && typeof properties[attribute] == 'string' && properties[attribute].indexOf("https://services.") != -1) {
        console.warn("should be prod? " + attribute + ": " + properties[attribute]);
        Warn++;
      }
    if (properties.environment != "development" && typeof properties[attribute] == 'string' && properties[attribute].indexOf("uoa.di.gr") != -1) {
      console.warn("should be in dev? " + attribute + ": " + properties[attribute]);
      Warn++;
    }

  }
  /*if(Warn > 0){
    alert("check properties: " + Warn)
  }*/
}
