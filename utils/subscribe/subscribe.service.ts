import {BehaviorSubject, Observable} from "rxjs";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class SubscribeService {
  
  private loading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private members: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  
  public setLoading(loading: boolean) {
    this.loading.next(loading);
  }
  
  public setMembers(members: number) {
    this.members.next(members);
  }
  
  public getLoading(): Observable<any> {
    return this.loading.asObservable();
  }
  
  public getMembers(): Observable<any> {
    return this.members.asObservable();
  }
  
}
