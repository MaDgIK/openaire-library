import {Component, Input} from '@angular/core';
import {ErrorCodes} from './properties/errorCodes';
import {properties} from "../../../environments/environment";

@Component({
    selector: 'errorMessages',
    template: `
      <div *ngIf="status.some(checkErrorCode(errorCodes.LOADING))"
            [class]="(tab_error_class ? '' : 'uk-animation-fade') + ' uk-margin-top  uk-width-1-1'" role="alert">
        <loading></loading>
      </div>
<!--      <div *ngIf="status.every(checkErrorCode(errorCodes.NONE))"-->
<!--            [class]="(tab_error_class ? 'uk-margin-top' : 'uk-animation-fade') + ' uk-alert uk-alert-primary'" role="alert">No {{type}} available</div>-->
      <div *ngIf="status.every(checkErrorCode(errorCodes.NONE))"
           class="uk-width-expand uk-height-small uk-flex uk-flex-center uk-flex-middle">
        <div class="uk-animation-fade uk-text-meta uk-text-large">
          No {{type}} available
        </div>
      </div>
      
      <div *ngIf="status.every(checkErrorCode(errorCodes.ERROR)) ||
                  (status.some(checkErrorCode(errorCodes.ERROR)) && (!status.every(checkErrorCode(errorCodes.DONE)) || !status.every(checkErrorCode(errorCodes.LOADING))))"
            [class]="(tab_error_class ? 'uk-margin-top' : 'uk-animation-fade') + ' uk-alert uk-alert-warning'" role="alert">
        An Error Occurred
      </div>
      <div *ngIf="status.every(checkErrorCode(errorCodes.NOT_AVAILABLE)) ||
                  (status.some(checkErrorCode(errorCodes.NOT_AVAILABLE)) && (!status.every(checkErrorCode(errorCodes.DONE)) || !status.every(checkErrorCode(errorCodes.LOADING))))"
            [class]="(tab_error_class ? 'uk-margin-top' : 'uk-animation-fade') + ' uk-alert uk-alert-warning'" role="alert">
        Service temporarily unavailable. Please try again later.
      </div>
      <div *ngIf="status.every(checkErrorCode(errorCodes.NOT_FOUND)) ||
                  (status.some(checkErrorCode(errorCodes.NOT_FOUND)) && (!status.every(checkErrorCode(errorCodes.DONE)) || !status.every(checkErrorCode(errorCodes.LOADING))))"
            [class]="(tab_error_class ? 'uk-margin-top' : 'uk-animation-fade') + ' uk-alert uk-alert-warning'" role="alert">
        No {{type}} found
      </div>
      <div *ngIf="status.every(checkErrorCode(errorCodes.OUT_OF_BOUND))"
            [class]="(tab_error_class ? 'uk-margin-top' : 'uk-animation-fade') + ' uk-alert uk-alert-warning'" role="alert">Requested page out of bounds
      </div>
      <div *ngIf="status.some(checkErrorCode(errorCodes.NOT_SAVED))"
            [class]="(tab_error_class ? 'uk-margin-top' : 'uk-animation-fade') + ' uk-alert uk-alert-warning'" role="alert">Changes could not be saved
      </div>
      <div *ngIf="status.some(checkErrorCode(errorCodes.FORBIDDEN))"
            [class]="(tab_error_class ? 'uk-margin-top' : 'uk-animation-fade') + ' uk-alert uk-alert-danger'" role="alert">You are not allowed to access this page
      </div>
    `
  })

  export class ErrorMessagesComponent {
      @Input() status: Array<number>;
      @Input() type: string;
      @Input() tab_error_class: boolean = false;

      public errorCodes:ErrorCodes;

      constructor () {
        this.errorCodes = new ErrorCodes();
      }

      ngOnInit() {
          if(!this.status) {
            this.status = [this.errorCodes.LOADING];
          }
      }

      ngOnDestroy() {}

      public checkErrorCode(code: number) {
        return function(status: number) {
          return (status == code);
        }
      }

      public getErrorCode(status: any) {
        if(status == '401' || status == 401) {
          return this.errorCodes.FORBIDDEN;
        } else if(status == "403" || status == 403) {
          return this.errorCodes.FORBIDDEN;
        } else if(status == "204" || status == 204) {
          return this.errorCodes.NONE;
        } else if(status == '404' || status == 404) {
          return this.errorCodes.NOT_FOUND;
        } else if(status == '500'|| status == 500) {
          return this.errorCodes.ERROR;
        } else {
          return this.errorCodes.NOT_AVAILABLE;
        }
      }
  }
