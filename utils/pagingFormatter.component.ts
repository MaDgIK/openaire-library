import {Component, Input} from '@angular/core';
import {RouterHelper} from './routerHelper.class';
import {EnvProperties} from './properties/env-properties';
import {properties} from "../../../environments/environment";

@Component({
  selector: 'paging',
  template: `
    <ul *ngIf="currentPage > 0 && currentPage <= getTotalPages()"
        [class.uk-disabled]="isDisabled" [class.uk-invisible]="getTotalPages() < 2"  class="uk-pagination uk-margin-remove" [ngClass]="'uk-flex-' + position">
      <li [class.uk-invisible]="currentPage <= 1">
         <a [queryParams]="routerHelper.createQueryParamsPaging(parameterNames,parameterValues,'page', (currentPage - 1))"
            [routerLink]=baseUrl aria-label="Previous">
          <icon name="chevron_left" ratio="1.2" [flex]="true" customClass="uk-pagination-previous"></icon>
         </a>
      </li>
      <li *ngIf=" currentPage -2 > 0"><a
          [queryParams]="routerHelper.createQueryParamsPaging(parameterNames,parameterValues,'page',(currentPage - 2))"
          [routerLink]=baseUrl>{{(currentPage - 2) | number}}</a></li>
      <li *ngIf=" currentPage -1 > 0 "><a
          [queryParams]="routerHelper.createQueryParamsPaging(parameterNames,parameterValues,'page',(currentPage - 1))"
          [routerLink]=baseUrl>{{(currentPage - 1) | number}}</a></li>
      <li class="uk-active"><span>{{currentPage | number}}</span></li>
      <li *ngIf=" currentPage +1 <= getTotalPages() "><a
          [queryParams]="routerHelper.createQueryParamsPaging(parameterNames,parameterValues,'page',(currentPage +1))"
          [routerLink]=baseUrl>{{(currentPage + 1) | number}}</a></li>
      <li *ngIf=" currentPage +2 <= getTotalPages() "><a
          [queryParams]="routerHelper.createQueryParamsPaging(parameterNames,parameterValues,'page',(currentPage +2))"
          [routerLink]=baseUrl>{{(currentPage + 2) | number}}</a></li>
      <li *ngIf=" (currentPage -2 <= 0)&&(currentPage +3 <= getTotalPages()) " class="uk-visible@l"><a
          [queryParams]="routerHelper.createQueryParamsPaging(parameterNames,parameterValues,'page',(currentPage +3))"
          [routerLink]=baseUrl>{{(currentPage + 3) | number}}</a></li>
      <li *ngIf=" (currentPage -1 <= 0)&&(currentPage +4 <= getTotalPages()) " class="uk-visible@l"><a
          [queryParams]="routerHelper.createQueryParamsPaging(parameterNames,parameterValues,'page',(currentPage +4))"
          [routerLink]=baseUrl>{{(currentPage + 4) | number}}</a></li>
      <li [class.uk-invisible]="getTotalPages() <= currentPage">
        <a [queryParams]="routerHelper.createQueryParamsPaging(parameterNames,parameterValues,'page',(currentPage + 1))"
          [routerLink]=baseUrl aria-label="Next">
          <icon name="chevron_right" ratio="1.2" [flex]="true" customClass="uk-pagination-next"></icon>
        </a>
      </li>
    </ul>
  `
})

export class PagingFormatter {
  @Input() isDisabled: boolean = false;
  @Input() currentPage: number = 1;
  @Input() size: number = 10;
  @Input() totalResults: number = 10;
  @Input() baseUrl: string = "";
  @Input() parameterNames: string[];
  @Input() parameterValues: string[];
  @Input() position: "left" | "center" | "right" = "right";
  private readonly limit: number;
  properties: EnvProperties = properties;
  public routerHelper: RouterHelper = new RouterHelper();
  
  constructor() {
    this.limit = this.properties.pagingLimit;
  }
  
  getTotalPages() {
    let total: number;
    var i: number = parseInt('' + (this.totalResults / this.size));
    total = (((this.totalResults / this.size) == i) ? i : (i + 1));
    if ((this.currentPage == this.limit) && (total > this.limit)) {
      total = this.limit;
    } else if ((this.currentPage > this.limit) && (total > this.limit)) {
      total = this.currentPage;
    }
    return total;
  }
}
