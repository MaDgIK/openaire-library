import { Page } from "./page";
import { Portal } from "./portal";

export interface HtmlPageContent {
    _id: string;
    page: Page | string;
    community: Portal | string;
    content: string;
}

export interface CheckHtmlPageContent {
    pageHelpContent: HtmlPageContent;
    checked: boolean;
}
