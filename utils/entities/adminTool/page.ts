import { Entity } from './entity';

export interface Page {
    _id: string;
    route: string;
    name: string;
    type: string;
    isEnabled: boolean;
    portalType: string;
		portalPid: string;
    entities: Entity[] | string[];
    top: boolean;
    bottom: boolean;
    left: boolean;
    right: boolean;
}

export interface CheckPage {
    page: Page;
    checked: boolean;
}
