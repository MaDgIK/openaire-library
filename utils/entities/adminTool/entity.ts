export class Entity {
    _id: string;
    pid: string;
    name: string;
    isEnabled: boolean;
}

export interface CheckEntity {
    entity: Entity;
    checked: boolean;
}
