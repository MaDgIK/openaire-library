

export class PluginTemplate{
  _id: string;
  name: string;
  code: string;
  description: string;
  image: string;
  page: string;
  custom:boolean = false;
  plan: "starter" | "extended";
  portalSpecific:string[];
  defaultIsActive:boolean = false;
  order:number;
  placement: string;
  portalType: string;
  settings: {};
  object:any;
}
