import {Page} from './page';

export interface DivId {
    _id: string;
    name: string;
    pages: string[] | Page[];
    portalType: string;
}

export interface CheckDivId {
   divId: DivId;
   checked: boolean;
}
