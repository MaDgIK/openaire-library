import {PluginTemplate} from "./pluginTemplate";

export class Plugin {
  _id: string;
  templateCode: string;
  templateId: string;
  page: string;
  pid:string;
  placement: string;
  order: number;
  active:boolean;
  custom:boolean = false;
  object:any;
  settingsValues:Map<string,string> = new Map<string, string>();

  constructor(page, pid, template:PluginTemplate) {
    this.page = page;
    this.pid = pid;
    this.templateCode = template.code;
    this.templateId = template._id;
    this.placement = template.placement;
    this.active = template.defaultIsActive;
    this.object = template.object && Object.keys(template.object).length > 0 ? Object.assign(template.object) : null;
    this.order = template.order;
    if (template.settings) {
      for (let attr of Object.keys(template.settings)) {
        this.settingsValues.set(attr, template.settings[attr].value)
      }
    }
  }
}
