/**
 * Created by stefania on 7/13/17.
 */
import { Page } from './page';
import { Portal } from './portal';

export interface PageHelpContent {
    _id: string;
    page: Page | string;
    portal: Portal | string;
    placement: string;
    order: number;
    content: string;
    isActive: boolean;
    isPriorTo: boolean;
}

export interface CheckPageHelpContent {
    pageHelpContent: PageHelpContent;
    checked: boolean;
}

export interface PageHelpContentFilterOptions {
    id: string;
    active: Boolean;
    text: RegExp;
}
