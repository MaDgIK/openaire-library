export class Curator {

  _id: string;
  email: string;
  name: string;
  affiliations: Affiliation[];
  photo: string;
  bio: string;
  visible: boolean = true;
}

export class Affiliation {
  communityId: string;
  id: string;
  name = '';
  logo_url = '';
  website_url = '';
}
