import {properties} from "../../../../environments/environment";
import {BelongsTo, Measure} from "./resultLandingInfo";

export class DataproviderProvenance {
  provenance: Map<string, { "urlPrefix": string, "name": string }>;

  constructor() {
    this.provenance = new Map();
    this.provenance.set("opendoar____::", {"urlPrefix": properties.openDoarURL, "name": "OpenDOAR"});//, "idRegexPrefix": ""});
    this.provenance.set("re3data_____::", {"urlPrefix": properties.r3DataURL, "name": "re3data.org"});
    this.provenance.set("fairsharing_::", {"urlPrefix": properties.fairSharingURL, "name": "FAIRsharing"});
    this.provenance.set("eosc________::", {
      "urlPrefix": properties.openScienceCloudURL,
      "name": "EOSC Resource Hub"    });
  }
}

export class DataProviderInfo {
  relcanId;
  objIdentifier: string;
  record;
  title: { "name": string, "url": string };
  officialName: string;
  type: string;
  registry: boolean;
  compatibility: { "info": string, "name": string, "id": string };
  oaiPmhURL: string;
  openDoarId: string;
  r3DataId: string;
  provenance: Map<string, { "url": string[] }>;
  originalId: string;
  countries: string[];
  journal: { "journal": "", "issn": string, "lissn": string, "eissn": string };
  description: string;
  subjects: string[];
  jurisdiction: string;
  thematic: boolean;
  contentpolicy: string;
  identifiers: Map<string, string[]>;     //key is the classname

  fundedContent: string; // search query
  measure: Measure;

  //collected from datasource api
  // aggregationStatus: {"fundedContent": string, "indexRecords": string, "fulltexts": string, "lastUpdateDate": string};
  aggregationStatus: { "fulltexts": string };

  /** @deprecated*/
  tabs: { "name": string, "content": string }[];
  /** @deprecated*/
  tabs2: string[] = [];
  /** @deprecated*/
  tabsInTypes = {
    "publicationsTab": new Set<string>(
        ["aggregator::datarepository",
          "aggregator::pubsrepository::institutional",
          "aggregator::pubsrepository::unknown",
          "aggregator::pubsrepository::journals",
          "crissystem",
          "datarepository::unknown",
          "infospace",
          "pubsrepository::institutional",
          "pubsrepository::journal",
          "pubsrepository::unknown",
          "scholarcomminfra",
          "pubsrepository::thematic",
          "pubscatalogue::unknown",
          "orprepository"
        ]),
    "datasetsTab": new Set<string>(
        ["aggregator::datarepository",
          "aggregator::pubsrepository::institutional",
          "aggregator::pubsrepository::unknown",
          "aggregator::pubsrepository::journals",
          "crissystem",
          "datarepository::unknown",
          "infospace",
          "pubsrepository::institutional",
          "pubsrepository::journal",
          "pubsrepository::unknown",
          "scholarcomminfra",
          "pubsrepository::thematic",
          "pubscatalogue::unknown",
          "orprepository"
        ]),
    "statisticsTab": new Set<string>(
        ["aggregator::datarepository",
          "aggregator::pubsrepository::institutional",
          "aggregator::pubsrepository::unknown",
          "aggregator::pubsrepository::journals",
          "crissystem",
          "datarepository::unknown",
          "pubsrepository::institutional",
          "pubsrepository::journal",
          "pubsrepository::unknown",
          "pubsrepository::thematic",
          "pubscatalogue::unknown",
          "orprepository"
        ]),
    // "organizationsTab": new Set<string>(
    //                 [   "entityregistry::projects",
    //                     "entityregistry::repositories"
    //                 ]),
    "projectsTab": new Set<string>(["entityregistry::projects"]),
    "datasourcesTab": new Set<string>(["entityregistry::repositories"]),
    "relatedDatasourcesTab": new Set<string>(
        ["aggregator::pubsrepository::unknown",
          "aggregator::pubsrepository::journals",
          "aggregator::pubsrepository::institutional",
          "aggregator::datarepository"
        ]),
    "softwareTab": new Set<string>(
        [
          "aggregator::datarepository",
          "aggregator::pubsrepository::institutional",
          "aggregator::pubsrepository::unknown",
          "aggregator::pubsrepository::journals",
          "crissystem",
          "datarepository::unknown",
          "infospace",
          "pubsrepository::institutional",
          "pubsrepository::journal",
          "pubsrepository::unknown",
          "scholarcomminfra",
          "pubsrepository::thematic",
          "pubscatalogue::unknown",
          "softwarerepository",
          "aggregator::softwarerepository",
          "orprepository"
        ]),
    "orpsTab": new Set<string>(
        [
          "aggregator::datarepository",
          "aggregator::pubsrepository::institutional",
          "aggregator::pubsrepository::unknown",
          "aggregator::pubsrepository::journals",
          "crissystem",
          "datarepository::unknown",
          "infospace",
          "pubsrepository::institutional",
          "pubsrepository::journal",
          "pubsrepository::unknown",
          "scholarcomminfra",
          "pubsrepository::thematic",
          "pubscatalogue::unknown",
          "softwarerepository",
          "aggregator::softwarerepository",
          "orprepository"
        ]),
    "metricsTab": new Set<string>(
        ["aggregator::datarepository",
          "aggregator::pubsrepository::institutional",
          "aggregator::pubsrepository::unknown",
          "aggregator::pubsrepository::journals",
          "crissystem",
          "datarepository::unknown",
          "infospace",
          "pubsrepository::institutional",
          "pubsrepository::journal",
          "websource",
          "pubsrepository::unknown",
          "scholarcomminfra",
          "pubsrepository::thematic",
          "pubscatalogue::unknown",
          "softwarerepository",
          "aggregator::softwarerepository",
          "orprepository"
        ])

  };

  resultsBy: string;
  resultTypes = {
    "collectedFrom": new Set<string>(
        ["aggregator::datarepository",
          "aggregator::pubsrepository::institutional",
          "aggregator::pubsrepository::unknown",
          "aggregator::pubsrepository::journals",
          "entityregistry::projects",
          "entityregistry::repositories",
          "infospace",
          "scholarcomminfra",
          "pubscatalogue::unknown",
          "aggregator::softwarerepository"
        ]),
    "hostedBy": new Set<string>(
        ["crissystem",
          "datarepository::unknown",
          "pubsrepository::institutional",
          "pubsrepository::journal",
          "pubsrepository::unknown",
          "pubsrepository::thematic",
          "softwarerepository",
          "orprepository"
        ])
  };

  organizations: { "acronym": string, "name": string, "id": string }[] = [];
  //publications: any;
  //datasets: any;
  /** @deprecated*/
  statistics: any;
  //projects: any;
  /** @deprecated*/
  datasources: any;

  //relatedDatasources: Map<string, {"name": string, "countPublications": string, "countDatasets": string, "countSoftware": string, "countOrps": string}>;
  // Search query
  relatedDatasources: { "id": string, "name": string, "count": number }[] = [];

  belongsTo: boolean = true;
  message: string;

  setBelongsTo(id: string) {
    this.belongsTo = !BelongsTo.datasource || BelongsTo.datasource.fields.findIndex(field => BelongsTo.check(this, field)) != -1;
    this.message = !this.belongsTo ? BelongsTo.datasource.message : null;
    if (this.message) {
      this.message = this.message.replace('((id))', id);
    }
  }
}
