import {BelongsTo} from "./resultLandingInfo";

export interface OrganizationProject {
  name: string;
  id: string;
  code: string;
  acronym: string;
  funder: string;
  funderId: string;
  fundingStream: string;
  fundingLevel1: string;
  fundingLevel2: string;
  // sc39: string;
  startDate: string;
  endDate: string;
}

export class OrganizationInfo {
  relcanId;
  title: { name: string; url: string; };
  name: string;
  country: string;
  objIdentifier: string;

  /** @deprecated*/ //deprecated - search query
  projects: Map<string, OrganizationProject[]>;
  //dataProviders: { name: string; url: string; type: string; websiteUrl: string;
  //                    organizations: {name: string; url: string}[]}[];
  
  deletedByInferenceIds: string[];
  children;
  identifiers: Map<string, string[]>;     //key is the classname
  belongsTo: boolean = true;
  message: string;

  setBelongsTo(id: string) {
    this.belongsTo = !BelongsTo.organization || BelongsTo.organization.fields.findIndex(field => BelongsTo.check(this, field)) != -1;
    this.message = !this.belongsTo ? BelongsTo.organization.message : null;
    if (this.message) {
      this.message = this.message.replace('((id))', id);
    }
  }
}
