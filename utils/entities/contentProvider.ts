export class ContentProvider {
  id: string;
  openaireId: string;
  communityId: string;
  name: string;
  officialname: string;
  selectioncriteria: SelectionCriteria;
  deposit: boolean;
  enabled:boolean;
  message: string;
}

export class SelectionCriteria {
  criteria: Criteria[] = [];
}

export class Criteria {
  constraint: Constraint[] = [];
}

export class Constraint {
  verb: string = 'contains';
  field: string = null;
  value: string;
  
  constructor(verb: string, field: string, value:string) {
    this.verb = verb;
    this.field = field;
    this.value = value;
  }
}
