export class Publication {
  title: string;
  publisher: string;
  DOI: string;
  source: string;
  type: string;
}
