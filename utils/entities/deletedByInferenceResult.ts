/*title, authors, abstract, List of projects, PIDs,
  collectedfrom (link pointing to the download url), access rights*/

export class DeletedByInferenceResult {
    title: string;
    accessMode: string;
    authors: {"fullName": string, "orcid": string}[];

    date: string;
    dateofacceptance: string;
    embargoEndDate: string;
    types: string[];

    hostedBy_collectedFrom: {"downloadName": string, "downloadUrl": string[], "collectedName": string, "collectedId": string, "accessMode": string[], "bestAccessMode": string, "type": string, "year":string}[];
    //collectedFrom: {"downloadName": string, "downloadUrl": string[]};
    identifiers: Map<string, string[]>;     //key is the classname
    //publisher: string;
    //journal: {"journal": string, "issn": string, "lissn": string, "eissn": string, "issue": string, "volume": string, "start_page": string, "end_page": string};

    languages: string[];
    countries: string[];

    description: string;
    projectsProvenanceVocabulary: { "sysimport:crosswalk:repository": string,
                                    "sysimport:crosswalk:entityregistry": string,
                                    "sysimport:crosswalk:datasetarchive": string,
                                    "iis": string,
                                    "sysimport:mining:repository": string,
                                    "user:claim:pid": string,
                                    "user:claim:search": string,
                                    "user:claim:datacite": string
                                  } = {
                                    "sysimport:crosswalk:repository": "Repository",
                                    "sysimport:crosswalk:entityregistry": "Repository",
                                    "sysimport:crosswalk:datasetarchive": "Repository",
                                    "iis": "OpenAIRE",
                                    "sysimport:mining:repository": "OpenAIRE",
                                    "user:claim:pid": "User",
                                    "user:claim:search": "User",
                                    "user:claim:datacite": "User"
                                  };
    fundedByProjects: { "id": string, "acronym": string, "title": string,
                        "funderShortname": string, "funderName": string,
                        "funding": string, "code": string, "provenanceAction": string,
                        "inline": boolean}[];

}
