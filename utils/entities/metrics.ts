export class Metrics {
    totalDownloads: number;
    totalViews: number;
    totalOpenaireViews: number;
    totalOpenaireDownloads: number;
    pageViews: number;
    infos: Map<string, {"name": string, "url": string, "numOfDownloads": string, "numOfViews": string, "openaireDownloads": string, "openaireViews": string}>;
}
