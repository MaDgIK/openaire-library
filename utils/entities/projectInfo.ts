import {BelongsTo, Measure} from "./resultLandingInfo";

export interface Funding {
  funderName: string,
  funderShortName: string,
  funderJurisdiction: string,
  code: string,
  callIdentifier: string,
  fundingStream: string,
  budget: string,
  contribution: string,
  currency: string
}

export interface ProjectOrganization {
  acronym: string,
  country: string,
  name: string,
  id: string
}

export class ProjectInfo {
  id: string;
  acronym: string;
  title: string;
  funding: Funding;
  startDate: number;
  endDate: number;
  currentDate: number;
  status: string;
  openAccessMandatePublications: string;
  openAccessMandateDatasets: string;
  // specialClause39: string;
  organizations: ProjectOrganization[];
  url: string;
  urlInfo: string;
  description: string;
  //publications: any;
  researchData: any;
  totalPublications: number;
  totalDatasets: number;
  publicationsStatus: any;
  measure: Measure;
  belongsTo: boolean = true;
  message: string;

  setBelongsTo(id: string) {
    this.belongsTo = !BelongsTo.project || BelongsTo.project.fields.findIndex(field => BelongsTo.check(this, field)) != -1;
    this.message = !this.belongsTo ? BelongsTo.project.message : null;
    if (this.message) {
      this.message = this.message.replace('((id))', id);
    }
  }
}
