import {Email} from "../email/email";

export class EmailRecaptcha {
  email: Email = null;
  recaptcha: string = null;
}
