import { NgModule }            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';

import {AltMetricsComponent} from './altmetrics.component';
import {SafeHtmlPipeModule} from './pipes/safeHTMLPipe.module';


@NgModule({
  imports: [
    CommonModule, FormsModule, SafeHtmlPipeModule
  ],
  declarations: [
     AltMetricsComponent
  ],
  exports: [
      AltMetricsComponent
    ]
})
export class AltMetricsModule { }
