import {SearchResearchResultsService} from '../../services/searchResearchResults.service';
import {ErrorCodes} from '../../utils/properties/errorCodes';
import {ErrorMessagesComponent} from '../../utils/errorMessages.component';
import {OpenaireEntities, SearchFields} from '../../utils/properties/searchFields';
import {SearchCustomFilter, SearchUtilsClass} from '../../searchPages/searchUtils/searchUtils.class';
import {DOI, StringUtils} from '../../utils/string-utils.class';
import {Subject, Subscriber} from 'rxjs';
import {EnvProperties} from '../../utils/properties/env-properties';

export class FetchResearchResults {
  private errorCodes: ErrorCodes;
  private errorMessages: ErrorMessagesComponent;

  public results =[];
  public allResults =[]; // in case of paging

  public requestComplete: Subject<void>;

  public searchUtils:SearchUtilsClass = new SearchUtilsClass();
  subscriptions = [];
  public searchFields:SearchFields = new SearchFields();

  public CSV: any = {
    "columnNames":  [
      "Title", "Authors", "Publication Year", "DOI",
      "Funder", "Project Name (GA Number)", "Access"
    ],
    "export":[]
  };
  public CSVDownloaded = false;
  public csvParams: string;

  constructor ( private _searchResearchResultsService: SearchResearchResultsService ) {
    this.errorCodes = new ErrorCodes();
    this.errorMessages = new ErrorMessagesComponent();
    this.searchUtils.status = this.errorCodes.LOADING;

    this.requestComplete = new Subject<void>();
  }

  public clearSubscriptions() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscriber) {
        subscription.unsubscribe();
      }
    });
  }

  public getResultsForCommunity(resultType:string, communityId: string, page: number, size: number, properties:EnvProperties, contextId = null) {
    this.searchUtils.status = this.errorCodes.LOADING;
    this.subscriptions.push(this._searchResearchResultsService.search(resultType, "", "&fq=communityid=" + communityId + (contextId?'&fq=categoryid=' + encodeURIComponent(contextId):''), page, size, "resultdateofacceptance,descending", [], properties).subscribe(
      data => {
        this.searchUtils.totalResults = data[0];
        this.results = data[1];

        this.searchUtils.status = this.errorCodes.DONE;
        if(this.searchUtils.totalResults == 0 ){
          this.searchUtils.status = this.errorCodes.NONE;
        }
      },
      err => {
        this.handleError("Error getting "+this.getEntityQueryName(resultType,true)+" for community: "+communityId, err);
        this.searchUtils.status = this.errorMessages.getErrorCode(err.status);
      }
    ));
  }

  public getNumForCommunity(resultType:string, communityId: string, properties:EnvProperties, contextId = null) {
    this.searchUtils.status = this.errorCodes.LOADING;

    this.subscriptions.push(this._searchResearchResultsService.countTotalResults(resultType, properties, "&fq=communityid=" + communityId +(contextId?'&fq=categoryid=' + encodeURIComponent(contextId):'')).subscribe(
      data => {
        this.searchUtils.totalResults = data;

        this.searchUtils.status = this.errorCodes.DONE;
        if(this.searchUtils.totalResults == 0 ){
          this.searchUtils.status = this.errorCodes.NONE;
        }
      },
      err => {
        this.handleError("Error getting number of "+this.getEntityQueryName(resultType,true)+" for community: "+communityId, err);
        this.searchUtils.status = this.errorMessages.getErrorCode(err.status);
      }
    ));
  }
  public getAllResultsForCommunity(resultType:string, communityId: string, page: number, size: number, properties:EnvProperties, contextId = null) {
    this.searchUtils.status = this.errorCodes.LOADING;

    this.subscriptions.push(this._searchResearchResultsService.advancedSearchResults(resultType, null,  page, size, null,  properties, "&type=results&fq=communityid=" + communityId +(contextId?'&fq=categoryid=' + encodeURIComponent(contextId):'')).subscribe(
      data => {
        this.searchUtils.totalResults = data[0];
        this.allResults = data[1];
        this.results = data[1].slice(0, 5) ;

        this.searchUtils.status = this.errorCodes.DONE;
        if(this.searchUtils.totalResults == 0 ){
          this.searchUtils.status = this.errorCodes.NONE;
        }
      },
      err => {
        this.handleError("Error getting "+this.getEntityQueryName(resultType,true)+" for community: "+communityId, err);
        this.searchUtils.status = this.errorMessages.getErrorCode(err.status);
      }
    ));
  }
  public getResultsByKeyword(resultType:string, keyword:string,  page: number, size: number, properties:EnvProperties,  customFilter:SearchCustomFilter=null){
    var parameters = "";
    if(keyword.length > 0){
      var DOIs:string[] = DOI.getDOIsFromString(keyword);
      var doisParams = "";

      for(var i =0 ;i < DOIs.length; i++){
        doisParams+=(doisParams.length > 0?"&":"")+'doi="'+ DOIs[i]+'"';
      }
      if(doisParams.length > 0){
        parameters +=  "&"+doisParams;
      }else{
        parameters = "q=" + StringUtils.URIEncode(keyword);
      }
    }

    this.searchUtils.status = this.errorCodes.LOADING;
    var refineParams = null;
    if(customFilter){
      refineParams = (refineParams?(refineParams+'&'):'')+"&fq="+StringUtils.URIEncode(customFilter.queryFieldName + " exact " + StringUtils.quote((customFilter.valueId )));
    }
    this.subscriptions.push(this._searchResearchResultsService.search(this.getEntityQueryName(resultType,false), parameters,refineParams, page, size, "", [], properties).subscribe(
      data => {
        this.searchUtils.totalResults = data[0];
        this.results = data[1];

        this.searchUtils.status = this.errorCodes.DONE;
        if(this.searchUtils.totalResults == 0 ){
          this.searchUtils.status = this.errorCodes.NONE;
        }
      },
      err => {
        /*console.log(err);
         //TODO check erros (service not available, bad request)
        // if( ){
        //   this.searchUtils.status = ErrorCodes.ERROR;
        // }
        //var errorCodes:ErrorCodes = new ErrorCodes();
        //this.searchUtils.status = errorCodes.ERROR;
        if(err.status == '404') {
          this.searchUtils.status = this.errorCodes.NOT_FOUND;
        } else if(err.status == '500') {
          this.searchUtils.status = this.errorCodes.ERROR;
        } else {
          this.searchUtils.status = this.errorCodes.NOT_AVAILABLE;
        }*/
        this.handleError("Error getting "+this.getEntityQueryName(resultType,true)+" for keyword: "+keyword + (doisParams ? "(DOI)" : ""), err);
        this.searchUtils.status = this.errorMessages.getErrorCode(err.status);
      }
    ));
  }

  public getNumForEntity(resultType: string, entity:string, id:string, properties:EnvProperties){
    this.searchUtils.status = this.errorCodes.LOADING;

    if(id != "" && entity != "") {
      this.subscriptions.push(this._searchResearchResultsService.numOfEntityResults(this.getEntityQueryName(resultType,false), id, entity, properties).subscribe(
        data => {
          this.searchUtils.totalResults = data;

          this.searchUtils.status = this.errorCodes.DONE;
          if(this.searchUtils.totalResults == 0 ){
            this.searchUtils.status = this.errorCodes.NONE;
          }
        },
        err => {
          /*console.log(err);
           //TODO check erros (service not available, bad request)
          // if( ){
          //   this.searchUtils.status = ErrorCodes.ERROR;
          // }
          //var errorCodes:ErrorCodes = new ErrorCodes();
          //this.searchUtils.status = errorCodes.ERROR;
          if(err.status == '404') {
            this.searchUtils.status = this.errorCodes.NOT_FOUND;
          } else if(err.status == '500') {
            this.searchUtils.status = this.errorCodes.ERROR;
          } else {
            this.searchUtils.status = this.errorCodes.NOT_AVAILABLE;
          }*/
          this.handleError("Error getting "+this.getEntityQueryName(resultType,true)+" for "+entity+" with id: "+id, err);
          this.searchUtils.status = this.errorMessages.getErrorCode(err.status);
        }
      ));
    }
  }

  public getDmps(entity:string, id:string, page: number, size: number, properties:EnvProperties) {
    this.searchUtils.status = this.errorCodes.LOADING;
    let parameters = "";
    if(entity == "project") {
      parameters = '(relprojectid exact "'+id+'") and (instancetypename exact "Data Management Plan")';
    } else if(entity == "organization") {
      parameters = '(relorganizationid exact "'+id+'") and (instancetypename exact "Data Management Plan")';
    }
    if(parameters != "") {
      this.subscriptions.push(this._searchResearchResultsService.searchResultForEntity('publication', parameters, page, size, properties).subscribe(
        data => {
          this.searchUtils.totalResults = data[0];
          this.results = data[1];
      
          this.searchUtils.status = this.errorCodes.DONE;
          if (this.searchUtils.totalResults == 0) {
            this.searchUtils.status = this.errorCodes.NONE;
          }
        },
        err => {
          /*console.log(err);
           //TODO check erros (service not available, bad request)
          // if( ){
          //   this.searchUtils.status = ErrorCodes.ERROR;
          // }
          //var errorCodes:ErrorCodes = new ErrorCodes();
          //this.searchUtils.status = errorCodes.ERROR;

          if(err.status == '404') {
            this.searchUtils.status = this.errorCodes.NOT_FOUND;
          } else if(err.status == '500') {
            this.searchUtils.status = this.errorCodes.ERROR;
          } else {
            this.searchUtils.status = this.errorCodes.NOT_AVAILABLE;
          }*/
          this.handleError("Error getting dmps for " + entity + " with id: " + id, err);
          this.searchUtils.status = this.errorMessages.getErrorCode(err.status);
        }
      ));
    }
  }
  
  public getResultsForEntity(resultType: string, entity:string, id:string, page: number, size: number, properties:EnvProperties){
    this.searchUtils.status = this.errorCodes.LOADING;

    let parameters = "";
    if(entity == "project") {
      //parameters = "projects/"+id;
      parameters = '(relprojectid exact "'+id+'")';
    } else if(entity == "organization") {
      //parameters = "organizations/"+id;
      parameters = '(relorganizationid exact "'+id+'")';
    }

    if(parameters != "") {
      this.subscriptions.push(this._searchResearchResultsService.searchResultForEntity(this.getEntityQueryName(resultType,false), parameters, page, size, properties).subscribe(
        data => {
          this.searchUtils.totalResults = data[0];
          this.results = data[1];

          this.searchUtils.status = this.errorCodes.DONE;
          if(this.searchUtils.totalResults == 0 ){
            this.searchUtils.status = this.errorCodes.NONE;
          }
        },
        err => {
          /*console.log(err);
           //TODO check erros (service not available, bad request)
          // if( ){
          //   this.searchUtils.status = ErrorCodes.ERROR;
          // }
          //var errorCodes:ErrorCodes = new ErrorCodes();
          //this.searchUtils.status = errorCodes.ERROR;

          if(err.status == '404') {
            this.searchUtils.status = this.errorCodes.NOT_FOUND;
          } else if(err.status == '500') {
            this.searchUtils.status = this.errorCodes.ERROR;
          } else {
            this.searchUtils.status = this.errorCodes.NOT_AVAILABLE;
          }*/
          this.handleError("Error getting "+this.getEntityQueryName(resultType,true)+" for "+entity+" with id: "+id, err);
          this.searchUtils.status = this.errorMessages.getErrorCode(err.status);
        }
      ));
    }
  }

  public getResultsForDataproviders(resultType: string, id:string, resultsFrom:string, page: number, size: number, properties:EnvProperties){
    this.searchUtils.status = this.errorCodes.LOADING;

    var parameters;
    // if(resultsFrom == "collectedFrom") {
    //   parameters = this.getEntityName(resultType,true)+"?fq=collectedfromdatasourceid exact "+'"'+id+'"';
    // } else if(resultsFrom == "hostedBy") {
    //   parameters = this.getEntityName(resultType,true)+"?fq=resulthostingdatasourceid exact "+'"'+id+'"';
    // }

    parameters = this.getEntityQueryName(resultType,true)+"?fq=collectedfromdatasourceid exact "+'"'+id+'"' + "or resulthostingdatasourceid exact "+'"'+id+'"';

    if(parameters != "") {

      this.subscriptions.push(this._searchResearchResultsService.searchForDataproviders(this.getEntityQueryName(resultType,false), parameters, page, size, properties).subscribe(
        data => {
          this.searchUtils.totalResults = data[0];
          this.results = data[1];

          this.searchUtils.status = this.errorCodes.DONE;
          if(this.searchUtils.totalResults == 0 ){
            this.searchUtils.status = this.errorCodes.NONE;
          }
        },
        err => {
          /*console.log(err);
           //TODO check erros (service not available, bad request)
          // if( ){
          //   this.searchUtils.status = ErrorCodes.ERROR;
          // }
          //var errorCodes:ErrorCodes = new ErrorCodes();
          //this.searchUtils.status = errorCodes.ERROR;
          if(err.status == '404') {
            this.searchUtils.status = this.errorCodes.NOT_FOUND;
          } else if(err.status == '500') {
            this.searchUtils.status = this.errorCodes.ERROR;
          } else {
            this.searchUtils.status = this.errorCodes.NOT_AVAILABLE;
          }*/
          this.handleError("Error getting "+this.getEntityQueryName(resultType,true)+" for "+OpenaireEntities.DATASOURCE+" ("+resultsFrom+") with id: "+id, err);
          this.searchUtils.status = this.errorMessages.getErrorCode(err.status);
        }
      ));
    }
  }

  public getAggregatorResults(resultType: string, id:string, page: number, size: number, properties:EnvProperties){
    this.searchUtils.status = this.errorCodes.LOADING;

    // this.getEntityQueryName(resultType,false)
    this.subscriptions.push(this._searchResearchResultsService.searchAggregators(resultType, id, '&fq=(collectedfromdatasourceid exact "'+id+'" or resulthostingdatasourceid exact "'+id+'")',"&refine=true&fields=resulthostingdatasource&type="+resultType , page, size, properties).subscribe(
      data => {
        this.results = data;
        this.searchUtils.totalResults = this.results.length;

        this.searchUtils.status = this.errorCodes.DONE;
        if(this.searchUtils.totalResults == 0 ){
          this.searchUtils.status = this.errorCodes.NONE;
        }

        this.requestComplete.complete();
      },
      err => {
        /*console.log(err);
         //TODO check erros (service not available, bad request)
        // if( ){
        //   this.searchUtils.status = ErrorCodes.ERROR;
        // }
        //var errorCodes:ErrorCodes = new ErrorCodes();
        //this.searchUtils.status = errorCodes.ERROR;
        if(err.status == '404') {
          this.searchUtils.status = this.errorCodes.NOT_FOUND;
        } else if(err.status == '500') {
          this.searchUtils.status = this.errorCodes.ERROR;
        } else {
          this.searchUtils.status = this.errorCodes.NOT_AVAILABLE;
        }*/
        this.handleError("Error getting "+this.getEntityQueryName(resultType,true)+" for aggregator with id: "+id, err);
        this.searchUtils.status = this.errorMessages.getErrorCode(err.status);

        this.requestComplete.complete();
      }
    ));
  }

  private handleError(message: string, error) {
    console.error("Fetch "+OpenaireEntities.RESULTS+" (class): "+message, error);
  }

  private getEntityQueryName(entityType: string, plural: boolean) {
    if (entityType == "publication" || entityType == "dataset" || entityType == "organization" || entityType == "datasource" || entityType == "project") {
      if (plural) {
        return entityType + "s";
      }
    }
    return entityType;
  }
}
