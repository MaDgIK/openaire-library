 import {SearchOrganizationsService} from '../../services/searchOrganizations.service';
 import { ErrorCodes} from '../../utils/properties/errorCodes';
 import {ErrorMessagesComponent}    from '../../utils/errorMessages.component';
 import {SearchCustomFilter, SearchUtilsClass} from '../../searchPages/searchUtils/searchUtils.class';
 import{EnvProperties} from '../../utils/properties/env-properties';
import {StringUtils} from '../../utils/string-utils.class';
export class FetchOrganizations {
  private errorCodes: ErrorCodes;
  private errorMessages: ErrorMessagesComponent;

  public results =[];

  public searchUtils:SearchUtilsClass = new SearchUtilsClass();
   public subResults: any;



  constructor ( private _searchOrganizationsService: SearchOrganizationsService ) {

    this.errorCodes = new ErrorCodes();
    this.errorMessages = new ErrorMessagesComponent();
    this.searchUtils.status = this.errorCodes.LOADING;

  }


  public clearSubscriptions() {

    if(this.subResults){
      this.subResults.unsubscribe();
    }
  }


    public getResultsByKeyword(keyword:string , page: number, size: number, properties:EnvProperties,  customFilter:SearchCustomFilter=null){
      var parameters = "";
      if(keyword.length > 0){
        parameters = "q=" + StringUtils.URIEncode(keyword);
      }

      //var errorCodes:ErrorCodes = new ErrorCodes();
      this.searchUtils.status = this.errorCodes.LOADING;
      var refineParams = null;
      if(customFilter){
        refineParams = (refineParams?(refineParams+'&'):'')+"&fq="+StringUtils.URIEncode(customFilter.queryFieldName + " exact " + StringUtils.quote((customFilter.valueId )));
      }
      this.subResults = this._searchOrganizationsService.searchOrganizations(parameters, refineParams, page, size, [], properties).subscribe(
          data => {
              this.searchUtils.totalResults = data[0];
              //console.info("search Organizations: [Parameters:"+parameters+" ]  [total results:"+this.searchUtils.totalResults+"]");
              this.results = data[1];

              //var errorCodes:ErrorCodes = new ErrorCodes();
              this.searchUtils.status = this.errorCodes.DONE;
              if(this.searchUtils.totalResults == 0 ){
                this.searchUtils.status = this.errorCodes.NONE;
              }
           },
          err => {
              /*console.log(err);
               //TODO check erros (service not available, bad request)
              // if( ){
              //   this.searchUtils.status = ErrorCodes.ERROR;
              // }
              //var errorCodes:ErrorCodes = new ErrorCodes();
              //this.searchUtils.status = errorCodes.ERROR;

              if(err.status == '404') {
                this.searchUtils.status = this.errorCodes.NOT_FOUND;
              } else if(err.status == '500') {
                this.searchUtils.status = this.errorCodes.ERROR;
              } else {
                this.searchUtils.status = this.errorCodes.NOT_AVAILABLE;
              }*/
              this.handleError("Error getting organization for keyword: "+keyword, err);
              this.searchUtils.status = this.errorMessages.getErrorCode(err.status);
           }
      );
    }

    private handleError(message: string, error) {
        console.error("Fetch Organizations (class): "+message, error);
    }
}
