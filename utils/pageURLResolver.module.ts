import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {PageURLResolverComponent} from "./pageURLResolver.component";


@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule
  ],
  declarations: [
    PageURLResolverComponent
  ],
  exports: [
    PageURLResolverComponent
  ]
})
export class PageURLResolverModule {}
