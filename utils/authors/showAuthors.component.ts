import {
  AfterContentInit,
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  Inject,
  Input,
  PLATFORM_ID,
  ViewChild
} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {RouterHelper} from "../routerHelper.class";
import {EnvProperties} from '../properties/env-properties';
import {isPlatformBrowser} from "@angular/common";
import {Author} from "../result-preview/result-preview";
import {AlertModal} from "../modal/alert";
import {properties} from "../../../../environments/environment";

@Component({
  selector: 'showAuthors',
  template: `
      <ng-template #author_template let-author="author" let-i="i" let-italic="italic" let-modal="modal">
        <span *ngIf="isSticky || (!author.orcid && !author.orcid_pending) || !isBrowser" style="margin-right: 5px;"
              [class.uk-text-italic]="italic">
          {{author.fullName + ";"}}
        </span>
        <ng-container *ngIf="!isSticky && (author.orcid || author.orcid_pending) && isBrowser">
            <a #toggle class="uk-flex-inline uk-flex-middle uk-link-text">
                <img *ngIf="author.orcid" src="assets/common-assets/common/ORCIDiD_icon16x16.png" alt="orcid"
                     loading="lazy" style="width:16px; height:16px; margin-right: 3px;">
                <img *ngIf="!author.orcid && author.orcid_pending"
                     src="assets/common-assets/common/ORCIDiD_iconbw16x16.png" alt="orcid bw" loading="lazy"
                     style="margin-right: 3px;">
                <span style="margin-right: 5px;" [class.uk-text-italic]="italic">
                  {{author.fullName + ";"}}
                </span>
            </a>
            <div *ngIf="!isMobile" class="default-dropdown uk-margin-remove-top uk-dropdown orcid-dropdown" 
                 uk-dropdown="mode:click; offset: 4; pos: bottom-right" style="min-width: 465px !important;" [attr.container]="'#modal-container'">
                <ng-container *ngTemplateOutlet="dropdown"></ng-container>
            </div>
            <mobile-dropdown *ngIf="isMobile" [toggle]="toggle">
                <div class="orcid-dropdown">
                    <ng-container *ngTemplateOutlet="dropdown"></ng-container>
                </div>
            </mobile-dropdown>
            <ng-template #dropdown>
                <div class="uk-padding-small">
                    <h6 class="uk-margin-remove">{{author.fullName}}</h6>
                    <div>
                        <div class="uk-text-meta uk-margin-bottom">ORCID</div>
  
                        <div class="uk-text-meta uk-text-xsmall uk-margin-small-top uk-margin-small-bottom">
                            <img *ngIf="author.orcid" src="assets/common-assets/common/ORCIDiD_icon16x16.png" alt=""
                                 loading="lazy">{{" "}}
                            <img *ngIf="!author.orcid && author.orcid_pending"
                                 src="assets/common-assets/common/ORCIDiD_iconbw16x16.png" alt="" loading="lazy">{{" "}}
  
                            <i *ngIf="author.orcid">Harvested from ORCID Public Data File</i>
                            <i *ngIf="!author.orcid && author.orcid_pending">Derived by OpenAIRE algorithms or harvested
                                from 3rd party repositories</i>
                        </div>
                        <div>
                            <div class="clipboard-wrapper uk-width-1-1 uk-flex uk-flex-middle uk-flex-center"
                                 style="min-height: 43px; min-width: 280px;">
                                <input #element class="uk-padding-small uk-text-emphasis uk-width-expand uk-disabled"
                                       [value]="properties.orcidURL+(author.orcid ? author.orcid : author.orcid_pending)"/>
                                <button class="uk-button uk-button-link uk-margin-small-right copy orcid_clipboard_btn" [ngClass]="'orcid_clipboard_btn_auhtor_'+i"
                                   title="Copy to clipboard" (click)="copyToClipboard(element)">
                                    <icon name="content_copy" visuallyHidden="Copy to clipboard" [flex]="true"></icon>
                                </button>
                            </div>
                            <div class="uk-text-center uk-margin-small-top">
                                <a class="uk-button uk-button-text custom-external"
                                   title="Visit author in ORCID"
                                   [href]="properties.orcidURL+(author.orcid ? author.orcid : author.orcid_pending)"
                                   target="_blank">
                                    VISIT AUTHOR IN ORCID
                                </a>
                            </div>
                        </div>
                    </div>
  
                    <hr>
                    <div class="uk-margin-top uk-text-bold uk-text-emphasis uk-text-center">
                        {{author.fullName}} in OpenAIRE
                    </div>
                    <div class="uk-text-center uk-margin-top uk-margin-large-left uk-margin-large-right">
                        <a *ngIf="properties.adminToolsPortalType !== 'eosc'"
                           class="uk-button uk-button-primary uk-text-bold uk-padding-remove-top uk-padding-remove-bottom"
                           (click)="onClick()"
                           [queryParams]="routerHelper.createQueryParams(['orcid','oc'],[(author['orcid'] ? author['orcid'] : author['orcid_pending']),'and'])"
                           routerLinkActive="router-link-active" [routerLink]="properties.searchLinkToAdvancedResults">
                            <span class="space">Search</span>
                        </a>
                        <a *ngIf="properties.adminToolsPortalType == 'eosc'"
                           class="uk-button uk-button-primary uk-text-bold uk-padding-remove-top uk-padding-remove-bottom uk-light"
                           [href]="'https://explore.openaire.eu'+properties.searchLinkToAdvancedResults+'?orcid='+(author['orcid'] ? author['orcid'] : author['orcid_pending'])+'&oc=and'"
                           target="_blank">
                            <span class="space custom-external">Search in OpenAIRE</span>
                        </a>
                    </div>
                </div>
            </ng-template>
        </ng-container>
      </ng-template>

      <div *ngIf="authors"
           class="uk-height-max-medium uk-overflow-auto uk-text-small uk-text-emphasis uk-flex uk-flex-wrap">
      <span class="uk-text-meta uk-margin-xsmall-right">
        Authors:
      </span>
          <ng-container *ngFor="let author of authors.slice(0, viewAll?authors.length:authorsLimit) let i=index">
              <ng-container
                      *ngTemplateOutlet="author_template; context: { author: author, i:i, italic: true, modal: modal}"></ng-container>
          </ng-container>
          <span *ngIf="!showAll && authors && authors.length > authorsLimit" class="uk-text-meta">
        +{{authors.length - authorsLimit | number}} more
      </span>
          <span *ngIf="showAll && authors && authors.length > authorsLimit && !viewAll">
        <a (click)="viewAllClick();" class="uk-background-muted custom-extra-entities">
          +{{authors.length - authorsLimit | number}} Authors
        </a>
      </span>
          <div *ngIf="viewAll && lessBtn" class="uk-text-small">
              <a (click)="viewAll = !viewAll; lessBtn=false;">View less</a>
          </div>
      </div>

      <modal-alert *ngIf="!isMobile && showAll && authors && authors.length > authorsLimit" #authorsModal>
          <div class="uk-text-small uk-text-emphasis uk-grid uk-grid-column-collapse uk-grid-row-small" uk-grid>
              <ng-container *ngFor="let author of authors; let i=index">
                  <ng-container *ngTemplateOutlet="author_template; context: { author: author, i:i, modal: authorsModal}"></ng-container>
              </ng-container>
          </div>
      </modal-alert>

      <fs-modal *ngIf="isMobile && showAll && authors && authors.length > authorsLimit" #authorsModal classTitle="uk-tile-default uk-border-bottom">
          <div class="uk-text-small uk-text-emphasis uk-grid uk-grid-column-collapse uk-grid-row-small" uk-grid>
              <ng-container *ngFor="let author of authors; let i=index">
                  <ng-container *ngTemplateOutlet="author_template; context: { author: author, i:i, modal: authorsModal}"></ng-container>
              </ng-container>
          </div>
      </fs-modal>
  `
  
})

export class ShowAuthorsComponent {
  @Input() isMobile: boolean = false;
  @Input() authors: Author[];
  @Input() isSticky: boolean = false;
  @Input() authorsLimit: number = 7;
  @Input() showAll: boolean = true;
  @Input() modal: AlertModal;
  @Input() viewAll: boolean = false;
  @Input() showInline: boolean = false; // do not open modal for "view more" when this is true
  public lessBtn: boolean = false;
  @ViewChild('authorsModal') authorsModal;

  public properties: EnvProperties = properties;
  public routerHelper: RouterHelper = new RouterHelper();
  
  isBrowser: boolean;
  public clipboard;

  constructor(private route: ActivatedRoute, @Inject(PLATFORM_ID) private platformId: string) {
    this.isBrowser = isPlatformBrowser(platformId);
  }
  
  ngOnInit() {}

  ngOnDestroy() {
    if(typeof document !== 'undefined') {
      let modal_container = document.getElementById("modal-container");
      modal_container.childNodes.forEach(c=> {
        if (c['className'] && c['className'].includes("orcid-dropdown")) {
          modal_container.removeChild(c);
        }
      });
    }
  }

  public onClick() {
    if (this.modal) {
      this.modal.cancel();
    }
    if(this.authorsModal) {
      this.authorsModal.cancel();
    }
  }

  public viewAllClick() {
    if(this.authors.length <= this.authorsLimit*2 || this.showInline) {
      this.viewAll = true;
      this.lessBtn = true;
    } else {
      this.openAuthorsModal();
    }
  }

  public openAuthorsModal() {
    if (this.isMobile) {
      this.authorsModal.okButton = false;
      this.authorsModal.title = "Authors";
      this.authorsModal.open();
    } else {
      this.authorsModal.cancelButton = false;
      this.authorsModal.okButton = false;
      this.authorsModal.alertTitle = "Authors";
      this.authorsModal.open();
    }
  }

  copyToClipboard(element: HTMLInputElement) {
    element.select();
    if (typeof document !== 'undefined') {
      document.execCommand('copy');
    }
  }
}
