import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {ShowAuthorsComponent} from './showAuthors.component';
import {AlertModalModule} from "../modal/alertModal.module";
import {FullScreenModalModule} from "../modal/full-screen-modal/full-screen-modal.module";
import {MobileDropdownModule} from "../mobile-dropdown/mobile-dropdown.module";
import {IconsModule} from "../icons/icons.module";

@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule, AlertModalModule, FullScreenModalModule, MobileDropdownModule, IconsModule
  ],
  declarations: [
    ShowAuthorsComponent
  ],
  providers:[
  ],
  exports: [
    ShowAuthorsComponent
  ]
})
export class ShowAuthorsModule { }
