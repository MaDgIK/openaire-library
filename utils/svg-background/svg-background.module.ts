import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {SvgBackgroundDirective} from "./svg-background.directive";

@NgModule({
  imports: [CommonModule],
  declarations: [SvgBackgroundDirective],
  exports: [SvgBackgroundDirective]
})
export class SvgBackgroundModule {

}
