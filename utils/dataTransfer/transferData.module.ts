import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {EGIDataTransferComponent} from "./transferData.component";
import {InputModule} from "../../sharedComponents/input/input.module";
import {AlertModalModule} from "../modal/alertModal.module";
import {FullScreenModalModule} from "../modal/full-screen-modal/full-screen-modal.module";
import {IconsModule} from '../icons/icons.module';

@NgModule({
  imports: [
    CommonModule, FormsModule, InputModule, AlertModalModule, FullScreenModalModule, IconsModule
  ],
  declarations: [
    EGIDataTransferComponent
	],

  exports: [
    EGIDataTransferComponent
	]
})
export class EGIDataTransferModule { }
