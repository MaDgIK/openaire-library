export enum Level {
  ALL,
  K,
  M,
  B
}


export interface NumberSize {
  number: number;
  size: "B" | "M" | "K" | ""
  count: number;
}

export class NumberUtils {
  
  public static roundNumber(num: number | string, level: Level = Level.ALL, decimal = 0): any {
    num = Number.parseFloat(num.toString());
    let roundNum: NumberSize;
    let initialNum = num;
    let variance = Math.pow(10, decimal);
    if (num >= 1000000000 && level <= Level.B) {
      num = num / 1000000;
      num = Math.round(num * variance) / variance;
      roundNum = {number: num, size: "B", count: initialNum};
    } else if (num >= 1000000 && level <= Level.M) {
      num = num / 1000000;
      num = Math.round(num * variance) / variance;
      roundNum = {number: num, size: "M", count: initialNum};
    } else if (num >= 1000 && level <= Level.K) {
      num = num / 1000;
      num = Math.round(num * variance) / variance;
      roundNum = {number: num, size: "K", count: initialNum};
    } else {
      roundNum = {number: num, size: "", count: initialNum};
    }
    return roundNum;
  }
  
  
}
