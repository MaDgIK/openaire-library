import {Component, ElementRef, Input} from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
//Usage :: <i-frame [url]="url" width="30%" height="250"></i-frame>`
@Component({
  selector: 'i-frame',
  template: `
    <div  *ngIf="!style" class=" uk-height-large" [class.iframeContainer]="addClass" [ngClass]="customContainerClass">
      <iframe allowtransparency="true"   [src]="safeUrl" [ngClass]="customIframeClass"></iframe>
    </div>
    <div *ngIf="style"   [class.iframeContainer]="addClass" [ngStyle]="style" [ngClass]="customContainerClass">
      <iframe [src]="safeUrl" [ngClass]="customIframeClass"></iframe>
    </div>
  `
})
export class IFrameComponent {
  public safeUrl: SafeResourceUrl;
  @Input() url ;
  @Input() width: number;
  @Input() height: number;
  @Input() unit: string = 'px';
  @Input() addClass: boolean= true;
  @Input() customContainerClass: string = "";
  @Input() customIframeClass: string = "";
  public style: any;

  constructor(private sanitizer: DomSanitizer) {
  }
  ngOnInit() {
    this.safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
    let width = 'width.' + this.unit;
    let height = 'height.' + this.unit;
    if(this.width && this.height) {
      this.style = {};
      this.style[width] = this.width;
      this.style[height] = this.height;
    } else if(this.height) {
      this.style = {};
      this.style[height] = this.height;
    }
  }
}
