import { NgModule }            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';

import {StaticAutoCompleteComponent} from './staticAutoComplete.component';
import {RefineFieldResultsServiceModule} from '../../services/refineFieldResultsService.module';
//import {ISVocabulariesService} from './ISVocabularies.service';


@NgModule({
  imports: [
    CommonModule, FormsModule, RefineFieldResultsServiceModule
  ],
  declarations: [
     StaticAutoCompleteComponent
  ],
  exports: [
      StaticAutoCompleteComponent
    ],
    providers:[ /*ISVocabulariesService*/ ]
})
export class StaticAutocompleteModule { }
