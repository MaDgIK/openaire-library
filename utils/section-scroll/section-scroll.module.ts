import {NgModule} from "@angular/core";
import {SectionScrollComponent} from "./section-scroll.component";
import {CommonModule} from "@angular/common";

@NgModule({
  imports: [CommonModule],
  declarations: [SectionScrollComponent],
  exports: [SectionScrollComponent]
})
export class SectionScrollModule {}
