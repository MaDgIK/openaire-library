
import {Component, Input, Output, EventEmitter} from '@angular/core';
import{EnvProperties} from './properties/env-properties';
import {properties} from "../../../environments/environment";

@Component({
  selector: 'paging-no-load',
  template: `
    <ul *ngIf="currentPage > 0 && currentPage <= getTotalPages()"
        [class.uk-disabled]="loading" [class.uk-invisible]="getTotalPages() < 2" class="uk-pagination uk-margin-remove" [ngClass]="customClasses">
      <li [class.uk-invisible]="currentPage <= 1">
        <a (click)="onPage((currentPage -1))" aria-label="Previous">
          <icon name="chevron_left" ratio="1.2" [flex]="true" customClass="uk-pagination-previous"></icon>
        </a>
      </li>
      <li *ngIf=" currentPage -2 > 0"><a (click)="onPage((currentPage -2))">{{(currentPage -2) | number}}</a></li>
      <li *ngIf=" currentPage -1 > 0 "><a (click)="onPage((currentPage -1))">{{(currentPage -1) | number}}</a></li>
      <li class="uk-active"><span >{{currentPage | number}}</span></li>
      <li *ngIf=" currentPage +1 <= getTotalPages() "><a (click)="onPage((currentPage +1))">{{(currentPage +1) | number}}</a></li>
      <li *ngIf=" currentPage +2 <= getTotalPages() "><a (click)="onPage((currentPage +2))">{{(currentPage +2) | number}}</a></li>
      <li *ngIf=" (currentPage -2 <= 0) && (currentPage +3 <= getTotalPages()) " class="uk-visible@l"><a (click)="onPage((currentPage +3))">{{(currentPage +3) | number}}</a></li>
      <li *ngIf=" (currentPage -1 <= 0) && (currentPage +4 <= getTotalPages()) " class="uk-visible@l"><a (click)="onPage((currentPage +4))">{{(currentPage +4) | number}}</a></li>
      <li [class.uk-invisible]="getTotalPages() <= currentPage">
       <a (click)="onPage(currentPage +1)" aria-label="Next">
         <icon name="chevron_right" ratio="1.2" [flex]="true" customClass="uk-pagination-next"></icon>
       </a>
      </li>
   </ul>
  `
})

export class pagingFormatterNoLoad {
  @Input() public currentPage: number = 1;
  @Input() public customClasses: string = 'uk-flex-center';
  @Input() public term: string='';
  @Input() public size: number=10;
  @Input() public totalResults: number = 10;
  @Input() public limitPaging: boolean = false;
  @Output() pageChange  = new EventEmitter();
  public properties:EnvProperties = properties;
  private limit: number = this.properties.pagingLimit;
  @Input()
  public loading:boolean = false;
  
  getTotalPages(){
    let total: number = 0;
    let i= this.totalResults/this.size;
    let integerI=parseInt(''+i);
    total = parseInt(''+((i==integerI)?i:i+1));
    if(this.limitPaging) {
      if((this.currentPage == this.limit) && (total > this.limit)) {
        total = this.limit;
      } else if((this.currentPage > this.limit) && (total > this.limit)) {
        total = this.currentPage;
      }
    }
    return total;
  }
  
  onPrev(){
    this.currentPage=this.currentPage-1;
    this.pageChange.emit({
      value: this.currentPage
    });
  }
  
  onNext(){
    this.currentPage=this.currentPage+1;
    this.pageChange.emit({
      value: this.currentPage
    });
  }
  
  onPage(pageNum: number){
    if(!this.loading) {
      this.currentPage = pageNum;
      this.pageChange.emit({
        value: this.currentPage
      });
    }
  }
}
