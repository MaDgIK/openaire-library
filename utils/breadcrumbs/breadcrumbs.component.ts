import {Component, Input} from "@angular/core";

export interface Breadcrumb {
  name: string;
  route?: string;
  keepFormat?: boolean
}

@Component({
  selector: 'breadcrumbs',
  template: `
    <ul class="uk-breadcrumb uk-margin-remove-bottom" [ngClass]="addClass" [class.uk-light]="light">
      <li *ngFor="let breadcrumb of breadcrumbs">
        <a [class.uk-text-capitalize]="!breadcrumb.keepFormat" *ngIf="breadcrumb.route" [routerLink]="breadcrumb.route">{{breadcrumb.name}}</a>
        <span [class.uk-text-capitalize]="!breadcrumb.keepFormat" *ngIf="!breadcrumb.route">{{breadcrumb.name}}</span>
      </li>
    </ul>`
})
export class BreadcrumbsComponent {

  @Input() public light: boolean = false;
  @Input() public breadcrumbs: Breadcrumb[] = [];
  @Input() public addClass = "";
}
