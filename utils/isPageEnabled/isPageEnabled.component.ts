import {properties} from "src/environments/environment";
import {Component, Input, OnDestroy, OnInit} from "@angular/core";
import {EnvProperties} from "../properties/env-properties";
import {CommunityInfo} from "../../connect/community/communityInfo";
import {ActivatedRoute, Router} from "@angular/router";
import {CommunityService} from "../../connect/community/community.service";
import {ConfigurationService} from "../configuration/configuration.service";
import {Subscription} from "rxjs";

@Component({
  selector: '[page-enabled]',
  template: `
    
<ng-container *ngIf="pageRoute && pageEnabled">
  <ng-content></ng-content>
</ng-container>
  `
})
export class IsPageEnabledComponent implements OnInit, OnDestroy {
  @Input() pageRoute:string;
  @Input() communityId:string;
   public properties: EnvProperties = properties;
  pageEnabled =  false;

  public community: CommunityInfo;
 loading;
  private subs: any[] = [];

  constructor(private route: ActivatedRoute,
              private router: Router,
              private communityService: CommunityService,
              private configurationService: ConfigurationService) {
  }


  ngOnInit() {
      this.getPageStatus();
  }

  ngOnDestroy() {
    this.subs.forEach(sub => {
      if (sub instanceof Subscription) {
        sub.unsubscribe();
      }
    })
  }




  private getPageStatus() {
    this.configurationService.isPageEnabled(this.properties, this.communityId, '/' + this.pageRoute).subscribe((page) => {
      this.pageEnabled = page;
    })
  }


}
