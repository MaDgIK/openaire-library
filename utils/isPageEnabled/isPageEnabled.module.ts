import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from "@angular/router";
import {IsPageEnabledComponent} from "./isPageEnabled.component";

@NgModule({
  declarations: [IsPageEnabledComponent],
  imports: [
    CommonModule, RouterModule
  ],
  exports:[IsPageEnabledComponent],
  providers:[]
})
export class IsPageEnabledModule {
}
