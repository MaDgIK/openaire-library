import { Component, Input} from '@angular/core';
import {EnvProperties} from "../properties/env-properties";
import {properties} from "../../../../environments/environment";
@Component({
    selector: 'manage',
    template: `
        <a [href]="link" class="uk-button uk-button-text uk-text-uppercase" target="_blank">Manage</a>
    `
})

export class ManageComponent {
   @Input() communityId:string;
   @Input() alias: string;
   properties: EnvProperties = properties;
   constructor() {
   }
   
   get link(): string {
     if(this.communityId) {
       return this.properties.adminPortalURL + '/' + this.communityId;
     } else if(this.alias) {
       return this.properties.domain + properties.baseLink + '/admin/' + this.alias;
     } else {
       return null;
     }
   }
}
