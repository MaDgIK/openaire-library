import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { RouterModule } from '@angular/router';

import {ManageComponent} from './manage.component';
import {AlertModalModule} from '../modal/alertModal.module';

@NgModule({
  imports: [
    CommonModule,   RouterModule, AlertModalModule
  ],
  declarations: [
    ManageComponent
  ],
  exports: [
    ManageComponent
  ]
})
export class ManageModule { }
