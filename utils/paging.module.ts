import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {pagingFormatterNoLoad} from './pagingFormatterNoLoad.component';

import {PagingFormatter} from './pagingFormatter.component';
import {IconsModule} from "./icons/icons.module";
import {ResultsAndPagesNumComponent} from "./resultsAndPagesNum.component";


@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule, IconsModule
  ],
  declarations: [
    ResultsAndPagesNumComponent,
    pagingFormatterNoLoad,
    PagingFormatter,
  ],
  exports: [
    ResultsAndPagesNumComponent,
    pagingFormatterNoLoad,
    PagingFormatter,
  ]
})
export class PagingModule {}
