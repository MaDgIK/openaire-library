import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {EntityActionsComponent} from "./entity-actions.component";
import {RouterModule} from "@angular/router";
import {IconsModule} from "../icons/icons.module";
import {AlertModalModule} from "../modal/alertModal.module";
import {CiteThisModule} from "../../landingPages/landing-utils/citeThis/citeThis.module";
import {LandingModule} from "../../landingPages/landing-utils/landing.module";
import {InputModule} from "../../sharedComponents/input/input.module";

@NgModule({
  imports: [CommonModule, IconsModule, AlertModalModule, CiteThisModule, LandingModule, RouterModule, InputModule],
  declarations: [EntityActionsComponent],
  exports: [EntityActionsComponent]
})
export class EntityActionsModule {

}
