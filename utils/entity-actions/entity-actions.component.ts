import {Component, Input, OnInit, ViewChild} from "@angular/core";
import {StringUtils} from "../string-utils.class";
import {RouterHelper} from "../routerHelper.class";
import {properties} from "../../../../environments/environment";
import {Router} from "@angular/router";
import {Option} from "../../sharedComponents/input/input.component";
import {OpenaireEntities} from "../properties/searchFields";
import {EnvProperties} from "../properties/env-properties";

@Component({
  selector: 'entity-actions',
  template: `
    <div class="" [ngClass]="isMobile ? 'uk-flex uk-flex-column' : 'uk-grid uk-grid-small uk-flex-middle uk-child-width-auto'" [attr.uk-grid]="!isMobile ? '' : null">
      <div *ngIf="linking && isRouteAvailable('participate/direct-claim')">
        <a [queryParams]="routerHelper.createQueryParams(['id','type','linkTo'], [id,type,linkTo])"
          routerLinkActive="router-link-active" routerLink="/participate/direct-claim"
          [attr.uk-tooltip]="showTooltip ? 'title: Link '+openaireEntities.RESULTS+' with a '+openaireEntities.PROJECT+', a '+openaireEntities.COMMUNITY+' or other '+openaireEntities.RESULTS+' and make the new information available in OpenAIRE information space.; pos: bottom; cls: uk-active uk-text-small uk-padding-small uk-width-medium' : 'cls: uk-invisible'"
          class="uk-flex uk-flex-middle uk-button-link" [ngClass]="isMobile ? 'uk-margin-left uk-margin-xsmall-bottom' : 'uk-text-bolder uk-flex-center'">
          <icon [flex]="true" [ratio]="0.7" name="link_to" visuallyHidden="link"></icon>
          <span *ngIf="!compactView" class="uk-margin-xsmall-left">Link to</span>
        </a>
      </div>
      <div *ngIf="share">
        <a (click)="openAddThisModal()"
          [attr.uk-tooltip]="showTooltip ? 'title: Share this '+getTypeName()+' in your social networks; pos: bottom; cls: uk-active  uk-text-small uk-padding-small' : 'cls: uk-invisible'"
          class="uk-flex uk-flex-middle uk-button-link" [ngClass]="isMobile ? 'uk-margin-left uk-margin-xsmall-bottom' : 'uk-text-bolder uk-flex-center'">
          <icon class="uk-text-bolder" [flex]="true" [ratio]="0.8" name="share" visuallyHidden="share"></icon>
          <span *ngIf="!compactView" class="uk-margin-xsmall-left">Share</span>
        </a>
      </div>
      <div *ngIf="cite">
        <a (click)="openCiteModal()"
          class="uk-flex uk-flex-middle uk-button-link" [ngClass]="isMobile ? 'uk-margin-left uk-margin-xsmall-bottom' : 'uk-text-bolder uk-flex-center'">
          <icon class="uk-text-bolder" [flex]="true" [ratio]="0.7" name="cite" visuallyHidden="cite"></icon>
          <span *ngIf="!compactView" class="uk-margin-xsmall-left">Cite</span>
        </a>
      </div>
      <div *ngIf="deposit && isRouteAvailable('participate/deposit/learn-how')">
        <a routerLinkActive="router-link-active" routerLink="/participate/deposit/learn-how"
          [attr.uk-tooltip]="showTooltip ? 'title: Find a repository to deposit or publish your research in Open Access; pos: bottom; cls: uk-active  uk-text-small uk-padding-small uk-width-medium' : 'cls: uk-invisible'"
          class="uk-flex uk-flex-middle uk-button-link" [ngClass]="isMobile ? 'uk-margin-left uk-margin-xsmall-bottom' : 'uk-text-bolder uk-flex-center'">
          <icon flex="true" ratio="0.7" name="upload" visuallyHidden="upload"></icon>
          <span *ngIf="!compactView" class="uk-margin-xsmall-left">Deposit</span>
        </a>
      </div>
      <div *ngIf="embed && properties.environment !== 'beta'">
        <a (click)="openEmbedResultsModal()"
          [attr.uk-tooltip]="showTooltip ? 'title: Embed the related '+openaireEntities.RESULTS+' of this '+getTypeName()+' in your website; pos: bottom; cls: uk-active  uk-text-small uk-padding-small uk-width-medium' : 'cls: uk-invisible'"
          class="uk-flex uk-flex-middle uk-button-link" [ngClass]="isMobile ? 'uk-margin-left uk-margin-xsmall-bottom' : 'uk-text-bolder uk-flex-center'">
          <icon flex="true" ratio="0.8" name="code" visuallyHidden="code"></icon>
          <span *ngIf="!compactView" class="uk-margin-xsmall-left">Embed</span>
        </a>
      </div>
      <ng-content></ng-content>
    </div>
    <modal-alert *ngIf="cite" #citeModal>
      <citeThis *ngIf="citeThisClicked" [result]="result" [id]="id"
                [type]="getTypeName().toLowerCase()"></citeThis>
    </modal-alert>
    <modal-alert *ngIf="share" #addThisModal classBody="uk-flex uk-flex-center uk-flex-middle">
      <addThis *ngIf="addThisClicked" [url]="url"></addThis>
    </modal-alert>
    <modal-alert *ngIf="result" #embedResultsModal large="true">
      <div class="uk-padding-small uk-margin-small-left uk-margin-small-right">
        <div class="uk-padding-small uk-padding-remove-vertical">
          <div input type="select" placeholder="Select content type to embed" inputClass="flat x-small"
            [options]="resultTypesForEmbedding" [(value)]="embed_research_results_type"></div>
          <div *ngIf="embed_research_results_type" class="clipboard-wrapper box-content uk-grid uk-margin-auto uk-margin-small-top ">
        <pre id="dynamic_content_id" class="uk-overflow-auto uk-padding-small uk-padding-remove-vertical uk-margin-top"><code
        >{{getDynamicContent(embed_research_results_type)}}</code></pre>
            <div class="uk-width-1-1 uk-padding-small uk-text-right">
              <a class="uk-link copy clipboard_btn" data-clipboard-target="#dynamic_content_id" title="Copy script">
                COPY SCRIPT
              </a>
            </div>
          </div>

          <div class="uk-text-small uk-margin-top">
            For further information contact us at
            <u><a [href]="'mailto:'+properties.helpdeskEmail" class="uk-link-text">{{properties.helpdeskEmail}}</a></u>
          </div>
        </div>
      </div>
    </modal-alert>
  `
})
export class EntityActionsComponent implements OnInit {
  @Input() result: any;
  @Input() type: string;
  @Input() id: string;
  @Input() linking: boolean = false;
  @Input() share: boolean = false;
  @Input() cite: boolean = false;
  @Input() deposit: boolean = false;
  @Input() embed: boolean = false;
  @Input() url: string;
  @Input() isMobile: boolean = false;
  @Input() showTooltip: boolean = true;
  @Input() compactView: boolean = false; // if true, do not show label for actions
  public citeThisClicked: boolean;
  public addThisClicked: boolean;
  public routerHelper: RouterHelper = new RouterHelper();
  @ViewChild('citeModal') citeModal;
  @ViewChild('embedResultsModal') embedResultsModal;
  @ViewChild('addThisModal') addThisModal;
  properties: EnvProperties = properties;
	openaireEntities = OpenaireEntities;


  /* Embed */
  public embed_research_results_type: string = "result";
  public clipboard;
  public resultTypesForEmbedding: Option[]=[
    {label: "All "+OpenaireEntities.RESULTS, value: "result"},
    {label: OpenaireEntities.PUBLICATIONS, value: "publication"},
    {label: OpenaireEntities.DATASETS, value: "dataset"},
    {label: OpenaireEntities.SOFTWARE, value: "software"},
    {label: OpenaireEntities.OTHER, value: "other"}];
  
  constructor(private router: Router) {
  }
  
  ngOnInit() {
    this.createClipboard();
  }
  
  get linkTo():string {
    return this.type === 'project'?'result':'project';
  }

  public getTypeName(): string {
    return StringUtils.getEntityName(this.type, false);
  }

  public openCiteModal() {
    this.citeThisClicked = true;
    this.citeModal.cancelButton = false;
    this.citeModal.okButton = false;
    this.citeModal.alertTitle = "Cite this " + this.getTypeName();
    this.citeModal.open();
  }

  public openAddThisModal() {
    this.addThisClicked = true;
    this.addThisModal.cancelButton = false;
    this.addThisModal.okButton = false;
    this.addThisModal.alertTitle = "Share this " + this.getTypeName() + " in your social networks";
    this.addThisModal.open();
  }
  
  public openEmbedResultsModal() {
    this.embedResultsModal.cancelButton = false;
    this.embedResultsModal.okButton = false;
    this.embedResultsModal.alertTitle = "Embed results";
    this.embedResultsModal.open();
  }
  
  private createClipboard() {
    if (typeof window !== 'undefined') {
      delete this.clipboard;
      let Clipboard;
      Clipboard = require('clipboard');
      this.clipboard = new Clipboard('.clipboard_btn');
    }
  }
  
  getDynamicContent(type: string) {
    return  "<script type=\"text/javascript\">"
      + "\n<!--"
      + "\ndocument.write('<div id=\"oa_widget\"></div>');"
      + "\ndocument.write('<script type=\"text/javascript\""
      + " src=\"" + this.properties.widgetLink
      + this.result.id + "&type="
      + type
      + "\"></script>');"
      + "\n-->"
      + "\n</script>";
  }
  
  isRouteAvailable(routeToCheck: string) {
    for (let i = 0; i < this.router.config.length; i++) {
      let routePath: string = this.router.config[i].path;
      if (routePath == routeToCheck || routeToCheck.split('/')[0] == routePath) {
        return true;
      }
    }
    return false;
  }
}
