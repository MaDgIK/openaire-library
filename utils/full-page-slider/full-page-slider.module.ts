import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FullPageSliderComponent} from "./full-page-slider.component";
import {SlideComponent} from "./slide.component";
import {BottomModule} from '../../sharedComponents/bottom.module';
import {IconsModule} from '../icons/icons.module';

@NgModule({
  imports: [CommonModule, BottomModule, IconsModule],
  declarations: [FullPageSliderComponent, SlideComponent],
  exports: [FullPageSliderComponent, SlideComponent],
})
export class FullPageSliderModule {}
