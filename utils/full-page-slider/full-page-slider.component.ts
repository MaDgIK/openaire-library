import {AfterContentInit, Component, ContentChildren, Input, OnInit, QueryList} from "@angular/core";
import {SlideComponent} from "./slide.component";
import {BehaviorSubject, Observable} from 'rxjs';

@Component({
  selector: 'fp-slider',
  template: `
    <div class="menu">
      <img class="logo" *ngIf="logoURL" [src]="logoURL">
      <a *ngIf="stateValue > 1" class="previous" (click)="onClick(stateValue - 1)">
        <icon name="arrow_up"></icon>
      </a>
      <nav>
        <ul>
          <li *ngFor="let slide of slides.toArray();let i=index" [class.uk-active]="i === (stateValue - 1)">
            <a (click)="onClick(i + 1)"></a>
          </li>
        </ul>
      </nav>
      <a *ngIf="stateValue < slides.length" class="next" (click)="onClick(stateValue + 1)">
        <icon name="arrow_down"></icon>
      </a>
    </div>
    <div [ngClass]="topBar" class="top-bar"></div>
    <section (wheel)="onWheel($event)" [class.has-footer]="hasFooter && state.value === slides.length">
      <ng-content></ng-content>
    </section>
    <bottom *ngIf="hasFooter && state.value === slides.length" class="bottom-bar uk-animation-slide-bottom"
             [shortView]="true" [ngClass]="footerClass"
             [showOpenaire]="true" [darkBackground]="false"></bottom>
  `,
  styleUrls: ['full-page-slider.component.css']
})
export class FullPageSliderComponent implements AfterContentInit {
  
  @ContentChildren(SlideComponent) slides: QueryList<SlideComponent>;
  @Input()
  public initSlide = 1;
  @Input()
  public logoURL;
  @Input() topBar: string = null;
  @Input() hasFooter: boolean = null;
  @Input() footerClass: string;
  public animate: boolean = false;
  public state: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  
  ngAfterContentInit() {
    this.state.next(this.initSlide);
    this.setSlides(this.state.value);
  }
  
  setSlides(state = 1) {
    this.slides.forEach((slide, index) => {
      slide.state = state;
      slide.y = -50 + (index + 1) * 200 - state * 200;
    });
  }
  
  onWheel(event) {
    if (!this.animate) {
      this.animate = true;
      if (event.deltaY > 0 && (this.state.value < this.slides.length)) {
        this.state.next(+this.state.value + 1);
        this.setSlides(this.state.value);
        setTimeout(() => {
          this.animate = false;
        }, 500);
      } else if (event.deltaY < 0 && (this.state.value !== 1)) {
        this.state.next(this.state.value - 1);
        this.setSlides(this.state.value);
        setTimeout(() => {
          this.animate = false;
        }, 500);
      } else {
        this.animate = false;
      }
    }
  }
  
  public onClick(index: number) {
    if (!this.animate) {
      this.animate = true;
      this.state.next(index);
      this.setSlides(this.state.value);
      setTimeout(() => {
        this.animate = false;
      }, 500);
    }
  }
  
  public get stateValue() {
    return this.state.value;
  }
  
  public get stateAsObservable(): Observable<number> {
    return this.state.asObservable();
  }
}
