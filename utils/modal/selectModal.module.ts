import { NgModule }            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';

 import {ModalSelect} from './selectModal.component';
import {ModalModule} from './modal.module';

//helpers

@NgModule({
  imports: [ CommonModule, FormsModule,ModalModule  ],
  declarations: [
    ModalSelect
  ],
  exports: [
   ModalSelect
  ]
})
export class SelectModalModule { }
