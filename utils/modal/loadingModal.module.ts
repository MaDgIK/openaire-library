import { NgModule }            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';

 import {ModalLoading} from './loading.component';
import {ModalModule} from './modal.module';
import {LoadingModule} from "../loading/loading.module";

//helpers

@NgModule({
  imports: [CommonModule, FormsModule, ModalModule, LoadingModule],
  declarations: [
    ModalLoading
  ],
  exports: [
   ModalLoading
  ]
})
export class LoadingModalModule { }
