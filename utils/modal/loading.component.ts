import {
  Component,
  ViewEncapsulation,
  ComponentRef,
  ElementRef,
  Input,
  EventEmitter,
  Output,
  ViewChild
} from '@angular/core';
declare var UIkit: any;

@Component({
  selector: 'modal-loading',
  template: `
  <div  #loading_element [class.uk-modal]="!isMobile" [class.uk-modal-full]="isMobile" [id]="id" uk-modal="container: #modal-container; bg-close:false; ">
    <div class="uk-modal-dialog">
      <div *ngIf="isMobile" #header class="uk-modal-header uk-flex uk-flex-middle" [ngClass]="classTitle">
        <div [class.uk-invisible]="!title"
             class="uk-width-expand uk-padding-small uk-padding-remove-vertical uk-flex uk-flex-center">
          <h4 class="uk-margin-remove">{{title}}</h4>
        </div>
      </div>
        <div  #body class="uk-modal-body" [class.uk-overflow-auto]="isMobile">
          <div  [class.uk-container]="isMobile" [class.uk-height-1-1]="isMobile">
          <h4 class="text-center" >{{message}}</h4>
            <loading></loading>
            <ng-content></ng-content>
          </div>
        </div>

      </div>
  </div>
   `,
  encapsulation: ViewEncapsulation.None,
})
/**
  * API to an open alert window.
  */
export class ModalLoading{
  private static MODAL_LOADING_COUNTER: number = 0;

  id: string = "modal-loading";
  title: string;

  @Input() isMobile: boolean = false;
  @Input() classTitle: string = "uk-background-primary uk-light";
  @Input() public message:string ="Loading";
  @ViewChild('loading_element') element: ElementRef;

  /**
    * if the value is true alert will be visible or else it will be hidden.
    */
  // public isOpen:boolean=false;
  /**
    * Emitted when a ok button was clicked
    * or when Ok method is called.
    */
  @Output() public alertOutput:EventEmitter<any> = new EventEmitter();
  constructor( public _elementRef: ElementRef){

  }

  ngOnInit() {
    ModalLoading.MODAL_LOADING_COUNTER++;
    this.id = 'modal-loading-' + ModalLoading.MODAL_LOADING_COUNTER;
  }

  ngOnDestroy() {
    if(typeof document !== "undefined") {
      const element = document.getElementById("modal-container");
      for (let i = element.childNodes.length - 1; i >= 0; --i) {
        let child: ChildNode = element.childNodes[i];
        if (child['id'] == this.id) {
          child.remove();
        }
      }
    }
  }

  /**
       * Opens a alert window creating backdrop.
       */
  open(){
    // this.isOpen= true;
    UIkit.modal(this.element.nativeElement).show();

  }

  close(){
    // this.isOpen = false;
    UIkit.modal(this.element.nativeElement).hide();
  }
}
