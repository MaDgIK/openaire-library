import {Component, ViewEncapsulation, ComponentRef, ElementRef, Input, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'modal-select',
  template: `
    <div [class]="(!isOpen)?'uk-modal ':'uk-modal uk-open uk-animation-fade'" [open]="!isOpen" uk-modal="center:true"
         tabindex="-1" role="dialog">
      <div class="uk-modal-dialog" role="">
        <div class="modal-content">

          <div class="modal-body">
            <div>
              <h4 class="text-center">{{message}}</h4>
              <div class="-select" data--select>
                <span></span>
                <select (change)="selected=$event.target.value">
                  <option *ngFor="let option of options let i=index" value="{{option}}">{{option}}</option>
                </select>
              </div>

              <div class="uk-modal-footer uk-text-right">
                <button class=" uk-button uk-button-default" (click)="close()">Proceed</button>
              </div>
            </div>

          </div>
        </div>
      </div>
      <!--div   class="uk-modal uk-open" aria-hidden="false" style="display: block; overflow-y: scroll;">
                                <div class="uk-modal-dialog" tabindex="">
                                    <div class="uk-modal-spinner"></div>
                                </div>
                            </div-->
  `,
  encapsulation: ViewEncapsulation.None,
})
/**
 * API to an open alert window.
 */
export class ModalSelect {
  
  @Input() public message: string = "Loading";
  @Input() public options: string[] = [];
  
  public selected: string;
  
  /**
   * if the value is true alert will be visible or else it will be hidden.
   */
  public isOpen: boolean = false;
  /**
   * Emitted when a ok button was clicked
   * or when Ok method is called.
   */
  @Output() public alertOutput: EventEmitter<any> = new EventEmitter();
  
  constructor(public _elementRef: ElementRef) {
  }
  
  /**
   * Opens a alert window creating backdrop.
   */
  open() {
    this.isOpen = true;
  }
  
  close() {
    this.isOpen = false;
    if (!this.selected) {
      this.selected = this.options[0];
    }
    this.alertOutput.emit(this.selected);
  }
  
}
