


export class ExportCSVComponent {
    stockData : any =
        {
            "columnNames":
                ["Symbol", "Company", "Price"],
            "export":
/*
                [
                    {
                        Symbol: "AAPL",
                        Company: "Apple Inc.",
                        Price: "132.54"
                    },
                    {
                        Symbol: "INTC",
                        Company: "Intel Corporation",
                        Price: "33.45"
                    },
                    {
                        Symbol: "GOOG",
                        Company: "Google Inc",
                        Price: "554.52"
                    },
                ],
*/

                [
                    [
                        "AAPL",
                        "Apple Inc.",
                        "132.54"
                    ],
                    [
                        "INTC",
                        "Intel Corporation",
                        "33.45"
                    ],
                    [
                        "GOOG",
                        "Google Inc",
                        "554.52"
                    ],
                ],


            "columnDelimiter": ',',
            "lineDelimiter": '\n'
        };

     data: any = this.stockData;
     filename: string;
     linkname: string = "Download CSV";

    constructor () {
    }

    ngOnInit() {
    }

    public static convertArrayOfObjectsToCSV(args) {

        var result, ctr, keys, columnDelimiter, lineDelimiter, data;

        data = args.export || null;

        if (data == null || !data.length) {
            return null;
        }

        columnDelimiter = args.columnDelimiter || ',';
        lineDelimiter = args.lineDelimiter || '\n';

        //keys = Object.keys(data[0]);
        keys = args.columnNames;

        result = '';
        result += keys.join(columnDelimiter);
        result += lineDelimiter;
/*
        data.forEach(function(item) {
            ctr = 0;
            keys.forEach(function(key) {
                if (ctr > 0) result += columnDelimiter;
                result += item[key];
                ctr++;
            });
            result += lineDelimiter;
        });
*/

        for(let line of data) {
            ctr = 0;
            for(let column of line) {
                if (ctr > 0) result += columnDelimiter;
                result += column;
                ctr++;
            }
            result += lineDelimiter;
        }

        return result;
    }

    public static downloadCSV(data: any, filenameArg: string) {

        var encodedData, filename, link;

        var csv = this.convertArrayOfObjectsToCSV(data);
        if (csv == null) return;

        filename = filenameArg || 'export.csv';

        if (!csv.match(/^data:text\/csv/i)) {
            csv = 'data:text/csv;charset=utf-8,' + csv;
        }
        encodedData = encodeURI(csv);

        //link = document.createElement('a');
        link = document.getElementsByTagName('a');
        link[0].setAttribute('href', encodedData);
        link[0].setAttribute('download', filename);
        //document.body.appendChild(link);
        link[0].click();
        //document.body.removeChild(link);
    }
}
