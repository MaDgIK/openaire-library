import {HttpClient} from "@angular/common/http";
import {EnvProperties} from '../properties/env-properties';
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";

export abstract class Log{
  action: "link" | "orcid-link" | "upload-dois";
  message:string;
  protected constructor(action, message){
    this.action =action;
    this.message = message;
  }
}

export class LinkLog extends Log{
  constructor(sourceTitle,targetTitle) {
    super("link","a user linked the \"" +sourceTitle+"\" " +   " to the \"" +targetTitle+"\" " );

  }
}

export class OrcidLinkLog extends Log{
  constructor( action:'added'|'removed', title: string, id: string) {
    super("orcid-link","user with ORCID ID " + action + " research product "+ (title?"\"" + title+ "\"":"") + " (" + id + ") " + (action == 'added'?'to':'from')
      + " their ORCID record.");
  }

}

export class UploadLog extends Log{
  constructor(dois:number) {
    super("upload-dois","a user uploaded a list of " + dois +" DOIs to the Irish Monitor to check their presence and retrieve the Open Access types and additional key metadata");
  }
}

@Injectable()
export class LogService {
  constructor(private http: HttpClient) {
  }

   logUploadDOIs(properties: EnvProperties, dois:number){
     return this.http.post(properties.logServiceUrl+"logAction", new UploadLog(dois) );
   }
  logLink(properties: EnvProperties, sourceTitle,targetTitle){
    return this.http.post(properties.logServiceUrl+"logAction", new LinkLog(sourceTitle, targetTitle) );
  }
  logOrcidLink(properties: EnvProperties,  action:'added'|'removed', title: string, doi: string){
    return this.http.post(properties.logServiceUrl+"logAction", new OrcidLinkLog(action, title, doi) );
  }
  logRemoveOrcidLink(properties: EnvProperties,  code: string){
    return this.http.post(properties.logServiceUrl+"logAction", new OrcidLinkLog('removed',null, code) );
  }
  getLogs(properties: EnvProperties):Observable<any>{
    return this.http.get(properties.logServiceUrl+"log");
  }
}
