import { NgModule }            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';
import {LogService} from "./log.service";



@NgModule({
  imports: [
    CommonModule, FormsModule,

  ],
  declarations: [

  ],
  providers: [ LogService ],
  exports: [

    ]
})
export class LogServiceModule { }
