import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { SharedModule } from '../../../openaireLibrary/shared/shared.module';
import { ConfigurationService } from './configuration.service';


@NgModule({
  imports: [
    SharedModule,
     CommonModule

    ],
  providers:[ ConfigurationService]

})
export class ConfigurationServiceModule {
  static forRoot() {
    return {
      ngModule: ConfigurationServiceModule,
      providers: [ ConfigurationService ]
    }
  }
}
