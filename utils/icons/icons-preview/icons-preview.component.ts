import {Component, OnInit} from "@angular/core";
import {Icon} from "../icons";
import {IconsService} from "../icons.service";

@Component({
  selector: 'icons-preview',
  template: `
    <div class="uk-section uk-container uk-container-large" style="font-size: 14px;">
      <h4 class="uk-text-bold">Icons</h4>
      <div class="uk-grid uk-grid-small uk-child-width-1-6" uk-grid>
        <div *ngFor="let icon of icons" class="uk-text-center">
          <icon [name]="icon.name"></icon>
          <div class="uk-text-bold uk-margin-small-top">{{icon.name}}</div>
        </div>
      </div>
      <div class="uk-margin-medium-top">
        <h4 class="uk-text-bold">Usage</h4>
        <ul class="uk-list">
          <li>1. Import IconsModule</li>
          <li>2. Add this to your module with these icons you will need<br>
            <div class="uk-margin-top">
              <pre><code>constructor(private iconsService: IconsService) {{ "{" }}
   this.iconsService.registerIcons([edit]);
{{ "}" }}</code></pre>
            </div>
          </li>
          <li>3. Use an icon with icon tag. Add parameter ratio to scale this icon. Default size 20x20 (ratio: 1)<br>
            <div class="uk-margin-top">
              <pre><code>{{'<'}}icon name="edit"{{'><'}}/icon{{'>'}}</code></pre>
            </div>
          </li>
          <li>4. Add a color class in parent of icon to give your icon a color.<br>
            <div class="uk-margin-top">
              <pre><code>{{'<'}}div class="uk-text-secondary"{{'>'}}
  {{'<'}}icon name="edit"{{'><'}}/icon{{'>'}}
{{'<'}}/div{{'>'}}</code></pre>
            </div>
          </li>
        </ul>
      </div>
      <div class="uk-margin-medium-top">
        <h4 class="uk-text-bold">Add a new icon</h4>
        <ul class="uk-list">
          <li>1. Go to <a href="https://material.io/resources/icons/?style=baseline">Material Icons</a></li>
          <li>2. Find your icon and download it as svg.</li>
          <li>3. Open svg file with an editor and change width and height to 20</li>
          <li>4. Create an entry on icons file with your new icon.</li>
        </ul>
      </div>
    </div>
  `
})
export class IconsPreviewComponent implements OnInit{
  icons: Icon[] = [];

  constructor(private iconsService: IconsService) {
  }

  ngOnInit() {
    this.icons = this.iconsService.getAll();
  }
}
