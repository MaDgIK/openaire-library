import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {IconsPreviewComponent} from "./icons-preview.component";
import {IconsService} from "../icons.service";
import {IconsModule} from "../icons.module";
import {isDevelopmentGuard} from "../../../error/isDevelopmentGuard.guard";

@NgModule({
  imports: [CommonModule, RouterModule.forChild([
    {path: '', component: IconsPreviewComponent, canActivate: [isDevelopmentGuard]}
  ]), IconsModule],
  declarations: [IconsPreviewComponent],
  exports: [IconsPreviewComponent]
})
export class IconsPreviewModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons(this.iconsService.getAll());
  }
}
