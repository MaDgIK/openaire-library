import { NgModule }            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';

import {ErrorMessagesComponent} from './errorMessages.component';
import {LoadingModule} from "./loading/loading.module";


@NgModule({
  imports: [
    CommonModule, FormsModule, LoadingModule
  ],
  declarations: [
     ErrorMessagesComponent
  ],
  exports: [
      ErrorMessagesComponent
    ]
})
export class ErrorMessagesModule { }
