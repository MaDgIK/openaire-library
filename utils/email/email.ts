export class Email {
  body: string;
  subject: string;
  recipients?: string[];
  recipient?: string;
}
