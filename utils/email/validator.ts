import {Email} from "./email";

export class Validator {

  //private static regex = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  private static regex = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/;
  /*
  private static regex2= /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  // for expanation paste it in https://www.regextester.com/
  // RFC5322 https://emailregex.com/
  */

  public static hasValidEmails(data: any): boolean {
    for(let i = 0; i < data.length; i++) {
      if (!this.emailValidator(data[i])) {
        return false;
      }
    }
    return true;
  }

  public static emailValidator(email: any): boolean {
    return !!email.match(this.regex);
  }

}
