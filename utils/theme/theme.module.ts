import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {ThemeRoutingModule} from "./theme-routing.module";
import {ThemeComponent} from "./theme.component";
import {IconsModule} from "../icons/icons.module";
import {InputModule} from "../../sharedComponents/input/input.module";
import {SliderUtilsModule} from "../../sharedComponents/slider-utils/slider-utils.module";

@NgModule({
  imports: [
    CommonModule, RouterModule,
    ThemeRoutingModule, IconsModule, InputModule, SliderUtilsModule
  ],
  declarations: [
    ThemeComponent
  ],
  exports: [
    ThemeComponent
  ]
})
export class ThemeModule {}
