import {Component, OnDestroy, OnInit} from '@angular/core';
import {ValidatorFn, Validators} from "@angular/forms";
import {StringUtils} from "../string-utils.class";
import {ActivatedRoute} from "@angular/router";
import {Subscriber} from "rxjs";
import {OpenaireEntities} from "../properties/searchFields";


@Component({
  selector: 'theme',
  templateUrl: 'theme.component.html',
})

export class ThemeComponent implements OnInit, OnDestroy {
  url: string = 'https://example.com';
  logoURL: string = 'https://example.com/test.png';
  parentClass: string;
  private subscriptions: any[] = [];
  required: ValidatorFn = Validators.required;
  urlValidator: ValidatorFn = StringUtils.urlValidator;
  
  constructor(private route: ActivatedRoute) {
  }
  
  ngOnInit() {
    this.subscriptions.push(this.route.queryParams.subscribe(params => {
      if(params['parentClass']) {
        this.parentClass = params['parentClass'];
      }
    }));
  }
  
  ngOnDestroy() {
    this.subscriptions.forEach(value => {
      if (value instanceof Subscriber) {
        value.unsubscribe();
      }
    });
  }

  protected readonly OpenaireEntities = OpenaireEntities;
}
