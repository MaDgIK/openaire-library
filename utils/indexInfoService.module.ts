import { NgModule }            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';

import {IndexInfoService} from './indexInfo.service';


@NgModule({
  imports: [
    CommonModule, FormsModule,

  ],
  declarations: [

  ],
  providers: [ IndexInfoService ],
  exports: [

    ]
})
export class IndexInfoServiceModule{ }
