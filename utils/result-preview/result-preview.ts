import {SearchResult} from "../entities/searchResult";
import {Measure, Metric, ResultLandingInfo} from "../entities/resultLandingInfo";
import {OrganizationInfo} from "../entities/organizationInfo";

export interface HostedByCollectedFrom {
  downloadNames: string[];
  downloadUrl: string;
  collectedNamesAndIds: Map<string, string>;
  accessRight: string;
  types: string[];
  years: string[];
  accessRightIcon: string;
  license?: string;
  fulltext?: string;
  peerReviewed?: boolean;
}

export interface Journal {
  journal?: string;
  issn?: string;
  lissn: string;
  eissn?: string;
  issue?: string;
  volume?: string;
  start_page?: string;
  end_page?: string;
}

export interface RelationResult {
  name: string;
  id: string;
  date: string;
  percentage: number;
  percentageName?: string;
  class: string
  provenanceAction?: string;
  relationName?: string;
}

export interface RelationDatasource {
  name: string;
  id: string;
  percentage: number;
  percentageName?: string;
  class: string
  provenanceAction?: string;
  relationName?: string;
  openaireCompatibility: string;
}

export interface Project {
  id: string;
  acronym: string;
  title: string;
  funderShortname: string;
  funderName: string;
  funderJurisdiction?: string
  funding?: string;
  code: string;
  validated?: boolean;
  budget?: string;
  contribution?: string;
  currency?: string;
  provenanceAction?: string;
}

export interface Author {
  fullName: string;
  orcid: string;
  orcid_pending: string;
}

export interface ResultTitle {
  name: string;
  accessMode: string;
  // sc39: string;
}

export interface Organization {
  id: string;
  name: string;
  shortname?: string;
  websiteUrl?: string;
  country?: string;
  trust?: number;
}

export interface OARoutes {
  green: boolean;
  oaColor: "gold" | "hybrid" | "bronze";
  isInDiamondJournal: boolean;
}

export class ResultPreview {
  objId: string;
  relcanId: string;
  id: string;
  title: string;
  accessMode: string;
  // sc39: string;
  countries: string[];
  date: string;
  dateofacceptance: Date;
  journal: Journal;

  //Impact factor
  DOI: string;
  measure: Measure;

  //publications & datasets & orp & software & organizations:
  projects: Project[];

  //datasets & orp & publications & software
  description: string;
  year: string;
  embargoEndDate: Date | string;
  authors: Author[];
  languages: string[];
  identifiers: Map<string, string[]>;     //key is the classname
  hostedBy_collectedFrom: HostedByCollectedFrom[];
  orcidPutCodes: string[];
  orcidUpdateDates: string [];
  orcidCreationDates: string [];

  //datasets & orp & software:
  publisher: string;

  //software
  programmingLanguages: string[];

  //dataproviders & projects:
  organizations: Organization[];

  //projects:
  acronym: string;
  code: string;
  // callIdentifier: string;  // currently not parsed
  funderName: string;
  funderShortname: string;
  budget: string;
  contribution: string;
  currency: string;
  startYear: number;
  endYear: number;
  openAccessMandatePublications: boolean;
  openAccessMandateDatasets: boolean;

  //organizations:
  country: string;

  //dataproviders:
  englishname: string;
  websiteURL: string;
  OAIPMHURL: string;
  compatibility: string;
  compatibilityUNKNOWN: boolean;
  subjects: string[];

  resultType: string;
  types: string[];

  // Relation result
  relationName: string;
  relation: string;
  percentage: number;
  provenanceAction: string;

  //enermaps id - image
  enermapsId: string;

  oaRoutes: OARoutes;
  publiclyFunded: boolean;

  public static searchResultConvert(result: SearchResult, type: string): ResultPreview {
    let resultPreview: ResultPreview = new ResultPreview();
    resultPreview.id = result.id;
    resultPreview.relcanId = result.relcanId;
    resultPreview.objId = result.objId;
    resultPreview.title = result.title.name;
    resultPreview.accessMode = result.title.accessMode;
    // resultPreview.sc39 = result.title.sc39;
    if (result.countriesForResults) {
      resultPreview.countries = result.countriesForResults;
    } else if (result.country) {
      resultPreview.countries = [result.country];
    } else {
      resultPreview.countries = result.countries;
    }
    resultPreview.projects = result.projects;
    resultPreview.description = result.description;
    resultPreview.year = result.year;
    resultPreview.embargoEndDate = result.embargoEndDate;
    resultPreview.date = result.date;
    resultPreview.dateofacceptance = result.dateofacceptance;
    resultPreview.journal = result.journal;
    resultPreview.authors = result.authors;
    resultPreview.languages = result.languages;
    resultPreview.publisher = result.publisher;
    resultPreview.programmingLanguages = result.programmingLanguages;
    resultPreview.organizations = result.organizations;
    resultPreview.acronym = result.acronym;
    resultPreview.code = result.code;
    // resultPreview.callIdentifier = result.callIdentifier;  // currently not parsed
    resultPreview.funderName = result.funderName;
    resultPreview.funderShortname = result.funderShortname;
    resultPreview.budget = result.budget;
    resultPreview.contribution = result.contribution;
    resultPreview.currency = result.currency;
    resultPreview.startYear = result.startYear;
    resultPreview.endYear = result.endYear;
    resultPreview.openAccessMandatePublications = result.openAccessMandatePublications;
    resultPreview.openAccessMandateDatasets = result.openAccessMandateDatasets;
    resultPreview.englishname = result.englishname;
    if (result.type) {
      resultPreview.types = [result.type];
    } else {
      resultPreview.types = result.types;
    }
    resultPreview.websiteURL = result.websiteURL;
    resultPreview.OAIPMHURL = result.OAIPMHURL;
    resultPreview.compatibility = result.compatibility;
    resultPreview.compatibilityUNKNOWN = result.compatibilityUNKNOWN;
    resultPreview.subjects = result.subjects;
    resultPreview.resultType = type;
    //impact factor;
    // if(result.DOIs && result.DOIs.length > 0) {
    //   resultPreview.identifiers = new Map();
    //   resultPreview.identifiers.set("doi", result.DOIs);
    // }
    resultPreview.identifiers = result.identifiers;
    resultPreview.enermapsId = result.enermapsId;
    resultPreview.measure = result.measure;
    resultPreview.hostedBy_collectedFrom = result.hostedBy_collectedFrom;

    resultPreview.oaRoutes = result.oaRoutes;
    resultPreview.publiclyFunded = result.publiclyFunded;
    return resultPreview;
  }

  public static resultLandingInfoConvert(result: ResultLandingInfo, type: string): ResultPreview {
    let resultPreview: ResultPreview = new ResultPreview();
    resultPreview.title = result.title;
    resultPreview.accessMode = result.accessMode;
    resultPreview.countries = result.countries;
    resultPreview.projects = result.fundedByProjects;
    resultPreview.description = result.description;//.length > 0 ? result.description[0] : "";
    if (result.dateofacceptance) {
      resultPreview.year = new Date(result.dateofacceptance).getFullYear().toString();
    }
    resultPreview.embargoEndDate = result.embargoEndDate;
    resultPreview.date = result.date;
    resultPreview.dateofacceptance = result.dateofacceptance;
    resultPreview.journal = result.journal;
    resultPreview.authors = result.authors;
    resultPreview.languages = result.languages;
    resultPreview.publisher = result.publisher;
    resultPreview.programmingLanguages = result.programmingLanguages;
    resultPreview.organizations = result.organizations;
    resultPreview.types = result.types;
    resultPreview.subjects = result.subjects;
    resultPreview.resultType = type;
    resultPreview.identifiers = result.identifiers;
    resultPreview.hostedBy_collectedFrom = result.hostedBy_collectedFrom;

    resultPreview.oaRoutes = result.oaRoutes;
    resultPreview.publiclyFunded = result.publiclyFunded;
    return resultPreview;
  }

  public static relationResultConvert(result: RelationResult): ResultPreview {
    let resultPreview: ResultPreview = new ResultPreview();
    resultPreview.id = result.id;
    resultPreview.title = result.name;
    resultPreview.resultType = result.class;
    if (result.date) {
      resultPreview.year = result.date.toString();
    }
    resultPreview.relationName = result.relationName;
    resultPreview.relation = result.percentageName;
    resultPreview.percentage = result.percentage;
    resultPreview.provenanceAction = result.provenanceAction;
    return resultPreview;
  }

  public static relationDatasourceConvert(result: RelationDatasource): ResultPreview {
    let resultPreview: ResultPreview = new ResultPreview();
    resultPreview.id = result.id;
    resultPreview.title = result.name;
    resultPreview.resultType = "dataprovider";
    resultPreview.types = [result.class];
    resultPreview.relationName = result.relationName;
    resultPreview.relation = result.percentageName;
    resultPreview.percentage = result.percentage;
    resultPreview.provenanceAction = result.provenanceAction;
    resultPreview.compatibility = result.openaireCompatibility;
    return resultPreview;
  }

  public static organizationConvert(result: Organization, relation: string = 'trust'): ResultPreview {
    let resultPreview: ResultPreview = new ResultPreview();
    resultPreview.id = result.id;
    if (result.name) {
      resultPreview.title = result.name;
      if (result.shortname) {
        resultPreview.title += ' (' + result.shortname + ')';
      }
    } else if (result.shortname) {
      resultPreview.title = result.shortname;
    }
    if (result.country) {
      resultPreview.countries = [result.country];
    }
    resultPreview.relation = relation;
    resultPreview.percentage = result.trust;
    resultPreview.websiteURL = result.websiteUrl;
    resultPreview.resultType = 'organization';
    return resultPreview;
  }

  public static organizationInfoConvert(result: OrganizationInfo): ResultPreview {
    let resultPreview: ResultPreview = new ResultPreview();
    if (result.title && result.title.name) {
      resultPreview.title = result.title.name;
      if (result.name) {
        resultPreview.title += ' (' + result.name + ')';
      }
    } else if (result.name) {
      resultPreview.title = result.name;
    }
    if (result.country) {
      resultPreview.countries = [result.country];
    }
    if (result.title && result.title.url) {
      resultPreview.websiteURL = result.title.url;
    }
    resultPreview.resultType = 'organization';
    return resultPreview;
  }

}
