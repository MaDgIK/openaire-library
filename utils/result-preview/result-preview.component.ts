import {ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, SimpleChanges} from "@angular/core";
import {ResultPreview} from "./result-preview";
import {EnvProperties} from "../properties/env-properties";
import {RouterHelper} from "../routerHelper.class";
import {AlertModal} from "../modal/alert";
import {properties} from "../../../../environments/environment";
import {Identifier, StringUtils} from "../string-utils.class";
import {OpenaireEntities} from "../properties/searchFields";
import {HelperFunctions} from "../HelperFunctions.class";
import {NumberUtils} from '../number-utils.class';

@Component({
  selector: 'result-preview',
  templateUrl: 'result-preview.component.html',
  styleUrls: ['result-preview.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResultPreviewComponent implements OnInit, OnChanges {
  @Input() prevPath: string = "";
  @Input() result: ResultPreview;
  @Input() properties: EnvProperties;
  public openaireEntities = OpenaireEntities;
  @Input() showSubjects: boolean = true;
  @Input() showOrganizations: boolean = true;
  @Input() modal: AlertModal = null;
  @Input() promoteWebsiteURL: boolean = false;
  @Input() hasLink: boolean = true;
  /**
   * @deprecated
   * */
  @Input() isCard: boolean = false;
  @Input() isMobile: boolean = false;
  @Input() modifier: 'uk-card-default' | null = null;
  public routerHelper: RouterHelper = new RouterHelper();
  public urlParam: string;
  public url: string;
  @Input() customUrl: string;
  @Input() externalUrl: string;
  @Input() showOrcid: boolean = true;
  @Input() showEnermaps: boolean = false;
  @Input() provenanceActionVocabulary = null;
  @Input() relationsVocabulary = null;
  @Input() showInline: boolean = false; // do not open modal for "view more" when this is true
  @Input() isDeletedByInferenceModal: boolean = false; // do not show action bar in results when in "Other versions" modal section
  @Input() showEntityActions: boolean = true; // we can use this instead of the above as it is more generic
  @Input() compactView: boolean = false; // if true, show less info (e.g. hide description) on each result

  /* Metadata */
  public type: string;
  public types: string[];
  public provenanceAction: string;
  public relationName: string;

  public linking: boolean = false;
  public share: boolean = false;
  public cite: boolean = false;
  public orcid: boolean = false;
  public deposit: boolean = false;
  public embed: boolean = false;

  ngOnInit(): void {
    if(this.isMobile) {
      this.modifier = "uk-card-default";
    }
    if (this.hasLink) {
      if (this.result.resultType === "publication") {
        this.urlParam = "articleId";
        this.url = properties.searchLinkToPublication.split('?')[0];
        this.resultActions();
      } else if (this.result.resultType === "dataset") {
        this.urlParam = "datasetId";
        this.url = properties.searchLinkToDataset.split('?')[0];
        this.resultActions();
      } else if (this.result.resultType === "software") {
        this.urlParam = "softwareId";
        this.url = properties.searchLinkToSoftwareLanding.split('?')[0];
        this.resultActions();
      } else if (this.result.resultType === "other") {
        this.urlParam = "orpId";
        this.url = properties.searchLinkToOrp.split('?')[0];
        this.resultActions();
      } else if (this.result.resultType == "project") {
        if (this.result.id) {
          this.urlParam = "projectId";
        } else if (this.result.code && this.result.funderShortname) {
          this.result.id = this.result.code;
          this.urlParam = "grantId";
        }
        this.url = properties.searchLinkToProject.split('?')[0];
        if(this.result.id == "-1"){
          //community projetcs - not yet loaded in the graph
          this.url = null;
          this.result.id = null;
        }
        this.projectActions();
      } else if (this.result.resultType == "organization") {
        this.urlParam = "organizationId";
        this.url = properties.searchLinkToOrganization.split('?')[0];
        this.organizationActions();
      } else if (this.result.resultType == "dataprovider") {
        if (this.result.compatibilityUNKNOWN && properties.adminToolsPortalType == "eosc") {
          this.urlParam = "serviceId";
          this.url = properties.searchLinkToService.split('?')[0];
        } else {
          this.urlParam = "datasourceId";
          this.url = properties.searchLinkToDataProvider.split('?')[0];
        }
        this.contentProviderActions();
      } else if (this.result.resultType == "service") {
        this.urlParam = "serviceId";
        this.url = properties.searchLinkToService.split('?')[0];
      } else {
        this.urlParam = "id";
        this.url = properties.searchLinkToResult.split('?')[0];
      }
      this.checkPID();
    }
    this.initMetadata();
    if (this.result.languages) {
      this.result.languages = this.removeUnknown(this.result.languages);
    }
    if (this.result.countries) {
      this.result.countries = this.removeUnknown(this.result.countries);
    }
  }

  resultActions() {
    this.linking = true;
    this.share = true;
    this.cite = true;
    this.orcid = (this.properties.adminToolsPortalType == 'explore' || this.properties.adminToolsPortalType == 'community' || this.properties.adminToolsPortalType == 'aggregator' || this.properties.dashboard == 'irish') &&
      // this.showOrcid && ((this.result.identifiers && this.result.identifiers.size > 0) || properties.environment == 'development');
      this.showOrcid;
  }
  
  projectActions() {
    this.linking = true;
    this.share = true;
    this.deposit = true;
    this.embed = true;
  }
  
  organizationActions() {
    this.share = true;
  }
  
  contentProviderActions() {
    this.share = true;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.result && this.hasLink) {
      this.checkPID();
    }
  }

  checkPID() {
    // if result has a pid use it as parameter instead of openaireId
    let pid: Identifier = this.getPID();
    if (pid) {
      this.urlParam = "pid";
      this.result.id = pid.id;

    }
  }

  getPID() {
    return Identifier.getPIDFromIdentifiers(this.result.identifiers);
  }

  public initMetadata() {
    if (this.result.resultType && this.result.resultType !== 'dataprovider') {
      this.type = this.getTypeName(this.result.resultType);
    }
    if (this.result.types) {
      this.types = this.removeUnknown(this.removeDuplicates(this.result.types));
    }
    if (this.result.provenanceAction) {
      this.provenanceAction = HelperFunctions.getVocabularyLabel(this.result.provenanceAction, this.provenanceActionVocabulary, false);
    }
    if (this.result.relationName) {
      this.relationName = HelperFunctions.getVocabularyLabel(this.result.relationName, this.relationsVocabulary);
    }
    // if(this.result.percentage) {
    //   this.beforeTitle.push((this.result.relation ? this.result.relation+": " : "") + this.result.percentage.toString() + "%");
    // }
  }

  get hasActions() {
    return this.linking || this.share || this.cite || this.orcid;
  }

  public getTypeName(type: string): string {
    return StringUtils.getEntityName(type, false);
  }

  public removeUnknown(array: string[]): string[] {
    return array.filter(value => value.toLowerCase() !== 'unknown');
  }

  public removeDuplicates(array: string[]): string[] {
    return array.filter(value => value.toLowerCase() !== this.result.resultType);
  }

  public accessClass(accessMode: string): string {
    if (accessMode.toLowerCase().indexOf('open') !== -1) {
      return 'success';
    } else if (accessMode.toLowerCase() === 'not available') {
      return 'disabled'; // unknown
    } else {
      return 'disabled'; // closed
    }
  }

  public onClick() {
    if (this.modal) {
      this.modal.cancel();
    }
  }

  createParam() {
    if (this.urlParam == "grantId") {
      return this.routerHelper.createQueryParams([this.urlParam, "funder"], [this.result.id, this.result.funderShortname])

    }
    return this.routerHelper.createQueryParam(this.urlParam, this.result.id)

  }

  public get isResultType() {
    return this.result.resultType == "publication" || this.result.resultType == "dataset" ||
        this.result.resultType == "software" || this.result.resultType == "other" || this.result.resultType == "result";
  }

  public formatNumber(num: number | string) {
    let formatted = NumberUtils.roundNumber(+num);
    return formatted.number + formatted.size;
  }

  public isNumber(value): boolean {
    return typeof value === 'number';
  }

  public getAccessLabel(accessRight) : string {
    if(accessRight) {
      return (accessRight + (accessRight.toLowerCase().endsWith(" access") ? "" : " access"));
    }
    return "Not available access";
  }

  public addEoscPrevInParams(obj) {
    if(properties.adminToolsPortalType == "eosc" && this.prevPath) {
      let splitted: string[] = this.prevPath.split("?");
      obj = this.routerHelper.addQueryParam("return_path", splitted[0], obj);
      if(splitted.length > 0) {
        obj = this.routerHelper.addQueryParam("search_params", splitted[1], obj);
      }
    }
    return obj;
  }

  public get countDots() {
    return (this.result.oaRoutes.green ? 1 : 0) + (this.result.oaRoutes.oaColor ? 1 : 0) + (this.result.oaRoutes.isInDiamondJournal ? 1 : 0);
  }
}
