import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {ResultPreviewComponent} from "./result-preview.component";
import {RouterModule} from "@angular/router";
import {ShowAuthorsModule} from "../authors/showAuthors.module";
import {ResultLandingUtilsModule} from "../../landingPages/landing-utils/resultLandingUtils.module";
import {IconsModule} from "../icons/icons.module";
import {IconsService} from "../icons/icons.service";
import {cite, fire, landmark, link, link_to, quotes, rocket} from "../icons/icons";
import {EntityActionsModule} from "../entity-actions/entity-actions.module";
import {EntityMetadataModule} from "../../landingPages/landing-utils/entity-metadata.module";
import {OrcidCoreModule} from "../../orcid/orcid-core.module";

@NgModule({
  imports: [CommonModule, RouterModule, ShowAuthorsModule, ResultLandingUtilsModule, OrcidCoreModule, IconsModule, EntityActionsModule, EntityMetadataModule],
  declarations: [ResultPreviewComponent],
  exports: [ResultPreviewComponent]
})
export class ResultPreviewModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([link, quotes, cite, link_to, rocket, fire, landmark])
  }
}
