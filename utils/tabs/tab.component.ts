/**
 * A single tab page. It renders the passed template
 * via the @Input properties by using the ngTemplateOutlet
 * and ngTemplateOutletContext directives.
 */

import { Component, Input } from '@angular/core';

export interface TabIcon {
  name?: string;
	svg?: string;
	ratio?: number;
  active: string;
}

@Component({
  selector: 'my-tab',
  template: `
    <div>
      <ng-content></ng-content>
    </div>
  `
})
export class TabComponent {
  @Input('tabTitle') title: string;
  @Input('tabNumber') num: number;
  @Input('customClass') customClass:string = "";
  @Input('tabId') tabId: string;
	@Input('tabIcon') tabIcon: TabIcon;
	@Input ('active') active: boolean = false;
  @Input ('disabled') disabled: boolean = false;
}
