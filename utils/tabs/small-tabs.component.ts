/**
 * The main component that renders single TabComponent
 * instances.
 */

import {
  Component,
  ContentChildren,
  QueryList,
  AfterContentInit, Output, EventEmitter, ViewChild, ElementRef,
} from '@angular/core';

import { TabComponent } from './tab.component';

declare var UIkit: any;

@Component({
  selector: 'my-small-tabs',
  template: `
    <ng-container *ngIf="tabs.toArray().length > 2">
      <ul class="uk-hidden@m uk-tab main-tabs uk-margin-remove uk-child-width-expand">
  <!--      *ngFor="let activeTab of activeTabs"-->
        <li [class]="'uk-active uk-padding-remove '+activeTab.customClass">
          <a class="uk-width-1-1 uk-height-1-1">
            <div class="tab-header">{{activeTab.title}}</div>
            <div *ngIf="activeTab.num" class="number">{{activeTab.num | number}}</div>
            <div *ngIf="activeTab.customClass == 'statistics'" class="number">
              <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24">
                <path d="M0 0h24v24H0z" fill="none"></path>
                <path d="M10 20h4V4h-4v16zm-6 0h4v-8H4v8zM16 9v11h4V9h-4z"></path>
              </svg>
            </div>
          </a>
        </li>
        <li class="uk-padding-remove fake-tab" uk-tooltip="title: More tabs">
          <a class="uk-text-center uk-width-1-1 uk-height-1-1 uk-icon">
            <svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="more">
              <circle cx="3" cy="10" r="2"></circle>
              <circle cx="10" cy="10" r="2"></circle>
              <circle cx="17" cy="10" r="2"></circle>
            </svg>
          </a>
          <div #drop_element uk-drop="mode: click" class="uk-drop">
            <div class="uk-card uk-card-body uk-card-default">
              <ul class="uk-list main-small-tabs" uk-switcher="connect: .small-tabs-content">
                <ng-container *ngFor="let tab of tabs.toArray()">
                  <li (click)="selectTab(tab)" 
                      [class]="'uk-margin-top uk-margin-bottom uk-height-1-1 uk-width-1-1 ' + (tab == activeTab ? 'uk-hidden' : '')">
                    <a class="uk-display-block uk-height-1-1 uk-width-1-1">{{tab.title}}</a>
                  </li>
                </ng-container>
              </ul>
            </div>
          </div>
        </li>
      </ul>
      
      <div class="uk-hidden@m uk-switcher small-tabs-content main-tabs-content">
        <ng-content></ng-content>
      </div>
    </ng-container>
    
  `
})
export class SmallTabsComponent implements AfterContentInit {
  @ViewChild('drop_element') dropElement: ElementRef;

  @ContentChildren(TabComponent) tabs: QueryList<TabComponent>;
  @Output() public selectedActiveTab: EventEmitter<any> = new EventEmitter();
  public activeTab: TabComponent = null;

  // contentChildren are set
  ngAfterContentInit() {
    // if there is no active tab set, activate the first
    if(this.tabs && this.tabs.length > 0) {
      this.selectTab(this.tabs.first);
    }
  }

  selectTab(tab: TabComponent){
    this.activeTab = tab;

    // activate the tab the user has clicked on.
    this.selectedActiveTab.emit(tab.tabId);

    if(typeof document !== 'undefined' && this.dropElement) {
      UIkit.drop(this.dropElement.nativeElement).hide(false);
    }
  }
}
