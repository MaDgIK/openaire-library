import { NgModule }            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';

import {TabsComponent} from './tabs.component';
import {TabComponent} from "./tab.component";
import {SmallTabsComponent} from "./small-tabs.component";
import {IconsModule} from '../icons/icons.module';

@NgModule({
  imports: [
    CommonModule, FormsModule, IconsModule
  ],
  declarations: [
    TabsComponent, TabComponent, SmallTabsComponent
  ],
  exports: [
    TabsComponent, TabComponent, SmallTabsComponent
  ]
})
export class TabsModule { }
