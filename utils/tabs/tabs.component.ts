/**
 * The main component that renders single TabComponent
 * instances.
 */
import {
  AfterContentInit,
  Component,
  ContentChildren,
  EventEmitter,
  HostListener, Input,
  Output,
  QueryList,
} from '@angular/core';
import {TabComponent} from './tab.component';

@Component({
  selector: 'my-tabs',
  template: `    
    <div id="mytabs" class="uk-grid uk-margin-top landing-tab uk-margin-remove-left">
      <div class="uk-margin-medium-right uk uk-padding-remove uk-width-expand" >
        <div class=" uk-width-1-1" uk-slider="finite: true">
          <div    class="uk-position-relative " >
            <div class="uk-slider-container ">
              <ul class=" uk-slider-items " style="flex-wrap: nowrap !important;" >
                <ng-container *ngFor="let tab of tabs.toArray(); let i=index">
                  <ng-container *ngIf="!tab.customClass">
                    <li  [ngClass]="tab.customClass" (click)="selectTab(tab)" >
                      <ul class="uk-tab">
                        <li [class.uk-active]="tab.active" [class.uk-disabled]="tab.disabled">
                          <a class="uk-width-1-1 uk-height-1-1 uk-flex uk-flex-center" [ngClass]="tab.tabIcon ? 'uk-flex-column' : ''">
                            <icon *ngIf="tab.tabIcon" [svg]="tab.tabIcon.svg" [ratio]="tab.tabIcon.ratio?tab.tabIcon.ratio:1" class="uk-margin-small-bottom"
                                  [ngClass]="(selected === tab.tabId)?tab.tabIcon.active:null"></icon>
                            <div>{{tab.title}}</div>
                            <div *ngIf="tab.num && !tab.disabled" class="">({{tab.num | number}})</div>
                            <div *ngIf="tab.disabled" class="">(-)</div>
                          </a>
                        </li>
                      </ul>
                    </li>
                  </ng-container>
                </ng-container>
              </ul>
            </div>
            <div class="">
              <a class="uk-position-center-left uk-blur-background" uk-slider-item="previous"><span uk-icon="chevron-left"></span></a>
              <a class="uk-position-center-right uk-blur-background" uk-slider-item="next"><span uk-icon="chevron-right"></span></a>
            </div>
          </div>
        </div>
      </div>
      <ng-container *ngFor="let tab of tabs.toArray(); let i=index">
        <ng-container *ngIf="tab.customClass">
          <div class="uk-width-small">
            <ul class="uk-tab uk-height-1-1" [class.uk-flex-right]="tab.tabId == 'statistics' || 'recommended'">
              <li  [ngClass]="tab.customClass" (click)="selectTab(tab)" [class.uk-active]="tab.active" >
                <a class="uk-width-1-1 uk-height-1-1 uk-flex uk-flex-center featuredTab" [ngClass]="tab.tabIcon ? 'uk-flex-column' : ''">
                  <!--<icon *ngIf="tab.tabIcon" [svg]="tab.tabIcon.svg" [ratio]="tab.tabIcon.ratio?tab.tabIcon.ratio:1" class="uk-margin-small-bottom"
                        [ngClass]="(selected === tab.tabId)?tab.tabIcon.active:null"></icon>-->
                    <span *ngIf="tab.tabId=='statistics'" class="material-icons">bar_chart</span>
                    <icon *ngIf="tab.tabId=='recommended'" [name]="'tips_and_updates'" [flex]="true"></icon>
                  <div>{{tab.title}}</div>
                  <div *ngIf="tab.num" class="number">{{tab.num | number}}</div>
                </a>
              </li>
            </ul>
          </div>
        </ng-container>
      </ng-container>
    </div>
  `
})
export class TabsComponent implements AfterContentInit {
  
  public customClass: string;
  @ContentChildren(TabComponent) tabs: QueryList<TabComponent>;
  @Output() public selectedActiveTab: EventEmitter<any> = new EventEmitter();
  @Input() offsetForSticky:number=0;
  @Input() isSticky:boolean;
  @Output() isStickyChange :EventEmitter<boolean> = new EventEmitter();
  public selected: string;
  disableScroll = false;
  @HostListener("window:scroll", [])
  onWindowScroll() {
    this.scroll();
  }
  ngAfterContentInit() {
    if(this.tabs.length > 0) {
      this.selected = this.tabs.get(0).tabId;
    }
  }
  
  selectTab(tab: TabComponent, scroll=true){
    this.unSelectTab(this.selected, tab.tabId);
    tab.active = true;
    this.selected = tab.tabId;
    this.selectedActiveTab.emit(tab.tabId);
    if(scroll) {
      this.disableScroll = true;
      setTimeout(() => {
        window.scrollTo({
          top: document.getElementById(tab.tabId) ? document.getElementById(tab.tabId).offsetTop - (this.isSticky?250:180) : 250,
          behavior: 'smooth'
        });
        setTimeout(() => {
        this.disableScroll = false;
        }, 600);
      }, 200);
    }
  }
  scroll(){
    let tabDistanceFromCurrentViewTop = document.getElementById("main-tabs-div") ? document.getElementById("main-tabs-div").getBoundingClientRect().top : null;
    if((tabDistanceFromCurrentViewTop <= this.offsetForSticky)){
      this.isStickyChange.emit(true);
      this.isSticky =true;
    }else if(!(tabDistanceFromCurrentViewTop <= this.offsetForSticky)){
      this.isStickyChange.emit(false);
      this.isSticky =false;
    }
    if(this.disableScroll){
      return;
    }
    // console.log(window.scrollY)
    let currentTabView = null;
    let windowInnerHeight = window && window.innerHeight ? window.innerHeight : 300;
    // console.log("find CT", windowInnerHeight)
    for (let tab of this.tabs) {
      
        let distanceFromCurrentViewTop = document.getElementById(tab.tabId) ? document.getElementById(tab.tabId).getBoundingClientRect().top : null;
        // console.log(pos, distanceFromCurrentViewTop,  windowInnerHeight/2);
        if (distanceFromCurrentViewTop != null && distanceFromCurrentViewTop <= windowInnerHeight / 2) {
          currentTabView = tab;
        
        } else if (distanceFromCurrentViewTop != null && distanceFromCurrentViewTop > windowInnerHeight) {
          break;
        }
      }
    // console.log("current currentTabView", currentTabView.tabId)
    if (currentTabView && this.selected != currentTabView.tabId) {
      this.selectTab(currentTabView, false);
      this.disableScroll =true;
      setTimeout(() => {
        this.disableScroll = false;
      }, 600);
    }
  }
  unSelectTab(oldTabId, newTabId){
    for (let tab of this.tabs) {
      if(tab.tabId == oldTabId){
        tab.active = false;
        break;
      }
    }
    this.selected = newTabId;
  }
}
