import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {ErrorMessagesModule} from "../../errorMessages.module";
import {SearchTabComponent} from "./search-tab.component";
import {SearchResultsModule} from "../../../searchPages/searchUtils/searchResults.module";

@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule,
    ErrorMessagesModule, SearchResultsModule
  ],
  declarations: [
    SearchTabComponent
  ],
  providers:[],
  exports: [
    SearchTabComponent
  ]
})
export class SearchTabModule { }
