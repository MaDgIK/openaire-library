import {Component, Input, SimpleChanges} from '@angular/core';
import {EnvProperties} from "../../properties/env-properties";
import {ErrorCodes} from "../../properties/errorCodes";
import {OpenaireEntities} from "../../properties/searchFields";
import {StringUtils} from "../../string-utils.class";

@Component({
    selector: 'search-tab',
    template: `
			<a *ngIf="properties.adminToolsPortalType !== 'eosc' && fetch.searchUtils.status == errorCodes.LOADING" class="uk-invisible" 
				[queryParams]="params" [routerLink]="searchLinkToAdvancedPage"></a>
			
			<ng-container> <!-- *ngIf="fetch.searchUtils.status != errorCodes.LOADING" -->
				<div class="uk-flex uk-flex-between uk-flex-middle uk-margin-bottom" [class.uk-flex-column]="isMobile">
					<div class="uk-text-meta uk-text-large uk-text-uppercase">
						<span *ngIf="!customTitle && resultType"><span *ngIf="resultType != 'organization' && resultType != 'dataprovider' && resultType != 'project'">Recent</span>
							{{getEntityName(resultType)}}
						</span>
						<span *ngIf="customTitle">{{customTitle}}</span>
					</div>
					<div *ngIf="searchLinkToAdvancedPage">
						<a *ngIf="properties.adminToolsPortalType !== 'eosc'" class="el-content uk-button uk-button-text" [queryParams]="params" [routerLink]="searchLinkToAdvancedPage">
							View all
							<span *ngIf="fetch.searchUtils.totalResults <= searchNumber">in search page</span>
						</a>
            <a *ngIf="properties.adminToolsPortalType == 'eosc'" class="el-content uk-button uk-button-text custom-external" 
               [href]="'https://explore.openaire.eu'+searchLinkToAdvancedPage+paramsForExternalUrl" target="_blank">
              View all in OpenAIRE
            </a>
					</div>
				</div>

				<ng-content></ng-content>
				
				<div *ngIf="resultType && fetch.searchUtils.status != errorCodes.DONE" class="uk-margin-xlarge-top">
					<errorMessages [status]="[fetch.searchUtils.status]" [type]="getEntityName(resultType)"
												tab_error_class=true></errorMessages>
				</div>

			</ng-container>

			<ng-container *ngIf="fetch.searchUtils.status == errorCodes.DONE">
				<search-result	[properties]="properties"
												[results]="fetch.results"
												[status]="fetch.searchUtils.status" 
												[type]="resultType" [showEnermaps]="showEnermaps" [prevPath]="prevPath">
				</search-result>

<!--        <div *ngIf="searchLinkToAdvancedPage && fetch.results?.length > 0" class="uk-text-right">-->
<!--          <a class="el-content uk-button uk-button-text" [queryParams]="params" [routerLink]="searchLinkToAdvancedPage">-->
<!--            View all-->
<!--            <span *ngIf="fetch.searchUtils.totalResults <= searchNumber">in search page</span>-->
<!--          </a>-->
<!--        </div>-->
			</ng-container>
    `
})

export class SearchTabComponent {
    @Input() public isMobile: boolean = false;
    @Input() prevPath: string = "";
    @Input() public fetch;
    @Input() public resultType: string;
    @Input() public params: any;
    @Input() public searchNumber: number = 5;
    @Input() public searchLinkToAdvancedPage: string;
    @Input() properties: EnvProperties;
    @Input() customTitle;
    @Input() showEnermaps: boolean;

    public errorCodes: ErrorCodes = new ErrorCodes();

    public getEntityName(entityType: string): string {
      return StringUtils.getEntityName(entityType, true);
    }

    get paramsForExternalUrl() {
      let parameters: string = "";
      Object.keys(this.params).forEach(paramKey => {
        parameters += (parameters ? "&" : "?") + paramKey+"="+this.params[paramKey];
      })
      return parameters;
    }
}
