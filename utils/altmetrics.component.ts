import {Component, Input} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

declare var loadAltmetrics:any;
//<altmetrics [id]="10.7717/peerj.1150" type="doi"></altmetrics>
@Component({
  selector: 'altmetrics',
  template: `
		<div [innerHTML]="altmetrics | safeHtml"></div>
  `
})
export class AltMetricsComponent {
  @Input() id;
  @Input() type = 'doi'; // doi or arxiv
	@Input() size: string = 'donut';

  // public doi="10.7717/peerj.1150";
  public altmetrics:string;
  private sub:any;

  constructor(private route: ActivatedRoute) {

    // if (typeof document !== 'undefined') {
    //    let yourModule = require('../utils/altmetrics.js');
    //  }
  }
  ngOnInit() {
    this.sub =  this.route.queryParams.subscribe(data => {
      if(this.type == "doi"){
        this.altmetrics='<div data-badge-details="right" data-hide-no-mentions="true" data-link-target="_blank" data-badge-type="'+this.size+'" data-doi="'+this.id+'" class="altmetric-embed"> </div>';
      }else{
        this.altmetrics='<div data-badge-details="right" data-hide-no-mentions="true" data-link-target="_blank" data-badge-type="'+this.size+'" data-arxiv-id="'+this.id+'" class="altmetric-embed"> </div>';
      }
      if (typeof document !== 'undefined') {
      //   let yourModule = require('../utils/altmetrics.js');
        loadAltmetrics("altmetric-embed-js","https://d1bxh8uas1mnw7.cloudfront.net/assets/altmetric_badges-8f271adb184c21cc5169a7f67f7fe5ab.js");
      }
    });
  }
  
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
