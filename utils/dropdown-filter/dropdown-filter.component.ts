import {Component, ElementRef, Input, ViewChild} from "@angular/core";
import {MobileDropdownComponent} from "../mobile-dropdown/mobile-dropdown.component";

declare var UIkit;

@Component({
  selector: 'dropdown-filter',
  template: `
  <button #toggle class="uk-button uk-button-default uk-flex uk-flex-middle"
          [class.uk-disabled]="disabled" [disabled]="disabled">
    <span>{{name}}<span *ngIf="count > 0">({{count}})</span></span>
    <icon [flex]="true" class="uk-margin-xsmall-left" [name]="'expand_' + (isOpen?'less':'more')"></icon>
  </button>
  <div *ngIf="!isMobile" #dropdownElement class="uk-dropdown"
       [class.uk-height-max-large]="overflow" [class.uk-overflow-auto]="overflow"
       [ngStyle]="dropdownMinWidth?{'min-width.px': dropdownMinWidth}:''"
       [ngClass]="dropdownClass" uk-dropdown="mode: click; delay-hide: 0;">
    <div>
      <ng-container *ngTemplateOutlet="content"></ng-container>
    </div>
  </div>
  <mobile-dropdown *ngIf="isMobile" #mobileDropdown [toggle]="toggle">
      <div [ngClass]="dropdownClass">
          <ng-container *ngTemplateOutlet="content"></ng-container>
      </div>
  </mobile-dropdown>
  <ng-template #content>
      <ng-content></ng-content>
  </ng-template>
  `
})
export class DropdownFilterComponent {
  @Input()
  public name;
  @Input()
  public count: number = 0;
  @Input()
  public dropdownClass: string;
  @Input()
  public dropdownMinWidth: number;
  @Input()
  public overflow: boolean = true;
  @Input()
  public disabled = false;
  @Input()
  public isMobile: boolean = false;
  @ViewChild("dropdownElement") dropdownElement: ElementRef;
  @ViewChild("mobileDropdown") mobileDropdown: MobileDropdownComponent;

  get isOpen() {
    if(this.isMobile) {
      return this.mobileDropdown?.opened;
    } else {
      return (typeof document !== 'undefined') && this.dropdownElement && UIkit.dropdown(this.dropdownElement.nativeElement).isActive();
    }
  }
  
  closeDropdown() {
    if(this.isMobile) {
      this.mobileDropdown.close();
    } else {
      UIkit.dropdown(this.dropdownElement.nativeElement).hide();
    }
  }
}
