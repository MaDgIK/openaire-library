import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {DropdownFilterComponent} from "./dropdown-filter.component";
import {IconsModule} from "../icons/icons.module";
import {MobileDropdownModule} from "../mobile-dropdown/mobile-dropdown.module";

@NgModule({
  imports: [CommonModule, IconsModule, MobileDropdownModule],
  declarations: [DropdownFilterComponent],
  exports: [DropdownFilterComponent]
})
export class DropdownFilterModule {}
