import {Pipe, PipeTransform} from '@angular/core';

import {SafeUrl} from '@angular/platform-browser';
import {StringUtils} from '../string-utils.class';

@Pipe({ name: 'htmlToString'})
export class HTMLToStringPipe implements PipeTransform  {
  constructor() {}
  
  transform(value):SafeUrl {
    return StringUtils.HTMLToString(value);
  }
}
