import { NgModule }            from '@angular/core';
import {HTMLToStringPipe} from './HTMLToString.pipe';



@NgModule({
  imports: [

  ],
  declarations: [
    HTMLToStringPipe
  ],
  providers:[],
  exports: [
    HTMLToStringPipe
    ]
})


export class HTMLToStringPipeModule{

}
