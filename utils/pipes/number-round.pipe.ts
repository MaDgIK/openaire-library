import {Pipe, PipeTransform} from "@angular/core";
import {Level, NumberSize, NumberUtils} from "../number-utils.class";
import {DecimalPipe, registerLocaleData} from "@angular/common";
import l from '@angular/common/locales/eu';
import e from '@angular/common/locales/extra/eu';

@Pipe({name: 'numberRound'})
export class NumberRoundPipe implements PipeTransform {
  decimalPipe: DecimalPipe = new DecimalPipe("en");

  constructor() {
    registerLocaleData(l, 'eu', e);
  }

  /**
   * Args: Level: 0 (default): ALL, 1: K, 2: M, 3:B
   *       Decimal: how many decimals should be shown (e.g 1 -> 62.1)
   * */
  transform(value: number | string, ...args: any[]): any {
    let level = Level.ALL;
    let decimal = 0;
    let locale: string = 'en';
    if (args[0]) {
      level = args[0];
    }
    if (args[1]) {
      decimal = args[1];
    }
    if (args[2]) {
      locale = args[2];
      this.decimalPipe = new DecimalPipe(locale);
    } else {
      this.decimalPipe = new DecimalPipe('en');
    }
    let size: NumberSize = NumberUtils.roundNumber(value, level, decimal);
    return this.decimalPipe.transform(size.number) + (size.size ? '<span class="number-size">' + size.size + '</span>' : '');
  }
}
