import {NgModule} from "@angular/core";
import {NumberRoundPipe} from "./number-round.pipe";
import {NumberPercentagePipe} from "./number-percentage.pipe";

@NgModule({
  declarations: [NumberRoundPipe, NumberPercentagePipe],
  exports: [NumberRoundPipe, NumberPercentagePipe]
  
})
export class NumberRoundModule {

}
