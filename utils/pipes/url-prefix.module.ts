import {NgModule} from "@angular/core";
import {UrlPrefixPipe} from "./url-prefix.pipe";

@NgModule({
  declarations: [UrlPrefixPipe],
  exports: [UrlPrefixPipe]
})
export class UrlPrefixModule {}
