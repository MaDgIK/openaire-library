import {Pipe, PipeTransform} from "@angular/core";
import {Level, NumberSize, NumberUtils} from "../number-utils.class";
import {DecimalPipe} from "@angular/common";

@Pipe({name: 'numberPercentage'})
export class NumberPercentagePipe implements PipeTransform {
  decimalPipe: DecimalPipe = new DecimalPipe("en");

  constructor() {
  }

  /**
   * */
  transform(value: number | string, ...args: any[]): any {
    let locale: string = 'en';
    if (args[0]) {
      locale = args[0];
      this.decimalPipe = new DecimalPipe(locale);
    } else {
      this.decimalPipe = new DecimalPipe('en');
    }
    value = Number.parseFloat(value.toString()) * 100;
    return this.decimalPipe.transform(value) + '<span class="number-size">%</span>';
  }
}
