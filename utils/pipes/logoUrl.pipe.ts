import {Pipe, PipeTransform} from '@angular/core'
import {Stakeholder} from "../../monitor/entities/stakeholder";
import {CommunityInfo} from "../../connect/community/communityInfo";
import {StringUtils} from "../string-utils.class";

@Pipe({ name: 'logoUrl'})
export class LogoUrlPipe implements PipeTransform  {
  
  constructor() {}
  
  transform(value: Stakeholder | CommunityInfo): string {
    return StringUtils.getLogoUrl(value);
  }
}
