import {Pipe, PipeTransform} from "@angular/core";
import {DomSanitizer, SafeUrl} from "@angular/platform-browser";
import {StringUtils} from "../string-utils.class";

@Pipe({name: 'urlPrefix'})
export class UrlPrefixPipe implements PipeTransform  {
  transform(value): SafeUrl {
    return StringUtils.urlPrefix(value) + value;
  }
}
