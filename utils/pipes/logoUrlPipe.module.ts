import {NgModule} from '@angular/core';
import {LogoUrlPipe} from "./logoUrl.pipe";

@NgModule({
  imports: [],
  declarations: [
    LogoUrlPipe
  ],
  providers: [],
  exports: [
    LogoUrlPipe
  ]
})
export class LogoUrlPipeModule {

}
