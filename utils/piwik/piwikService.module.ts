import { NgModule }            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';

import {PiwikService} from './piwik.service';


@NgModule({
  imports: [
    CommonModule, FormsModule,

  ],
  declarations: [

  ],
  providers: [ PiwikService ],
  exports: [

    ]
})
export class PiwikServiceModule{ }
