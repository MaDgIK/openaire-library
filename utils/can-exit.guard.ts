import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';


export interface IDeactivateComponent {
    canExit: () => Observable<boolean> | Promise<boolean> | boolean;
}

@Injectable({
    providedIn: 'root'
})
export class CanExitGuard  {

    constructor() {
    }

    canDeactivate(component: IDeactivateComponent): Observable<boolean> | Promise<boolean> | boolean {

        return component.canExit ? component.canExit() : true;
    }

}