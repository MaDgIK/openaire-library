import {Component, Input} from "@angular/core";

@Component({
  selector: 'results-and-pages',
  template: `
    <ng-container *ngIf="!hasSearch; else elseBlock">
      <h6 *ngIf="type && totalResults > 0" [ngClass]="customClasses">
        <span>{{totalResults | number}}</span>
        <span class="uk-text-meta uk-text-capitalize"> {{type}}, page </span>
        <span>{{currentPage}}</span>
        <span class="uk-text-meta"> of </span>
        <span>{{getTotalPages() | number}}</span>
      </h6>
    </ng-container>
    <ng-template #elseBlock>
      <h6 *ngIf="type && totalResults > 0" [ngClass]="customClasses">
        <span>{{totalResults | number}}</span>
        <span class="uk-text-meta uk-text-capitalize"> {{type}}</span>
        <ng-container *ngIf="searchTerm">
          <span class="uk-text-meta"> for </span>
          <span class="uk-text-bold">{{searchTerm}}</span>
        </ng-container>
      </h6>
    </ng-template>
  `
})

export class ResultsAndPagesNumComponent {
  @Input() type: string;
  @Input() page: number = 1;
  @Input() pageSize: number = 10;
  @Input() totalResults: number = 0;
  @Input() customClasses: string = "";
  @Input() hasSearch: boolean = false;
  @Input() searchTerm: string;

  constructor() {}
  
  get currentPage() {
    if(this.page > this.getTotalPages()) {
      return this.getTotalPages();
    } else {
      return this.page;
    }
  }
  
  getTotalPages(){
    let total: number;
    let i= this.totalResults/this.pageSize;
    let integerI=parseInt(''+i);
    total = parseInt(''+((i==integerI)?i:i+1));

    return total;
  }
}
